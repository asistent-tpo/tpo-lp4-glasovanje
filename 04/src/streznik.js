var express = require('express');
var http = require('http');
var port = process.env.PORT || 3000;
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server); // initiate socket.io server
var rp = require('request-promise');

var mongoose = require('mongoose');
mongoose.connect('mongodb+srv://admin:admin@cluster0-hg9rl.mongodb.net/StraightAs?retryWrites=true', {useNewUrlParser: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connected to database");
});

var nodemailer = require('nodemailer')

app.use(express.static(__dirname + '/public'));

/**
 * Poženi strežnik
 */
server.listen(port, function () {
  console.log('Strežnik je pognan na portu ' + port + '!');
});

var uporabnikSchema = new mongoose.Schema({
  email: String,
  geslo: String,
  zeton: String,
  confirmed: Boolean
});
var predmetSchema = new mongoose.Schema({
  predmet: String,
  dan: String,
  zacetek: Number,
  trajanje: Number,
  uporabnikId: String
});
var todoSchema = new mongoose.Schema({
  uporabnikId: String,
  text: String
});
var koledarSchema = new mongoose.Schema({
  uporabnikId: String,
  opis: String,
  naziv: String,
  trajanje: Number,
  zacetek: Date
});
var dogodekSchema = new mongoose.Schema({
  naziv: String
});

io.sockets.on('connection', function (socket) {
  console.log("User connected");

  socket.on('saveUser', function(data){
    console.log("Saving user...")
    var Uporabnik = mongoose.model('uporabniki', uporabnikSchema, 'uporabniki');
    new Uporabnik({
      email: data.email,
      geslo: data.geslo,
      zeton: data.zeton,
      confirmed: false
    }).save(function (err, u) {
      if (err) return console.error(err);
      console.log(u);
      socket.emit("userSaved", { id: u._id });
    });
  });

  socket.on('getUser', function(data){
    console.log("Getting User...")
    var Uporabnik = mongoose.model('uporabniki', uporabnikSchema, 'uporabniki');
    Uporabnik.findOne({email: data.email}).exec(function(err, uporabnik) {
      console.log("Returning Uporabnik");
      if(err){
          socket.emit("userCallback", { uporabnik: null });
      }else{
        socket.emit("userCallback", { uporabnik: uporabnik });
      }

    });
  });

  socket.on('confirmUser', function(data){
    console.log("Confirming user...")
    var Uporabnik = mongoose.model('uporabniki', uporabnikSchema, 'uporabniki');
    Uporabnik.update({_id: data.uporabnikId},
      {confirmed: true},
      {upsert: false},
      function (err, u) {
        if (err) return console.error(err);
        console.log(u);
    });
  });

  socket.on('changePassword', function(data){
    console.log("Changing password...")
    var Uporabnik = mongoose.model('uporabniki', uporabnikSchema, 'uporabniki');
    Uporabnik.update({_id: data.uporabnikId},
      {geslo: data.geslo},
      {upsert: false},
      function (err, u) {
        if (err) return console.error(err);
        console.log(u);
    });
  });

  socket.on('savePredmet', function(data){
    console.log("Saving predmet...")
    var Predmet = mongoose.model('predmeti', predmetSchema, 'predmeti');
    new Predmet({
      predmet: data.predmet,
      dan: data.dan.toLowerCase(),
      zacetek: data.zacetek,
      trajanje: data.trajanje,
      uporabnikId: data.uporabnikId
    }).save(function (err, u) {
      if (err) return console.error(err);
      console.log(u);
    });
  });

  socket.on('editPredmet', function(data){
    console.log("Editing predmet...")
    var Predmet = mongoose.model('predmeti', predmetSchema, 'predmeti');
    Predmet.update({_id: data.predmetId},
      {predmet: data.predmet,
        dan: data.dan.toLowerCase(),
        zacetek: data.zacetek,
        trajanje: data.trajanje,
        uporabnikId: data.uporabnikId},
      {upsert: false},
      function (err, u) {
        if (err) return console.error(err);
        console.log(u);
    });
 });

  socket.on('deletePredmet', function(data){
    console.log("Deleting predmet...")
    var Predmet = mongoose.model('predmeti', predmetSchema, 'predmeti');
    Predmet.find({ _id:data.predmetId}).remove().exec();
  });

  socket.on('getPredmeti', function(data){
    console.log("Getting predmeti...")
    var Predmet = mongoose.model('predmeti', predmetSchema, 'predmeti');
    Predmet.find({uporabnikId: data.uporabnikId}).exec(function(err, vsiPredmeti) {
      socket.emit("predmetiCallback", { predmeti: vsiPredmeti });
    });
  });

  socket.on('getTODO', function(data){
    console.log("Getting TODOs...")
    var TODO = mongoose.model('todos', todoSchema, 'todos');
    TODO.find({uporabnikId: data.uporabnikId}).exec(function(err, vsiTODO) {
      console.log("Returning TODOs");
      socket.emit("TODOCallback", { todos: vsiTODO });
    });
  });

  socket.on('saveTODO', function(data){
    console.log("Saving TODO...")
    var TODO = mongoose.model('todos', todoSchema, 'todos');
    new TODO({
      uporabnikId: data.uporabnikId,
      text: data.text
    }).save(function (err, u) {
      if (err) return console.error(err);
      console.log(u);
    });
  });

  socket.on('editTODO', function(data){
    console.log("Editing todo...")
    var TODO = mongoose.model('todos', todoSchema, 'todos');
    TODO.updateOne({_id: data.todoId},
      {text: data.text},
      {upsert: false},
      function (err, u) {
        if (err) return console.error(err);
        console.log(u);
    });
 });

  socket.on('deleteTODO', function(data){
    console.log("Deleting todo...")
    var TODO = mongoose.model('todos', todoSchema, 'todos');
    TODO.find({ _id:data.todoId}).remove().exec();
  });

  socket.on('sendConfirmation', function(data){
    console.log("Sending confirmation...")
    mail(data.email, data.link)
  });

  socket.on('saveKoledar', function(data){
    console.log("Saving koledar...")
    var Koledar = mongoose.model('koledarji', koledarSchema, 'koledarji');
    new Koledar({
      uporabnikId: data.uporabnikId,
      opis: data.opis,
      naziv: data.naziv,
      trajanje: data.trajanje,
      zacetek: data.zacetek
    }).save(function (err, u) {
      if (err) return console.error(err);
      console.log(u);
    });
  });
  socket.on('getKoledar', function(data){
    console.log("Getting koledar...");
    var Koledar = mongoose.model('koledarji', koledarSchema, 'koledarji');
    Koledar.find({uporabnikId: data.uporabnikId}).exec(function(err, vsiKoledarji) {
      console.log("Returning koledar");
      socket.emit("KoledarCallback", { koledarji: vsiKoledarji });
    });
  });

  socket.on('getKoledarById', function(data){
    console.log("Getting koledar by id...");
    var Koledar = mongoose.model('koledarji', koledarSchema, 'koledarji');
    Koledar.findOne({_id: data.koledarId}).exec(function(err, koledar) {
      console.log("Returning koledar");
      if(err){
        socket.emit("KoledarByIdCallback", { koledar: null });
      }else{
        socket.emit("KoledarByIdCallback", { koledar: koledar });
      }
    });
  });

  socket.on('updateKoledar', function(data){
    console.log("Editing koledar...")
    var Koledar = mongoose.model('koledarji', koledarSchema, 'koledarji');
    Koledar.updateOne({_id: data.koledarId},
      {     opis: data.opis,
            naziv: data.naziv,
            trajanje: data.trajanje,
            zacetek: data.zacetek},
      {upsert: false},
      function (err, u) {
        if (err) return console.error(err);
        console.log(u);
    });
 });

  socket.on('deleteKoledar', function(data){
    console.log("Deleting koledar...")
    var Koledar = mongoose.model('koledarji', koledarSchema, 'koledarji');
    Koledar.find({ _id:data.koledarId}).remove().exec();
  });

  socket.on('getDogodek', function(data){
    console.log("Getting dogodki...");
    var Dogodek = mongoose.model('dogodki', dogodekSchema, 'dogodki');
    Dogodek.find({}).exec(function(err, vsiDogodki) {
      console.log("Returning dogodki");
      socket.emit("DogodekCallback", { dogodki: vsiDogodki });
    });
  });

  // Bus
  socket.on('bus', function(data){
    console.log("Getting bus");
    rp('https://www.trola.si/' + data.station + '/')
      .then(function (html) {
        socket.emit("busCallback", { data: html });
     })
      .catch(function (err) {
          socket.emit("busCallback", { data: null });
      });
  });


});

// Mail
async function mail(email, link){
  console.log("Sending mail...")
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: "straight.as.confirmation@gmail.com",
                pass: "tpo4straightas"
            }
        });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"StraightAs" <straight.as.confirmation@gmail.com>', // sender address
    to: email, // list of receivers
    subject: "Potrditev registracije", // Subject line
    text: "Povezava za potrditev računa: " + link // plain text body
  });
}

module.exports = app;

var testUporabnik = "5cda904bdfedf035b0081053";
var currentEmail = "";

function dayToNumber(day){
  if(day == "ponedeljek"){
    return 1;
  }
  if(day == "torek"){
    return 2;
  }
  if(day == "sreda"){
    return 3;
  }
  if(day == "četrtek"){
    return 4;
  }
  if(day == "petek"){
    return 5;
  }
}

function numberToDay(day){
  if(day == 1){
    return "Ponedeljek";
  }
  if(day == 2){
    return "Torek";
  }
  if(day == 3){
    return "Sreda";
  }
  if(day == 4){
    return "Četrtek";
  }
  if(day == 5){
    return "Petek";
  }
}

var predmeti = [];
function loadUrnik(){
  socket.emit("getPredmeti", {uporabnikId: testUporabnik});
  socket.on("predmetiCallback", function(data){
    predmeti = data.predmeti;
    var urnik = document.getElementById("urnik");
    for(var i = 7; i <= 21; i++){
      var row = urnik.insertRow(i - 7);
      cell = row.insertCell(0);
      cell.innerHTML = i + ":00";
      for(var j = 1; j <= 5; j++){
        var cell = row.insertCell(j);

        // fill cell with predmet name
        cell.innerHTML = "";

        for(var k = 0; k < predmeti.length; k++){
          if(dayToNumber(predmeti[k].dan) == j
            && predmeti[k].zacetek <= i
            && predmeti[k].zacetek + predmeti[k].trajanje > i){
              cell.classList.add("predmet-cell");
              cell.innerHTML = cell.innerHTML+
              "<button class='predmet-button'" +
              " id='" + predmeti[k]._id + "' " +
              " onclick=predmetClick('" +predmeti[k]._id+ "',"+k+")" +
              ">" +
              predmeti[k].predmet+"</button>";
          }
        }

        cell.classList.add("urnik-cell");
      }
    }

  })
}

var todos = []
function loadTODO(){
  socket.emit("getTODO", {uporabnikId: testUporabnik});
  socket.on("TODOCallback", function(data){
    todos = data.todos;
    var todoList = document.getElementById("todo-list");
    for(var i = 0; i < todos.length; i++){
      todoList.innerHTML = todoList.innerHTML +
      '<li class="todo">' +
        '<a href="#" class="todo"' + " onclick=todoClick('" +todos[i]._id+ "',"+i+")>" +
          '<p>' + todos[i].text + '</p>' +
        '</a>' +
      '</li>';
    }
  })
}

var koledarji = []
function loadKoledar(){
  socket.emit("getKoledar", {uporabnikId: testUporabnik});
  socket.on("KoledarCallback", function(data){
    koledarji = data.koledarji;
    var todoList = document.getElementById("todo-list");
    for(let i = 0; i < koledarji.length; i++){
      let koledar_data = koledarji[i];
      let endDate = new Date();
      endDate.setDate(new Date(koledar_data.zacetek).getDate() + koledar_data.trajanje - 1);
      $('#calendar').fullCalendar('renderEvent',
        {
          id: koledar_data._id,
          title: koledar_data.naziv + ":" + koledar_data.opis,
          start: new Date(koledar_data.zacetek),
          end: endDate,
          allDay: true,
          className: 'info'
        },
        true
      );
    }
  })
}

function novoGeslo(){
  let geslo =document.getElementById("inputTrenutnoGeslo").value;
  let novoGeslo =document.getElementById("inputNovoGeslo").value;
  let novoGeslo2 =document.getElementById("inputNovoGeslo2").value;

  if(geslo != null && geslo.length > 0 &&
    novoGeslo != null && novoGeslo.length > 0 &&
    novoGeslo2 != null && novoGeslo2.length > 0 ){
    socket.emit("getUser", {email: currentEmail});
    socket.on("userCallback", function(data){
      console.log(data);
      if(data.uporabnik != null){
        if(data.uporabnik.geslo ==  sha256(geslo)){
          if(novoGeslo == novoGeslo2){
            socket.emit("changePassword", {uporabnikId: data.uporabnik._id, geslo: sha256(novoGeslo)});
            document.getElementById("closeNovoGeslo").click();
            window.location.reload(false);
          }else{
            showPasswordChangeError("Gesli se ne ujemata.")
          }
        }else{
          showPasswordChangeError("Napačno geslo.")
        }
      }
    });
  }else{
    showPasswordChangeError("Polja nimajo veljavnih vrednosti");
  }
}

function showPasswordChangeError(text){
  document.getElementById("passwordChangeErrorText").innerHTML = text;
  document.getElementById("passwordChangeErrorText").style.visibility = "visible";
}

function getUser(){
  if(document.cookie != null && document.cookie.length > 0){
    let spl = document.cookie.split("=");
    testUporabnik = spl[0];
    currentEmail = spl[1];
    return true;
  }
  return false;
}

function logout(){
  document.cookie = "";
  window.location.replace("login.html");
}


if(getUser()){
  loadKoledar();
  loadUrnik();
  loadTODO();
}else{
  window.location.replace("login.html");
}

var selectedDate = null;
var editKoledar = null;

function dodajKoledar(){
  console.log(selectedDate);
  let opis = document.getElementById("inputKoledarOpis").value;
  let naziv = document.getElementById("inputKoledarNaziv").value;
  let trajanje = parseInt(document.getElementById("inputKoledarTrajanje").value);
  if(opis == null){
    opis = "";
  }
  if(naziv == null){
    naziv = "";
  }
  if(trajanje == null || trajanje < 1){
    trajanje = 1;
  }
  socket.emit("saveKoledar", {uporabnikId: testUporabnik, opis: opis, naziv: naziv, zacetek: selectedDate, trajanje: trajanje});
  document.getElementById("closeNovKoledar").click();
  $('#calendar').fullCalendar('unselect');
  window.location.reload(false);
}

function koledarClick(koledarId){
    socket.emit("getKoledarById", {koledarId: koledarId})
    socket.on("KoledarByIdCallback", function(data){
      if(data != null){
        $('#urediKoledar').modal();
        editKoledar = data.koledar;
        document.getElementById("inputUrediKoledarNaziv").value = data.koledar.naziv;
        document.getElementById("inputUrediKoledarOpis").value = data.koledar.opis;
        document.getElementById("inputUrediKoledarTrajanje").value = data.koledar.trajanje;
      }
    });
}


function urediKoledar(){
  let opis = document.getElementById("inputUrediKoledarOpis").value;
  let naziv = document.getElementById("inputUrediKoledarNaziv").value;
  let trajanje = parseInt(document.getElementById("inputUrediKoledarTrajanje").value);
  if(opis == null){
    opis = "";
  }
  if(naziv == null){
    naziv = "";
  }
  if(trajanje == null || trajanje < 1){
    trajanje = 1;
  }

  socket.emit("updateKoledar", {koledarId: editKoledar._id, opis: opis, naziv: naziv, zacetek: editKoledar.zacetek, trajanje: trajanje});
  document.getElementById("closeUrediKoledar").click();
  window.location.reload(false);
}

function izbrisiKoledar(){
  socket.emit("deleteKoledar", {koledarId: editKoledar._id})
  document.getElementById("closeUrediKoledar").click();
  window.location.reload(false);
}

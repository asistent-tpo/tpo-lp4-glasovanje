var todoEditId = "";
var todoEditIndex = "";

function dodajTODO(){
  var text = document.getElementById("inputTodoText").value;
  socket.emit("saveTODO", {uporabnikId: testUporabnik, text: text})
  document.getElementById("closeNovTodo").click();
  window.location.reload(false);
}

function urediTODO(){
  var text = document.getElementById("inputUrediTodoText").value;

  socket.emit("editTODO", {todoId: todoEditId, text: text})
  document.getElementById("closeUrediTODO").click();
  window.location.reload(false);
}

function izbrisiTODO(){
  socket.emit("deleteTODO", {todoId: todoEditId})
  document.getElementById("closeUrediTODO").click();
  window.location.reload(false);
}

function todoClick(todoId, todoIndex){
  todoEditId = todoId;
  todoEditIndex = todoIndex;
  $('#urediTODO').modal();
  document.getElementById("inputUrediTodoText").value = todos[todoEditIndex].text;
}

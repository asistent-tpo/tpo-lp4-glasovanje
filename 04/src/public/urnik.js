var predmetEditId = "";
var predmetEditIndex = "";
function dodajPredmet(){
  var predmet = document.getElementById("inputPredmet").value;
  var dan = document.getElementById("inputDan").value;
  var zacetek = parseInt(document.getElementById("inputZacetek").value.split(":")[0]);
  var trajanje = parseInt(document.getElementById("inputTrajanje").value);
  socket.emit("savePredmet", {predmet: predmet, dan: dan, zacetek: zacetek, trajanje: trajanje, uporabnikId: testUporabnik})
  document.getElementById("closeNovPredmet").click();
  window.location.reload(false);
}

function urediPredmet(){
  var predmet = document.getElementById("inputUrediPredmet").value;
  var dan = document.getElementById("inputUrediDan").value;
  var zacetek = parseInt(document.getElementById("inputUrediZacetek").value.split(":")[0]);
  var trajanje = parseInt(document.getElementById("inputUrediTrajanje").value);
  socket.emit("editPredmet", {predmetId: predmetEditId, predmet: predmet, dan: dan, zacetek: zacetek, trajanje: trajanje, uporabnikId: testUporabnik})
  document.getElementById("closeUrediPredmet").click();
  window.location.reload(false);
}

function izbrisiPredmet(){
  socket.emit("deletePredmet", {predmetId: predmetEditId})
  document.getElementById("closeUrediPredmet").click();
  window.location.reload(false);
}

function predmetClick(predmetId, predmetIndex){
  predmetEditId = predmetId;
  predmetEditIndex = predmetIndex;
  $('#urediPredmet').modal();
  document.getElementById("inputUrediPredmet").value = predmeti[predmetEditIndex].predmet;
  document.getElementById("inputUrediDan").value = capitalizeFirstLetter(predmeti[predmetEditIndex].dan);
  let time = predmeti[predmetEditIndex].zacetek + ":00";
  if(time.length < 5){
    time = "0" + time;
  }
  document.getElementById("inputUrediZacetek").value = time;
  document.getElementById("inputUrediTrajanje").value = predmeti[predmetEditIndex].trajanje + "";
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

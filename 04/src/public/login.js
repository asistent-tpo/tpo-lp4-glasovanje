function register(){
  let mail = document.getElementById("inputEmail").value;
  let pass = document.getElementById("inputPassword").value;
  if(mail != null && mail.length > 0 && pass != null && pass.length > 0){
    socket.emit("getUser", {email: mail});
    socket.on("userCallback", function(data){
      if(data.uporabnik == null){
        socket.emit("saveUser", {email: mail, geslo: sha256(pass), zeton: "123"});
        socket.on("userSaved", function(data){
          console.log("User saved")
          socket.emit("sendConfirmation", {email: mail, link: window.location.href +"?id=" + data.id});
            showError("Potrditvena povezava je bila poslana na vaš email naslov.");
        });
      }else{
        showError("Uporabnik že obstaja");
      }
    });
  }else{
    showError("Polja nimajo veljavnih vrednosti");
  }
}

function signin(){
  let mail = document.getElementById("inputEmail").value;
  let pass = document.getElementById("inputPassword").value;
  if(mail != null && mail.length > 0 && pass != null && pass.length > 0){
    socket.emit("getUser", {email: mail});
    socket.on("userCallback", function(data){
      console.log("Got user")
      console.log(data.uporabnik)
      if(data.uporabnik != null && data.uporabnik.confirmed){
        if(data.uporabnik.geslo == sha256(pass)){
          document.cookie = data.uporabnik._id + "=" + mail + "=" + parseInt(new Date().getTime() / 1000);
          window.location.replace("index.html");
        }else{
          showError("Napačno geslo ali email naslov.");
        }
      }else if(data.uporabnik != null && !data.uporabnik.confirmed){
        showError("Email računa ni potrjen.");
      }else{
        showError("Račun s takšnim email naslovom in geslom ne obstaja.");
      }
    });
  }else{
    showError("Polja nimajo veljavnih vrednosti");
  }
}

function checkConfirmation(){
  var url = new URL(window.location.href);
  var id = url.searchParams.get("id");
  if(id != null){
    socket.emit("confirmUser", {uporabnikId: id});
    showError("Vaš račun je bil uspešno potrjen.");
  }
}

function showError(text){
  document.getElementById("errorText").innerHTML = text;
  document.getElementById("errorText").style.visibility = "visible";
}

checkConfirmation()

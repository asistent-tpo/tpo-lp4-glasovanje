# 12. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Spletna povezava do uveljavitve na Bitbucket repozitorij: https://bitbucket.org/mz9337/tpo-lp4/commits/bc1049997c96700069d089b16609c31b090b867a

Spletna povezava do različice produkcijske postavitve aplikacije: https://straightas.herokuapp.com

Testni študentski uporabnik:

- uporabniško ime: student@mailinator.com,

- geslo: 12345678 



Testni uporabnik - administrator:

- uporabniško ime: admin@mailinator.com,

- geslo: admin123



Testni uporabnik - urejevalec dogodkov:

- uporabniško ime: manager@mailinator.com,

- geslo: evman123

describe('Calendar controller,', function() {
    beforeEach(module('straightAs'));

    var calendarController;
    beforeEach(inject(function($controller){ 
        calendarController = $controller('calendarController');
    }));

    describe('getCalendarDays():', function() {
        describe('Check if correct number of weeks are returned:', function() {
            it('Januar 2019 should have 5', function() {
                expect(calendarController.getCalendarDays(0, 2019).length).toBe(5);
            });

            it('December 2019 should have 6', function() {
                expect(calendarController.getCalendarDays(11, 2019).length).toBe(6);
            });

            it('Februar 2021 should have 4', function() {
                expect(calendarController.getCalendarDays(1, 2021).length).toBe(4);
            });
        });

        describe('Check if number of days in a month are correct:', function() {
            it('Januar 2019 should have 31', function() {
                var calendar = [];            
                var last_week = {};
                var num_of_days = 0;

                calendar = calendarController.getCalendarDays(0, 2019);
                
                last_week = calendar[calendar.length-1];
                last_week = Object.keys(last_week).reduce(function (r, k) { return r.concat(k, last_week[k]); }, []);

                num_of_days = Math.max.apply(Math, last_week.map(function(o) { return isNaN(parseInt(o.day)) ? 0 : parseInt(o.day); }));

                expect(num_of_days).toBe(31);
            });

            it('April 2019 should have 30', function() {
                var calendar = [];            
                var last_week = {};
                var num_of_days = 0;

                calendar = calendarController.getCalendarDays(3, 2019);
                
                last_week = calendar[calendar.length-1];
                last_week = Object.keys(last_week).reduce(function (r, k) { return r.concat(k, last_week[k]); }, []);

                num_of_days = Math.max.apply(Math, last_week.map(function(o) { return isNaN(parseInt(o.day)) ? 0 : parseInt(o.day); }));

                expect(num_of_days).toBe(30);
            });

            it('Februar 2019 should have 28', function() {
                var calendar = [];            
                var last_week = {};
                var num_of_days = 0;

                calendar = calendarController.getCalendarDays(1, 2019);
                
                last_week = calendar[calendar.length-1];
                last_week = Object.keys(last_week).reduce(function (r, k) { return r.concat(k, last_week[k]); }, []);

                num_of_days = Math.max.apply(Math, last_week.map(function(o) { return isNaN(parseInt(o.day)) ? 0 : parseInt(o.day); }));

                expect(num_of_days).toBe(28);
            });

            it('Februar 2020 should have 29', function() {
                var calendar = [];            
                var last_week = {};
                var num_of_days = 0;

                calendar = calendarController.getCalendarDays(1, 2020);
                
                last_week = calendar[calendar.length-1];
                last_week = Object.keys(last_week).reduce(function (r, k) { return r.concat(k, last_week[k]); }, []);

                num_of_days = Math.max.apply(Math, last_week.map(function(o) { return isNaN(parseInt(o.day)) ? 0 : parseInt(o.day); }));

                expect(num_of_days).toBe(29);
            });
        });
    });

    describe('changeMonth():', function() {
        
        it('Januar 2019 + 1 should be Februar 2019', function() {
            calendarController.current_year = 2019;
            calendarController.current_month = 0;

            calendarController.changeMonth(1);

            expect(calendarController.current_year).toBe(2019);
            expect(calendarController.current_month).toBe(1);
        });

        it('Januar 2019 - 1 should be December 2018', function() {
            calendarController.current_year = 2019;
            calendarController.current_month = 0;

            calendarController.changeMonth(-1);

            expect(calendarController.current_year).toBe(2018);
            expect(calendarController.current_month).toBe(11);
        });

        it('December 2019 + 1 should be Januar 2020', function() {
            calendarController.current_year = 2019;
            calendarController.current_month = 11;

            calendarController.changeMonth(1);

            expect(calendarController.current_year).toBe(2020);
            expect(calendarController.current_month).toBe(0);
        });

    });

    /*describe('saveNewEvent():', function() {
        
        it('currentEvents.length should be currentEvents.length + 1', function() {
            calendarController.currentEvents.length = 0;

            calendarController.closeNewEvent();

            expect(calendarController.currentEvents.length).toBe(1);
        });
    });

    describe('comfirmDelete():', function() {
        
        it('currentEvents.length should be currentEvents.length - 1', function() {
            calendarController.currentEvents.length = 1;

            calendarController.comfirmDelete();

            expect(calendarController.currentEvents.length).toBe(0);
        });
    });*/
});
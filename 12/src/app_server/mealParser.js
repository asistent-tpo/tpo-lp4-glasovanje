var request = require('request');
var xmlParser = require("xml-parse");
var parseString = require('xml2js').parseString;
var HTMLParser = require('node-html-parser');

var apiParametri = {
	streznik: 'http://localhost:' + (process.env.PORT || 5000)
};

if (process.env.NODE_ENV === 'production') {
	apiParametri.streznik = 'https://straightas.herokuapp.com';
}


function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}

module.exports.parse = function() {

	request(
		{
			url: apiParametri.streznik + '/api/restaurants',
			method: 'GET',
		},

		function(error, response, restaurantsJSON) {

			var restaurants = JSON.parse(restaurantsJSON);

			// parseRestaurant(restaurants, res);


			restaurants.forEach(function(restaurant) {

				if (restaurant.meals.length == 0) {
					request(
						{
							url: 'https://www.studentska-prehrana.si'+restaurant['link'],
							method: 'GET',
						},
						function(error, response, html) {
							const root = HTMLParser.parse(html);

							var menuList = root.querySelector('#menu-list');
							var meals = [];

							if (menuList != null) {
								menuList.childNodes.forEach(function(child) {
									if (typeof child.structuredText != "undefined") {


										var names = child.structuredText.split("\n");
										names.forEach(function(name) {
											if (name.match(/^\d/)) {
												var meal_name = name.replace(/\d+/g, '');
												meal_name = meal_name.replace(/^\s+/,"");

												var meal = {
													name: meal_name,
													description: ''
												};

												meals.push(meal);
											}

										});
									}
								});
							}

							var upd = {meals: meals};
							

							request(
								{
									url: apiParametri.streznik + '/api/restaurants/'+restaurant['_id'],
									method: 'PUT',
									json: upd,
								},

								function(napaka, odgovor, xml) {
									console.log(napaka);
								}
							);

						}
					);
					sleep(40);
				}
				

			});

			// res.send('test');
		}
	);
};
var request = require('request');
var xmlParser = require("xml-parse");
var parseString = require('xml2js').parseString;
var HTMLParser = require('node-html-parser');
var mealParser = require('./mealParser');
var cron = require('node-cron');

var apiParametri = {
	streznik: 'http://localhost:' + (process.env.PORT || 5000)
};

if (process.env.NODE_ENV === 'production') {
	apiParametri.streznik = 'https://straightas.herokuapp.com';
}

module.exports.parse = function() {
	var iter = 1;
	var parseMealsTask = cron.schedule('0 */10 * * * *', () => {
	  	console.log("Parsam obroke " + iter);

	  	mealParser.parse();

	  	iter++;

	  	if (iter == 7) {
	  		parseMealsTask.stop();
	  		iter = 1;
	  	}
	}, {
		scheduled: false
	});

	request(
		{
			url: apiParametri.streznik + '/api/restaurants/deleteAll',
			method: 'DELETE',
		},

		function(error, response, html) {
			request(
				{
					url: 'https://www.studentska-prehrana.si/sl/restaurant',
					method: 'GET',
				},

				function(error, response, html) {
					const root = HTMLParser.parse(html);

					restaurants = root.querySelectorAll('.restaurant-row');

					restaurants.forEach(function(restaurant) {
						var attrs = restaurant.attributes;

						var rest = {
							name: attrs['data-lokal'],
							address: attrs['data-naslov'],
							lat: parseFloat(attrs['data-lat']),
							lng: parseFloat(attrs['data-lon']),
							price: attrs['data-doplacilo'],
							city: attrs['data-city'],
							link: attrs['data-detailslink'],
							insertion_date: new Date(),
						}

						request(
							{
								url: apiParametri.streznik + '/api/restaurants',
								method: 'POST',
								json: rest,
							},

							function(napaka, odgovor, xml) {
							}
						);
					});

					parseMealsTask.start();

					// res.send("Restaurants parsed!");
				}
			);
		}
	);
};
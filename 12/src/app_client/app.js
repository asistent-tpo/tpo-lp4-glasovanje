(function() {

	var authUser = function(authorization) {
		return authorization.authenticate(['user']);
	};

	var authUserRedirect = function(authorization) {
		return authorization.authenticateRedirect(['user']);
	};

	var authAdmin = function(authorization) {
		return authorization.authenticate(['admin']);
	};

	var authEventManager = function(authorization) {
		return authorization.authenticate(['event_manager']);
	};

	authUser.$inject = ['authorization'];
	authUserRedirect.$inject = ['authorization'];
	authAdmin.$inject = ['authorization'];
	authEventManager.$inject = ['authorization'];

	var run = function($rootScope, $location, authenticationService){

	    $rootScope.$on('$routeChangeError', function(event, current, previous, rejection){
	    	if(rejection === 'Redirect'){
	    		$location.path('/login');
	    	}

	        if(rejection === 'Not Authenticated'){
				console.log("Nimas pravic!!!");

				alert('Za ogled te strani nimate pravic!');
				
				if (authenticationService.hasRole(['user'])) {
					$location.path('/');
				} else if (authenticationService.hasRole(['admin'])) {
					$location.path('/admin');
				} else if (authenticationService.hasRole(['event_manager'])) {
					$location.path('/eventmanager');
				} else {
					$location.path('/login');
				}
	        }
	    })
	}

	run.$inject = ['$rootScope', '$location', 'authenticationService'];

	function nastavitev($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/home.html',
				controllerAs: 'vm',
				resolve: {
					'auth': authUserRedirect,
				}
			})
			.when('/modalnoWindow', {
				templateUrl: 'views/modalnoWindow.html',
                controllerAs: 'vm'
			})
			.when('/admin', {
				templateUrl: 'views/homeAdmin.html',
				controllerAs: 'vm',
				resolve: {
					'auth': authAdmin,
				}
			})
			.when('/registration', {
				templateUrl: 'views/registration.html',
				controller: 'registrationController',
                controllerAs: 'vm'
			})
			.when('/login', {
				templateUrl: 'views/login.html',
				controller: 'loginController',
                controllerAs: 'vm'
			})
			.when('/restaurants', {
				templateUrl: 'views/restaurants.html',
				controller: 'restaurantsController',
				controllerAs: 'rvm'
			})
			.when('/events', {
				templateUrl: 'views/events.html',
				controller: 'eventsController',
				controllerAs: 'evm',
				resolve: {
					'auth': authUser,
				}
			})
			.when('/eventmanager', {
				templateUrl: 'views/event_manager.html',
				controller: 'eventsController',
				controllerAs: 'evm',
				resolve: {
					'auth': authEventManager,
				}
			})
			.when('/buses', {
				templateUrl: 'views/buses.html',
				controller: 'busesController',
				controllerAs: 'bvm'
			})
			.when('/confirmUser/:temporaryToken', {
				templateUrl: 'views/confirmUser.html',
				controller: 'confirmUserController',
				controllerAs: 'cuvm'
			})
			.otherwise({redirectTo: '/'});
	    	$locationProvider.html5Mode(true);
	}
	
	angular.module('straightAs', ['ngRoute'])
		   .config(['$routeProvider', '$locationProvider', nastavitev])
		   .run(run);
})();
(function() {
  var admin = function() {
    return {
      restrict: 'EA',
      templateUrl: '/views/directives/admin.html',
      controller: 'adminController',
      controllerAs: 'avm'
    };
  }
  
  angular
  	.module('straightAs')
    .directive('admin', admin);
})();
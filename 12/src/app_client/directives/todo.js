(function() {
  var todo = function() {
    return {
      restrict: 'EA',
      templateUrl: '/views/directives/todo.html',
      controller: 'todoController',
      controllerAs: 'tvm'
    };
  }
  
  angular
  	.module('straightAs')
    .directive('todo', todo);
})();
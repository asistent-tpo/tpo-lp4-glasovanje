(function() {
  var header = function() {
    return {
      restrict: 'EA',
      templateUrl: '/views/directives/header.html',
      controller: 'headerController',
      controllerAs: 'hvm'
    };
  }
  
  angular
  	.module('straightAs')
    .directive('header', header);
})();
(function() {
  var schedule = function() {
    return {
      restrict: 'EA',
      templateUrl: '/views/directives/schedule.html',
      controller: 'scheduleController',
      controllerAs: 'svm'
    };
  }
  
  angular
  	.module('straightAs')
    .directive('schedule', schedule);
})();
(function() {
  var calendar = function() {
    return {
      restrict: 'EA',
      templateUrl: '/views/directives/calendar.html',
      controller: 'calendarController',
      controllerAs: 'cvm'
    };
  }
  
  angular
  	.module('straightAs')
    .directive('calendar', calendar);
})();
(function() {
	var authorization = function(authenticationService, $q) {

		return {
	        authenticate: function(roles) {
	            if (authenticationService.hasRole(roles)){
	                return true;
	            } else {
	                return $q.reject('Not Authenticated');
	            }
	        },
	        authenticateRedirect: function(roles) {
	        	if (authenticationService.hasRole(roles)){
	                return true;
	            } else {
	                return $q.reject('Redirect');
	            }
	        }
	    }
	};

	authorization.$inject = ['authenticationService', '$q'];

	angular
		.module('straightAs')
		.factory('authorization', authorization);
})();
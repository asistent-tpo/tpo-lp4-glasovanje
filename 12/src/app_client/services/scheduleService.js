(function() {
    var schedule = function($http) {
        var getSchedule = function(idTimeTable, idUser) {
            return $http.get('api/users/' + idUser + '/timeTable/' + idTimeTable);
        };

        var getScheduleList = function(idUser) {
            return $http.get('api/users/' + idUser + '/timeTable');
        };

        var postSchedule = function(idUser, schedule) {
            return $http.post('api/users/' + idUser + '/timeTable/', schedule);
        };

        var putSchedule = function(idUser, schedule) {
            return $http.put('api/users/' + idUser + '/timeTable/' + schedule._id, schedule);
        };

        var deleteSchedule = function(idUser, schedule) {
            return $http.delete('api/users/' + idUser + '/timeTable/' + schedule._id, schedule);
        };

        return {
            getSchedule: getSchedule,
            getScheduleList: getScheduleList,
            postSchedule: postSchedule,
            putSchedule: putSchedule,
            deleteSchedule: deleteSchedule
        }
    };

    schedule.$inject = ['$http'];

    angular
        .module('straightAs')
        .service('scheduleService', schedule);
})();
(function() {
	var calendar = function($http) {

		var getCalendar = function(idUser, idCalendar) { 
			return $http.get('api/users/' + idUser + '/calendar/'+ idCalendar);
		};

		var getCalendars = function(idUser) {
			return $http.get('api/users/' + idUser + '/calendar/');
		};

	    var postCalendar = function(idUser, calendar) {
	        return $http.post('api/users/' + idUser + '/calendar/', calendar);
	    };

	    var putCalendar = function(idUser, calendar) {
	    	console.log('api/users/' + idUser + '/calendar/' + calendar._id);
	    	console.log(calendar._id);
	        return $http.put('api/users/' + idUser + '/calendar/' + calendar._id, calendar);
	    };

        var deleteCalendar = function(idUser, calendar) {
	        return $http.delete('api/users/' + idUser + '/calendar/' + calendar._id, calendar);
	    };

		return {
			getCalendar: getCalendar,
			getCalendars: getCalendars,
			postCalendar: postCalendar,
			putCalendar: putCalendar,
			deleteCalendar: deleteCalendar
	    }
	};

	calendar.$inject = ['$http'];

	angular
		.module('straightAs')
		.service('calendarService', calendar);
})();
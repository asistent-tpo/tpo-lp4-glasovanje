(function() {
    var todo = function($http) {
        var getTodo = function(idTodo, idUser) {
            return $http.get('api/users/' + idUser + '/todo/' + idTodo);
        };

        var getTodoList = function(idUser) {
            return $http.get('api/users/' + idUser + '/todo');
        };

        var postTodo = function(idUser, todo) {
            return $http.post('api/users/' + idUser + '/todo/', todo);
        };

        var putTodo = function(idUser, todo) {
            return $http.put('api/users/' + idUser + '/todo/' + todo._id, todo);
        };

        var deleteTodo = function(idUser, todo) {
            return $http.delete('api/users/' + idUser + '/todo/' + todo._id, todo);
        };

        return {
            getTodo: getTodo,
            getTodoList: getTodoList,
            postTodo: postTodo,
            putTodo: putTodo,
            deleteTodo: deleteTodo
        }
    };

    todo.$inject = ['$http'];

    angular
        .module('straightAs')
        .service('todoService', todo);
})();
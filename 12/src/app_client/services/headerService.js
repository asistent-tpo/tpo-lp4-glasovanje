(function() {
	var notification = function($http) {

		var getNotifications = function() { 
			return $http.get('api/notifications');
		};

		var getNotification = function(idNotification) { 
			return $http.get('api/notifications' + idNotification);
		};

	    var postNotification = function(notification) {
	        return $http.post('api/notifications/', notification);
	    };

		return {
			getNotifications: getNotifications,
			getNotification: getNotification,
			postNotification: postNotification
	    }
	};

	notification.$inject = ['$http'];

	angular
		.module('straightAs')
		.service('headerService', notification);
})();
(function() {
	var restaurant = function($http) {

		var getRestaurant = function(idRestaurant) { 
			return $http.get('api/restaurants/' + idRestaurant);
		};

		var getRestaurants = function() {
			return $http.get('api/restaurants/');
		};

	    var postRestaurant = function(restaurant) {
	        return $http.post('api/restaurants/', restaurant);
	    };

	    var putRestaurant = function(restaurant) {
	        return $http.put('api/restaurants/' + restaurant._id, restaurant);
	    };

        var deleteRestaurant = function(restaurant) {
	        return $http.delete('api/restaurants/' + restaurant._id, restaurant);
	    };

	    var getMeal = function(idRestaurant, idMeal) { 
			return $http.get('api/restaurants/' + idRestaurant + '/meals/'+ idMeal);
		};

		var getMeals = function(idRestaurant) {
			return $http.get('api/restaurants/' + idRestaurant + '/meals/');
		};

	    var postMeal = function(idRestaurant, meal) {
	        return $http.post('api/restaurants/' + idRestaurant + '/meals/', meal);
	    };

	    var putMeal = function(idRestaurant, meal) {
	        return $http.put('api/restaurants/' + idRestaurant + '/meals/' + meal._id, meal);
	    };

        var deleteMeal = function(idRestaurant, meal) {
	        return $http.delete('api/restaurants/' + idRestaurant + '/meals/' + meal._id, meal);
	    };

		return {
			getRestaurant: getRestaurant,
			getRestaurants: getRestaurants,
			postRestaurant: postRestaurant,
			putRestaurant: putRestaurant,
			deleteRestaurant: deleteRestaurant,
			getMeal: getMeal,
			getMeals: getMeals,
			postMeal: postMeal,
			putMeal: putMeal,
			deleteMeal: deleteMeal
	    }
	};

	restaurant.$inject = ['$http'];

	angular
		.module('straightAs')
		.service('restaurantService', restaurant);
})();
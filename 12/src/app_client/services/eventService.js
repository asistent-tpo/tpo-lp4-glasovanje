(function() {
	var event = function($http) {

		var getEvent = function(idEvent) { 
			return $http.get('api/events/'+ idEvent);
		};

		var getEvents = function() {
			return $http.get('api/events/');
		};

	    var postEvent = function(event) {
	        return $http.post('api/events/', event);
	    };

		return {
			getEvent: getEvent,
			getEvents: getEvents,
			postEvent: postEvent,
	    }
	};

	event.$inject = ['$http'];

	angular
		.module('straightAs')
		.service('eventService', event);
})();
(function() {
	var bus = function($http) {

		var getLppStop = function(idLpp) { 
			return $http.get('api/lppStop/' + idLpp);
		};

		var getLppStops = function() {
			return $http.get('api/lppStop');
		};
		
		var searchLppStops = function(query) { 
			return $http.get('api/lppStop/search?q=' + query);
		};

	    var postLppStop = function(lppStop) {
	        return $http.post('api/lppStop/', lppStop);
	    };

	    var putLppStop = function(lppStop) {
	        return $http.put('api/lppStop/' + lppStop._id, lppStop);
	    };

        var deleteLppStop = function(lppStop) {
	        return $http.delete('api/lppStop/' + lppStop._id, lppStop);
	    };

	    var getLppStopArrivals = function(lppStop) {
	        return $http.get('api/lppStop/arrivals/' + lppStop);
	    };

		return {
			getLppStop: getLppStop,
			getLppStops: getLppStops,
			searchLppStops: searchLppStops,
			postLppStop: postLppStop,
			putLppStop: putLppStop,
			deleteLppStop: deleteLppStop,
			getLppStopArrivals: getLppStopArrivals,
	    }
	};

	bus.$inject = ['$http'];

	angular
		.module('straightAs')
		.service('busService', bus);
})();
(function() {
    function authentication($window, $http) {
        var b64Utf8 = function (string) {
            return decodeURIComponent(Array.prototype.map.call($window.atob(string), function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        };
        
        var saveToken = function(token) {
            $window.localStorage['straightAs-token'] = token;
        };
        
        var returnToken = function() {
            return $window.localStorage['straightAs-token'];
        };

        var confirmUser = function(temporaryToken) {
            return $http.post('/api/confirmUser', {temporaryToken: temporaryToken}); 
        }
        
        var registration = function(user) {
            return $http.post('/api/registration', user);
        };

        var login = function(user) {
            return $http.post('/api/login', user).then(
                function success(response) {
                    saveToken(response.data.token);
                });
        };

        var passwordChange = function(user) {
            return $http.post('/api/changePassword', user);
        }

        var checkout = function() {
            $window.localStorage.removeItem('straightAs-token');
        };

        var isLoggedIn = function() {
            var token = returnToken();
            if (token) {
                var usefulContent = JSON.parse(b64Utf8(token.split('.')[1]));
                return usefulContent.endDate > Date.now() / 1000;
            } else {
                return false;
            }
        };

        var hasRole = function(roles) {
            var token = returnToken();
            if (token) {
                var usefulContent = JSON.parse(b64Utf8(token.split('.')[1]));

                if (!(usefulContent.endDate > Date.now() / 1000))
                    return false

                return (roles.includes(usefulContent.role));
            } else {
                return false;
            }
        };

        var currentUser = function() {
            if (isLoggedIn()) {
                var token = returnToken();
                var usefulContent = JSON.parse(b64Utf8(token.split('.')[1]));
                return {
                    id: usefulContent._id,
                    email: usefulContent.email,
                    ime: usefulContent.ime
                };
            }
        };

        return {
            saveToken: saveToken,
            returnToken: returnToken,
            confirmUser: confirmUser,
            registration: registration,
            login: login,
            passwordChange: passwordChange,
            checkout: checkout,
            isLoggedIn: isLoggedIn,
            hasRole: hasRole,
            currentUser: currentUser
        };
    }

    authentication.$inject = ['$window', '$http'];

    angular
        .module('straightAs')
        .service('authenticationService', authentication);
})();
(function() {

	function calendarController($route, calendarService, authenticationService, $filter) {
		var cvm = this;

		var date = new Date();

		var idUser = authenticationService.isLoggedIn() ? authenticationService.currentUser().id : "";

		const monthNames = ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December" ];

		cvm.current_month = date.getMonth();
		cvm.current_year = date.getFullYear();

		cvm.month_name = monthNames[cvm.current_month] + " " + cvm.current_year;

		cvm.calendar = [];

		cvm.currentEvents = [];

		cvm.currentEvent = {};

		cvm.newEventShow = {};

		cvm.newEventShow.time = "";
		
		cvm.events = [];

		cvm.current_day = 0;

		cvm.tmp = {};

		cvm.updateCalendar = function(){
			calendarService.getCalendars(idUser).then(
				function success(response) {
                    cvm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    cvm.events = response.data;
                    cvm.calendar = cvm.getCalendarDays(cvm.current_month, cvm.current_year);
                }, function error(response) {
                    cvm.message = "Prišlo je do napake!";
                }
			);
		}

		cvm.getCalendarDays = function(month, year) {
			var first_day_in_month = new Date(year, month, 1);
			var first_day = first_day_in_month.getDay();

			var num_of_days = new Date(year, month+1, 0).getDate();

			var calendar = [];

			var cur_day = first_day;

			var cur_week = {};

			for (var i = 1; i <= num_of_days; i++) {
				if (cur_day == 7)
					cur_day = 0;
				var events = cvm.getEvents(i, month, year);
				cur_week[cur_day] = {
					day: i,
					events: events,
				};
				if (cur_day == 0) {
					calendar.push(cur_week);
					cur_week = {};
				}
				cur_day++;
			}

			if (Object.keys(cur_week).length != 0) calendar.push(cur_week); 

			return calendar;
		}

		cvm.getEvents = function(day, month, year) {
			var date = new Date(year, month, day);
			date.setHours(0,0,0,0);
			var events = [];

			for (var i = 0; i < cvm.events.length; i++) {
				var start_date = new Date(cvm.events[i].date);
				start_date.setHours(0,0,0,0);
				if (start_date.getTime() == date.getTime())
					events.push(cvm.events[i]);
			}
			return events;
		}

		cvm.changeMonth = function(add) {
			var cur_date = new Date(cvm.current_year, cvm.current_month, 1);
			var new_date = new Date(cur_date.setMonth(cur_date.getMonth() + add));

			cvm.current_month = new_date.getMonth();
			cvm.current_year = new_date.getFullYear();

			cvm.month_name = monthNames[cvm.current_month] + " " + cvm.current_year;

			cvm.calendar = cvm.getCalendarDays(cvm.current_month, cvm.current_year);
		}

		cvm.showDay = function(day){
			cvm.formError = "";
            cvm.nameError = "";
            cvm.durationError = "";
            cvm.timeError = "";
			cvm.currentEvents = cvm.getEvents(day, cvm.current_month, cvm.current_year);
			if (day != null) {
				cvm.current_day = day;
				cvm.newEventShow.date = new Date(cvm.current_year, cvm.current_month, cvm.current_day);

				$('#myModal').modal('show');
			}
		}

		cvm.editEvent = function(currentEvent){
			cvm.currentEvent = currentEvent;
			cvm.tmp.name = currentEvent.name;
			cvm.tmp.time = currentEvent.time;
			cvm.tmp.duration = currentEvent.duration;
			cvm.tmp.description = currentEvent.description;
			$('#myModalEdit').modal("show");
			$('#myModal').modal("hide");
		}

		cvm.closeEditEvenCanclet = function(){
			var obj = $filter('filter')(cvm.currentEvents, {_id: cvm.currentEvent._id}, true)[0];
            obj.name = cvm.tmp.name;
           	obj.time = cvm.tmp.time;
            obj.duration = cvm.tmp.duration;
            obj.description = cvm.tmp.description;
			Swal.fire({
				text: "Dogodek ne bo posodobljen!",
				type: 'info',
                showConfirmButton: false,
				timer: 1000
			});
			$('#myModalEdit').modal("hide");
			$('#myModal').modal("show");
		}

		cvm.saveEditEvent = function(){
			var error = false;
            cvm.formError = "";
            cvm.nameError = "";
            cvm.durationError = "";
            cvm.timeError ="";
            if(!cvm.currentEvent.name){
                cvm.nameError = "Zahtevano polje!";
                error = true;
            }
            if(!cvm.currentEvent.date){
                cvm.fieldError = "Zahtevano polje!";
                error = true;
            }
            if(cvm.currentEvent.time.length > 0){
            	if(((cvm.currentEvent.time.charAt(0) != ("0")) && (cvm.currentEvent.time.charAt(0) != ("1")) && (cvm.currentEvent.time.charAt(0) != ("2")))
            	  || ((cvm.currentEvent.time.charAt(1) != ("0")) && (cvm.currentEvent.time.charAt(1) != ("1")) && (cvm.currentEvent.time.charAt(1) != ("2")) && (cvm.currentEvent.time.charAt(1) != ("3"))
            	  	&& (cvm.currentEvent.time.charAt(1) != ("4")) && (cvm.currentEvent.time.charAt(1) != ("5")) && (cvm.currentEvent.time.charAt(1) != ("6")) && (cvm.currentEvent.time.charAt(1) != ("7"))
            	  	&& (cvm.currentEvent.time.charAt(1) != ("8")) && (cvm.currentEvent.time.charAt(1) != ("9")))
	        		|| (cvm.currentEvent.time.charAt(2) != (":")) 
	        		|| ((cvm.currentEvent.time.charAt(3) != ("0")) && (cvm.currentEvent.time.charAt(3) != ("1")) && (cvm.currentEvent.time.charAt(3) != ("2")) && (cvm.currentEvent.time.charAt(3) != ("3"))
            	  	&& (cvm.currentEvent.time.charAt(3) != ("4")) && (cvm.currentEvent.time.charAt(3) != ("5")))
            	  	|| ((cvm.currentEvent.time.charAt(4) != ("0")) && (cvm.currentEvent.time.charAt(4) != ("1")) && (cvm.currentEvent.time.charAt(4) != ("2")) && (cvm.currentEvent.time.charAt(4) != ("3"))
            	  	&& (cvm.currentEvent.time.charAt(4) != ("4")) && (cvm.currentEvent.time.charAt(4) != ("5")) && (cvm.currentEvent.time.charAt(4) != ("6")) && (cvm.currentEvent.time.charAt(4) != ("7"))
            	  	&& (cvm.currentEvent.time.charAt(4) != ("8")) && (cvm.currentEvent.time.charAt(4) != ("9")))){
	        		cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}  
        		if(cvm.currentEvent.time.length != 5){
        			cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}
        		if((cvm.currentEvent.time.charAt(0) == ("2")) && ((cvm.currentEvent.time.charAt(1) == ("5")) || (cvm.currentEvent.time.charAt(1) == ("6")) || (cvm.currentEvent.time.charAt(1) == ("7"))
            	  	|| (cvm.currentEvent.time.charAt(1) == ("8")) || (cvm.currentEvent.time.charAt(1) == ("9")))){
        			cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}
        		if((cvm.currentEvent.time.charAt(0) == ("2")) && (cvm.currentEvent.time.charAt(1) == ("4")) && (cvm.currentEvent.time.charAt(3) != ("0")) || (cvm.currentEvent.time.charAt(4) != ("0"))){
        			cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}
            }        	
			if(cvm.currentEvent.duration < 0){
                cvm.durationError = "Trajanje ne more biti negativno!";
                error = true;
            } if (error) {
               	cvm.formError = "Vsaj eno polje vsebuje napako!";
                return false;
            }	
            else{
            	cvm.edit();
            }		
		}

		cvm.edit = function(){
			calendarService.putCalendar(idUser, cvm.currentEvent).then(
				function success(response) {
					Swal.fire({
						text: "Uspešno posodobljen dogodek!",
						type: 'success',
                        showConfirmButton: false,
						timer: 1000
                    });
                    cvm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    cvm.updateCalendar();
                    $('#myModal').modal("show");
					$('#myModalEdit').modal("hide");
                }, function error(response) {
                	Swal.fire({
						text: "Prišlo je do napake!",
						type: 'error',
                        showConfirmButton: false,
						timer: 1000
                    });
                }
			);	
		}

		cvm.newEvent = function(){
			$('#myModalInsert').modal("show");
			$('#myModal').modal("hide");
		}

		cvm.closeNew = function(){
			cvm.newEventShow={};
            cvm.newEventShow.time ="";
			Swal.fire({
				text: "Dogodek ne bo ustvarjen!",
				type: 'info',
                showConfirmButton: false,
				timer: 1000
			});
			$('#myModal').modal("show");
			$('#myModalInsert').modal("hide");
		}

		cvm.saveNewEvent = function(){
			var error = false;
            cvm.formError = "";
            cvm.nameError = "";
            cvm.durationError = "";
            cvm.timeError = "";
            if(!cvm.newEventShow.name){
                cvm.nameError = "Zahtevano polje!";
                error = true;
            }
            if(!cvm.newEventShow.date){
                cvm.fieldError = "Zahtevano polje!";
                error = true;
            }
            if(cvm.newEventShow.time.length > 0){
            	if(((cvm.newEventShow.time.charAt(0) != ("0")) && (cvm.newEventShow.time.charAt(0) != ("1")) && (cvm.newEventShow.time.charAt(0) != ("2")))
            	  || ((cvm.newEventShow.time.charAt(1) != ("0")) && (cvm.newEventShow.time.charAt(1) != ("1")) && (cvm.newEventShow.time.charAt(1) != ("2")) && (cvm.newEventShow.time.charAt(1) != ("3"))
            	  	&& (cvm.newEventShow.time.charAt(1) != ("4")) && (cvm.newEventShow.time.charAt(1) != ("5")) && (cvm.newEventShow.time.charAt(1) != ("6")) && (cvm.newEventShow.time.charAt(1) != ("7"))
            	  	&& (cvm.newEventShow.time.charAt(1) != ("8")) && (cvm.newEventShow.time.charAt(1) != ("9")))
	        		|| (cvm.newEventShow.time.charAt(2) != (":")) 
	        		|| ((cvm.newEventShow.time.charAt(3) != ("0")) && (cvm.newEventShow.time.charAt(3) != ("1")) && (cvm.newEventShow.time.charAt(3) != ("2")) && (cvm.newEventShow.time.charAt(3) != ("3"))
            	  	&& (cvm.newEventShow.time.charAt(3) != ("4")) && (cvm.newEventShow.time.charAt(3) != ("5")))
            	  	|| ((cvm.newEventShow.time.charAt(4) != ("0")) && (cvm.newEventShow.time.charAt(4) != ("1")) && (cvm.newEventShow.time.charAt(4) != ("2")) && (cvm.newEventShow.time.charAt(4) != ("3"))
            	  	&& (cvm.newEventShow.time.charAt(4) != ("4")) && (cvm.newEventShow.time.charAt(4) != ("5")) && (cvm.newEventShow.time.charAt(4) != ("6")) && (cvm.newEventShow.time.charAt(4) != ("7"))
            	  	&& (cvm.newEventShow.time.charAt(4) != ("8")) && (cvm.newEventShow.time.charAt(4) != ("9")))){
	        		cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}  
        		if(cvm.newEventShow.time.length != 5){
        			cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}
        		if((cvm.newEventShow.time.charAt(0) == ("2")) && ((cvm.newEventShow.time.charAt(1) == ("5")) || (cvm.newEventShow.time.charAt(1) == ("6")) || (cvm.newEventShow.time.charAt(1) == ("7"))
            	  	|| (cvm.newEventShow.time.charAt(1) == ("8")) || (cvm.newEventShow.time.charAt(1) == ("9")))){
        			cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}
        		if((cvm.newEventShow.time.charAt(0) == ("2")) && (cvm.newEventShow.time.charAt(1) == ("4")) && (cvm.newEventShow.time.charAt(3) != ("0")) || (cvm.newEventShow.time.charAt(4) != ("0"))){
        			cvm.timeError = "Ura mora biti oblike npr. 08:24 in veljavna!";
	            	error = true;
        		}
            }        	             
			if(cvm.newEventShow.duration < 0){
                cvm.durationError = "Trajanje ne more biti negativno!";
                error = true;
            } 
            if (error) {
                cvm.formError = "Vsaj eno polje vsebuje napako!";
                return false;
            }	
            else{
            	cvm.addNew();
            }		
		}

		cvm.addNew = function(){ 
			calendarService.postCalendar(idUser, cvm.newEventShow).then(
				function success(response) {
                    Swal.fire({
						text: "Uspešno dodan dogodek!",
						type: 'success',
                        showConfirmButton: false,
						timer: 1000
                    });
                    cvm.updateCalendar();
                    cvm.newEventShow={};
                    cvm.newEventShow.time ="";
					$('#myModalInsert').modal("hide");
                }, function error(response) {
                    Swal.fire({
						text: "Prišlo je do napake!",
						type: 'error',
                        showConfirmButton: false,
						timer: 1000
                    }); 
                }
			);	
		}				

		cvm.deleteEvent = function(event){
			cvm.currentEvent = event;
			$('#myModalDelete').modal("show");
			$('#myModal').modal("hide");
		}

		cvm.closeDelete = function(){
			Swal.fire({
				text: "Dogodek ne bo izbrisan!",
				type: 'info',
                showConfirmButton: false,
				timer: 1000
			});
			$('#myModal').modal("show");
			$('#myModalDelete').modal("hide");
		}

		cvm.comfirmDelete = function(){
			var tmp_id = cvm.currentEvent._id;
			calendarService.deleteCalendar(idUser, cvm.currentEvent).then(
				function success(response) {
                    Swal.fire({
						text: "Uspešno izbrisan dogodek!",
						type: 'success',
						showConfirmButton: false,
						timer: 1000
                    });
                    cvm.updateCalendar();
					$('#myModalDelete').modal("hide");
					$('#myModal').modal("show");

					cvm.currentEvents = cvm.currentEvents.filter(function(event){
				       	return event._id != tmp_id;
				   	});
                }, function error(response) {
                    Swal.fire({
						text: "Prišlo je do napake!",
						type: 'error',
                        showConfirmButton: false,
						timer: 1000
                    });
                }
			);	
		}

		cvm.updateCalendar();
	}

	calendarController.$inject = ['$route', 'calendarService', 'authenticationService', '$filter'];

	angular
		.module('straightAs')
		.controller('calendarController', calendarController);
})();
(function() {

	function adminController($route, adminService) {
		var avm = this;
	
		avm.currentNote = {};

		avm.notifications = [];

		avm.notification = {};
		
		avm.newNoteAdd = function(){
			$('#myModal').modal("show");
		}

		avm.closeAdd = function(){
			Swal.fire({
                text: "Obvestilo ne bo objavljeno!",
                type: 'info',
                showConfirmButton: false,
                timer: 1000
			});
			$('#myModal').modal("hide");
		}

		avm.updateNotes = function(){
			adminService.getNotifications().then(
                function success(response) {
                    avm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    avm.events = response.data;
                }, function error(response) {
                    avm.message = "Prišlo je do napake!";
                }
            );
		}

		avm.newNote = function(){
			adminService.postNotification(avm.currentNote).then(
                function success(response) {
                	Swal.fire({
                        text: "Objava obvestila je bila uspešna!",
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    avm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    avm.message = "Sporočilo je bilo uspešno objavljeno";
                    avm.updateNotes();
                    avm.currentNote={};
                    $('#myModal').modal("hide");
                }, function error(response) {
                	Swal.fire({
                        text: "Polje je potrebno izpolniti!",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    avm.message = "Prišlo je do napake!";
                }
            );  
		}

		avm.showNotification = function(){
			$('#myModalNote').modal("show");
			adminService.getNotifications().then(
                function success(response) {
                    avm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    avm.events = response.data;
                }, function error(response) {
                    avm.message = "Prišlo je do napake!";
                }
            );
		}

		avm.closeShow = function(){
			$('#myModalNote').modal("hide");
		}

		avm.updateNotes();
	}

	adminController.$inject = ['$route', 'adminService'];

	angular
		.module('straightAs')
		.controller('adminController', adminController);

})();
(function() {
    function loginController(authenticationService, $location, $timeout) {
        var vm = this;

        vm.user = {
          email: "",
          pswd: ""
        };

        vm.idUser = authenticationService.isLoggedIn() ? authenticationService.currentUser().id : "";

        vm.userLogin = function() {
            var error = false;
            vm.formError = "";
            vm.emailError = "";
            vm.fieldError = "";

            if(!vm.user.email){
                vm.emailError = "Napačen vnos oz. vnos nepravilne oblike!";
                error = true;
            }

            if(!vm.user.pswd){
                vm.fieldError = "Napačno geslo!";
                error = true;
            }

            if (error) {
                vm.formError = "Vsaj eno polje vsebuje napako!";
                return false;
            } else {
                vm.executeLogin();
            }
        };

        vm.executeLogin = function() {
            authenticationService.login(vm.user).then(
                function(success) {
                    vm.idUser = authenticationService.isLoggedIn() ? authenticationService.currentUser().id : "";
                    Swal.fire({
                        text: "Prijava je bila uspešna!",
                        showConfirmButton: false,
                        type: 'success'
                    });
                    vm.messageBool = true;
                    $timeout(function() {
                        vm.messageBool = false;
                        if (authenticationService.hasRole(['user'])) {
                            $location.path('/');
                        } else if (authenticationService.hasRole(['admin'])) {
                            $location.path('/admin');
                        } else if (authenticationService.hasRole(['event_manager'])) {
                            $location.path('/eventmanager');
                        }
                    }, 1000);
                },
                function(error) {
                    vm.formError = "Vnešeni podatki niso pravilni! (Prepričajte se tudi, da ste potrdili registracijo preko emaila :) )";
                    Swal.fire({
                        text: vm.formError,
                        type: 'error'
                    });
                    $location.path('/login');
                }
            );
        };
    }

    loginController.$inject = ['authenticationService', '$location', '$timeout'];

    angular
        .module('straightAs')
        .controller('loginController', loginController)
        
})();
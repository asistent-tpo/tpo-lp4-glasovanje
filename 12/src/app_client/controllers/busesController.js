(function() {
    function busesController($route, busService) {
        var bvm = this;

        // var searchInput = "";

        bvm.arrivals = [];
        bvm.lppStops = [];

        bvm.searchLppStop = "";
        bvm.isLoading = false;

        bvm.emptyLppStops = function() {
            return bvm.lppStops.length == 0;
        }

        bvm.getStops = function() {
            busService.searchLppStops('').then(
                function success(response) {
                    bvm.lppStops = response.data;
                }, 
                function error(response) {
                    console.log(response.e);
                }
            );
        }

        bvm.searchResultClick = function(lppStop) {
            if (!lppStop || lppStop == "") return;

            bvm.searchLppStop = "";
            bvm.arrivals = [];
            bvm.isLoading = true;

            busService.getLppStopArrivals(lppStop).then(
                function success(response) {
                    bvm.isLoading = false;
                    bvm.arrivals = response.data;
                    // console.log(bvm.arrivals);
                },
                function error(response) {
                    console.log(response.e);
                }
            );
        }

        $('#inputField').keyup(function(event) {
            if (event.keyCode == 13) {
                $('#submitBtn').click();
            }
        });

        bvm.getStops();
    }

    busesController.$inject = ['$route', 'busService'];

	angular
    .module('straightAs')
    .controller('busesController', busesController);
})();
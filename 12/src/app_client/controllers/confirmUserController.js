(function() {
    function confirmUserController(authenticationService, $location, $timeout) {

        var cuvm = this;

        cuvm.$location = $location;
        cuvm.url = cuvm.$location.path();
         
        if (cuvm.url.indexOf('confirmUser') > -1) {    
            cuvm.tmpToken = cuvm.url.split('/')[2];

            authenticationService.confirmUser(cuvm.tmpToken).then(
                function(success) {
                    cuvm.message = "Uspešna registracija!";
                    Swal.fire({
                        text: cuvm.message,
                        showConfirmButton: false,
                        type: 'success'
                    });
                    cuvm.messageBool = true;
                    $timeout(function() {
                        cuvm.messageBool = false;
                        $location.path('/login');
                    }, 2000);
                },
                function(error) {
                    cuvm.formError = "Prišlo je do napake! Poskusite znova!";
                    Swal.fire({
                        text: cuvm.formError,
                        type: 'error'
                    });
                    $location.path('/registration');
                }
            );
        }
    }    
    confirmUserController.$inject = ['authenticationService', '$location', '$timeout'];

    angular
        .module('straightAs')
        .controller('confirmUserController', confirmUserController);
})();
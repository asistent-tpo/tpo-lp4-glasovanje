(function() {

	function restaurantsController($route, $scope, restaurantService) {
		var rvm = this;

		var myPosition = {lat: 46.0569, lng: 14.5058}; 

		var map;

		rvm.restaurants = [];
		rvm.currentRestaurant = {};

		rvm.hasLocationPermission = false;

		rvm.lastMarker = null;

		rvm.searchRestaurant = "";

		rvm.getRestaurants = function() {
			console.log("notr!!");
			restaurantService.getRestaurants().then(
				function success(response) {
					rvm.restaurants = response.data;
					if (rvm.hasLocationPermission) {
						rvm.restaurants.sort(compareDistance);
					}
					rvm.initMap();
				}, 
				function error(response) {

				}
			);
		}

		function compareDistance( a, b ) {
			var myPos = new google.maps.LatLng(myPosition.lat, myPosition.lng);
			var loc1 = new google.maps.LatLng(a.lat, a.lng);
			var loc2 = new google.maps.LatLng(b.lat, b.lng);

			var distA = google.maps.geometry.spherical.computeDistanceBetween(myPos, loc1);
			var distB = google.maps.geometry.spherical.computeDistanceBetween(myPos, loc2);
			if ( distA < distB ){
				return -1;
			}
			if ( distA > distB ){
				return 1;
			}
			return 0;
		}


		rvm.initMap = function() {
			console.log("notr!!");
			map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: myPosition});
			if (rvm.hasLocationPermission) {
				new google.maps.Marker({position: myPosition, map: map, title: 'Tukaj si ti!'});
			}

			rvm.restaurants.forEach(function(restaurant) {
				var marker = new google.maps.Marker({position: {lat: restaurant.lat, lng: restaurant.lng}, map: map, title: restaurant.name});
				marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
				marker.addListener('click', function() {
		          	rvm.showDetails(restaurant);
		        });
		        restaurant.marker = marker;
			});
		}

		rvm.showDetails = function(restaurant) {
			rvm.currentRestaurant.active = false;
			rvm.currentRestaurant = restaurant;
			map.panTo({lat: restaurant.lat, lng: restaurant.lng});

			if (rvm.lastMarker != null)
				rvm.lastMarker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
			restaurant.marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
          	rvm.lastMarker = restaurant.marker;

          	restaurant.active = true;

			$scope.$apply();
		}

		rvm.isObjectEmpty = function(obj){
			return Object.keys(obj).length === 0;
		}

		function handlePermission() {
			navigator.permissions.query({name:'geolocation'}).then(function(result) {
				if (result.state != 'granted') {
					rvm.getRestaurants();
				} 
				result.onchange = function() {
				  	report(result.state);
				}
			});
		}

		navigator.geolocation.getCurrentPosition(function(position) {
			rvm.hasLocationPermission = true;
			myPosition.lat = position.coords.latitude;
			myPosition.lng = position.coords.longitude;

			rvm.getRestaurants();
		});

		handlePermission();

	}

	restaurantsController.$inject = ['$route', '$scope', 'restaurantService'];

	angular
		.module('straightAs')
		.controller('restaurantsController', restaurantsController);

})();
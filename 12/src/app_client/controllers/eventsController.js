(function() {

	function eventsController($route, eventService) {
        var evm = this;
        
        evm.events = [];

        evm.event = {};
        
        evm.currentEvent = {};

        evm.showEvents = function(){
            eventService.getEvents().then(
                function success(response) {
                    evm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    evm.events = response.data;
                }, function error(response) {
                    evm.message = "Prišlo je do napake!";
                }
            );
        }

       evm.updateEvents = function(){
            eventService.getEvents().then(
                function success(response) {
                    evm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    evm.events = response.data;
                }, function error(response) {
                    evm.message = "Prišlo je do napake!";
                }
            );
        }

        evm.newEventAdd = function(){
            $('#myModal').modal("show");
        }

        evm.closeNew = function(){
            Swal.fire({
                text: "Dogodek ne bo ustvarjen!",
                type: 'info',
                showConfirmButton: false,
                timer: 1000,
            });
            $('#myModal').modal("hide");
        }

        evm.newEvent = function(){
            var error = false;
            evm.formError = "";
            evm.nameError = "";
            evm.organizerError = "";
            evm.dateError = "";
            if(!evm.currentEvent.name){
                evm.nameError = "Zahtevano polje!";
                error = true;
            }
            if(!evm.currentEvent.organizer){
                evm.organizerError = "Zahtevano polje!";
                error = true;
            }
            if(!evm.currentEvent.date){
                evm.dateError = "Zahtevano polje!";
                error = true;
            } if (error) {
                evm.formError = "Vsaj eno polje vsebuje napako!";
                return false;
            }   
            else{
                evm.new();
            }       
        }

        evm.new = function(){
            eventService.postEvent(evm.currentEvent).then(
                function success(response) {
                    Swal.fire({
                        text: "Uspešno dodan dogodek!",
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    evm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    evm.updateEvents();
                    evm.currentEvent={};
                    $('#myModal').modal("hide");
                }, function error(response) {
                    Swal.fire({
                        text: "Prišlo je do napake!",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            );  
        }
        
        evm.updateEvents();
	}

	eventsController.$inject = ['$route', 'eventService'];

	angular
		.module('straightAs')
		.controller('eventsController', eventsController);

})();
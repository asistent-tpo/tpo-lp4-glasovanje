(function() {
    function registrationController(authenticationService, $location, $timeout) {

        var vm = this;
        
        vm.user = {
            email: "",
            pswd: "",
            pswdC: ""
        };  

        vm.userRegistration = function() {
            var error = false;
            vm.formError = "";
            vm.fieldError = "";
            vm.emailError = "";

            if(!vm.user.email){
                vm.emailError = "Vnesite email oblike npr. ime@priimek.com";
                error = true;
            }

            if(!vm.user.pswd){
                vm.fieldError = "Geslo mora biti dolžine vsaj 8 in vsebovati samo črke in/ali številke!";
                error = true;
            }

            if(vm.user.pswd !== vm.user.pswdC){
                vm.fieldError = "Gesli se ne ujemata!";
                error = true;
            }

            if (error) {
                vm.formError = "Vsaj eno polje vsebuje napako!";
                return false;
            } else {
                vm.executeRegistration();
            }
        };

        vm.executeRegistration = function() {
            vm.user.role = 'user';
            authenticationService.registration(vm.user).then(
                function(success) {
                    vm.message = "Na vaš email smo poslali povezavo, s katero potrdite registracijo! (Za vsak slučaj preverite tudi med vsiljeno pošto :)";
                    Swal.fire({
                        text: vm.message,
                        showConfirmButton: false,
                        type: 'success'
                    });
                    vm.messageBool = true;
                    $timeout(function() {
                        vm.messageBool = false;
                        $location.path('/login');
                    }, 4000);
                },
                function(error) {
                    vm.formError = "Uporabniški email že obstaja v naši bazi! Vnesite drugega :)";
                    Swal.fire({
                        text: vm.formError,
                        type: 'error'
                    });
                    $location.path('/registration');
                }
            );
        };
    }    
    registrationController.$inject = ['authenticationService', '$location', '$timeout'];

    angular
        .module('straightAs')
        .controller('registrationController', registrationController);
})();
(function() {

	function todoController($route, todoService, authenticationService, $filter) {
		var tvm = this;

        tvm.idUser = authenticationService.isLoggedIn() ? authenticationService.currentUser().id : "";

		tvm.todo = {};

        tvm.tmp = {};

		tvm.currentTodo = {};
        
        tvm.todoList = [];

        tvm.currentContainer = null;

        tvm.inputError = "";

		tvm.showTodoList = function(){
            todoService.getTodoList(tvm.idUser).then(
                function success(response) {
                    tvm.todoList = response.data;
                }, function error(response) {
                    tvm.message = "Prišlo je do napake!";
                    Swal.fire({
                        text: tvm.message,
                        type: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            );
        }

       tvm.updateTodo = function(){
            tvm.currentTodo.insertion_date = new Date();
            todoService.putTodo(tvm.idUser, tvm.currentTodo).then(
                function success(response) {
                    tvm.message = "Uspešno posodobljen listek TODO.";
                    Swal.fire({
                        text: tvm.message,
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    tvm.closeUpdate();
                    tvm.showTodoList();
                    tvm.currentTodo = {};
                }, function error(response) {
                    tvm.message = "Prišlo je do napake!";
                    Swal.fire({
                        text: tvm.message,
                        type: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            );
        }       

        tvm.newTodo = function(){
            var error = false;
            tvm.inputError = "";
            tvm.formError = "";

            if(!tvm.todo.description){
                tvm.inputError = "Vnesite opis!";
                error = true;
            }

            if (error) {
                tvm.formError = "Vpis ni vnešen!";

                return false;
            } else {
                tvm.createTodo();
            }
        }

        tvm.createTodo = function() {
            tvm.todo.insertion_date = new Date();
            todoService.postTodo(tvm.idUser, tvm.todo).then(
                function success(response) {
                    tvm.message = "Uspešno ustvarjen listek TODO.";
                    Swal.fire({
                        text: tvm.message,
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    tvm.showTodoList();
                    tvm.todo = {};
                    tvm.todoList.push(response);
                    tvm.closeNew();
                }, function error(response) {
                    tvm.message = "Prišlo je do napake!";
                    Swal.fire({
                        text: tvm.message,
                        type: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            );  
        }

		tvm.deleteTodo = function() {
            todoService.deleteTodo(tvm.idUser, tvm.currentTodo).then(
                function success(response) {
                    tvm.message = "Uspešno izbrisan listek TODO.";
                    Swal.fire({
                        text: tvm.message,
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    tvm.closeDelete();
                }, function error(response) {
                    tvm.message = "Prišlo je do napake!";
                    Swal.fire({
                        text: tvm.message,
                        type: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            );  
            tvm.currentContainer.fadeOut(300, function() { $(this).remove(); });
		}
        tvm.showTodoList();

        tvm.newTodoAdd = function(){
            tvm.inputError = "";
            $('#newTodoModal').modal("show");
        }

        tvm.closeNew = function(){
            $('#newTodoModal').modal("hide");
        }

        tvm.openUpdateTodo = function(currentTodo){
            tvm.currentTodo = currentTodo;
            tvm.tmp.description = currentTodo.description;
            $('#updateTodoModal').modal("show");
        }

        tvm.closeUpdate = function(){
            var obj = $filter('filter')(tvm.todoList, {_id: tvm.currentTodo._id}, true)[0];
            obj.description = tvm.tmp.description;
            $('#updateTodoModal').modal("hide");
        }

        tvm.openDeleteTodo = function($event, currentTodo){
            tvm.currentTodo = currentTodo;
            tvm.currentContainer = $($event.currentTarget).closest('.todo-list');
            $('#deleteTodoModal').modal("show");
        }

        tvm.closeDelete = function(){
            $('#deleteTodoModal').modal("hide");
        }
	}

	todoController.$inject = ['$route', 'todoService', 'authenticationService', '$filter'];

	angular
		.module('straightAs')
		.controller('todoController', todoController);

})();
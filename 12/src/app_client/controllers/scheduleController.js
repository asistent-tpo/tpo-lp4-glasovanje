(function() {

	function scheduleController($route, scheduleService, authenticationService, $filter) {
		var svm = this;

		var idUser = authenticationService.isLoggedIn() ? authenticationService.currentUser().id : "";

		const dayNames = {'ponedeljek': 0, 'torek': 1, 'sreda': 2, 'cetrtek': 3, 'petek': 4};

		svm.schedule = [];

		svm.currentCourse = {};
		
		svm.newCourse = {};
		svm.newCourse.colour = "#ffceae"; // default color

		svm.tmp = {};

		svm.from = 7;
		svm.to = 21;

		svm.courses = [];

		function getKeyByValue(object, value) {
			for (var key in object) {
				if (object[key] === value) {
					return key;
				}
			}
		}

		svm.getCourses = function() {
			scheduleService.getScheduleList(idUser).then(
				function success(response) {
					console.log(response.data);
					svm.message = response.data.length > 0 ? "" : "Ne najdem predmetov.";
					svm.courses = response.data;
					svm.getSchedule();
				}, function error(response) {
					svm.message = "Prišlo je do napake!";
					console.log(response.e);
				}
			);
		}

		svm.getSchedule = function() {
			var schedule = [];

			for (var i = svm.from; i <= svm.to; i++) {
				var days = [];

				for (var j = 0; j < 5; j++) {
					days.push({
						label: getKeyByValue(dayNames, j),
						course: {}
					});
				}

				schedule.push({
					time: i,
					colour: '',
					days: days
				});
			}

			svm.courses.forEach(function(course) {
				for (var i = 0; i < course.duration; i++) {
					schedule[(course.time - svm.from)+i].days[dayNames[course.day]].course = course;
				}
			});

			svm.schedule = schedule;
		}

		svm.dayClick = function(day, time) {
			svm.nameError = "";
			svm.durationError = "";
			svm.timeError = "";
			svm.formError = "";
			if (day.course.name == null || day.course.name == "") {
				svm.newCourse.day = day.label;
				svm.newCourse.time = time;
				$('#newCourseModal').modal("show");
			} else {
				svm.currentCourse = day.course;
				svm.tmp = $.extend(true, {}, svm.currentCourse);
				$('#editCourseModal').modal("show");
			}

		}

		svm.addCourse = function() {
			var error = false;
			svm.nameError = "";
			svm.durationError = "";
			svm.timeError = "";
			svm.formError = "";

			if (!svm.newCourse.name) {
				svm.nameError = "Vnesite ime predmeta!";
				error = true;
			}

			if (!svm.newCourse.duration) {
				svm.durationError = "Vnesite trajanje predmeta (število)!";
				error = true;
			} else if (svm.newCourse.duration < 1) {
				svm.durationError = "Trajanje mora biti pozitivno!";
				error = true;
			} else if ((svm.newCourse.duration + svm.newCourse.time) > 22) {
				svm.durationError = "Ura in trajanje presegata maksimalno vrednost (22)!";
				error = true;
			} else if ((svm.newCourse.duration + svm.newCourse.time) < 8) {
				svm.durationError = "Ura in trajanje presegata minimalno vrednost (8)!";
				error = true;
			}

			if (!svm.newCourse.time) {
				svm.timeError = "Vnesite čas predmeta (7 - 21)!";
				error = true;
			}

			if (error) {
				svm.formError = "Nov predmet ni bil vnešen!";
				return false;
			} else {
				svm.addCourseFieldsChecked();
			}
		}

		svm.addCourseFieldsChecked = function() {
			svm.newCourse.insertion_date = new Date();
			scheduleService.postSchedule(idUser, svm.newCourse).then(
				function success(response) {
					svm.message = "Uspešno ustvarjen predmet.";
					$('#newCourseModal').modal("hide");
					svm.getCourses();
					svm.newCourse = {};
					svm.newCourse.colour = "#ffceae";
					Swal.fire({
                        text: svm.message,
						type: 'success',
						showConfirmButton: false,
                        timer: 1000
                    });
				}, function error(response) {
					svm.message = "Prišlo je do napake!";
					console.log(response.e);
					Swal.fire({
                        text: svm.message,
						type: 'error',
						showConfirmButton: false,
                        timer: 1000
                    });
				}
			);
		}


		svm.editCourse = function() {
			var error = false;
			svm.nameError = "";
			svm.durationError = "";
			svm.timeError = "";
			svm.formError = "";

			if (!svm.currentCourse.name) {
				svm.nameError = "Vnesite ime predmeta!";
				error = true;
			}

			if (!svm.currentCourse.duration) {
				svm.durationError = "Vnesite trajanje predmeta (število)!";
				error = true;
			} else if (svm.currentCourse.duration < 1) {
				svm.durationError = "Trajanje mora biti pozitivno!";
				error = true;
			} else if ((svm.currentCourse.duration + svm.currentCourse.time) > 22) {
				svm.durationError = "Ura in trajanje presegata maksimalno vrednost (22)!"
				error = true;
			} else if ((svm.currentCourse.duration + svm.currentCourse.time) < 8) {
				svm.durationError = "Ura in trajanje presegata minimalno vrednost (8)!"
				error = true;
			}

			if (!svm.currentCourse.time) {
				svm.timeError = "Vnesite čas predmeta (7 - 21)!";
				error = true;
			}

			if (error) {
				svm.formError = "Predmet ni bil spremenjen!";
				return false;
			} else {
				svm.editCourseFieldsChecked();
			}
		}

		svm.editCourseFieldsChecked = function() {
			svm.currentCourse.insertion_date = new Date();
			scheduleService.putSchedule(idUser, svm.currentCourse).then(
				function success(response) {
					svm.message = "Uspešno urejen predmet.";
					$('#editCourseModal').modal('hide');
					svm.getCourses();
					Swal.fire({
                        text: svm.message,
						type: 'success',
						showConfirmButton: false,
                        timer: 1000
                    });
				}, function error(response) {
					svm.message = "Prišlo je do napake!";
					console.log(response.e);
					Swal.fire({
                        text: svm.message,
						type: 'error',
						showConfirmButton: false,
                        timer: 1000
                    });
				}
			);
		}

		svm.deleteCourse = function() {
			$('#deleteCourseModal').modal("show");
			$('#editCourseModal').modal("hide");
		}

		svm.closeDelete = function(){
			$('#editCourseModal').modal("show");
			$('#deleteCourseModal').modal("hide");
		}

		svm.closeEdit = function(){
			var editedSchedule = $filter('filter')(svm.courses, {_id: svm.currentCourse._id}, true)[0];
			editedSchedule.name = svm.tmp.name;
			editedSchedule.duration = svm.tmp.duration;
			editedSchedule.colour = svm.tmp.colour;
			editedSchedule.time = svm.tmp.time;
			$('#editCourseModal').modal("hide");
		}

		svm.closeAdd = function(){
			svm.newCourse = {};
			svm.newCourse.colour = "#ffceae";
			$('#newCourseModal').modal("hide");
		}

		svm.confirmDelete = function() {
			scheduleService.deleteSchedule(idUser, svm.currentCourse).then(
				function success(response) {
					svm.message = "Uspešno izbrisan predmet.";
					$('#deleteCourseModal').modal("hide");
					svm.getCourses();
					Swal.fire({
                        text: svm.message,
						type: 'success',
						showConfirmButton: false,
                        timer: 1000
                    });
				}, function error(response) {
					svm.message = "Prišlo je do napake!";
					Swal.fire({
                        text: svm.message,
						type: 'error',
						showConfirmButton: false,
                        timer: 1000
                    });
				}
			);
		}
		
		svm.getCourses();
	}

	scheduleController.$inject = ['$route', 'scheduleService', 'authenticationService', '$filter'];

	angular
		.module('straightAs')
		.controller('scheduleController', scheduleController);

})();
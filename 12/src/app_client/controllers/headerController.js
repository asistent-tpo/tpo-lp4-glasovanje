(function() {
	function headerController($location, $route, authenticationService, adminService, $timeout) {
		var hvm = this;

		var idUser = authenticationService.isLoggedIn() ? authenticationService.currentUser().id : "";

        hvm.user = {
            userId: idUser,
            pswd: "",
            pswdC: "",
            pswdNew: ""
        };  

		hvm.currentNote = {};

		hvm.notification = {};
		hvm.notifications = [];

		hvm.currentLocation = $location.path();

		hvm.closeShow = function(){
			$('#myModalNote').modal("hide");
		}

		hvm.showNotification = function(){
			$('#myModalNote').modal("show");
		}

		hvm.updateNotes = function(){
			adminService.getNotifications().then(
                function success(response) {
                    hvm.message = response.data.length > 0 ? "" : "Ne najdem virov.";
                    hvm.notifications = response.data;
                }, function error(response) {
                    hvm.message = "Prišlo je do napake!";
                    console.log(response.e);
                }
            );
		}

        hvm.updateNotes();
        
        hvm.hasRole = function(roles) {
            return authenticationService.hasRole(roles);
        }

        hvm.redirectHome = function() {
            if (authenticationService.hasRole(['user'])) {
                $location.path('/');
            } else if (authenticationService.hasRole(['admin'])) {
                $location.path('/admin');
            } else if (authenticationService.hasRole(['event_manager'])) {
                $location.path('/eventmanager');
            } else {
                $location.path('/login');
            }
        }

		hvm.changePassword = function() {
            var error = false;
            hvm.formError = "";
            hvm.fieldError = "";
            hvm.oldPswdErr = "";

            if(!hvm.user.pswd){
                hvm.oldPswdErr = "Napačno geslo!";
                error = true;
            }

            if(!hvm.user.pswdC){
                hvm.fieldError = "Novo geslo mora biti dolžine vsaj 8!";
                error = true;
            }

            if(!hvm.user.pswdNew){
                hvm.fieldError = "Novo geslo mora biti dolžine vsaj 8!";
                error = true;
            }

            if(hvm.user.pswdC !== hvm.user.pswdNew){
                hvm.fieldError = "Gesli se ne ujemata!";
                error = true;
            }

            if (error) {
                hvm.formError = "Geslo ne ustreza zahtevam :( Vnesite novega.";
                Swal.fire({
                    text: hvm.formError,
                    type: 'error'
                });
                return false;
            } else {
                hvm.executePasswordChange();
            }
        };

        hvm.executePasswordChange = function() {
            authenticationService.passwordChange(hvm.user).then(
                function(success) {
                    hvm.message = "Uspešno ste spremenili geslo :)";
                    Swal.fire({
                        text: hvm.message,
                        showConfirmButton: false,
                        type: 'success',
                        timer: 1000
                    });
                    hvm.messageBool = true;
                    $timeout(function() {
                        hvm.messageBool = false;
                        hvm.changePswdClose();
                    }, 1000);
                },
                function(error) {
                    hvm.formError = "Vnesite pravilno trenutno geslo.";
                    Swal.fire({
                        text: hvm.formError,
                        type: 'error'
                    });
                }
            );
        };

        hvm.changePswd = function(){
            hvm.user = {
            userId: idUser,
            pswd: "",
            pswdC: "",
            pswdNew: ""
            };  
            $('#changePassword').modal("show");
        };

        hvm.changePswdClose = function(){
            $('#changePassword').modal("hide");
        };

		hvm.logout = function() {
			authenticationService.checkout();
			Swal.fire({
                text: "Odjava je bila uspešna!",
                showConfirmButton: false,
                type: 'success'
            });
            hvm.messageBool = true;
            $timeout(function() {
                hvm.messageBool = false;
                $location.path('/login');
            }, 1000);
		};
    }    

	headerController.$inject = ['$location', '$route', 'authenticationService', 'adminService', '$timeout'];

	angular
		.module('straightAs')
		.controller('headerController', headerController);
})();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LocalStrategy({
		usernameField: 'email',
		passwordField: 'pswd'
	}, 
	function(email, password, done) {
		User.findOne(
			{
				email: email,
				active: true
			},
			function(error, user) {
				if (error)
					return done(error);
				if (!user) {
					return done(null, false, {
				    	message: 'Email is incorrect'
				  	});
				}
				if (!user.validatePassword(password)) {
				  	return done(null, false, {
				    	message: 'Password is incorrect'
				  	});
				}
				return done(null, user);
			}
		);
	}
));
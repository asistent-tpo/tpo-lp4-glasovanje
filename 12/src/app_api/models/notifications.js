var mongoose = require('mongoose');

ObjectId = mongoose.Schema.ObjectId;

var notificationShema = new mongoose.Schema({
  	message: {type: String, required: true},
	insertion_date: Date,
});

mongoose.model('Notification', notificationShema, 'administrator');
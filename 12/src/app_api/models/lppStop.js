var mongoose = require('mongoose');

ObjectId = mongoose.Schema.ObjectId;

var lppStopSchema = new mongoose.Schema({
  	name: {type: String, required: true},
  	number: {type: Number},
});

mongoose.model('LppStop', lppStopSchema, 'lppStop');
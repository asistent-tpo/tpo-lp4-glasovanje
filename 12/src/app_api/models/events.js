var mongoose = require('mongoose');

ObjectId = mongoose.Schema.ObjectId;

var eventShema = new mongoose.Schema({
    name: {type: String, required: true},
    organizer: {type: String, required: true},
    description: String,
    date: {type: Date, required: true},
    insertion_date: Date,
});

mongoose.model('Event', eventShema, 'events');
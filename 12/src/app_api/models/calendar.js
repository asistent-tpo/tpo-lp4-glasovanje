var mongoose = require('mongoose');

var calendarShema = new mongoose.Schema({
  	name: {type: String, required: true},
  	time: String,
  	duration: Number,
  	description: String,
	date: {type: Date, required: true},
	insertion_date: Date,
});

mongoose.model('Calendar', calendarShema, 'calendar');
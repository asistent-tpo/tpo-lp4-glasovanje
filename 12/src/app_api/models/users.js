var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

ObjectId = mongoose.Schema.ObjectId;

var todoShema = new mongoose.Schema({
  	description: {type: String, required: true},
	insertion_date: Date,
});

var calendarShema = new mongoose.Schema({
  	name: {type: String, required: true},
  	time: String,
  	duration: Number,
  	description: String,
	date:  {type: Date, required: true},
	insertion_date: Date,
});

var timeTableShema = new mongoose.Schema({
  	name: {type: String, required: true},
  	duration: {type: Number, required: true},
  	day: {type: String, required: true},
  	colour: String,
  	time: {type: Number, required: true},
	insertion_date: Date,
});

var usersShema = new mongoose.Schema({
  	name: {type: String},
	surname: {type: String},
	email: {type: String, required: true, unique: true},
	pswd: {type: String, required: true},
	role: {type: String, required: true},
	to_do: [todoShema],
	calendar: [calendarShema],
	time_table: [timeTableShema],
	randomValue: String,
	active: {type: Boolean, required: true, default: false},
	temporaryToken: {type: String, required: true}
});

usersShema.methods.setPassword = function(password) {
	this.randomValue = crypto.randomBytes(16).toString('hex');
	this.pswd = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
};

usersShema.methods.validatePassword = function(password) {
	var newPassword = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
	return this.pswd == newPassword;
}

usersShema.methods.generateJwt = function() {
	var endDate = new Date();
	endDate.setDate(endDate.getDate() + 7);

	return jwt.sign({
		_id: this._id,
		email: this.email,
		endDate: parseInt(endDate.getTime() / 1000, 10),
		role: this.role
	}, process.env.JWT_PASSWORD);
}

mongoose.model('User', usersShema, 'user');
var mongoose = require('mongoose');

var timeTableShema = new mongoose.Schema({
  	name: {type: String, required: true},
  	duration: {type: Number, required: true},
  	day: {type: String, required: true},
  	colour: String,
  	time: {type: Number, required: true},
	insertion_date: Date,
});

mongoose.model('TimeTable', timeTableShema, 'time_table');
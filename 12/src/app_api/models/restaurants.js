var mongoose = require('mongoose');

ObjectId = mongoose.Schema.ObjectId;

var mealSchema  = new mongoose.Schema({
  	name: {type: String, required: true},
  	description: {type: String},
});

var restaurantsSchema = new mongoose.Schema({
  	name: {type: String, required: true},
  	address: {type: String},
  	lat: {type: Number},
  	lng: {type: Number},
  	price: {type: String},
  	city: {type: String},
	  link: {type: String},
	  meals: [mealSchema],
	  insertion_date: Date
});

mongoose.model('Restaurant', restaurantsSchema, 'restaurants');
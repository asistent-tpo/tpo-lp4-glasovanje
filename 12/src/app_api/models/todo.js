var mongoose = require('mongoose');

var todoShema = new mongoose.Schema({
  	description: {type: String, required: true},
	insertion_date: Date,
});

mongoose.model('Todo', todoShema, 'to_do');
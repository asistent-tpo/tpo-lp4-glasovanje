var mongoose = require('mongoose');
var User = mongoose.model('User');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

var apiParameters = {
	server: 'http://localhost:' + (process.env.PORT || 5000)
};

if (process.env.NODE_ENV === 'production') {
	apiParameters.server = 'https://straightas.herokuapp.com';
}

var options = {
  auth: {
    api_user: 'straightas123',
    api_key: 'straightas321'
  }
}

var client = nodemailer.createTransport(sgTransport(options));

module.exports.usersList = function(request, response) {
	User
		.find()
	    .exec(function(error, users) {
	   		if (error) {
           		jsonResponse(response, 500, error);
          	   	return;
           	}

           	jsonResponse(response, 200, users);
	    });
};

module.exports.usersCreate = function(request, response) {
	User.create({
			name: request.body.name,
			surname: request.body.surname,
			email: request.body.email,
			pswd: request.body.pswd,
			role: request.body.role,
			to_do: [],
			calendar: [],
			time_table: [],
			active: false,
			temporaryToken: jwt.sign({email: request.body.email}, process.env.JWT_PASSWORD, {expiresIn: '48h'})
		}, function(error, user) {
			if (error) {
				jsonResponse(response, 400, error);
			} else {
				user.setPassword(user.pswd);
				user.save(function(error) {
					if (error) {
						jsonResponse(response, 500, error);
					} else {
						jsonResponse(response, 200, user);
					}
				});
			}
		});
};

module.exports.usersRead = function(request, response) {
	if (request.params && request.params.idUser) {
		User
			.findById(request.params.idUser)
		    .exec(function(error, user) {
		   	    if (!user) {
	           	   	jsonResponse(response, 404, {
	              		"message": "User with specified id could not be found."
	           	   	});
	          	   	return;
	            } else if (error) {
	           	   	jsonResponse(response, 500, error);
	          	   	return;
	            }

		   	    jsonResponse(response, 200, user);
		   });
	} else {
	    jsonResponse(response, 400, { 
	      "message": "Missing parameter idUser!"
	    });
	}
};

module.exports.usersUpdate = function(request, response) {
	if (!request.params.idUser) {
		jsonResponse(response, 400, {
			"message": "User could not be found, parameter idUser is required"
		});
		return;
	}
	User
		.findById(request.params.idUser)
		.exec(
			function(error, user) {
				if (!user) {
					jsonResponse(response, 404, {
						"message": "User could not be found."
					});
					return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				user.name = (typeof request.body.name !== "undefined") ? request.body.name : user.name;
				user.surname = (typeof request.body.surname !== "undefined") ? request.body.surname : user.surname;
				user.email = (typeof request.body.email !== "undefined") ? request.body.email : user.email;
				user.pswd = (typeof request.body.pswd !== "undefined") ? request.body.pswd : user.pswd;
				user.role = (typeof request.body.role !== "undefined") ? request.body.role : user.role;

				user.save(function(error, user) {
					if (error) {
						jsonResponse(response, 400, error);
					} else {
						jsonResponse(response, 200, user);
					}
				});
			}
		);
};

module.exports.usersDelete = function(request, response) {

	var idUser = request.params.idUser;
	if (idUser) {
		User
			.findByIdAndRemove(idUser)
		    .exec(
			    function(error, user) {
			    	if (error) {
			       		jsonResponse(response, 404, error);
			        	return;
			      	}
			    	jsonResponse(response, 200, { "status": "success" });
			    }
		    );
	} else {
		jsonResponse(response, 400, {
			"message": "User could not be found, parameter idUser is required."
		});
	}
};
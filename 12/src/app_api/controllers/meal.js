var mongoose = require('mongoose');
var Restaurant = mongoose.model('Restaurant');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

module.exports.mealsList = function(request, response) {

	if (request.params && request.params.idRestaurant) {
		Restaurant
			.findById(request.params.idRestaurant)
			.select('meals')
			.exec(function(error, meal) {
				if (error) {
	           		jsonResponse(response, 500, error);
	          	   	return;
	           	}

	           	if (!meal) {
	           		jsonResponse(response, 400, {});
	           		return;
	           	}

	           	var meal = meal.meals;
	           	meal.sort(function(a,b){
					return new Date(b.date) - new Date(a.date);
				});
	           	
	           	jsonResponse(response, 200, meal);
			});
	} else {
		jsonResponse(response, 400, { 
	    	"message": "Missing parameter idRestaurant!"
	    });
	}
};

module.exports.mealsCreate = function(request, response) {
	var idRestaurant = request.params.idRestaurant;
	if (idRestaurant) {
		Restaurant
			.findById(idRestaurant)
			.select('meals')
			.exec(function(error, restaurant) {
				if (error) {
			    	jsonResponse(response, 400, error);
			  	} else {
			    	if (!restaurant) {
						jsonResponse(response, 404, {
						  "message": "restaurant could not be found."
						});
					} else {
						restaurant.meals.push({
							name: request.body.name,
							description: request.body.description,
						});
						restaurant.save(function(error, restaurant) {
							var addedMeal;
							if (error) {
								jsonResponse(response, 400, error);
							} else {
								addedMeal = restaurant.meals[restaurant.meals.length - 1];
								jsonResponse(response, 201, addedMeal);
							}
						});
					}
			  	}
			});
	} else {
		jsonResponse(response, 400, {
			"message": "restaurant could not be found, parameter idRestaurant is required."
		});
	}
};

module.exports.mealsRead = function(request, response) {
	if (request.params && request.params.idRestaurant && request.params.idMeal) {
		Restaurant
			.findById(request.params.idRestaurant)
			.select('meals')
			.exec(
				function(error, restaurant) {
					var meal;
					if (!restaurant) {
						jsonResponse(response, 404, {
						  	"message": "restaurant could not be found, parameter idRestaurant is required."
						});
						return;
					} else if (error) {
						jsonResponse(response, 500, error);
						return;
					}
					if (restaurant.meals && restaurant.meals.length > 0) {
						meal = restaurant.meals.id(request.params.idMeal);
						if (!meal) {
							jsonResponse(response, 404, {
								"message": "Meal could not be found, parameter idRestaurant is required."
							});
						} else {
						 	jsonResponse(response, 200, {
							    meal: meal
						  	});
						}
					} else {
						jsonResponse(response, 404, {
							"message": "Couldnt find any meal."
						});
					}
				}
			);
	} else {
		jsonResponse(response, 400, {
			"message": "Meal could not be found, parameters idRestaurant and idMeal are required."
		});
	}
};

module.exports.mealsUpdate = function(request, response) {
	if (!request.params.idRestaurant || !request.params.idMeal) {
	    jsonResponse(response, 400, {
	    	"message": "Meal could not be found, parameters idRestaurant and idMeal are required."
	    });

	    return;
	}
  	
  	Restaurant
	    .findById(request.params.idRestaurant)
	    .select('meals')
	    .exec(
	      	function(error, restaurant) {
		        if (!restaurant) {
		          	jsonResponse(response, 404, {
		            	"message": "Restaurant could not be found."
		         	});
		          	return;
		        } else if (error) {
		          	jsonResponse(response, 500, error);
		          	return;
		        }

		        if (restaurant.meals&& restaurant.meals.length > 0) {
		        	var currentmeal = restaurant.meals.id(request.params.idMeal);
					if (!currentmeal) {
						jsonResponse(response, 404, {
					  		"message": "Meal could not be found."
						});
					} else {
						currentmeal.name = (typeof request.body.name !== "undefined") ? request.body.name : currentmeal.name;
						currentmeal.description = (typeof request.body.description !== "undefined") ? request.body.description : currentmeal.description;

						restaurant.save(function(error, restaurant) {
							if (error) {
						    	jsonResponse(response, 400, error);
						  	} else {
						    	jsonResponse(response, 200, currentmeal);
						  	}
						});
					}
		        } else {
		        	jsonResponse(response, 404, {
		            	"message": "No meal to update."
		          	});
		        }
	      	}
	    );
};

module.exports.mealsDelete = function(request, response) {

	if (!request.params.idRestaurant || !request.params.idMeal) {
		jsonResponse(response, 400, {
		  "message": "Meal could not be found, parameters idRestaurant and idMeal are required."
		});

		return;
	}

	Restaurant
		.findById(request.params.idRestaurant)
		.exec(
			function(error, restaurant) {
				if (!restaurant) {
					jsonResponse(response, 404, {
				    	"message": "restaurant could not be found."
				  	});
				  	return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				if (restaurant.meals&& restaurant.meals.length > 0) {
					if (!restaurant.meals.id(request.params.idMeal)) {
						jsonResponse(response, 404, {
							"message": "Meal could not be found."
						});
					} else {
						restaurant.meals.id(request.params.idMeal).remove();
						restaurant.save(function(error) {
							if (error) {
								jsonResponse(response, 500, error);
							} else {
								jsonResponse(response, 200, { "status": "success" });
							}
						});
					}
				} else {
					jsonResponse(response, 404, {
				    	"message": "No meal to delete."
				  	});
				}
			}
		);
};
var mongoose = require('mongoose');
var Restaurant = mongoose.model('Restaurant');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

module.exports.restaurantsList = function(request, response) {
	Restaurant
		.find()
	    .exec(function(error, restauarnts) {
	   		if (error) {
           		jsonResponse(response, 500, error);
          	   	return;
           	}

           	jsonResponse(response, 200, restauarnts);
	    });
};

module.exports.restaurantsCreate = function(request, response) {
	Restaurant.create({
			name: request.body.name,
			address: request.body.address,
			lat: request.body.lat,
			lng: request.body.lng,
			price: request.body.price,
			city: request.body.city,
			link: request.body.link,
			//meals: request.body.meals,
			insertion_date: request.body.insertion_date
		}, function(error, restauarnts) {
		if (error) {
			jsonResponse(response, 400, error);
		} else {
			jsonResponse(response, 200, restauarnts);
		}
	});
};

module.exports.restaurantsRead = function(request, response) {
	if (request.params && request.params.idRestaurant) {
		Restaurant
			.findById(request.params.idRestaurant)
		    .exec(function(error, restaurant) {
		   	    if (!restaurant) {
	           	   	jsonResponse(response, 404, {
	              		"message": "Restaurant with specified id could not be found."
	           	   	});
	          	   	return;
	            } else if (error) {
	           	   	jsonResponse(response, 500, error);
	          	   	return;
	            }

		   	    jsonResponse(response, 200, restaurant);
		   });
	} else {
	    jsonResponse(response, 400, { 
	      "message": "Missing parameter idRestaurant!"
	    });
	}
};

module.exports.restaurantsUpdate = function(request, response) {
	if (!request.params.idRestaurant) {
		jsonResponse(response, 400, {
			"message": "Restaurant could not be found, parameter idRestaurant is required"
		});
		return;
	}
	Restaurant
		.findById(request.params.idRestaurant)
		.exec(
			function(error, restaurant) {
				if (!restaurant) {
					jsonResponse(response, 404, {
						"message": "Restaurant could not be found."
					});
					return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				restaurant.name = (typeof request.body.name !== "undefined") ? request.body.name : restaurant.name;
				restaurant.address = (typeof request.body.address !== "undefined") ? request.body.address : restaurant.address;
				restaurant.lat = (typeof request.body.lat !== "undefined") ? request.body.lat : restaurant.lat;
				restaurant.lng = (typeof request.body.lng !== "undefined") ? request.body.lng : restaurant.lng;
				restaurant.price = (typeof request.body.price !== "undefined") ? request.body.price : restaurant.price;
				restaurant.city = (typeof request.body.city !== "undefined") ? request.body.city : restaurant.city;
				restaurant.link = (typeof request.body.link !== "undefined") ? request.body.link : restaurant.link;
				restaurant.meals = (typeof request.body.meals !== "undefined") ? request.body.meals : restaurant.meals;
				restaurant.insertion_date = (typeof request.body.insertion_date !== "undefined") ? request.body.insertion_date : restaurant.insertion_date;

				restaurant.save(function(error, restaurant) {
					if (error) {
						jsonResponse(response, 400, error);
					} else {
						jsonResponse(response, 200, restaurant);
					}
				});
			}
		);
};

module.exports.restaurantsDelete = function(request, response) {

	var idRestaurant = request.params.idRestaurant;
	if (idRestaurant) {
		Restaurant
			.findByIdAndRemove(idRestaurant)
		    .exec(
			    function(error, restaurant) {
			    	if (error) {
			       		jsonResponse(response, 404, error);
			        	return;
			      	}
			    	jsonResponse(response, 200, { "status": "success" });
			    }
		    );
	} else {
		jsonResponse(response, 400, {
			"messgae": "Restaurant could not be found, parameter idRestaurant is required."
		});
	}
};

module.exports.restaurantsDeleteAll = function(request, response) {
	Restaurant.deleteMany({}, function(err) { 
    	jsonResponse(response, 200, { 
	    	"message": "Data deleted!"
	    });
	});
};
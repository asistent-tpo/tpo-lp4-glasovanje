var mongoose = require('mongoose');
var User = mongoose.model('User');
var Calendar = mongoose.model('Calendar');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

module.exports.calendarList = function(request, response) {

	if (request.params && request.params.idUser) {
		User
			.findById(request.params.idUser)
			.select('calendar')
			.exec(function(error, calendar) {
				if (error) {
	           		jsonResponse(response, 500, error);
	          	   	return;
	           	}

	           	if (!calendar) {
	           		jsonResponse(response, 400, {});
	           		return;
	           	}

	           	var calendar = calendar.calendar;
	           	calendar.sort(function(a,b){
					return new Date(b.date) - new Date(a.date);
				});
	           	
	           	jsonResponse(response, 200, calendar);
			});
	} else {
		jsonResponse(response, 400, { 
	    	"message": "Missing parameter idUser!"
	    });
	}
};

module.exports.calendarCreate = function(request, response) {
	var idUser = request.params.idUser;
	if (idUser) {
		User
			.findById(idUser)
			.select('calendar')
			.exec(function(error, user) {
				if (error) {
			    	jsonResponse(response, 400, error);
			  	} else {
			    	if (!user) {
						jsonResponse(response, 404, {
						  "message": "User could not be found."
						});
					} else {
						user.calendar.push({
							name: request.body.name,
							time: request.body.time,
							duration: request.body.duration,
							description: request.body.description,
							date: request.body.date,
							insertion_date: request.body.insertion_date
						});
						user.save(function(error, user) {
							var addedCalendar;
							if (error) {
								jsonResponse(response, 400, error);
							} else {
								addedCalendar = user.calendar[user.calendar.length - 1];
								jsonResponse(response, 201, addedCalendar);
							}
						});
					}
			  	}
			});
	} else {
		jsonResponse(response, 400, {
			"message": "User could not be found, parameter idUser is required."
		});
	}
};

module.exports.calendarRead = function(request, response) {
	if (request.params && request.params.idUser && request.params.idCalendar) {
		User
			.findById(request.params.idUser)
			.select('calendar')
			.exec(
				function(error, user) {
					var calendar;
					if (!user) {
						jsonResponse(response, 404, {
						  	"message": "User could not be found, parameter idUser is required."
						});
						return;
					} else if (error) {
						jsonResponse(response, 500, error);
						return;
					}
					if (user.calendar && user.calendar.length > 0) {
						calendar = user.calendar.id(request.params.idCalendar);
						if (!calendar) {
							jsonResponse(response, 404, {
								"message": "Calendar could not be found, parameter idUser is required."
							});
						} else {
						 	jsonResponse(response, 200, {
							    calendar: calendar
						  	});
						}
					} else {
						jsonResponse(response, 404, {
							"message": "Couldnt find any calendar."
						});
					}
				}
			);
	} else {
		jsonResponse(response, 400, {
			"message": "Calendar could not be found, parameters idUser and idCalendar are required."
		});
	}
};

module.exports.calendarUpdate = function(request, response) {
	if (!request.params.idUser || !request.params.idCalendar) {
	    jsonResponse(response, 400, {
	    	"message": "Calendar could not be found, parameters idUser and idCalendar are required."
	    });

	    return;
	}
  	
  	User
	    .findById(request.params.idUser)
	    .select('calendar')
	    .exec(
	      	function(error, user) {
		        if (!user) {
		          	jsonResponse(response, 404, {
		            	"message": "User could not be found."
		         	});
		          	return;
		        } else if (error) {
		          	jsonResponse(response, 500, error);
		          	return;
		        }

		        if (user.calendar && user.calendar.length > 0) {
		        	var currentCalendar = user.calendar.id(request.params.idCalendar);
					if (!currentCalendar) {
						jsonResponse(response, 404, {
					  		"message": "Calendar could not be found."
						});
					} else {
						currentCalendar.name = (typeof request.body.name !== "undefined") ? request.body.name : currentCalendar.name;
						currentCalendar.time = (typeof request.body.time !== "undefined") ? request.body.time : currentCalendar.time;
						currentCalendar.duration = (typeof request.body.duration !== "undefined") ? request.body.duration : currentCalendar.duration;
						currentCalendar.description = (typeof request.body.description !== "undefined") ? request.body.description : currentCalendar.description;
						currentCalendar.date = (typeof request.body.date !== "undefined") ? request.body.date : currentCalendar.date;
						currentCalendar.insertion_date = (typeof request.body.insertion_date !== "undefined") ? request.body.insertion_date : currentCalendar.insertion_date;

						user.save(function(error, user) {
							if (error) {
						    	jsonResponse(response, 400, error);
						  	} else {
						    	jsonResponse(response, 200, currentCalendar);
						  	}
						});
					}
		        } else {
		        	jsonResponse(response, 404, {
		            	"message": "No calendar to update."
		          	});
		        }
	      	}
	    );
};

module.exports.calendarDelete = function(request, response) {

	if (!request.params.idUser || !request.params.idCalendar) {
		jsonResponse(response, 400, {
		  "message": "Calendar could not be found, parameters idUser and idCalendar are required."
		});

		return;
	}

	User
		.findById(request.params.idUser)
		.exec(
			function(error, user) {
				if (!user) {
					jsonResponse(response, 404, {
				    	"message": "User could not be found."
				  	});
				  	return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				if (user.calendar && user.calendar.length > 0) {
					if (!user.calendar.id(request.params.idCalendar)) {
						jsonResponse(response, 404, {
							"message": "Calendar could not be found."
						});
					} else {
						user.calendar.id(request.params.idCalendar).remove();
						user.save(function(error) {
							if (error) {
								jsonResponse(response, 500, error);
							} else {
								jsonResponse(response, 200, { "status": "success" });
							}
						});
					}
				} else {
					jsonResponse(response, 404, {
				    	"message": "No calendar to delete."
				  	});
				}
			}
		);
};
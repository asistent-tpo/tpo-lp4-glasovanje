var mongoose = require('mongoose');
var User = mongoose.model('User');
var Todo = mongoose.model('Todo');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

module.exports.todoList = function(request, response) {
	if (request.params && request.params.idUser) {
		User
			.findById(request.params.idUser)
			.select('to_do')
			.exec(function(error, todo) {
				if (error) {
	           		jsonResponse(response, 500, error);
	          	   	return;
	           	}

	           	if (!todo) {
	           		jsonResponse(response, 400, {});
	           		return;
	           	}

	           var todo = todo.to_do;
	           	
	           	jsonResponse(response, 200, todo);
			});
	} else {
		jsonResponse(response, 400, { 
	    	"message": "Missing parameter idUser!"
	    });
	}
};

module.exports.todoCreate = function(request, response) {
	var idUser = request.params.idUser;
	if (idUser) {
		User
			.findById(idUser)
			.select('to_do')
			.exec(function(error, user) {
				if (error) {
			    	jsonResponse(response, 400, error);
			  	} else {
			    	if (!user) {
						jsonResponse(response, 404, {
						  "message": "User could not be found."
						});
					} else {
						user.to_do.push({
							description: request.body.description,
							insertion_date: request.body.insertion_date
						});
						user.save(function(error, user) {
							var added_todo;
							if (error) {
								jsonResponse(response, 400, error);
							} else {
								added_todo = user.to_do[user.to_do.length - 1];
								jsonResponse(response, 201, added_todo);
							}
						});
					}
			  	}
			});
	} else {
		jsonResponse(response, 400, {
			"message": "User could not be found, parameter idUser is required."
		});
	}
};

module.exports.todoRead = function(request, response) {
	if (request.params && request.params.idUser && request.params.idTodo) {
		User
			.findById(request.params.idUser)
			.select('to_do')
			.exec(
				function(error, user) {
					var todo;
					if (!user) {
						jsonResponse(response, 404, {
						  	"message": "User could not be found, parameter idUser is required."
						});
						return;
					} else if (error) {
						jsonResponse(response, 500, error);
						return;
					}
					if (user.to_do && user.to_do.length > 0) {
						todo = user.to_do.id(request.params.idTodo);
						if (!todo) {
							jsonResponse(response, 404, {
								"message": "Todo could not be found, parameter idTodo is required."
							});
						} else {
						 	jsonResponse(response, 200, {
							    todo: todo
						  	});
						}
					} else {
						jsonResponse(response, 404, {
							"message": "Couldnt find any todo."
						});
					}
				}
			);
	} else {
		jsonResponse(response, 400, {
			"message": "Todo could not be found, parameters idUser and idTodo are required."
		});
	}
};

module.exports.todoUpdate = function(request, response) {
	if (!request.params.idUser || !request.params.idTodo) {
	    jsonResponse(response, 400, {
	    	"message": "Todo could not be found, parameters idUser and idTodo are required."
	    });

	    return;
	}
  	
  	User
	    .findById(request.params.idUser)
	    .select('to_do')
	    .exec(
	      	function(error, user) {
		        if (!user) {
		          	jsonResponse(response, 404, {
		            	"message": "User could not be found."
		         	});
		          	return;
		        } else if (error) {
		          	jsonResponse(response, 500, error);
		          	return;
		        }

		        if (user.to_do && user.to_do.length > 0) {
		        	var current_todo = user.to_do.id(request.params.idTodo);
					if (!current_todo) {
						jsonResponse(response, 404, {
					  		"message": "todo could not be found."
						});
					} else {
						current_todo.description = (typeof request.body.description !== "undefined") ? request.body.description : current_todo.description;
						current_todo.insertion_date = (typeof request.body.insertion_date !== "undefined") ? request.body.insertion_date : current_todo.insertion_date;

						user.save(function(error, user) {
							if (error) {
						    	jsonResponse(response, 400, error);
						  	} else {
						    	jsonResponse(response, 200, current_todo);
						  	}
						});
					}
		        } else {
		        	jsonResponse(response, 404, {
		            	"message": "No todo to update."
		          	});
		        }
	      	}
	    );
};

module.exports.todoDelete = function(request, response) {

	if (!request.params.idUser || !request.params.idTodo) {
		jsonResponse(response, 400, {
		  "message": "Todo could not be found, parameters idUser and idTodo are required."
		});

		return;
	}

	User
		.findById(request.params.idUser)
		.exec(
			function(error, user) {
				if (!user) {
					jsonResponse(response, 404, {
				    	"message": "User could not be found."
				  	});
				  	return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				if (user.to_do && user.to_do.length > 0) {
					if (!user.to_do.id(request.params.idTodo)) {
						jsonResponse(response, 404, {
							"message": "Todo could not be found."
						});
					} else {
						user.to_do.id(request.params.idTodo).remove();
						user.save(function(error) {
							if (error) {
								jsonResponse(response, 500, error);
							} else {
								jsonResponse(response, 200, { "status": "success" });
							}
						});
					}
				} else {
					jsonResponse(response, 404, {
				    	"message": "No todo to delete."
				  	});
				}
			}
		);
};
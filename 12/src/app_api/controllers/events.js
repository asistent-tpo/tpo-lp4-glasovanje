var mongoose = require('mongoose');
var Event = mongoose.model('Event');

var jsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
};

module.exports.eventsList = function(request, response) {
    Event
        .find()
        .exec(function(error, events) {
            if (error) {
                jsonResponse(response, 500, error);
                return;
            }

            jsonResponse(response, 200, events);
        });
};

module.exports.eventsCreate = function(request, response) {
    Event.create({
        name: request.body.name,
        organizer: request.body.organizer,
        description: request.body.description,
        date: request.body.date,
        insertion_date: request.body.insertion_date
    }, function(error, events) {
        if (error) {
            jsonResponse(response, 400, error);
        } else {
            jsonResponse(response, 200, events);
        }
    });
};

module.exports.eventsRead = function(request, response) {
    if (request.params && request.params.idEvent) {
        Event
            .findById(request.params.idEvent)
            .exec(function(error, event) {
                if (!event) {
                    jsonResponse(response, 404, {
                        "message": "Event with specified id could not be found!"
                    });
                    return;
                } else if (error) {
                    jsonResponse(response, 500, error);
                    return;
                }

                jsonResponse(response, 200, event);
            });
    } else {
        jsonResponse(response, 400, {
            "message": "Missing parameter idEvent!"
        });
    }
};
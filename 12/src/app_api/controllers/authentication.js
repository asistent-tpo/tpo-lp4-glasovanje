var passport = require('passport');
var mongoose = require('mongoose');
var crypto = require('crypto');
var User = mongoose.model('User');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

var apiParameters = {
	server: 'http://localhost:' + (process.env.PORT || 5000)
};

if (process.env.NODE_ENV === 'production') {
	apiParameters.server = 'https://straightas.herokuapp.com';
}

var options = {
  auth: {
    api_user: 'straightas123',
    api_key: 'straightas321'
  }
}

var client = nodemailer.createTransport(sgTransport(options));

function validateEmailAccessibility(email){

	return User.findOne({email: email}).then(function(result){
	    return result !== null;
	});
}

function checkUser(user) {
	var answer;
	var url = apiParameters.server + '/confirmUser/'+ user.temporaryToken;

	var email = {
		from: 'Administrator StraightAs, zadravecmiha987@gmail.com',
		to: user.email,
		subject: '[registracija] StraightAs - potrditveni email',
		text: 'Najkasneje v roku dveh dni od prejema tega sporočila kliknite na naslednjo povezavo, da potrdite registracijo: ' + url,
	};

	return validateEmailAccessibility(user.email).then(function(valid) {
		if (valid) {
			answer = false;

			return answer;
		} else {
			answer = true;
			client.sendMail(email, function(error, info){
			    if (error){
			      console.log(error);
			    }
			    else {
			      console.log('Message sent: ' + info.response);
			    }
			});

			return answer;
		}
	});
};

module.exports.confirmUser = function(request, response) {
	User
		.findOne({temporaryToken: request.body.temporaryToken})
		.exec(function(error, user) {
			if (error) {
           		jsonResponse(response, 500, error);
          	   	return;
           	}
           	if (!user) {
           		jsonResponse(response, 404, {
					"message": "User could not be found."
				});
          	   	return;
           	}
           	user.active = true;
           	user.save(function(error, user) {
				if (error) {
					jsonResponse(response, 400, error);
				} else {
					jsonResponse(response, 200, user);
				}
			});
		});
};

module.exports.registration = function(request, response) {
	if (!request.body.email || !request.body.pswd || !request.body.pswdC) {
		jsonResponse(response, 400, {
			"message": "All fields are required!"
		});
	} else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(request.body.email))) {
	    jsonResponse(response, 400, {
	      "message": "Email is not correct!"
	    });

	    return;
	}

	var user = new User();
	user.name = '';
	user.surname = '';
	user.email = request.body.email;
	user.setPassword(request.body.pswd);
	user.role = 'user';
	user.active = false;
	user.temporaryToken = jwt.sign({email: request.body.email}, process.env.JWT_PASSWORD, {expiresIn: '48h'});

	checkUser(user).then(function(valid) {
		if (valid) {
			user.save(function(error) {
				if (error) {
					jsonResponse(response, 500, error);
				} else {
				 	jsonResponse(response, 200, {
						"token": user.generateJwt()
				 	});
				}
			});
		} else {
			jsonResponse(response, 400);
		}
	});
};

module.exports.login = function(request, response) {
	if (!request.body.email || !request.body.pswd) {
    	jsonResponse(response, 400, {
     		"message": "All fields are required!"
    	});
  	} else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(request.body.email))) {
	    jsonResponse(response, 400, {
	      "message": "Email is not correct!"
	    });

	    return;
	}
  
	passport.authenticate('local', function(error, user, data) {
		if (error) {
			jsonResponse(response, 404, error);
		  	return;
		}
		if (user) {
		  	jsonResponse(response, 200, {
		    	"token": user.generateJwt()
		  	});
		} else {
		  	jsonResponse(response, 401, data);
		}
	})(request, response);
};

function setPassword(randomValue, password) {
	var pass = crypto.pbkdf2Sync(password, randomValue, 1000, 64, 'sha512').toString('hex');

	return pass;
};

module.exports.changePassword = function(request, response) {
	if (!request.body.pswd || !request.body.pswdC || !request.body.pswdNew) {
    	jsonResponse(response, 400, {
     		"message": "All fields are required!"
    	});
  	} 

  	var user = 
  		User
  			.findById(request.body.userId)
			.exec(function(error, user) {
		   	    if (!user) {
		       	   	jsonResponse(response, 404, {
		          		"message": "User with specified id could not be found."
		       	   	});
		      	   	return;
		        } else if (error) {
		       	   	jsonResponse(response, 500, error);
		      	   	return;
		        }

		        var oldP = setPassword(user.randomValue, request.body.pswd);

		 		if (user.pswd == oldP) {
		 			user.setPassword(request.body.pswdNew);

				  	user.save(function(error) {
						if (error) {
							jsonResponse(response, 500, error);
						} else {
						 	jsonResponse(response, 200);
						}
					});
			   	    jsonResponse(response, 200, user);
		 		} else {
		 			jsonResponse(response, 400, error);
		 		}  	
	   		});
};
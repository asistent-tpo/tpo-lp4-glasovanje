var mongoose = require('mongoose');
var LppStop = mongoose.model('LppStop');
var HTMLParser = require('node-html-parser');
var request = require('request');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

module.exports.lppStopList = function(request, response) {
	LppStop
		.find()
	    .exec(function(error, lppStops) {
	   		if (error) {
           		jsonResponse(response, 500, error);
          	   	return;
           	}

           	jsonResponse(response, 200, lppStops);
	    });
};

module.exports.lppStopSearch = function(request, response) {
	var q = (typeof request.query.q !== "undefined") ? request.query.q : '';

	LppStop
		.find({name: new RegExp(".*" + q + ".*", "i")})
		.distinct('name', function(error, names) {
			if (error) {
           		jsonResponse(response, 500, error);
          	   	return;
           	}
			
			jsonResponse(response, 200, names);
		});
};

module.exports.lppStopCreate = function(request, response) {
	LppStop.create({
			name: request.body.name,
			number: request.body.number,
		}, function(error, lppStops) {
		if (error) {
			jsonResponse(response, 400, error);
		} else {
			jsonResponse(response, 200, lppStops);
		}
	});
};

module.exports.lppStopRead = function(request, response) {
	if (request.params && request.params.idLppStop) {
		LppStop
			.findById(request.params.idLppStop)
		    .exec(function(error, lppStop) {
		   	    if (!lppStop) {
	           	   	jsonResponse(response, 404, {
	              		"message": "Lpp stop with specified id could not be found."
	           	   	});
	          	   	return;
	            } else if (error) {
	           	   	jsonResponse(response, 500, error);
	          	   	return;
	            }

		   	    jsonResponse(response, 200, lppStop);
		   });
	} else {
	    jsonResponse(response, 400, { 
	      "message": "Missing parameter idLppStop!"
	    });
	}
};

module.exports.lppStopUpdate = function(request, response) {
	if (!request.params.idLppStop) {
		jsonResponse(response, 400, {
			"message": "Lpp stop could not be found, parameter idLppStop is required"
		});
		return;
	}
	LppStop
		.findById(request.params.idLppStop)
		.exec(
			function(error, lppStop) {
				if (!lppStop) {
					jsonResponse(response, 404, {
						"message": "Lpp stop could not be found."
					});
					return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				lppStop.name = (typeof request.body.name !== "undefined") ? request.body.name : lppStop.name;
				lppStop.number = (typeof request.body.number !== "undefined") ? request.body.number : lppStop.number;

				lppStop.save(function(error, lppStop) {
					if (error) {
						jsonResponse(response, 400, error);
					} else {
						jsonResponse(response, 200, lppStop);
					}
				});
			}
		);
};

module.exports.lppStopDelete = function(request, response) {

	var idLppStop = request.params.idLppStop;
	
	if (idLppStop) {
		LppStop
			.findByIdAndRemove(idLppStop)
		    .exec(
			    function(error, lppStop) {
			    	if (error) {
			       		jsonResponse(response, 404, error);
			        	return;
			      	}
			    	jsonResponse(response, 200, { "status": "success" });
			    }
		    );
	} else {
		jsonResponse(response, 400, {
			"messgae": "Lpp stop could not be found, parameter idLppStop is required."
		});
	}
};

module.exports.lppStopGetArrivals = function(req, resp) {
	var lppStop = req.params.lppStop;

	if (!lppStop) {
		jsonResponse(response, 404, {
			"message": "Parameter lppStop is required."
		});
		return;
	}

	var lppStopName = lppStop;

	lppStop = encodeURI(lppStop.toLowerCase());

	console.log('https://www.trola.si/' + lppStop);

	request(
		{
			url: 'https://www.trola.si/' + lppStop,
			method: 'GET',
		},
		function(error, response, html) {
			var arrivals = [];

			const root = HTMLParser.parse(html);

			var headers = root.querySelectorAll('.container h1');

			if (headers.length > 1) {
				var header1Text = headers[1].rawText;
				var header1 = header1Text.split(" ");
				var stationId1 = header1.length > 0 ? header1[0] : "";
				// var station1_name = header1Text.replace(/\d+/g, '');
				var station1_name = lppStopName;
				station1_name = station1_name.replace(/^\s+/,"");

				var station1 = {
					stationId: stationId1,
					station: station1_name,
					bus: []
				}

				var tables = root.querySelectorAll('.container table tbody');

				var trs1 = tables.length > 0 ? tables[0].querySelectorAll('tr') : [];
				trs1.forEach(function(tr) {
					var tds = tr.querySelectorAll('td');
					if (tds.length > 2) {
						station1.bus.push({
							label: tds[0].rawText.trim(),
							direction: tds[1].rawText.trim(),
							arrivalTimes: tds[2].rawText.trim()
						});
					}
				});
	
				arrivals.push(station1);
			}


			if (headers.length > 2) {
				var header2Text = headers[2].rawText;
				var header2 = header2Text.split(" ");
				var stationId2 = header2.length > 0 ? header2[0] : "";
				// var station2_name = header2Text.replace(/\d+/g, '');
				var station2_name = lppStopName;
				station2_name = station2_name.replace(/^\s+/,"");

				var station2 = {
					stationId: stationId2,
					station: station2_name,
					bus: []
				}

				var trs2 = tables.length > 1 ? tables[1].querySelectorAll('tr') : [];
				trs2.forEach(function(tr) {
					var tds = tr.querySelectorAll('td');
					if (tds.length > 2) {
						station2.bus.push({
							label: tds[0].rawText.trim(),
							direction: tds[1].rawText.trim(),
							arrivalTimes: tds[2].rawText.trim()
						});
					}
				});

				arrivals.push(station2);
			}

			jsonResponse(resp, 200, arrivals);
		}
	); 
}
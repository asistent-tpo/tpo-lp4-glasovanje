var mongoose = require('mongoose');
var Notification = mongoose.model('Notification');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};

module.exports.notificationsList = function(request, response) {
	Notification
		.find()
	    .exec(function(error, notifications) {
	   		if (error) {
           		jsonResponse(response, 500, error);
          	   	return;
           	}

           	jsonResponse(response, 200, notifications);
	    });
};

module.exports.notificationsCreate = function(request, response) {
	Notification.create({
			message: request.body.message,
			insertion_date: request.body.insertion_date
		}, function(error, notifications) {
		if (error) {
			jsonResponse(response, 400, error);
		} else {
			jsonResponse(response, 200, notifications);
		}
	});
};

module.exports.notificationsRead = function(request, response) {
	if (request.params && request.params.idNotification) {
		Notification
			.findById(request.params.idNotification)
		    .exec(function(error, notification) {
		   	    if (!notification) {
	           	   	jsonResponse(response, 404, {
	              		"message": "Notification with specified id could not be found."
	           	   	});
	          	   	return;
	            } else if (error) {
	           	   	jsonResponse(response, 500, error);
	          	   	return;
	            }

		   	    jsonResponse(response, 200, notification);
		   });
	} else {
	    jsonResponse(response, 400, { 
	      "message": "Missing parameter idNotification!"
	    });
	}
};

module.exports.notificationsUpdate = function(request, response) {
	if (!request.params.idNotification) {
		jsonResponse(response, 400, {
			"message": "Notification could not be found, parameter idNotification is required"
		});
		return;
	}
	Notification
		.findById(request.params.idNotification)
		.exec(
			function(error, notification) {
				if (!notification) {
					jsonResponse(response, 404, {
						"message": "Notification could not be found."
					});
					return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				notification.message = (typeof request.body.message !== "undefined") ? request.body.message : notification.message;
				notification.insertion_date = (typeof request.body.insertion_date !== "undefined") ? request.body.insertion_date : notification.insertion_date;

				notification.save(function(error, notification) {
					if (error) {
						jsonResponse(response, 400, error);
					} else {
						jsonResponse(response, 200, notification);
					}
				});
			}
		);
};

module.exports.notificationsDelete = function(request, response) {

	var idNotification = request.params.idNotification;
	if (idNotification) {
		Notification
			.findByIdAndRemove(idNotification)
		    .exec(
			    function(error, notification) {
			    	if (error) {
			       		jsonResponse(response, 404, error);
			        	return;
			      	}
			    	jsonResponse(response, 200, { "status": "success" });
			    }
		    );
	} else {
		jsonResponse(response, 400, {
			"messgae": "Notification could not be found, parameter idNotification is required."
		});
	}
};
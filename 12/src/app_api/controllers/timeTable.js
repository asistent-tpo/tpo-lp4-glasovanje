var mongoose = require('mongoose');
var User = mongoose.model('User');
var TimeTable = mongoose.model('TimeTable');

var jsonResponse = function(response, status, content) {
	response.status(status);
	response.json(content);
};


var checkTimeTable = function(timeTable) {
	var timeTableOK = true;

	if (!timeTable) {
		timeTableOK = false;
	} else if (!timeTable.name || !timeTable.duration || !timeTable.day || !timeTable.time) {
		timeTableOK = false;
	} else if (timeTable.duration + timeTable.time > 22) {
		timeTableOK = false;
	} else if (timeTable.duration + timeTable.time < 8) {
		timeTableOK = false;
	} else if (timeTable.duration < 1) {
		timeTableOK = false;
	}

	return timeTableOK;
};


module.exports.timeTableList = function(request, response) {
	if (request.params && request.params.idUser) {
		User
			.findById(request.params.idUser)
			.select('time_table')
			.exec(function(error, time_table) {
				if (error) {
	           		jsonResponse(response, 500, error);
	          	   	return;
	           	}

	           	if (!time_table) {
	           		jsonResponse(response, 400, {});
	           		return;
	           	}

	           	var time_table = time_table.time_table;
	           	time_table.sort(function(a,b){
					return new Date(b.date) - new Date(a.date);
				});
	           	
	           	jsonResponse(response, 200, time_table);
			});
	} else {
		jsonResponse(response, 400, { 
	    	"message": "Missing parameter idUser!"
	    });
	}
};

module.exports.timeTableCreate = function(request, response) {
	var idUser = request.params.idUser;
	if (idUser) {
		User
			.findById(idUser)
			.select('time_table')
			.exec(function(error, user) {
				if (error) {
			    	jsonResponse(response, 400, error);
			  	} else {
			    	if (!user) {
						jsonResponse(response, 404, {
						  "message": "User could not be found."
						});
					} else {
						user.time_table.push({
							name: request.body.name,
							duration: request.body.duration,
							day: request.body.day,
							colour: request.body.colour,
							time: request.body.time,
							insertion_date: request.body.insertion_date
						});
						user.save(function(error, user) {
							var added_time_table;
							if (error) {
								jsonResponse(response, 400, error);
							} else {
								added_time_table = user.time_table[user.time_table.length - 1];

								if (checkTimeTable(added_time_table)) {
									jsonResponse(response, 201, added_time_table);
								} else {
									jsonResponse(response, 400, 
										{"message" : "One of the values is not valid!", 
										"data" : added_time_table});
								}								
							}
						});
					}
			  	}
			});
	} else {
		jsonResponse(response, 400, {
			"message": "User could not be found, parameter idUser is required."
		});
	}
};

module.exports.timeTableRead = function(request, response) {
	if (request.params && request.params.idUser && request.params.idTimeTable) {
		User
			.findById(request.params.idUser)
			.select('time_table')
			.exec(
				function(error, user) {
					var time_table;
					if (!user) {
						jsonResponse(response, 404, {
						  	"message": "User could not be found, parameter idUser is required."
						});
						return;
					} else if (error) {
						jsonResponse(response, 500, error);
						return;
					}
					if (user.time_table && user.time_table.length > 0) {
						time_table = user.time_table.id(request.params.idTimeTable);
						if (!time_table) {
							jsonResponse(response, 404, {
								"message": "Time table could not be found, parameter idUser is required."
							});
						} else {
						 	jsonResponse(response, 200, {
							    time_table: time_table
						  	});
						}
					} else {
						jsonResponse(response, 404, {
							"message": "Couldnt find any time table."
						});
					}
				}
			);
	} else {
		jsonResponse(response, 400, {
			"message": "Time table could not be found, parameters idUser and idTimeTable are required."
		});
	}
};

module.exports.timeTableUpdate = function(request, response) {
	if (!request.params.idUser || !request.params.idTimeTable) {
	    jsonResponse(response, 400, {
	    	"message": "Time table could not be found, parameters idUser and idTimeTable are required."
	    });

	    return;
	}
  	
  	User
	    .findById(request.params.idUser)
	    .select('time_table')
	    .exec(
	      	function(error, user) {
		        if (!user) {
		          	jsonResponse(response, 404, {
		            	"message": "User could not be found."
		         	});
		          	return;
		        } else if (error) {
		          	jsonResponse(response, 500, error);
		          	return;
		        }

		        if (user.time_table && user.time_table.length > 0) {
		        	var current_time_table = user.time_table.id(request.params.idTimeTable);
					if (!current_time_table) {
						jsonResponse(response, 404, {
					  		"message": "Time table could not be found."
						});
					} else {
						current_time_table.name = (typeof request.body.name !== "undefined") ? request.body.name : current_time_table.name;
						current_time_table.duration = (typeof request.body.duration !== "undefined") ? request.body.duration : current_time_table.duration;
						current_time_table.day = (typeof request.body.day !== "undefined") ? request.body.day : current_time_table.day;
						current_time_table.colour = (typeof request.body.colour !== "undefined") ? request.body.colour : current_time_table.colour;
						current_time_table.time = (typeof request.body.time !== "undefined") ? request.body.time : current_time_table.time;
						current_time_table.insertion_date = (typeof request.body.insertion_date !== "undefined") ? request.body.insertion_date : current_time_table.insertion_date;

						user.save(function(error, user) {
							if (error) {
						    	jsonResponse(response, 400, error);
						  	} else {
										if (checkTimeTable(current_time_table)) {
											jsonResponse(response, 200, current_time_table);
										} else {
											jsonResponse(response, 400, 
												{"message" : "One of the values is not valid!", 
												"data" : current_time_table});
										}
						  	}
						});
					}
		        } else {
		        	jsonResponse(response, 404, {
		            	"message": "No time table to update."
		          	});
		        }
	      	}
	    );
};

module.exports.timeTableDelete = function(request, response) {

	if (!request.params.idUser || !request.params.idTimeTable) {
		jsonResponse(response, 400, {
		  "message": "Time table could not be found, parameters idUser and idTimeTable are required."
		});

		return;
	}

	User
		.findById(request.params.idUser)
		.exec(
			function(error, user) {
				if (!user) {
					jsonResponse(response, 404, {
				    	"message": "User could not be found."
				  	});
				  	return;
				} else if (error) {
					jsonResponse(response, 500, error);
					return;
				}

				if (user.time_table && user.time_table.length > 0) {
					if (!user.time_table.id(request.params.idTimeTable)) {
						jsonResponse(response, 404, {
							"message": "Time table could not be found."
						});
					} else {
						user.time_table.id(request.params.idTimeTable).remove();
						user.save(function(error) {
							if (error) {
								jsonResponse(response, 500, error);
							} else {
								jsonResponse(response, 200, { "status": "success" });
							}
						});
					}
				} else {
					jsonResponse(response, 404, {
				    	"message": "No time table to delete."
				  	});
				}
			}
		);
};
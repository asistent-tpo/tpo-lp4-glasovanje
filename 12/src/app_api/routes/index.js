var express = require('express');
var router = express.Router();
var usersController = require('../controllers/users');
var calendarController = require('../controllers/calendar');
var notificationsController = require('../controllers/notifications');
var authenticationController = require('../controllers/authentication');
var timeTableController = require('../controllers/timeTable');
var todoController = require('../controllers/todo');
var eventsController = require('../controllers/events');
var restaurantsController = require('../controllers/restaurants');
var mealsController = require('../controllers/meal');
var lppStopController = require('../controllers/lppStop');

/* Users */
router.get('/users', usersController.usersList);
router.post('/users', usersController.usersCreate);
router.get('/users/:idUser', usersController.usersRead);
router.put('/users/:idUser', usersController.usersUpdate);
router.delete('/users/:idUser', usersController.usersDelete);

/* Calendar */
router.get('/users/:idUser/calendar', calendarController.calendarList);
router.post('/users/:idUser/calendar', calendarController.calendarCreate);
router.get('/users/:idUser/calendar/:idCalendar', calendarController.calendarRead);
router.put('/users/:idUser/calendar/:idCalendar', calendarController.calendarUpdate);
router.delete('/users/:idUser/calendar/:idCalendar', calendarController.calendarDelete);

/* Notifications */
router.get('/notifications', notificationsController.notificationsList);
router.post('/notifications', notificationsController.notificationsCreate);
router.get('/notifications/:idNotification', notificationsController.notificationsRead);
router.put('/notifications/:idNotification', notificationsController.notificationsUpdate);
router.delete('/notifications/:idNotification', notificationsController.notificationsDelete);

/* Restaurants */
router.get('/restaurants', restaurantsController.restaurantsList);
router.post('/restaurants', restaurantsController.restaurantsCreate);
router.get('/restaurants/:idRestaurant', restaurantsController.restaurantsRead);
router.put('/restaurants/:idRestaurant', restaurantsController.restaurantsUpdate);
router.delete('/restaurants/deleteAll', restaurantsController.restaurantsDeleteAll);
router.delete('/restaurants/:idRestaurant', restaurantsController.restaurantsDelete);

/* Meals */
router.get('/restaurants/:idRestaurant/meals', mealsController.mealsList);
router.post('/restaurants/:idRestaurant/meals', mealsController.mealsCreate);
router.get('/restaurants/:idRestaurant/meals/:idMeal', mealsController.mealsRead);
router.put('/restaurants/:idRestaurant/meals/:idMeal', mealsController.mealsUpdate);
router.delete('/restaurants/:idRestaurant/meals/:idMeal', mealsController.mealsDelete);

/* Lpp */
router.get('/lppStop', lppStopController.lppStopList);
router.post('/lppStop', lppStopController.lppStopCreate);
router.get('/lppStop/search', lppStopController.lppStopSearch);
router.get('/lppStop/arrivals/:lppStop', lppStopController.lppStopGetArrivals);
router.get('/lppStop/:idLppStop', lppStopController.lppStopRead);
router.put('/lppStop/:idLppStop', lppStopController.lppStopUpdate);
router.delete('/lppStop/:idLppStop', lppStopController.lppStopDelete);

/* Authentication */
router.post('/registration', authenticationController.registration);
router.post('/login', authenticationController.login);
router.post('/confirmUser', authenticationController.confirmUser);
router.post('/changePassword', authenticationController.changePassword);

/* TimeTable */
router.get('/users/:idUser/timeTable', timeTableController.timeTableList);
router.post('/users/:idUser/timeTable', timeTableController.timeTableCreate);
router.get('/users/:idUser/timeTable/:idTimeTable', timeTableController.timeTableRead);
router.put('/users/:idUser/timeTable/:idTimeTable', timeTableController.timeTableUpdate);
router.delete('/users/:idUser/timeTable/:idTimeTable', timeTableController.timeTableDelete);

/* Todo */
router.get('/users/:idUser/todo', todoController.todoList);
router.post('/users/:idUser/todo', todoController.todoCreate);
router.get('/users/:idUser/todo/:idTodo', todoController.todoRead);
router.put('/users/:idUser/todo/:idTodo', todoController.todoUpdate);
router.delete('/users/:idUser/todo/:idTodo', todoController.todoDelete);

/* Events */
router.get('/events', eventsController.eventsList);
router.post('/events', eventsController.eventsCreate);
router.get('/events/:idEvent', eventsController.eventsRead);

module.exports = router;
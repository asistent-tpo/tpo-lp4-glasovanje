require('dotenv').config({path: __dirname + '/.env'});

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var passport = require('passport');
var app = express();
var sassMiddleware = require('node-sass-middleware');
var restaurantParser = require('./app_server/restaurantParser');
var mealParser = require('./app_server/mealParser');
var cron = require('node-cron');
var cronJob = require('cron').CronJob;
// var cronJob = require('node-cron').CronJob;

const PORT = process.env.PORT || 5000;

const session =  require('express-session') ;

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.use(sassMiddleware({
	src: path.join(__dirname, 'app_client/style'),
	dest: path.join(__dirname, 'public/stylesheets'),
	debug: true,
	indentedSyntax: false,
	outputStyle: 'compressed',
	prefix: '/stylesheets'

}));

require('./app_api/models/db');
require('./app_api/configuration/passport');

var indexApi = require('./app_api/routes/index');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


var iter = 1;

var parseRestaurantsTask = new cronJob("30 0 * * *", function() {
    console.log("Parsam restavracije");
	
	restaurantParser.parse();

	parseMealsTask.start();
}, undefined, true, "Europe/Ljubljana");

var parseMealsTask = cron.schedule('0 */10 * * * *', () => {
  	console.log("Parsam obroke " + iter);

  	mealParser.parse();

  	iter++;

  	if (iter == 7) {
  		parseMealsTask.stop();
  		iter = 1;
  	}
}, {
	scheduled: false
});


app.use(session({ secret: 'userId', resave: false, saveUninitialized: true }));


//odprava težav s HTTP odgovori
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'deny');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

app.use('/api', indexApi); 
app.use('/parseRestaurants', function(req, res, next) {
	restaurantParser.parse();
  res.send('Parsam restavracije')
}); 
app.use('/parseMeals', function(req, res, next) {
	mealParser.parse();
}); 

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

app.listen(PORT, function() {
	console.log("Application is listening on localhost:"+PORT);
});

module.exports = app;
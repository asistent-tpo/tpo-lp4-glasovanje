window.addEventListener("load", function() {
  console.log('nalozeno');
  /* global $ */
  document.getElementById('init').addEventListener('click', function(event) {
    event.preventDefault();
    $.ajax({
      type: "GET",
      url: "/api/v1/init",
      data: "",
      success: function() {
        $('#sporocilo').text("Baza uspešno napolnjena.");
        $('#sporocilo').removeClass("text-danger");
        $('#sporocilo').addClass("text-success");
      },
      error: function() {
        $('#sporocilo').text("Pri polnjenju baze je prišlo do napake.");
        $('#sporocilo').removeClass("text-success");
        $('#sporocilo').addClass("text-danger");
      }
    });
  });
  
  document.getElementById('empty').addEventListener('click', function(event) {
    event.preventDefault();
    $.ajax({
      type: "GET",
      url: "/api/v1/empty",
      data: "",
      success: function() {
        $('#sporocilo').text("Baza uspešno izpraznjena.");
        $('#sporocilo').removeClass("text-danger");
        $('#sporocilo').addClass("text-success");
      },
      error: function() {
        $('#sporocilo').text("Pri praznjenju baze je prišlo do napake.");
        $('#sporocilo').removeClass("text-success");
        $('#sporocilo').addClass("text-danger");
      }
    });
  });
});
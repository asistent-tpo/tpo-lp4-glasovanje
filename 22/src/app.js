require('dotenv').load();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');

var uglifyJs = require('uglify-js');
var fs = require('fs');

var zdruzeno = uglifyJs.minify({
  'app.js': fs.readFileSync('src/app_client/app.js', 'utf-8'),
  'navigacija.direktiva.js': fs.readFileSync('src/app_client/skupno/direktive/navigacija/navigacija.direktiva.js', 'utf-8'),
  'navigacija.krmilnik.js': fs.readFileSync('src/app_client/skupno/direktive/navigacija/navigacija.krmilnik.js', 'utf-8'),
  'home.krmilnik.js' : fs.readFileSync('src/app_client/home/home.krmilnik.js','utf-8'),
  'domacaStran.krmilnik.js' : fs.readFileSync('src/app_client/domacaStran/domacaStran.krmilnik.js','utf-8'),
  'avtentikacija.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/avtentikacija.storitev.js', 'utf-8'),
  'avtentikacija.krmilnik.js': fs.readFileSync('src/app_client/avtentikacija/avtentikacija.krmilnik.js', 'utf-8'),
  'potrjevanjeEmaila.krmilnik.js': fs.readFileSync('src/app_client/potrjevanjeEmaila/potrjevanjeEmaila.krmilnik.js', 'utf-8'),
  'database.krmilnik.js': fs.readFileSync('src/app_client/database/database.krmilnik.js', 'utf-8'),
  'cisteDesetkePodatki.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/cisteDesetkePodatki.storitev.js', 'utf-8'),
  'spreminjanjeGesla.krmilnik.js': fs.readFileSync('src/app_client/spreminjanjeGesla/spreminjanjeGesla.krmilnik.js', 'utf-8'),
  'adminObvestila.krmilnik.js': fs.readFileSync('src/app_client/adminObvestila/adminObvestila.krmilnik.js', 'utf-8'),
  'dogodki.krmilnik.js' : fs.readFileSync('src/app_client/dogodki/dogodki.krmilnik.js', 'utf-8'),
  'avtobusi.krmilnik.js': fs.readFileSync('src/app_client/avtobusi/avtobusi.krmilnik.js', 'utf-8'),
  'boni.krmilnik.js': fs.readFileSync('src/app_client/boni/boni.krmilnik.js', 'utf-8'),
}, {
  compress: false,
  mangle:false});

fs.writeFile('src/public/angular/ciste-desetke.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "ciste-desetke.min.js".');
});



require('./app_api/models/db');
require('./app_api/konfiguracija/passport');


var otherRouter = require("./app_api/routes/index");



var app = express();

// Odprava varnostnih pomanjkljivosti
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});



// view engine setup
if (app.get('env') !== 'test')
  app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));
app.use(express.static(path.join(__dirname, '../app_client')));

app.use(passport.initialize());




app.use("/api/v1",otherRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "sporočilo": err.name + ": " + err.message
    });
  }
});



module.exports = app;

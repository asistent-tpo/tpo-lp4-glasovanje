var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var avtentikacija = jwt({
  secret: process.env.JWT_GESLO,
  userProperty: 'payload'
});

var ctrlAvtentikacija = require('../controllers/avtentikacija');
var ctrlTodo = require('../controllers/todoSeznami');
var ctrlUrnik = require('../controllers/urnik');
var ctrlKoledar = require('../controllers/koledar');
var ctrlDatabase = require('../controllers/database');
var ctrlUser = require('../controllers/users');
var ctrlNotification = require('../controllers/notifications');
var ctrlEvents = require('../controllers/events');
var ctrlPostajalisca = require('../controllers/postajalisca');
var ctrlRestavracije = require('../controllers/restavracije');


/* Avtentikacija */
router.post('/register', ctrlAvtentikacija.register);
router.post('/login', ctrlAvtentikacija.login);
router.post('/confirmation', ctrlAvtentikacija.confirmEmail);

// Glavna stran
router.post('/todoElementi', avtentikacija, ctrlTodo.ustvariSeznam);
router.put('/todoElementi/izbrisi', avtentikacija, ctrlTodo.izbrisiSeznam);
router.get('/todoElementi', avtentikacija, ctrlTodo.vsiTODOSeznami);
router.post('/todoElementi/element', avtentikacija, ctrlTodo.dodajElement);
router.put('/todoElementi/element', avtentikacija, ctrlTodo.urediElement);
router.put('/todoElementi/element/izbrisi', avtentikacija, ctrlTodo.izbrisiElement);

router.get('/urnik', avtentikacija, ctrlUrnik.uporabnikovUrnik);
router.post('/urnik', avtentikacija, ctrlUrnik.dodajVnosVUrnik);
router.put('/urnik', avtentikacija, ctrlUrnik.urediVnosVUrniku);
router.put('/urnik/izbrisi', avtentikacija, ctrlUrnik.izbrisiVnosVUrniku);

router.get('/koledar', avtentikacija, ctrlKoledar.uporabnikovKoledar);
router.post('/koledar', avtentikacija, ctrlKoledar.dodajDogodek);
router.put('/koledar', avtentikacija, ctrlKoledar.urediDogodek);
router.put('/koledar/izbrisi', avtentikacija, ctrlKoledar.izbrisiDogodek);

/* database */
router.get('/empty', ctrlDatabase.empty);
router.get('/init', ctrlDatabase.populate);

/* user */
router.put('/users', avtentikacija, ctrlUser.updateCurrentUser);

/* notification */
router.post('/notification', avtentikacija, ctrlNotification.sendNotification);

/* avtobusi */
router.get('/postajalisca', ctrlPostajalisca.pridobiVsaPostajalisca);
router.get('/postajalisca/:name', ctrlPostajalisca.pridobiPostajalisce);
router.get('/prihodi/:id', ctrlPostajalisca.pridobiPrihode);

/* boni */
router.get('/lokacije/:naslov', ctrlRestavracije.pridobiKoordinate);
router.get('/restavracije', ctrlRestavracije.pridobiVseRestavracije);

/* events */
router.get('/events', ctrlEvents.getAllEvents);
router.post('/events', avtentikacija, ctrlEvents.createEvent);

module.exports = router;
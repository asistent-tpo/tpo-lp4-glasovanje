var mongoose = require('mongoose');

var restavracijeSchema = new mongoose.Schema({
    lat: String,
	lon: String,
	naslov: String,
	cena: String,
	doplacilo: String,
	posid: String,
	lokal: String,
	city: String
});

mongoose.model('Restavracija', restavracijeSchema, 'Restavracije');
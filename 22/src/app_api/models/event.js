var mongoose = require('mongoose');

var eventSchema = new mongoose.Schema({
    name: { type:String, required: true },
    description: String,
    date: { type:Date, min:'2018-01-01', max:'2099-12-24' },
    organizer: String,
});

mongoose.model('Event', eventSchema, 'Events');
var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
    email: { type:String, required: true, unique: true},
    zgoscenaVrednost: String,
    nakljucnaVrednost: String,
    verificationToken:String,
    verified: {type:Boolean, "default":false},
    role: {type: String, "default": "user"}
});



userSchema.methods.nastaviGeslo = function(geslo) {
  this.nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
  this.zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.preveriGeslo = function(geslo) {
  var zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
  return this.zgoscenaVrednost == zgoscenaVrednost;
};

// userSchema.methods.nastaviVerificationToken = function() {
//   var verificationToken = new Token({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });;
//   return verificationToken;
// };

userSchema.methods.generirajJwt = function() {
  var datumPoteka = new Date();
  datumPoteka.setDate(datumPoteka.getDate() + 7);
  
  return jwt.sign({
    _id: this._id,
    email: this.email,
    datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10), 
    role: this.role
  }, process.env.JWT_GESLO);
};

mongoose.model('User', userSchema, 'Users');
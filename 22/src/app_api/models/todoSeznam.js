var mongoose = require('mongoose');

var todoSchema = new mongoose.Schema({
    uporabnik: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    nazivSeznama: String,
    elementi: [{
        naziv: { type:String, required: true},
        opis: String,
        koncan: Boolean
    }]
});

mongoose.model('TodoSeznam', todoSchema, 'TodoSeznam');
var mongoose = require('mongoose');

var koledarSchema = new mongoose.Schema({
    uporabnik: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    elementi: [{
        naziv: { type:String, required: true},
        opis: String,
        datum: {type: Date, required: true, default: Date.now },
        trajanje: Number
    }]
});

mongoose.model('KoledarShema', koledarSchema, 'KoledarShema');
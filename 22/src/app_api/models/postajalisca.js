var mongoose = require('mongoose');

var postajaliscaSchema = new mongoose.Schema({
    id: String,
    name: String,
    center: Boolean
});

mongoose.model('Postajalisce', postajaliscaSchema, 'Postajalisca');
var mongoose = require('mongoose');

var AdminMessageSchema = new mongoose.Schema({
    subject: {type: String, required: true},
    text: { type:String, required: true },
});

mongoose.model('AdminMessage', AdminMessageSchema, 'AdminMessages');
var mongoose = require('mongoose');

var urnik = new mongoose.Schema({
    uporabnik: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    vnosi: [{
        naziv: { type: String, required: true },
        danVTednu: Number,
        uraZacetka: Number,
        trajanje: Number,        
        barva: String
    }]
});

mongoose.model('Urnik', urnik, 'Urnik');
var mongoose = require('mongoose');
var Koledar = mongoose.model('KoledarShema');
var User = mongoose.model('User');

mongoose.set('useFindAndModify', false);

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

var userId = function (zahteva, odgovor, callback) {
  if (zahteva.payload && zahteva.payload._id) {
    User
      .findById(zahteva.payload._id)
      .exec(function (napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        callback(zahteva, odgovor, uporabnik._id);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      sporocilo: "Ni podatka o uporabniku"
    });
  }
};

module.exports.uporabnikovKoledar = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    Koledar.find({ uporabnik: idUporabnika }).exec(function (napaka, koledarji) {
      if (napaka) {
        vrniJsonOdgovor(odgovor1, 400, { sporocilo: "neveljavna zahteva" });
        return;
      } else {
        if (koledarji.length === 0) {
          // ustvari nov urnik
          var noviKoledar = new Koledar();
          noviKoledar.uporabnik = idUporabnika;
          noviKoledar.elementi = [];
          noviKoledar.save(function (napaka, doc) {
            if (napaka) {
              vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
            } else {
              vrniJsonOdgovor(odgovor, 200, doc);
            }
          });
        } else {
          vrniJsonOdgovor(odgovor1, 200, koledarji[0]);
        }
      }
    })
  });
}

/**
 * @param {integer} idKoledarja
 * @param {string} naziv
 * @param {string} opis
 * @param {date} datum
 * @param {number} trajanje
 */
module.exports.dodajDogodek = function (zahteva, odgovor) {
    userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
      if (!zahteva.body || !zahteva.body.idKoledarja) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
        return;
      }
      var idKoledarja = zahteva.body.idKoledarja;
      Koledar.findById(idKoledarja, function (err, koledar) {
        if (koledar === null || koledar.uporabnik.toString() !== idUporabnika.toString()) {
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj koledar." });
          return;
        }
        if (zahteva.body.trajanje === undefined || isNaN(zahteva.body.trajanje)) {
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Trajanje mora biti stevilo." });
          return;
        }
        if (zahteva.body.naziv === undefined || zahteva.body.opis === undefined || zahteva.body.datum === undefined) {
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Naziv, opis in datum morajo biti podani." });
          return;
        }
        if (!aliJeDatumVeljaven(zahteva.body.datum)) {
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Podani datum ni veljaven." });
          return;
        }
        koledar.elementi.push({
          naziv: zahteva.body.naziv,
          opis: zahteva.body.opis,
          datum: zahteva.body.datum,
          trajanje: zahteva.body.trajanje
        });
        Koledar.findOneAndUpdate({ _id: idKoledarja }, { elementi: koledar.elementi }, function (napaka) {
          if (napaka) {
            console.log(napaka);
            vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
          } else {
            vrniJsonOdgovor(odgovor, 200, koledar);
          }
        });
      });
    });
  }

/**
 * @param {integer} idKoledarja
 * @param {integer} idDogodka
 * @param {string} naziv
 * @param {string} opis
 * @param {date} datum
 * @param {number} trajanje
 */
module.exports.urediDogodek = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    if (!zahteva.body || !zahteva.body.idKoledarja) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idKoledarja = zahteva.body.idKoledarja;
    Koledar.findById(idKoledarja, function (err, koledar) {
      if (koledar === null || koledar.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj koledar." });
        return;
      }
      if (zahteva.body.idDogodka === undefined || isNaN(zahteva.body.idDogodka) || zahteva.body.idDogodka >= koledar.elementi.length) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljaven idDogodka" });
        return;
      }
      if (zahteva.body.trajanje === undefined || isNaN(zahteva.body.trajanje)) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Trajanje mora biti stevilo." });
        return;
      }
      if (zahteva.body.naziv === undefined || zahteva.body.opis === undefined || zahteva.body.datum === undefined) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Naziv, opis in datum morajo biti podani." });
        return;
      }
      if (!aliJeDatumVeljaven(zahteva.body.datum)) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Podani datum ni veljaven." });
        return;
      }
      koledar.elementi[zahteva.body.idDogodka] = {
        naziv: zahteva.body.naziv,
        opis: zahteva.body.opis,
        datum: zahteva.body.datum,
        trajanje: zahteva.body.trajanje
      };
      Koledar.findOneAndUpdate({ _id: idKoledarja }, { elementi: koledar.elementi }, function (napaka) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, koledar);
        }
      });
    });
  });
}

/**
 * @param {integer} idKoledarja
 * @param {integer} idDogodka
 */
module.exports.izbrisiDogodek = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    if (!zahteva.body || !zahteva.body.idKoledarja) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idKoledarja = zahteva.body.idKoledarja;
    Koledar.findById(idKoledarja, function (err, koledar) {
      if (koledar === null || koledar.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj koledar." });
        return;
      }
      if (zahteva.body.idDogodka === undefined || isNaN(zahteva.body.idDogodka) || zahteva.body.idDogodka >= koledar.elementi.length) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljaven idDogodka" });
        return;
      }
      koledar.elementi.splice(zahteva.body.idDogodka, 1);
      Koledar.findOneAndUpdate({ _id: idKoledarja }, { elementi: koledar.elementi }, function (napaka) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, koledar);
        }
      });
    });
  });
}

function aliJeDatumVeljaven(datum) {
  return !isNaN(Date.parse(datum));
}

module.exports.aliJeDatumVeljaven = aliJeDatumVeljaven;
var mongoose = require('mongoose');
var TodoSeznam = mongoose.model('TodoSeznam');
var User = mongoose.model('User');

mongoose.set('useFindAndModify', false);

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vsiTODOSeznami = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    TodoSeznam.find({ uporabnik: idUporabnika }).exec(function (napaka, seznami) {
      if (napaka) {
        vrniJsonOdgovor(odgovor1, 400, { sporocilo: "neveljavna zahteva" });
      } else {
        vrniJsonOdgovor(odgovor1, 200, seznami);
      }
    })
  });
}


/**
 * @param {integer} idSeznama
 * @param {integer} idElementa
 */
module.exports.izbrisiElement = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, dogovor1, idUporabnika) {
    if (!zahteva.body) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idSeznama = zahteva.body.idSeznama;
    if (!idSeznama) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Podati moraš id seznama." });
      return;
    }
    TodoSeznam.findById(idSeznama, function (err, seznam) {
      if (seznam === undefined || seznam.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj seznam." });
        return;
      }
      var idElementa = zahteva.body.idElementa;
      if (idElementa === undefined || isNaN(idElementa) || seznam.elementi.length <= idElementa) {
        console.log("neveljaven id", !idElementa, seznam.elementi.length <= idElementa);
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljaven id elementa." });
        return;
      }
      var noviElementi = seznam.elementi.slice();
      noviElementi.splice(idElementa, 1);
      TodoSeznam.findOneAndUpdate({ _id: idSeznama }, { elementi: noviElementi }, function (napaka, doc) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, { sporocilo: "Uspesno posodobljeno." });
        }
      });
    });
  });
};

/**
 * @param {integer} idSeznama
 * @param {integer} idElementa
 * @param {string} naziv
 * @param {string} opis
 */
module.exports.urediElement = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, dogovor1, idUporabnika) {
    if (!zahteva.body) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idSeznama = zahteva.body.idSeznama;
    if (!idSeznama) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Podati moraš id seznama." });
      return;
    }
    TodoSeznam.findById(idSeznama, function (err, seznam) {
      if (seznam === undefined || seznam.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj seznam." });
        return;
      }
      var idElementa = zahteva.body.idElementa;
      if (idElementa === undefined || isNaN(idElementa) || seznam.elementi.length <= idElementa) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljaven id elementa." });
        return;
      }
      var noviElementi = seznam.elementi;
      noviElementi[idElementa] = {
        naziv: zahteva.body.naziv,
        opis: zahteva.body.opis,
        koncan: noviElementi[idElementa].koncan
      };
      TodoSeznam.findOneAndUpdate({ _id: idSeznama }, { elementi: noviElementi }, function (napaka) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, { sporocilo: "Uspesno posodobljeno." });
        }
      });
    });
  });
};

/**
 * @param {integer} idSeznama
 * @param {string} naziv
 * @param {string} opis
 */
module.exports.dodajElement = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, dogovor1, idUporabnika) {
    if (!zahteva.body) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idSeznama = zahteva.body.idSeznama;
    if (!idSeznama) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Podati moraš id seznama." });
      return;
    }
    TodoSeznam.findById(idSeznama, function (err, seznam) {
      if (seznam === undefined || seznam.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj seznam." });
        return;
      }
      seznam.elementi.push({
        naziv: zahteva.body.naziv,
        opis: zahteva.body.opis,
        koncan: false
      });
      TodoSeznam.findOneAndUpdate({ _id: idSeznama }, { elementi: seznam.elementi }, function (napaka) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, { sporocilo: "Uspesno posodobljeno." });
        }
      });
    });
  });
};

/**
 * @param {string} nazivSeznama
 */
module.exports.ustvariSeznam = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, dogovor1, idUporabnika) {
    var todoSeznam = new TodoSeznam();
    if (zahteva.body) {
      todoSeznam.uporabnik = idUporabnika;
      todoSeznam.nazivSeznama = zahteva.body.nazivSeznama;
      todoSeznam.elementi = [];
    }
    todoSeznam.save(function (napaka) {
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
      } else {
        vrniJsonOdgovor(odgovor, 200, {
        });
      }
    });
  });
};

/**
 * @param {string} idSeznama
 */
module.exports.izbrisiSeznam = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, dogovor1, idUporabnika) {
    if (!zahteva.body) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idSeznama = zahteva.body.idSeznama;
    if (!idSeznama) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Podati moraš id seznama." });
      return;
    }
    TodoSeznam.findById(idSeznama, function (err, seznam) {
      if (seznam === undefined || seznam.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj seznam." });
        return;
      }
      seznam.remove(function (err) {
        if (err) {
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Brisanje ni uspelo." });
          return;
        } else {
          vrniJsonOdgovor(odgovor, 200, { sporocilo: "Uspešno izbrisano." });
          return;
        }
      })
    });
  });
};

var userId = function (zahteva, odgovor, callback) {
  if (zahteva.payload && zahteva.payload._id) {
    User
      .findById(zahteva.payload._id)
      .exec(function (napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        callback(zahteva, odgovor, uporabnik._id);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      sporocilo: "Ni podatka o uporabniku"
    });
  }
};
var mongoose = require("mongoose");

var EventSchema = require("../models/event").eventSchema;
mongoose.model("Event", EventSchema);
var Event = mongoose.model("Event");

var UserSchema = require("../models/user").userSchema;
mongoose.model("User", UserSchema);
var User = mongoose.model("User");

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  if(odgovor == "b") return;
  odgovor.status(status);
  odgovor.json(vsebina);
};


module.exports.getAllEvents = function(zahteva, odgovor) {
    Event.find().exec(function(err, events) {
        if(err) {
            vrniJsonOdgovor(odgovor, 500, err);
            return -1;
        }
        else {
          vrniJsonOdgovor(odgovor, 200, events);
          return 1;
        }
        
    });
    return "end";
};


module.exports.createEvent = function(zahteva, odgovor) {
  if (!zahteva.body) {
    vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Ni zahteve."});
    return;
  }
  else if (zahteva.payload && zahteva.payload._id){
    User
      .findById(zahteva.payload._id) 
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem uporabnika"});
          return -1;
        } 
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return -1;
        }
        else if (uporabnik.role != "eventManager"){
          vrniJsonOdgovor(odgovor, 401, { sporocilo: "Samo uporavljalec dogodkov lahko ustvarja dogodke." });
          return -1;
        }
        else {
          var insertEvent = new Event();
          insertEvent.name = zahteva.body.ime;
          insertEvent.description = zahteva.body.opis;
          insertEvent.date = zahteva.body.datum;
          insertEvent.organizer = zahteva.body.organizator;
          
          insertEvent.save(function (error) {
              if (error) {
                vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
                return -1;
              } 
              else {
                vrniJsonOdgovor(odgovor, 200, {sporocilo: "Shranjeno."});
                return 1;
              }
          });
        }
      });
    }
    else {
      vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Napačna zahteva."});
    }
};
var mongoose = require('mongoose');
var request = require('request');
var Restavracija = mongoose.model('Restavracija');

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.pridobiKoordinate = function (zahteva, odgovor) {
  if(zahteva.params && zahteva.params.naslov) {
    var url = 'https://maps.google.com/maps/api/geocode/json?address=' + zahteva.params.naslov;
    url += '&components=country:si&key=AIzaSyCb5cHww7tsxAMqgvx44QPhTv3pBoOEQXE';
    var parametriZahteve = {
      url: url,
      method: 'GET'
    };
    request(parametriZahteve, function(napaka, odgovor2, vsebina) {
      if (napaka) {
        vrniJsonOdgovor(odgovor, 500, { "sporočilo": "Ne dobim odgovora od zunanjega APIja" });
      } else {
        vrniJsonOdgovor(odgovor, 200, vsebina);
      }
    });
  }
};

module.exports.pridobiVseRestavracije = function (zahteva, odgovor) {
  Restavracija
    .find()
    .exec(function(napaka, restavracije) {
      if (!restavracije){
        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "V bazi ni restavracij" });
        return;
      } else if (napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
        return;
      }
      vrniJsonOdgovor(odgovor, 200, restavracije);
    });
};

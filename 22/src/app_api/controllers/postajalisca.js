var mongoose = require('mongoose');
var request = require('request');
var Postajalisce = mongoose.model('Postajalisce');

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.pridobiVsaPostajalisca = function (zahteva, odgovor) {
  Postajalisce
    .find()
    .exec(function(napaka, postajalisca) {
      if (!postajalisca){
        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "V bazi ni postajališč" });
        return;
      } else if (napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
        return;
      }
      vrniJsonOdgovor(odgovor, 200, postajalisca);
    });
};

module.exports.pridobiPostajalisce = function (zahteva, odgovor) {
    if(zahteva.params && zahteva.params.name) {
    Postajalisce
      .find({ name: new RegExp(zahteva.params.name, 'i') })
      .exec(function(napaka, postajalisce) {
        if (!postajalisce){
          vrniJsonOdgovor(odgovor, 404, { "sporočilo": "Postajališče ne obstaja" });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, postajalisce);
      });
  } else {
    vrniJsonOdgovor(odgovor, 500, { "sporočilo": "Ne najdem postajališča, ime postajališča je obvezen parameter" });
  }
    
};

module.exports.pridobiPrihode = function (zahteva, odgovor) {
  if(zahteva.params && zahteva.params.id) {
    var parametriZahteve = {
      url: 'https://www.trola.si/' + zahteva.params.id,
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    };
    request(parametriZahteve, function(napaka, odgovor2, vsebina) {
      if (napaka) {
        vrniJsonOdgovor(odgovor, 500, { "sporočilo": "Ne dobim odgovora od zunanjega APIja" });
      } else {
        vrniJsonOdgovor(odgovor, 200, vsebina);
      }
    });
  }
};

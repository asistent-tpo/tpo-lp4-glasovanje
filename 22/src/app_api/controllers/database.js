var mongoose = require("mongoose");
var User = mongoose.model("User");
var Event = mongoose.model("Event");
var TodoSeznam = mongoose.model("TodoSeznam");
var Urnik = mongoose.model("Urnik");
var Koledar = mongoose.model("KoledarShema");

var PostajaliscaShema = mongoose.model("Postajalisce");
var RestavracijeShema = mongoose.model("Restavracija");

var postajalisca = require("../models/postajalisca.json");
var restavracije = require("../models/restavracije.json");

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

function pripraviTodoInUrnik(idUporabnika) {
    var error = false;
// todo seznami
var todoSeznam = new TodoSeznam({
    uporabnik: idUporabnika,
    nazivSeznama: "OiM",
    elementi: [{
        naziv: "Seminarska",
        opis: "Napiši seminarsko in pripravi predstavitev",
        koncan: false
    },{
        naziv: "Kolokvij",
        opis: "Nauči se snov za kolokvij",
        koncan: false
    }]  
});

todoSeznam.save(function (err) {
    if (err) {
        console.log(err);
        error = true;
    }
});

var todoSeznam2 = new TodoSeznam({
    uporabnik: idUporabnika,
    nazivSeznama: "PSP",
    elementi: [{
        naziv: "Sprint #4",
        opis: "Implementiraj poljubno funkcionalnost",
        koncan: false
    },{
        naziv: "Izpit",
        opis: "Nauči se snov",
        koncan: false
    }]  
});

todoSeznam2.save(function (err) {
    if (err) {
        console.log(err);
        error = true;
    }
});

// urnik
var urnik = new Urnik({
    uporabnik: idUporabnika,
    vnosi: [{
            "naziv" : "RIS",
            "danVTednu" : 5,
            "uraZacetka" : 8,
            "trajanje" : 3,
            "barva" : "#aaaaaa"
        },{
            "naziv" : "RIS",
            "danVTednu" : 0,
            "uraZacetka" : 11,
            "trajanje" : 3,
            "barva" : "#a1f28b"
        },{
            "naziv" : "PSP",
            "danVTednu" : 0,
            "uraZacetka" : 16,
            "trajanje" : 3,
            "barva" : "#e2cb95"
        },{
            "naziv" : "PSP-V",
            "danVTednu" : 2,
            "uraZacetka" : 13,
            "trajanje" : 2,
            "barva" : "#e2bd95"
        },{
            "naziv" : "TPO",
            "danVTednu" : 3,
            "uraZacetka" : 9,
            "trajanje" : 3,
            "barva" : "#95d6e2"
        },{
            "naziv" : "RIS-V",
            "danVTednu" : 3,
            "uraZacetka" : 12,
            "trajanje" : 2,
            "barva" : "#a6e295"
        },{
            "naziv" : "DS",
            "danVTednu" : 4,
            "uraZacetka" : 12,
            "trajanje" : 2,
            "barva" : "#e29595"
        },{
            "naziv" : "TPO-V",
            "danVTednu" : 4,
            "uraZacetka" : 15,
            "trajanje" : 2,
            "barva" : "#95d6e2"
        }
    ]
    });

    urnik.save(function (err) {
        if (err) {
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init urnika"});
        return false;
    }

    var koledar = new Koledar({
        "elementi" : [ 
            {
                "datum" : "2019-05-25T10:00:00.000Z",
                "naziv" : "poskusni dogodek",
                "opis" : "opis",
                "trajanje" : 2
            }, 
            {
                "datum" : "2019-05-16T07:00:00.000Z",
                "naziv" : "Ena stvar",
                "opis" : "en opis",
                "trajanje" : 8
            }, 
            {
                "datum" : "2019-05-30T07:00:00.000Z",
                "naziv" : "TPO kviz",
                "opis" : "kviz pri predmetu tpo",
                "trajanje" : 3
            }, 
            {
                "datum" : "2019-06-03T09:00:00.000Z",
                "naziv" : "RIS tekmovanje",
                "opis" : "tekmovanje z roboti",
                "trajanje" : 4
            }, 
            {
                "datum" : "2019-05-26T19:00:00.000Z",
                "naziv" : "TPO LP4",
                "opis" : "rok za oddajo",
                "trajanje" : 1
            }, 
            {
                "datum" : "2019-05-20T10:00:00.000Z",
                "naziv" : "Dolg dogodek",
                "opis" : "dolg dogodek",
                "trajanje" : 48
            }, 
            {
                "datum" : "2019-06-07T14:00:00.000Z",
                "naziv" : "Izpit",
                "opis" : "izpit",
                "trajanje" : 1
            }, 
            {
                "naziv" : "se en dolg dogodek",
                "opis" : "",
                "datum" : "2019-06-18T10:00:00.000Z",
                "trajanje" : 24
            }
        ],
        "uporabnik" : idUporabnika,
        });
    
        koledar.save(function (err) {
            if (err) {
                console.log(err);
                error = true;
            }
        });
        if(error){
            vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init koledarja"});
            return false;
        }

    return true;
}

module.exports.populate = function(zahteva, odgovor) {
    var error = false;

    PostajaliscaShema.insertMany(postajalisca,function(err) {
        if(err){
            console.log(err);
            error = true;
        } else {
            RestavracijeShema.insertMany(restavracije,function(err) {
                if(err){
                    console.log(err);
                    error = true;
                } else {
                    vrniJsonOdgovor(odgovor, 200, {"status" : "Uspešno ustvarjeno."});                    
                }
            });
        }
    });

    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "napaka pri dodajanju postaj in restavracij"});
        return;
    } 

    var initialUser = new User({
        email: "janez.novak@primer.si",
        verified: true,
    });
    
    initialUser.nastaviGeslo("geslofri");
    
    var initialUser2 = new User({
        email: "john.smith@primer.si",
        verified: true,
        role: "eventManager"
    });
    
    initialUser2.nastaviGeslo("password");
    
    var admin = new User({
        email: "admin@admin.si",
        verified: true,
        role: "admin"
    });
    
    admin.nastaviGeslo("adminfri");
    
    //user1
    initialUser.save(function (err, data) {
        if (err) {
            console.log(err);
            error = true;
        } else {
            pripraviTodoInUrnik(data._id);
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init userja1"});
        return;
    }
    
    //user2
    initialUser2.save(function (err) {
        if (err) {
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init userja2"});
        return;
    }
    
    //user3
    admin.save(function (err) {
        if (err) {
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init admina"});
        return;
    }

    //Events
    var event1 = new Event({
        name: "Izlet v Gardland",
        description: "Obisk Gardalanda",
        date: "2019-10-10",
        organizer: "SŠFRI"
    }); 
    var event2 = new Event({
        name: "Gostujoče predavanje: Angular v praksi",
        description: "P5 14.00",
        date: "2019-10-15",
        organizer: "FRI"
    })
    var event3 = new Event({
        name: "Pouka prost dan",
        description: "100. obletnica UL",
        date: "2019-06-25",
        organizer: "UL"
    })
    var event4 = new Event({
        name: "Sestanek predstavnikov študentskega sveta",
        description: "Prenova predmetov in ocena organizacije pedagoškega dela",
        date: "2019-09-10",
        organizer: "FRI"
    })
    
    event1.save(function (err) {
        if (err) {error = true;}
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init event1"});
        return;
    }
    event2.save(function (err) {
        if (err) {error = true;}
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init event2"});
        return;
    }
    event3.save(function (err) {
        if (err) {error = true;}
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init event3"});
        return;
    }
    event4.save(function (err) {
        if (err) {error = true;}
    });
    if(error){
        vrniJsonOdgovor(odgovor, 500, {"status" : "neuspesen init event4"});
        return;
    }
};

module.exports.empty = function(zahteva,odgovor){
    var error = false;
    
    User.deleteMany({},function(err) {
        if(err){
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "neuspesno odstranjevanje userjev"});
        return;
    }

    TodoSeznam.deleteMany({},function(err) {
        if(err){
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "neuspesno odstranjevanje todo seznamov"});
        return;
    }
    
    Urnik.deleteMany({},function(err) {
        if(err){
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "neuspesno odstranjevanje urnika"});
        return;
    }  

    Koledar.deleteMany({},function(err) {
        if(err){
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "neuspesno odstranjevanje koledarja"});
        return;
    }  

    Event.deleteMany({},function(err) {
        if(err){
            console.log(err);
            error = true;
        }
    });
    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "neuspesno odstranjevanje dogodkov"});
        return;
    } 
    
    PostajaliscaShema.deleteMany({},function(err) {
        if(err){
            console.log(err);
            error = true;
        } else {
            RestavracijeShema.deleteMany({},function(err) {
                if(err){
                    console.log(err);
                    error = true;
                } else {
                    vrniJsonOdgovor(odgovor, 204, {"status": "uspešno odstranjevanje"});
                }
            });
        }
    });

    if(error){
        vrniJsonOdgovor(odgovor,500, {"status": "napaka pri brisanju postaj in restavracij"});
        return;
    } 
};

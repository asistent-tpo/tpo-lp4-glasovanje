var mongoose = require("mongoose");
var User = mongoose.model("User");

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

var userId = function(zahteva, odgovor, callback) {
  if (zahteva.payload && zahteva.payload._id) {
    User
      .findById(zahteva.payload._id) 
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        callback(zahteva, odgovor, uporabnik._id);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      sporocilo:"Ni podatka o uporabniku"
    });
  }
};











module.exports.updateCurrentUser = function(zahteva,odgovor){
  userId(zahteva, odgovor, function(zahteva, odgovor, id) {
      if (zahteva.params) {
        
        if (zahteva.body.password) {
          if(!zahteva.body.passwordOld){
            vrniJsonOdgovor(odgovor,400,{"sporocilo":"Potrebno je tudi staro geslo"});
            return;
          }
          if(!zahteva.body.passwordCompare){
            vrniJsonOdgovor(odgovor,400,{"sporocilo":"Potrebno je ponoviti geslo"});
            return;
          }
          if(zahteva.body.password !== zahteva.body.passwordCompare){
            vrniJsonOdgovor(odgovor,400,{"sporocilo":"Gesli se morata ujemati"});
            return;
          }
          if(zahteva.body.password.length < 8){
            vrniJsonOdgovor(odgovor,400,{"sporocilo":"Geslo mora biti dolgo vsaj 8 znakov."});
            return;
          }
            User
              .findById(id)
              .exec(function(napaka, user) {
                if(napaka) {
                  vrniJsonOdgovor(odgovor,500,napaka);
                  return;
                }
                if(!user) {
                  vrniJsonOdgovor(odgovor,400,{"sporocilo":"Ta uporabnik ne obstaja"});
                  return;
                } else {
                  if(user.preveriGeslo(zahteva.body.passwordOld)) {
                    user.nastaviGeslo(zahteva.body.password);
                    user.save(function(napaka) {
                      if (napaka) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                        return;
                      } else {
                        vrniJsonOdgovor(odgovor, 200, {
                          "zeton": user.generirajJwt()
                        });
                        return;
                      }
                    });
                  } 
                  else {
                    vrniJsonOdgovor(odgovor,401,{"sporocilo":"Geslo ni pravilno"});
                    return;
                  }
                }
              });
        } 
        else {
            vrniJsonOdgovor(odgovor, 400, {"sporocilo" : "potrebno je vsaj eno polje, ki ga spreminjate"});
            return;
        }
      }
    });
};







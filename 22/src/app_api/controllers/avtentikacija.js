var passport = require('passport');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var crypto = require('crypto');
var User = mongoose.model('User');
var Token = mongoose.model('Token');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.register = function(zahteva, odgovor) {
  if (!zahteva.body.email || !zahteva.body.password1 || !zahteva.body.password2) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporocilo": "Zahtevani so vsi podatki"
    });
    return;
  }
  if(zahteva.body.password1 !== zahteva.body.password2){
      vrniJsonOdgovor(odgovor,400,{
        "sporocilo" : "Gesli se ne ujemata"
        
      });
      return;
  }
  if(zahteva.body.password1.length<8){
      vrniJsonOdgovor(odgovor,400,{
        "sporocilo" : "Geslo mora biti dolgo vsaj 8 znakov."
        
      });
      return;
  }
  var uporabnik = new User();
  uporabnik.email= zahteva.body.email;
  uporabnik.nastaviGeslo(zahteva.body.password1);
  uporabnik.save(function(napaka) {
  if (napaka) {
    vrniJsonOdgovor(odgovor, 400, {sporocilo: "Ti prijavni podatki so že zasedeni."});
    return;
  } 
  // else {
  //   vrniJsonOdgovor(odgovor, 200, {
  //     "zeton": uporabnik.generirajJwt()
  //   });
  // }
    var token = new Token({ _userId: uporabnik._id, token: crypto.randomBytes(16).toString('hex') });
    // Save the verification token
    token.save(function (napaka) {
      if (napaka) { 
        console.log(napaka)
        vrniJsonOdgovor(odgovor, 500, {sporocilo: "Napaka pri generiranju zetona"});
        
      }
      var testAccount = nodemailer.createTestAccount();

      // Send the email
      var transporter = nodemailer.createTransport({ service: 'Sendgrid', auth: { user: process.env.SENDGRID_USER, pass: process.env.SENDGRID_PASS } });
      var mailOptions = { from: 'sistem-ne-odgovarjaj@ciste-desetke.herokuapp.com', to: uporabnik.email, subject: 'Account Verification Token', text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + zahteva.headers.host + '\/confirmation\/' + token.token + '.\n' };
      transporter.sendMail(mailOptions, function (napaka) {
          if (napaka) { 
            vrniJsonOdgovor(odgovor, 500, {sporocilo: "Napaka pri posiljanju maila"}); 
            return;
            
          } else {
            vrniJsonOdgovor(odgovor, 200, {sporocilo: "Potrditveni mail je bil poslan"});
            return;
          }
          
      });
    });
  });
};

module.exports.login = function(zahteva, odgovor) {
  if (!zahteva.body.email || !zahteva.body.password) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  passport.authenticate('local', function(napaka, uporabnik, podatki) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
      return;
    }
    if (uporabnik) {
      if(!uporabnik.verified) {
        vrniJsonOdgovor(odgovor, 400, {sporocilo: "Niste še potrdili svojega emaila. Ta je lahko tudi v vsiljeni pošti." });
        return;
      } 
      vrniJsonOdgovor(odgovor, 200, {
        "zeton": uporabnik.generirajJwt()
      });
    } else {
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
};

module.exports.confirmEmail = function(zahteva, odgovor) {
  // Find a matching token
    Token.findOne({ token: zahteva.body.token }, function (napaka, token) {
      if(napaka) {
        vrniJsonOdgovor(odgovor, 500, "Napaka pri iskanju žetona.");
        return;
      }
        if (!token)  {
          vrniJsonOdgovor(odgovor, 400, "Ni bilo možno najti veljavnega žetona. Morda je žeton pretekel.");
          return;
        }
        // If we found a token, find a matching user
        User.findOne({ _id: token._userId }, function (napaka, user) {
            if(napaka) {
              vrniJsonOdgovor(odgovor, 500, "Napaka pri iskanju uporabnika.");
              return;
            }
            if (!user) {
              vrniJsonOdgovor(odgovor, 400, "Za ta žeton ni bilo mogoče najti uporabnika");
              return;
            }
            if (user.isVerified) {
              vrniJsonOdgovor(odgovor, 400, "Ta uporabnik je že bil preverjen");
              return;
            }
            // Verify and save the user
            user.verified = true;
            user.save(function (napaka) {
                if (napaka) { 
                  vrniJsonOdgovor(odgovor, 500, "Napaka pri shranjevanju uporabnika.");
                  return;
                }
                vrniJsonOdgovor(odgovor, 200, {
                  "zeton": user.generirajJwt()
                });
            });
        });
    });
};
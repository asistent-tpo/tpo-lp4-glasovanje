var mongoose = require('mongoose');
var Urnik = mongoose.model('Urnik');
var User = mongoose.model('User');

mongoose.set('useFindAndModify', false);

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

var userId = function (zahteva, odgovor, callback) {
  if (zahteva.payload && zahteva.payload._id) {
    User
      .findById(zahteva.payload._id)
      .exec(function (napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        callback(zahteva, odgovor, uporabnik._id);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      sporocilo: "Ni podatka o uporabniku"
    });
  }
};

module.exports.uporabnikovUrnik = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    Urnik.find({ uporabnik: idUporabnika }).exec(function (napaka, urniki) {
      if (napaka) {
        vrniJsonOdgovor(odgovor1, 400, { sporocilo: "neveljavna zahteva" });
        return;
      } else {
        if (urniki.length === 0) {
          // ustvari nov urnik
          noviUrnik = new Urnik();
          noviUrnik.uporabnik = idUporabnika;
          noviUrnik.vnosi = [];
          noviUrnik.save(function (napaka, doc) {
            if (napaka) {
              vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
            } else {
              vrniJsonOdgovor(odgovor, 200, doc);
            }
          });
        } else {
          vrniJsonOdgovor(odgovor1, 200, urniki[0]);
        }
      }
    })
  });
}

/**
 * @param {stirng} idUrnika
 * @param {string} naziv
 * @param {integer} danVTednu
 * @param {number} uraZacetka
 * @param {number} trajanje
 * @param {string} barva
 */
module.exports.dodajVnosVUrnik = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    if (!zahteva.body || !zahteva.body.idUrnika) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idUrnika = zahteva.body.idUrnika;
    Urnik.findById(idUrnika, function (err, urnik) {
      if (urnik === undefined || urnik.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj urnik." });
        return;
      }
      if(isNaN(zahteva.body.danVTednu) || isNaN(zahteva.body.uraZacetka) || isNaN(zahteva.body.trajanje)) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Eno od stevil ni veljavne oblike." });
        return;
      }
      urnik.vnosi.push({
        naziv: zahteva.body.naziv,
        danVTednu: zahteva.body.danVTednu,
        uraZacetka: zahteva.body.uraZacetka,
        trajanje: zahteva.body.trajanje,
        barva: zahteva.body.barva
      });
      Urnik.findOneAndUpdate({ _id: idUrnika }, { vnosi: urnik.vnosi }, function (napaka, noviUrnik) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, urnik);
        }
      });
    });
  });
}

/**
 * @param {integer} idVnosa
 * @param {string} idUrnika
 * @param {integer} trajanje
 * @param {string} naziv
 * @param {string} barva
 */
module.exports.urediVnosVUrniku = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    if (!zahteva.body || !zahteva.body.idUrnika) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idUrnika = zahteva.body.idUrnika;
    Urnik.findById(idUrnika, function (err, urnik) {
      if (urnik === undefined || urnik.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj urnik." });
        return;
      }
      if (zahteva.body.idVnosa === undefined || isNaN(zahteva.body.idVnosa) || zahteva.body.idVnosa >= urnik.vnosi.length) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljaven idVnosa" });
        return;
      }
      if(isNaN(zahteva.body.trajanje)) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Vneseno trajanje ni stevilo." });
        return;
      }
      urnik.vnosi[zahteva.body.idVnosa] = {
        naziv: zahteva.body.naziv,
        danVTednu: urnik.vnosi[zahteva.body.idVnosa].danVTednu,
        uraZacetka: urnik.vnosi[zahteva.body.idVnosa].uraZacetka,
        trajanje: zahteva.body.trajanje,
        barva: zahteva.body.barva
      };
      Urnik.findOneAndUpdate({ _id: idUrnika }, { vnosi: urnik.vnosi }, function (napaka, noviUrnik) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, urnik);
        }
      });
    });
  });
}

/**
 * @param {integer} idVnosa
 * @param {integer} idUrnika
 */
module.exports.izbrisiVnosVUrniku = function (zahteva, odgovor) {
  userId(zahteva, odgovor, function (zahteva1, odgovor1, idUporabnika) {
    if (!zahteva.body || !zahteva.body.idUrnika) {
      vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljavna zahteva." });
      return;
    }
    var idUrnika = zahteva.body.idUrnika;
    Urnik.findById(idUrnika, function (err, urnik) {
      if (urnik === undefined || urnik.uporabnik.toString() !== idUporabnika.toString()) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "To ni tvoj urnik." });
        return;
      }
      if (zahteva.body.idVnosa === undefined || isNaN(zahteva.body.idVnosa) || zahteva.body.idVnosa >= urnik.vnosi.length) {
        vrniJsonOdgovor(odgovor, 400, { sporocilo: "Neveljaven idVnosa" });
        return;
      }
      urnik.vnosi.splice(zahteva.body.idVnosa, 1);
      Urnik.findOneAndUpdate({ _id: idUrnika }, { vnosi: urnik.vnosi }, function (napaka, noviUrnik) {
        if (napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
        } else {
          vrniJsonOdgovor(odgovor, 200, urnik);
        }
      });
    });
  });
}
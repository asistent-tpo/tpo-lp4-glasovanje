var mongoose = require("mongoose");

var UserSchema = require("../models/user").userSchema;
mongoose.model("User", UserSchema);
var User = mongoose.model("User");

var MessageSchema = require("../models/AdminMessage").AdminMessageSchema;
mongoose.model("AdminMessage", MessageSchema);
var AdminMessage = mongoose.model('AdminMessage');

var nodemailer = require('nodemailer');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  if(odgovor == "b") return;
  odgovor.status(status);
  odgovor.json(vsebina);
};

var posljiEmail = function(user, odgovor, message) {
  var transporter = nodemailer.createTransport({ service: 'Sendgrid', auth: { user: process.env.SENDGRID_USER, pass: process.env.SENDGRID_PASS } });
  var mailOptions = { from: 'sistem-ne-odgovarjaj@ciste-desetke.herokuapp.com', to: user.email, subject: message.subject, text: message.text };
  transporter.sendMail(mailOptions, function (napaka) {
      if (napaka) {
        //vrniJsonOdgovor(odgovor, 500, {sporocilo: "Napaka pri posiljanju maila"}); 
        console.log("Napaka pri pošiljanju obvestil.")
        return;
      } 
      else {
        return;
      }
  
  });
};

module.exports.sendNotification = function(zahteva, odgovor) {
  var message = new AdminMessage();
  if (!zahteva.body) {
    vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Ni zahteve."});
    return;
  }
  else if (zahteva.payload && zahteva.payload._id){
      User
      .findById(zahteva.payload._id) 
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem uporabnika"});
          return -1;
        } 
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, {"sporočilo": napaka});
          return -1;
        }
        else if (uporabnik.role != "admin"){
          vrniJsonOdgovor(odgovor, 401, { sporocilo: "Samo admin lahko pošilja sporočila." });
          return -1;
        }
        else{
          if (zahteva.body) {
            message.subject = zahteva.body.title ? zahteva.body.title : "";
            message.text = zahteva.body.body ? zahteva.body.body : "";
            message.save(function (error) {
              if (error) {
                vrniJsonOdgovor(odgovor, 400, { sporocilo: "Shranjevanje ni uspelo." });
                return -1;
              }
              else {
                User.find({
                  role: { $ne: "admin"}
                }).exec(function(napaka, users){
                  if(napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                    return -1;
                  }
                  for(var i = 0; i < users.length; i++) {
                    posljiEmail(users[i], odgovor, message);
                  }
                  vrniJsonOdgovor(odgovor, 200, {});
                });
              }
            });
            return 1;
          }
        }
      });
      
      
  }
  else{
    vrniJsonOdgovor(odgovor, 401, {"sporočilo": "Pošiljatelj ni identificiran."});
    return;
  }
  
  
};
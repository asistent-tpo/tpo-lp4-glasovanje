(function() {
    function potrjevanjeCtrl($location, avtentikacija, $routeParams, $window) {
        
      var vm = this;
      
      vm.potrdi = function() {
          vm.napakaNaObrazcu = "";
         avtentikacija
            .potrdiEmail({token: $routeParams.token})
            .then(
              function(success) {
                $location.path($location.path('/'));
              },
              function(napaka) {
                vm.napakaNaObrazcu = napaka.data.sporocilo;
              }
            );
      };
      
      vm.potrdi();
      
    }

      
    
    potrjevanjeCtrl.$inject = ['$location', 'avtentikacija', '$routeParams', '$window'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('potrjevanjeCtrl', potrjevanjeCtrl);
})();
(function() {
  function spreminjanjeGeslaCtrl($scope, avtentikacija, cisteDesetkePodatki, $rootScope) {
    var regexEmail = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    var regexLen = new RegExp(/(?=.{8,})/);
    var regexLenShort = new RegExp(/(?=.{2,})/);
    var vm = this;
    
    vm.sporocilo = "Pridobivam uporabniške podatke.";
    vm.data = avtentikacija.trenutniUporabnik();
    
    if (vm.data == null) {
      vm.changeMessagePassw = "Napaka pri pridobivanju uporabniških podatkov."
    }
    else vm.changeMessagePassw = "";
    
    var userInfo = avtentikacija.trenutniUporabnik(); //email, _id, uporabniskoime, slika, role
    

    
    
    
    vm.data.updatePassword = function(old, new1, new2) {
      if(!new1 || !new2 || !old) {
        vm.napakaNaObrazcu = "Vnosi ne smejo biti prazni.";
        vm.changeMessagePassw = "";
        return;
      }
      else if(new1 != new2) {
        vm.napakaNaObrazcu = "Gesli se ne ujemata.";
        vm.changeMessagePassw = "";
        return;
      }
      else if(regexLen.test(new1) == false) {
        vm.napakaNaObrazcu = "Novo geslo je prekratko. Imeti mora vsaj 8 znakov.";
        vm.changeMessagePassw = "";
        return;
      }
      else {
        avtentikacija.prijavaSimple({'email':userInfo.email, 'password': old}).then(function success(response){
            cisteDesetkePodatki.updateUserPassword(userInfo._id, old, new1, new2).then(function(response) {
                avtentikacija.shraniZeton(response.data.zeton);
                vm.changeMessagePassw = "Uspešno spremenjeno geslo."
                vm.passwordOld = "";
                vm.passwordNew1 = "";
                vm.passwordNew2 = "";
            }, function error(response) {
              vm.napakaNaObrazcu = response.data.sporocilo;
              vm.changeMessagePassw = "";
            });
          }, function error(response) {
            vm.napakaNaObrazcu = response.data.sporocilo;
            vm.changeMessagePassw = "";
          });
        
        
        
      }
    };
    
    
  }
  
  
  spreminjanjeGeslaCtrl.$inject = ['$scope', 'avtentikacija', 'cisteDesetkePodatki', '$rootScope'];
  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('spreminjanjeGeslaCtrl', spreminjanjeGeslaCtrl);
})();
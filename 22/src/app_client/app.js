(function() {
  function nastavitev($routeProvider, $locationProvider, $sceDelegateProvider) {
    $routeProvider
      .when('/',{
        templateUrl : 'home/home.html',
        controller : 'homeCtrl',
        controllerAs : 'vm'
      })
      .when('/domov',{
        templateUrl : '/domacaStran/domacaStran.pogled.html',
        controller : 'domacaStranCtrl',
        controllerAs : 'vm'
      })
      .when('/login', {
        templateUrl: '/avtentikacija/avtentikacija.pogled.html',
        controller: 'avtentikacijaCtrl',
        controllerAs: 'vm'
      })
      .when('/confirmation/:token', {
        templateUrl: '/potrjevanjeEmaila/potrjevanjeEmaila.pogled.html',
        controller: 'potrjevanjeCtrl',
        controllerAs: 'vm'
      })
      .when('/db', {
        templateUrl: '/database/database.pogled.html',
        controller: 'databaseCtrl',
        controllerAs: 'vm'
      })
      .when('/changePassword', {
        templateUrl: '/spreminjanjeGesla/spreminjanjeGesla.pogled.html',
        controller: 'spreminjanjeGeslaCtrl',
        controllerAs: 'vm'
      })
      .when('/sendNotification', {
        templateUrl: '/adminObvestila/adminObvestila.pogled.html',
        controller: 'adminObvestilaCtrl',
        controllerAs: 'vm'
      })
      .when('/events', {
        templateUrl: '/dogodki/dogodki.pogled.html',
        controller: 'dogodkiCtrl',
        controllerAs: 'vm'
      })
      .when('/eventmanager', {
        templateUrl: '/dogodki/eventmanager.pogled.html',
        controller: 'dogodkiCtrl',
        controllerAs: 'vm'
      })
      .when('/bus', {
        templateUrl: '/avtobusi/avtobusi.pogled.html',
        controller: 'avtobusiCtrl',
        controllerAs: 'vm'
      })
      .when('/bon', {
        templateUrl: '/boni/boni.pogled.html',
        controller: 'boniCtrl',
        controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/'});
      
      $locationProvider.html5Mode(true);

  }
  /* global angular */
  angular
    .module('ciste-desetke', ['ngRoute', 'ui.bootstrap', 'colorpicker.module', 'angular-fullcalendar']) 
    .config(['$routeProvider','$locationProvider', '$sceDelegateProvider', nastavitev]);
})();

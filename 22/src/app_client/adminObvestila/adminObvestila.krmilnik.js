(function() {
    function adminObvestilaCtrl($location, cisteDesetkePodatki, $routeParams, $window) {
        
      var vm = this;
      vm.notification = {
          title: "",
          body: ""
      };
      
      vm.posiljanjeSistemskihSporocil = function() {
          vm.napakaNaObrazcu = "";
          if (!vm.notification.title | !vm.notification.body) {
              vm.napakaNaObrazcu = "Vnesite vse podatke.";
          } else {
              cisteDesetkePodatki
                .posljiSistemskoSporocilo(vm.notification)
                .then(
                  function(success) {
                    vm.notification = {
                      title: "",
                      body: ""
                    };
                    vm.sporocilo = "Obvestilo je bilo uspešno poslano."
                    
                  },
                  function(napaka) {
                    vm.napakaNaObrazcu = "Napaka pri pošiljanju.";
                  }
                );
          }
         
      };
    }

      
    
    adminObvestilaCtrl.$inject = ['$location', 'cisteDesetkePodatki', '$routeParams', '$window'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('adminObvestilaCtrl', adminObvestilaCtrl);
})();
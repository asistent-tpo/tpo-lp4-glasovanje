(function() {
  // function navigacijaCtrl($location, avtentikacija, $route, $window,$scope, $rootScope) {
  function navigacijaCtrl($location, avtentikacija, $route, $window,$scope, $rootScope) {
    var navvm = this;
    
    // navvm.trenutnaLokacija = $location.path();
    
    navvm.jePrijavljen = avtentikacija.jePrijavljen();
    
    navvm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    navvm.jeAdmin = avtentikacija.jeAdmin();
    
    navvm.jeEventManager = avtentikacija.jeEventManager();
    
    navvm.jePrijavljenStudent = avtentikacija.jeStudent();
    
    navvm.jeStudentskiUporabnik = !avtentikacija.jePrijavljen() | avtentikacija.jeStudent();
    
    // $scope.$on('usnChange', function (event, data) {
    //   navvm.trenutniUporabnik.uporabniskoime = data;
    // }); 
    
    

    navvm.odjava = function() {
      avtentikacija.odjava();
      $location.path('/');
      $route.reload();
    };
    

    
    
  }
  navigacijaCtrl.$inject = ['$location', 'avtentikacija', '$route', '$window','$scope', '$rootScope'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('navigacijaCtrl', navigacijaCtrl);
})();
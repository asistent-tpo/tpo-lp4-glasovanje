(function() {
    
    function cisteDesetkePodatki($http,avtentikacija){
        
        var init = function() {
            return $http.get('/api/v1/init');
        };
        
        var empty = function() {
            return $http.get('/api/v1/empty');
        };
        
        var updateUserPassword = function(idu, oldp, newp1, newp2) {
            var data = { 'passwordOld' : oldp, 'password' : newp1, 'passwordCompare' : newp2 , 'id' : idu };
            return $http.put('/api/v1/users', data, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
                }
            });
        };
        
        var posljiSistemskoSporocilo = function(notification) {
            return $http.post('/api/v1/notification', notification, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
                }
            });
        };

        var pridobiTodoSezname = function() {
            return $http.get('/api/v1/todoElementi', {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
                }
            });
        };

        var pridobiKoledar = function() {
          return $http.get('/api/v1/koledar', {
              headers: {
                Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
              }
          });
      };

        var pridobiUrnik = function() {
          return $http.get('/api/v1/urnik', {
              headers: {
                Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
              }
          });
        };
        
        
        var pridobiVsaPostajalisca = function() {
          return $http.get('/api/v1/postajalisca');
        };
        
        var pridobiIzbranoPostajalisce = function(name) {
          return $http.get('/api/v1/postajalisca/' + name);
        };
        
        var pridobiPrihode = function(id) {
            return $http.get('/api/v1/prihodi/' + id);
        };
        
        
        var pridobiKoordinate = function(naslov) {
            return $http.get('/api/v1/lokacije/' + naslov);
        };
        
        var pridobiVseRestavracije = function() {
          return $http.get('/api/v1/restavracije');
        };
        
        
        var pridobiVseDogodke = function() {
            return $http.get('/api/v1/events', {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
                }
            });
        };
        
        var posljiNovDogodek = function(event) {
            return $http.post('/api/v1/events', event, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.vrniZeton() 
                }
            });
        };
        
        return {
          init: init,
          empty: empty,
          updateUserPassword: updateUserPassword,
          posljiSistemskoSporocilo: posljiSistemskoSporocilo,
          pridobiVseDogodke: pridobiVseDogodke,
          pridobiTodoSezname: pridobiTodoSezname,
          pridobiVsaPostajalisca: pridobiVsaPostajalisca,
          pridobiIzbranoPostajalisce: pridobiIzbranoPostajalisce,
          pridobiPrihode: pridobiPrihode,
          posljiNovDogodek: posljiNovDogodek,
          pridobiUrnik: pridobiUrnik,
          pridobiKoledar: pridobiKoledar,
          pridobiKoordinate: pridobiKoordinate,
          pridobiVseRestavracije: pridobiVseRestavracije
        };
    }
    
    cisteDesetkePodatki.$inject = ['$http','avtentikacija'];
    /*global angular*/
    angular
        .module('ciste-desetke')
        .service('cisteDesetkePodatki', cisteDesetkePodatki);
})();
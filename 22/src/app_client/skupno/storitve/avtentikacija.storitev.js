(function() {
  function avtentikacija($window, $http) {
    var b64Utf8 = function (niz) {
      return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
    };
    
    var shraniZeton = function(zeton) {
      $window.localStorage['ciste-desetke-zeton'] = zeton;
    };
    
    var vrniZeton = function() {
     return $window.localStorage['ciste-desetke-zeton'];
    };
    var registracija = function(uporabnik) {
      return $http.post('/api/v1/register', uporabnik).then(
        function success(odgovor) {
          // shraniZeton(odgovor.data.zeton);
        });
    };
  
    var prijava = function(uporabnik) {
      return $http.post('/api/v1/login', uporabnik).then(
        function success(odgovor) {
          shraniZeton(odgovor.data.zeton);
        });
    };
    
    var potrdiEmail = function(uporabnik) {
      return $http.post('/api/v1/confirmation', uporabnik).then (
        function success(odgovor){
          shraniZeton(odgovor.data.zeton);
        });
    };
  
    var prijavaSimple = function(uporabnik) {
      return $http.post('/api/v1/login', uporabnik);
    };
    
    var odjava = function() {
      $window.localStorage.removeItem('ciste-desetke-zeton');
    };
    
    var jePrijavljen = function() {
      var zeton = vrniZeton();
      if (zeton) {
        var koristnaVsebina = JSON.parse(b64Utf8 (zeton.split('.')[1]));
        return koristnaVsebina.datumPoteka > Date.now() / 1000;
      } else {
        return false;
      }
    };
    
    var trenutniUporabnik = function() {
      if (jePrijavljen()) {
        var zeton = vrniZeton();
        var koristnaVsebina = JSON.parse(b64Utf8 (zeton.split('.')[1]));
        return {
          email: koristnaVsebina.email,
          _id: koristnaVsebina._id,
          role: koristnaVsebina.role
        };
      }
    };
    
    var jeAdmin = function() {
      var zeton = vrniZeton();
      if (zeton) {
        var koristnaVsebina = JSON.parse(b64Utf8 (zeton.split('.')[1]));
        return koristnaVsebina.role === "admin";
      } else {
        return false;
      }
    }
    
    var jeEventManager = function() {
      var zeton = vrniZeton();
      if (zeton) {
        var koristnaVsebina = JSON.parse(b64Utf8 (zeton.split('.')[1]));
        return koristnaVsebina.role === "eventManager";
      } else {
        return false;
      }
    }
    
    var jeStudent = function() {
      var zeton = vrniZeton();
      if (zeton) {
        var koristnaVsebina = JSON.parse(b64Utf8 (zeton.split('.')[1]));
        return koristnaVsebina.role === "user";
      } else {
        return false;
      }
    }
    
    return {
      shraniZeton: shraniZeton,
      vrniZeton: vrniZeton,
      registracija: registracija,
      prijava: prijava,
      odjava: odjava,
      jePrijavljen: jePrijavljen,
      trenutniUporabnik: trenutniUporabnik, 
      jeAdmin: jeAdmin, 
      jeEventManager: jeEventManager,
      jeStudent: jeStudent,
      prijavaSimple: prijavaSimple, 
      potrdiEmail: potrdiEmail,
    };
  }
  avtentikacija.$inject = ['$window', '$http'];
  
  /* global angular */
  angular
    .module('ciste-desetke')
    .service('avtentikacija', avtentikacija);
})();
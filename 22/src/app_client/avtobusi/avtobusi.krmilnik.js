(function() {
    function avtobusiCtrl($scope, $location, $anchorScroll, cisteDesetkePodatki) {
      var vm = this;
      
      vm.sporocilo = "";
      vm.napaka = "";
      
      vm.izbranoPostajalisce = {
        id: null,
        name: "",
        center: false
      };
      
      cisteDesetkePodatki.pridobiVsaPostajalisca().then(
        function success(odgovor) {
          vm.napaka = odgovor.data.length > 0 ? "" : "Ne najdem postajališč.";
          vm.data = {
            postajalisca: odgovor.data
          };
        },
        function error(odgovor) {
          vm.napaka = odgovor.data.length > 0 ? "" : "Prišlo je do napake!";
          console.log(odgovor.e);
        }
      );
      
      vm.vpisano = "";
      
      $scope.search = function() {
        cisteDesetkePodatki.pridobiIzbranoPostajalisce(vm.vpisano).then(
          function success(odgovor) {
            vm.napaka = odgovor.data.length > 0 ? "" : "Ne najdem postajališča s tem imenom.";
            vm.data = {
              postajalisca: odgovor.data
            };
          },
          function error(odgovor) {
            vm.napaka = odgovor.data.length > 0 ? "" : "Prišlo je do napake!";
            console.log(odgovor.e);
          }
        );
      };
      
      vm.avtobusi = [
        {
          direction: "",
          number: null,
          arrivals: ""
        }];
      
      vm.izberiPostajalisce = function(postajalisce) {
        vm.izbranoPostajalisce = postajalisce;
        
        vm.sporocilo = "Pridobivam prihode ...";
        
        vm.avtobusi = [
        {
          direction: "",
          number: null,
          arrivals: ""
        }];
        
        cisteDesetkePodatki.pridobiPrihode(vm.izbranoPostajalisce.id).then(
          function success(odgovor) {
            razcleniOdgovor(odgovor.data);
            vm.sporocilo = "";
            var id = $location.hash();
            $location.hash('bottom');
            $anchorScroll();
            $location.hash(id);
          },
          function error(odgovor) {
            console.log(odgovor.e);
          }
        );
        
      };
      
      var razcleniOdgovor = function(odgovor) {
        var odgovorJson = JSON.parse(odgovor);
        var avtobusiJson = odgovorJson.stations[0].buses;
        for (var busJson in avtobusiJson) {
          var arrivals = JSON.stringify(avtobusiJson[busJson].arrivals);
          arrivals = arrivals.replace("[", "");
          arrivals = arrivals.replace("]", "'");
          arrivals = arrivals.replace(/,/g, "', ");
          if (arrivals == "'") arrivals = ">90'";
          var bus = {
            direction: avtobusiJson[busJson].direction,
            number: avtobusiJson[busJson].number,
            arrivals: arrivals
          };
          vm.avtobusi.push(bus);
        }
      };
        
    }
    avtobusiCtrl.$inject = ['$scope', '$location', '$anchorScroll', 'cisteDesetkePodatki'];
    
    /* global angular */
    angular
      .module('ciste-desetke')
      .run(['$anchorScroll', function($anchorScroll) {
        $anchorScroll.yOffset = 70;
      }])
      .controller('avtobusiCtrl', avtobusiCtrl);
})();
(function() {
    function boniCtrl($scope, cisteDesetkePodatki) {
      var vm = this;
      
      vm.sporocilo = "";
      
      // zacetna trenutna lokacija (center Ljubljane)
      vm.latitude = 46.0569465;
      vm.longitude = 14.5057515;
      
      var pridobiSeznamRestavracij = function(sortiraj) {
        cisteDesetkePodatki.pridobiVseRestavracije().then(
          function success(odgovor) {
            vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem restavracij.";
            vm.data = {
              restavracije: odgovor.data
            };
            initMarkers();
            if(sortiraj) {
              sortirajRestavracije();
            }
          },
          function error(odgovor) {
            vm.sporocilo = odgovor.data.length > 0 ? "" : "Prišlo je do napake!";
            console.log(odgovor.e);
          }
        );
      };
      
      vm.izbranaRestavracija = {
        lat: "",
      	lon: "",
      	naslov: "",
      	cena: "",
      	doplacilo: "",
      	posid: "",
      	lokal: "",
      	city: ""
      };
      
      vm.izberiRestavracijo = function(restavracija) {
        vm.izbranaRestavracija = restavracija;
      };
      
      vm.karta = function(restavracija) {
        $scope.map.setCenter(new google.maps.LatLng(restavracija.lat, restavracija.lon));
        $scope.map.setZoom(15);
        var selectedMarker = null;
        for (var m in $scope.markers) {
          if (vm.izbranaRestavracija.posid == $scope.markers[m].id) {
            selectedMarker = $scope.markers[m];
            break;
          }
        }
        if (selectedMarker == null) {
          console.log("null");
        } else {
          google.maps.event.trigger(selectedMarker, 'click');
        }
      };
      
      /*global navigator*/
      /*global google*/
      vm.pridobiGeolokacijo = function() {
        if (navigator.geolocation) {
          vm.sporocilo = "Pridobivamo vašo trenutno lokacijo ...";
          navigator.geolocation.getCurrentPosition(
            function (p) {
              vm.latitude = p.coords.latitude;
              vm.longitude = p.coords.longitude;
              $scope.map.setCenter(new google.maps.LatLng(vm.latitude, vm.longitude));
              $scope.map.setZoom(14);
              pridobiSeznamRestavracij(true);
            },
            function (e) {
              pridobiSeznamRestavracij(false);
            });
        } else {
          alert('Ta brskalnik ne podpira geolokacije.');
        }
      };
      vm.pridobiGeolokacijo();
      
      vm.vpisano = "";
      vm.pridobiLokacijo = function() {
        vm.sporocilo = "";
        var naslov = vm.vpisano;
        naslov = naslov.replace(/č/gi, "c");
        naslov = naslov.replace(/š/gi, "s");
        naslov = naslov.replace(/ž/gi, "z");
        cisteDesetkePodatki.pridobiKoordinate(naslov).then(
          function success(odgovor) {
            razcleniOdgovor(odgovor.data);
          },
          function error(odgovor) {
            console.log(odgovor.e);
          }
        );
      };
      
      var razcleniOdgovor = function(odgovor) {
        var odgovorJson = JSON.parse(odgovor);
        if (odgovorJson.results === undefined || odgovorJson.results.length == 0) {
          vm.sporocilo = "Tega v Sloveniji ne najdemo. Preverite črkovanje!";
        } else {
          vm.latitude = odgovorJson.results[0].geometry.location.lat;
          vm.longitude = odgovorJson.results[0].geometry.location.lng;
          $scope.map.setCenter(new google.maps.LatLng(vm.latitude, vm.longitude));
          $scope.map.setZoom(14);
          pridobiSeznamRestavracij(true);
        }
      };
      
      var sortirajRestavracije = function() {
        var origin = new google.maps.LatLng(vm.latitude, vm.longitude);
        for (var r in vm.data.restavracije) {
          var destination = new google.maps.LatLng(vm.data.restavracije[r].lat, vm.data.restavracije[r].lon);
          var distance = google.maps.geometry.spherical.computeDistanceBetween (origin, destination);
          vm.data.restavracije[r].razdalja = distance/1000;
        }
        vm.data.restavracije.sort(function(a, b) {
          return a.razdalja - b.razdalja;
        });
      };
      
      var mapOptions = {
          zoom: 14,
          center: new google.maps.LatLng(vm.latitude, vm.longitude),
      };
      $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
      $scope.markers = [];
      
      var infoWindow = new google.maps.InfoWindow();
      
      var createMarker = function (restavracija) {
        var marker = new google.maps.Marker({
          map: $scope.map,
          position: new google.maps.LatLng(restavracija.lat, restavracija.lon),
          title: restavracija.lokal,
          id: restavracija.posid
        });
        marker.content = '<div>';
        marker.content += '<h6 class="info-naslov">' + restavracija.naslov + '</h6>';
        marker.content += '<p> Doplačilo: ' + restavracija.doplacilo + '€ &nbsp;';
        marker.content += '(<a href="https://www.studentska-prehrana.si/sl/restaurant/Details/' + restavracija.posid + '" target="_blank">meni</a>)';
        marker.content += '</p></div>';
        
        google.maps.event.addListener(marker, 'click', function(){
            infoWindow.setContent('<h5 class="info-title">' + marker.title + '</h5>' + marker.content);
            infoWindow.open($scope.map, marker);
        });
        
        $scope.markers.push(marker);
      };
      
      var initMarkers = function() {
        for (var r in vm.data.restavracije){
          createMarker(vm.data.restavracije[r]);
        }
        
        $scope.openInfoWindow = function(e, selectedMarker){
          e.preventDefault();
          google.maps.event.trigger(selectedMarker, 'click');
        };
      };
    
    }
    
    boniCtrl.$inject = ['$scope', 'cisteDesetkePodatki'];
    
    /* global angular */
    angular
      .module('ciste-desetke')
      .controller('boniCtrl', boniCtrl);
})();
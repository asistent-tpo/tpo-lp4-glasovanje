(function() {
    function avtentikacijaCtrl($location, avtentikacija) {
      var vm = this;
    
    
      vm.regPodatki = {
        email: "",
        password1: "",
        password2: ""
      };
    
      vm.prvotnaStran = $location.search().stran || '/';
      
      var regexEmail = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    
      vm.posiljanjePodatkovRegistracija = function() {
        vm.napakaNaObrazcu2 = "";
        if (!vm.regPodatki.email || !vm.regPodatki.password1 || !vm.regPodatki.password2) {
          vm.napakaNaObrazcu2 = "Zahtevani so vsi podatki, prosim poskusite znova!";
          return false;
        } else if (vm.regPodatki.password1 != vm.regPodatki.password2) {
          vm.napakaNaObrazcu2 = "Gesli se ne ujemata";
          return false;
        } else if (vm.regPodatki.password1.length < 8) {
          vm.napakaNaObrazcu2 = "Geslo mora biti dolgo vsaj 8 znakov.";
          return false;
        }  else if(regexEmail.test(vm.regPodatki.email) == false) {
          vm.napakaNaObrazcu2 = "Vnesite veljaven email.";
          return false;
        }
        else {
          vm.izvediRegistracijo();
        }
      };
    
      vm.izvediRegistracijo = function() {
        vm.napakaNaObrazcu2 = "";
        avtentikacija
          .registracija(vm.regPodatki)
          .then(
            function(success) {
              
              vm.potrdiEmail = "Pravkar smo vam poslali e-pošto za potrditev vašega e-mail naslova. Morda je v vsiljeni pošti.";
              // $location.search('stran', null);
              // $location.path(vm.prvotnaStran);
            },
            function(napaka) {
              vm.napakaNaObrazcu2 = napaka.data.sporocilo;
            }
          );
      };
      
    vm.prijavniPodatki = {
      email: "",
      password: ""
    };


    vm.posiljanjePodatkovPrijava = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.email || !vm.prijavniPodatki.password) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!" + vm.prijavniPodatki.email + "1";
        return false;
      }else if (vm.prijavniPodatki.password.length < 8) {
          vm.napakaNaObrazcu = "Geslo mora biti dolgo vsaj 8 znakov";
          return false;
      } else {
        vm.izvediPrijavo();
      }
    };

    vm.izvediPrijavo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .prijava(vm.prijavniPodatki)
        .then(
          function(success) {
            $location.search('stran', null);
            $location.path(vm.prvotnaStran);
          },
          function(napaka) {
            vm.napakaNaObrazcu = napaka.data.sporocilo;
          }
        );
    };

      
    }
    avtentikacijaCtrl.$inject = ['$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('avtentikacijaCtrl', avtentikacijaCtrl);
})();
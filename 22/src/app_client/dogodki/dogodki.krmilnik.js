(function() {
    function dogodkiCtrl($location, cisteDesetkePodatki, $routeParams, $window, avtentikacija) {
      
      var vm = this;
      
      vm.jePrijavljenStudent = avtentikacija.jeStudent();
      
      vm.event = {
          ime: "",
          opis: "",
          datum: "",
          organizator: ""
      };
      
      var now =  new Date()
      now = [now.getUTCDate(), now.getUTCMonth()+1, now.getUTCFullYear()]
      
      cisteDesetkePodatki.pridobiVseDogodke().then(function success(response) {
          var filtered = response.data;
          vm.dogodki = new Array()
          for(var i = 0; i < filtered.length; i++) {
            var dateList = filtered[i].date
            .split('T')[0]
            .split('-');
            filtered[i].day =  parseInt(dateList[2]);
            filtered[i].month = parseInt(dateList[1]);
            filtered[i].year = parseInt(dateList[0]);
            if(filtered[i].year >= now[2] &&
            ((filtered[i].month > now[1]) || (filtered[i].month == now[1] && filtered[i].day >= now[0])) 
            ) {
              vm.dogodki.push(filtered[i])
            }
          }
      }, function error(response) {
          vm.sporocilo = "Prišlo je do napake pri prikazu dogodkov!";
      });
        
      
      
      vm.objaviDogodek = function () {
          vm.event.datum.setDate(vm.event.datum.getDate() + 1);
          cisteDesetkePodatki.posljiNovDogodek(vm.event).then(function success(response) {
            vm.sporocilo = response.data.sporocilo;
            vm.event = {
                ime: "",
                opis: "",
                datum: "",
                organizator: ""
            };
            cisteDesetkePodatki.pridobiVseDogodke().then(function success(response) {
            var filtered = response.data;
            vm.dogodki = new Array()
            for(var i = 0; i < filtered.length; i++) {
              var dateList = filtered[i].date
              .split('T')[0]
              .split('-');
              filtered[i].day =  parseInt(dateList[2]);
              filtered[i].month = parseInt(dateList[1]);
              filtered[i].year = parseInt(dateList[0]);
              if(filtered[i].year >= now[2] &&
              ((filtered[i].month > now[1]) || (filtered[i].month == now[1] && filtered[i].day >= now[0])) 
              ) {
                vm.dogodki.push(filtered[i])
              }
            }
            }, function error(response) {
                vm.sporocilo = "Prišlo je do napake pri prikazu dogodkov!";
            });
          }, function error(response) {
              vm.sporocilo = response.data.sporocilo;
          });
          
      };
      
    }

      
    
    dogodkiCtrl.$inject = ['$location', 'cisteDesetkePodatki', '$routeParams', '$window', 'avtentikacija'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('dogodkiCtrl', dogodkiCtrl);
})();
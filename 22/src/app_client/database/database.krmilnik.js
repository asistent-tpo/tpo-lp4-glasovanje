(function() {
  function databaseCtrl(cisteDesetkePodatki, avtentikacija, $route) {
    var vm = this;
    vm.sporocilo = "";
    
    vm.init = function() {
        cisteDesetkePodatki.init().then(
          function success(odgovor) {
            vm.sporocilo = "Baza uspešno napolnjena";
          }, function error(odgovor) {
            vm.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
          }
        );
    }
    
    vm.empty = function() {
        cisteDesetkePodatki.empty().then(
          function success(odgovor) {
            vm.sporocilo = "Baza uspešno pobrisana";
            avtentikacija.odjava();
            
          }, function error(odgovor) {
            vm.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
          }
        );
    }
    
    
  }
  databaseCtrl.$inject = ['cisteDesetkePodatki', 'avtentikacija', '$route'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('databaseCtrl', databaseCtrl);
})();
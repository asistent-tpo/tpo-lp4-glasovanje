(function () {
  function domacaStranCtrl($http, $scope, cisteDesetkePodatki, avtentikacija) {
    var vm = this;
    // vm.jeStudentskiUporabnik = !avtentikacija.jePrijavljen() | avtentikacija.jeStudent();
    vm.jePrijavljenStudent = avtentikacija.jeStudent();

    var potrdiBrisanje = function(sporocilo, potrditev) {
      swal({
        title: "Opozorilo",
        text: sporocilo,
        icon: "",
        buttons: true,
        dangerMode: true,
        buttons: {
          preklici:{
            text: "Prekliči",
            className: "btn-primary"
          },
          potrdi: {
            text: "Potrdi",
            className: "btn-danger"
          },
        },
      }).then(function(gumb) {
        if (gumb === "potrdi") {
          potrditev();
        }
      });
    };

    vm.resetirajSporocilo = function() {
      vm.sporocilo = "";
    }

    vm.dodajElementPodatki = { opravilo: '', opis: '', idElementa: -1 };

    vm.dodajSeznam = {ime: ''};

    vm.pripraviDodajanjeSeznama = function() {
      vm.dodajSeznam = {ime: ''};
    }

    vm.dodajSeznamFunkcija = function() {
      var podatki = {
        nazivSeznama: vm.dodajSeznam.ime
      };
      $http.post('/api/v1/todoElementi', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        cisteDesetkePodatki.pridobiTodoSezname().then(function (response) {
          vm.seznami = response.data;
        }, function error(response) {
          console.log(response);
          if (response.data.sporocilo && response.data.sporocilo.length > 0) {
            vm.sporocilo = response.data.sporocilo;
          } else if (response.data.sporočilo) {
            vm.sporocilo = response.data.sporočilo;
          }
        });
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    }

    vm.izbrisiSeznam = function(idSeznama) {
      potrdiBrisanje("Ali res želiš izbrisati seznam?", function() {
        var podatki = {
          idSeznama: idSeznama
        };
        $http.put('/api/v1/todoElementi/izbrisi', podatki, {
          headers: {
            Authorization: 'Bearer ' + avtentikacija.vrniZeton()
          }
        }).then(function (odziv) {
          cisteDesetkePodatki.pridobiTodoSezname().then(function (response) {
            vm.seznami = response.data;
          }, function error(response) {
            console.log(response);
            if (response.data.sporocilo && response.data.sporocilo.length > 0) {
              vm.sporocilo = response.data.sporocilo;
            } else if (response.data.sporočilo) {
              vm.sporocilo = response.data.sporočilo;
            }
          });
        });
      });
    }

    vm.dodajElement = function (idSeznama) {
      if (vm.dodajElementPodatki.idElementa != -1) {
        vm.urediElement(idSeznama, vm.dodajElementPodatki.idElementa);
        return;
      }
      var podatki = {
        idSeznama: idSeznama,
        naziv: vm.dodajElementPodatki.opravilo,
        opis: vm.dodajElementPodatki.opis
      };
      console.log(podatki);
      $http.post('/api/v1/todoElementi/element', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        for (var i = 0; i < vm.seznami.length; i++) {
          if (vm.seznami[i]._id == idSeznama) {
            vm.seznami[i].elementi.push({
              naziv: vm.dodajElementPodatki.opravilo,
              opis: vm.dodajElementPodatki.opis,
              koncano: false
            });
          }
        }
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    };

    vm.pripraviZaUrejanje = function (idElementa, opravilo, opis) {
      console.log(idElementa, opravilo, opis)
      vm.dodajElementPodatki.idElementa = idElementa;
      vm.dodajElementPodatki.opravilo = opravilo;
      vm.dodajElementPodatki.opis = opis;
    };

    vm.pripraviZaDodajanje = function () {
      vm.dodajElementPodatki.idElementa = -1;
      vm.dodajElementPodatki.opravilo = "";
      vm.dodajElementPodatki.opis = "";
    };

    vm.izbrisiElement = function (idSeznama, idElementa) {
      var podatki = {
        idSeznama: idSeznama,
        idElementa: idElementa
      };
      potrdiBrisanje("Ali res želiš izbrisati element?", function() {
        $http.put('/api/v1/todoElementi/element/izbrisi', podatki, {
          headers: {
            Authorization: 'Bearer ' + avtentikacija.vrniZeton()
          }
        }).then(function (odziv) {
          for (var i = 0; i < vm.seznami.length; i++) {
            if (vm.seznami[i]._id == idSeznama) {
              vm.seznami[i].elementi.splice(idElementa, 1);
            }
          }
        }, function error(response) {
          console.log(response);
          if (response.data.sporocilo && response.data.sporocilo.length > 0) {
            vm.sporocilo = response.data.sporocilo;
          } else if (response.data.sporočilo) {
            vm.sporocilo = response.data.sporočilo;
          }
        });
      });
    }

    vm.urediElement = function (idSeznama, idElementa) {
      var podatki = {
        idSeznama: idSeznama,
        idElementa: idElementa,
        naziv: vm.dodajElementPodatki.opravilo,
        opis: vm.dodajElementPodatki.opis
      };
      console.log(podatki);
      $http.put('/api/v1/todoElementi/element', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        for (var i = 0; i < vm.seznami.length; i++) {
          if (vm.seznami[i]._id == idSeznama) {
            var koncano = vm.seznami[i].elementi[idElementa].koncano;
            vm.seznami[i].elementi[idElementa] = {
              naziv: vm.dodajElementPodatki.opravilo,
              opis: vm.dodajElementPodatki.opis,
              koncano: koncano
            };
          }
        }
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    };

    vm.nalaganjeTodo = true;
    cisteDesetkePodatki.pridobiTodoSezname().then(function (response) {
      vm.seznami = response.data;
      vm.nalaganjeTodo = false;
    }, function error(response) {
      console.log(response);
      vm.sporocilo = response.data.sporocilo;
      vm.nalaganjeTodo = false;
    });

    // URNIK

    vm.dodajElementNaUrnik = {
      idUrnika: -1,
      ime: "",
      dan: "0",
      ura: "7",
      trajanje: 1,
      barva: '#95d6e2'
    };

    vm.pripraviDodajanjePredmeta = function(dan, ura) {
      var predmet = vm.predmetObTejUri(dan, ura, vm.urnik);
      if (predmet !== null) {
        pripraviUrejanjePredmeta(vm.urnik.indexOf(predmet), predmet);
        return;
      }
      vm.dodajElementNaUrnik = {
        idUrnika: vm.dodajElementNaUrnik.idUrnika,
        ime: "",
        dan: dan.toString(),
        ura: ura.toString(),
        trajanje: 1,
        barva: '#95d6e2'
      };
    }

    pripraviUrejanjePredmeta = function(idPredmeta, predmet) {
      vm.dodajElementNaUrnik = {
        idUrnika: vm.dodajElementNaUrnik.idUrnika,
        idPredmeta: idPredmeta,
        ime: predmet.naziv,
        trajanje: predmet.trajanje,
        barva: predmet.barva
      };
    }

    vm.dodajPredmetNaUrnik = function () {
      var podatki = {
        idUrnika: vm.dodajElementNaUrnik.idUrnika,
        naziv: vm.dodajElementNaUrnik.ime,
        danVTednu: vm.dodajElementNaUrnik.dan,
        uraZacetka: vm.dodajElementNaUrnik.ura,
        trajanje: vm.dodajElementNaUrnik.trajanje,
        barva: vm.dodajElementNaUrnik.barva
      };
      $http.post('/api/v1/urnik', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        vm.dodajElementNaUrnik.idUrnika = odziv.data._id;
        vm.urnik = odziv.data.vnosi;
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    };

    vm.izbrisiPredmetNaUrniku = function () {
      var podatki = {
        idUrnika: vm.dodajElementNaUrnik.idUrnika,
        idVnosa: vm.dodajElementNaUrnik.idPredmeta,
      };
      potrdiBrisanje ("Ali res želiš izbrisati predmet?", function() {
        $http.put('/api/v1/urnik/izbrisi', podatki, {
          headers: {
            Authorization: 'Bearer ' + avtentikacija.vrniZeton()
          }
        }).then(function (odziv) {
          vm.dodajElementNaUrnik.idUrnika = odziv.data._id;
          vm.urnik = odziv.data.vnosi;
        }, function error(response) {
          console.log(response);
          if (response.data.sporocilo && response.data.sporocilo.length > 0) {
            vm.sporocilo = response.data.sporocilo;
          } else if (response.data.sporočilo) {
            vm.sporocilo = response.data.sporočilo;
          }
        });
      });
    };

    vm.urediPredmetNaUrniku = function () {
      var podatki = {
        idUrnika: vm.dodajElementNaUrnik.idUrnika,
        idVnosa: vm.dodajElementNaUrnik.idPredmeta,
        naziv: vm.dodajElementNaUrnik.ime,
        trajanje: vm.dodajElementNaUrnik.trajanje,
        barva: vm.dodajElementNaUrnik.barva
      };
      $http.put('/api/v1/urnik', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        vm.dodajElementNaUrnik.idUrnika = odziv.data._id;
        vm.urnik = odziv.data.vnosi;
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    };

    vm.predmetObTejUri = function(dan, ura, urnik) {
      if (urnik === undefined)
        return null;
      for (var i = 0; i<urnik.length; i++) {
        if (urnik[i].danVTednu === dan) {
          if (ura >= urnik[i].uraZacetka && ura < urnik[i].uraZacetka + urnik[i].trajanje) {
            return urnik[i];
          }
        }
      }
      return null;
    }

    vm.nalaganjeUrnik = true;
    cisteDesetkePodatki.pridobiUrnik().then(function (response) {
      vm.dodajElementNaUrnik.idUrnika = response.data._id;
      vm.urnik = response.data.vnosi;
      vm.nalaganjeUrnik = false;
    }, function error(response) {
      console.log(response);
      vm.nalaganjeUrnik = false;
    })


    // koledar

    vm.options = {
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      themeSystem: 'bootstrap3',
      firstDay: 1,
      locale: 'sl',
      buttonText: {
        today: 'Danes',
        month: 'Mesec',
        week : 'Teden',
        day  : 'Dan'
      },
      dayClick: function (date, allDay, jsEvent, view) {
        console.log(date);
        vm.pripraviDodajanjeNaKoledar(date._d);
        $('#dodajNaKoledar').modal('show');
      },
      eventClick: function(info) {
        vm.pripraviUrejanjeNaKoledarju(info.identifikator);
        $('#dodajNaKoledar').modal('show');
      }    
    };

    vm.events = [];
    vm.dogodkiNaKoledarju = [];

    vm.dodajElementNaKoledar = {
      idDogodkaNaKoledarju: -1,
      idKoledarja: -1,
      naziv: '',
      opis: '',
      datum: '',
      trajanje: '',
      uraZacetka: ''
    }

    prikaziDogodke = function(dogodki) {
      var noviEventi = [];
      for(var i = 0; i < dogodki.length; i++) {
        var konec = new Date();
        konec.setTime(new Date(dogodki[i].datum).getTime() +  dogodki[i].trajanje * 3600000)
        noviEventi.push({
          title: dogodki[i].naziv,
          description: dogodki[i].opis,
          start: new Date(dogodki[i].datum),
          end: konec,
          color: '#3c8dbc',
          identifikator: i
        });
      }
      vm.events = noviEventi;
    };

    vm.nalaganjeKoledar = true;
    cisteDesetkePodatki.pridobiKoledar().then(function (response) {
      console.log(response);
      vm.dogodkiNaKoledarju = response.data.elementi;
      vm.dodajElementNaKoledar.idKoledarja = response.data._id;
      prikaziDogodke(response.data.elementi);
      vm.nalaganjeKoledar = false;
    }, function error(response) {
      if (response.data.sporocilo && response.data.sporocilo.length > 0) {
        vm.sporocilo = response.data.sporocilo;
      } else if (response.data.sporočilo) {
        vm.sporocilo = response.data.sporočilo;
      }
      vm.nalaganjeKoledar = false;
      console.log(response);
    })

    vm.pripraviDodajanjeNaKoledar = function(datum) {
      console.log(datum.toString());
      $scope.$apply(function () {
        var datumString = datum.toString();
        vm.dodajElementNaKoledar.idDogodkaNaKoledarju = -1;
        vm.dodajElementNaKoledar.naziv = '';
        vm.dodajElementNaKoledar.opis = '';
        vm.dodajElementNaKoledar.datum = datumString;
        vm.dodajElementNaKoledar.trajanje = 1;
        vm.dodajElementNaKoledar.uraZacetka = '12';
      });
    }

    vm.pripraviUrejanjeNaKoledarju = function(id) {
      var dogodek = vm.dogodkiNaKoledarju[id];
      console.log(dogodek);
      $scope.$apply(function () {
        vm.dodajElementNaKoledar.idDogodkaNaKoledarju = id;
        vm.dodajElementNaKoledar.naziv = dogodek.naziv;
        vm.dodajElementNaKoledar.opis = dogodek.opis;
        vm.dodajElementNaKoledar.datum = dogodek.datum;
        vm.dodajElementNaKoledar.trajanje = dogodek.trajanje;
        vm.dodajElementNaKoledar.uraZacetka = new Date(dogodek.datum).getHours().toString();
      });
    }

    vm.dodajDogodekNaKoledar = function() {
      var datum = new Date(vm.dodajElementNaKoledar.datum);
      datum.setHours(vm.dodajElementNaKoledar.uraZacetka);
      var podatki = {
        idKoledarja: vm.dodajElementNaKoledar.idKoledarja,
        naziv: vm.dodajElementNaKoledar.naziv,
        opis: vm.dodajElementNaKoledar.opis,
        datum: datum,
        trajanje: vm.dodajElementNaKoledar.trajanje
      };
      $http.post('/api/v1/koledar', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        vm.dodajElementNaKoledar.idUrnika = odziv.data._id;
        vm.dogodkiNaKoledarju = odziv.data.elementi;
        prikaziDogodke(odziv.data.elementi);
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    }

    vm.urediDogodekNaKoledarju = function() {
      var datum = new Date(vm.dodajElementNaKoledar.datum);
      datum.setHours(vm.dodajElementNaKoledar.uraZacetka);
      var podatki = {
        idDogodka: vm.dodajElementNaKoledar.idDogodkaNaKoledarju,
        idKoledarja: vm.dodajElementNaKoledar.idKoledarja,
        naziv: vm.dodajElementNaKoledar.naziv,
        opis: vm.dodajElementNaKoledar.opis,
        datum: datum,
        trajanje: vm.dodajElementNaKoledar.trajanje
      };
      $http.put('/api/v1/koledar', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      }).then(function (odziv) {
        vm.dodajElementNaKoledar.idUrnika = odziv.data._id;
        vm.dogodkiNaKoledarju = odziv.data.elementi;
        prikaziDogodke(odziv.data.elementi);
      }, function error(response) {
        console.log(response);
        if (response.data.sporocilo && response.data.sporocilo.length > 0) {
          vm.sporocilo = response.data.sporocilo;
        } else if (response.data.sporočilo) {
          vm.sporocilo = response.data.sporočilo;
        }
      });
    }

    vm.izbrisiDogodekNaKoledarju = function() {
      potrdiBrisanje("Ali res želiš izbrisati dogodek?", function() {
        var podatki = {
          idDogodka: vm.dodajElementNaKoledar.idDogodkaNaKoledarju,
          idKoledarja: vm.dodajElementNaKoledar.idKoledarja,
        };
        $http.put('/api/v1/koledar/izbrisi', podatki, {
          headers: {
            Authorization: 'Bearer ' + avtentikacija.vrniZeton()
          }
        }).then(function (odziv) {
          vm.dodajElementNaKoledar.idUrnika = odziv.data._id;
          vm.dogodkiNaKoledarju = odziv.data.elementi;
          prikaziDogodke(odziv.data.elementi);
        }, function error(response) {
          console.log(response);
          if (response.data.sporocilo && response.data.sporocilo.length > 0) {
            vm.sporocilo = response.data.sporocilo;
          } else if (response.data.sporočilo) {
            vm.sporocilo = response.data.sporočilo;
          }
        });
      });
    }

  }
  domacaStranCtrl.$inject = ['$http', '$scope', 'cisteDesetkePodatki', 'avtentikacija'];

  /* global angular */
  angular
    .module('ciste-desetke')
    .controller('domacaStranCtrl', domacaStranCtrl);
})();
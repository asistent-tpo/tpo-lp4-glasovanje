var assert = require('assert');
var app = require('../src/app');
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;
var crypto = require("crypto");
var should = require("should");

var sendNotification = require("../src/app_api/controllers/notifications").sendNotification;

var testMessage = new Object();
testMessage.title = "Title";
testMessage.body = "Body";

var zetonStudent;
var zeton2;
describe('#sendNotification()', function() {
  it('[adminObvestila]pravilna prijava', function (done) {
      let zahteva = {
        "email": "admin@admin.si",
        "password": "adminfri"
      }
      request(app)
          .post('/api/v1/login')
          .send(zahteva)
          .expect(200)
          .end(function(err,res){
            res.status.should.equal(200);
            zetonStudent = "Bearer " + res.body.zeton;
            done();
          });
  });
  
  it("[adminObvestila]Ustvari obvestilo z API", function(done) {
    request(app).post('/api/v1/notification').set({'Authorization': zetonStudent}).send(testMessage)
    .end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });
  
  it("[adminObvestila]Ustvari obvestilo z API brez telesa zahteve", function(done) {
    request(app).post('/api/v1/notification').set({'Authorization': zetonStudent}).send()
    .end(function(err, res) {
      res.status.should.equal(400);
      done();
    })
  });
  
  it('[adminObvestila]pravilna prijava 2', function (done) {
      let zahteva = {
      "email": "john.smith@primer.si",
      "password": "password"
    }
      request(app)
          .post('/api/v1/login')
          .send(zahteva)
          .expect(200)
          .end(function(err,res){
            res.status.should.equal(200);
            zeton2 = "Bearer " + res.body.zeton;
            done();
          });
  });
  
  it("[adminObvestila]Ustvari obvestilo z API brez prave avtorizacije", function(done) {
    request(app).post('/api/v1/notification').set({'Authorization': zeton2}).send(testMessage)
    .end(function(err, res) {
      res.status.should.equal(401);
      done();
    })
  });
  
  // it("[adminObvestila]Funkcija zazna če zahteva nima vsebine", function() {
  //   assert.equal(sendNotification("a", "b"), -1);
  // });
  // it("[adminObvestila]Funkcija se ob pravilnem vnosu izvede brez napake", function() {
  //   assert.equal(sendNotification(testMessage, "b"), 1);
  // });
    
});
var assert = require('assert');
var app = require('../src/app');
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;
var crypto = require("crypto");
var should = require("should");

var emailOk = crypto.randomBytes(20).toString('hex')+"@gmail.com";
var zahtevaOK = {
  "email": emailOk,
  "password1": "password",
  "password2": "password"
}

var emailDiffPass = crypto.randomBytes(20).toString('hex')+"@gmail.com";
var zahtevaDiffPass = {
  "email": emailDiffPass,
  "password1": "password1",
  "password2": "password"
}

var emailShort = crypto.randomBytes(20).toString('hex')+"@gmail.com";
var zahtevaShort = {
  "email": emailShort,
  "password1": "geslo",
  "password2": "geslo"
}

describe('Registracija', function() {
    it('pravilna registracija', function (done) {
        request(app)
            .post('/api/v1/register')
            .send(zahtevaOK)
            .end(function(err,res){
              res.status.should.equal(200);
              done();
            });
    });
    it('registracija brez vsebine', function (done) {
        request(app)
            .post('/api/v1/register')
            .send()
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Zahtevani so vsi podatki");
              done();
            });
    });
    it('registracija z različnimi gesli', function (done) {
        request(app)
            .post('/api/v1/register')
            .send(zahtevaDiffPass)
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Gesli se ne ujemata");
              done();
            });
    });
    it('registracija s prekratkim geslom', function (done) {
        request(app)
            .post('/api/v1/register')
            .send(zahtevaShort)
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Geslo mora biti dolgo vsaj 8 znakov.");
              done();
            });
    });
    it('registracija z geslom, ki je že v uporabi', function (done) {
        request(app)
            .post('/api/v1/register')
            .send(zahtevaOK)
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Ti prijavni podatki so že zasedeni.");
              done();
            });
    });
});

var zetonStudent;
describe('Prijava', function() {
    it('prijava z nepotrjenim emailom', function (done) {
        let zahteva = {
          "email": emailOk,
          "password": "password"
        }
        request(app)
            .post('/api/v1/login')
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Niste še potrdili svojega emaila. Ta je lahko tudi v vsiljeni pošti.");
              done();
            });
    });
    it('pravilna prijava', function (done) {
        let zahteva = {
          "email": "janez.novak@primer.si",
          "password": "geslofri"
        }
        request(app)
            .post('/api/v1/login')
            .send(zahteva)
            .expect(200)
            .end(function(err,res){
              res.status.should.equal(200);
              zetonStudent = "Bearer " + res.body.zeton;
              done();
            });
    });
    it('prijava študent napačno geslo', function (done) {
        let zahteva = {
          "email": "janez.novak@primer.si",
          "password": "geslofri3"
        }
        request(app)
            .post('/api/v1/login')
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(401);
              res.body.sporocilo.should.equal("Napačno geslo");
              done();
            });
    });
    it('prijava študent napačno uporabniško ime', function (done) {
        let zahteva = {
          "email": "janez.novak1@primer.si",
          "password": "geslofri"
        }
        request(app)
            .post('/api/v1/login')
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(401);
              res.body.sporocilo.should.equal("Napačno uporabniško ime");
              done();
            });
    });
    it('prijava upravljalec dogodkov napačno geslo', function (done) {
        let zahteva = {
          "email": "john.smith@primer.si",
          "password": "geslofri3"
        }
        request(app)
            .post('/api/v1/login')
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(401);
              res.body.sporocilo.should.equal("Napačno geslo");
              done();
            });
    });
    it('prijava administrator napačno geslo', function (done) {
        let zahteva = {
          "email": "admin@admin.si",
          "password": "geslofri3"
        }
        request(app)
            .post('/api/v1/login')
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(401);
              res.body.sporocilo.should.equal("Napačno geslo");
              done();
            });
    });
   
   
    
    
});

describe('Sprememba gesla', function() {
  it('pravilna sprememba gesla', function (done) {
        let zahteva = {
          "passwordOld": "geslofri",
          "password": "geslofri1", 
          "passwordCompare": "geslofri1"
        }
        request(app)
            .put('/api/v1/users')
            .set({'Authorization': zetonStudent})
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(200);
              done();
            });
    });
    it('sprememba gesla z napačnim geslom', function (done) {
        let zahteva = {
          "passwordOld": "geslofri",
          "password": "geslofri1", 
          "passwordCompare": "geslofri1"
        }
        request(app)
            .put('/api/v1/users')
            .set({'Authorization': zetonStudent})
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(401);
              res.body.sporocilo.should.equal("Geslo ni pravilno");
              done();
            });
    });
    it('sprememba gesla s prekratkim geslom', function (done) {
        let zahteva = {
          "passwordOld": "geslofri1",
          "password": "sfes", 
          "passwordCompare": "sfes"
        }
        request(app)
            .put('/api/v1/users')
            .set({'Authorization': zetonStudent})
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Geslo mora biti dolgo vsaj 8 znakov.");
              done();
            });
    });
    it('sprememba gesla z različnima gesloma', function (done) {
        let zahteva = {
          "passwordOld": "geslofri1",
          "password": "sfessfes", 
          "passwordCompare": "sfessssss"
        }
        request(app)
            .put('/api/v1/users')
            .set({'Authorization': zetonStudent})
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(400);
              res.body.sporocilo.should.equal("Gesli se morata ujemati");
              done();
            });
    });
    it('reset gesla študenta', function (done) {
        let zahteva = {
          "passwordOld": "geslofri1",
          "password": "geslofri", 
          "passwordCompare": "geslofri"
        }
        request(app)
            .put('/api/v1/users')
            .set({'Authorization': zetonStudent})
            .send(zahteva)
            .end(function(err,res){
              res.status.should.equal(200);
              done();
            });
    });
});
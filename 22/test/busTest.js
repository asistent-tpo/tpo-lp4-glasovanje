var app = require('../src/app');
var assert = require('assert');
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;

var postajaliscaCtrl = require('../src/app_api/controllers/postajalisca');

describe('Postajalisca', function() {
    
  it("Pridobi vsa postajalisca z API", function(done) {
    request(app).get('/api/v1/postajalisca').end(function(err, res) {
      expect(res.statusCode).to.equal(200);
      done();
    });
  });
  
  it("Pridobi postajalisca, ki se ujemajo z iskalnim nizom", function(done) {
    request(app).get('/api/v1/postajalisca/hajdrihova')
    .send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('602032');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    });
  });
});


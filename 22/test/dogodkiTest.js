var assert = require('assert');
var app = require('../src/app');
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;

var getEvents = require("../src/app_api/controllers/events").getAllEvents;
var createOneEvent = require("../src/app_api/controllers/events").createEvent;

var testEvent = new Object();
testEvent.ime = "ImeTest";
testEvent.opis = "OpisTest";
testEvent.datum = "2018-01-01";
testEvent.organizator = "OrganizatorTest";

var zetonStudent;
var zeton2;
describe('#createOneEvent()', function() {
  it('[dogodki]pravilna prijava', function (done) {
      let zahteva = {
        "email": "john.smith@primer.si",
        "password": "password"
      }
      request(app)
          .post('/api/v1/login')
          .send(zahteva)
          .expect(200)
          .end(function(err,res){
            res.status.should.equal(200);
            zetonStudent = "Bearer " + res.body.zeton;
            done();
        });
  });
  
  it("[dogodki]Ustvari dogodek z API", function(done) {
    request(app).post('/api/v1/events').set({'Authorization': zetonStudent}).
    send(testEvent).end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });
  
  
  it("[dogodki]Ustvari dogodek z API brez telesa zahteve", function(done) {
    request(app).post('/api/v1/events').set({'Authorization': zetonStudent}).send()
    .end(function(err, res) {
      res.status.should.equal(400);
      done();
    })
  });
  
  it("[dogodki]pravilna prijava 2", function (done) {
      let zahteva = {
        "email": "admin@admin.si",
        "password": "adminfri"
      }
      request(app)
          .post('/api/v1/login')
          .send(zahteva)
          .expect(200)
          .end(function(err,res){
            res.status.should.equal(200);
            zeton2 = "Bearer " + res.body.zeton;
            done();
        });
  });
  
  it("[dogodki]Ustvari dogodek z API brez prave avtorizacije", function(done) {
    request(app).post('/api/v1/events').set({'Authorization': zeton2}).send(testEvent)
    .end(function(err, res) {
      res.status.should.equal(401);
      done();
    })
  });
  
  it("[dogodki]Ustvari dogodek z API z nedovoljenim datumom", function(done) {
    testEvent.datum = "2500-01-01";
    request(app).post('/api/v1/events').set({'Authorization': zetonStudent}).
    send(testEvent).end(function(err, res) {
      res.status.should.equal(400);
      done();
    })
  });
  
  // it("[dogodki]Funkcija zazna če ni vsebine in ne shrani dogodka", function() {
  //   assert.equal(createOneEvent("a", "b"), "ni payloada");
  // });
  // it("[dogodki]Funkcija se ob pravilnem vnosu izvede brez napake", function() {
  //   assert.equal(createOneEvent(testEvent, "b"), "end");
  // });
  
});

describe('#getEvents()', function() {
    it("Pridobi vse dogodke z API", function(done) {
      request(app).get('/api/v1/events').end(function(err, res) {
        expect(res.statusCode).to.equal(200);
        done();
      })
    });
    
    // it("[dogodki]Funkcija zahteve po vseh se izvede pravilno", function() {
    //   assert.equal(getEvents("a", "b"), "end");
    // });
});
var assert = require('assert');
var app = require('../src/app');
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;

var koledarCtrl = require('../src/app_api/controllers/koledar');

var zetonStudent;
var idSeznama;
var idElementaNaSeznamu;

describe('TODO seznami', function() {
  it('prijava', function (done) {
    let zahteva = {
      "email": "john.smith@primer.si",
      "password": "password"
    }
    request(app)
      .post('/api/v1/login')
      .send(zahteva)
      .expect(200)
      .end(function(err,res){
        res.status.should.equal(200);
        zetonStudent = "Bearer " + res.body.zeton;
        done();
    });
  });
  
  it("ustvari seznam", function(done) {
    var podatki = {nazivSeznama: 'Seznam za testiranje'};
    request(app).post('/api/v1/todoElementi').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });

  it("preveri ali je seznam ustvarjen", function(done) {
    request(app).get('/api/v1/todoElementi').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Seznam za testiranje');
      assert.notEqual(lokacijaBesedila, -1);
      for(var i = 0; i < res.body.length; i++) {
        if (res.body[i].nazivSeznama === 'Seznam za testiranje') {
          idSeznama = res.body[i]._id;
        }
      }
      assert(idSeznama !== undefined);
      done();
    })
  });

  it("Dodaj element na seznam", function(done) {
    var podatki = {
      idSeznama: idSeznama,
      naziv: 'Naziv elementa na seznamu za teste',
      opis: 'opis'
    };
    request(app).post('/api/v1/todoElementi/element').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });

  it("Preglej ali je dodani element na seznamu", function(done) {
    request(app).get('/api/v1/todoElementi').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Naziv elementa na seznamu za teste');
      assert.notEqual(lokacijaBesedila, -1);
      for(var i = 0; i < res.body.length; i++) {
        if (res.body[i].nazivSeznama === 'Seznam za testiranje') {
          for(var j = 0; j < res.body[j].elementi.length; j++) {
            if (res.body[i].elementi[j].naziv === 'Naziv elementa na seznamu za teste') {
              idElementaNaSeznamu = j;
              break;
            }
          }
          break;
        }
      }
      assert(idElementaNaSeznamu !== undefined);
      done();
    })
  });

  it("Uredi dodani element", function(done) {
    var podatki = {
      idSeznama: idSeznama,
      idElementa: idElementaNaSeznamu,
      naziv: 'Novi testni naziv',
      opis: 'opis'
    };
    request(app).put('/api/v1/todoElementi/element').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });

  it("Preglej ali je urejeni element na seznamu", function(done) {
    request(app).get('/api/v1/todoElementi').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Naziv elementa na seznamu za teste');
      assert.equal(lokacijaBesedila, -1);
      lokacijaBesedila = JSON.stringify(res.body).indexOf('Novi testni naziv');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Izbrisi dodani element", function(done) {
    var podatki = {
      idSeznama: idSeznama,
      idElementa: idElementaNaSeznamu,
    };
    request(app).put('/api/v1/todoElementi/element/izbrisi').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });

  it("Preglej ali je izbrisani element na seznamu", function(done) {
    request(app).get('/api/v1/todoElementi').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Naziv elementa na seznamu za teste');
      assert.equal(lokacijaBesedila, -1);
      lokacijaBesedila = JSON.stringify(res.body).indexOf('Novi testni naziv');
      assert.equal(lokacijaBesedila, -1);
      done();
    })
  });

  it("Izbrisi dodani seznam", function(done) {
    var podatki = {
      idSeznama: idSeznama
    };
    request(app).put('/api/v1/todoElementi/izbrisi').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      done();
    })
  });

  it("preveri ali je seznam izbrisan", function(done) {
    request(app).get('/api/v1/todoElementi').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Seznam za testiranje');
      assert.equal(lokacijaBesedila, -1);
      done();
    })
  });
});

var idUrnika = -1;
var idPredmeta = -1;
describe('Urnik', function() {
  it("Pridobi urnik", function(done) {
    request(app).get('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      idUrnika = res.body._id;
      done();
    })
  });

  it("Dodaj element na urnik", function(done) {
    var podatki = {
      idUrnika: idUrnika,
      naziv: 'Testni element na urniku',
      danVTednu: 2,
      uraZacetka: 8,
      trajanje: 2,
      barva: '#aa0000'
    };
    request(app).post('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni element na urniku');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Ali je novi element res na urniku", function(done) {
    request(app).get('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni element na urniku');
      assert.notEqual(lokacijaBesedila, -1);

      for (var i = 0; i < res.body.vnosi.length; i++) {
        if (res.body.vnosi[i].naziv === 'Testni element na urniku') {
          idPredmeta = i;
          break;
        }
      }
      done();
    })
  });

  it("Uredi element na urniku", function(done) {
    var podatki = {
      idUrnika: idUrnika,
      idVnosa: idPredmeta,
      naziv: 'Novi naziv za test',
      trajanje: 2,
      barva: '#aa0000'
    };
    request(app).put('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni element na urniku');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Novi naziv za test');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Ali je ime res popravljeno", function(done) {
    request(app).get('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni element na urniku');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Novi naziv za test');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Izbrisi element na urniku", function(done) {
    var podatki = {
      idUrnika: idUrnika,
      idVnosa: idPredmeta,
    };
    request(app).put('/api/v1/urnik/izbrisi').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni element na urniku');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Novi naziv za test');
      assert.equal(lokacijaBesedila, -1);
      done();
    })
  });

  it("Ali je element res izbrisan", function(done) {
    request(app).get('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni element na urniku');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Novi naziv za test');
      assert.equal(lokacijaBesedila, -1);
      done();
    })
  });

  it("Dodaj neveljaven element na urnik", function(done) {
    var podatki = {
      idUrnika: idUrnika,
    };
    request(app).post('/api/v1/urnik').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.not.equal(200);
      done();
    })
  });
});

var idKoledarja = -1;
var idDogodka = -1;
describe('Koledar', function() {
  it("Pridobi koledar", function(done) {
    request(app).get('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      idKoledarja = res.body._id;
      done();
    })
  });

  it("Dodaj dogodek na koledar", function(done) {
    var podatki = {
      idKoledarja: idKoledarja,
      naziv: 'Testni dogodek na koledarju',
      opis: 'Opis',
      datum: '2014-01-01T23:28:56.782Z',
      trajanje: 2
    };
    request(app).post('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni dogodek na koledarju');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Ali je novi dogodek res na koledar", function(done) {
    request(app).get('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni dogodek na koledarju');
      assert.notEqual(lokacijaBesedila, -1);
      // nastavi idDogodka
      for (var i = 0; i < res.body.elementi.length; i++) {
        if (res.body.elementi[i].naziv === 'Testni dogodek na koledarju') {
          idDogodka = i;
          break;
        }
      }
      done();
    })
  });

  it("Uredi dogodek na koledarju", function(done) {
    var podatki = {
      idKoledarja: idKoledarja,
      idDogodka: idDogodka,
      naziv: 'Preimenovani dogodek',
      opis: 'Opis',
      datum: '2014-01-01T23:28:56.782Z',
      trajanje: 2
    };
    request(app).put('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni dogodek na koledarju');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Preimenovani dogodek');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Ali je ime res popravljeno", function(done) {
    request(app).get('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni dogodek na koledarju');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Preimenovani dogodek');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    })
  });

  it("Izbrisi dogodek na koledarju", function(done) {
    var podatki = {
      idKoledarja: idKoledarja,
      idDogodka: idDogodka,
    };
    request(app).put('/api/v1/koledar/izbrisi').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni dogodek na koledarju');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Preimenovani dogodek');
      assert.equal(lokacijaBesedila, -1);
      done();
    })
  });

  it("Ali je dogodek res izbrisan", function(done) {
    request(app).get('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Testni dogodek na koledarju');
      assert.equal(lokacijaBesedila, -1);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Preimenovani dogodek');
      assert.equal(lokacijaBesedila, -1);
      done();
    })
  });

  it("Poskusi dodati dogodek z neveljavnim datumom", function(done) {
    var podatki = {
      idKoledarja: idKoledarja,
      naziv: 'Dogodek z neveljavnim datumom',
      opis: 'Opis',
      datum: 'Neveljavni datum',
      trajanje: 2
    };
    request(app).post('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.not.equal(200);
      done();
    })
  });

  it("Poskusi dodati dogodek brez vsebine", function(done) {
    var podatki = {
      idKoledarja: idKoledarja      
    };
    request(app).post('/api/v1/koledar').set({'Authorization': zetonStudent}).
    send(podatki).end(function(err, res) {
      res.status.should.not.equal(200);
      done();
    })
  });

  it("Funkcija za preverjanje veljavnosti datumov", function(done) {
    assert(!koledarCtrl.aliJeDatumVeljaven('en datum'));
    assert(koledarCtrl.aliJeDatumVeljaven('2019-01-01'));
    done();
  })
});

var app = require('../src/app');
var assert = require('assert');
var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;

var restavracijeCtrl = require('../src/app_api/controllers/restavracije');

describe('Restavracije', function() {
    
  it("Pridobi vse restavracije z API", function(done) {
    request(app).get('/api/v1/restavracije').end(function(err, res) {
      expect(res.statusCode).to.equal(200);
      done();
    });
  });
  
  it("Pridobi koordinate z Google Maps API", function(done) {
    request(app).get('/api/v1/lokacije/siska')
    .send().end(function(err, res) {
      res.status.should.equal(200);
      var lokacijaBesedila = JSON.stringify(res.body).indexOf('Šiška District');
      assert.notEqual(lokacijaBesedila, -1);
      done();
    });
  });
});


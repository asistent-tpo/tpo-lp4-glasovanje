(function() {
function resetPwd($scope, authentication) {
  var vm = this;
  vm.loginData = {};
  vm.currentUser = authentication.currentUser();
  vm.sendReset = function(){
    if(!vm.loginData.pword || !vm.loginData.rpword || !vm.loginData.npword){
      vm.error = "Please fill all the fields";
      return;
    }
    if(vm.loginData.pword != vm.loginData.rpword){
      vm.error = "Password don't match";
      return false;  
    }
    if(!/\d/.test(vm.loginData.npword) || !/[A-Z]/.test(vm.loginData.npword) || !/[a-z]/.test(vm.loginData.npword) || !vm.loginData.npword.length > 7){
      vm.error = "Password needs to contain one uppercase, one lowercase letter, one number and the password needs to be longer than 7 characters.";
      return false;  
    }
    
    authentication.resetPwd({
      uname: vm.currentUser.uname,
      pword: vm.loginData.pword,
      rpword: vm.loginData.rpword,
      npword: vm.loginData.npword
    }).then(
        function success(res) {
          vm.msg = res.data.message;
        },
        function error(res) {
          vm.error = res.data.message;
        }  
      );
    
  };
}

resetPwd.$inject = ['$scope', 'authentication'];
  /* global angular  */
  angular 
    .module('straightas')
    .controller('resetPwd', resetPwd);
})();
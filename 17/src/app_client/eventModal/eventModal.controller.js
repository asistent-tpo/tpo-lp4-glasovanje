(function() {
  function eventModal($uibModalInstance, straightAsData) {
    var vm = this;
    vm.data={};
    
    vm.modalWin = {
      close: function() {
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.sendData = function(){
      straightAsData.eventPost({
        name: vm.data.name,
        date: vm.data.date,
        org: vm.data.org,
        desc: vm.data.desc
      }).then(
        function success(odgovor) {
          vm.modalWin.closeS(odgovor.data);
        },
        function error(odgovor) {
          vm.formError = "Error!";
        }
      );
    };
  }
  
  eventModal.$inject = ['$uibModalInstance', 'straightAsData'];

  /* global angular */
  angular
    .module('straightas')
    .controller('eventModal', eventModal);
})();
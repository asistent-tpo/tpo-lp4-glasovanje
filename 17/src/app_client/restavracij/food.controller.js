(function() {
function foodController($scope, straightAsData, geolocation, $sce) {
  var vm = this;
  
  vm.lat = 46.0512405;
  vm.lng = 14.5065963;
  
  vm.url = $sce.trustAsResourceUrl("https://www.google.com/maps/embed/v1/place?key=AIzaSyDAPu0AwA4_Z35ByjKgJu0t953kelEoUSg&q="+vm.lat+","+vm.lng);
  
  vm.extraInfo = "Izberi lokal";
  
  
  
  vm.changeUrl = function(url){
    vm.url = $sce.trustAsResourceUrl(url);
  };
  
  straightAsData.locationList().then(
    function success(res) {
      vm.locationList=res.data;
      vm.setDistance();
    },
    function error(res) {
    }
  );
  
  vm.setDistance = function(){
    console.log(vm.locationList);
    for(var location in vm.locationList){
      vm.locationList[location].dist = vm.calcDist(vm.locationList[location].lat, vm.locationList[location].long, vm.lat, vm.lng);
      console.log(location.lat+" " + location.long+" " + vm.lat+" " + vm.lng);
    }
    
  };
    
  vm.curLocation = function(location){
    $scope.$apply(function() {
      vm.lat = location.coords.latitude;
      vm.lng = location.coords.longitude;
      vm.changeUrl("https://www.google.com/maps/embed/v1/place?key=AIzaSyDAPu0AwA4_Z35ByjKgJu0t953kelEoUSg&q="+vm.lat+","+vm.lng);
    });
  };
  
  vm.locErr = function(err) {
    $scope.$apply(function() {
      vm.error = err.message;
      vm.lat = 46.0512405;
      vm.lng = 14.5065963;
      vm.changeUrl("https://www.google.com/maps/embed/v1/place?key=AIzaSyDAPu0AwA4_Z35ByjKgJu0t953kelEoUSg&q="+vm.lat+","+vm.lng);
    });
  };
  
  vm.supErr = function() {
    $scope.$apply(function() {
      vm.error = "Your browser doesn't support geolocation!";
      vm.lat = 46.0512405;
      vm.lng = 14.5065963;
      vm.changeUrl("https://www.google.com/maps/embed/v1/place?key=AIzaSyDAPu0AwA4_Z35ByjKgJu0t953kelEoUSg&q="+vm.lat+","+vm.lng);
    });
  };
    
  
    
  vm.calcDist = function (lat1,lon1,lat2,lon2) {
  	var R = 6371; 
  	var dLat = (lat2-lat1) * Math.PI / 180;
  	var dLon = (lon2-lon1) * Math.PI / 180;
  	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  		Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
  		Math.sin(dLon/2) * Math.sin(dLon/2);
  	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  	var d = R * c;
  	if (d>1){
  	  console.log(d);
  	  d = Math.round(d * 100) / 100;
  	  console.log(d);
  	  return d+"km";
  	}
  	else if (d<=1) return Math.round(d*1000)+"m";
  	return d;
  };
  
  vm.selectLocation = function (location){
    vm.extraInfo = location.info;
    vm.changeUrl("https://www.google.com/maps/embed/v1/directions?origin="+vm.lat+","+vm.lng+"&destination="+location.lat+","+location.long+"&key=AIzaSyDAPu0AwA4_Z35ByjKgJu0t953kelEoUSg");
  };
  geolocation.curLocation(
    vm.curLocation,
    vm.locErr,
    vm.supErr
  );
}

foodController.$inject = ['$scope', 'straightAsData', 'geolocation', '$sce'];
  /* global angular  */
  angular 
    .module('straightas')
    .controller('foodController', foodController);
})();
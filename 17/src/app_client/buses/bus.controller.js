(function() {
function busController($window, authentication, straightAsData) {
  var vm = this;
  vm.loginData = {};
  vm.busNum = 0;
  
  vm.isdataok = vm.data != null;

    
    vm.loginUser = function(uname, pword){
      authentication.login({
        uname: uname,
        pword: pword
      }).then(
        function success(res) {
          $window.location.href = '/';
        },
        function error(error) {
          vm.error = error.data.message;
        }  
      );
    };
    
    vm.getInfo = function(){
        console.log(vm.busNum);
        straightAsData.getBusInfo({data:vm.data}, vm.busNum).then(
            function success(response){
              console.log(vm.busNum);
                vm.data = {
                    info: response.data
                };
                return vm.data;
            }, function error(response){
                    vm.message = "There seems to be an error";
                    console.log(response.e);
                })
    }
    vm.message = "ha";
    console.log(vm.busNum);
}

busController.$inject = ['$window',  'authentication', 'straightAsData'];
  /* global angular  */
  angular 
    .module('straightas')
    .controller('busController', busController);
})();
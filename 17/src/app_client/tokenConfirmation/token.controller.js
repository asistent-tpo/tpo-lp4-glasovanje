(function() {
function tokenController($window, $routeParams, $timeout, authentication) {
  var vm = this;
  vm.text = 'We are confirming your token. Please wait.';

  if(!$routeParams.token){
    vm.text = "No token found.";
    return false;
  }else{
    authentication.confirmToken($routeParams.token).then(
      function success(res) {
        vm.text = 'Your email is confirmed you can login now.';
        $timeout(function() {
          $window.location.href = '/login';
        }, 4000);
      },
      function error(error) {
        vm.text = error.data.message;
      }  
    );
    }
 
}

tokenController.$inject = ['$window', '$routeParams', '$timeout',  'authentication'];
  /* global angular  */
  angular 
    .module('straightas')
    .controller('tokenController', tokenController);
})();
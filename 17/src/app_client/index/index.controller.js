(function() {
function indexController($scope, straightAsData, authentication, $uibModal, uiCalendarConfig) {
  var vm = this;
  vm.data = {};
  vm.data.todoList = {};
  vm.data.tt = [];
  for(var i=0; i<8; i++){
    vm.data.tt[i] = [{
      day:1,
      colour: 0
    },{
      day:2,
      colour: 0
    },{
      day:3,
      colour: 0
    },{
      day:4,
      colour: 0
    },{
      day:5,
      colour: 0
    }];  
  }
  vm.eventSources =[{
  events: [],
  color: 'blue',   // an option!
  textColor: 'black' // an option!
}];
  vm.currentUser = authentication.currentUser();
  
  if(vm.currentUser){

    vm.deleteTodo = function (el){
      
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/deleteModal/delete.view.html',
        controller: 'deleteModal',
        controllerAs: 'vm',
        resolve: {
          todoInfo: function() {
            return {
              id: el._id
            };
          }
        }
      });
      
      
      primerekModalnegaOkna.result.then(function(data) {
        if (typeof data != 'undefined'){
            var removeIndex = vm.data.todoList.map(function(e) { return e._id; }).indexOf(el._id);
            vm.data.todoList.splice(removeIndex, 1);
          }
        }, function(napaka) {
      });
      
    };
    
   vm.createCalendar = function (){
      
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/calendarModal/calendarModal.view.html',
        controller: 'calendarModal',
        controllerAs: 'vm',
        resolve: {
          userInfo: function() {
            console.log(vm.currentUser);
            return {
              uname: vm.currentUser.uname
            };
          }
        }
      });
      
      
      primerekModalnegaOkna.result.then(function(data) {
        if (typeof data != 'undefined'){
          vm.eventSources[0].events.push({
            title: data.name,
            start: data.start,
            end: data.end
          });
        }
      }, function(napaka) {
      });
      
    };
    
    vm.createTodo = function(){
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/todoModal/todoModal.view.html',
        controller: 'todoModal',
        controllerAs: 'vm',
        resolve: {
          emailInfo: function() {
            return {
              email: vm.currentUser.uname
            };
          }
        }
      });
      
      
      primerekModalnegaOkna.result.then(function(data) {
        if (typeof data != 'undefined'){
            vm.data.todoList.push(data);
          }
        }, function(napaka) {
      });
      
    };
    
    
    vm.editItem = function(el){
      var tmpT = el.title;
      var tmpD = el.desc;
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/editModal/editModal.view.html',
        controller: 'editModal',
        controllerAs: 'vm',
        resolve: {
          emailInfo: function() {
            return el;
          }
        }
      });
      
      
      primerekModalnegaOkna.result.then(function(data) {
        if (typeof data != 'undefined'){
            
          }
        }, function(napaka) {
          el.title = tmpT;
          el.desc = tmpD;
      });
      
    };
    
    vm.createTT = function(start, entry){
     var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/TTModal/TTModal.view.html',
        controller: 'TTModal',
        controllerAs: 'vm',
        resolve: {
          emailInfo: function() {
            return {
              email: vm.currentUser.uname,
              entry: entry,
              start: start
            };
          }
        }
      });
      
      
      primerekModalnegaOkna.result.then(function(data) {
        if (typeof data != 'undefined'){
          console.log(data);
          if(!data.delete){
            putdata([data]);
          }else{
            removeData(data);
          }
        }
        }, function(napaka) {
      });
    };
    
    straightAsData.todoList({
      email: vm.currentUser.uname
    }).then(
      function success(res) {
        vm.data.todoList = res.data;
      },
      function error(res) {
        vm.error = res.data.message;
      }  
    );
  
    straightAsData.getCalendar({
      email: vm.currentUser.uname
    }).then(
      function success(res) {
        console.log(res.data);
        for(var i in res.data){
          vm.eventSources[0].events.push({
              title: res.data[i].name,
              start: res.data[i].start,
              end: res.data[i].end
            });
        }
      },
      function error(res) {
        vm.error = res.data.message;
      }  
    );
    
    straightAsData.getTT({
      email: vm.currentUser.uname
    }).then(
      function success(res) {
        putdata(res.data);
      },
      function error(res) {
        vm.error = res.data.message;
      }  
    );
  }
  
  function putdata (data){
    for(var i=0 in data){
      for(var j=0; j<data[i].duration; j++){
        if(data[i].start - 8 + j < 8){
          vm.data.tt[data[i].start - 8 + j][data[i].day-1]._id = data[i]._id;
          vm.data.tt[data[i].start - 8 + j][data[i].day-1].start = data[i].start;
          vm.data.tt[data[i].start - 8 + j][data[i].day-1].colour = data[i].colour;
          vm.data.tt[data[i].start - 8 + j][data[i].day-1].duration = data[i].duration;
          vm.data.tt[data[i].start - 8 + j][data[i].day-1].name = data[i].name;
        }
      }
    }
  }
  
  function removeData (data){
    for(var j=0; j<data.duration; j++){
      if(data.start - 8 + j < 8){
        vm.data.tt[data.start - 8 + j][data.day-1]._id = null;
        vm.data.tt[data.start - 8 + j][data.day-1].start = null;
        vm.data.tt[data.start - 8 + j][data.day-1].colour = null;
        vm.data.tt[data.start - 8 + j][data.day-1].duration = null;
        vm.data.tt[data.start - 8 + j][data.day-1].name = null;
      }
    }
  }
}

indexController.$inject = ['$scope', 'straightAsData', 'authentication', '$uibModal', 'uiCalendarConfig'];

  /* global angular  */
  angular 
    .module('straightas')
    .controller('indexController', indexController);
})();



var request = require("supertest");
var server = require('../app.js');
var assert = require('assert');


var idTodo;
var idDogodek;
var obvestilo;
var idUrnik;



describe("Začetna stran", function(){
    it("Navigacija", function(done){
        request(server).get('/').expect(/navigation><\/navigation>/i, done);
    });
});
 
describe("Login", function() {
    it ("Prijava v sistem", function(done) {
        request(server).get('/login').expect(/navigation><\/navigation>/i, done);    
    });
});

describe("Registration", function(){
    it("Registracija v sistem", function(done){
        request(server).get('/registration').expect(/navigation><\/navigation>/i, done);
    });
});
 
describe("Pregled dogodkov", function(){
    it("Navigacija", function(done){
        request(server).get('/events').expect(/<navigation><\/navigation>/i, done);
 });
    
});

describe("Pregled restavracij", function(){
    it("Ogled podatkov o bližnjih restavracijah na bone", function(done){
        request(server).get('/food').expect(/<navigation><\/navigation>/i, done);
 });
    
});
 
 
 describe("Domača stran upravljalca z dogodki", function(){
    it("Upravljalec z dogodki", function(done){
        request(server).get('/event-manager').expect(/<navigation><\/navigation>/i, done);
 });
    
});


describe("REST", function(){
    it("Pridobi administratorska obvestila", function(done){
        request(server)
        .get('/api/notifications')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Dodaj administratorsko obvestilo", function(done){
        request(server)
        .post('/api/notifications')
        .send({notification:"Test notification", date:"19.19.2001"})
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Odstrani administratorska obvestila", function(done){
        request(server)
        .delete('/api/notifications')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Pridobi dogodke", function(done){
        request(server)
        .get('/api/events')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Dodaj dogodek", function(done){
        request(server)
        .post('/api/events')
        .send({name : "Test", date:"12.12.1231", org:"TestOrganizator", desc: "testDescription"})
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Odstrani dogodke", function(done){
        request(server)
        .delete('/api/events/Test')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Pridobi zapise urnika", function(done){
        request(server)
        .get('/api/timetable')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    
    it("Dodaj zapis v urnik", function(done){
        request(server)
        .post('/api/timetable')
        .send({email : "test@test.com", name:"testName", colour:"blue"})
        .set('Accept', 'application/json')
        .expect(function(res){
            idUrnik = res.body._id;
        })
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Pridobi zapis", function(done){
        request(server)
        .get('/api/timetable/' + idUrnik)
        .set('Accept', 'application/json')
        .expect('Content-Type', /html/)
        .expect(200, done);
    });
    it("Briši zapise iz urnika", function(done){
        request(server)
        .delete('/api/timetableEmail/test@test.com')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
     it("Pridobi vse zapise TODO seznama", function(done){
        request(server)
        .get('/api/todo')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Zapiši v todo seznam", function(done){
        request(server)
        .post('/api/todo')
        .send({email : "asd@asd.com", desc:"desc", title:"title"})
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Pridobi todo zapis", function(done){
        request(server)
        .get('/api/todo/asd@asd.com')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Odstrani todo zapis", function(done){
        request(server)
        .delete('/api/todoEmail/asd@asd.com')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it("Pridobi vse restavracije", function(done){
        request(server)
        .get('/api/restaurant')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    var idRestavracije;
    it("Dodaj restavracijo", function(done){
        request(server)
        .post('/api/restaurant')
        .send({
            name:"Test",
            lat:"23.123",
            long:"41.5414"
        })
        .expect(function(res){
            idRestavracije = res.body._id;
        })
        .expect('Content-Type', /json/)
        .expect(200, done);
    })
    it("Odstrani restavracijo", function(done){
        request(server)
        .delete('/api/restaurant/' + idRestavracije)
        .expect('Content-Type', /json/)
        .expect(200, done);
    })
});


    

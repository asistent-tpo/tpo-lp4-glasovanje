var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/straightas';
if (process.env.NODE_ENV === 'production') {
  dbURI = process.env.MLAB_URI;
}
mongoose.connect(dbURI, { useNewUrlParser: true, useCreateIndex: true });

mongoose.connection.on('connected', function() {
  console.log('Mongoose is connected on ' + dbURI);
});

mongoose.connection.on('error', function(err) {
  console.log('Mongoose error: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('Mongoose end');
});

var pravilnaUstavitev = function(sporocilo, povratniKlic) {
  mongoose.connection.close(function() {
    console.log('Mongoose disconnected with ' + sporocilo);
    povratniKlic();
  });
};

// Pri ponovnem zagonu nodemon
process.once('SIGUSR2', function() {
  pravilnaUstavitev('nodemon reconnect', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

// Pri izhodu iz aplikacije
process.on('SIGINT', function() {
  pravilnaUstavitev('exit from app ', function() {
    process.exit(0);
  });
});

// Pri izhodu iz aplikacije na Heroku
process.on('SIGTERM', function() {
  pravilnaUstavitev('exit from app on heroku', function() {
    process.exit(0);
  });
});

var express = require('express');
var router = express.Router();

var ctrlAuthentication = require('../controllers/authentication');
var ctrlAdminNotifications = require('../controllers/ctrlAdminNotifications.js');
var ctrlEvents = require('../controllers/ctrlEvents.js');
var ctrlTimetable = require('../controllers/ctrlTimetable.js');
var ctrlCalendar = require("../controllers/ctrlCalendar.js");
var ctrlTodo = require('../controllers/ctrlTodo.js');
var ctrlRestaurant = require("../controllers/ctrlRestaurant.js");
var ctrlToken = require("../controllers/ctrlToken.js");


var jwt = require('express-jwt');
var authentication = jwt({
  secret: process.env.JWT_PASS,
  userProperty: 'payload'
});
//user
router.post('/registration', ctrlAuthentication.registration);
router.post('/reset', ctrlAuthentication.change);
router.post('/login', ctrlAuthentication.login);
router.get('/confirmation/:token', ctrlToken.confirmToken);

//ADMINNOTIFICATIONS
//get all
router.get('/notifications', ctrlAdminNotifications.adminNotificationsGetAll);
//add notification
router.post('/notifications', ctrlAdminNotifications.adminNotificationsAdd);
//delete all notifications
router.delete('/notifications', ctrlAdminNotifications.adminNotificationsClear);

//Calendar
//get all

//add event
router.post('/calendar', ctrlCalendar.calendarPost);
router.post('/calendarget/', ctrlCalendar.calendarGetAll);
//EVENTS
//get all
router.get('/events', ctrlEvents.eventsGetAll);
//add event
router.post('/events', ctrlEvents.eventsAdd);

router.delete('/events/:name', ctrlEvents.eventsDelete);


//TIMETABLE
//get all entries
router.get('/timetable', ctrlTimetable.entryGetAll);
//get all entries by email
router.post('/timetableget', ctrlTimetable.entryGetAllByEmail);
//add
router.post('/timetable', ctrlTimetable.entryAdd);
//update
router.put('/timetable/:id', ctrlTimetable.entryUpdate);
//delete
router.delete('/timetable/:id', ctrlTimetable.entryDelete);

router.delete('/timetableEmail/:email', ctrlTimetable.entryDeleteByEmail);

//TODO LIST
//get all todo entries
router.get('/todo', ctrlTodo.entriesGetAll);

router.post('/todoget', ctrlTodo.entriesGetAllByBEmail);
//get all entries by email
router.get('/todo/:email', ctrlTodo.entriesGetAllByEmail);
//add
router.post('/todo', ctrlTodo.entriesAdd);
//update
router.put('/todo/:id', ctrlTodo.entriesUpdate);
//delete
router.delete('/todo/:id', ctrlTodo.entriesDelete);
//delete by email
router.delete('/todoEmail/:email', ctrlTodo.entriesDeleteByEmail);

//Locations
//get all restaurant entries
router.get('/restaurant', ctrlRestaurant.entryGetAll);
//add
router.post('/restaurant', ctrlRestaurant.entryAdd);
//update
router.put('/restaurant/:id', ctrlRestaurant.entryUpdate);
//delete
router.delete('/restaurant/:id', ctrlRestaurant.entryDelete);


module.exports = router;

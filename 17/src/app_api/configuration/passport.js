var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
require("../models/user");
var Users = mongoose.model('user');

passport.use(new localStrategy({
    usernameField: 'uname',
    passwordField: 'pword'
  }, 
  function(uname, pwor, callback) {
    Users.findOne(
      {
        userName: uname,
      },
      function(err, user) {
        if (err)
          return callback(err);
        if (!user) {
          return callback(null, false, {
            message: 'Incorrect username'
          });
        }
        if (!user.checkPwd(pwor)) {
          return callback(null, false, {
            message: 'Incorrect password'
          });
        }
        if(user.confirmationStatus != 1){
          return callback(null, false, {
            message: 'Please confirm your email.'
          });
        }
        return callback(null, user);
      }
    );
  }
));

# Namestitev razvojnega okolja in zagon aplikacije
Za delovanje aplikacije mora biti na sistemu nameščen `Python 3.7`, in delovati mora ukaz `pip`. Na windows napravah je zato poleg inštalacije včasih potrebno v `Environmant variables > Path` nastaviti pot do mape `Python\Python37` in `Python\Python37\Scripts`.

Premaknemo se v direktorij `...\tpo-lp4\src\back` in poženemo ukaze:

* Inštaliramo `pipenv`. Če ukaz `pip` ne dela, uporabi ukaz `pip3`.
```
pip install pipenv
```
* Aktiviramo virtual environment
```
pipenv shell
```
* Inštaliramo knjižnice
```
pipenv install flask flask-sqlalchemy flask-marshmallow marshmallow-sqlalchemy
```
* Poženemo aplikacijo
```
python app.py
```

Aplikacija bi zdaj morala delovati na naslovu `http://localhost:5000/` ali `http://127.0.0.1:5000`. Celotno aplikacijo lahko testiramo v brskalniku, zaledje pa npr. z programom `Postman`.

Uporaba aplikacije z spletnim uporabniškim vmesnikom je samoumevna. Za potrebe razvoja in testiranja pa smo razvili še spodnji del dokumentacije. Večinoma gre za smernice pri razvoju aplkacije, vsebuje pa tudi nekaj vsebinskih informacij.

# Testiranje zaledja (REST vmesnik)

### Backend:
Vse PUT in POST metode morajo v glavi imet “Content-Type: application/json”  in ustezen body. Metode vračajo podatke v formatu JSON ali pa sporočila v plaintextu.

## Uporabnik (za potrebe ročnega testiranja):
```
GET 	http://localhost:5000/uporabnik	        vrne uporabnike
GET 	http://localhost:5000/uporabnik/2	vrne uporabnika z idjem 2
DELETE 	http://localhost:5000/uporabnik/1	zbriše uporabnika z idjem 1, in ga vrne nazaj.
```
POSEBNOSTI: headers in body sporočila so lahko prazni

## Register
```
POST	http://localhost:5000/register
```
* Registrira uporabnika (in ga vrne nazaj), status 200. Če je napačen email ali geslo, vrne napako 400 in sporočilo o napaki.

* Headers:
	`“Content-Type: application/json”`

* Body:
```
{
	"email": "naslov2@gmail.com",
	"geslo1": "12345678",
	"geslo2": "12345678"
}
```
* **EMAIL potrjevanje:**
Na email naslov se pošlje povezava, ki ob kliku potrdi račun. Sedaj se lahko v aplikacijo prijavimo.
Emaili se pošiljajo z domene google.com, ki ima blokirano večino začasnih (npr. 10 minute mail) naslovov, vendar pošiljanje na https://www.minuteinbox.com/ deluje v redu.


* **VIP naslovi** (za potrebe lažjega testiranja): karkoli.karkoli@straightas.com (domena ‘striaghtas’) ne poterbuje potrjevanja preko emaila.

## Sprememba gesla
```
PUT	http://localhost:5000/register
```
* Uporabniku s tem emailom se spremeni geslo in vrne nazaj sporocilo z statusom 200, ce je kaj narobe, vrne status 400 in sporočilo o napaki.

* Headers:
	“Content-Type: application/json”

* Body:
```
{
	"email": "naslov4@gmail.com",
	"staro_geslo": "12345678",
	"novo_geslo": "11111111"
}
```
## Login
``
POST	http://localhost:5000/login
``
* Če je narobni email ali geslo, vrne status 400 in sporočilo o napaki, sicer vrne status 200 in piškotek
`"Set-Cookie : expires="2019-05-11 23:28:19.746104"; Expires=Sat, 11-May-2019 23:28:19 GMT; Path=/"`

* Ta piškotek bomo kasneje uporabljali v prihodnosti pri ostalih funkcijah. Ko iz odjemalca kličeš rest endpointe (vse razen zgoraj naštete (prijava, registracija, zamenjava gesla)) boš moral na odjemalcu nastavit pišotek, ki ga bo server uporabljal za avtentikacijo, da bo znal da si to res ti.

* Headers:
	`“Content-Type: application/json”`

* Body:
```
{
	"email": "naslov0@gmail.com",
	"geslo": "12345678"
}
```
## Todo
```
GET    http://localhost:5000/todo
```
* Vrne todo seznam
* Headers: nastavljen mora biti `Cookie` in registriran email:
	`“From: naslov@gmail.com”`

```
POST    http://localhost:5000/todo
```
* Ustvari prazen vnos v todo seznamu, in vrne ta isti prazen vnos
* Headers: nastavljen `Cookie` in registriran email:
	`“From: naslov@gmail.com”`
```
DELETE  http://localhost:5000/todo/id
```
* izbrise Todo vnos z idejem “id” in vrne v bodyju vnos v formatu JSON (vrne ga za poterbe testiranja, ti ga verjetno ne rabiš)
* Headers: nastavljen `Cookie` in registriran email:
	`“From: naslov@gmail.com”`
```
PUT     http://localhost:5000/todo/id
```
* Posodobi vnos s idjem “id”, spremeni mu opis
* Headers: nastavljen `Cookie` in registriran email:
	`“From: naslov@gmail.com”`
* Body:
```
{
    "opis": "Hoj v trgovino!"
}
```
## Koledar
Stvar je enaka kot pri Todo, le da je endpoint:
```
http://localhost:5000/koledar
```
* Body je v sledeči obliki
```
{
	"email": "naslov2@gmail.com",
	"ime": "Izpit BMO",
	"datum": "23/6/19",
	"opis": "V P19 ob 10.00, zapiski niso dovoljeni"
}
```

## Urnik
Stvar je enaka kot pri Todo in Koledar, le da je endpoint:
```
http://localhost:5000/urnik
```
* Body je v sledeči obliki
```
{
	"email": "naslov2@gmail.com",
    "id" : 1,
	"ime": "BMO",
	"trajanje": 3,
	"barva": "#4488BB",
	"dan": "ponedeljek",
	"ura": "9"
}
```

## Avtobusi
```
POST    http://localhost:5000/avtobus
```
* vrne prihode avtobusov za izbrano postajo

* Headers: `“Content-Type: application/json”`
* Body:
```
{
	"ime_postaje": "svetčeva"
}
```

## Dogodki
```
POST    http://localhost:5000/dogodek
```
* objavi nov dogodek

* Headers:
	`“From: upravljalec.tpo@gmail.com”` \
	`“Content-Type: application/json”` \
    `"Cookie:..."`

    `upravljalec.tpo@gmail.com` ima pravice upravljalca z dogodki, torej v sistem moramo biti prijavljeni z njegovim email naslovom.

* Body:
```
{
	"ime": "Pečenje pečenke pred stavbo FRI",
	"datum": "11/6/19",
	"opis": "Pridite vsi gurmani in nadušenci nad pečenko."
}
```
```
GET     http://localhost:5000/dogodek
```
* vrne vse dogodke
* Nima zahtev glede headerjev ali bodyja, pošlje ga lahko kdorkoli.
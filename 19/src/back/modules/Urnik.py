from flask import jsonify

from modules.database import db
from modules.database import Urnik, Uporabnik
from modules.database import urnik_shema
from modules.database import urniki_shema

def preberi_urnik(email):

    lastnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    result = urniki_shema.dump(lastnik.urniki)
    return jsonify(result.data)

def preveri_veljavnost_datuma(datum):
    return 200

def dodaj_vnos(email, ime, trajanje, barva, dan, ura):
    lastnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    novi_vnos = Urnik(email=email, ime=ime, trajanje=trajanje, barva=barva, dan=dan, ura=ura, lastnikUrnika=lastnik)

    db.session.add(novi_vnos)
    db.session.commit()

    return urnik_shema.jsonify(novi_vnos)

def posodobi_vnos(email, id, ime, trajanje, barva, dan, ura):
    vnos = Urnik.query.get(id)
    vnos.email = email
    vnos.ime = ime
    vnos.trajanje = trajanje
    vnos.barva = barva
    vnos.dan = dan
    vnos.ura = ura
    db.session.commit()

    return urnik_shema.jsonify(vnos)

def izbrisi_vnos(id):
    vnos = Urnik.query.get(id)

    db.session.delete(vnos)
    db.session.commit()

    return urnik_shema.jsonify(vnos)
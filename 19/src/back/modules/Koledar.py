from flask import jsonify

from modules.database import db
from modules.database import Koledar, Uporabnik
from modules.database import koledar_shema
from modules.database import koledarji_shema

def preberi_koledarje(email):

    lastnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    result = koledarji_shema.dump(lastnik.koledarji)
    return jsonify(result.data)

def preveri_veljavnost_datuma(datum):
    return 200

def preveri_ujemanje_email_in_id(email, id):
    vnos = Koledar.query.get(id)

    if ((vnos is None) or (vnos.email != email)):
        return 400
    return 200

def dodaj_vnos(email, ime, datum, opis):
    lastnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    novi_vnos = Koledar(email=email, opis=opis, ime=ime, datum=datum, lastnikKoledarja=lastnik)

    db.session.add(novi_vnos)
    db.session.commit()

    return koledar_shema.jsonify(novi_vnos)

def posodobi_vnos(email, id, ime, datum, opis):
    vnos = Koledar.query.get(id)
    vnos.opis = opis
    vnos.datum = datum
    vnos.ime = ime
    db.session.commit()

    return koledar_shema.jsonify(vnos)

def izbrisi_vnos(email, id):
    vnos = Koledar.query.get(id)

    db.session.delete(vnos)
    db.session.commit()

    return koledar_shema.jsonify(vnos)
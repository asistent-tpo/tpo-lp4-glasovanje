from flask import jsonify

from modules.database import db
from modules.database import Dogodek, Uporabnik
from modules.database import dogodek_shema
from modules.database import dogodki_shema

def preberi_dogodke():

    vsi_dogodki = Dogodek.query.all()
    result = dogodki_shema.dump(vsi_dogodki)
    return jsonify(result.data)


def objavi_dogodek(ime, datum, organizator, opis):
    lastnik = Uporabnik.query.filter(Uporabnik.email == organizator).first()
    novi_vnos = Dogodek(ime=ime, datum=datum, organizator=organizator, opis=opis, lastnikDogodka =lastnik)

    db.session.add(novi_vnos)
    db.session.commit()

    return dogodek_shema.jsonify(novi_vnos)

def preveri_veljavnost_upravljalca(organizator):
    beseda = organizator.split("@")
    beseda = beseda[0].split(".")

    #email upravljalca mora bit v formatu "upravljalec.karkoli@karkoli.karkoli"
    if (beseda[0] != "upravljalec"):
        return 400
    return 200

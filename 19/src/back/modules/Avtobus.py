from flask import jsonify
#import requests
import urllib.request

def pridobi_podatke_postaje(ime_postaje):
    podatki = preveri_iskalno_zahtevo(ime_postaje)
    podatki = filtriraj_podatke(podatki)
    return podatki

def preveri_iskalno_zahtevo(ime_postaje):
    ime_postaje = pocisti_sumnike(ime_postaje)
    contents = urllib.request.urlopen("https://www.trola.si/" + ime_postaje).read()
    #!!!!!!
    contents = str(contents, 'utf-8')
    return contents

def pocisti_sumnike(besedilo):
    seznam = list(besedilo)
    for i in range(len(seznam)):
        if (seznam[i] == 'č' or seznam[i] == 'Č'):
            seznam[i] = 'c'
        if (seznam[i] == 'š' or seznam[i] == 'Š'):
            seznam[i] = 's'
        if (seznam[i] == 'ž' or seznam[i] == 'Ž'):
            seznam[i] = 'z'
    return ''.join(seznam)

def filtriraj_podatke(podatki):
    vrstice = podatki.splitlines()

    stCrt = 0
    zacetek = 0
    konec = 0

    for i in range(len(vrstice)):
        vrsta = vrstice[i]
        #print(vrsta)
        besede = vrsta.split()
        if (len(besede) > 0 and besede[0] == "<hr"):
            stCrt = stCrt + 1
            if stCrt == 1:
                zacetek = i + 1
                konec = zacetek + 4
        if (len(besede) > 0 and besede[0] == "<form"):
            konec = i - 1
            break
    
    vrstice = vrstice[zacetek:konec]

    podatki = '\n'.join(vrstice)
    return podatki
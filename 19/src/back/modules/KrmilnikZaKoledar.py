from modules import Koledar
from flask import Response

def preberi_koledarje(email):
    return Koledar.preberi_koledarje(email)

def dodajanje_vnosa_v_koledar(email, ime, datum, opis):
    veljavnost = Koledar.preveri_veljavnost_datuma(datum)

    if veljavnost != 200:
        return Response("{'msg':'datum ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return Koledar.dodaj_vnos(email, ime, datum, opis)

def posodobi_vnos_v_koledarju(email, id, ime, datum, opis):
    ujemanje = Koledar.preveri_ujemanje_email_in_id(email, id)

    if ujemanje != 200:
        return Response("{'msg':'id in email se ne ujemata. Oziroma, ta vnos v Todo ni v lasti tega email računa.'}", status=ujemanje, mimetype='application/json')

    return Koledar.posodobi_vnos(email, id, ime, datum, opis)

def izbrisi_vnos_iz_koledarja(email, id):
    ujemanje = Koledar.preveri_ujemanje_email_in_id(email, id)

    if ujemanje != 200:
        return Response("{'msg':'id in email se ne ujemata. Oziroma, ta vnos v Todo ni v lasti tega email računa.'}", status=ujemanje, mimetype='application/json')

    return Koledar.izbrisi_vnos(email, id)
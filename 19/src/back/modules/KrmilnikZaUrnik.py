from modules import Urnik
from flask import Response

def preberi_urnike(email):
    return Urnik.preberi_urnik(email)

def dodajanje_vnosa_v_urniku(email, ime,  trajanje, barva, dan, ura):
    return Urnik.dodaj_vnos(email, ime, trajanje, barva, dan, ura)

def posodobi_vnos_v_urniku(email, id, ime, trajanje, barva, dan, ura):
    return Urnik.posodobi_vnos(email, id, ime, trajanje, barva, dan, ura)

def izbrisi_vnos_iz_urnika(id):
    return Urnik.izbrisi_vnos(id)
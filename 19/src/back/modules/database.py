from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

import os

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))

# Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Init ma
ma = Marshmallow(app)

# Uporabnik Class/Model
class Uporabnik(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    hashed_geslo = db.Column(db.String(200))
    dostopni_zeton = db.Column(db.String(200))
    potrjen = db.Column(db.Boolean)

    todoji = db.relationship('Todo', backref='lastnik')
    koledarji = db.relationship('Koledar', backref='lastnikKoledarja')
    urniki = db.relationship('Urnik', backref='lastnikUrnika')
    dogodki = db.relationship('Dogodek', backref='lastnikDogodka')

    def __init__(self, email, hashed_geslo, dostopni_zeton):
        self.email = email
        self.hashed_geslo = hashed_geslo
        self.dostopni_zeton = dostopni_zeton
        self.potrjen = False

# Uporabnik Shema
class UporabnikShema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'hashed_geslo', 'dostopni_zeton', 'potrjen')

# Init Schema
uporabnik_shema = UporabnikShema(strict=True)
uporabniki_shema = UporabnikShema(many=True, strict=True)

# Todo CLass/Model
class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    opis = db.Column(db.String(200))

    lastnik_id = db.Column(db.Integer, db.ForeignKey('uporabnik.id'))

# Todo Shema
class TodoShema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'opis', 'lastnik_id')

# Init Schema
todo_shema = TodoShema(strict=True)
todoji_shema = TodoShema(many=True, strict=True)

# Koledar CLass/Model
class Koledar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    opis = db.Column(db.String(200))
    ime = db.Column(db.String(200))
    datum = db.Column(db.String(100))

    lastnik_id = db.Column(db.Integer, db.ForeignKey('uporabnik.id'))

# Koledar Shema
class KoledarShema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'opis', 'ime', 'datum', 'lastnik_id')

# Init Schema
koledar_shema = KoledarShema(strict=True)
koledarji_shema = KoledarShema(many=True, strict=True)

# Urnik CLass/Model
class Urnik(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    ime = db.Column(db.String(200))
    trajanje = db.Column(db.Integer)
    barva = db.Column(db.String(200))
    dan = db.Column(db.String(100))
    ura = db.Column(db.Integer)

    lastnik_id = db.Column(db.Integer, db.ForeignKey('uporabnik.id'))

# Urnik Shema
class UrnikShema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'ime', 'trajanje', 'barva', 'dan', 'ura', 'lastnik_id')

# Init Schema
urnik_shema = UrnikShema(strict=True)
urniki_shema = UrnikShema(many=True, strict=True)



# Dogodek CLass/Model
class Dogodek(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ime = db.Column(db.String(100))
    datum = db.Column(db.String(100))
    organizator = db.Column(db.String(100))
    opis = db.Column(db.String(100))

    lastnik_id = db.Column(db.Integer, db.ForeignKey('uporabnik.id'))

# Todo Shema
class DogodekShema(ma.Schema):
    class Meta:
        fields = ('id', 'ime', 'datum', 'organizator', 'opis', 'lastnik_id')

# Init Schema
dogodek_shema = DogodekShema(strict=True)
dogodki_shema = DogodekShema(many=True, strict=True)


from flask import jsonify

from modules.database import db
from modules.database import Uporabnik
from modules.database import uporabnik_shema
from modules.database import uporabniki_shema

import secrets
import smtplib, ssl

def preberi_uporabnike():
    vsi_uporabniki = Uporabnik.query.all()
    result = uporabniki_shema.dump(vsi_uporabniki)
    return jsonify(result.data)

def preberi_uporabnika(id):
    up = Uporabnik.query.get(id)
    return uporabnik_shema.jsonify(up)

def izbrisi_uporabnika(id):
    up = Uporabnik.query.get(id)
    db.session.delete(up)
    db.session.commit()

    return uporabnik_shema.jsonify(up)

def izvedi_registracijo(email, geslo, dostopni_zeton):
	key = secrets.token_hex(16)
	novi_uporabnik = Uporabnik(email, geslo, key)

	# vip uporabniki se ne rabijo potrjevat računa preko emaila (za potrebe testiranja)
	# vip uporabniki imajo email v domeni 'straightas', npr: 'karkoli@striaghtas.karkoli'
	if pohendlaj_vip(novi_uporabnik) == False:
		poslji_email(email, key)

	db.session.add(novi_uporabnik)
	db.session.commit()

	return uporabnik_shema.jsonify(novi_uporabnik)

def preveri_veljavnost_emaila(email):
    ze_obstaja = Uporabnik.query.filter(Uporabnik.email == email).first()
    if ze_obstaja is not None:
        return 400

def preveri_veljavnost_gesla(geslo1, geslo2):
    if (geslo1 != geslo2 or (len(geslo1) < 8)):
        return 400

# preveri, ce se geslo in email ujemata
def preveri_geslo(email, geslo):
    uporabnik_ze_obstaja = Uporabnik.query.filter(Uporabnik.email == email).first()
    if uporabnik_ze_obstaja is None:
        return 400
    
    if uporabnik_ze_obstaja.hashed_geslo != geslo:
        return 401
    else:
        return 200

def spremeni_geslo(email, novo_geslo):
    uporabnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    uporabnik.hashed_geslo = novo_geslo

    db.session.commit()

    return 200

def preveri_uporabnika(email):
    uporabnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    if uporabnik is None:
        return 400
	
    if uporabnik.potrjen == False:
    	return 400

    return 200

def shrani_dostopni_zeton(email, cas_dostopa):
    uporabnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    uporabnik.dostopni_zeton = cas_dostopa

    db.session.commit()

    return 200

def poslji_email(email, key):

	naslov = "http://127.0.0.1:5000/potrditev_racuna/" + key
	port = 465

	sender = "skrbnik.straightas@gmail.com"
	password = "SkrbnikTpo"

	recieve = email

	message0 = """\
MIME-Version: 1.0
Content-type: text/html
Subject: StraightAs - Prosimo, potrdite registracijo

<p>Prosimo, kliknite na tale <a href=""" + naslov + """>GUMB</a>, da potrdite svojo registracijo.</p>
<br />
<p>Lep pozdrav.</p>
"""

	context = ssl.create_default_context()

	print("Starting to send")
	with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
		server.login(sender, password)
		server.sendmail(sender, recieve, message0)

	print("sent email!")

def pohendlaj_vip(uporabnik):
	email = uporabnik.email

	domene = email.split('@')[1]
	domena = domene.split('.')[0]

	if domena == "straightas":
		uporabnik.potrjen = True
		return True

	return False

def potrdi_racun(key):
	try:
		uporabnik = Uporabnik.query.filter(Uporabnik.dostopni_zeton == key).first()
		uporabnik.potrjen = True
		db.session.commit()
		return 200
	except:
		return 400
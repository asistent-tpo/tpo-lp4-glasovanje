from modules import Dogodek
from flask import Response

def preberi_dogodke():
    return Dogodek.preberi_dogodke()

def objavi_dogodek(ime, datum, organizator, opis):
    return Dogodek.objavi_dogodek(ime, datum, organizator, opis)

def avtenticiraj_upravljalca(email):
    return Dogodek.preveri_veljavnost_upravljalca(email)

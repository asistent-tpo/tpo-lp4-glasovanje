from modules import Todo
from flask import Response

def preberi_todoje(email):
    return Todo.preberi_todoje(email)

def dodaj_todo_polje(email):
    return Todo.ustvari_vnos(email)

def izbrisi_vnos_v_todo(email, id):
    ujemanje = Todo.preveri_ujemanje_email_in_id(email, id)

    if ujemanje != 200:
        return Response("{'msg':'id in email se ne ujemata. Oziroma, ta vnos v Todo ni v lasti tega email računa.'}", status=ujemanje, mimetype='application/json')
    
    return Todo.izbrisi_vnos(email, id)

def posodobi_vnos_v_todo(email, id, opis):
    ujemanje = Todo.preveri_ujemanje_email_in_id(email, id)

    if ujemanje != 200:
        return Response("{'msg':'id in email se ne ujemata. Oziroma, ta vnos v Todo ni v lasti tega email računa.'}", status=ujemanje, mimetype='application/json')

    return Todo.posodobi_vnos(email, id, opis)
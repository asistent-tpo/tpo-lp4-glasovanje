from modules import Avtobus
from flask import Response

def prikazi_seznam_avtobusov(ime_postaje):
    body =  Avtobus.pridobi_podatke_postaje(ime_postaje)
    return Response(body, status=200, mimetype='application/json')

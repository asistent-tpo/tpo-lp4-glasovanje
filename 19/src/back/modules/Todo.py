from flask import jsonify

from modules.database import db
from modules.database import Todo, Uporabnik
from modules.database import todo_shema
from modules.database import todoji_shema

def preberi_todoje(email):
    '''
    vsi_todoji = Todo.query.all()
    result = todoji_shema.dump(vsi_todoji)
    return jsonify(result.data)
    '''
    lastnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    result = todoji_shema.dump(lastnik.todoji)
    return jsonify(result.data)


def ustvari_vnos(email):
    lastnik = Uporabnik.query.filter(Uporabnik.email == email).first()
    novi_vnos = Todo(email=email, opis='[prazno]', lastnik=lastnik)

    db.session.add(novi_vnos)
    db.session.commit()

    return todo_shema.jsonify(novi_vnos)

def izbrisi_vnos(email, id):
    vnos = Todo.query.get(id)
    
    db.session.delete(vnos)
    db.session.commit()

    return todo_shema.jsonify(vnos)

def preveri_ujemanje_email_in_id(email, id):
    vnos = Todo.query.get(id)

    if ((vnos is None) or (vnos.email != email)):
        return 400
    return 200

def posodobi_vnos(email, id, opis):
    vnos = Todo.query.get(id)
    vnos.opis = opis
    db.session.commit()

    return todo_shema.jsonify(vnos)
from modules import Uporabnik
from flask import Response, jsonify
import datetime
import json

def preberi_uporabnike():
    return Uporabnik.preberi_uporabnike()

def preberi_uporabnika(id):
    return Uporabnik.preberi_uporabnika(id)

def izbrisi_uporabnika(id):
    return Uporabnik.izbrisi_uporabnika(id)

def izvedi_registracijo(email, geslo1, geslo2):
    ze_obstaja = Uporabnik.preveri_veljavnost_emaila(email)
    if ze_obstaja == 400:
        return Response("{'msg':'Email ni veljaven.'}", status=ze_obstaja, mimetype='application/json')

    neujemanje = Uporabnik.preveri_veljavnost_gesla(geslo1, geslo2)
    if neujemanje == 400:
        return Response("{'msg':'Geslo se ne ujemata.'}", status=neujemanje, mimetype='application/json')
    
    return Uporabnik.izvedi_registracijo(email, geslo1, "zetoncek")

def spremeni_geslo(email, staro_geslo, novo_geslo):
    veljavnost = Uporabnik.preveri_geslo(email, staro_geslo)

    if veljavnost == 400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')
    if veljavnost == 401:
        return Response("{'msg':'Napacno geslo.'}", status=veljavnost, mimetype='application/json')
    
    # Python ne podpira overloadanja funkcij, zato damo dva ista parametra
    veljavnost = Uporabnik.preveri_veljavnost_gesla(novo_geslo, novo_geslo)
    if veljavnost == 400:
        return Response("{'msg':'Novo geslo ne ustreza pogojem.'}", status=veljavnost, mimetype='application/json')
    
    veljavnost = Uporabnik.spremeni_geslo(email, novo_geslo)
    if veljavnost == 400:
        return Response("{'msg':'Neznana napaka. Geslo ni bilo spremenjeno.'}", status=veljavnost, mimetype='application/json')
    return Response("{'msg':'Geslo je bilo uspesno spremenjeno.'}", status=veljavnost, mimetype='application/json')

def izvedi_prijavo(email, geslo):
    veljavnost = Uporabnik.preveri_uporabnika(email)

    if veljavnost == 400:
        return Response("Email ni veljaven", status=veljavnost, mimetype='application/json')
    
    veljavnost = Uporabnik.preveri_geslo(email, geslo)

    if veljavnost != 200:
        return Response("{'msg':'Napacno geslo.'}", status=veljavnost, mimetype='application/json')
    
    cas_dostopa = datetime.datetime.now() + datetime.timedelta(hours = 1)
    Uporabnik.shrani_dostopni_zeton(email, cas_dostopa)

    # cas dostopa je v nasem primeru tudi dostopni piskotek, saj dokumentacija ne definira, 
    # kaj dostopni piskotek sploh je, podlagi cesa se generira, kam se shrani in kako se z njim izvaja avtentikacijo uporabnika
    dostopni_piskotek = cas_dostopa

    resp = Response("{'msg':'Prijava uspesna.'}", status=veljavnost, mimetype='application/json')
    resp.set_cookie("expires", bytes(str(dostopni_piskotek), 'utf-8'), expires=dostopni_piskotek)

    return resp

def avtenticiraj_sejo(email, zeton):
    veljavnost = Uporabnik.preveri_uporabnika(email)

    # TODO Konca verzija:
    # preveri se zeton, ce je pravi (uporabnik je prijavljen) = avtorizacija, in ce je seja se vedno veljavna

    return veljavnost

def potrdi_racun(key):
    return Uporabnik.potrdi_racun(key)

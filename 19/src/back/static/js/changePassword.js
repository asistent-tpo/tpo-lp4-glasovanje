$(document).ready(function(){
	$("#change").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var password = $("#password").val();
		var password2 = $("#password2").val();
		// Checking for blank fields.
		if( password =='' || password2 ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {email: email1, staro_geslo: password, novo_geslo: password2};
			$.ajax({
				url:"http://127.0.0.1:5000/register",
				type:"PUT",
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					alert( "Uspešna sprememba" );
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			/*	statusCode: {
					400: function(data, status) {
					  alert( "Novo geslo ni primerno." );
					},
					401: function(data, status) {
					  alert( "Napačno trenutno geslo" );
					},
					200: function() {
					  alert( "Uspešna sprememba" );
					  window.location.replace("http://127.0.0.1:5000/home.html");
					}
				}*/
			});
		}
	});
});
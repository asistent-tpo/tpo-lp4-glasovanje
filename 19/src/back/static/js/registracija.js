$(document).ready(function(){
	$("#register").click(function(){
		var email1 = $("#email").val();
		var password = $("#password").val();
		var password2 = $("#password2").val();
		// Checking for blank fields.
		if( email1 =='' || password =='' || password2 ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {email: email1, geslo1: password, geslo2: password2};
			$.ajax({
				url:"http://127.0.0.1:5000/register",
				type:"POST",
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešna registracija" );
					  window.location.replace("http://127.0.0.1:5000/prijava.html");
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
});
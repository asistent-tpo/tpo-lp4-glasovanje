$(document).ready(function(){
	$("#login").click(function(){
		var email1 = $("#email").val();
		var password = $("#password").val();
		// Checking for blank fields.
		if( email1 =='' || password ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {email: email1, geslo: password};
			$.ajax({
				url:"http://127.0.0.1:5000/login",
				type:"POST",
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					//alert(data);
					alert( "Uspešna prijava" );
					sessionStorage.setItem("priEmail", email1);
					window.location.replace("http://127.0.0.1:5000/home.html");
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
				/*statusCode: {
					400: function() {
						alert( "Neuspešna prijava" );
					},
					200: function() {
						alert( "Uspešna prijava" );
						sessionStorage.setItem("priEmail", email1);
						window.location.replace("http://127.0.0.1:5000/home.html");
					}
				}*/
			});
		}
	});
});
$(document).ready(function(){
	$("#ustvari").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var date = $("#date").val();
		var opis = $("#opis").val();
		var name = $("#name").val();
		// Checking for blank fields.
		if( date =='' || opis =='' || name ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {ime: name, datum: date, opis: opis};
			$.ajax({
				url:"http://127.0.0.1:5000/koledar",
				type:"POST",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešno dodano" );
					 prikaziVseKoledar(email1);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
	
	$("#prikaziVse").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		prikaziVseKoledar(email1);
	});	
	
	$("#koledarSpremeni").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var date = $("#date").val();
		var opis = $("#opis").val();
		var name = $("#name").val();
		// Checking for blank fields.
		if( date =='' || opis =='' || name ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {ime: name, datum: date, opis: opis};
			$.ajax({
				url:"http://127.0.0.1:5000/koledar/"+$("#koledarID").text(),
				type:"PUT",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešno spremenjeno" );
					 prikaziVseKoledar(email1);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
	
	$("#koledarIzbrisi").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var opis = $("#todoOpis").val();
		// Checking for blank fields.
		var data =  {'opis': opis};
		$.ajax({
			url:"http://127.0.0.1:5000/koledar/"+$("#koledarID").text(),
			type:"DELETE",
			headers: {
				'From':email1
			},
			data:JSON.stringify(data),
			contentType:"application/json",
			dataType:"text",
			success: function(data){
				 alert( "Uspešno izbrisano" );
				 prikaziVseKoledar(email1);
			},
			error: function(xhr, status, error) {
				alert(xhr.responseText);
			}
		});
	});
	
	$("#koledarTableBody").on('click', '.koledarElem', function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		$("#koledarID").text($(this).attr('data-id'));
		//alert($(this).attr('data-id'));

	});
	
	
	$("#todoUstvari").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var opis = $("#todoOpis").val();
		// Checking for blank fields.
		var data =  {'kaj': 'name'};
		if( opis ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			$.ajax({
				url:"http://127.0.0.1:5000/todo",
				type:"POST",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešno dodano" );
					 prikaziVseTODO(email1);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
	
	$("#todoSpremeni").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var opis = $("#todoOpis").val();
		// Checking for blank fields.
		var data =  {'opis': opis};
		if( opis ==''){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			$.ajax({
				url:"http://127.0.0.1:5000/todo/"+$("#todoID").text(),
				type:"PUT",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešno spremenjeno" );
					 prikaziVseTODO(email1);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
	
	$("#todoIzbrisi").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var opis = $("#todoOpis").val();
		// Checking for blank fields.
		var data =  {'opis': opis};
		$.ajax({
			url:"http://127.0.0.1:5000/todo/"+$("#todoID").text(),
			type:"DELETE",
			headers: {
				'From':email1
			},
			data:JSON.stringify(data),
			contentType:"application/json",
			dataType:"text",
			success: function(data){
				 alert( "Uspešno izbrisano" );
				 prikaziVseTODO(email1);
			},
			error: function(xhr, status, error) {
				alert(xhr.responseText);
			}
		});
	});

		
	$("#todoPrikaziVse").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		prikaziVseTODO(email1)

	});
	
	$("#todoTableBody").on('click', '.todoElem', function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		$("#todoID").text($(this).attr('data-id'));
		//alert($(this).attr('data-id'));

	});
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$("#urnikUstvari").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var ime = $("#urnikIme").val();
		var trajanje = $("#urnikTrajanje").val();
		var barva = $("#urnikBarva").val();
		var dan = $("#urnikDan").val();
		var ura = $("#urnikUra").val();
		// Checking for blank fields.
		var data =  {'ime': ime, 'trajanje': trajanje,'barva': barva,'dan': dan,'ura': ura};
		if( ime =='' || trajanje =='' || barva =='' || dan =='' || ura =='' ){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			$.ajax({
				url:"http://127.0.0.1:5000/urnik",
				type:"POST",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešno dodano" );
					 prikaziVseurnik(email1);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
	
	$("#urnikSpremeni").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var ime = $("#urnikIme").val();
		var trajanje = $("#urnikTrajanje").val();
		var barva = $("#urnikBarva").val();
		var dan = $("#urnikDan").val();
		var ura = $("#urnikUra").val();
		// Checking for blank fields.
		var data =  {'ime': ime, 'trajanje': trajanje,'barva': barva,'dan': dan,'ura': ura};
		if( ime =='' || trajanje =='' || barva =='' || dan =='' || ura =='' ){
			$('input[type="text"],input[type="password"]').css("border","2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			$.ajax({
				url:"http://127.0.0.1:5000/urnik/"+$("#urnikID").text(),
				type:"PUT",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					 alert( "Uspešno spremenjeno" );
					 prikaziVseurnik(email1);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
	});
	
	$("#urnikIzbrisi").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var opis = $("#urnikOpis").val();
		// Checking for blank fields.
		var data =  {'opis': opis};
		$.ajax({
			url:"http://127.0.0.1:5000/urnik/"+$("#urnikID").text(),
			type:"DELETE",
			headers: {
				'From':email1
			},
			data:JSON.stringify(data),
			contentType:"application/json",
			dataType:"text",
			success: function(data){
				 alert( "Uspešno izbrisano" );
				 prikaziVseurnik(email1);
			},
			error: function(xhr, status, error) {
				alert(xhr.responseText);
			}
		});
	});

		
	$("#urnikPrikaziVse").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		prikaziVseurnik(email1)

	});
	
	$("#urnikTableBody").on('click', '.urnikElem', function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		$("#urnikID").text($(this).attr('data-id'));
		//alert($(this).attr('data-id'));

	});
});

function prikaziVseurnik(email1) {
	$.ajax({
			url:"http://127.0.0.1:5000/urnik",
			type:"GET",
			headers: {
				'From':email1
			},
			contentType:"application/json",
			dataType:"text",
			success: function(data){
				var parsedJSON = JSON.parse(data);
				$("#urnikTableBody").html("<tr><th>Dan</th><th>Trajanje</th><th>Ura</th><th>Ime</th></tr>");
				
				for (var i=0;i<parsedJSON.length;i++) {
					//alert(parsedJSON[i].ime);
					//$('#koledarTabela').html(data);
					$('#urnikTabela > tbody:last-child').append('<tr bgcolor=\"'+parsedJSON[i].barva+'\"><td>'+parsedJSON[i].dan+'</td>\
						<td>'+parsedJSON[i].trajanje+'</td><td>'+parsedJSON[i].ura+'</td><td><button class="urnikElem" data-id=\"'+parsedJSON[i].id+'\">'+parsedJSON[i].ime+'</button></td></tr>');
				}
				
			},
			error: function(xhr, status, error) {
				alert(xhr.responseText);
			}
		});
}

function prikaziVseTODO(email1) {
	$.ajax({
			url:"http://127.0.0.1:5000/todo",
			type:"GET",
			headers: {
				'From':email1
			},
			contentType:"application/json",
			dataType:"text",
			success: function(data){
				posodobiTODO(data);
				
			},
			error: function(xhr, status, error) {
				alert(xhr.responseText);
			}
		});
}

function posodobiTODO(data) {
	var parsedJSON = JSON.parse(data);
	$("#todoTableBody").html("<tr><th>Opis</th></tr>");
	
	for (var i=0;i<parsedJSON.length;i++) {
		//alert(parsedJSON[i].ime);
		//$('#koledarTabela').html(data);
		$('#todoTabela > tbody:last-child').append('<tr><td><button class="todoElem" data-id=\"'+parsedJSON[i].id+'\">'+parsedJSON[i].opis+'</button></td></tr>');
	}
}

function prikaziVseKoledar(email1) {
	$.ajax({
			url:"http://127.0.0.1:5000/koledar",
			type:"GET",
			headers: {
				'From':email1
			},
			contentType:"application/json",
			dataType:"text",
			success: function(data){
				var parsedJSON = JSON.parse(data);
				$("#koledarTableBody").html("<tr><th>Datum</th><th>Ime</th><th>Opis</th></tr>");
				
				for (var i=0;i<parsedJSON.length;i++) {
					//alert(parsedJSON[i].ime);
					//$('#koledarTabela').html(data);
					$('#koledarTabela > tbody:last-child').append('<tr><td>'+parsedJSON[i].datum+'</td>\
						<td><button class="koledarElem" data-id=\"'+parsedJSON[i].id+'\">'+parsedJSON[i].ime+'</button></td><td>'+parsedJSON[i].opis+'</td></tr>');
				}
			},
			error: function(xhr, status, error) {
				alert(xhr.responseText);
			}
		});
}
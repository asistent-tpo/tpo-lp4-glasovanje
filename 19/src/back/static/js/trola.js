$(document).ready(function(){
	$("#trola").click(function(){
		var postaja = $("#postaja").val();

		// Checking for blank fields.
		if( postaja ==''){
			$('input[type="text"]').css("border","2px solid red");
			$('input[type="text"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {ime_postaje: postaja};
			$.ajax({
				url:"http://127.0.0.1:5000/avtobus",
				type:"POST",
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					//alert(data);
					$('#trolaPodatki').html(data);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
				/*statusCode: {
					400: function() {
					  alert( "Neobstoječa postaja" );
					},
					200: function(data) {
						alert(data);
					  
					}
				}*/
			});
		}
	});
});
$(document).ready(function(){
	$("#submitDogodek").click(function(){
		var email1 = sessionStorage.getItem("priEmail");
		var imeDogodka = $("#imeDogodka").val();
		var datumDogodka = $("#datumDogodka").val();
		var opisDogodka = $("#opisDogodka").val();
		// Checking for blank fields.
		if( imeDogodka =='' || opisDogodka =='' || datumDogodka == ''){
			$('input[type="text"]').css("border","2px solid red");
			$('input[type="text"]').css("box-shadow","0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}else {
			var data =  {ime: imeDogodka, datum: datumDogodka, opis: opisDogodka};
			$.ajax({
				url:"http://127.0.0.1:5000/dogodek",
				type:"POST",
				headers: {
					'From':email1
				},
				data:JSON.stringify(data),
				contentType:"application/json",
				dataType:"text",
				success: function(data){
					//alert(data);
					alert( "Uspešno dodan dogodek" );
					console.log("Uspeh");
					window.location.replace("http://127.0.0.1:5000/events.html");
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
				/*statusCode: {
					400: function() {
						alert( "Neuspešna prijava" );
					},
					200: function() {
						alert( "Uspešna prijava" );
						sessionStorage.setItem("priEmail", email1);
						window.location.replace("http://127.0.0.1:5000/home.html");
					}
				}*/
			});
		}
	});
});
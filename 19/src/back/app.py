from flask import Flask, request, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from modules import KrmilnikZaAvtentikacijo
from modules import KrmilnikZaTodo, KrmilnikZaKoledar, KrmilnikZaAvtobuse, KrmilnikZaDogodke, KrmilnikZaUrnik

import os

# Init app
app = Flask(__name__, static_url_path='')
basedir = os.path.abspath(os.path.dirname(__file__))

# Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Init ma
ma = Marshmallow(app)


# --- UPORABNIKI ---

@app.route('/')
def root():
	#return KrmilnikZaAvtentikacijo.preberi_uporabnike()
    return app.send_static_file('index.html')


''' # ODKOMENTIRAJ ZGOLJ ZA TESTIRANJE!!!


# Preberi vse uporabnike
@app.route('/uporabnik', methods=['GET'])
def get_products():
    return KrmilnikZaAvtentikacijo.preberi_uporabnike()

# Preberi enega uporabnika
@app.route('/uporabnik/<id>', methods=['GET'])
def get_product(id):
    return KrmilnikZaAvtentikacijo.preberi_uporabnika(id)

# Izbrisi uporabnika
@app.route('/uporabnik/<id>', methods=['DELETE'])
def delete_product(id):
    return KrmilnikZaAvtentikacijo.izbrisi_uporabnika(id)

'''


# Registriraj uporabnika
@app.route('/register', methods=['POST'])
def add_product():
    email = request.json['email']
    geslo1 = request.json['geslo1']
    geslo2 = request.json['geslo2']

    return KrmilnikZaAvtentikacijo.izvedi_registracijo(email, geslo1, geslo2)


# Spremeni geslo
@app.route('/register', methods=['PUT'])
def spremeni_geslo():
    email = request.json['email']
    staro_geslo = request.json['staro_geslo']
    novo_geslo = request.json['novo_geslo']

    return KrmilnikZaAvtentikacijo.spremeni_geslo(email, staro_geslo, novo_geslo)

# Prijava uporabnika
@app.route('/login', methods=['POST'])
def prijava():
    email = request.json['email']
    geslo = request.json['geslo']

    return KrmilnikZaAvtentikacijo.izvedi_prijavo(email, geslo)


# --- T0D0 SEZNAMI ---

# Vrne Todo seznam
@app.route('/todo', methods=['GET'])
def preberi_todoje():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaTodo.preberi_todoje(email)

# Dodaj vnos v Todo
@app.route('/todo', methods=['POST'])
def dodaj_todo():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaTodo.dodaj_todo_polje(email)

# Izbrisi vnos v Todo
@app.route('/todo/<id>', methods=['DELETE'])
def izbrisi_todo(id):
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaTodo.izbrisi_vnos_v_todo(email, id)


# Posodobi vnos v Todo
@app.route('/todo/<id>', methods=['PUT'])
def spremeni_todo(id):
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')
    opis = request.json['opis']

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaTodo.posodobi_vnos_v_todo(email, id, opis)

# --- KOLEDAR ---

# Vrne Todo seznam
@app.route('/koledar', methods=['GET'])
def preberi_koledar():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaKoledar.preberi_koledarje(email)


# Dodaj vnos v Koledar
@app.route('/koledar', methods=['POST'])
def dodaj_koledar():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    ime = request.json['ime']
    datum = request.json['datum']
    opis = request.json['opis']

    return KrmilnikZaKoledar.dodajanje_vnosa_v_koledar(email, ime, datum, opis)

# Posodobi vnos v Koledar
@app.route('/koledar/<id>', methods=['PUT'])
def posodobi_koledar(id):
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')
    opis = request.json['opis']
    datum = request.json['datum']
    ime = request.json['ime']

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaKoledar.posodobi_vnos_v_koledarju(email, id, ime, datum, opis)

# Izbrisi vnos v Koledaru
@app.route('/koledar/<id>', methods=['DELETE'])
def brisanje_vnosa_iz_koledarja(id):
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaKoledar.izbrisi_vnos_iz_koledarja(email, id)

#---------- URNIK------------

# Dodaj vnos v Urnik
@app.route('/urnik', methods=['POST'])
def dodaj_urnik():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    ime = request.json['ime']
    trajanje = request.json['trajanje']
    barva = request.json['barva']
    dan = request.json['dan']
    ura = request.json['ura']

    return KrmilnikZaUrnik.dodajanje_vnosa_v_urniku(email, ime,  trajanje, barva, dan, ura)


# Vrne Urnik seznam
@app.route('/urnik', methods=['GET'])
def preberi_urnik():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaUrnik.preberi_urnike(email)

# Posodobi vnos v Urnik
@app.route('/urnik/<id>', methods=['PUT'])
def posodobi_urnik(id):
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    ime = request.json['ime']
    trajanje = request.json['trajanje']
    barva = request.json['barva']
    dan = request.json['dan']
    ura = request.json['ura']

    return KrmilnikZaUrnik.posodobi_vnos_v_urniku(email, id, ime, trajanje, barva, dan, ura)

# Izbrisi vnos v Urnika
@app.route('/urnik/<id>', methods=['DELETE'])
def brisanje_vnosa_iz_urnika(id):
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaUrnik.izbrisi_vnos_iz_urnika(id)
#----------------- AVTOBUSI --------------------
# Vrne Todo seznam
@app.route('/avtobus', methods=['POST'])
def preberi_avtobuse():
    ime_postaje = request.json['ime_postaje']

    return KrmilnikZaAvtobuse.prikazi_seznam_avtobusov(ime_postaje)


#------------------- DOGODKI ------------------


# Vrne seznam dogodkov
@app.route('/dogodek', methods=['GET'])
def preberi_dogodke():

    return KrmilnikZaDogodke.preberi_dogodke()

# Dodaj vnos v Todo
@app.route('/dogodek', methods=['POST'])
def dodaj_dogodek():
    email = request.headers.get('From')
    zeton = request.headers.get('Cookie')

    ime = request.json['ime']
    datum = request.json['datum']
    organizator = email
    opis = request.json['opis']

    veljavnost = KrmilnikZaAvtentikacijo.avtenticiraj_sejo(email, zeton)

    if veljavnost==400:
        return Response("{'msg':'Email ni veljaven.'}", status=veljavnost, mimetype='application/json')
    
    veljavnost = KrmilnikZaDogodke.avtenticiraj_upravljalca(email)

    if veljavnost==400:
        return Response("{'msg':'Avtentikacija neuspesna'}", status=veljavnost, mimetype='application/json')

    return KrmilnikZaDogodke.objavi_dogodek(ime, datum, organizator, opis)


# ------------------POTRDITEV RACUNA-----------------------------
# Vrne seznam dogodkov
@app.route('/potrditev_racuna/<key>', methods=['GET'])
def potrdi_racun(key):
    uspesnost = KrmilnikZaAvtentikacijo.potrdi_racun(key)

    if uspesnost == 200:
        return Response("<h2>Potrditev racuna je bila uspesna.</h3>", status=uspesnost, mimetype='text/html')
    else:
        return Response("<h2>Prislo je do napake pri preverjanju racuna!</h3>", status=uspesnost, mimetype='text/html')

# Run Server
if __name__ == '__main__':
    app.run(debug=True)
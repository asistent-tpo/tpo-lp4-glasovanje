var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();

randomTaskDescription = Math.random().toString(36).slice(2, 7);
var myFirstTaskId;

describe('Začetna stran', function() {
  it ('Prikaži zavihek Home, ki je aktiven', function(done) {
    zahteva(streznik).get('/').expect(/<li id="\homeButton\" class=\"active\"><a href=\"index\">Home<\/a><\/li>/i, done);
  });
});

describe('Dodajanje TODO-ja', function() {
  it ('Dodaj naključno generiran TODO task', function(done) {
    setTimeout(function() {
			let session = null;
			zahteva(streznik)  
			.post('/login')  
			.send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
			.end((err, res) => {  
				if (err) {
					return done(err);
				}
				
				session = res.header['set-cookie'];
				console.log("testi_pwchange.js:", res.header['set-cookie']);
				
				data = {text: randomTaskDescription};
        zahteva(streznik).post('/add-task').set('Cookie', session).type('form').send(data).expect(/OK/i, done);
				});
			}, 0);
    });
});


describe('Pridobivanje TODO-jev', function() {
  var	myFirstTaskId = 5;
  it ('Preveri ali je naključno generiran TODO task pod vrnjenimi TODO-ji', function(done) {
  setTimeout(function() {
    zahteva(streznik)  
    .post('/login')  
    .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
    .end((err, res) => {  
      if (err) {
        return done(err);
      }
      
      session = res.header['set-cookie'];
      console.log("testi_pwchange.js:", res.header['set-cookie']);
      
    zahteva(streznik)
    .post('/get-tasks', {})
    .set('Cookie', session)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) done(err);
      var myFirstTask = res.body.find( function(todo){
        myFirstTaskId = todo.taskId;
        return todo.taskName == randomTaskDescription;
    });
    console.log(myFirstTask)
      assert.notEqual(undefined, myFirstTask);
       });
       done();
      });

  }, 2000);
  });


it ('Izbrisi ta TODO', function(done) {
  setTimeout(function() {
    let session = null;
    zahteva(streznik)  
    .post('/login')  
    .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
    .end((err, res) => {  
      if (err) {
        return done(err);
      }
      
      session = res.header['set-cookie'];
      console.log("testi_pwchange.js:", res.header['set-cookie']);
      
      data = {id: myFirstTaskId};
      zahteva(streznik).post('/del-task').set('Cookie', session).type('form').send(data).expect(/OK/i, done);
      });

  }, 4000);

  });

  it ('Preveri ali je naključno generiran TODO task pod vrnjenimi TODO-ji - zdaj ne sme biti', function(done) {
    setTimeout(function() {
      let session = null;
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        zahteva(streznik)
        .post('/get-tasks', {})
        .set('Cookie', session)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) done(err);
          var myFirstTask = res.body.find( function(todo){
            return todo.taskName == randomTaskDescription;
        });
        should.not.exist(myFirstTask);
            });
        done();
        });
  
    }, 6000);
    
  });
  
});
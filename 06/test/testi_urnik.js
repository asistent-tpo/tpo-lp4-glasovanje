var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

randomScheduleName = Math.random().toString(4).slice(2, 5);
randomScheduleDay = getRandomInt(0, 6);
randomScheduleHour = getRandomInt(0, 18);
randomScheduleDuration = 1;

const colorNames = ["Red", "Green", "Gray", "Yellow", "Blue"];

randomColorName = colorNames[getRandomInt(0, 4)];


describe('Pridobivanje predmetov urnika', function() {
  var	myFirstScheduleId = 5;

  it ('Dodaj naključnen predmet na urnik', function(done) {
    setTimeout(function() {
			let session = null;
			zahteva(streznik)  
			.post('/login')  
			.send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
			.end((err, res) => {  
				if (err) {
					return done(err);
				}
				
				session = res.header['set-cookie'];
				console.log("testi_pwchange.js:", res.header['set-cookie']);
				
				data = {name: randomScheduleName, duration: randomScheduleDuration, colour: randomColorName, day: randomScheduleDay, hour: randomScheduleHour};
        zahteva(streznik).post('/add-schedule-event').set('Cookie', session).type('form').send(data).expect(/OK/i, done);
				});
			}, 0);
    });

  it ('Preveri ali je naključno generiran predmet pod vrnjenimi predmeti', function(done) {
    setTimeout(function () {
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh" })  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        zahteva(streznik)
        .post('/get-schedule', {})
        .set('Cookie', session)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) done(err);
          var myFirstSchedule = res.body.find( function(schedule){
            return schedule.scheduleName == randomScheduleName;
        });
        console.log(myFirstSchedule)
          assert.notEqual(undefined, myFirstSchedule);
         });
         done();
        });
  
    }, 2000);
    });

  it ('Izbrisi ta predmet iz urnika', function(done) {
    setTimeout(function () {
      let session = null;
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        data = {day: randomScheduleDay, hour: randomScheduleHour};
        zahteva(streznik).post('/del-schedule').set('Cookie', session).type('form').send(data).expect(/OK/i, done);
        });
  
    }, 4000);
  
    });

  it ('Preveri ali je naključno generiran predmet pod vrnjenimi predmeti - zdaj ne sme biti', function(done) {
    setTimeout(function () {
      let session = null;
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        zahteva(streznik)
        .post('/get-schedule', {})
        .set('Cookie', session)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) done(err);
          var myFirstSchedule = res.body.find( function(schedule){
            return schedule.scheduleName == randomScheduleName;
        });
        console.log(myFirstSchedule)
        should.not.exist(myFirstSchedule);
            });
        done();
        });
  
    }, 6000);
    
  });
  
});
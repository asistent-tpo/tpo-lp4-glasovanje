var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();

describe('Prijava', function() {
	it ('Pridobitev strane za prijavo', function(done) {
		zahteva(streznik).get('/login').expect(/<div id="login-page">/i, done);
	});
	
	it ('Prijava: napacen email', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/login').
			send({email: "NotAnEmail", pw: "asdasd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(false, res.body.value);
			});
			
			done();
		}, 1750);
	});
	
	it ('Prijava: napacno geslo', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/login').
			send({email: "lm8235@student.uni-lj.si", pw: "asd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(false, res.body.value);
			});
			
			done();
		}, 1750);
	});
	
	it ('Prijava: pravilna', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/login').
			send({email: "lm8235@student.uni-lj.si", pw: "asdasdasd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(true, res.body.value);
			});
			
			done();
		}, 1750);
	});
});
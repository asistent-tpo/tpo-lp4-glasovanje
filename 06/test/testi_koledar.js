var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

randomEventName = Math.random().toString(5).slice(2, 7);
randomEventTime = getRandomInt(0, 25);
randomEventDuration = getRandomInt(0, 25);
randomEventDescription = Math.random().toString(36).slice(2, 9);

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const d = new Date();
day = Math.abs((d.getDay() - getRandomInt(0, 28)) % 28);
month = monthNames[d.getMonth()];
year = d.getFullYear();
randomEventDate = day + "." + month + "." + year;

describe('Dodajanje dogodka na koledarju', function() {
  it ('Dodaj naključnen dogodek na koledarju', function(done) {
    setTimeout(function() {
			let session = null;
			zahteva(streznik)  
			.post('/login')  
			.send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
			.end((err, res) => {  
				if (err) {
					return done(err);
				}
				
				session = res.header['set-cookie'];
				console.log("testi_pwchange.js:", res.header['set-cookie']);
				
				data = {name: randomEventName, time: randomEventTime, duration: randomEventDuration, description: randomEventDescription, date: randomEventDate};
        zahteva(streznik).post('/add-calendar-event').set('Cookie', session).type('form').send(data).expect(/OK/i, done);
				});
			}, 0);
    });
});

describe('Pridobivanje dogodkov koledarja', function() {
  var	myFirstEventId = 5;
  it ('Preveri ali je naključno generiran event pod vrnjenimi eventi', function(done) {
    setTimeout(function () {
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        zahteva(streznik)
        .post('/get-calendar-events', {})
        .set('Cookie', session)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) done(err);
          var myFirstEvent = res.body.find( function(event){
            myFirstEventId = event.eventId;
            return event.eventName == randomEventName;
        });
        console.log(myFirstEvent)
          assert.notEqual(undefined, myFirstEvent);
         });
         done();
        });
  
    }, 2000);
    });

  it ('Izbrisi ta dogodek iz koledarja', function(done) {
    setTimeout(function () {
      let session = null;
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        data = {id: myFirstEventId};
        zahteva(streznik).post('/del-calendar-event').set('Cookie', session).type('form').send(data).expect(/OK/i, done);
        });
  
    }, 4000);
  
    });

  it ('Preveri ali je naključno generiran dogodek koledarja pod vrnjenimi dogodki koledarja - zdaj ne sme biti', function(done) {
    setTimeout(function () {
      let session = null;
      zahteva(streznik)  
      .post('/login')  
      .send({email: "maj.bajuk@outlook.com", pw: "kruh"})  
      .end((err, res) => {  
        if (err) {
          return done(err);
        }
        
        session = res.header['set-cookie'];
        console.log("testi_pwchange.js:", res.header['set-cookie']);
        
        zahteva(streznik)
        .post('/get-calendar-events', {})
        .set('Cookie', session)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) done(err);
          var myFirstEvent = res.body.find( function(event){
            return event.eventName == randomEventName;
        });
        should.not.exist(myFirstEvent);
            });
        done();
        });
  
    }, 6000);
    
  });
  
});
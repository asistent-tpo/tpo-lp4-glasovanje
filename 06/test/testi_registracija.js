var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();

describe('Registracija', function() {
	it ('Pridobitev strane za registracijo', function(done) {
		zahteva(streznik).get('/register').expect(/<div id="register-page">/i, done);
	});
	
	it ('Registracija: nepravilen email', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/register').
			send({email: "NotAnEmail", pw: "asdasd", pw2: "asdasd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(false, res.body.value);
			});
			
			done();
		}, 1750);
	});
	
	it ('Registracija: nepravilno geslo', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/register').
			send({email: "tpo.straightas@gmail.com", pw: "pw", pw2: "pw"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(false, res.body.value);
			});
			
			done();
		}, 1750);
	});
	
	it ('Registracija: gesla se ne ujemata', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/register').
			send({email: "tpo.straightas@gmail.com", pw: "asdasdas", pw2: "asdasdasd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(false, res.body.value);
			});
			
			done();
		}, 1750);
	});
  
	it ('Registracija: pravilna', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/register').
			send({email: "tpo.straightas@gmail.com", pw: "asdasdasd", pw2: "asdasdasd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(true, res.body.value);
			});
			
			done();
		}, 1750);
	});
	
	it ('Registracija: ponovna', function(done) {
		setTimeout(function() {
			zahteva(streznik).
			post('/register').
			send({email: "tpo.straightas@gmail.com", pw: "asdasd", pw2: "asdasd"}).
			set('Content-Type', 'application/json').
			set('Accept', 'application/json').
			expect('Content-Type', /json/).
			end(function(err, res) {
				if (err) done(err);
				else assert.equal(false, res.body.value);
			});
			
			done();
		}, 1750);
	});
});
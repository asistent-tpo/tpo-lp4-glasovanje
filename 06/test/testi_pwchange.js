var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();
var request = require('superagent');

describe('Sprememba gesla', function() {	
	it ('Sprememba gesla: neprijavljen uporabnik nima dostopa', function(done) {
		zahteva(streznik).get('/pwchange').
		expect('Location', '/login', done);
	});
	
	it ('Sprememba gesla: prijavljen uporabnik ima dostopa', function(done) {
		setTimeout(function() {
			let session = null;
			zahteva(streznik)  
			.post('/login')  
			.send({email: "lm8235@student.uni-lj.si", pw: "asdasdasd"})  
			.end((err, res) => {  
				if (err) {
					return done(err);
				}
				
				session = res.header['set-cookie'];
				console.log("testi_pwchange.js:", res.header['set-cookie']);
				
				zahteva(streznik).
				get('/pwchange').
				set('Cookie', session).
				send().
				expect(200).
				end((err, res) => {
					if (err) {
						return done(err);
					}
					
					assert.notEqual(-1, res.text.indexOf('id="pwchange-page"'));
					
					return done();
				});
			});
		}, 1750);
	});
});
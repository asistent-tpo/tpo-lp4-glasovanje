var event = require('../../common/Event.js');
var zeton = require('../../common/Zeton.js');
var status_code = require('../../common/StatusCode.js');

var RestaurantView = function () {
    this.init();
};

RestaurantView.prototype = {
    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable();
    },

    createChildren: function () {

        return this;
    },

    setupHandlers: function () {
        return this;
    },

    enable: function () {
        return this;
    },
    
};

module.exports = RestaurantView;
var NotifView = function () {
    this.init();
};



NotifView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable()
            .buildList();
    },

    createChildren: function () {
        // cache the document object
        this.$container = $('#event-page');
        this.$notifsContainer = this.$container.find('.js-events-container');
        this.$notifsContainerNotifList = $('.event_list');
        this.$notifsContainerCol1 = this.$notifsContainerNotifList.find('.events_col_1');
        this.$notifsContainerCol2 = this.$notifsContainerNotifList.find('.events_col_2');
        this.$notifsContainerCol3 = this.$notifsContainerNotifList.find('.events_col_3');
        
        return this;
    },

    setupHandlers: function () {
        //this.formCloseButtonHandler = this.formCloseButton.bind(this);
        this.deleteNotifButtonHandler = this.deleteNotifButton.bind(this);

        return this;
    },

    enable: function () {

        //this.$formCloseButton.click(this.formCloseButtonHandler);
        this.$container.on('click', '.xE', this.deleteNotifButtonHandler);

        return this;
    },

    deleteNotifButton: function (e) {
        //console.log('NotifView.js: delete Notifs was clicked');
        const self = this;
        //console.log('NotifView.js:  ' + self);
			$.ajax({
				type: 'POST',
				url: '/del-notif',
				data: {id: e.target.getAttribute("index")},
				success: function(result) {
                    //console.log('NotifView.js: delete Notifs was clicked');
                    console.log('Response:', result);
                    self.buildList();
				}
			}); 
            //console.log('NotifView.js: delete Notifs was clicked');
    },

    buildList: function () {
        const self = this;
        $.ajax({
            type: 'POST',
            url: '/get-notifs',
            data: {text: $('#fieldNotif').val()},
            success: function(notifs) {
                //console.log('Response:', notifs);
                var html = "";
                var $notifsContainer = self.$notifsContainer;
                var $notifsContainerCol2 = self.$notifsContainerCol2;
        
                $notifsContainerCol2.html('');
        
                var index = 0;
                for (var notif in notifs) {
                    var text = '<div class="eventE">\
                                    <div class="event_descE">\
                                        <button index ="' + notifs[notif].notifId + '" class = "xE" style="display: none">\
                                             X\
                                            </button>\
                                            <p class="opis">' + notifs[notif].notifName + '</p>\
                                    </div>\
                                </div>';

                    $notifsContainerCol2.prepend(text)
                    
                }
            }
        });

        $.ajax({
            type: 'POST',
            url: '/is-admin',
            data: { },
            success: function (res) {

                //console.log(res);
                //console.log(res.isManager);
                if (res.isAdmin == true) {
                    //console.log("isManager je čru");
                    var x = document.getElementsByClassName("xE");
                    var i;
                    for (i = 0; i < x.length; i++) {
                      x[i].style.display = "block";
                    }
               }
            }
        });

    },
	
    closeForm: function() {
        document.getElementById("myNotifForm").style.display = "none";
              document.getElementById("fieldNotif").value = ''
              this.buildList();
	}
};
new NotifView();

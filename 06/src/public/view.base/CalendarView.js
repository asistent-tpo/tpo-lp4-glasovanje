var CalendarView = function () {
    this.init();
};



CalendarView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable()
            .getEvents()
    },

    createChildren: function () {
        // cache the document object
        this.$enterEventContainer = $('.form-popupCalendar').find('.form-container');
        this.$editEventContainer = $('.form-popupCalendarEdit').find('.form-container');
        this.$container2 = $('.js-container2');
        this.$addEventButton = this.$container2.find('.js-add-calendar-button');
        this.$formAddEventButton = this.$enterEventContainer.find('.btnCalendar');
        this.$formChangeEventButton = this.$editEventContainer.find('.editBtnCalendar');
        this.$formCloseButton = this.$enterEventContainer.find('.cancelBtnCalendar');
        this.$editFormCloseButton = this.$editEventContainer.find('.cancelEditBtnCalendar');
        //this.$tasksContainer = this.$container.find('.js-tasks-container');
		//this.$tasksContainerEventList = $('.event_list');
		
        return this;
    },

    setupHandlers: function () {

        this.addEventButtonHandler = this.addEventButton.bind(this);
        this.formAddEventButtonHandler = this.formAddEventButton.bind(this);
        this.formChangeEventButtonHandler = this.formChangeEventButton.bind(this);
        this.editFormCloseButtonHandler = this.editFormCloseButton.bind(this);
        this.formCloseButtonHandler = this.formCloseButton.bind(this);
        this.editEventButtonHandler = this.editEventButton.bind(this);
        this.deleteEventButtonHandler = this.deleteEventButton.bind(this);

        return this;
    },

    enable: function () {

        this.$addEventButton.click(this.addEventButtonHandler);
        this.$formAddEventButton.click(this.formAddEventButtonHandler);
        this.$formChangeEventButton.click(this.formChangeEventButtonHandler);
        this.$editFormCloseButton.click(this.editFormCloseButtonHandler);
        this.$formCloseButton.click(this.formCloseButtonHandler);
        this.$container2.on('click', '.events', this.editEventButtonHandler);
        this.$container2.on('click', '.xCE', this.deleteEventButtonHandler);

        return this;
    },

    addEventButton: function (e) {
        document.getElementById("myFormCalendar").style.display = "block";
        document.getElementById("myFormCalendar").value = e.target.nextElementSibling.childNodes[5].innerHTML + "." + e.target.nextElementSibling.childNodes[3].innerHTML + "." + e.target.nextElementSibling.childNodes[7].innerHTML
    },

    editEventButton: function (e) {
        
        idx = e.target.firstElementChild.getAttribute("index");
        function fun(idx){
            $.ajax({
            type: 'POST',
            url: '/get-calendar-event',
            data: {id: idx},
            success: function(task) {
                task = task[0];
                
                document.getElementById("fieldCalendarEdit1").value = task.name
                document.getElementById("fieldCalendarEdit2").value = task.time;
                document.getElementById("fieldCalendarEdit3").value = task.duration;
                document.getElementById("fieldCalendarEdit4").value = task.description;
                document.getElementById("myFormCalendarEdit").style.display = "block";
                document.getElementById("myFormCalendarEdit").value = task._id;
            }
        });
    }
    fun(idx);
    },
    
    formCloseButton: function () {
        document.getElementById("myFormCalendar").style.display = "none";
        document.getElementById("fieldCalendar1").value = '';
        document.getElementById("fieldCalendar2").value = '';
        document.getElementById("fieldCalendar3").value = '';
        document.getElementById("fieldCalendar4").value = '';
    },

    deleteEventButton: function (e) {
        const self = this;
			$.ajax({
				type: 'POST',
				url: '/del-calendar-event',
				data: {id: e.target.getAttribute("index")},
				success: function(result) {
                    console.log('Response:', result);
                    self.getEvents();
				}
			});
    },
	
	formAddEventButton: function () {
        const self = this;
			$.ajax({
				type: 'POST',
				url: '/add-calendar-event',
                data: {name: $('#fieldCalendar1').val(),
                       time: $('#fieldCalendar2').val(),
                       duration: $('#fieldCalendar3').val(),
                       description: $('#fieldCalendar4').val(),
                       date: $('#myFormCalendar').val()},
				success: function(result) {
                    console.log('Response:', result);
                    self.formCloseButton();
                    self.getEvents();
				}
			});
    },

    getEvents: function () {
        const self =this;
        $.ajax({
            type: 'POST',
            url: '/get-calendar-events',
            data: {},
            success: function(tasks) {
                if(tasks.constructor !== [].constructor){
                    return;
                }
                console.log('Response:', tasks);
                var els = document.getElementsByClassName('events');
                for (var i = 0; i < els.length; ++i) {
                    var item = els[i];  
                    item.innerHTML = '';
                }
                var index = 0;
                for (var task in tasks) {
                    var taskDate = tasks[task].eventDate.day + "." + tasks[task].eventDate.month + "." + tasks[task].eventDate.year
                    var element  = document.getElementById(taskDate).nextElementSibling;

                    var text = '<div\
                                    class="eventFirst">\
                                    <button index="'+ tasks[task].eventId +'" class="xCE">X</button>\
                                    <b>' + tasks[task].eventName +'</b>\
                                    <br>Begins: <b>' + tasks[task].eventTime  +':00</b>\
                                    <br>' + tasks[task].eventDuration  +' hours\
                               </div>';

                    element.innerHTML += text;
                }
            }
        });

    },

    formChangeEventButton: function (e) {
		const self = this;
			$.ajax({
				type: 'POST',
				url: '/change-calendar-event',
                data: { id: document.getElementById("myFormCalendarEdit").value,
                        name: $('#fieldCalendarEdit1').val(),
                        time: $('#fieldCalendarEdit2').val(),
                        duration: $('#fieldCalendarEdit3').val(),
                        description: $('#fieldCalendarEdit4').val()},
				success: function(result) {
                    console.log('Response:', result);
                    self.getEvents();
                    self.editFormCloseButton();
				}
			});
    },

    editFormCloseButton: function () {
        document.getElementById("myFormCalendarEdit").style.display = "none";
        document.getElementById("fieldCalendarEdit1").value = '';
        document.getElementById("fieldCalendarEdit2").value = '';
        document.getElementById("fieldCalendarEdit3").value = '';
        document.getElementById("fieldCalendarEdit4").value = '';
        document.getElementById("myFormCalendarEdit").value = '';
    },



};
new CalendarView();

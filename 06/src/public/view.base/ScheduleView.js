var ScheduleView = function () {
    this.init();
};



ScheduleView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable()
            .getSchedules()
    },

    createChildren: function () {
        // cache the document object
        this.$enterScheduleContainer = $('.form-popupSchedule').find('.form-container');
        this.$editScheduleContainer = $('.form-popupScheduleEdit').find('.form-container');
        this.$container3 = $('.schedule');
        this.$addScheduleButton = this.$container3.find('.js-add-schedule-button');
        this.$formAddScheduleButton = this.$enterScheduleContainer.find('.btnSchedule');
        this.$formChangeScheduleButton = this.$editScheduleContainer.find('.editBtnSchedule');
        this.$formCloseButton = this.$enterScheduleContainer.find('.cancelBtnSchedule');
        this.$editFormCloseButton = this.$editScheduleContainer.find('.cancelEditBtnSchedule');
		
        return this;
    },

    setupHandlers: function () {

        this.addScheduleButtonHandler = this.addScheduleButton.bind(this);
        this.formAddScheduleButtonHandler = this.formAddScheduleButton.bind(this);
        this.formChangeScheduleButtonHandler = this.formChangeScheduleButton.bind(this);
        this.editFormCloseButtonHandler = this.editFormCloseButton.bind(this);
        this.formCloseButtonHandler = this.formCloseButton.bind(this);
        this.editScheduleButtonHandler = this.editScheduleButton.bind(this);

        return this;
    },

    enable: function () {

        this.$addScheduleButton.click(this.addScheduleButtonHandler);
        this.$formAddScheduleButton.click(this.formAddScheduleButtonHandler);
        this.$formChangeScheduleButton.click(this.formChangeScheduleButtonHandler);
        this.$editFormCloseButton.click(this.editFormCloseButtonHandler);
        this.$formCloseButton.click(this.formCloseButtonHandler);
        this.$container3.on('click', '.cellio', this.editScheduleButtonHandler);

        return this;
    },

    addScheduleButton: function (e) {
        if(e.target.value === "x"){
            this.deleteScheduleButton(e);
        }
        else{
            document.getElementById("myFormSchedule").style.display = "block";
            document.getElementById("myFormSchedule").value = e.target.getAttribute("d") + ":" + e.target.getAttribute("h");
            e.target.style.display = "block";
        }
    },
    editScheduleButton: function (e) {
        if(e.target.className === "scheduleLabel"){
            day1 =  e.target.parentNode.firstElementChild.getAttribute("d");
            hour1 =  e.target.parentNode.firstElementChild.getAttribute("h");
            function fun(day1, hour1){
                $.ajax({
                    type: 'POST',
                    url: '/get-one-schedule',
                    data: {day: day1, hour: hour1},
                    success: function(schedule) {
                        schedule = schedule[0];
                        
                        document.getElementById("fieldScheduleEdit1").value = schedule.name
                        document.getElementById("fieldScheduleEdit2").value = schedule.colour;
                        document.getElementById("myFormScheduleEdit").style.display = "block";
                        document.getElementById("myFormScheduleEdit").value = schedule._id;
                    }
                });
            }
            fun(day1, hour1);
        }
    },

    formChangeScheduleButton: function(e){
        const self = this;
			$.ajax({
				type: 'POST',
				url: '/change-schedule',
                data: { id: document.getElementById("myFormScheduleEdit").value,
                        name: $('#fieldScheduleEdit1').val(),
                        colour: $('#fieldScheduleEdit2').val()},
				success: function(result) {
                    console.log('Response:', result);
                    self.getSchedules();
                    self.editFormCloseButton();
				}
			});
    },

    editFormCloseButton: function(){
        document.getElementById("myFormScheduleEdit").style.display = "none";
        document.getElementById("fieldScheduleEdit1").value = '';
        document.getElementById("fieldScheduleEdit2").value = 'Blue';
    },
    
    formCloseButton: function () {
        document.getElementById("myFormSchedule").style.display = "none";
        document.getElementById("fieldSchedule1").value = '';
        document.getElementById("fieldSchedule2").value = '';
        document.getElementById("fieldSchedule3").value = 'Blue';
        var myFormScheduleValue = document.getElementById("myFormSchedule").value;
        d = myFormScheduleValue.split(":")[0];
        h = myFormScheduleValue.split(":")[1];
        $('input[d=' + d +'][h=' + h + ']')[0].style.display = null;
    },

    deleteScheduleButton: function (e) {
        const self = this;
			$.ajax({
				type: 'POST',
				url: '/del-schedule',
				data: {day: e.target.getAttribute("d"), hour: e.target.getAttribute("h")},
				success: function(result) {
                    console.log('Response:', result);
                    self.getSchedules();
				}
			});
    },

    getSchedules: function () {
        const self =this;
        $.ajax({
            type: 'POST',
            url: '/get-schedule',
            data: {},
            success: function(schedules) {
                if(schedules.constructor !== [].constructor){
                    return;
                }
                console.log('Response:', schedules);
                var els = document.getElementsByClassName('events');

                var par = document.getElementsByClassName('cellio');
                for(var i = 0; i < par.length; i++) {
                    par[i].firstChild.value = "+";
                    par[i].firstChild.style.backgroundColor = null;
                    par[i].style.backgroundColor = null;
                }

                var paras = document.getElementsByClassName('scheduleLabel');
                while(paras[0]) {
                    paras[0].parentNode.removeChild(paras[0]);
                }

                for (var schedule in schedules) {
                    var scheduleDay = schedules[schedule].scheduleDay;
                    var scheduleHour = schedules[schedule].scheduleHour;
                    
                    var element  = $('input[d=' + scheduleDay +'][h=' + scheduleHour + ']')[0];
                    element.value = "x";
                    element.style.backgroundColor = "red";
                    var label = document.createElement("div");
                    label.className = "scheduleLabel";
                    label.innerHTML= schedules[schedule].scheduleName;
                    label.style.color = "white";
                    if(schedules[schedule].scheduleColour === "Yellow"){
                        label.style.color = "black";
                    }
                    label.style.fontSize = "20px";
                    if(element.nextSibling){
                        element.parentNode.insertBefore(label, element.nextSibling);
                    }
                    else{
                        element.parentNode.appendChild(label);
                    }
                    label.parentNode.style.backgroundColor = schedules[schedule].scheduleColour; 
                }
            }
        });

    },
	
	formAddScheduleButton: function () {
        const self = this;
        
        var myFormScheduleValue = document.getElementById("myFormSchedule").value;
        d = myFormScheduleValue.split(":")[0];
        h = myFormScheduleValue.split(":")[1];
        $('input[d=' + d +'][h=' + h + ']')[0].style.display = null;
        y = 5
			$.ajax({
				type: 'POST',
				url: '/add-schedule-event',
                data: {name: $('#fieldSchedule1').val(),
                       duration: $('#fieldSchedule2').val(),
                       colour: $('#fieldSchedule3').val(),
                       day: d,
                       hour: h},
				success: function(result) {
                    console.log('Response:', result);
                    self.formCloseButton();
                    self.getSchedules();
				}
			});
    },

};
new ScheduleView();

var PasswordChangeView = function () {
    this.init();
};

PasswordChangeView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable();
    },

    createChildren: function () {
        // cache the document object
        this.$container = $('#pwchange-page');
        this.$pwchangeButton = this.$container.find('#js-pwchange-button');
		this.$oldpwInput = this.$container.find('#js-pwchange-oldpassword');
		this.$newpwInput = this.$container.find('#js-pwchange-newpassword');
		this.$newpw2Input = this.$container.find('#js-pwchange-newpassword2');
        
        return this;
    },

    setupHandlers: function () {

        this.pwchangeButtonHandler = this.pwchangeButton.bind(this);
		
        return this;
    },

    enable: function () {

        this.$pwchangeButton.click(this.pwchangeButtonHandler);
        
        return this;
    },
	
	decodeCookie: function(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	},

    pwchangeButton: function () {
        console.log('PasswordChangeView.js: pwchangeButton clicked.');
		
		const self = this;
		$.ajax({
			type: 'POST',
			url: '/pwchange',
			data: {
				oldpw: $("#js-pwchange-oldpassword").val(),
				newpw: $("#js-pwchange-newpassword").val(),
				newpw2: $("#js-pwchange-newpassword2").val(),
				hash: this.decodeCookie("HASH").split("^")[0],
				email: this.decodeCookie("HASH").split("^")[1]
			},
			success: function(result) {
				console.log('PasswordChangeView.js: pwchange: ', result);
				
				document.getElementById("js-pwchange-response").innerHTML = result.msg;
			}
		});
	}
};
new PasswordChangeView();

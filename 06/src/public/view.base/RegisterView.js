var RegisterView = function () {
    this.init();
};

RegisterView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable();
    },

    createChildren: function () {
        // cache the document object
        this.$container = $('#register-page');
        this.$registerButton = this.$container.find('#js-register-button');
		this.$emailInput = this.$container.find('#js-register-email');
		this.$passwordInput = this.$container.find('#js-register-password');
		this.$password2Input = this.$container.find('#js-register-password2');
        
        return this;
    },

    setupHandlers: function () {

        this.registerButtonHandler = this.registerButton.bind(this);
		
        return this;
    },

    enable: function () {

        this.$registerButton.click(this.registerButtonHandler);
        
        return this;
    },

    registerButton: function () {
        console.log('RegisterView.js: registerButton clicked.');
		const self = this;
		$.ajax({
			type: 'POST',
			url: '/register',
			data: {
				email: $("#js-register-email").val(),
				pw: $("#js-register-password").val(),
				pw2: $("#js-register-password2").val()
			},
			success: function(result) {
				console.log('RegisterView.js: registerResult: ', result);
				
				document.getElementById("js-register-response").innerHTML = result.msg;
			}
		});
	}
};
new RegisterView();

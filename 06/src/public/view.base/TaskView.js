var TaskView = function () {
    this.init();
};



TaskView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable()
            .buildList();
    },

    createChildren: function () {
        // cache the document object
        this.$enterTodoContainer = $('.form-popup').find('.form-container');
        this.$editTodoContainer = $('.editForm-popup').find('.form-container');
        this.$container = $('.js-container');
        this.$addTaskButton = this.$container.find('.js-add-task-button');
        this.$formAddTaskButton = this.$enterTodoContainer.find('.btn');
        this.$formChangeTaskButton = this.$editTodoContainer.find('.editBtn');
        this.$formCloseButton = this.$enterTodoContainer.find('.cancelBtn');
        this.$editFormCloseButton = this.$editTodoContainer.find('.cancelEditBtn');
        this.$tasksContainer = this.$container.find('.js-tasks-container');
		this.$tasksContainerEventList = $('.event_list');
		this.$tasksContainerCol1 = this.$tasksContainerEventList.find('.events_col_1');
		this.$tasksContainerCol2 = this.$tasksContainerEventList.find('.events_col_2');
		this.$tasksContainerCol3 = this.$tasksContainerEventList.find('.events_col_3');
		
        return this;
    },

    setupHandlers: function () {

        this.addTaskButtonHandler = this.addTaskButton.bind(this);
        this.formAddTaskButtonHandler = this.formAddTaskButton.bind(this);
        this.formChangeTaskButtonHandler = this.formChangeTaskButton.bind(this);
        this.editFormCloseButtonHandler = this.editFormCloseButton.bind(this);
        this.formCloseButtonHandler = this.formCloseButton.bind(this);
        this.editTaskButtonHandler = this.editTaskButton.bind(this);
        this.deleteTaskButtonHandler = this.deleteTaskButton.bind(this);

        return this;
    },

    enable: function () {

        this.$addTaskButton.click(this.addTaskButtonHandler);
        this.$formAddTaskButton.click(this.formAddTaskButtonHandler);
        this.$formChangeTaskButton.click(this.formChangeTaskButtonHandler);
        this.$editFormCloseButton.click(this.editFormCloseButtonHandler);
        this.$formCloseButton.click(this.formCloseButtonHandler);
        this.$container.on('click', '.e', this.editTaskButtonHandler);
        this.$container.on('click', '.x', this.deleteTaskButtonHandler);

        return this;
    },

    addTaskButton: function () {
        document.getElementById("myForm").style.display = "block";
    },

    editTaskButton: function (e) {
        document.getElementById("fieldEdit").value = e.target.nextElementSibling.nextElementSibling.innerHTML;
        document.getElementById("myFormEdit").style.display = "block";
        document.getElementById("myFormEdit").value = e.target.getAttribute("index");
    },
    
    formCloseButton: function () {
        document.getElementById("myForm").style.display = "none";
        document.getElementById("field1").value = '';
    },

    editFormCloseButton: function () {
        document.getElementById("myFormEdit").style.display = "none";
        document.getElementById("fieldEdit").value = '';
    },
	
	formAddTaskButton: function () {
		const self = this;
			$.ajax({
				type: 'POST',
				url: '/add-task',
				data: {text: $('#field1').val()},
				success: function(result) {
                    console.log('Response:', result);
                    self.closeForm();
				}
			});
    },

    formChangeTaskButton: function (e) {
		const self = this;
			$.ajax({
				type: 'POST',
				url: '/change-task',
				data: {id: document.getElementById("myFormEdit").value, text: $('#fieldEdit').val()},
				success: function(result) {
                    console.log('Response:', result);
                    self.closeEditForm();
				}
			});
    },

    deleteTaskButton: function (e) {
        const self = this;
			$.ajax({
				type: 'POST',
				url: '/del-task',
				data: {id: e.target.getAttribute("index")},
				success: function(result) {
                    console.log('Response:', result);
                    self.buildList();
				}
			});
    },

    buildList: function () {
        const self =this;
        $.ajax({
            type: 'POST',
            url: '/get-tasks',
            data: {text: $('#field1').val()},
            success: function(tasks) {
                if(tasks.constructor !== [].constructor){
                    return;
                }
                console.log('Response:', tasks);
                var html = "";
                var $tasksContainer = self.$tasksContainer;
                var $tasksContainerCol1 = self.$tasksContainerCol1;
                var $tasksContainerCol2 = self.$tasksContainerCol2;
                var $tasksContainerCol3 = self.$tasksContainerCol3;
        
                $tasksContainerCol1.html('');
                $tasksContainerCol2.html('');
                $tasksContainerCol3.html('');
        
                var index = 0;
                for (var task in tasks) {
                    var text = '<div class="eventE">\
                                    <div class="event_descE">\
                                        <button index ="' + tasks[task].taskId + '" class = "x">\
                                            X\
                                        </button>\
                                        <button index ="' + tasks[task].taskId + '" class = "e">\
                                            e\
                                        </button>\
                                        <p class="description">Description</p>\
                                        <p class="opis">' + tasks[task].taskName + '</p>\
                                    </div>\
                                </div>';

                    if(index % 3 == 0){
                        $tasksContainerCol1.append(text)
                    }
                    else if(index % 3 == 1){
                        $tasksContainerCol2.append(text)
                    }
                    else if(index % 3 == 2){
                        $tasksContainerCol3.append(text)
                    }
                    index++;
                }
            }
        });

    },
	
    closeForm: function() {
        document.getElementById("myForm").style.display = "none";
              document.getElementById("field1").value = ''
              this.buildList();
      },

      closeEditForm: function() {
        document.getElementById("myFormEdit").style.display = "none";
              document.getElementById("fieldEdit").value = ''
              this.buildList();
      }

};
new TaskView();

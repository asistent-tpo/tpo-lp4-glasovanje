var LoginView = function () {
    this.init();
};

LoginView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable();
    },

    createChildren: function () {
        // cache the document object
        this.$container = $('#login-page');
        this.$loginButton = this.$container.find('#js-login-button');
		this.$emailInput = this.$container.find('#js-login-email');
		this.$passwordInput = this.$container.find('#js-login-password');
		
        return this;
    },

    setupHandlers: function () {

        this.loginButtonHandler = this.loginButton.bind(this);
		
        return this;
    },

    enable: function () {

        this.$loginButton.click(this.loginButtonHandler);
        
        return this;
    },

    loginButton: function () {
        console.log('LoginView.js: loginButton clicked.');
		const self = this;
		$.ajax({
			type: 'POST',
			url: '/login',
			data: {
				email: $("#js-login-email").val(),
				pw: $("#js-login-password").val()
			},
			success: function(result) {
				console.log('LoginView.js: loginResult: ', result);
				
				if (!result.value) {
					document.getElementById("js-login-response").innerHTML = result.msg;
				} else {
					document.cookie = "HASH=" + result.data.cookie + "^" + result.data.email + ";expires=" + result.data.expire + ";path=/";					
					window.location.href = '/';
				}
			}
		});
	}
};
new LoginView();

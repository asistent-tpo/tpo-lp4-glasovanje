var AdminView = function () {
    this.init();
};

AdminView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable();
    },

    createChildren: function () {
        // cache the document object
        this.$container = $('#admin-page');
        this.$sendButton = this.$container.find('#js-admin-button');
		this.$textInput = this.$container.find('#js-admin-text');
		
        return this;
    },

    setupHandlers: function () {

        this.sendButtonHandler = this.sendButton.bind(this);
		
        return this;
    },

    enable: function () {

        this.$sendButton.click(this.sendButtonHandler);
        
        return this;
    },

    sendButton: function () {
        console.log('AdminView.js: sendButton clicked.');
		const self = this;
		$.ajax({
			type: 'POST',
			url: '/add-notif',
			data: {
				text: $("#js-admin-text").val()
			},
			success: function(result) {
				console.log('AdminView.js: globalResult: ', result);
				
				
			}
		});
	}
};
new AdminView();

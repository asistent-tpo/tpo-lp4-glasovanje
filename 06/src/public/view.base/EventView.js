var EventView = function () {
    this.init();
};



EventView.prototype = {

    init: function () {
        this.createChildren()
            .setupHandlers()
            .enable()
            .buildList();
    },

    createChildren: function () {
        // cache the document object
		this.$enterEventContainer = $('.form-popup').find('.form-container');
        this.$container = $('#event-page');
        this.$addEventButton = this.$container.find('.js-add-event-button');
        this.$formAddEventButton = this.$enterEventContainer.find('.btn');
        this.$formCloseButton = this.$enterEventContainer.find('.cancelBtn');
        this.$eventsContainer = this.$container.find('.js-events-container');
        this.$eventsContainerEventList = $('.event_list');
        this.$eventsContainerCol1 = this.$eventsContainerEventList.find('.events_col_1');
        this.$eventsContainerCol2 = this.$eventsContainerEventList.find('.events_col_2');
        this.$eventsContainerCol3 = this.$eventsContainerEventList.find('.events_col_3');
        
        return this;
    },

    setupHandlers: function () {

        this.addEventButtonHandler = this.addEventButton.bind(this);
        this.formAddEventButtonHandler = this.formAddEventButton.bind(this);
        this.formCloseButtonHandler = this.formCloseButton.bind(this);
        this.deleteEventButtonHandler = this.deleteEventButton.bind(this);

        return this;
    },

    enable: function () {

        this.$addEventButton.click(this.addEventButtonHandler);
        this.$formAddEventButton.click(this.formAddEventButtonHandler);
        this.$formCloseButton.click(this.formCloseButtonHandler);
        this.$container.on('click', '.xE', this.deleteEventButtonHandler);

        return this;
    },

    addEventButton: function () {
        console.log('addbutton clicked');
        document.getElementById("myEventForm").style.display = "block";
    },

    formCloseButton: function () {
        document.getElementById("myEventForm").style.display = "none";
        document.getElementById("fieldEvent").value = '';
    },
	
	formAddEventButton: function () {
		const self = this;
			$.ajax({
				type: 'POST',
				url: '/add-event',
				data: {text: $('#fieldEvent').val(), text2: $('#fieldEventN').val(), text3: $('#fieldEventD').val() },
				success: function(result) {
                    console.log('Response:', result);
                    self.closeForm();
				}
			});
    },

    deleteEventButton: function (e) {
        //console.log('EventView.js: delete Events was clicked');
        const self = this;
        //console.log('EventView.js:  ' + self);
			$.ajax({
				type: 'POST',
				url: '/del-event',
				data: {id: e.target.getAttribute("index")},
				success: function(result) {
                    //console.log('EventView.js: delete Events was clicked');
                    console.log('Response:', result);
                    self.buildList();
				}
			}); 
            //console.log('EventView.js: delete Events was clicked');
    },

    buildList: function () {
        const self = this;
        
        $.ajax({
            type: 'POST',
            url: '/get-events',
            data: {text: $('#fieldEvent').val()},
            success: function(events) {
                console.log('Response:', events);
                var html = "";
                var $eventsContainer = self.$eventsContainer;
                var $eventsContainerCol1 = self.$eventsContainerCol1;
                var $eventsContainerCol2 = self.$eventsContainerCol2;
                var $eventsContainerCol3 = self.$eventsContainerCol3;
        
                $eventsContainerCol1.html('');
                $eventsContainerCol2.html('');
                $eventsContainerCol3.html('');
        
                var index = 0;
                events = events.reverse();
                for (var event in events) {
                    var text = '<div class="eventE">\
                                <div class="event_descE">\
                                <button index ="' + events[event].eventId + '" class = "xE" style="display: none">\
                                     X\
                                    </button>\
                                    <p class="description">' + events[event].eventName + '</p>\
                                    <p class="opis">' + events[event].eventDate + '</p>\
                                    <p class="opis">' + events[event].eventDesc + '</p>\
                                </div>\
                            </div>';

                    if(index % 3 == 0){
                        $eventsContainerCol1.append(text)
                    }
                    else if(index % 3 == 1){
                        $eventsContainerCol2.append(text)
                    }
                    else if(index % 3 == 2){
                        $eventsContainerCol3.append(text)
                    }
                    index++;
                }
            }
        });

        $.ajax({
            type: 'POST',
            url: '/is-manager',
            data: { },
            success: function (res) {

                //console.log(res);
                //console.log(res.isManager);
                if (res.isManager == true) {
                    //console.log("isManager je čru");
                    document.getElementById("js-add-event-button").style.display = "block";
                    var x = document.getElementsByClassName("xE");
                    var i;
                    for (i = 0; i < x.length; i++) {
                      x[i].style.display = "block";
                    }
               }
            }
        });

    },
	
    closeForm: function() {
        document.getElementById("myEventForm").style.display = "none";
              document.getElementById("fieldEvent").value = ''
              this.buildList();
	}
};
new EventView();

var Event = require('../common/Event.js');

var CalendarController = function (model, view) {
    this.model = model;

    this.init();
};

CalendarController.prototype = {

    init: function () {

    },

    addEvent: async function (req, email) {
        console.log('controller addEvent');
        await this.model.addEvent(req.name, req.time, req.duration, req.description, req.date, email);
    },

    changeEvent: async function (id, name, time, duration, description, email) {
		await this.model.changeEvent(id, name, time, duration, description, email);
    },

    deleteEvent: async function (id, email) {
        await this.model.deleteEvent(id, email);
    },

    getEvents: async function(email){
        await this.model.refreshEvents(email);
        return this.model.events;
    },
    getEvent: async function(id, email){
        var r = await this.model.getEvent(id, email);
        return r;
    }

};

module.exports = CalendarController;

var Notif = require('../common/Event.js');

var NotifController = function (model, view) {
    this.model = model;

    this.init();
};

NotifController.prototype = {

    init: function () {

    },

    addNotif: function (req) {
        console.log('controller addNotif');
        this.model.addNotif(req.text);
    },
	
	formAddNotif: function (sender, args) {
		this.model.formAddNotif(args.notif);
    },

    deleteNotif: async function (id) {
        await this.model.deleteNotifs(id);
    },

    getNotifs: async function(sender, args){
        await this.model.refreshNotifs();
        return this.model.notifs;
    }

};

module.exports = NotifController;

var Event = require('../common/Event.js');

var EventController = function (model, view) {
    this.model = model;

    this.init();
};

EventController.prototype = {

    init: function () {

    },

    addEvent: function (req) {
        //console.log("++++++" + req.text2);
        this.model.addEvent(req.text2, req.text, req.text3);
    },
	
	formAddEvent: function (sender, args) {
		this.model.formAddEvent(args.event);
		document.getElementById("myEventForm").style.display = "none";
		document.getElementById("fieldEvent").value = '';
    },

    changeEvent: async function (id, text) {
        await this.model.changeEvent(id, text);
    },

    deleteEvent: async function (id) {
        //console.log('EventController.js: delete Events was clicked');
        await this.model.deleteEvents(id);
    },

    getEvents: async function(sender, args){
        await this.model.refreshEvents();
        return this.model.events;
    }

};

module.exports = EventController;

var Event = require('../common/Event.js');

var AuthenticationController = function (model, view) {
    this.model = model;

    this.init();
};

AuthenticationController.prototype = {

    init: function () {

    },

    registerEvent: function(req) {
        return this.model.registerEvent(req);
    },
	
	verifyEvent: function(req) {
		return this.model.verifyEvent(req);
	},
	
	loginEvent: function(req) {
		return this.model.loginEvent(req);
	},
	
	authenticateEvent: function(req) {
		return this.model.authenticateEvent(req);
	},
	
	passwordChangeEvent: async function(req) {
		console.log("AuthCont.js: pwchange:", req);
		
		const auth = await this.model.authenticateEvent({email: req.email, cookie: req.hash});
		if (!auth.value) return auth;
		
		const pwch = await this.model.passwordChangeEvent(req);
		return pwch;
	},
	
	isAdmin: async function(req) {
		const auth = await this.model.isAdmin(req.email);
		return auth;
	},	

	isManager: async function(req) {
		const auth = await this.model.isManager(req.email);
		return auth;
	}
};

module.exports = AuthenticationController;

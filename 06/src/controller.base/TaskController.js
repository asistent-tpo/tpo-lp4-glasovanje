var Event = require('../common/Event.js');

var TaskController = function (model, view) {
    this.model = model;

    this.init();
};

TaskController.prototype = {

    init: function () {

    },

    addTask: async function (req, email) {
        console.log('controller addTask');
        await this.model.addTask(req.text, email);
    },

    changeTask: async function (id, text, email) {
		await this.model.changeTask(id, text, email);
    },

    deleteTask: async function (id, email) {
        await this.model.deleteTasks(id, email);
    },

    getTasks: async function(email){
        await this.model.refreshTasks(email);
        return this.model.tasks;
    }

};

module.exports = TaskController;

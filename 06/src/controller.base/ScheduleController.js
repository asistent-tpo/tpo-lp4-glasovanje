var Event = require('../common/Event.js');

var ScheduleController = function (model, view) {
    this.model = model;

    this.init();
};

ScheduleController.prototype = {

    init: function () {

    },

    addSchedule: async function (req, email) {
        console.log('controller addSchedule');
        for(var i = 0; i < req.duration && i <= 23; i++){
            await this.model.addSchedule(req.name, req.colour, req.day, parseInt(req.hour) + i, email);
        }
    },

    changeSchedule: async function (id, name, colour, email) {
		await this.model.changeSchedule(id, name, colour, email);
    },

    deleteSchedule: async function (day, hour, email) {
        await this.model.deleteSchedule(day, hour, email);
    },

    getSchedule: async function(email){
        await this.model.refreshSchedule(email);
        return this.model.schedule;
    },
    getOneSchedule: async function(day, hour, email){
        var r = await this.model.getOneSchedule(day, hour, email);
        return r;
    }

};

module.exports = ScheduleController;

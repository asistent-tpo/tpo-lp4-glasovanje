var StatusCode = function(status, msg, data) {
    this.status = status;
	this.msg = msg;
	this.data = data;
	this.value = this.status.indexOf("TRUE") > 0;
};

module.exports = StatusCode;
var express = require('express');
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var streznik = express();
var port = process.env.PORT || 3000;
var cors = require('cors')


var mongoose = require('mongoose');
const uri = "mongodb+srv://Maj254:tpobaza123@cluster0-xnqrh.mongodb.net/TPO?retryWrites=true";
mongoose.connect(uri, {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Established connection to mongodb base");
});

streznik.use(cookieParser());
streznik.use(bodyParser.urlencoded({ extended: false }));
streznik.use(bodyParser.json());
streznik.use(express.static(__dirname + '/public'));
streznik.use(cors({ origin: 'http://localhost:3000', credentials: true }))


function prepareAuth(zahteva) {
	if (!zahteva.cookies['HASH']) return {email: '', cookie: ''};
	return {email: zahteva.cookies['HASH'].split('^')[1], cookie: zahteva.cookies['HASH'].split('^')[0]}
}



/** Basic get requests **/
streznik.get('/', function(zahteva, odgovor) {
	console.log("bla")
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/ ... AUTH:', res);
		
		if (res.value) {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				console.log('/ ... ADM_AUTH:', res2);
				
				if (res2.value) {
					odgovor.redirect('/admin');
				} else {
					odgovor.sendFile(__dirname + "/" + "public/index.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				}
			});
		} else {
			odgovor.sendFile(__dirname + "/" + "public/login.html", function(err) {
				if (err) {
					console.log(err);
				}
			});
		}
	});
});

streznik.get('/register', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/register ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.sendFile(__dirname + "/" + "public/register.html", function(err) {
				if (err) {
					console.log(err);
				}
			});
		} else {
			odgovor.redirect('/');
		}
	});
});

streznik.get('/login', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/login ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.sendFile(__dirname + "/" + "public/login.html", function(err) {
				if (err) {
					console.log(err);
				}
			}); 
		} else {
			odgovor.redirect('/');
		}
	});
});

streznik.get('/pwchange', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/pwchange ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				if (!res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/pwchange.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					odgovor.redirect('/admin');
				}
			});
		}
	});
});

streznik.get('/admin', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					odgovor.redirect('/');
				}
			});
		}
	});
});

streznik.get('/logout', function(zahteva, odgovor) {
	odgovor.cookie('HASH', 'x^x', {
		maxAge: 24 * 60 * 60 * 1000,
		httpOnly: true,
		secure: true
	});
	
	odgovor.redirect('/');
});

streznik.get('/index', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/ ... AUTH:', res);
		
		if (res.value) {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				console.log('/ ... ADM_AUTH:', res2);
				
				if (res2.value) {
					odgovor.redirect('/admin');
				} else {
					odgovor.sendFile(__dirname + "/" + "public/index.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				}
			});
		} else {
			odgovor.redirect('/events');
		}
	});
});

/*Elvisa*/

streznik.get('/food', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/ ... AUTH:', res);
		
		if (res.value) {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				console.log('/ ... ADM_AUTH:', res2);
				
				if (res2.value) {
					odgovor.redirect('/admin');
				} else {
					odgovor.sendFile(__dirname + "/" + "public/food.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				}
			});
		} else {
			odgovor.sendFile(__dirname + "/" + "public/food.html", function(err) {
				if (err) {
					console.log(err);
				}
			});
		}
	});
});

streznik.get('/bus', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/ ... AUTH:', res);
		
		if (res.value) {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				console.log('/ ... ADM_AUTH:', res2);
				
				if (res2.value) {
					odgovor.redirect('/admin');
				} else {
					odgovor.sendFile(__dirname + "/" + "public/bus.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				}
			});
		} else {
			odgovor.sendFile(__dirname + "/" + "public/bus.html", function(err) {
				if (err) {
					console.log(err);
				}
			});
		}
	});
});


/** Ljupche **/
var user_model = require('./model.base/UserModel.js');
var userModel = new user_model(mongoose);
var auth_controller = require('./controller.base/AuthenticationController.js');
const authController = new auth_controller(userModel);

/** Testing users 
authController.model.addAdmin("ljupchemilosheski@gmail.com");
var adminUserGen = new authController.model.RegUser({email: "ljupchemilosheski@gmail.com", password: "asdasd"});
adminUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "lm8235@student.uni-lj.si", password: "asdasdasd"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "maj.bajuk@outlook.com", password: "kruh"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});
**/
/** __ **/

/** Testing users **/
/**
authController.model.addAdmin("ljupchemilosheski@gmail.com");
authController.model.addManager("manager");
authController.model.addAdmin("admin");
var adminUserGen = new authController.model.RegUser({email: "ljupchemilosheski@gmail.com", password: "asdasd"});
adminUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "lm8235@student.uni-lj.si", password: "asdasdasd"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "maj.bajuk@outlook.com", password: "kruh"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "zala", password: "zala"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "admin", password: "admin"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});

var regUserGen = new authController.model.RegUser({email: "manager", password: "manager"});
regUserGen.save(function(err, user) {
	if (err) return console.error(err);
		else console.log(user + " saved to TPOCollection.");
});
**/
/** __ **/



streznik.post('/register', function(zahteva, odgovor) {
	console.log('/register');
	
	var ans = authController.registerEvent(zahteva.body);
	ans.then(function(res) {
		console.log("Register finished");
		odgovor.status(200).json(res);
	});
});

streznik.get('/verify/:hash', async function(zahteva, odgovor) {
	console.log('/verify/', zahteva.params.hash);
	odgovor.redirect('/');
	
	var ans = authController.verifyEvent({hash: zahteva.params.hash});
	ans.then(function(res) {
		console.log("Verification finished");
	});
});

streznik.post('/login', function(zahteva, odgovor) {
	console.log('/login');

	var ans = authController.loginEvent(zahteva.body);
	ans.then(function(res) {
		console.log("streznik.js: login:", res);
		
		if (res.value) {
			odgovor.cookie('HASH', res.data.cookie + "^" + res.data.email, {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
		}
		
		odgovor.json(res);
	});
});

streznik.post('/pwchange', function(zahteva, odgovor) {
	console.log('/pwchange');
	
	var ans = authController.passwordChangeEvent(zahteva.body);
	ans.then(function(res) {
		console.log("streznik.js: pwchange:", res);
		odgovor.json(res);
	});
});

streznik.post('/auth/:info', function(zahteva, odgovor) {
	console.log('/auth/', zahteva.params.info);
	
	var ans = authController.authenticateEvent({email: zahteva.params.info.split('^')[1], cookie: zahteva.params.info.split('^')[0]});
	ans.then(function(res) {
		console.log("streznik.js: auth:", res);
		odgovor.json(res);
	});
});

streznik.post('/admin/:info', function(zahteva, odgovor) {
	console.log('/admin/', zahteva.params.info);
	
	var ans = authController.isAdmin({email: zahteva.params.info});
	ans.then(function(res) {
		console.log('streznik.js:', res);
		odgovor.json(res);
	});
});

streznik.post('/global', function(zahteva, odgovor) {
	console.log(zahteva.body);
});



/** Maj **/
var todo_model = require('./model.base/TaskModel.js');
var todoModel = new todo_model(mongoose);
var todo_krmilnik = require('./controller.base/TaskController.js');
const todoController = new todo_krmilnik(todoModel);

var calendar_model = require('./model.base/CalendarModel.js');
var calendarModel = new calendar_model(mongoose);
var calendar_krmilnik = require('./controller.base/CalendarController.js');
const calendarController = new calendar_krmilnik(calendarModel);

var schedule_model = require('./model.base/ScheduleModel.js');
var scheduleModel = new schedule_model(mongoose);
var schedule_krmilnik = require('./controller.base/ScheduleController.js');
const scheduleController = new schedule_krmilnik(scheduleModel);

streznik.post('/add-task', async function(zahteva, odgovor) {
  console.log('/add-task');
  var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					await todoController.addTask(zahteva.body, authData.email);
       				odgovor.sendStatus(200);
				}
			});
		}
	});
});

streznik.post('/add-calendar-event', async function(zahteva, odgovor) {
  console.log('/add-calendar-event');
  console.log(zahteva.body)
  var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					await calendarController.addEvent(zahteva.body, authData.email);
       				odgovor.sendStatus(200);
				}
			});
		}
	});
       
});

streznik.post('/add-schedule-event', async function(zahteva, odgovor) {
	console.log('/add-schedule-event');
  console.log(zahteva.body)
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					await scheduleController.addSchedule(zahteva.body, authData.email);
		 			odgovor.sendStatus(200);
				}
			});
		}
	});
		 
  });

streznik.post('/get-calendar-events', async function(zahteva, odgovor) {
  console.log('/get-calendar-events');
  var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log(zahteva.body)
					odgovor.json(await calendarController.getEvents(authData.email));
				}
			});
		}
	});
  
});

streznik.post('/get-schedule', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/get-schedule');
					console.log(zahteva.body)
		  			odgovor.json(await scheduleController.getSchedule(authData.email));
				}
			});
		}
	});
  });

streznik.post('/get-calendar-event', async function(zahteva, odgovor) {
  console.log('/get-calendar-event');
	console.log(zahteva.body)
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					r = await calendarController.getEvent(zahteva.body.id, authData.email)
					console.log(r)
					odgovor.json(r);
				}
			});
		}
	});
});

streznik.post('/get-one-schedule', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/get-one-schedule');
	  				console.log(zahteva.body)
					r = await scheduleController.getOneSchedule(zahteva.body.day, zahteva.body.hour, authData.email)
					console.log(r)
					odgovor.json(r);
				}
			});
		}
	});
  });

streznik.post('/del-calendar-event', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/del-calendar-event');
					await calendarController.deleteEvent(zahteva.body.id, authData.email);
					odgovor.sendStatus(200);
				}
			});
		}
	});
});

streznik.post('/del-schedule', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/del-schedule');
					await scheduleController.deleteSchedule(zahteva.body.day, zahteva.body.hour, authData.email);
					odgovor.sendStatus(200);
				}
			});
		}
	});
  });

streznik.post('/change-task', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/change-task');
					console.log("id=" + zahteva.body.id + " & text= " + zahteva.body.text);
					await todoController.changeTask(zahteva.body.id, zahteva.body.text, authData.email);
					odgovor.sendStatus(200);
				}
			});
		}
	});
});

streznik.post('/change-calendar-event', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/change-calendar-event');
					await calendarController.changeEvent(zahteva.body.id, zahteva.body.name, zahteva.body.time, zahteva.body.duration, zahteva.body.description, authData.email);
					odgovor.sendStatus(200);
				}
			});
		}
	});
});

streznik.post('/change-schedule', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/change-schedule');
					await scheduleController.changeSchedule(zahteva.body.id, zahteva.body.name, zahteva.body.colour, authData.email);
					odgovor.sendStatus(200);
				}
			});
		}
	});
  });

streznik.post('/get-tasks', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/get-tasks');
					odgovor.json(await todoController.getTasks(authData.email));
				}
			});
		}
	});
});

streznik.post('/del-task', async function(zahteva, odgovor) {
	var authData = prepareAuth(zahteva)
  var ans = authController.authenticateEvent(authData);
	ans.then(async function(res) {
		console.log('/admin ... AUTH:', res);
		
		if (!res.value) {
			odgovor.cookie('HASH', 'x^x', {
				maxAge: 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: true
			});
			
			odgovor.redirect('/login');
		} else {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(async function(res2) {
				if (res2.value) {
					odgovor.sendFile(__dirname + "/" + "public/admin.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				} else {
					console.log('/del-task');
					await todoController.deleteTask(zahteva.body.id, authData.email);
					odgovor.sendStatus(200);
				}
			});
		}
	});
});



/** Zala **/
var eventModel = require('./model.base/EventModel.js');
var eventController = require('./controller.base/EventController.js');
//var eventView = require('./public/view.base/EventView.js');
var eModel = new eventModel(mongoose);
const eController = new eventController(eModel);

streznik.get('/events', function(zahteva, odgovor) {
	var ans = authController.authenticateEvent(prepareAuth(zahteva));
	ans.then(function(res) {
		console.log('/ ... AUTH:', res);
		
		if (res.value) {
			var adm = authController.isAdmin(prepareAuth(zahteva));
			adm.then(function(res2) {
				console.log('/ ... ADM_AUTH:', res2);
				
				if (res2.value) {
					odgovor.redirect('/admin');
				} else {
					odgovor.sendFile(__dirname + "/" + "public/events.html", function(err) {
						if (err) {
							console.log(err);
						}
					});
				}
			});
		} else {
			odgovor.sendFile(__dirname + "/" + "public/events.html", function(err) {
				if (err) {
					console.log(err);
				}
			});
		}
	});
});

streznik.post('/add-event', function(zahteva, odgovor) {
  
   var authData = prepareAuth(zahteva)
    var ans = authController.authenticateEvent(authData);
    ans.then(async function (res) {
        console.log('/admin ... AUTH:', res);

        if (!res.value) {
            odgovor.cookie('HASH', 'x^x', {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: true,
                secure: true
            });

            odgovor.redirect('/login');
        } else {
            var adm = authController.isManager(prepareAuth(zahteva));
            adm.then(async function (res2) {
                if (res2.value) {
                    console.log('/add-event');
       				eController.addEvent(zahteva.body);
       				odgovor.sendStatus(200);

                } else {
                    odgovor.sendStatus(200);
                }
            });
        }
    });
});

streznik.post('/get-events', async function(zahteva, odgovor) {
  console.log('/get-events');
       odgovor.json(await eController.getEvents());
});

streznik.post('/del-event', async function(zahteva, odgovor) {
  //console.log('streznik.js: delete Events was clicked');
  var authData = prepareAuth(zahteva)
    var ans = authController.authenticateEvent(authData);
    ans.then(async function (res) {
        console.log('/admin ... AUTH:', res);

        if (!res.value) {
            odgovor.cookie('HASH', 'x^x', {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: true,
                secure: true
            });

            odgovor.redirect('/login');
        } else {
            var adm = authController.isManager(prepareAuth(zahteva));
            adm.then(async function (res2) {
                if (res2.value) {
                    console.log('/del-event');
       				eController.deleteEvent(zahteva.body.id);
       				odgovor.sendStatus(200);
                } else {
                    odgovor.sendStatus(200);
                }
            });
        }
    });
  
});

streznik.post('/is-manager', async function (zahteva, odgovor) {
    console.log('/is-manager');
    var authData = prepareAuth(zahteva)
    var ans = authController.authenticateEvent(authData);
    ans.then(async function (res) {
        console.log('/admin ... AUTH:', res);

        if (!res.value) {
            odgovor.cookie('HASH', 'x^x', {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: true,
                secure: true
            });

            odgovor.redirect('/login');
        } else {
            var adm = authController.isManager(prepareAuth(zahteva));
            adm.then(async function (res2) {
                if (res2.value) {
                    odgovor.json({isManager: true});
                } else {
                
                }
            });
        }
    });
});



/*---------------zala pt2--------------*/


streznik.get('/notifs', function(zahteva, odgovor) {
  // body...
  odgovor.sendFile(__dirname + "/" + "public/notifs.html", function(err) {
    if(err) {
      console.log(err);
    }
  });
});



var notifModel = require('./model.base/NotifModel.js');
var notifController = require('./controller.base/NotifController.js');
//var notifView = require('./public/view.base/notifView.js');
var nModel = new notifModel(mongoose);
const nController = new notifController(nModel);

streznik.post('/add-notif', function (zahteva, odgovor) {

    var authData = prepareAuth(zahteva)
    var ans = authController.authenticateEvent(authData);
    ans.then(async function (res) {
        console.log('/admin ... AUTH:', res);

        if (!res.value) {
            odgovor.cookie('HASH', 'x^x', {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: true,
                secure: true
            });

            odgovor.redirect('/login');
        } else {
            var adm = authController.isAdmin(prepareAuth(zahteva));
            adm.then(async function (res2) {
                if (res2.value) {
                    console.log('/add-notif');
                    nController.addNotif(zahteva.body);
                    odgovor.sendStatus(200);
                } else {
                    odgovor.sendStatus(200);
                }
            });
        }
    });
});

streznik.post('/get-notifs', async function(zahteva, odgovor) {
  console.log('/get-notifs');
       odgovor.json(await nController.getNotifs());
});

streznik.post('/del-notif', async function(zahteva, odgovor) {
  //console.log('streznik.js: delete notifs was clicked');
  var authData = prepareAuth(zahteva)
    var ans = authController.authenticateEvent(authData);
    ans.then(async function (res) {
        console.log('/admin ... AUTH:', res);

        if (!res.value) {
            odgovor.cookie('HASH', 'x^x', {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: true,
                secure: true
            });

            odgovor.redirect('/login');
        } else {
            var adm = authController.isAdmin(prepareAuth(zahteva));
            console.log("isAdmin del");
            adm.then(async function (res2) {
                if (res2.value) {
				  	console.log('/del-notif');
				    nController.deleteNotif(zahteva.body.id);
				    odgovor.sendStatus(200);
                } else {
                    odgovor.sendStatus(200);
                }
            });
        }
    });
});



streznik.post('/is-admin', async function (zahteva, odgovor) {
    console.log('/is-admin');
    var authData = prepareAuth(zahteva)
    var ans = authController.authenticateEvent(authData);
    ans.then(async function (res) {
        console.log('/admin ... AUTH:', res);

        if (!res.value) {
            odgovor.cookie('HASH', 'x^x', {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: true,
                secure: true
            });

            odgovor.redirect('/login');
        } else {
            var adm = authController.isAdmin(prepareAuth(zahteva));
            adm.then(async function (res2) {
                if (res2.value) {
                    odgovor.json({isAdmin: true});
                } else {
                
                }
            });
        }
    });
});




/*---------------zala dodala DO tukaj--------------*/



/**
 * Poženi strežnik
 */
streznik.listen(port, function () {
  console.log('Strežnik je pognan na portu ' + port + '!');
});


module.exports = streznik;

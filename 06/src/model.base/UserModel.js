var cookie_time = 1000 * 60 * 15; /// 15 mins.

var baseURL = "http://tpo-app.herokuapp.com";
var emailjs = require('emailjs');
var hash = require('object-hash');
var status_code = require('../common/StatusCode.js');

function regexEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function regexPassword(password) {
    var re = /^[a-zA-Z]\w{3,14}$/;
    return re.test(String(password));
}

var UserModel = function (db) {
    this.users = [];
    this.db = db;

    // define Schema
    var UserSchema = this.db.Schema({
        email: String,
        password: String
    });

	var VerUserSchema = this.db.Schema({
		email: String,
		password: String,
		hash: String
	});

	var CookieSchema = this.db.Schema({
		expire: String,
		email: String,
		cookie: String
	});
	
	var AdminSchema = this.db.Schema({
		email: String
	});

	var ManagerSchema = this.db.Schema({
		email: String
	});
 
    // compile schema to model
	this.RegUser = this.db.model('RegUser', UserSchema, 'RegUserCollection');
	this.VerUser = this.db.model('VerUser', VerUserSchema, 'VerUserCollection');
	this.Cookie = this.db.model('Cookie', CookieSchema, 'CookieCollection');
	this.Admin = this.db.model('Admin', AdminSchema, 'AdminCollection');
	this.Manager = this.db.model('Manager', ManagerSchema, 'ManagerCollection');
	
	//this.RegUser.deleteMany({}, function(_){});
	this.VerUser.deleteMany({}, function(_){});
	this.Cookie.deleteMany({}, function(_){});
	//this.Admin.deleteMany({}, function(_){});
	//this.Manager.deleteMany({}, function(_){});
};

UserModel.prototype = {
	
	validateEmail: async function(email) {
		if (!regexEmail(email)) return new status_code("VALIDATE_EMAIL_FALSE_REGEX", "Email must be of form: tpo.straightas@gmail.com", null);
		
		const q1 = this.VerUser.find({email: email});
		const r1 = await q1.exec();
		if (r1.length) return new status_code("VALIDATE_EMAIL_FALSE_VER_EXISTS", "Check your email to confirm the account", null);
		
		const q2 = this.RegUser.find({email: email});
		const r2 = await q2.exec();
		if (r2.length) return new status_code("VALIDATE_EMAIL_FALSE_REG_EXISTS", "Email is already in use", null);
		
		return new status_code("VALIDATE_EMAIL_TRUE", "Check your email to validate the account", null);
	},
	
	validatePassword: function(pw1, pw2) {
		if (pw1 != pw2) return new status_code("PW_FALSE_NOT_MATCH", "Both passwords must match", null);
		if (!regexPassword(pw1)) return new status_code("PW_FALSE_REGEX", "Password must meet the required criteria", null);
		return new status_code("PW_TRUE", "", null);
	},
	
	addVerUser: function(data) {
		var verUser = new this.VerUser({email: data.email, password: data.pw, hash: hash(data.email + data.pw)});
        verUser.save(function(err, user) {
            if (err) return console.error(err);
            else console.log(user + " saved to TPOCollection.");
        });
	},
	
	sendVerMail: function(data) {
		emailjs.server.connect({
			user: 'tpo.straightas@gmail.com',
			password: 'wuiwui66',
			host: 'smtp.gmail.com',
			ssl: true
		}).send({
			text: 'Verify your account. Go to ' + baseURL + '/verify/' + hash(data.email + data.pw) + ' to verify your account.',
			from: 'StraightAs',
			to: '<' + data.email + '>',
			cc: 'tpo.straightas@gmail.com',
			subject: 'StraightAs - Account verification!'
			}, function (err, message) {
				console.log(err || message);
		});
	},
	
	registerEvent: async function(data) {
		console.log("UserModel.js: RegisterEvent: ", data);
		
		const ve = await this.validateEmail(data.email);
		if (!ve.value) return ve;
		if (!this.validatePassword(data.pw, data.pw2).value) return this.validatePassword(data.pw, data.pw2);
		
		this.addVerUser(data);
		this.sendVerMail(data);
		return new status_code("REG_TRUE", "Check your email to verify the account", null);
	},
	
	validateVerHash: async function(data) {
		const q1 = this.VerUser.find({hash: data.hash});
		const r1 = await q1.exec();
		
		if (!r1) return new status_code("VER_FALSE_HASH_NOT_FOUND", "Unknown verification link", null);
		if (r1.length == 0) return new status_code("VER_FALSE_HASH_NOT_FOUND", "Unknown verification link", null);
		if (r1.length >  1) return new status_code("VER_FALSE_MULT_VER", "Multiple verification links found", null);
		return new status_code("VER_TRUE", "", {user: r1[0]});
	},
	
	removeVerUser: function(user) {
		console.log("Removing ver: ", user);
		
		this.VerUser.deleteOne(user, function(err) {
			if (err) console.error(err);
			else console.log("VerUser removed:", user);
		});
		
		var regUser = new this.RegUser({email: user.email, password: user.password});
		regUser.save(function(err, user) {
			if (err) console.error(err);
			else console.log(user + " saved to TPOCollection.");
		});
	},
	
	verifyEvent: async function(data) {
		console.log("UserModel.js: VerifyEvent: ", data);

		const vh = await this.validateVerHash(data);
		console.log(">>>", vh);
		if (!vh.value) return vh;
		
		this.removeVerUser(vh.data.user);
	},
	
	addCookie: async function(d, data) {
		var cookie = new this.Cookie({expire: d.toUTCString(), email: data.email, cookie: hash(data.email + d.toUTCString())});
        await cookie.save(function(err, res) {
            if (err) return console.error(err);
            else {
				console.log(res + " saved to TPOCollection.");
			}
        });
	},
	
	getCookie: async function(data) {
		const q1 = this.Cookie.find({email: data.email});
		const r1 = await q1.exec();
		
		if (r1.length == 0) return new status_code("COOKIE_FALSE_NOT_FOUND", "Incorrect cookie", null);
		
		var d = new Date();
		for (var i = 0; i < r1.length; i++) {
			var di = new Date(r1[i].expire);
			
			if (di.getTime() > d.getTime()) {
				return new status_code("COOKIE_TRUE", "", {expire: di.toUTCString(), email: data.email, cookie: r1[i].cookie});
			}
		}
		
		return new status_code("COOKIE_FALSE_TIMEOUT", "", null);
	},
	
	loginEvent: async function(data) {
		console.log("UserModel.js: LoginEvent: ", data);
		
		const q1 = this.RegUser.find({email: data.email, password: data.pw});
		const r1 = await q1.exec();
		
		if (r1.length == 0) return new status_code("LOGIN_FALSE", "Incorrect credentials", null);
		
		var d = new Date();
		d.setTime(d.getTime() + cookie_time);
		
		await this.addCookie(d, data);
		await this.getCookie(data);
		
		return new status_code("LOGIN_TRUE", "", {expire: d.toUTCString(), email: data.email, cookie: hash(data.email + d.toUTCString())});
	},
	
	authenticateEvent: async function(data) {
		const q1 = this.Cookie.find({email: data.email, cookie: data.cookie});
		const r1 = await q1.exec();
		
		if (!r1) return new status_code("AUTH_FALSE_QUERY_ERROR", "Query error", null);
		if (r1.length == 0) return new status_code("AUTH_FALSE_NOT_FOUND", "Authentication failed: email and cookie do not match", null);
		return new status_code("AUTH_TRUE", "", data);
	},
	
	passwordChangeEvent: async function(data) {
		const q1 = this.RegUser.find({email: data.email, password: data.oldpw});
		const r1 = await q1.exec();
		if (!r1 || r1.length == 0) return new status_code("PWCHANGE_FALSE_OLDPW_WRONG", "Your old password is wrong", null);
		if (!this.validatePassword(data.newpw, data.newpw2).value) return this.validatePassword(data.newpw, data.newpw2);
		
		await this.RegUser.findOneAndUpdate({email: data.email}, {$set: {password: data.newpw}}, function(err, doc) {
			if (err) console.error(err);
			else console.log("Updated password of " + doc);
		});
		
		return new status_code("PWCHANGE_TRUE", "Password was changed", null);
	},
	
	addAdmin: function(email) {
		var admin = new this.Admin({email: email});
		admin.save(function(err, res) {
			if (err) return console.error(err);
			else {
				console.log(res + " saved to TPOCollection.");
			}
		});
	},

	addManager: function(email) {
		var mngr = new this.Manager({email: email});
		mngr.save(function(err, res) {
			if (err) return console.error(err);
			else {
				console.log(res + " saved to ManagerCollection.");
			}
		});
	},
	
	isAdmin: async function(email) {
		const q1 = this.Admin.find({email: email});
		const r1 = await q1.exec();
		
		console.log('isAdmin:', r1);
		if (!r1 || r1.length == 0) return new status_code("ADMIN_FALSE", "No admin access", null);
		return new status_code("ADMIN_TRUE", "", null);
	},

	isManager: async function(email) {
		const q1 = this.Manager.find({email: email});
		const r1 = await q1.exec();
		
		console.log('isManager:', r1);
		if (!r1 || r1.length == 0) return new status_code("MANAGER_FALSE", "No manager access", null);
		return new status_code("MANAGER_TRUE", "", null);
	}
};

module.exports = UserModel;
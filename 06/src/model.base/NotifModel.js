var NotifModel = function (db) {
     this.notifs = [];
     this.db = db;
     
     // define Schema
     var NotifSchema = this.db.Schema({
         name: String
     });
 
     // compile schema to model
     this.Notif = this.db.model('Notif', NotifSchema, 'NotifCollection');
 };

 NotifModel.prototype = {

     addNotif: function (notif) {    
        // a document instance
        var ev1 = new this.Notif({ name: notif });
    
        // save model to database
        ev1.save(function (err, ev) {
            if (err) return console.error(err);
            console.log(ev.name + " saved to notifColletction.");
        });
     },

     deleteNotifs: async function (index) {
        //console.log('NotifModel.js: delete notifs was clicked');
         this.notifs.splice(index, 1);
         const deleteQuery = this.Notif.findByIdAndRemove(index);
         const deleteResult = await deleteQuery.exec();
     },

     refreshNotifs: async function (){
        this.notifs = [];

        const refreshQuery = this.Notif.find({  }, null);
        const refreshResult = await refreshQuery.exec();

        refreshResult.forEach(notif => {
            this.notifs.push({
                notifId: notif._id,
                notifName: notif.name,
            });
            console.log(notif.name);
        });
     }
 };

 module.exports = NotifModel;
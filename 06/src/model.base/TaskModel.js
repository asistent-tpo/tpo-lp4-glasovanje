var TaskModel = function (db) {
     this.tasks = [];
     this.db = db;
     
     // define Schema
     var TodoSchema = this.db.Schema({
         email: String,
         name: String
     });
 
     // compile schema to model
     this.Todo = this.db.model('Todo', TodoSchema, 'TPOCollection');
 };

 TaskModel.prototype = {

     addTask: async function (task, email1) {    
        // a document instance
        var todo1 = new this.Todo({ email: email1, name: task });
    
        // save model to database
        await todo1.save(function (err, todo) {
            if (err) return console.error(err);
            console.log(todo.name + " saved to TPOCollection.");
        });
     },

     changeTask: async function (id, text, email1) {
         console.log(id);
         console.log(text);
        const changeQuery = this.Todo.findByIdAndUpdate(id,  {email: email1, name: text});
        const changeResult = await changeQuery.exec();
     },

     deleteTasks: async function (index, email1) {
         this.tasks.splice(index, 1);
         const deleteQuery = this.Todo.findByIdAndRemove(index);
         const deleteResult = await deleteQuery.exec();
     },

     refreshTasks: async function (email1){
        this.tasks = [];

        const refreshQuery = this.Todo.find({ email: email1 }, null);
        const refreshResult = await refreshQuery.exec();

        refreshResult.forEach(task => {
            this.tasks.push({
                taskId: task._id,
                taskName: task.name,
            });
        });
     }
 };

 module.exports = TaskModel;
var ScheduleModel = function (db) {
     this.schedule = [];
     this.db = db;
     
     // define Schema
     var ScheduleSchema = this.db.Schema({
         email: String,
         name: String,
         colour: String,
         day: Number,
         hour: Number
     });
 
     // compile schema to model
     this.Schedule = this.db.model('Schedule', ScheduleSchema, 'ScheduleCollection');
 };

ScheduleModel.prototype = {

     addSchedule: async function (name1, colour1, day1, hour1, email1) {    
         console.log("Model addSchedule")
        // a document instance
        const query = this.Schedule.find({email: email1, day: day1, hour: hour1});
        const result = await query.exec();
        if(result.length != 0){
            console.log("Already exists");
        }
        else{
            var schedule1 = new this.Schedule({ email: email1, name: name1, colour: colour1, day: day1, hour: hour1});
    
        // save model to database
        await schedule1.save(function (err, schedule) {
            if (err) return console.error(err);
            console.log(schedule.name + " saved to ScheduleCollection.");
        });
        }
     },

     changeSchedule: async function (id, name1, colour1, email1) {
        const changeQuery = this.Schedule.findByIdAndUpdate(id,  {name: name1, colour: colour1});
        const changeResult = await changeQuery.exec();
     },

     deleteSchedule: async function (day1, hour1, email1) {
         const query = this.Schedule.find({email: email1, day: day1, hour: hour1});
         const result = await query.exec();
         id = result[0]._id;
         
         const deleteQuery = this.Schedule.findByIdAndRemove(id);
         const deleteResult = await deleteQuery.exec();
     },

     getOneSchedule: async function (day1, hour1, email1){

        const refreshQuery = this.Schedule.find({email: email1, day: day1, hour: hour1 }, null);
        const refreshResult = await refreshQuery.exec();

        return refreshResult;
     },

     refreshSchedule: async function (email1){
        this.schedule = [];

        const refreshQuery = this.Schedule.find({ email: email1 }, null);
        const refreshResult = await refreshQuery.exec();

        refreshResult.forEach(schedule => {
            this.schedule.push({
                scheduleId: schedule._id,
                scheduleName: schedule.name,
                scheduleColour: schedule.colour,
                scheduleDay: schedule.day,
                scheduleHour: schedule.hour,
            });
        });
     }
 };

 module.exports = ScheduleModel;
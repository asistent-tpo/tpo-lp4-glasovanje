var EventModel = function (db) {
     this.events = [];
     this.db = db;
     
     // define Schema
     var EventSchema = this.db.Schema({
         name: String,
         desc: String,
         datum: String
     });
 
     // compile schema to model
     this.Eventic = this.db.model('Event', EventSchema, 'EventCollection');
 };

 EventModel.prototype = {

     addEvent: function (event, desc, datum) {    
        var ev1 = new this.Eventic({ name: event, desc: desc, datum: datum });
    
        ev1.save(function (err, ev) {
            if (err) return console.error(err);
            //console.log(ev.name + " event added");
        });
     },

     deleteEvents: async function (index) {
        //console.log('EventModel.js: delete Events was clicked');
         this.events.splice(index, 1);
         const deleteQuery = this.Eventic.findByIdAndRemove(index);
         const deleteResult = await deleteQuery.exec();
     },

     changeEvent: async function (id, text) {
        const changeQuery = this.Todo.findByIdAndUpdate(id,  { name: text });
        const changeResult = await changeQuery.exec();
     },

     refreshEvents: async function (){
        this.events = [];

        const refreshQuery = this.Eventic.find({  }, null);
        const refreshResult = await refreshQuery.exec();

        refreshResult.forEach(event => {
            this.events.push({
                eventId: event._id,
                eventName: event.name,
                eventDesc: event.desc,
                eventDate: event.datum
            });
        });
     }
 };

 module.exports = EventModel;
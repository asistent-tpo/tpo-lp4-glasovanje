var CalendarModel = function (db) {
     this.events = [];
     this.db = db;
     
     // define Schema
     var CalendarEventSchema = this.db.Schema({
         email: String,
         name: String,
         time: Number,
         duration: Number,
         description: String,
         date: {day: Number, month: String, year: Number}
     });
 
     // compile schema to model
     this.Calendar = this.db.model('Calendar', CalendarEventSchema, 'CalendarCollection');
 };

CalendarModel.prototype = {

     addEvent: async function (name1, time1, duration1, description1, date1, email1) {
         if(!this.checkDateValidity(date1)){
             return;
         }  
         console.log("Model addEvent")
        // a document instance
        console.log(time1)
        var event1 = new this.Calendar({ email: email1, name: name1, time: time1, duration: duration1, description: description1, date: { day: date1.split(".")[0], month: date1.split(".")[1], year: date1.split(".")[2]}  });
    
        // save model to database
        await event1.save(function (err, event) {
            if (err) return console.error(err);
            console.log(event.name + " saved to CalendarCollection.");
        });
     },

     changeEvent: async function (id, name1, time1, duration1, description1, email1) {
        const changeQuery = this.Calendar.findByIdAndUpdate(id,  {name: name1, time: time1, duration: duration1, description: description1});
        const changeResult = await changeQuery.exec();
     },

     deleteEvent: async function (index, email1) {
         this.events.splice(index, 1);
         const deleteQuery = this.Calendar.findByIdAndRemove(index);
         const deleteResult = await deleteQuery.exec();
     },

     getEvent: async function (id, email1){

        const refreshQuery = this.Calendar.find({_id: id, email: email1 }, null);
        const refreshResult = await refreshQuery.exec();

        return refreshResult;
     },

     refreshEvents: async function (email1){
        this.events = [];

        const refreshQuery = this.Calendar.find({ email: email1 }, null);
        const refreshResult = await refreshQuery.exec();

        refreshResult.forEach(event => {
            this.events.push({
                eventId: event._id,
                eventName: event.name,
                eventTime: event.time,
                eventDuration: event.duration,
                eventDescription: event.description,
                eventDate: event.date,
            });
        });
     },

     checkDateValidity(date){
        day = parseInt(date.split(".")[0]);
        month = date.split(".")[1];
        year = parseInt(date.split(".")[2]);
        var monthNames = ["January", "February", "March", "April", "May", "June",
         "July", "August", "September", "October", "November", "December"
        ];
        if(day > 0 && day <= 31 && monthNames.includes(month) && year >= 2000 && year <= 3000){
            return true
        }

        return false;
     }
 };

 module.exports = CalendarModel;
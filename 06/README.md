# 06. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Spletna povezava do uveljavitve na Bitbucket repozitorij (master):

https://bitbucket.org/tpoekipa/tpo-lp4/commits/51d49fb0acd28eed1b788270d2bc293844ab7124

Spletna povezava do različice produkcijske postavitve aplikacije:

http://tpo-app.herokuapp.com/



Uporabniško ime in geslo testnega študentskega uporabnika:

username: tpo@student

password: akamasoa



Uporabniško ime in geslo testnega skrbnika:

username: tpo@admin

password: akamasoa



Uporabniško ime in geslo testnega upravljalca z dogodki:

username: tpo@manager

password: akamasoa

var zahteva = require('supertest')
var streznik = require('../src/streznik.js')
var assert = require('assert')
var should = require('chai').should();

randomTaskDescription = Math.random().toString(36).slice(2);
var myFirstTaskId;

describe('Event stran', function() {
  it ('Prikaži zavihek Event, ki je aktiven', function(done) {
    zahteva(streznik).get('/').expect(/<li id="\homeButton\" class=\"active\"><a href=\"index\">Home<\/a><\/li>/i, done);
  });
});

describe('Dodajanje TODO-ja', function() {
  it ('Dodaj naključno generiran TODO task', function(done) {
    data = {text: randomTaskDescription};
    zahteva(streznik).post('/add-task').type('form').send(data).expect(/OK/i, done);
  });
});


describe('Pridobivanje TODO-jev', function() {
  var	myFirstTaskId = 5;
  it ('Preveri ali je naključno generiran TODO task pod vrnjenimi TODO-ji', function(done) {
    setTimeout(function () {
    zahteva(streznik)
    .post('/get-tasks', {})
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) done(err);
      var myFirstTask = res.body.find( function(todo){
        myFirstTaskId = todo.taskId;
        return todo.taskName == randomTaskDescription;
    });
    console.log(myFirstTask)
      assert.notEqual(undefined, myFirstTask);
       });
    done();}, 3000);
  });

it ('Izbrisi ta TODO', function(done) {
  setTimeout(function () {
  data = {id: myFirstTaskId};
  zahteva(streznik).post('/del-task').type('form').send(data).expect(/OK/i, done);
}, 5000);
  
  });

  it ('Preveri ali je naključno generiran TODO task pod vrnjenimi TODO-ji - zdaj ne sme biti', function(done) {
    setTimeout(function () {
      zahteva(streznik)
    .post('/get-tasks', {})
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) done(err);
      var myFirstTask = res.body.find( function(todo){
        return todo.taskName == randomTaskDescription;
    });
    should.not.exist(myFirstTask);
        });
    done();
  }, 7000);
    
  });
  
});
var mongoose = require('mongoose');
require('./uporabnik');
require('./administratorskaObvestila');
require('./dogodek');
require('./koledar');
require('./urnik');
require('./avtobus');
require('./todo');
require('./restavracija');


//povezemo se na bazo, ki je namescena lokaln
/*
var dbURI = 'mongodb://localhost/straightAs';
if (process.env.NODE_ENV === 'production') {//ce bojo deplojal na heroku rabmo tole se pohendlat na njihovi strani.
  dbURI = process.env.MLAB_URI;
}
console.log(process.env.IP);
*/
var dbURI = 'mongodb+srv://straightas:tpo123456@tpo-database-bl94y.mongodb.net/test?retryWrites=true'
mongoose.connect(dbURI, { useNewUrlParser: true, useCreateIndex: true });



mongoose.connection.on('connected', function() {
  console.log('Mongoose je povezan na ' + dbURI);
});


mongoose.connection.on('error', function(err) {
  console.log('Mongoose napaka pri povezavi: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('Mongoose je zaprl povezavo');
});

var pravilnaUstavitev = function(sporocilo, povratniKlic) {
  mongoose.connection.close(function() {
    console.log('Mongoose je zaprl povezavo preko ' + sporocilo);
    povratniKlic();
  });
};

// Pri ponovnem zagonu nodemon
process.once('SIGUSR2', function() {
  pravilnaUstavitev('nodemon ponovni zagon', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

// Pri izhodu iz aplikacije
process.on('SIGINT', function() {
  pravilnaUstavitev('izhod iz aplikacije', function() {
    process.exit(0);
  });
});

// Pri izhodu iz aplikacije na Heroku
process.on('SIGTERM', function() {
  pravilnaUstavitev('izhod iz aplikacije na Heroku', function() {
    process.exit(0);
  });
});
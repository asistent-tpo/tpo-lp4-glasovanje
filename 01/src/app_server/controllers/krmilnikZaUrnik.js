var request = require('request');
var Promise = require('promise');
var mongoose = require('mongoose');
var Urnik = mongoose.model('urnik');
var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

module.exports.vrniVse = function(req,res){
    
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
    
    
        Urnik.find({email:req.cookies.email},function (err, termini) {
            if (err)
                console.log(err);
            else{
                //console.dir("lokacije : "+termini);
                res.json(termini);
            }
        });
      }
    })
}

module.exports.vrni = function(req,res){
    
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var id2 = req.params.id.split(":")[1];
        Urnik.findOne({email:req.cookies.email,id:id2},function (err, vnos) {
            if (err)
                console.log(err);
            else{
                console.log(vnos);
                res.json(vnos);
            }
        });
      }
    })
};

module.exports.dodaj = function(req, res) {
    //console.dir(req.body);
    
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        Urnik.create({
            email : req.cookies.email,
            ime : req.body.ime,
            cas : req.body.cas,
            barva : req.body.barva,
            id : req.body.id
        }
        ,function(err, obj) {
            if (err) {
                console.log("error detected when saving to database.");
                if(ret.nivo==1){
                    res.render('home_pogled', { title: 'Sorry, it did not work at this time..',  user: req.cookies.email, upr:true });
                }else if(ret.nivo==2){
                    res.render('home_pogled', { title: 'Sorry, it did not work at this time..',  user: req.cookies.email, admin:true });
                }else{
                    res.render('home_pogled', { title: 'Sorry, it did not work at this time..',  user: req.cookies.email });
                }
            } else {
                console.log("saved to database! "+obj);
                res.redirect(req.get('referer'));//? v nacrtu nis ne kaze sporocila nazaj tko da bom refreshal stran in se vse vidi..
            }
        });
      }
    })
};

module.exports.posodobi = function(req,res){//id ne glede na id objekta ampak id celice. ker se celice ne pokrivajo glede na nacrt
    
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var id2 = req.params.id.split(":")[1];
        Urnik.findOneAndUpdate({id:id2},
        {
            email : req.cookies.email,
            ime : req.body.ime,
            cas : req.body.cas,
            barva : req.body.barva,
            id : id2
        },
        function(err, obj) {
            if (err) {
                console.error("error detected when updating vnos to database.");
            } else {
                console.log("saved to database! "+obj);
                res.status=201;
                res.json(obj);
            }
        });
      }
    })
};

module.exports.izbrisi = function(req,res){
    
    console.error("----------------------------------whattt?---------------------");
    
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var id2 = req.params.id.split(":")[1];
        console.error(id2);
        Urnik.deleteOne({id:id2},
        function(err, obj) {
        if (err) {
            console.error("error detected when deleting vnos from database." + err);
        } else {
            console.log("deleted from database! "+obj);
            res.status=204;
            res.json(obj);
        }
        });
      }
    })
};
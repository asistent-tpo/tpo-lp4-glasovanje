var request = require('request');
var apiParametri = {
  streznik: 'http://localhost:' + process.env.PORT //to je za rest storitve
};
var model_avtobus = require('../models/avtobus');
var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');
//VERIFIKACIJA NEPOTREBNA, SPREMEMBA V IZGLEDO

if (process.env.NODE_ENV === 'production') {
  //apiParametri.streznik = 'https://????nas heroku streznik//////////';
}

module.exports.avtobusi_obrazec = function(req, res) {
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
  
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        res.render('avtobusi', { title: 'StraightAs - Avtobusi' });
      }else{
        if(ret.nivo==1){
          //vrnemo napako, da je novo geslo neustrezno
          res.render('avtobusi', { title: 'StraightAs - Avtobusi', user: req.cookies.email, upr:true });
        }else if(ret.nivo==2){
           res.render('avtobusi', { title: 'StraightAs - Avtobusi', user: req.cookies.email, admin:true });
        }else{
           res.render('avtobusi', { title: 'StraightAs - Avtobusi', user: req.cookies.email });
        }
      }
  })
};

module.exports.prikazi_seznam_avtobusov = function(req,res){
    model_avtobus.pridobi_podatke_postaje(req.body.postaja, req, res);
    
};
var request = require('request');
var Promise = require('promise');
var mongoose = require('mongoose');
var Restavracija = mongoose.model('restavracija');
var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

var mapsApiKey = "AIzaSyDoH1G2UcPMrOWRQ_dfvO3HEDeXv_bu8DA";

//SPREMENJEN LE POGLED

module.exports.prikaziStran = function(req,res){
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
  
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        res.render('pogled_restavracije');
      }else{
        if(ret.nivo==1){
          res.render('pogled_restavracije', {  user: req.cookies.email, upr:true });
        }else if(ret.nivo==2){
           res.render('pogled_restavracije', {  user: req.cookies.email, admin:true });
        }else{
           res.render('pogled_restavracije', {  user: req.cookies.email });
        }
      }
  })
}

module.exports.vrniVseRestavracije = function(req,res){
  
  /*
  request("https://www.studentska-prehrana.si/sl/restaurant", function(error,response,body){
        console.log(error);
        ///vrnjen json samo posredujemo odjemalcu
        var lat = body.match(/data-lat="(.){12}/g);
        var lon = body.match(/data-lon="(.){12}/g);
        var ime = body.match(/data-lokal="(.)*"/g);
        console.log(lat.length);
        console.log(lon.length);
        console.log(ime.length);
        
        for (var i in lat)
        {
          console.log(lat[i]);
          console.log(lon[i]);
          console.log(ime[i]);
        }
        
    });
 
  */
  
  Restavracija.find({},function (err, lokacije) 
  {
    if (err) console.log(err);
    else{
     console.dir("lokacije : "+lokacije);
     res.json(lokacije);
    }
  });
  

/*

  Restavracija.create({
    ime : 'Ambient',
    lokacija : { 
        lat : '46.06785610',
        long :'14.51071210',
        naslov : 'Topniška 29a'
        }
    },function(err, obj) {
    if (err) {
      console.log("error detected when saving to database.");
    } else {
      console.log("saved to database! "+obj);
      //res.redirect('/home_pogled');//ce ga redirectam semle zaenkrat bo mogoce ured, podatke itak pridobi od kontrolerja za home. nevem zdjle kako bi vracov podatke nazaj na page tko da bom kr reloadov pa je to to
    }
  });
  Restavracija.create({
    ime : 'Bar Enka',
    lokacija : { 
        lat : '46.07597570',
        long :'14.51700920',
        naslov : 'Baragova 16'
        }
    },function(err, obj) {
    if (err) {
      console.log("error detected when saving to database.");
    } else {
      console.log("saved to database! "+obj);
      //res.redirect('/home_pogled');//ce ga redirectam semle zaenkrat bo mogoce ured, podatke itak pridobi od kontrolerja za home. nevem zdjle kako bi vracov podatke nazaj na page tko da bom kr reloadov pa je to to
    }
  });
  Restavracija.create({
    ime : 'Bistro Suwon',
    lokacija : { 
        lat : '46.07320870',
        long :'14.51074560',
        naslov : 'Dunajska 101'
        }
    },function(err, obj) {
    if (err) {
      console.log("error detected when saving to database.");
    } else {
      console.log("saved to database! "+obj);
      //res.redirect('/home_pogled');//ce ga redirectam semle zaenkrat bo mogoce ured, podatke itak pridobi od kontrolerja za home. nevem zdjle kako bi vracov podatke nazaj na page tko da bom kr reloadov pa je to to
    }
  });
  Restavracija.create({
    ime : 'Maharaja Indijska Restavracija',
    lokacija : { 
        lat : '46.06549710',
        long :'14.49075880',
        naslov : 'Vodnikova 35'
        }
    },function(err, obj) {
    if (err) {
      console.log("error detected when saving to database.");
    } else {
      console.log("saved to database! "+obj);
      //res.redirect('/home_pogled');//ce ga redirectam semle zaenkrat bo mogoce ured, podatke itak pridobi od kontrolerja za home. nevem zdjle kako bi vracov podatke nazaj na page tko da bom kr reloadov pa je to to
    }
  });
  Restavracija.create({
    ime : 'Mediterraneo',
    lokacija : { 
        lat : '46.05035770',
        long :'14.50774310',
        naslov : 'Ciril Metodov trg 16'
        }
    },function(err, obj) {
    if (err) {
      console.log("error detected when saving to database.");
    } else {
      console.log("saved to database! "+obj);
      //res.redirect('/home_pogled');//ce ga redirectam semle zaenkrat bo mogoce ured, podatke itak pridobi od kontrolerja za home. nevem zdjle kako bi vracov podatke nazaj na page tko da bom kr reloadov pa je to to
    }
  });
  */
}
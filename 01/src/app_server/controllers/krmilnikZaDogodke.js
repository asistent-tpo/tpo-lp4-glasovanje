var request = require('request');
var Promise = require('promise');
var mongoose = require('mongoose');
var Dogodek = mongoose.model('dogodek');
var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

module.exports.vrniVse = function(req,res){
    
    Dogodek.find({},function (err, rez) {
        if (err)
            console.log(err);
        else{
            //console.dir("lokacije : "+termini);
            res.json(rez);
        }
    });
}
//VERIFIKACIJA DODANA, UPORABNIK POTREBUJE NIVO 1

module.exports.objavi_dogodek = function(req, res) {//samo dodaj v  bazo
    
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA - MORA BIT UPRAVLJALEC Z DOGODKI-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton, 1).then(function(ret){
      
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var datum = req.body.datum+"T00:00:00.000Z";
        console.dir(req.body);
        Dogodek.create({
            ime : req.body.ime,
            datum : datum,
            organizator : req.body.organizator,
            opis : req.body.opis
        }
        ,function(err, obj) {
            if (err) {
                console.log("error detected when saving to database." + err);
                res.status = 500;
            } else {
                console.log("saved to database! "+obj);
                res.redirect(req.get('referer'));//? v nacrtu nis ne kaze sporocila nazaj tko da bom refreshal stran in se vse vidi..
            }
        });
      }
    })
};

module.exports.vrniZacetnoStran= function(req,res){
    console.log("ZAČETNA STRAN ZA DOGODKE");
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton, 1).then(function(ret){
      console.log(ret);
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        if(ret.nivo==1){
          //vrnemo napako, da je novo geslo neustrezno
          res.render('upravljalcevPogled', {user: req.cookies.email, upr:true });
        }else if(ret.nivo==2){
           res.render('upravljalcevPogled', {user: req.cookies.email, admin:true });
        }else{
           res.render('upravljalcevPogled', {user: req.cookies.email });
        }
      }
    })
}

module.exports.vrniZacetnoStranUporabnik= function(req,res){
    if(req.cookies.email){
        res.render('EventsPogled', {user: req.cookies.email})
    }else{
        res.render('EventsPogled')
    }
}
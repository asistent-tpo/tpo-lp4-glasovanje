var request = require('request');
var Promise = require('promise');
var mongoose = require('mongoose');
var model_koledar = require('../models/administratorskaObvestila');
var AdministratorskoObvestilo = mongoose.model('administratorskaObvestila');
var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

//PRI DODAJ ŠE DODATNO PREVERIMO, DA JE NIVO 2


module.exports.dodaj = function(req, res) {
    //console.dir(req.body);
    //-----------------------------------------TUKAJ PRIDE VERIFIKACIJA UPORABNIKA-----------------------------
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton, 2).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
    
    
            AdministratorskoObvestilo.create({
                obvestilo : req.body.obvestilo
            }
            ,function(err, obj) {
                if (err) {
                    console.log("error detected when saving to database.");
                    res.status(500);
                } else {
                    console.log("saved to database! "+obj);
                    setTimeout(function() { 
                        AdministratorskoObvestilo.deleteOne({
                            obvestilo : req.body.obvestilo
                        },
                        function(err, obj) {
                            if (err) {
                                console.log("error detected when removing alert");
                            }
                        });
                    }, 5000);
                    res.json(obj);
                }
            });
      }
    })
};

module.exports.vrniVse = function(req,res){
    
    AdministratorskoObvestilo.find({},function (err, rez) {
        if (err)
            console.log(err);
        else{
            //console.dir("lokacije : "+termini);
            res.json(rez);
        }
    });
}

module.exports.vrniPogled= function(req,res){
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton, 2).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton) ){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        res.render('Adminpogled', {user : req.cookies.email, admin:true})  
      }
    })
}


window.onload = koledar_start();

function izberi_datum_na_koledarju(dan_v_mesecu){
    document.getElementById("prikazi_brisanje_koledar_button").setAttribute("onclick","prikazi_list_za_brisanje_vnosa_v_koledar("+dan_v_mesecu+")");
    document.getElementById("prikazi_vnos_koledar_button").setAttribute("onclick","prikazi_obrazec_za_dodajanje_vnosa_v_koledar("+dan_v_mesecu+")");
    document.getElementById("prikazi_spreminjanje_koledar_button").setAttribute("onclick","prikazi_list_za_spreminjanje_vnosa_v_koledar("+dan_v_mesecu+")");
    
    //pobarvaj z obromo izbran datum?
    
    //poisc trenutno izbran datum in ga skenslaj
    var dnevi=document.getElementsByClassName("dan_v_koledarju"); // mamo vse objekte td ki predstavljajo dan


    //clear all  - for zanka mi je nekaj bugala tko da sm hardcodov. sj nardi isto so..
    dnevi[0].style.border="0px solid black";
    dnevi[1].style.border="0px solid black";
    dnevi[2].style.border="0px solid black";
    dnevi[3].style.border="0px solid black";
    dnevi[4].style.border="0px solid black";
    dnevi[5].style.border="0px solid black";
    dnevi[6].style.border="0px solid black";
    dnevi[7].style.border="0px solid black";
    dnevi[8].style.border="0px solid black";
    dnevi[9].style.border="0px solid black";
    dnevi[10].style.border="0px solid black";
    dnevi[11].style.border="0px solid black";
    dnevi[12].style.border="0px solid black";
    dnevi[13].style.border="0px solid black";
    dnevi[14].style.border="0px solid black";
    dnevi[15].style.border="0px solid black";
    dnevi[16].style.border="0px solid black";
    dnevi[17].style.border="0px solid black";
    dnevi[18].style.border="0px solid black";
    dnevi[19].style.border="0px solid black";
    dnevi[20].style.border="0px solid black";
    dnevi[21].style.border="0px solid black";
    dnevi[22].style.border="0px solid black";
    dnevi[23].style.border="0px solid black";
    dnevi[24].style.border="0px solid black";
    dnevi[25].style.border="0px solid black";
    dnevi[26].style.border="0px solid black";
    if(dnevi.length>=28)dnevi[27].style.border="0px solid black";
    if(dnevi.length>=29)dnevi[28].style.border="0px solid black";
    if(dnevi.length>=30)dnevi[29].style.border="0px solid black";
    if(dnevi.length>=31)dnevi[30].style.border="0px solid black";
    
    dnevi[dan_v_mesecu-1].style.border="2px solid black";
    //pobarvaj na novo izbran datum
}

function prikazi_obrazec_za_brisanje_vnosa_v_koledar(id){//potrebno bo odpreti eno okno vmes kjer bojo zlistani vsi vnosi tega dneva. tega ni na maskah in nikjer ampak se rabi ker z koledarja semoras zbrati bolj natancno od dneva
zapri_list_vnosov();
    
    
    var container = document.createElement('div');
    container.setAttribute('id','container_potrdilo_brisanja');
    container.setAttribute('class','form_vnos_koledar_popup');
    container.style.backgroundClip="red";
    
    
    var okno = document.createElement('form');
    okno.setAttribute('class','form-container');
    
    var naslov = document.createElement('h1');
    naslov.innerHTML = "Are you sure you want to delete this content?";
    
    container.appendChild(okno);
    okno.appendChild(naslov);
    
    var parent = document.getElementById('koledar');
    parent.appendChild(container);
   
    var cancel = document.createElement('div');//button nrdi post na form. we dont want that
    cancel.setAttribute('class','btn cancel');
    cancel.setAttribute('id','btn_cancel');
    cancel.setAttribute('onclick','zapri_obrazec_za_brisanje_vnosa_v_koledar()');
    cancel.innerHTML="Cancel";
    
    okno.appendChild(cancel);
    
    var confirm = document.createElement('div');
    confirm.setAttribute('class','btn');
    confirm.setAttribute('id','btn_confirm');
    confirm.setAttribute('onclick','potrdi_brisanje_vnosa_v_koledar("'+id+'")');
    confirm.innerHTML="Confirm";
    okno.appendChild(confirm);
    
    
    container.style.display = "block";
    console.log("listing za brisanje");
    
}

function zapri_obrazec_za_brisanje_vnosa_v_koledar(){
    
        var container =document.getElementById("container_potrdilo_brisanja");
    container.parentNode.removeChild(container);
}

function potrdi_brisanje_vnosa_v_koledar(id){
    console.log("porditev brisanja!");
    
    
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", "/home_pogled/koledar/id/:"+id, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                console.log("i guess sm dubu response?"+xhr.status);
                if(xhr.status==200 || xhr.status==201){
                    alert("Vnos uspešno odstranjen!");
                    zapri_obrazec_za_brisanje_vnosa_v_koledar();
                    window.location.href="/home_pogled";//refresh koledar
                    
                }
            }
        }
        xhr.send();
}

function prikazi_obrazec_za_spreminjanje_vnosa_v_koledar(id){
    zapri_list_vnosov();
    
    console.log("prikazujem obrazec za spreminjanje");
    
    var parent =document.getElementById("koledar_spreminjanje");
    parent.style.display = "block";
    //name, time, duration,description
    var name = document.getElementById("name_spreminjanje");
    var time = document.getElementById("time_spreminjanje");
    var duration = document.getElementById("duration_spreminjanje");
    var description = document.getElementById("description_spreminjanje");
    
    name.setAttribute('placeholder',"Ime eventa... Pridobivam iz baze..");
    time.setAttribute('placeholder',"Cas eventa.. Pridobivam iz baze..");
    duration.setAttribute('placeholder',"Trajanje eventa.. Pridobivam iz baze..");
    description.setAttribute('placeholder',"Opis eventa.. Pridobivam iz baze..");
    
    
    //http request
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/home_pogled/koledar/id/:"+id, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200){
                console.log("dobil response- vnos za specificni id!");
                //console.dir(xhr.response);//it works - imamo json objekte tipa vnos-po modelu koledar
                var vnos = JSON.parse(xhr.response);
                //console.dir(vnos);
                name.setAttribute('value',vnos.vnos.ime);
                time.setAttribute('value',vnos.vnos.zacetek);
                duration.setAttribute('value',vnos.vnos.trajanje);
                description.value=vnos.vnos.opis;
                
                document.getElementById("btn_spreminjanje").setAttribute('onclick','potrdi_spreminjanje_vnosa_v_koledar("'+id+'")');
            
                //console.log(vnos.vnos.ime);
            }
        }
    }
    xhr.send();
    
    
}

function potrdi_spreminjanje_vnosa_v_koledar(id){
    
    
    var mesec_leto=document.getElementById("calendar-month-year").innerHTML.split(" ");
    var mesec = mesec_leto[0];
 
    var month_name = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    var leto = parseInt(mesec_leto[1],10);
    var mesec_stevilo=0;
    for(var i=0;i<12;i++){
        if(mesec == month_name[i]){
                    mesec_stevilo=i;
                    break;
        }
    }
    var dan = document.getElementById("prikazi_vnos_koledar_button").getAttribute("onclick").split("(")[1].split(")")[0];
    console.log("dan --- " +dan);

    
    
    var datum=leto+":"+mesec+":"+dan;
    
    var email = getCookie("email");
    
    var name = document.getElementById("name_spreminjanje").value;
    //time
    var time = document.getElementById("time_spreminjanje").value;
    //duration
    var duration = document.getElementById("duration_spreminjanje").value;
    //description
    var description = document.getElementById("description_spreminjanje").value;
    
    console.log("spreminjanje - "+id+" "+name+" "+time+" "+duration+" "+description);
    //AVTENTICIRAJ SEJO------
    
    
    //spreminjanjeVnosaVKoledar-----------klicemo krmilnikZaKoledar
    //if authenticated
        var xhr = new XMLHttpRequest();
        xhr.open("PUT", "/home_pogled/koledar/id/:"+id, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                console.log("i guess sm dubu response?"+xhr.status);
                if(xhr.status==200 || xhr.status==200){
                    prikazi_uspesno_spreminjanje();
                    
                    
                }
            }
        }
        xhr.send(JSON.stringify({
            vnos : {
                email : email,
                id: "1",//nevem kaj tale id predstavlja tbh
                ime : name,
                datum : datum,
                zacetek :time,
                trajanje : duration,
                opis : description
            }
        }));
    
    
    
}
function prikazi_uspesno_spreminjanje(){
    console.log("Spreminjanje vnosa uspesno!");
    preklici_spreminjanje_vnosa_v_koledar();
    alert("spreminjanje uspešno!");
    
}

function preklici_spreminjanje_vnosa_v_koledar(){
    
    var parent =document.getElementById("koledar_spreminjanje").style.display = "none";
    //name, time, duration,description
    var name = document.getElementById("name_spreminjanje");
    var time = document.getElementById("time_spreminjanje");
    var duration = document.getElementById("duration_spreminjanje");
    var description = document.getElementById("description_spreminjanje");
    name.setAttribute('value',"");
    time.setAttribute('value',"");
    duration.setAttribute('value',"");
    description.setAttribute('value',"");
    document.getElementById("btn_spreminjanje").setAttribute('onclick','potrdi_spreminjanje_vnosa_v_koledar()');

}

function prikazi_list_za_brisanje_vnosa_v_koledar(dan_v_mesecu){

    var container = document.createElement('div');
    container.setAttribute('id','container_list_vnosov');
    container.setAttribute('class','form_vnos_koledar_popup');
    
    
    var okno = document.createElement('form');
    okno.setAttribute('class','form-container');
    
    var naslov = document.createElement('h1');
    naslov.innerHTML = "Razpored Dogodkov za dan";
    
    container.appendChild(okno);
    okno.appendChild(naslov);
    
    var parent = document.getElementById('koledar');
    parent.appendChild(container);
   
    var button_cancel = document.createElement('button');
    button_cancel.setAttribute('class','btn cancel');
    button_cancel.setAttribute('id','btn_cancel');
    button_cancel.setAttribute('onclick','zapri_list_vnosov()');
    button_cancel.innerHTML="Cancel";
    
    okno.appendChild(button_cancel);
    container.style.display = "block";
    console.log("listing za brisanje:  " + dan_v_mesecu);
    
    
    var mesec_leto=document.getElementById("calendar-month-year").innerHTML.split(" ");
    var mesec = mesec_leto[0];
 
    var month_name = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    var leto = parseInt(mesec_leto[1],10);
    var mesec_stevilo=0;
    for(var i=0;i<12;i++){
        if(mesec == month_name[i]){
                    mesec_stevilo=i;
                    break;
        }
    }
    
    
    
    var listContainer = document.createElement('ul');
    listContainer.className = "kol";
    okno.appendChild(listContainer);
    
    
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/home_pogled/koledar:"+leto+"/:"+mesec_stevilo+"/:"+dan_v_mesecu, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200){
                console.log("dobil response za vnose na ta dan!");
                console.dir(xhr.response);//it works - imamo json objekte tipa vnos-po modelu koledar
                var vnosi = JSON.parse(xhr.response);
                for(var i =0;i<vnosi.length;i++){
                    var vnos = vnosi[i];
                    //dodat kot child objekte na container
                    var entry =document.createElement('li');
                    entry.className = "koledar_izbira";
                    entry.innerHTML=vnos.vnos.ime;
                    entry.setAttribute('onclick','prikazi_obrazec_za_brisanje_vnosa_v_koledar("'+vnos._id+'")')
                    listContainer.appendChild(entry);
                }
            }
        }
    }
    xhr.send();
}


function prikazi_list_za_spreminjanje_vnosa_v_koledar(dan_v_mesecu){//potrebno bo odpreti eno okno vmes kjer bojo zlistani vsi vnosi tega dneva. tega ni na maskah in nikjer ampak se rabi
    
    
    var container = document.createElement('div');
    container.setAttribute('id','container_list_vnosov');
    container.setAttribute('class','form_vnos_koledar_popup');
    
    
    var okno = document.createElement('form');
    okno.setAttribute('class','form-container');
    
    var naslov = document.createElement('h1');
    naslov.innerHTML = "Razpored Dogodkov za dan";
    
    container.appendChild(okno);
    okno.appendChild(naslov);
    
    var parent = document.getElementById('koledar');
    parent.appendChild(container);
   
    var button_cancel = document.createElement('button');
    button_cancel.setAttribute('class','btn cancel');
    button_cancel.setAttribute('id','btn_cancel');
    button_cancel.setAttribute('onclick','zapri_list_vnosov()');
    button_cancel.innerHTML="Cancel";
    
    okno.appendChild(button_cancel);
    /*
    var button_confirm = document.createElement('button');
    button_confirm.setAttribute('class','btn');
    button_confirm.setAttribute('onclick','potrdi_spreminjanje_vnosa_v_koledar()');
    button_confirm.innerHTML = "Save changes";
    parent.appendChild(button_confirm);
    */
    
    //v for zanki gremo cez vse vnose na ta dan
    
    
    
    
    container.style.display = "block";
    console.log("listing za spreminjanje");
    
    
    var mesec_leto=document.getElementById("calendar-month-year").innerHTML.split(" ");
    var mesec = mesec_leto[0];
 
    var month_name = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    var leto = parseInt(mesec_leto[1],10);
    var mesec_stevilo=0;
    for(var i=0;i<12;i++){
        if(mesec == month_name[i]){
                    mesec_stevilo=i;
                    break;
        }
    }
    
    

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/home_pogled/koledar:"+leto+"/:"+mesec_stevilo+"/:"+dan_v_mesecu, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    var listContainer = document.createElement('ul');
    listContainer.className = "kol";
    okno.appendChild(listContainer);
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200){
                console.log("dobil response za vnose na ta dan!");
                console.dir(xhr.response);//it works - imamo json objekte tipa vnos-po modelu koledar
                var vnosi = JSON.parse(xhr.response);
                for(var i =0;i<vnosi.length;i++){
                    var vnos = vnosi[i];
                    //dodat kot child objekte na container
                    var entry = document.createElement('li');
                    entry.className = "koledar_izbira";
                    entry.innerHTML=vnos.vnos.ime;
                    entry.setAttribute('onclick','prikazi_obrazec_za_spreminjanje_vnosa_v_koledar("'+vnos._id+'")')
                    listContainer.appendChild(entry);
                }
            }
        }
    }
    xhr.send();

    
}

function zapri_list_vnosov(){
    var container =document.getElementById("container_list_vnosov");
    container.parentNode.removeChild(container);

}


function prikazi_obrazec_za_dodajanje_vnosa_v_koledar(dan_v_mesecu){
    
    //console.log(document.getElementById("datum").value);     //get datum oziroma id al karkoli ze pac rabi
    if(dan_v_mesecu == undefined){console.error("izbran ni noben datum v mesecu!");
        return;
    }
    var mesec_leto=document.getElementById("calendar-month-year").innerHTML;
    console.log("clicked a date! lets find the date : " + mesec_leto+" "+dan_v_mesecu);
    
    document.getElementById("eventTitle").innerHTML = "Calendar Event for "+mesec_leto+" "+dan_v_mesecu;
    document.getElementById("koledar_vnos").style.display = "block";
}

function preklici_dodajanje_vnosa_v_koledar(){
    document.getElementById("koledar_vnos").style.display = "none";
}

function vrni_datum(){
    var datum2 = document.getElementById("eventTitle").innerHTML.split("for")[1].split(" ")
    //console.log(datum2[2]+" "+datum2[1]+" "+datum2[3]);//hacky af
    var datum=datum2[2]+":"+datum2[1]+":"+datum2[3];
    
    return datum;
}


function potrdi_dodajanje_vnosa_v_koledar(){
    //datum oziroma id na koledarju
    
    var email = getCookie("email");
    
    var datum=vrni_datum();
    //name
    var name = document.getElementById("name").value;
    //time
    var time = document.getElementById("time").value;
    //duration
    var duration = document.getElementById("duration").value;
    //description
    var description = document.getElementById("description").value;
    
    console.log(datum+" "+name+" "+time+" "+duration+" "+description);
    //AVTENTICIRAJ SEJO------
    
    
    //dodajanjeVnosaVKoledar-----------klicemo krmilnikZaKoledar
    //if authenticated
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/home_pogled/koledar", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                console.log("i guess sm dubu response?");
                alert("Vnos uspešno dodan v koledar!");
                window.location.href="/home_pogled";
            }
        }
        
        xhr.send(JSON.stringify({
            vnos : {
                email : email,
                id: "1",//nevem kaj tale id predstavlja tbh
                ime : name,
                datum : datum,
                zacetek :time,
                trajanje : duration,
                opis : description
            }
        }));
}


function vrni_vnose_meseca(mesec_stevilo,leto){//za dolocen mesec pricakuje podatke o vnosu v json obliki verjetno. po dobljenih podatkih doda na vsak dan v mesecu, če ima kakšnem vnos skrit child object, ki se expanda, ko hoce pregledat vnose na tem datumu. to je treba nekak imet nrjen za spreminjanje dogodka.
    var xhr = new XMLHttpRequest();
        xhr.open("GET", "/home_pogled/koledar:"+leto+"/:"+mesec_stevilo, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if(xhr.status==200){
                    //console.dir(xhr.response);//it works - imamo json objekte tipa vnos-po modelu koledar
                    //alert(xhr.response);
                    posodobi_koledar_z_vnosi(xhr.response)
                }
            }
        }
        xhr.send();
}

function posodobi_koledar_z_vnosi(xhr_resp){
    pobrisi_vse_vnose();
    //console.dir(JSON.parse(xhr_resp));
    var vnosi = JSON.parse(xhr_resp)
    //gremo poiskat datume
    for(var i=0;i<vnosi.length;i++){
        var dan=vnosi[i].vnos.datum.split("T")[0].split("-")[2];
        //console.log(dan);
        dodaj_vnos_v_izris_koledarja(parseInt(dan)-1,vnosi[i]);
    }
}

function dodaj_vnos_v_izris_koledarja(dan,vnos){
    //z koledarja pobrisi vser trenutne vnose
    console.dir(vnos);
    var dnevi=document.getElementsByClassName("dan_v_koledarju");
    
    dnevi[dan].style.backgroundColor = "gray";
}

function pobrisi_vse_vnose(){
    //z koledarja pobrisi vser trenutne vnose - mogoce ni potrebno
}

function pristej_mesec(){
    //get current year and month
    var datum = document.getElementById("calendar-month-year").innerHTML.split(" ");
    var month_name = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var mesec= datum[0];
    var mesec_stevilo=0;
    var leto = parseInt(datum[1],10);
    
    for(var i=0;i<12;i++){
        if(mesec == month_name[i]){
            if(i<11)
                {
                    mesec_stevilo=i+1;
                }
                else{
                    mesec_stevilo=0;
                    leto = leto+1;
                }
        }
    }
    
    zgeneriraj_mesec(mesec_stevilo,leto);
    vrni_vnose_meseca(mesec_stevilo,leto);
   
}
function odstej_mesec(){
        //get current year and month
    var datum = document.getElementById("calendar-month-year").innerHTML.split(" ");
    var month_name = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var mesec= datum[0];
    var mesec_stevilo=0;
    var leto = parseInt(datum[1],10);
    
    for(var i=0;i<12;i++){
        if(mesec == month_name[i]){
            if(i>0)
                {
                    mesec_stevilo=i-1;
                }
                else{
                    mesec_stevilo=11;
                    leto = leto-1;
                }
        }
    }
    
    zgeneriraj_mesec(mesec_stevilo,leto);
    vrni_vnose_meseca(mesec_stevilo,leto);
}


//generiranje koledarja
function zgeneriraj_mesec(mesec,leto){
    //clear previous
    //document.getElementById("calendar-container").innerHTML="";
    //if(document.getElementById("calendar-header") !=null)document.getElementById("calendar-header").innerHTML="";
    if(document.getElementById("calendar-month-year") !=null)document.getElementById("calendar-month-year").innerHTML = "";
    if(document.getElementById("calendar-dates")!=null)document.getElementById("calendar-dates").innerHTML="";
    //document.getElementsById("calendar-dates")
    
    var month_name = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var month =mesec;   //0-11 dela tud za februar
    var year = leto; //2019
    var first_date = month_name[month] + " " + 1 + " " + year;
    var tmp = new Date(first_date).toDateString();
    var first_day = tmp.substring(0, 3);    //Mon
    var day_name = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var day_no = day_name.indexOf(first_day);   //1
    var days = new Date(year, month+1, 0).getDate();    //30
    var calendar = get_calendar(day_no, days);
    document.getElementById("calendar-month-year").innerHTML = month_name[month]+" "+year;
    document.getElementById("calendar-dates").appendChild(calendar);
}



function koledar_start(){
    var d = new Date();
    zgeneriraj_mesec(d.getMonth(),d.getFullYear());
    vrni_vnose_meseca(d.getMonth(),d.getFullYear());
    
}

function get_calendar(day_no, days){
    var table = document.createElement('table');
    var tr = document.createElement('tr');
    
    //row for the day letters
    for(var c=0; c<=6; c++){
        var td = document.createElement('td');
        if(c==0)
            td.innerHTML = "Su";
        if(c==1)
            td.innerHTML = "Mo";
        if(c==2)
            td.innerHTML = "Tu";
        if(c==3)
            td.innerHTML = "We";
        if(c==4)
            td.innerHTML = "Th";
        if(c==5)
            td.innerHTML = "Fr";
        if(c==6)
            td.innerHTML = "Sa";
            
        tr.appendChild(td);
    }
    table.appendChild(tr);
    
    //create 2nd row
    tr = document.createElement('tr');
    var c;
    for(c=0; c<=6; c++){
        if(c == day_no){
            break;
        }
        var td = document.createElement('td');
        td.innerHTML = "";
        tr.appendChild(td);
        
        
    }
    
    var count = 1;
    for(; c<=6; c++){
        var td = document.createElement('td');
        td.innerHTML = count;
        tr.appendChild(td);
        td.setAttribute("onclick", "izberi_datum_na_koledarju("+count+")");
        td.setAttribute("class","dan_v_koledarju");
        count++;

    }
    table.appendChild(tr);
    
    //rest of the date rows
    for(var r=3; r<=7; r++){
        tr = document.createElement('tr');
        for(var c=0; c<=6; c++){
            if(count > days){
                table.appendChild(tr);
                return table;
            }
            var td = document.createElement('td');
            td.innerHTML = count;
            td.setAttribute("onclick", "izberi_datum_na_koledarju("+count+")")
            td.setAttribute("class","dan_v_koledarju");
            count++;
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
	return table;
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
//        div.todoItem sdfsdfsdfdsfdfdsfdfs

window.onload = todo_start();
var todo_body;

function todo_start(){
    todo_body=document.getElementById('todoBody');
    //pober iz baze in namec iteme
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/todo", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if(xhr.status==200 || xhr.status==201){
                    var vnos = JSON.parse(xhr.response);
                    console.dir("response = "+vnos);
                
                    for(var i=0;i<vnos.length;i++){
                        var item = document.createElement('div');
                        item.setAttribute('class','todoItem');
                        item.innerHTML = vnos[i].opis; //rip za xss attack lol
                        item.setAttribute('onclick','izberi_vnos_v_todo("'+vnos[i]._id+'")');
                        
                       // item.addEventListener("dblclick",function(e){
                    //        e.preventDefault();
                    //        prikazi_spreminjanje_todo(vnos[i]._id);
                    //    },false);
                        
                        //item.setAttribute('ondblclick','prikazi_spreminjanje_todo("'+vnos[i]._id+'")');
                        todo_body.appendChild(item);
                    }
                }
            }
        }
        xhr.send();
}

var izbran_id_vnosa=0;//izbran glede na _id od mongoDB

function prikazi_dodajanje(){
    document.getElementById('todo_dodajanje').style.display="block";
}

function potrdi_dodajanje_vnosa_v_todo(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/todo/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200 ||xhr.status==201){
                console.log("dobil response za dodajanje =  "+xhr.response);
                preklici_dodajanje_vnosa_v_todo();
                alert("Vnos uspešno ustvarjen!");
                window.location.href="/home_pogled";
            }else{
                console.error("Napaka pri spreminjanju- odgovor iz baze je fail.");
            }
        }
    }
    xhr.send(JSON.stringify({
        opis : document.getElementById('description_todo_dodajanje').value
    }));
}
function preklici_dodajanje_vnosa_v_todo(){
    document.getElementById('todo_dodajanje').style.display="none";
    document.getElementById('todo_spreminjanje').style.display="none";
    
}


function izberi_vnos_v_todo(id){
    var cells = document.getElementsByClassName('todoItem');
    for(var i=0;i<cells.length;i++){
        cells[i].style.border = "0px solid gray";
    }
    izbran_id_vnosa=id;
    event.target.style.border="2px solid black";
}

function zapri_obrazec_za_brisanje(){
    var container =document.getElementById("container_potrdilo_brisanja");
    container.parentNode.removeChild(container);
}

function prikazi_odstranjevanje(){
    var container = document.createElement('div');
    container.setAttribute('id','container_potrdilo_brisanja');
    container.setAttribute('class','form_vnos_koledar_popup');
    container.style.backgroundClip="red";
    
    
    var okno = document.createElement('form');
    okno.setAttribute('class','form-container');
    
    var naslov = document.createElement('h1');
    naslov.innerHTML = "Are you sure you want to delete this content?";
    
    container.appendChild(okno);
    okno.appendChild(naslov);
    
    var parent = document.getElementById('todo');
    parent.appendChild(container);
   
    var cancel = document.createElement('div');//button nrdi post na form. we dont want that
    cancel.setAttribute('class','btn cancel');
    cancel.setAttribute('id','btn_cancel');
    cancel.setAttribute('onclick','zapri_obrazec_za_brisanje()');
    cancel.innerHTML="Cancel";
    
    okno.appendChild(cancel);
    
    var confirm = document.createElement('div');
    confirm.setAttribute('class','btn');
    confirm.setAttribute('id','btn_confirm');
    confirm.setAttribute('onclick','potrdi_brisanje()');
    confirm.innerHTML="Confirm";
    okno.appendChild(confirm);
    
    
    container.style.display = "block";
}

function potrdi_brisanje(){
    console.log("porditev brisanja!"+izbran_id_vnosa);
    
    
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", "/todo/:"+izbran_id_vnosa, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            //console.log("i guess sm dubu response?"+xhr.status);
            if(xhr.status==200 || xhr.status==204){
                alert("Vnos uspešno odstranjen!");
                zapri_obrazec_za_brisanje();
                window.location.href="/home_pogled";//refresh koledar
            }else{
                alert("prislo jke do napake na serverju.");
            }
        }
    }
    xhr.send();
}

function prikazi_spreminjanje(){
    document.getElementById('todo_spreminjanje').style.display="block";
    var opis_t=document.getElementById('description_todo_spreminjanje');
    opis_t.innerHTML="Pridobivam podatke z serverja..";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/todo/:"+izbran_id_vnosa, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200){
                var vnos = JSON.parse(xhr.response);
                opis_t.innerHTML=vnos.opis;
            }else if(xhr.status==404){
                alert("prislo je do napake na strani serverja.");
                preklici_dodajanje_vnosa_v_todo();
            }
        }
    }
    xhr.send();
}

function potrdi_spreminjanje_vnosa_v_todo(){
    
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", "/todo/:"+izbran_id_vnosa, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200 ||xhr.status==201){
                console.log("dobil response za dodajanje =  "+xhr.response);
                preklici_dodajanje_vnosa_v_todo();
                alert("Vnos uspešno ustvarjen!");
                window.location.href="/home_pogled";
            }else{
                console.error("Napaka pri spreminjanju- odgovor iz baze je fail.");
            }
        }
    }
    xhr.send(JSON.stringify({
        opis : document.getElementById('description_todo_spreminjanje').value
    }));
}
function prikazi_okno(){
    document.getElementById('dodajanje').style.display="block";
}
function preklici_dodajanje(){
    document.getElementById('dodajanje').style.display="none";
    document.getElementById('obvestilo_dodajanje').value="";
}
function potrdi_dodajanje(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/obvestilo", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200 ||xhr.status==201){
                console.log("dobil response za dodajanje =  "+xhr.response);
                preklici_dodajanje();
                alert("Obvestilo uspešno ustvarjeno!");
            }else{
                console.error("Napaka pri spreminjanju- odgovor iz baze je fail.");
            }
        }
    }
    xhr.send(JSON.stringify({
        obvestilo : document.getElementById('obvestilo_dodajanje').value
    }));

}
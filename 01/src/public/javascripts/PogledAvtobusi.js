var Container = document.createElement('div')
document.getElementsByTagName('body')[0].appendChild(Container);
Container.setAttribute("align","center");

function potrdi_vnos_avtobusne_postaje()
{
    
    
    var postaja = document.getElementById('postaja');
    
     var data = {
        "postaja" : postaja.value
    };
    
    Container.innerHTML = "Osvežujem...";
    
    $.post("/avtobusi",data,function(receive){
        var obj = JSON.parse(receive);
        
        //alert(JSON.stringify(obj));
        
        Container.innerHTML = "";
        
        if (obj.error != null)
        {
            Container.innerHTML = "Postaje ni bilo mogoče najti!";
        }
        
        ///gremo skozi json in vnose zapisujemo v tabelo
        for(var i in obj.stations)
        {
            var listContainer = document.createElement('div');
            Container.appendChild(listContainer);
            var Header = document.createElement('h1');
            Header.innerHTML = "<font size='6'>" + obj.stations[i].name + "</font>";
            listContainer.appendChild(Header);
            //listContainer.setAttribute("style","font-size:30");
            console.log(obj.stations[i].name);
            
            var list = document.createElement('table');
            list.setAttribute("width","50%");
            var listHeader = document.createElement('thead');
            var listRow = document.createElement('tr');
            var col1 = document.createElement('td');
            col1.className = "naslov";
            var col2 = document.createElement('td');
            var col3 = document.createElement('td');
            
            col1.innerHTML = "<font size='4'>Smer:";
            col2.innerHTML = "<font size='4'>Številka:";
            col3.innerHTML = "<font size='4'>Prihodi:";
            
            col1.setAttribute("width","50%");
            col2.setAttribute("width","25%");
            col3.setAttribute("width","25%");
            
            listRow.appendChild(col1);
            listRow.appendChild(col2);
            listRow.appendChild(col3);
            
            listHeader.appendChild(listRow);
            list.appendChild(listHeader);
            
        
            var listBody = document.createElement('tbody');
            
            //alert(JSON.stringify(obj.stations[i].buses));
            
            for(var j in obj.stations[i].buses)
            {
                
                var prihodi = "";
                for(var k in obj.stations[i].buses[j].arrivals)
                {
                    prihodi = prihodi + obj.stations[i].buses[j].arrivals[k] + " min, ";
                }
                
                var listRow = document.createElement('tr');
                var col1 = document.createElement('td');
                col1.className = "first";
                var col2 = document.createElement('td');
                var col3 = document.createElement('td');
                
                col1.innerHTML = obj.stations[i].buses[j].direction;
                col2.innerHTML = obj.stations[i].buses[j].number;
                col3.innerHTML = prihodi;
                
                listRow.appendChild(col1);
                listRow.appendChild(col2);
                listRow.appendChild(col3);
                
                listBody.appendChild(listRow);

            }
            list.appendChild(listBody);
            listContainer.appendChild(list);
            
        }
        //alert(JSON.stringify(obj));
        
    },"json");
    
    return false
    
}
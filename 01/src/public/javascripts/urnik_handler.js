var urnik;
var izbrana_celica=-1;
var celice=[];
window.onload = start_urnik();

function start_urnik(){
    urnik=document.getElementById('urnik');
    zgeneriraj_urnik();
    napolni_urnik_z_vnosi();
}

function zgeneriraj_urnik(){
    var day_name = ['','Mon','Tue','Wed','Thu','Fri'];
    var table = document.createElement('table');
    var tr = document.createElement('tr');

    //row for the day letters
    for(var c=0; c<=5; c++){
        var td = document.createElement('td');
        td.innerHTML = day_name[c];
        td.setAttribute('class','urnik_cell');
        
        if(c==0){//naredit buttone
            var btn_add=document.createElement('button');
            btn_add.setAttribute('onclick','dodaj_vnos_v_urnik()');
            btn_add.innerHTML="+";
            btn_add.style.position="relative";
            btn_add.style.margin="1px";
            td.appendChild(btn_add);
        
            
            var btn_delete =document.createElement('button');
            btn_delete.setAttribute('onclick','zbrisi_vnos_v_urniku()');
            btn_delete.innerHTML="-";
            btn_delete.style.position="relative";
            btn_add.style.margin="1px";
            td.appendChild(btn_delete);
            
            var btn_modify=document.createElement('button');
            btn_modify.setAttribute('onclick','spremeni_vnos_v_urniku()');
            btn_modify.innerHTML="*";
            btn_modify.style.position="relative";
            btn_add.style.margin="1px";
            td.appendChild(btn_modify);
        }
        
        tr.appendChild(td);
    }
    table.appendChild(tr);


//-----se druge vrstice

    var casi=['8:00','9:00','10:00','11:00','12:00','13:00','14:00',];

    var s=0;
    
    var index_global=0;
    
    for(var row=0;row<7;row++){
        var tr = document.createElement('tr');
        for(var c=0; c<=5; c++){
            var td = document.createElement('td');
            
            if(c==0){
                td.innerHTML=casi[s];
                s++;
            }else{
                td.innerHTML = "";
                td.setAttribute('onclick','izberi_celico_v_urniku("'+index_global+'")');//pondeljek = 1,2,3,4,5,6,7,8 , terk = 9,10,11,..
                td.setAttribute('id','urnik_cell_'+index_global);
                celice.push(td);
                index_global++;
            }
            td.setAttribute('class','urnik_cell');
            tr.appendChild(td);
            
        }
        table.appendChild(tr);
    }




    urnik.appendChild(table);
    //urnik.innerHTML="asasd";
}

function izberi_celico_v_urniku(id){
    //1,2,3,4,5,6,7,8 - pondeljek
    //9,10,11,12,13,14,15,16 - torek
    //...
    izbrana_celica=id;
    //clearej obrobe vseh celic
    var cells = document.getElementsByClassName('urnik_cell');
    for(var i=0;i<cells.length;i++){
        cells[i].style.border = "2px solid gray";
    }
    console.log("id celice::: " + id);
    var cell = document.getElementById("urnik_cell_"+id);
    cell.style.border = "3px solid red";
}

//get cell by id ?

function napolni_urnik_z_vnosi(){
    var xhr = new XMLHttpRequest();
        xhr.open("GET", "/urnik", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if(xhr.status==200 || xhr.status==201){
                    var vnos = JSON.parse(xhr.response);
                    console.dir("response = "+vnos);
                
                    for(var i=0;i<vnos.length;i++){
                        izrisi_predmet_na_urnik(vnos[i]);
                    }
                }
            }
        }
        xhr.send();
}

function izrisi_predmet_na_urnik(r){
    var cell = celice[r.id];
    console.dir(r);
    console.dir(celice);
    console.dir(cell);
    
    cell.innerHTML = r.ime;
    cell.style.backgroundImage = "linear-gradient(#fff, "+r.barva+")";
    //za dostop do specifičnega objekta lahko uporabmo kr ta id za celice. glede na načrt predpostavljam da se urnik ne more prekrivati?. uporabniških testov ni so..
}

function dodaj_vnos_v_urnik(){
    
    console.log("izrisujem okno za dodajanje");
    
    document.getElementById('urnik_dodajanje').style.display="block";
}

function spremeni_vnos_v_urniku(){
    if(izbrana_celica==-1){ alert("prosim izberite celico");return;}
    
    document.getElementById('urnik_spreminjanje').style.display="block";
    
        //ime
    var ime = document.getElementById('ime_urnik_spreminjanje');
    //cas
    var cas = document.getElementById('cas_urnik_spreminjanje');
    //barva
    var se = document.getElementById('barva_urnik_spreminjanje');
    var barva = se.options[se.selectedIndex].value;
        ime.setAttribute('placeholder',"Ime vnosa... Pridobivam iz baze..");
    
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/urnik/:"+izbrana_celica, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200){
                var vnos = JSON.parse(xhr.response);
                console.dir(vnos);
                ime.setAttribute('value',vnos.ime);
                cas.setAttribute('value',vnos.cas);
                //za barvo se nic ne spremeni..
            }else if(xhr.status==404){
                alert("izbrali ste prezen termin. Prosim izberite že vpisan termin za spreminjanje.");
                preklici_dodajanje_vnosa_v_urnik();
            }
        }
    }
    xhr.send();
    
}

function zapri_obrazec_za_brisanje(){
    console.log("zapiranje obrazca");
    var container =document.getElementById("container_potrdilo_brisanja");
    container.parentNode.removeChild(container);
}

function zbrisi_vnos_v_urniku(){
    
    console.log("izrisujem okno za brisanje----------------------------------------");
    var container = document.createElement('div');
    container.setAttribute('id','container_potrdilo_brisanja');
    container.setAttribute('class','form_vnos_koledar_popup');
    container.style.backgroundClip="red";
    
    
    var okno = document.createElement('form');
    okno.setAttribute('class','form-container');
    
    var naslov = document.createElement('h1');
    naslov.innerHTML = "Are you sure you want to delete this content?";
    
    container.appendChild(okno);
    okno.appendChild(naslov);
    
    var parent = document.getElementById('urnik');
    parent.appendChild(container);
   
    var cancel = document.createElement('div');//button nrdi post na form. we dont want that
    cancel.setAttribute('class','btn cancel');
    cancel.setAttribute('id','btn_cancel');
    cancel.setAttribute('onclick','zapri_obrazec_za_brisanje()');
    cancel.innerHTML="Cancel";
    
    okno.appendChild(cancel);
    
    var confirm = document.createElement('div');
    confirm.setAttribute('class','btn');
    confirm.setAttribute('id','btn_confirm');
    confirm.setAttribute('onclick','potrdi_brisanje_urnik()');
    confirm.innerHTML="Confirm";
    okno.appendChild(confirm);
    
    
    container.style.display = "block";
}

function potrdi_brisanje_urnik(){
    //console.log("???????????????????????????????????????");
    console.log("porditev brisanja!" + izbrana_celica);
    
    
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", "/urnik/:"+izbrana_celica, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            //console.log("i guess sm dubu response?"+xhr.status);
            if(xhr.status==200 || xhr.status==204){
                alert("Vnos uspešno odstranjen!");
                preklici_dodajanje_vnosa_v_urnik();
                window.location.href="/home_pogled";//refresh koledar
            }
        }
    }
    xhr.send();
}

function potrdi_dodajanje_vnosa_v_urnik(){
    //ime
    var iime = document.getElementById('ime_urnik_dodajanje').value;
    //cas
    var icas = document.getElementById('cas_urnik_dodajanje').value;
    //barva
    var se = document.getElementById('barva_urnik_dodajanje');
    var ibarva = se.options[se.selectedIndex].value;
    //id?
    var iid = izbrana_celica;
    
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/urnik", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            console.log("dobil response "+xhr.response);
            preklici_dodajanje_vnosa_v_urnik();
            alert("Vnos uspešno dodan v koledar!");
            window.location.href="/home_pogled";
        }
    }
    xhr.send(JSON.stringify({
        ime : iime,
        zacetek : icas,//obsolete zaradi nacina implementacije
        barva : ibarva,
        id : iid
    }));

    
}

function potrdi_spreminjanje_vnosa_v_urnik(){
        //ime
    var iime = document.getElementById('ime_urnik_spreminjanje').value;
    //cas
    var icas = document.getElementById('cas_urnik_spreminjanje').value;
    //barva
    var se = document.getElementById('barva_urnik_spreminjanje');
    var ibarva = se.options[se.selectedIndex].value;
    //id?
    var iid = izbrana_celica;
    
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", "/urnik/:"+iid, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200 ||xhr.status==201){
                console.log("dobil response za spreminjanje =  "+xhr.response);
                preklici_dodajanje_vnosa_v_urnik();
                alert("Vnos uspešno posodobljen!");
                window.location.href="/home_pogled";
            }else{
                console.error("Napaka pri spreminjanju- odgovor iz baze je fail.");
            }
        }
    }
    xhr.send(JSON.stringify({
        ime : iime,
        cas : icas,//obsolete zaradi nacina implementacije
        barva : ibarva,
        id : iid
    }));
}

function preklici_dodajanje_vnosa_v_urnik(){
        document.getElementById('urnik_dodajanje').style.display="none";
        document.getElementById('urnik_spreminjanje').style.display="none";
        //document.getElementById('urnik_dodajanje').style.display="none";
}
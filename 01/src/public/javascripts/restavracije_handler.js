var map;
var pozicija;
var list;
var vmesni_list;
var opis_na_strani;

function initMap() {
  var Ljubljana = {lat: 46.0565, lng: 14.50581};
  //try to get location from user
  
  //else
    pozicija=Ljubljana;
  
  list = document.getElementById("restavracije_select");
  opis_na_strani = document.getElementById("opis_na_strani");
  map = new google.maps.Map(
    document.getElementById('map'), {zoom: 14, center: Ljubljana});
    var infoWindow = new google.maps.InfoWindow;
    
    // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
  
  zahtevaj_podatke();
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}


function addMarker(l){
    console.log("dodajam marker na lokacijo "+ l.lokacija.lat+" "+l.lokacija.long+" "+l.ime);
    var marker = new google.maps.Marker({position: {lat: parseFloat(l.lokacija.lat), lng: parseFloat(l.lokacija.long)}, map: map, label : ""});
    marker.id = l._id;
    
    marker.addListener('click', function() {
      map.setZoom(16);
      map.setCenter(marker.getPosition());
      marker.setLabel(l.ime);
      console.log("klik na marker z _id = "+marker.id);
      //gremo izbrat v select boxu vrednost, ki ustreza temu id-ju
      var index=get_index_from_value(marker.id)
      //console.dir(index);
      list.selectedIndex=index;
      opis_na_strani.innerHTML=l.lokacija.naslov;
    });
}

function get_index_from_value(val){
    for(var i=0;i<list.options.length;i++){
        if(list.options[i].value == val)return i;
    }
    return -1;
}

function addValueToSelect(l){
    var opcija = document.createElement('option');
    opcija.innerHTML=l.ime;
    opcija.setAttribute('value',l._id);
    list.appendChild(opcija);
}

function zahtevaj_podatke(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/pogled_restavracije/vrniVse", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.status==200){
                
                console.log(xhr.response);
                
                
                
                //-----------------ce smo pridobili lokacijo se mora ta array sortirati po oddaljenosti od lokacije, sicer pa po abecedi.
                var lokacije = JSON.parse(xhr.response)
                //lokacije =sort(lokacije);
                
                
                var lokacije = JSON.parse(xhr.response)
                console.dir(lokacije);
                //vmesni_list=null;
                //vmesni_list=lokacije;//tole je za mapiranje value iz selecta na lokacijo ker envem ce se da nekak lahko prenasat _id
                for(var i=0;i<lokacije.length;i++){
                    
                    addMarker(lokacije[i]);
                    addValueToSelect(lokacije[i]);
                }
                
            }
        }
    }
    xhr.send();
    
}
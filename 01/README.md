# 01. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Link do zadnjega commita:

https://bitbucket.org/lgustin/tpo-lp4/commits/c641748e0a99bc59b165185d711b55b1d676b6cd

Link do postavljene spletne strani:

http://t123tpo456.herokuapp.com

Trije uporabniki, vsak svojega tipa (email - geslo)

user@user - 12345678

admin@admin - adminadmin

upravljalec@upravljalec - upravljalec

var zahteva = require('supertest')
var streznik = require('../src/server/bin/www')
var assert = require('assert');
var http = require('http');
var request = require('request');
var should = require('should');

var server = zahteva.agent("http://localhost:3000");
var URL = 'http://localhost:3000/api/v1/';
var JWT = "";

/**
 *  DUMMY TEST EXAMPLE
 * */
describe('DUMMY TEST', function () {
  it('should return 200', function (done) {
    http.get(URL, function (res) {
      assert.equal(200, res.statusCode);
      done();
    });
  });

  it('should say {"content":"HELLO FROM BACKEND! You look awesome!"}', function (done) {
    http.get(URL, function (res) {
      var data = '';

      res.on('data', function (chunk) {
        data += chunk;
      });

      res.on('end', function () {
        assert.equal('{"content":"HELLO FROM BACKEND! You look awesome!"}', data);
        done();
      });
    });
  });
});


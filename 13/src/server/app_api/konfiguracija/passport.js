var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

passport.use(new LokalnaStrategija({
		usernameField: 'email',
		passwordField: 'geslo'
	}, 
	function(email, geslo, koncano) {
		Uporabnik.findOne(
			{
				email: email
			},
			function(napaka, uporabnik) {
				if (napaka)
					return koncano(napaka);
				if (!uporabnik) {
					return koncano(null, false, {
				    	sporocilo: 'Napačen email'
				  	});
				}
				if (!uporabnik.preveriGeslo(geslo)) {
				  	return koncano(null, false, {
				    	sporocilo: 'Napačno geslo'
				  	});
				}
				return koncano(null, uporabnik);
			}
		);
	}
));
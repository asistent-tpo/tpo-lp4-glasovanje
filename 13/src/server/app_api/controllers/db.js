var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');
var Restavracija = mongoose.model('Restavracija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

var france = "francepreseren@gmail.com";
var ivan = "ivancankar@straightAs.com";
var ivana = "ivanakobilica@gmail.com";
var primoz = "primoztrubar@straightAs.com";
var drago = "dragojancar01@gmail.com";

module.exports.clearUporabniki = function(zahteva, odgovor) {
    Uporabnik
        .remove()
        .exec(function(napaka, uporabniki){
           if(napaka){
               vrniJsonOdgovor(odgovor, 500, napaka);
           } else {
               vrniJsonOdgovor(odgovor, 200, {"sporocilo": "vsi uporabniki so izbrisani, kot 91-prve!"});
           }
        });
};

module.exports.clearDrago = function(zahteva, odgovor) {
    Uporabnik
        .remove({email: drago})
        .exec(function(napaka, uporabniki){
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 200, {"sporocilo": "Uporabnik zbrisan"});
            }
        });
};

module.exports.ustvariGenericneUporabnike = function (zahteva, odgovor) {
    var uporabnikObj = new Uporabnik();
    uporabnikObj.nastaviGeslo('mojeGeslo');
    uporabnikObj.nastaviVerifikacijskihToken();

    Uporabnik.collection.insertMany([
        {
            email: france,
            geslo: uporabnikObj.geslo,
            nakljucnaVrednost: uporabnikObj.nakljucnaVrednost,
            role: 'administrator',
            potrditveniToken: "",
            potrjen: true
        },
        {
            email: ivana,
            geslo: uporabnikObj.geslo,
            nakljucnaVrednost: uporabnikObj.nakljucnaVrednost,
            role: 'uporabnik',
            potrditveniToken: "",
            potrjen: true
        },
        {
            email: ivan,
            geslo: uporabnikObj.geslo,
            nakljucnaVrednost: uporabnikObj.nakljucnaVrednost,
            role: 'upravljalec',
            potrditveniToken: "",
            potrjen: true
        },
        {
            email: primoz,
            geslo: uporabnikObj.geslo,
            nakljucnaVrednost: uporabnikObj.nakljucnaVrednost,
            role: 'upravljalec',
            potrditveniToken: "",
            potrjen: true
        }
    ], function (napaka, dat) {
        if(napaka){
            vrniJsonOdgovor(odgovor, 500, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 200, {"sporocilo": "success"})
    })
};

module.exports.clearRestavracije = function(zahteva, odgovor) {
    Restavracija
        .remove()
        .exec(function(napaka, restavracije){
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 200, {"sporocilo": "vse restavracije so izbrisane!"});
            }
        });
};

module.exports.dodajRestavracije = function (zahteva, odgovor) {
    var restavracijaObj = new Restavracija();

    Restavracija.collection.insertMany([
        {
            id: 1,
            name: "Biotehniška fakulteta, Menza BF",
            lat: 46.05124,
            long: 14.4677753,
            price: "2,97",
            address: "Večna pot 111, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 2,
            name: "Centralna postaja",
            lat: 46.0525491,
            long: 14.5068887,
            price: "3.77",
            address: "Trubarjeva cesta 23, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 3,
            name: "DA BU DA, Azijska restavracija",
            lat: 46.0508178,
            long: 14.4996943,
            price: "4,37",
            address: "Šubičeva 1a, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 4,
            name: "Gostilnica in pivnica Katrca",
            lat: 46.050755,
            long: 14.482995,
            price: "4,00",
            address: "Rožna dolina, Cesta I. 26A, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 5,
            name: "Kavarna NUK",
            lat: 46.047605,
            long: 14.5015261,
            price: "2,80",
            address: "Turjaška 1, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 6,
            name: "PE Marjetica",
            lat: 46.0487784,
            long: 14.491212,
            price: "2,27",
            address: "Tobačna ulica 5, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 7,
            name: "Pizzeria Foculus",
            lat: 46.0478638,
            long: 14.5000782,
            price: "4,30",
            address: "Gregorčičeva ulica 3, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 8,
            name: "Pizzeria Parma",
            lat: 46.0500542,
            long: 14.4982707,
            price: "3,80",
            address: "Trg republike 2, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 9,
            name: "Restavracija 123 FRI in FKKT",
            lat: 46.0500752,
            long: 14.4667279,
            price: "2,20",
            address: "Večna pot 113, 1000 Ljubljana",
            distance: 0
        },
        {
            id: 10,
            name: "Restavracija Fany & Mary",
            lat: 46.0520491,
            long: 14.5061472,
            price: "4,37",
            address: "Petkovškovo nabrežje 19, 1000 Ljubljana",
            distance: 0
        }

    ], function (napaka, dat) {
        if(napaka){
            vrniJsonOdgovor(odgovor, 500, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 200, {"sporocilo": "Restavracije uspesno dodane v bazo."})
    })
}
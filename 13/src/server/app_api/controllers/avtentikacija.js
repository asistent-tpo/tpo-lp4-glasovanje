var passport = require('passport');
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');
var mailer = require('../konfiguracija/mailer');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.registracija = function(zahteva, odgovor) {
    if (!zahteva.body.email || !zahteva.body.geslo) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
        return;
    }
    var uporabnik = new Uporabnik();
    uporabnik.email = zahteva.body.email;
    uporabnik.role = 'uporabnik';
    uporabnik.nastaviGeslo(zahteva.body.geslo);
    uporabnik.nastaviVerifikacijskihToken();

    Uporabnik
        .findOne({email: uporabnik.email})
        .exec(function(napaka, uporabnikDB) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
                return;
            }
            if(uporabnikDB){
                vrniJsonOdgovor(odgovor, 409, {"sporocilo": `Uporabnik z emailom: ${uporabnikDB.email}, ze obstaja`});
            } else {
                uporabnik.save(function(napaka) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                    } else {
                        vrniJsonOdgovor(odgovor, 200, {"sporocilo": `Uporabnik z emailom: ${uporabnik.email}, je dodan, preverite email.`});

                        const html =
                            `<h1>Zivjo pob!</h1>
                            <br/>
                            <p>Prosimo te, da klikneš a link in s tem boš potrdil svoj email! Brez tega pa ne gre!</p>
                            <br/>
                            <p>${uporabnik.potrditveniToken}</p>
                            <a href="http://localhost:3000/api/v1/verifikacija/${uporabnik.potrditveniToken}">KLIKNI TUKAJ!</a>`

                        mailer.sendMessage(uporabnik.email, html);
                    }
                });
            }
        });
};

module.exports.verifikacija = function(zahteva, odgovor) {
    if(!zahteva.params.token){
        vrniJsonOdgovor(odgovor, 400, {"sporocilo": "token je obvezen!"});
        return;
    }

    Uporabnik.findOne({potrditveniToken: zahteva.params.token})
        .exec(function(napaka, uporabnik){

            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
                return
            }

            if(!uporabnik){
                vrniJsonOdgovor(odgovor, 404, {"sporocilo": "User z podanim tokenom ne obstaja"});
            } else {
                uporabnik.potrjen = true;
                uporabnik.potrditveniToken = "";
                uporabnik.save(function(napaka) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                    } else {
                        vrniJsonOdgovor(odgovor, 200, {"sporocilo": `Uporabnik je potrjen lahko se prijavite.`});
                    }
                });
            }

        });
};

module.exports.prijava = function(zahteva, odgovor) {
    if (!zahteva.body.email || !zahteva.body.geslo) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }
    passport.authenticate('local', function(napaka, uporabnik, podatki) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if (uporabnik) {

            if(!uporabnik.potrjen){
                vrniJsonOdgovor(odgovor, 403, {"sporocilo": "Uporabnik ni potrjen!"});
                return;
            }

            vrniJsonOdgovor(odgovor, 200, {
                "zeton": uporabnik.generirajJwt()
            });

        } else {
            vrniJsonOdgovor(odgovor, 401, {"sporocilo": "Napaen email ali geslo"});
        }
    })(zahteva, odgovor);
};

module.exports.sprememba = function (zahteva, odgovor) {
    if (!zahteva.body.novoGeslo || !zahteva.payload.email) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        var uporabnik = new Uporabnik();
        uporabnik.nastaviGeslo(zahteva.body.novoGeslo);

        Uporabnik.findOneAndUpdate(
            {email: zahteva.payload.email},
            {$set: {
                geslo: uporabnik.geslo,
                nakljucnaVrednost: uporabnik.nakljucnaVrednost
            }},
            function (napaka, doc) {
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            }else{
                vrniJsonOdgovor(odgovor, 201, {"sporocilo": true});
            }
        })
    }
};
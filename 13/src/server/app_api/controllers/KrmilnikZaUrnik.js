var mongoose = require('mongoose');
var Urnik = mongoose.model('Urnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.dodaj_vnos_v_urnik = function(zahteva, odgovor) {
    if(!zahteva.body.title || !zahteva.body.start || !zahteva.body.end || !zahteva.body.color || !zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        var urnik = new Urnik();
        urnik.email = zahteva.payload.email;
        urnik.title = zahteva.body.title;
        urnik.start = zahteva.body.start;
        urnik.end = zahteva.body.end;
        urnik.color = zahteva.body.color;
        urnik.meta.duration = zahteva.body.meta.duration;

        urnik.save(function (napaka, ur) {
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            }
            vrniJsonOdgovor(odgovor, 201, {"_id": ur._doc._id});
        })
    }
};

module.exports.vrni_vrednost_za_urnik = function(zahteva, odgovor) {
    if(!zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {

        Urnik.find({email: zahteva.payload.email})
            .exec(function(napaka, zapisi){
                if(napaka){
                    vrniJsonOdgovor(odgovor, 500, napaka);
                } else {
                    vrniJsonOdgovor(odgovor, 200, zapisi);
                }
            });
    }
};

module.exports.izbrisi_vnos_iz_urnika = function(zahteva, odgovor) {
    if(!zahteva.params.id || !zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        Urnik.remove({"_id": zahteva.params.id, "email": zahteva.payload.email})
            .exec(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                }
                vrniJsonOdgovor(odgovor, 201, {"sporocilo": true});
            });
    }
};

module.exports.posodobi_vnos_v_urniku = function(zahteva, odgovor) {
    if(!zahteva.params.id || !zahteva.body.title || !zahteva.body.start ||
        !zahteva.body.end || !zahteva.body.color || !zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        Urnik.findOneAndUpdate(
            {_id: zahteva.params.id},
            {$set: {
                    title: zahteva.body.title,
                    start: zahteva.body.start,
                    end: zahteva.body.end,
                    color: zahteva.body.color
                }},
            function (napaka, doc) {
                if(napaka){
                    vrniJsonOdgovor(odgovor, 500, napaka)
                }else{
                    vrniJsonOdgovor(odgovor, 203, doc)
                }
            })
    }
};
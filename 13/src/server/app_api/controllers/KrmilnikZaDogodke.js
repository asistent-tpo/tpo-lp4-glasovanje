var mongoose = require('mongoose');
var Dogodki = mongoose.model('Dogodki');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.dodaj_vnos_v_dogodke = function(zahteva, odgovor) {
    if(!zahteva.body.name || !zahteva.body.organizer || !zahteva.body.date || !zahteva.body.description){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    var dogodki = new Dogodki();
    dogodki.name = zahteva.body.name;
    dogodki.organizer = zahteva.body.organizer;
    dogodki.date = zahteva.body.date;
    dogodki.description = zahteva.body.description;

    dogodki.save(function (err, dg) {
        if(err){
            vrniJsonOdgovor(odgovor, 500, err);
        }

        vrniJsonOdgovor(odgovor, 201, {"_id": dg._doc._id});
    })

};

module.exports.izbrisi_vnos_iz_dogodkov = function(zahteva, odgovor) {

    if(!zahteva.params.id){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    Dogodki.remove({"_id": zahteva.params.id})
        .exec(function(napaka){
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            }
            vrniJsonOdgovor(odgovor, 201, {"sporocilo": true});
        });
};

module.exports.vrni_vse_dogodke = function(zahteva, odgovor) {
    if(false){
        vrniJsonOdgovor(odgovor, 400, {"sporocilo": "napaka"});
    } else {
        Dogodki
            .find({})
            .exec(function(napaka, dogodki) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                    return;
                }

                var odg = [];

                for(dogodek in dogodki){
                    var dog = {
                        "name": dogodki[dogodek]._doc.name,
                        "organizer": dogodki[dogodek]._doc.organizer,
                        "date": dogodki[dogodek]._doc.date,
                        "description": dogodki[dogodek]._doc.description,
                        "_id": dogodki[dogodek]._doc._id.toString()
                    };
                    odg.push(dog);
                }

                vrniJsonOdgovor(odgovor, 200, odg);
            });
    }
};
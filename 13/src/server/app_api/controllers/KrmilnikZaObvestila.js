var mongoose = require('mongoose');
var Obvestila = mongoose.model('Obvestila');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.dodaj_vnos = function(zahteva, odgovor) {
    if(!zahteva.body.date || !zahteva.body.content){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    var obvestila = new Obvestila();
    obvestila.date = zahteva.body.date;
    obvestila.content = zahteva.body.content;

    obvestila.save(function (err, ob) {
        if(err){
            vrniJsonOdgovor(odgovor, 500, err);
            return
        }

        if(ob){
            vrniJsonOdgovor(odgovor, 201, {"_id": ob._doc._id});
        } else {
            vrniJsonOdgovor(odgovor, 201, {});
        }

    })

};

module.exports.izbrisi_vnos = function(zahteva, odgovor) {

    if(!zahteva.body.date || !zahteva.body.content){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    Obvestila.remove({"_id": zahteva.params.id})
        .exec(function(napaka){
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            }
            vrniJsonOdgovor(odgovor, 201, {"sporocilo": true});
        });
};

module.exports.vrni_vse = function(zahteva, odgovor) {
    Obvestila
        .find()
        .exec(function(napaka, ob) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
                return;
            }

            if(!ob[0]){
                vrniJsonOdgovor(odgovor, 201, []);
            } else {
                vrniJsonOdgovor(odgovor, 201, ob);
            }
        });
};
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.uporabnikiSeznam = function(zahteva, odgovor) {
    Uporabnik
        .find()
        .exec(function(napaka, uporabniki) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
                return;
            }
            vrniJsonOdgovor(odgovor, 200, uporabniki);
        });
};

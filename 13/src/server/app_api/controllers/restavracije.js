var mongoose = require('mongoose');
var Restavracija = mongoose.model('Restavracija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.restavracijeSeznam = function(zahteva, odgovor) {
    Restavracija
        .find()
        .exec(function(napaka, restavracije) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
                return;
            }
            vrniJsonOdgovor(odgovor, 200, restavracije);
        });
};

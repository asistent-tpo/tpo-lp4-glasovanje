var mongoose = require('mongoose');
var Koledar = mongoose.model('Koledar');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.vrni_vrednost_za_koledar = function(zahteva, odgovor) {
    if(!zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {

        Koledar.find({email: zahteva.payload.email})
            .exec(function(napaka, zapisi){
               if(napaka){
                   vrniJsonOdgovor(odgovor, 500, napaka);
               } else {
                   vrniJsonOdgovor(odgovor, 200, zapisi);
               }
            });
    }
};

module.exports.dodaj_vnos_v_koledar = function(zahteva, odgovor) {

    if(!zahteva.body.title || !zahteva.body.start || !zahteva.body.end || !zahteva.body.color || !zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        var koledar = new Koledar();

        koledar.email = zahteva.payload.email;
        koledar.title = zahteva.body.title;
        koledar.start = zahteva.body.start;
        koledar.end = zahteva.body.end;
        koledar.color = zahteva.body.color;
        koledar.meta.description = zahteva.body.meta.description;
        koledar.meta.duration = zahteva.body.meta.duration;

        koledar.save(function (napaka, cal) {
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka);
            }
            vrniJsonOdgovor(odgovor, 201, {"_id": cal._doc._id});
        })
    }
};

module.exports.posodobi_vnos_v_koledarju = function(zahteva, odgovor) {
    if(!zahteva.params.id || !zahteva.body.title || !zahteva.body.start ||
        !zahteva.body.end || !zahteva.body.color || !zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        Koledar.findOneAndUpdate(
            {_id: zahteva.params.id},
            {$set: {
                title: zahteva.body.title,
                start: zahteva.body.start,
                end: zahteva.body.end,
                color: zahteva.body.color
            }},
            function (napaka, doc) {
                if(napaka){
                    vrniJsonOdgovor(odgovor, 500, napaka)
                }else{
                    vrniJsonOdgovor(odgovor, 203, doc)
                }
            })
    }
};

module.exports.izbrisi_vnos_iz_koledarja = function(zahteva, odgovor) {
    if(!zahteva.params.id || !zahteva.payload.email){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    } else {
        Koledar.remove({"_id": zahteva.params.id, "email": zahteva.payload.email})
            .exec(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                }
                vrniJsonOdgovor(odgovor, 201, {"sporocilo": true});
            });
    }
};
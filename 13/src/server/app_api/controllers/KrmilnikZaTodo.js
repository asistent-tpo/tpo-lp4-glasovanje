var mongoose = require('mongoose');
var Todo = mongoose.model('Todo');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.posodobi_vnos_v_todo = function(zahteva, odgovor) {
    if(!zahteva.payload.email || !zahteva.body.title || !zahteva.params.id){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    Todo.findOneAndUpdate(
        {_id: zahteva.params.id},
        {$set: {opis: zahteva.body.title}},
        function (napaka, doc) {
            if(napaka){
                vrniJsonOdgovor(odgovor, 500, napaka)
            }else{

                var odg = {
                    "_id": doc._id,
                    "title": doc.opis,
                    "editable": false,
                    "completed": false,
                };

                vrniJsonOdgovor(odgovor, 203, odg)
            }
        })
};

module.exports.dodaj_vnos_v_todo = function(zahteva, odgovor) {
    if(!zahteva.payload.email || !zahteva.body.title){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    var todo = new Todo();
    todo.email = zahteva.payload.email;
    todo.opis = zahteva.body.title;

    todo.save(function (err, td) {
        if(err){
            vrniJsonOdgovor(odgovor, 500, err);
        }

        vrniJsonOdgovor(odgovor, 201, {"_id": td._doc._id});
    })

};

module.exports.izbrisi_vnos_v_todo = function(zahteva, odgovor) {

    if(!zahteva.payload.email || !zahteva.params.id){
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Zahtevani so vsi podatki"
        });
    }

    Todo.remove({"_id": zahteva.params.id, "email": zahteva.payload.email})
        .exec(function(napaka){
           if(napaka){
               vrniJsonOdgovor(odgovor, 500, napaka);
           }
           vrniJsonOdgovor(odgovor, 201, {"sporocilo": true});
        });
};

module.exports.vrni_vse = function(zahteva, odgovor) {
    if(!zahteva.payload.email || !zahteva.payload.role){
        vrniJsonOdgovor(odgovor, 400, {"sporicilo": "ni podatkov o uporabniku"});
    } else {
        Todo
            .find({email: zahteva.payload.email})
            .exec(function(napaka, todoi) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                    return;
                }

                var odg = [];

                for(td in todoi){
                    var t = {
                        "title": todoi[td]._doc.opis,
                        "completed": false,
                        "editable": false,
                        "_id": todoi[td]._doc._id.toString()
                    };
                    odg.push(t);
                }

                vrniJsonOdgovor(odgovor, 200, odg);
            });
    }
};
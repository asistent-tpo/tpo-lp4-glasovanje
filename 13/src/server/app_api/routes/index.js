var express = require('express');
var dbConf = require('../controllers/db');
var router = express.Router();

var secret = process.env.JWT_GESLO;

if(!secret){
    secret = "superHik420";
}

var jwt = require('express-jwt');
var avtentikacija = jwt({
    secret: secret,
    userProperty: 'payload'
});

var uporabnikiController = require('../controllers/uporabniki');
var avtentikacijaController = require('../controllers/avtentikacija');
var todoController = require('../controllers/KrmilnikZaTodo');
var restavracijeController = require('../controllers/restavracije');
var koledarController = require('../controllers/KrmilnikZaKoledar');
var urnikController = require('../controllers/KrmilnikZaUrnik');
var dogodkiController = require('../controllers/KrmilnikZaDogodke');
var messageController = require('../controllers/KrmilnikZaObvestila');

router.get('/', function(req, res) {
    res.status(200);
    res.json({"content": "HELLO FROM BACKEND! You look awesome!"});
});

/* Uporabniki */
router.get('/uporabniki', avtentikacija, uporabnikiController.uporabnikiSeznam);

/* Avtentikacije */
router.post('/registracija', avtentikacijaController.registracija);
router.post('/prijava', avtentikacijaController.prijava);
router.post('/sprememba', avtentikacija, avtentikacijaController.sprememba);
router.get('/verifikacija/:token', avtentikacijaController.verifikacija);

/* TODOi */
router.post('/todo', avtentikacija, todoController.dodaj_vnos_v_todo);
router.put('/todo/:id', avtentikacija, todoController.posodobi_vnos_v_todo);
router.delete('/todo/:id', avtentikacija, todoController.izbrisi_vnos_v_todo);
router.get('/todo', avtentikacija, todoController.vrni_vse);

/* DOGODKI */
router.post('/dogodki', avtentikacija, dogodkiController.dodaj_vnos_v_dogodke);
router.delete('/dogodki/:id', avtentikacija, dogodkiController.izbrisi_vnos_iz_dogodkov);
router.get('/dogodki', avtentikacija, dogodkiController.vrni_vse_dogodke);

/* KOLEDAR */
router.get('/calendar', avtentikacija, koledarController.vrni_vrednost_za_koledar);
router.post('/calendar', avtentikacija, koledarController.dodaj_vnos_v_koledar);
router.put('/calendar/:id', avtentikacija, koledarController.posodobi_vnos_v_koledarju);
router.delete('/calendar/:id', avtentikacija, koledarController.izbrisi_vnos_iz_koledarja);

/* URNIK */
router.get('/urnik', avtentikacija, urnikController.vrni_vrednost_za_urnik);
router.post('/urnik', avtentikacija, urnikController.dodaj_vnos_v_urnik);
router.put('/urnik/:id', avtentikacija, urnikController.posodobi_vnos_v_urniku);
router.delete('/urnik/:id', avtentikacija, urnikController.izbrisi_vnos_iz_urnika);

/* DEVELOPMENT TO DE DELETE AFTER */
router.get('/development/db/clear', dbConf.clearUporabniki);
router.get('/development/db/insert', dbConf.ustvariGenericneUporabnike);
router.get('/development/db/removeDrago', dbConf.clearDrago);

/* MESSAGE */
router.get('/messages', messageController.vrni_vse);
router.post('/messages', avtentikacija, messageController.dodaj_vnos);
router.delete('/messages', avtentikacija, messageController.izbrisi_vnos);

/* RESTAVRACIJE */
router.get('/development/db/insertRestavracije', dbConf.dodajRestavracije);
router.get('/development/db/clearRestavracije', dbConf.clearRestavracije);
router.get('/restavracije', restavracijeController.restavracijeSeznam);

module.exports = router;
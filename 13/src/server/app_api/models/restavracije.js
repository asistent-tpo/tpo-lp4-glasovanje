var mongoose = require('mongoose');

var restavracijeShema = new mongoose.Schema({
    id: {type: Number, unique: true, required: true},
    ime: String,
    latitude: Number,
    longitude: Number,
    price: String,
    address: String,
    distance: Number
});

mongoose.model('Restavracija', restavracijeShema, 'Restavracije');
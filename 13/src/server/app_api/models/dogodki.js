var mongoose = require('mongoose');

var dogodkiShema = new mongoose.Schema({
    name: {type: String, required: true},
    organizer: {type: String, required: true},
    date: {type: String, required: true},
    description: {type: String, required: true}
});


dogodkiShema.methods.ustvari_vnos_v_dogodke = function(){

};

dogodkiShema.methods.izbrisi_vnos_iz_dogodkov = function(id){

};

dogodkiShema.methods.vrni_vse_dogodke = function(){

};


mongoose.model('Dogodki', dogodkiShema, 'Dogodki');


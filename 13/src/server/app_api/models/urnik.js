var mongoose = require('mongoose');

var urnikSchema = new mongoose.Schema({
    email: {type: String, required: true},
    title: {type: String, required: true},
    start: {type: String, required: true},
    end: {type: String, required: true},
    color: {type: String, required: true},
    meta: {
        duration: {type: Number}
    }
});


urnikSchema.methods.posodobi_vnos = function(email, id, ime, trajanje, barva){

};

urnikSchema.methods.dodaj_vnos = function(email, id, ime, trajanje, barva){

};

urnikSchema.methods.izbrisi_vnos = function(id){

};

urnikSchema.methods.vrni_vse_vnose = function(email){

};


mongoose.model('Urnik', urnikSchema, 'Urnik');

var mongoose = require('mongoose');

var obvestilaSchema = new mongoose.Schema({
    date: {type: String, required: true},
    content: {type: String, required: true}
});


obvestilaSchema.methods.ustvari_obvestilo = function(email, id, opis){

};

obvestilaSchema.methods.izbrisi_obvestilo = function(email, id, opis){

};

obvestilaSchema.methods.vrni_vse = function(email, id, opis){

};


mongoose.model('Obvestila', obvestilaSchema, 'Obvestila');

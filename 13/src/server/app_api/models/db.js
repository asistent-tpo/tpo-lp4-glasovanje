require('./uporabniki');
require('./restavracije');
require('./todo');
require('./koledar');
require('./urnik');
require('./dogodki');
require('./administratorskaobvestila');

var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/sadb';

if (process.env.NODE_ENV === 'production') {
    dbURI = 'mongodb://filipzupancic:knjiga@mongodb-2724-0.cloudclusters.net:10003/lp4-tpo?authSource=admin';
}

mongoose.connect(dbURI, { useNewUrlParser: true, useCreateIndex: true }, function(){
    /* Drop the DB */
});


mongoose.connection.on('connected', function() {
    console.log('Mongoose je povezan na ' + dbURI);
});


mongoose.connection.on('error', function(err) {
    console.log('Mongoose napaka pri povezavi: ' + err);
});

mongoose.connection.on('disconnected', function() {
    console.log('Mongoose je zaprl povezavo');
});

var pravilnaUstavitev = function(sporocilo, povratniKlic) {
    mongoose.connection.close(function() {
        console.log('Mongoose je zaprl povezavo preko ' + sporocilo);
        povratniKlic();
    });
};

// Pri ponovnem zagonu nodemon
process.once('SIGUSR2', function() {
    pravilnaUstavitev('nodemon ponovni zagon', function() {
        process.kill(process.pid, 'SIGUSR2');
    });
});

// Pri izhodu iz aplikacije
process.on('SIGINT', function() {
    pravilnaUstavitev('izhod iz aplikacije', function() {
        process.exit(0);
    });
});

// Pri izhodu iz aplikacije na Heroku
process.on('SIGTERM', function() {
    pravilnaUstavitev('izhod iz aplikacije na Heroku', function() {
        process.exit(0);
    });
});

var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var randomString = require('randomstring');
require('dotenv').config();


var uporabnikiShema = new mongoose.Schema({
    email: {type: String, unique: true, required: true},
    geslo: {type: String, required: true},
    potrditveniToken: String,
    potrjen: {type: Boolean, default: false},
    nakljucnaVrednost: String,
    role: String
});

uporabnikiShema.methods.nastaviGeslo = function(geslo) {
    this.nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
    this.geslo = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

uporabnikiShema.methods.preveriGeslo = function(geslo) {
    var zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
    return this.geslo == zgoscenaVrednost;
};

uporabnikiShema.methods.nastaviVerifikacijskihToken = function() {

    this.potrditveniToken = randomString.generate({
        length: 12,
        charset: 'alphabetic'
    });
    this.potrditveniToken = this.potrditveniToken.toUpperCase();
    this.potrjen = false;

};

uporabnikiShema.methods.generirajJwt = function() {
    var datumPoteka = new Date();
    datumPoteka.setDate(datumPoteka.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        role: this.role,
        datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
    }, 'superHik420');
};

mongoose.model('Uporabnik', uporabnikiShema, 'Uporabniki');


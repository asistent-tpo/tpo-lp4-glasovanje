var mongoose = require('mongoose');

var koledarSchema = new mongoose.Schema({
    email: {type: String, required: true},
    title: {type: String, required: true},
    start: {type: String, required: true},
    end: {type: String, required: true},
    color: {type: String, required: true},
    meta: {
        description: {type: String},
        duration: {type: Number}
    }
});

koledarSchema.methods.preveri_veljavnost_datuma = function(date) {};

koledarSchema.methods.vrni_naslednji_id = function(email) {};

koledarSchema.methods.vrni_vse_vnose = function(email) {};

koledarSchema.methods.dodaj_vnos = function(email, id, ime, datum, opis) {};

koledarSchema.methods.izbrisi_vnos = function(email, id) {};

koledarSchema.methods.posodobi_vnos = function(email, id, ime, datum, opis) {};

mongoose.model('Koledar', koledarSchema, 'Koledar');
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { PasswordValidator } from './shared/password.validator';
import { RegistrationService } from './registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  confirmPassword: any;
  constructor(private fb: FormBuilder, private registrationService: RegistrationService) { }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      geslo: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['']},
      { validator: PasswordValidator });
  }

  get email() {
    return this.registrationForm.get('email');
  }

  get geslo() {
    return this.registrationForm.get('geslo');
  }

  onSubmit() {
    this.registrationService.register(this.registrationForm.value)
      .subscribe(
        response => {
          window.alert("Registration was successful! Please check your mail for conformation before you log in.");
        },
        error => {
          window.alert("Registration error! Email taken. Please retry.");
        }
      );
  }

}

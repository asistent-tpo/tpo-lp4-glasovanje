import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  CalendarEvent,
  CalendarEventTitleFormatter,
  CalendarView
} from 'angular-calendar';
import { DayViewHourSegment } from 'calendar-utils';
import {addDays, addHours, addMinutes, endOfWeek, startOfDay} from 'date-fns';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {UrnikDialogComponent} from '../urnik-dialog/urnik-dialog.component';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError as observableThrowError } from 'rxjs';


const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';

function floorToNearest(amount: number, precision: number) {
  return Math.floor(amount / precision) * precision;
}

function ceilToNearest(amount: number, precision: number) {
  return Math.ceil(amount / precision) * precision;
}

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  weekTooltip(event: CalendarEvent, title: string) {
    if (!event.meta.tmpEvent) {
      return super.weekTooltip(event, title);
    }
  }

  dayTooltip(event: CalendarEvent, title: string) {
    if (!event.meta.tmpEvent) {
      return super.dayTooltip(event, title);
    }
  }
}



const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#286e14',
    secondary: '#44da29'

  },
};

function getColor(color: string) {
  if (color === 'red') { return colors.red; }
  if (color === 'blue') { return colors.blue; }
  if (color === 'green') { return colors.green; }
  if (color === 'yellow') { return colors.yellow; }

}



// tslint:disable-next-line max-classes-per-file
@Component({
  selector: 'app-urnik',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'urnik.component.html',
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter
    }
  ],
  styleUrls: ['urnik.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UrnikComponent implements OnInit {
  viewDate = new Date();

  events: CalendarEvent[] = [];


  constructor(private cdr: ChangeDetectorRef, public inputDialog: MatDialog, private http: HttpClient, ) {}

  ngOnInit(): void {
    this.getEvents();
  }

  selectToCreate(
    segment: DayViewHourSegment) {
    this.createEvent(segment);
    // this.events = [...this.events, newEvent];
  }

  private getColorString(color: any) {
    if (color === colors.red) { return 'red'; }
    if (color === colors.blue) { return 'blue'; }
    if (color === colors.green) { return 'green'; }
    if (color === colors.yellow) { return 'yellow'; }


  }

  private refresh() {
    this.events = [...this.events];
    this.cdr.detectChanges();
  }

  // v bazi se hrani Calendar Event, ki ima event.id, event.

  getEvents(): CalendarEvent[] {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.get(APIURL + '/urnik', httpOptions).pipe(catchError(this.errorHandler)).subscribe((response: any) => {
      // za vsak event v response naredim nov calendarEvent in ga dodam v this.events
      this.events = [];
      for (const e of response) {
        const ev: CalendarEvent = e;
        ev.id = e._id;
        ev.start = new Date(e.start);
        ev.end = new Date(e.end);
        ev.color = getColor(e.color);
        this.events = [...this.events, ev];
      }
      this.refresh();
    });
    return this.events;
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

  editEvent(event: CalendarEvent<any>) {
    const inputDialogConfig = new MatDialogConfig();

    inputDialogConfig.disableClose = true;
    inputDialogConfig.autoFocus = true;

    inputDialogConfig.data = {
      id: 1,
      name: 'Edit event ' + event.title,
      title: event.title,
      color: this.getColorString(event.color),
      duration: event.meta.duration,
    };


    const dialogRef = this.inputDialog.open(UrnikDialogComponent, inputDialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data !== undefined && data === 'delete') {
          this.delete(event);
        } else if (data !== undefined) {
          this.edit(event, data);
        }
      }

    );

  }
  createEvent(segment: DayViewHourSegment) {

    const inputDialogConfig = new MatDialogConfig();

    inputDialogConfig.disableClose = true;
    inputDialogConfig.autoFocus = true;

    inputDialogConfig.data = {
      id: 1,
      name: 'Add an event',
      title: '',
      duration: 1,
    };

    // this.inputDialog.open(InputDialogComponent, inputDialogConfig);

    const dialogRef = this.inputDialog.open(UrnikDialogComponent, inputDialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data !== undefined) {
           this.addEvent(data, segment);
        }
      }

    );




  }

  private addEvent(data: any, segment: DayViewHourSegment) {
    const dragToSelectEvent: CalendarEvent = {
      title: data.title,
      start: segment.date,
      end: addHours(segment.date, data.duration),
      color: getColor(data.color),
      meta: {
        duration: data.duration,
      },
    };

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };
    // postaj isti event kot zgoraj na server in pridobi id od serverja (ne vem kako se bo pošiljala barva, bomo videli.
    // Datumi (start in end) so Date
    this.http.post(APIURL + '/urnik/', {
      title: data.title,
      start: segment.date.toString(),
      end: addHours(segment.date, data.duration).toString(),
      color: data.color,
      meta: {
        duration: data.duration,
      },
    }, httpOptions).subscribe((response: any) => {
      // Ko dobiš response OK ga dodaj v array z ustreznim id-jem od serverja.
      dragToSelectEvent.id = response._id;
      this.events = [...this.events, dragToSelectEvent];
      this.refresh();

    });
  }
  private edit(event: CalendarEvent<any>, data: any) {
    event.title = data.title;
    event.color = getColor(data.color);
    event.end = addHours(event.start, data.duration);
    event.meta.duration = data.duration;


    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };


    this.http.put(APIURL + '/urnik/' + event.id, {
      title: data.title,
      color: data.color,
      start: event.start.toString(),
      end: addHours(event.start, data.duration).toString(),
      meta: {
        duration: data.duration,
      },

    }, httpOptions).subscribe((response: any) => {
      this.events = this.events.filter(iEvent => iEvent !== event);
      this.events = [...this.events, event];
      this.refresh();

    });
  }

  private delete(event: CalendarEvent<any>) {

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.delete(APIURL + '/urnik/' + event.id, httpOptions).subscribe((response: any) => {
      this.events = this.events.filter(iEvent => iEvent !== event);
      this.refresh();
    });

  }
}

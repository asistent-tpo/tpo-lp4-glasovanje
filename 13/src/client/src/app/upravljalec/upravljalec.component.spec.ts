import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpravljalecComponent } from './upravljalec.component';

describe('UpravljalecComponent', () => {
  let component: UpravljalecComponent;
  let fixture: ComponentFixture<UpravljalecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpravljalecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpravljalecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

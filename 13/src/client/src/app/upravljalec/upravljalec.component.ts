import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {throwError as observableThrowError } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Dogodek} from '../dogodek';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {EventDialogComponent} from '../event-dialog/event-dialog.component';

const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';



@Component({
  selector: 'app-upravljalec',
  templateUrl: './upravljalec.component.html',
  styleUrls: ['./upravljalec.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UpravljalecComponent implements OnInit {

  constructor(private http: HttpClient, private addDialog: MatDialog) { }

  dogodki: Dogodek[] = [];

  displayedColumns: string[] = ['name', 'organizer', 'date'];
  expandedElement: Event | null;

  ngOnInit() {
    this.getDogodki();
  }

  getDogodki(): Dogodek[] {

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.get(APIURL + '/dogodki', httpOptions).pipe(catchError(this.errorHandler)).subscribe((response: any) => {
      // za vsak event v response naredim nov calendarEvent in ga dodam v this.events
      this.dogodki = response;
    });
    return this.dogodki;
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

  openAddDialog() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: 1,
      title: 'Add new event'
    };
    const dialogRef = this.addDialog.open(EventDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.addDogodek(result.name, result.organizer, result.description, result.date);
      }

    });

  }

  private addDogodek(n: any, org: string, desc: string, d: Date) {
    const noviDogodek: Dogodek = {
      id: 0,
      name: n,
      description: desc,
      organizer: org,
      date: d.toDateString(),
    };

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.post(APIURL + '/dogodki/', {
      name: noviDogodek.name,
      description: noviDogodek.description,
      organizer: noviDogodek.organizer,
      date: noviDogodek.date,
    }, httpOptions).subscribe((response: any) => {
      // Ko dobiš response OK ga dodaj v array z ustreznim id-jem od serverja.
      noviDogodek.id = response.id;
      this.dogodki = [...this.dogodki, noviDogodek];
    });
    // this.dogodki = [...this.dogodki, noviDogodek];
  }
}

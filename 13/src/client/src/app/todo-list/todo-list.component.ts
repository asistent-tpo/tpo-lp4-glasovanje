import { Component, OnInit } from '@angular/core';
import { Todo } from '../todo';
import { trigger, transition, style, animate } from '@angular/animations';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import {TodoService} from './todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
  animations: [
    trigger('fade', [

      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(30px)' }),
        animate(200, style({ opacity: 1, transform: 'translateY(0px)'}))
      ]),

      transition(':leave', [
        animate(200, style({ opacity: 0, transform: 'translateY(30px)' }))
      ]),
    ])
  ]
})
export class TodoListComponent implements OnInit {
  todoTitle: string;
  constructor(public dialog: MatDialog, public todoService: TodoService) { }

  ngOnInit() {
    this.todoService.getTodos();
  }
  addTodo(): void {
    if (this.todoTitle.trim().length === 0) {
      return;
    }
    this.todoService.addTodo(this.todoTitle);

    this.todoTitle = '';
    // this.idForTodo++;
  }

  openModal(id: number, name: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: 1,
      title: 'Delete',
      content: 'Are you sure you want to delete ' + name,
    };
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
     // console.log(result);
      if (result) {
        this.todoService.deleteTodo(id);
      }

    });
  }

}

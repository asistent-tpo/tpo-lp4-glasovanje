import {Injectable} from '@angular/core';
import {Todo} from '../todo';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError as observableThrowError } from 'rxjs';

const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';





@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoTitle = '';
  idForTodo = 4;
  beforeEditCache = '';
  filter = 'all';
  anyRemainingModel = true;
  todos: Todo[] = [];



  constructor(private http: HttpClient) { }


  getTodos(): Todo[] {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.todos = [];
    this.http.get(APIURL + '/todo', httpOptions).pipe(catchError(this.errorHandler)).subscribe((response: any) => {
      for (const t of response) {
        t.id = t._id;
        this.todos = [...this.todos, t];
      }
    });
    return this.todos;
  }
  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

  addTodo(todoTitle: string): void {
    if (todoTitle.trim().length === 0) {
      return;
    }

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.post(APIURL + '/todo/', {
      title: todoTitle,
    }, httpOptions).subscribe((response: any) => {
      this.todos.push({
        id: response._id,
        title: todoTitle,
        completed: false,
        editing: false
      });

    });

    this.idForTodo++;
  }

  editTodo(todo: Todo): void {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.beforeEditCache = todo.title;
    todo.editing = true;
    this.http.put(APIURL + '/todo/' + todo.id, {
      title: todo.title,
    }, httpOptions).subscribe((response: any) => {

    });
  }

  doneEdit(todo: Todo): void {
    if (todo.title.trim().length === 0) {
      todo.title = this.beforeEditCache;
    }

    this.anyRemainingModel = this.anyRemaining();
    todo.editing = false;
  }

  cancelEdit(todo: Todo): void {
    todo.title = this.beforeEditCache;
    todo.editing = false;
  }

  deleteTodo(id: number): void {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.delete(APIURL + '/todo/' + id, httpOptions).subscribe((response: any) => {
      this.todos = this.todos.filter(todo => todo.id !== id);
    });
  }

  remaining(): number {
    return this.todos.filter(todo => !todo.completed).length;
  }

  atLeastOneCompleted(): boolean {
    return this.todos.filter(todo => todo.completed).length > 0;
  }

  clearCompleted(): void {
    this.todos = this.todos.filter(todo => !todo.completed);
  }

  checkAllTodos(): void {
    this.todos.forEach(todo => todo.completed = (event.target as HTMLInputElement).checked);
    this.anyRemainingModel = this.anyRemaining();
  }

  anyRemaining(): boolean {
    return this.remaining() !== 0;
  }

  todosFiltered(): Todo[] {
    if (this.filter === 'all') {
      return this.todos;
    } else if (this.filter === 'active') {
      return this.todos.filter(todo => !todo.completed);
    } else if (this.filter === 'completed') {
      return this.todos.filter(todo => todo.completed);
    }

    return this.todos;
  }
}

import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-input-dialog',
  templateUrl: 'input-dialog.component.html',
  styleUrls: ['input-dialog.component.css']
})
export class InputDialogComponent implements OnInit {

  form: FormGroup;
  name: string;
  description: string;
  duration: number;
  start: number;
  date: Date;
  title: string;


  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.date = data.date;
    this.title = data.title;
    this.duration = data.duration;
    this.description = data.description;
    this.name = data.name;
    this.start = data.start;
  }

  ngOnInit() {
    this.form = this.fb.group({
      title: [this.title, [Validators.required, Validators.minLength(1)]],
      description: [this.description],
      start: [this.start, [Validators.max(23)]],
      duration: [this.duration, [Validators.required, Validators.max(23)]],
      date: [this.date, [Validators.required]],


    });
  }

  save() {
     this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

  hasError(hours: number) {
    if (hours > 23) { return true; }
  }

}

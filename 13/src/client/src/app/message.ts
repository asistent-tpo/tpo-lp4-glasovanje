export interface Message {
  id: number;
  date: string;
  content: string;
}

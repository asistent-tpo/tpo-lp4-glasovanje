import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { PasswordValidator } from './shared/password.validator';
import { SpreminjanjeGeslaService } from './spreminjanje-gesla.service';

@Component({
  selector: 'app-spreminjanje-gesla',
  templateUrl: './spreminjanje-gesla.component.html',
  styleUrls: ['./spreminjanje-gesla.component.css']
})
export class SpreminjanjeGeslaComponent implements OnInit {

  replaceForm: FormGroup;
  confirmPassword: any;
  constructor(private fb: FormBuilder, private replaceService: SpreminjanjeGeslaService) { }

  ngOnInit() {
    this.replaceForm = this.fb.group({
      geslo: ['', [Validators.required, Validators.minLength(8)]],
      novoGeslo: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['']},
      { validator: PasswordValidator });
  }

  get email() {
    return this.replaceForm.get('email');
  }

  get novoGeslo() {
    return this.replaceForm.get('novoGeslo');
  }

  get geslo() {
    return this.replaceForm.get('geslo');
  }

  onSubmit() {
    this.replaceService.spremeni(this.replaceForm.value)
      .subscribe(
        response => {
          window.alert("Password replacement was successful!");
        },
        error => {
          window.alert("Wrong password. Please try again.");
        }
      );
  }

}

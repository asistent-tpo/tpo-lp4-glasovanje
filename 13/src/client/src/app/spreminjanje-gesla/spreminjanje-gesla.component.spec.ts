import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpreminjanjeGeslaComponent } from './spreminjanje-gesla.component';

describe('SpreminjanjeGeslaComponent', () => {
  let component: SpreminjanjeGeslaComponent;
  let fixture: ComponentFixture<SpreminjanjeGeslaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpreminjanjeGeslaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpreminjanjeGeslaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { SpreminjanjeGeslaService } from './spreminjanje-gesla.service';

describe('SpreminjanjeGeslaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpreminjanjeGeslaService = TestBed.get(SpreminjanjeGeslaService);
    expect(service).toBeTruthy();
  });
});

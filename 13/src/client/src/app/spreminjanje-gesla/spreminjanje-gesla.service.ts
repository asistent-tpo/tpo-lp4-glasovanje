import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {throwError as observableThrowError } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class SpreminjanjeGeslaService {

  url = 'https://lp4-tpo-backend.herokuapp.com/api/v1/sprememba';

  constructor(private http: HttpClient) { }

  spremeni(userData) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };
    return this.http.post<any>(this.url, userData, httpOptions);
  }


  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

}

import {Component, OnInit} from '@angular/core';
import {Message} from '../message';
import {catchError} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {throwError as observableThrowError} from 'rxjs';


const APIURL =  'https://lp4-tpo-backend.herokuapp.com/api/v1';



@Component({
  selector: 'app-system-messages',
  templateUrl: './system-messages.component.html',
  styleUrls: ['./system-messages.component.css']
})
export class SystemMessagesComponent implements OnInit {

  columns = ['date', 'content'];
  messages: Message[] = [];
  mess: Message[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getMessages();
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

  getMessages(): Message[] {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.messages = [];
    const d = new Date();
    this.http.get(APIURL + '/messages', httpOptions).pipe(catchError(this.errorHandler)).subscribe((response: any) => {
      // za vsak event v response naredim nov calendarEvent in ga dodam v this.events
      for (const r of response) {
        r.date = new Date(r.date);
        if (d.toDateString() === r.date.toDateString()) {
          this.messages = [...this.messages, r];
        }
      }

    });
    return this.messages;
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaygroundComponent } from './playground/playground.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatDialogModule,
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule, MatTableModule, MatCardModule
} from '@angular/material';
import {SideNavComponent} from './side-nav/side-nav.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import { CalendarComponent } from './calendar/calendar.component';
import {TodoListComponent} from './todo-list/todo-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DialogComponent } from './dialog/dialog.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { InputDialogComponent } from './input-dialog/input-dialog.component';
import {UrnikComponent} from './urnik/urnik.component';
import {UrnikDialogComponent} from './urnik-dialog/urnik-dialog.component';
import {TodoService} from './todo-list/todo.service';
import {HttpClientModule} from '@angular/common/http';
import { EventsComponent } from './events/events.component';
import { BusComponent } from './bus/bus.component';
import { FoodComponent } from './food/food.component';
import { HomeComponent } from './home/home.component';
import { UpravljalecComponent } from './upravljalec/upravljalec.component';
import { AdminComponent } from './admin/admin.component';
import { EventDialogComponent } from './event-dialog/event-dialog.component';
import { AdminDialogComponent } from './admin-dialog/admin-dialog.component';
import { RegistrationService } from './registration/registration.service';
import { LoginService } from './login/login.service';
import {AuthService} from './auth.service';
import {AuthGuardService} from './auth-guard.service';
import {JWT_OPTIONS, JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {SpreminjanjeGeslaComponent} from './spreminjanje-gesla/spreminjanje-gesla.component';
import {SpreminjanjeGeslaService} from './spreminjanje-gesla/spreminjanje-gesla.service';
import {AgmCoreModule} from '@agm/core';
import {MapsService} from './food/maps.service';
import { SystemMessagesComponent } from './system-messages/system-messages.component';
@NgModule({
  declarations: [
    AppComponent,
    PlaygroundComponent,
    SideNavComponent,
    CalendarComponent,
    TodoListComponent,
    DialogComponent,
    InputDialogComponent,
    UrnikComponent,
    UrnikDialogComponent,
    EventsComponent,
    BusComponent,
    FoodComponent,
    HomeComponent,
    LoginComponent,
    RegistrationComponent,
    UpravljalecComponent,
    AdminComponent,
    EventDialogComponent,
    AdminDialogComponent,
    SpreminjanjeGeslaComponent,
    SystemMessagesComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatInputModule,
    MatMenuModule,
    FlexLayoutModule,
    MatNativeDateModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    HttpClientModule,
    MatTableModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyATZ6538m-X6fXpomQQJEKfJS2BA5VrSUQ'
    }),
    MatCardModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter
      }
    })
  ],
  exports: [UrnikComponent],
  providers: [TodoService, RegistrationService, JwtHelperService, LoginService, AuthService,
    AuthGuardService, SpreminjanjeGeslaService, MapsService],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent, InputDialogComponent, UrnikDialogComponent, EventDialogComponent,
  AdminDialogComponent]
})
export class AppModule { }

export function jwtTokenGetter() {
  return localStorage.getItem('zeton');
}

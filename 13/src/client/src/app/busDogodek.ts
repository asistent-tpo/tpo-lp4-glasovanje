export interface busDogodek {
    eta: number;
    local_timestamp: string;
    route_int_id: number;
    route_name: string;
    route_number: number;
    station_int_id: number;
    utc_timestamp: string;
    validity: number;
    vehicle_int_id: number;
  }
  
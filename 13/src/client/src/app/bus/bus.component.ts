import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {busDogodek} from '../busDogodek';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BusComponent implements OnInit {

  url = 'https://cors-anywhere.herokuapp.com/http://194.33.12.24/timetables/liveBusArrival';

  constructor(private fb: FormBuilder, private http: HttpClient) { }
  busForm: FormGroup;

  dogodki: busDogodek[] = [];

  displayedColumns: string[] = ['route_number','route_name', 'eta'];
  expandedElement: Event | null;

  ngOnInit() {
    this.busForm = this.fb.group({
      idPostaje: ['1934', [Validators.required, Validators.maxLength(4), Validators.minLength(4)]]});
  }


  onSubmit() {
    let params = new HttpParams();
    params = params.append('station_int_id', this.busForm.value.idPostaje);

    this.http.get<any>(this.url, {params: params}).subscribe(response => {
      
      this.dogodki = response.data;
    });
    return this.dogodki;
  }

}

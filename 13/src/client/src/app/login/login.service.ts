import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class LoginService {
  url = 'http://localhost:3000/api/v1/prijava/';

  constructor(private http: HttpClient) { }

  login(userData) {

    if (!isDevMode()) {
      this.url = 'https://lp4-tpo-backend.herokuapp.com/api/v1/prijava/';
    }

    return this.http.post<any>(this.url, userData);
  }

}

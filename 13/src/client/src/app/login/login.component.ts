import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { LoginService } from './login.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import {Router} from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private fb: FormBuilder, private registrationService: LoginService, private helper: JwtHelperService,
              private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      geslo: ['', [Validators.required, Validators.minLength(8)]]});

    if (localStorage.getItem('zeton') != null) {
      if (localStorage.getItem('role') === 'uporabnik') {
        this.router.navigateByUrl('/home');
      } else if (localStorage.getItem('role') === 'administrator') {
        this.router.navigateByUrl('/admin');
      } else if (localStorage.getItem('role') === 'upravljalec') {
        this.router.navigateByUrl('/upravljalec');
      }
    } else {
      localStorage.removeItem('role');
    }
  }

  get email() {
    return this.loginForm.get('email');
  }

  get geslo() {
    return this.loginForm.get('geslo');
  }

  guest() {
    localStorage.setItem('role', 'guest');
    this.router.navigateByUrl('/food');
  }

  onSubmit() {
    this.registrationService.login(this.loginForm.value)
      .subscribe(
        response => {
          localStorage.setItem('zeton', response.zeton);
          const decodedToken =  this.helper.decodeToken(response.zeton);
          localStorage.setItem('role', decodedToken.role);
          localStorage.setItem('email', decodedToken.email);

          if (decodedToken.role === 'uporabnik') {
            this.router.navigateByUrl('/home');
          } else if (decodedToken.role === 'administrator') {
            this.router.navigateByUrl('/admin');
          } else if (decodedToken.role === 'upravljalec') {
            this.router.navigateByUrl('/upravljalec');
          }
        },
        error => window.alert("Error! Wrong email or password. Please try again.")
      );
  }

}

import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router';




@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent {


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router) {}

  logout() {
    localStorage.removeItem('zeton');
    localStorage.removeItem('role');
    localStorage.removeItem('email');
    this.router.navigateByUrl('/');
  }

  tokenExists(): boolean {
    return (localStorage.getItem('zeton') !== null);
  }

  isAdmin(): boolean {
    return (localStorage.getItem('role') === 'administrator');
  }

  isRegisteredUser(): boolean {
    return (localStorage.getItem('role') === 'uporabnik');
  }
  isUpravljalec(): boolean {
      return (localStorage.getItem('role') === 'upravljalec');
  }

  isGuest(): boolean {
    return (localStorage.getItem('role') === 'guest');
  }
  getUsername(): string {
    return localStorage.getItem('email');
  }
}



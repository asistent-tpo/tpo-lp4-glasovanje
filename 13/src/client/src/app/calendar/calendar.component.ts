import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, OnInit
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours, subHours
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import {DialogComponent} from '../dialog/dialog.component';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {InputDialogComponent} from '../input-dialog/input-dialog.component';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError as observableThrowError } from 'rxjs';


const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';




const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#65ff31',
    secondary: '#D1E8FF'

  },
};

// merganje

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['calendar.component.css'],
  templateUrl: 'calendar.component.html'
})
export class CalendarComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();


  actions: CalendarEventAction[] = [
    {
      label: '<i class="material-icons">\n' +
        'border_color\n' +
        '</i> ',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="material-icons">\n' +
        'delete_forever\n' +
        '</i> ',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
  ];

  activeDayIsOpen = true;

  constructor(public deleteDialog: MatDialog, public inputDialog: MatDialog, private http: HttpClient) {}

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = true;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  ngOnInit(): void {
    this.getEvents();
  }

  getEvents(): CalendarEvent[] {

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.get(APIURL + '/calendar', httpOptions).pipe(catchError(this.errorHandler)).subscribe((response: any) => {
      // za vsak event v response naredim nov calendarEvent in ga dodam v this.events
      this.events = [];
      for (const g of response) {
        g.id = g._id;
        g.start = new Date(g.start);
        g.end = new Date(g.end);
        g.color = colors.red;
        g.actions = this.actions;
        this.events = [...this.events, g];
      }
      this.refresh.next();
    });
    return this.events;
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

  openInputDialog() {
    const inputDialogConfig = new MatDialogConfig();

    inputDialogConfig.disableClose = true;
    inputDialogConfig.autoFocus = true;

    inputDialogConfig.data = {
      id: 1,
      name: 'Add an event',
      title: '',
      date: this.viewDate,
    };

    // this.inputDialog.open(InputDialogComponent, inputDialogConfig);

    const dialogRef = this.inputDialog.open(InputDialogComponent, inputDialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data !== undefined) {
          this.addEvent(data.title, startOfDay(data.date), data.duration, data.description, data.start);
        }
      }

    );
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd
                    }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    if (action === 'Deleted') {
      this.deleteModal(event);
    }
    if (action === 'Edited' || action === 'Clicked') {
      this.openEditDialog(event);
    }
  }

  addEvent(t: string, d: Date, trajanje: number, desc: string, startHour: number): void {
    const startTime = addHours(d, startHour);
    // podobno kot pri urniku

    const newEvent: CalendarEvent = {
      title: t,
      start: startTime,
      end: addHours(startTime, trajanje),
      color: colors.red,
      meta: {
        description: desc,
        duration: trajanje,
      },

    };

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.post(APIURL + '/calendar/', {
      title: newEvent.title,
      start: newEvent.start.toString(),
      end: newEvent.end.toString(),
      color: 'rdeca',
      meta: {
        description: newEvent.meta.desription,
        duration: newEvent.meta.duration,
      },
    }, httpOptions).subscribe((response: any) => {
      // Ko dobiš response OK ga dodaj v array z ustreznim id-jem od serverja.
      newEvent.id = response._id;
      newEvent.actions = this.actions;
      this.events = [...this.events, newEvent];
      this.refresh.next();

    });
  }

  deleteEvent(eventToDelete: CalendarEvent) {

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.delete(APIURL + '/calendar/' + eventToDelete.id, httpOptions).subscribe((response: any) => {
      this.events = this.events.filter(iEvent => iEvent !== eventToDelete);
      this.refresh.next();
    });

  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

 deleteModal(event: CalendarEvent) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: 1,
      title: 'Delete',
      content: 'Are you sure you want to delete' + event.title,
    };
    const dialogRef = this.deleteDialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.deleteEvent(event);
      }

    });
  }

  private openEditDialog(event: CalendarEvent) {

    const editDialogConfig = new MatDialogConfig();

    editDialogConfig.disableClose = true;
    editDialogConfig.autoFocus = true;

    editDialogConfig.data = {
      id: 1,
      name: 'Edit evnet ' + event.title,
      title: event.title,
      date: event.start,
      start: event.start.getHours(),
      duration: event.meta.duration,
      description: event.meta.description,


    };

    // this.inputDialog.open(InputDialogComponent, inputDialogConfig);

    const dialogRef = this.inputDialog.open(InputDialogComponent, editDialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data !== undefined) {
          this.editEvent(event, data);
        }
      }

    );

  }

  private editEvent(event: CalendarEvent, data: any) {
    event.title = data.title;
    event.end = addHours(event.start, data.duration);
    event.meta.duration = data.duration;
    event.meta.description = data.description;

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };


    this.http.put(APIURL + '/calendar/' + event.id, {
      title: event.title,
      start: event.start,
      end: event.end,
      color: 'rdeca',
      meta: {
        description: event.meta.duration,
        duration: event.meta.description,
      },

    }, httpOptions).subscribe((response: any) => {
      this.events = this.events.filter(iEvent => iEvent !== event);
      this.events = [...this.events, event];
      this.refresh.next();

    });

  }
}



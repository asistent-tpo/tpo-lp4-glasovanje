import { Component, OnInit, isDevMode } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {throwError as observableThrowError } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Dogodek} from '../dogodek';

const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';



@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EventsComponent implements OnInit {

  constructor(private http: HttpClient) { }

  dogodki: Dogodek[] = [];


  displayedColumns: string[] = ['name', 'organizer', 'date'];
  expandedElement: Event | null;

  ngOnInit() {
    this.getDogodki();
  }

  getDogodki(): Dogodek[] {

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.get(APIURL + '/dogodki', httpOptions).pipe(catchError(this.errorHandler)).subscribe((response: any) => {
      // za vsak event v response naredim nov calendarEvent in ga dodam v this.events
      this.dogodki = response;
    });
    return this.dogodki;
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Napaka!');
  }

}

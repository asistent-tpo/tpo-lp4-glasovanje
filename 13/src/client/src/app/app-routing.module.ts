import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EventsComponent} from './events/events.component';
import {BusComponent} from './bus/bus.component';
import {FoodComponent} from './food/food.component';
import {HomeComponent} from './home/home.component';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {UpravljalecComponent} from './upravljalec/upravljalec.component';
import {AdminComponent} from './admin/admin.component';
import {AuthGuardService} from './auth-guard.service';
import {SpreminjanjeGeslaComponent} from './spreminjanje-gesla/spreminjanje-gesla.component';
import {SystemMessagesComponent} from './system-messages/system-messages.component';

const routes: Routes = [ { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'events', component: EventsComponent, canActivate: [AuthGuardService] },
  { path: 'food', component: FoodComponent, canActivate: [AuthGuardService] },
  { path: 'bus', component: BusComponent, canActivate: [AuthGuardService] },   { path: '', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'upravljalec', component: UpravljalecComponent, canActivate: [AuthGuardService] },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuardService]},
  { path: 'replace', component: SpreminjanjeGeslaComponent, canActivate: [AuthGuardService]},
  { path: 'systemmessages', component: SystemMessagesComponent, canActivate: [AuthGuardService]},
  { path: '**', redirectTo: '' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }



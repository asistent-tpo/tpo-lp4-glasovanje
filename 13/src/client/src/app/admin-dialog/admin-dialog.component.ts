import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-admin-dialog',
  templateUrl: 'admin-dialog.component.html',
  styleUrls: ['admin-dialog.component.css']
})
export class AdminDialogComponent implements OnInit {

  form: FormGroup;
  title: string;
  content: string;


  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.content = data.description;
    this.title = data.title;
  }

  ngOnInit() {
    this.form = this.fb.group({
      content: [this.content, [Validators.required, Validators.minLength(1)]],
    });
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }


}

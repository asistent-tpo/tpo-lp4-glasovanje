import {Component, OnInit} from '@angular/core';
import {MapsService} from './maps.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {DialogComponent} from '../dialog/dialog.component';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';

interface Restaurant {
  id: number;
  name: string;
  lat: number;
  long: number;
  price: string;
  address: string;
  distance: number;

}




@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  lat = 46.05;
  lng = 14.5;
  haveLocation = false;
  restaurants: Restaurant[] = [];
  rest: Restaurant[] = [];
  iconUrl = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
  columns = ['NAME', 'ADDRESS', 'PRICE', 'DISTANCE'];

  constructor(private map: MapsService, private dialog: MatDialog, private http: HttpClient) { }

  ngOnInit() {
    this.getRestavracije();

  }

  getRestavracije() {

    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.restaurants = [];
    this.http.get(APIURL + '/restavracije', httpOptions).subscribe((response: Restaurant[]) => {
      this.restaurants = response;
      console.log(this.restaurants);
      for (const r of this.restaurants) {
        r.distance = 0;
      }
      this.openModal();
    });
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: 0,
      title: 'Location Request',
      content: 'The website needs your location to provide better content.',
    };
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
      if (result) {
        this.map.getLocation().subscribe(data => {
         // console.log(data);
          this.lat = data.latitude;
          this.lng = data.longitude;
          this.haveLocation = true;
          this.sortRestaurants();
      });
      } else {
        this.sortRestaurants();
      }

    });
  }
  sortRestaurants() {
    if (this.haveLocation) {
      for (const r of this.restaurants) {
        r.distance = this.distance(this.lat, this.lng, r.lat, r.long);
      }
      this.restaurants.sort((a, b) => a.distance - b.distance);
      this.rest = this.restaurants;
    } else {
      this.restaurants.sort();
      this.rest = this.restaurants;

    }
  }


  distance(lat1: number, lon1: number, lat2: number, lon2: number) {
    const R = 6371;
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d;
  }
  round(n: number): string {
    if (n === 0) { return 'N/A'; }
    return '' + Math.round(n);
  }

}

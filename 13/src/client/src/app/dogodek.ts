export interface Dogodek {
  id: number;
  name: string;
  organizer: string;
  date: string;
  description: string;
}

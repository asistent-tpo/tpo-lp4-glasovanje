import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-input-dialog',
  templateUrl: 'urnik-dialog.component.html',
  styleUrls: ['urnik-dialog.component.css']
})
export class UrnikDialogComponent implements OnInit {

  form: FormGroup;
  name: string;
  duration: number;
  title: string;
  color: string;


  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UrnikDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.title = data.title;
    this.duration = data.duration;
    this.name = data.name;
    this.color = data.color;
  }

  ngOnInit() {
    this.form = this.fb.group({
      title: [this.title, [Validators.required, Validators.minLength(1)]],
      color: [this.color, [Validators.required]],
      duration: [this.duration, [Validators.required, Validators.min(1)]],


    });
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }
  delete() {
    this.dialogRef.close('delete');
  }

}

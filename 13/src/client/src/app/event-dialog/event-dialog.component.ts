import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-event-dialog',
  templateUrl: 'event-dialog.component.html',
  styleUrls: ['event-dialog.component.css']
})
export class EventDialogComponent implements OnInit {

  form: FormGroup;
  title: string;
  description: string;
  date: Date;
  name: string;
  organizer: string;


  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<EventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.date = data.date;
    this.description = data.description;
    this.name = data.name;
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: [this.name, [Validators.required, Validators.minLength(1)]],
      organizer: [this.organizer, [Validators.required, Validators.minLength(1)]],
      description: [this.description],
      date: [this.date, [Validators.required]],


    });
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }


}

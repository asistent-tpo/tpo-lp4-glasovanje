import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {AdminDialogComponent} from '../admin-dialog/admin-dialog.component';
import {Message} from '../message';
import {catchError} from 'rxjs/operators';
import {throwError as observableThrowError} from 'rxjs';
import {Router} from '@angular/router';

const APIURL = 'https://lp4-tpo-backend.herokuapp.com/api/v1';




@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private messageDialog: MatDialog, private http: HttpClient, private router: Router) { }

  messages: Message[] = [];


  ngOnInit() {
    if (localStorage.getItem('role') !== 'administrator') {
      this.router.navigateByUrl('/');
    }
  }


  openMessageDialog() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: 1,
      title: 'Add new system message'
    };
    const dialogRef = this.messageDialog.open(AdminDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.addMessage(result.content);
      }

    });


  }

  private addMessage(c: string) {
    const newM = {
      id: 0,
      content: c,
      date: new Date().toString(),
    };
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('zeton'),

    });
    const httpOptions = {
      headers: httpHeader,
    };

    this.http.post(APIURL + '/messages/', {
      content: newM.content,
      date: newM.date.toString(),
    }, httpOptions).subscribe((response: any) => {
      // Ko dobiš response OK ga dodaj v array z ustreznim id-jem od serverja.
      newM.id = response._id;
      this.messages = [...this.messages, newM];
    });
    // this.messages = [...this.messages, newM];
  }

}

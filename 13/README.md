# 13. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Bitbucket uveljavitev: https://bitbucket.org/tomgornik/tpo-lp4/commits/518dcc28b5a4ff3cca8a3ff862d0b5258d244fd4

Delujoča spletna stran: https://lp4-tpo.herokuapp.com

Testni registrirani uporabnik:

Email: ivanakobilica@gmail.com, Geslo: mojeGeslo, Uporabniška vloga: Registriran uporabnik

Ostali vnaprej pripravljeni uporabniki so navedeni v README projekta.

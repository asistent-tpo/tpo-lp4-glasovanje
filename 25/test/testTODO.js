var app = require('../app.js');
var request = require('supertest');

const assert = require('chai').assert;
var id;

//osnovni tok
describe('TODO', function() {
    it('Odpre se obrazec za dodajanje vnosa v todo seznam', function(done) {
        request(app).get('/').expect(200, done);
    });
    
    //DODAJANJE VNOSOV V TODO
        
    //osnovni tok (ni izjemnih ali alternativnih)

        
    
    it('Uspešno dodan vnos v TODO seznam', function(done) {
             let data = {
                 "email" : "sara@gmail.com",
                 "opis" : "1. Naredi domačo nalogo  2.Pojdi v trgovino"
            };
            request(app)
            .post('/api/vnosTODO')
            .send(data)
            //.set('Accept', 'application/json')
            //.expect('Content-Type', /json/)
            .expect(201)
            .end(function(err, res){
                if(err) return done(err);
                id = res.body._id;
            });
            done();
    });
    

    
    //BRISANJE VNOSA IZ SEZNAMA TODO - izjemni tok
    it('Neuspešno izbrisan vnos iz seznama TODO', function(done) {
        request(app)
        .get('/')
        .expect(200, done)
        .end(function(err, res){
            if(err) return done(err);
            //else assert.exists(id);
        });
        done();
    });
    
    

    //SPREMINJANJE VNOSOV V SEZNAMU TODO
    //osnovni tok (alternativnih oz. izjemnih ni)
    
    
    /*
    it('Uspešno urejen vnos v seznamu TODO', function(done) {
        //this.retries(3);
             let data = {
                 "email" : "sara@gmail.com",
                 "opis" : "spremenjen opis: 1. Kupi kruh 3. Pospravi sobo"
            };
            request(app)
            .put('/api/todo/' + id)
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res){
                if(err) return done(err)
                else {
                    assert.equal(res.body.opis, data.opis);
                }
            });
            done();
    });
*/
    
    //BRISANJE VNOSA IZ SEZNAMA TODO
    /*
    //osnovni tok
    it('Uspešno izbrisan vnos iz TODO seznama', function(done) {
        //console.log("trenutni id : " + id);
        this.retries(3);
        setTimeout(function()  {
        request(app)
        .delete('/api/todo/' + id)
        .expect(204, done)
        }, 1000);
    }); 
    
    */
});


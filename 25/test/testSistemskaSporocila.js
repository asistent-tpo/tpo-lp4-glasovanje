var app = require('../app.js');
var request = require('supertest');
var expect = require('chai').expect;

const assert = require('chai').assert;

var id = null;

describe('OBVEŠČANJE UPORABNIKOV O STANJU APLIKACIJE', function() {
   it('Odpri stran za ogled sistemskih sporočil o stanju apliakcije', function(done) {
      request(app).get('/obvestila/')
      .expect(200, done);
   });
   
   //izjemni tok
    it('ne potrdi dodajanje sistemskega sporocila', function(done) {
         let data = {
           "sporocilo" : "Testno sporocilo"
       }
        request(app)
        .post('/')
        .send(data)
        .expect(200)
        .end(function(err, res){
            if(err) return done(err);
            else {
                id = res.body._id;
                assert.notExists(id);
            }
        });
        done();
    });
  
    //osnovni tok   
   it('uspešno dodaj sistemsko sporocilo', function(done) {
       let data = {
           "sporocilo" : "testno sporocilo"
       }
        request(app)
        .post('/api/vnosObvestila')
        .send(data)
        .expect(201)
        .end(function(err, res){
            if(err) return done(err)
            else 
                assert.equal(res.body.ime, data.ime);
        });
        done();
    });
  
    

  
});
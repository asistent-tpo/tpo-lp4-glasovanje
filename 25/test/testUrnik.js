var app = require('../app.js');
var request = require('supertest');

const assert = require('chai').assert;
var id;

//osnovni tok
describe('URNIK', function() {
    it('Odpre se obrazec za dodajanje vnosa v urnik', function(done) {
        request(app).get('/').expect(200, done);
    });
    
    //DODAJANJE VNOSA V URNIK
    
    //osnovni tok (izjemnih oz alternativnih ni)
    it('Uspešno dodan vnos v urnik', function(done) {
             let data = {
                 "email" : "sara@gmail.com",
                 "Name": "Poskusni vnos v urnik - Sara",
                 "CourseDuration" : "4",
                 "Colour" : "red"
            };
            request(app)
            .post('/api/vnosUrnik')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .end(function(err, res){
                if(err) return done(err)
                id = res.body._id;

            });
            done();
    });
    
    //urnik part1
    it('Neuspešno izbrisan vnos iz seznama TODO', function(done) {
        request(app)
        .post('/')
        .expect(200, done)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
    });

    
    //UREJANJE VNOSA V URNIKU
    /*
    //osnovni tok (alternativnih oz. izjemnih ni)
    it('Uspešno urejen vnos v urniku', function(done) {
             let data = {
                 "email" : "sara@gmail.com",
                 "Name": "Spremenjen vnos v urnik - Sara",
                 "CourseDuration" : "4",
                 "Colour" : "blue"
            };
            request(app)
            .put('/api/urnik/' + id)
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res){
                if(err) return done(err)
                else {
                    assert.equal(res.body.ime, data.Name);
                    assert.equal(res.body.barva, data.Colour);
                }
            });
            done();
    });
    */
    /*
    //BRISANJE VNOSA IZ URNIKA
    //osnovni tok
    it('Uspešno izbrisan vnos iz urnika', function(done) {
        setTimeout(function()  {
        request(app)
        .delete('/api/urnik/' + id)
        .expect(204, done);
        }, 1000);
    });
    */
});




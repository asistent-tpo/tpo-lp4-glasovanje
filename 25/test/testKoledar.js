var app = require('../app.js');
var request = require('supertest');

const assert = require('chai').assert;

//osnovni tok
describe('KOLEDAR', function() {
    var id = null;

    it('Odpre se obrazec za dodajanje vnosa v koledar', function(done) {
        request(app).get('/').expect(200, done);
    });
    
    //DODAJANJE VNOSA V KOLEDAR
    
    //izjemni tok 
    it('Neuspešno dodan vnos v koledar', function(done) {
             let data = {
                 "email" : "sara@gmail.com",
                 "Name": "Poskusni vnos v koledar",
                 "Date" : "2018-07-22T00:00:00.000Z",
                 "Duration" : "2",
                 "Description" : "neki neki"
            };
            request(app)
            .post('/')
            .send(data)
            .expect(200)
            .end(function(err, res){
                if(err) return done(err);
                /*var NovId = res.body.find(function(koledarVnos){
                    NovId = koledarVnos._id;
                    id = NovId;
                });*/
                else {
                assert.notExists(res.body._id);
                }
                //console.log("nov id koledar" + id);
            });
            done();
    });
    
    //osnovni tok za dodajanje vnosa

        
    it('Uspešno dodan vnos v koledar', function(done) {
             let data = {
                 "email" : "sara@gmail.com",
                 "Name": "Poskusni vnos v koledar",
                 "Date" : "2018-07-22T00:00:00.000Z",
                 "Duration" : "2",
                 "Description" : "neki neki"
            };
            request(app)
            .post('/api/vnosKoledar')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .end(function(err, res){
                if(err) {
                    console.log("ERR --->")
                    return done(err);
                } else {
                    id = res.body._id;
                    console.log("BODY ID --->" + res.body._id)
                }
            });
            done();
    });

    //UREJANJE VNOSA V KOLEDARJU
    
    //izjemni tok - urejanje vnosa 
    it('Neuspešno urejanje vnosa v koledarju - part 1', function(done) {
        let data = {
            "email" : "sara@gmail.com",
            "Name": "Spremenjen vnos",
            "Date" : "2018-07-22T00:00:00.000Z",
            "Duration" : "2",
            "Description" : "spremenjen opis"
        };
        request(app)
        .get('/')
        .expect(200)
        .end(function(err, res){
        if(err) return done(err)
        });
        done();
    });
    
    /*
    it('Neuspešno urejanje vnosa v koledarju - part 2', function(done) {
        console.log("Neuspešno urejanje vnosa v koledarju - part 2 " + id)
        //this.retries(3);
        let data = {
            "email" : "sara@gmail.com",
            "Name": "Spremenjen vnos",
            "Date" : "2018-07-22T00:00:00.000Z",
            "Duration" : "2",
            "Description" : "spremenjen opis"
        };
        request(app)
        .get('/api/koledar/' + id)
        .expect(200)
        .end(function(err, res){
            if(err) return done(err)
            else assert.notEqual(res.body.Name, data.Name);
        });
        done();
    });*/
    
    /*
    //osnovni tok 
    it('Uspešno urejanje vnosa v koledarju', function(done) {
        console.log("Uspešno urejanje vnosa v koledarju " + id)
        //this.retries(3);
        let data = {
            "email" : "sara@gmail.com",
            "Name": "Spremenjeno ime",
            "Date" : "2018-07-22T00:00:00.000Z",
            "Duration" : "2",
            "Description" : "spremenjen opis"
        };
        request(app)
        .put('/api/koledar/' + id)
        .send(data)
        .expect(200)
        .end(function(err, res){
            if(err) return done(err)
            else {
                assert.equal(res.body.ime, data.Name);
                assert.equal(res.body.opis, data.Description);
            }
        });
        done();
    });
    */
    
    
    // BRISANJE VNOSA IZ KOLEDARJA 
    
     //izjemni tok za brisanje vnosa
    it('Neuspešno izbrisan vnos iz koledarja', function(done) {
        //this.retries(3);
        request(app)
        .get('/')
        .expect(200)
        .end(function(err, res){
            if(err) return done(err);
            //else assert.exists(id);
        });
        done();
    });
    
    
    /*
    //osnovni tok za brisanje vnosa 
    it('Uspešno izbrisan vnos iz koledarja', function(done) {
        console.log("Uspešno izbrisan vnos iz koledarja " + id)
        //this.retries(3);
        request(app)
        .delete('/api/koledar/' + id)
        .expect(204)
        .end(function(err, res){
            if(err) return done(err);
            //else assert.exists(id);
        });
        done();
    });
    */
    
    
});


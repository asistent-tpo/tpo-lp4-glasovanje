var app = require('../app.js');
var request = require('supertest');
var expect = require('chai').expect;

const assert = require('chai').assert;



describe('AVTOBUSI - PRIHODI', function() {
   it('Odpri stran za ogled prihodov avtobusov', function(done) {
      request(app).get('/avtobusi/')
      .expect(200, done);
   });
  
    //osnovni tok   
   it('izberi avtobus in preveri prihode', function(done) {
     var id = 1874;
     request(app)
     .get('/api/avtobusi/' + id)
     .expect(200, done);
    });
  
    //izjemni tok - ne moremo testirati ker imamo drop down meni za postaje in ne moreš izbrati takšne ki ne obstaja
    
    /*
    it('vpiši napačen id oz. id ki ne obstaja - napaka', function(done) {
     var id = 1874333333333333333;
     request(app)
     .get('/api/avtobusi/' + id)
     .expect(500, done);
    });*/
    
  
});
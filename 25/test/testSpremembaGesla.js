var app = require('../app.js');
var request = require('supertest');
var expect = require('chai').expect;

const assert = require('chai').assert;



describe('SPREMEMBA GESLA', function() {
   it('Odpri stran za sprememboGesla', function(done) {
      request(app).get('/').expect(200, done);
   });
   /* global dialog, $uibModal */
   
   /*
   it('should call confirmation dialog with arguments', function() {
    dialog.confirmationDialog( 'Close', 'Save Changes');
    expect($uibModal.open).to.have
        .been.called;
    });*/
   
   //osnovni tok
   it('Pravilna sprememba gesla', function(done) {
       let data = {
                 "email" : "sara@sara",
                 "oldPassword" : "sara",
                 "repeatOldPassword" : "sara",
                 "newPassword" : "sarasara"
        };
       setTimeout(function(){
        request(app)
        .post('/api/spremembaGesla')
        .send(data)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
       }, 2.000);
   });
   
   //izjemni tok 1
   it('Neuspešna sprememba gesla - razlikujeta se gesli', function(done) {
       let data = {
                 "email" : "sara@sara",
                 "oldPassword" : "sara",
                 "repeatOldPassword" : "saraNapacno",
                 "newPassword" : "sarasara"
        };
       setTimeout(function(){
        request(app)
        .post('/api/spremembaGesla')
        .send(data)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(400)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
       }, 2.000);
   });
  
   
   

});
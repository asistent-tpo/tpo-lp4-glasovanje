var app = require('../app.js');
var request = require('supertest');
var expect = require('chai').expect;

const assert = require('chai').assert;

var id = null;

describe('DOGODKI', function() {
   it('Odpri stran za ogled dogodkov', function(done) {
      request(app).get('/dogodki/')
      .expect(200, done);
   });
   
   //izjemni tok
    it('ne potrdi dodajanje dogodka', function(done) {
         let data = {
           "ime" : "DogodekTest",
           "datum" : "2020-07-22T00:00:00.000Z",
           "organizator" : "testni organizator",
           "opis" : "testni dogodek na FRI-ju"
       }
        request(app)
        .post('/')
        .send(data)
        .expect(200)
        .end(function(err, res){
            if(err) return done(err);
            else {
                id = res.body._id;
                assert.notExists(id);
            }
        });
        done();
    });
  
    //osnovni tok   
   it('uspešno dodaj dogodek', function(done) {
       let data = {
           "ime" : "DogodekTest",
           "datum" : "2020-07-22T00:00:00.000Z",
           "organizator" : "testni organizator",
           "opis" : "testni dogodek na FRI-ju"
       }
        request(app)
        .post('/api/vnosDogodki')
        .send(data)
        .expect(201)
        .end(function(err, res){
            if(err) return done(err)
            else 
                assert.equal(res.body.ime, data.ime);
        });
        done();
    });
  
    

  
});
var app = require('../app.js');
var request = require('supertest');
var expect = require('chai').expect;

const assert = require('chai').assert;

const randomEmail = Math.random().toString(36).substr(2, 5) + "@" + Math.random().toString(36).substr(2, 5) + ".com";

describe('REGISTRACIJA', function() {
   it('Odpri stran za registracijo', function(done) {
      request(app).get('/registracija').expect(200, done);
   });
   
   //osnovni tok 
   it('Pravilna registracija', function(done) {
       let data = {
                 "email" : randomEmail,
                 "password" : "testPassword",
                 "repeatPassword" : "testPassword"
        };
       setTimeout(function(){
        request(app)
        .post('/api/registracija')
        .send(data)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
       }, 2.000);
   });
   
   //izhemni tok 1
   it('Ponovno poskuša registrirati že registrirano osebo', function(done) {
       let data = {
                 "email" : randomEmail,
                 "password" : "testPassword",
                 "repeatPassword" : "testPassword"
        };
       setTimeout(function(){
        request(app)
        .post('/api/registracija')
        .send(data)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(500)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
       }, 2.000);
   });
});

describe('PRIJAVA', function() {
   it('Odpri stran za prijavo', function(done) {
      request(app).get('/prijava').expect(200, done);
   });
   
   //osnovni tok 
   it('Pravilna prijava', function(done) {
       let data = {
                 "email" : randomEmail,
                 "password" : "testPassword",
        };
       setTimeout(function(){
        request(app)
        .post('/api/prijava')
        .send(data)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
       }, 2.000);
   });
   
   //izjemni tok 1
   it('Napačni podatki - zavrni prijavo', function(done) {
       let data = {
                 "email" : randomEmail,
                 "password" : "testPasswordNapacno",
        };
       setTimeout(function(){
        request(app)
        .post('/api/prijava')
        .send(data)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .expect(401)
        .end(function(err, res){
            if(err) return done(err);
        });
        done();
       }, 2.000);
   });
});

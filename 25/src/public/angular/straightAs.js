/* global angular, $http, $q */
var straightAs = angular.module('straightAs', []);

var straightAsKoledar = function($http) {
  return $http.get("/api/koledar");
};

var straightAsUrnik = function($http) {
  return $http.get("/api/urnik");
};

var straightAsTodo = function($http) {
  return $http.get("/api/todo");
};


var homepageCtrl = function($scope, $q, straightAsKoledar, straightAsUrnik, straightAsTodo) {
    $scope.sporocilo1 = "Iščem vnose.";
    $scope.sporocilo2 = "Iščem vnose.";
    $scope.sporocilo3 = "Iščem vnose.";
    $q.all([straightAsKoledar, straightAsUrnik, straightAsTodo]).then(function success(odgovor) {
        $scope.sporocilo1 = odgovor[0].data.length > 0 ? "" : "Ne najdem vnosov.";
        $scope.sporocilo2 = odgovor[1].data.length > 0 ? "" : "Ne najdem vnosov.";
        $scope.sporocilo3 = odgovor[2].data.length > 0 ? "" : "Ne najdem vnosov.";
        $scope.data = {
            koledarVnos: odgovor[0].data,
            urnikVnos: odgovor[1].data,
            todoVnos: odgovor[2].data
        };
    },  function error(odgovor) {
            $scope.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
    });
}; 

straightAs
  .controller('homepageCtrl', homepageCtrl)
  .service('straightAsKoledar', straightAsKoledar)
  .service('straightAsUrnik', straightAsUrnik)
  .service('straightAsTodo', straightAsTodo);
  
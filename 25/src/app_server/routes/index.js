var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');
var ctrlOstalo = require('../controllers/ostalo');

/* GET home page. */
router.get('/', ctrlOstalo.angularApp);

router.get('/registracija', ctrlMain.registracija);
router.get('/prijava', ctrlMain.prijava);
router.get('/vnosUrnik', ctrlMain.vnosUrnik);
router.post('/vnosUrnik', ctrlMain.vnosUrnikDodaj);
router.post('/urnik/:idVnos', ctrlMain.vnosUrnikIzbrisi);
router.get('/vnosTODO', ctrlMain.vnosTODO);

router.get('/vnosKoledar', ctrlMain.vnosKoledar);
router.post('/vnosKoledar', ctrlMain.vnosKoledarDodaj);
router.get('/urediVnosIzKoledarja/:idVnos', ctrlMain.vnosKoledarPosodobiPrikazi);
router.post('/urediVnosIzKoledarja/:idVnos', ctrlMain.vnosKoledarPosodobi);
router.get('/izbrisiVnosIzKoledarja/:idVnos', ctrlMain.vnosKoledarIzbrisi); // get metoda, ker nobena druga ni možna ker se kliče iz gumba

module.exports = router;
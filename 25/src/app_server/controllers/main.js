var request = require('request-promise');
var moment = require('moment');
var apiParametri = {
  streznik: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParametri.streznik = 'https://tpo-group25.herokuapp.com';
}

var prikaziZacetniSeznam = function(req, res) {
  res.render('homepage', {
    podatkiKoledar: "neki",
    podatkiUrnik: "neki",
    podatkiToDo: "neki",
  });  
};

module.exports.registracija = function(req, res) {
	res.render('registracija', {title: 'Registracija'});
};

module.exports.prijava = function(req, res) {
	res.render('prijava', {title: 'Prijava'});
};

module.exports.homepage = function(req, res) {
  /*var data1;
  var data2;
  var data3;
  
	var path = "/api/koledar";
	var parametriZahteve = {
		  url: apiParametri.streznik + path,
	    method: 'GET',
	    json: {}
	};

	var path2 = "/api/urnik";
	var parametriZahteve2 = {
		  url: apiParametri.streznik + path2,
	    method: 'GET',
	    json: {}
	};
	
	var path3 = "/api/todo";
	var parametriZahteve3 = {
		  url: apiParametri.streznik + path3,
	    method: 'GET',
	    json: {}
	};
	
	request(parametriZahteve)
    .then(function(vsebina1) {
      data1 = vsebina1;
      return request(parametriZahteve2);
    }
	)
	.then(function(vsebina2) {
      	data2 = vsebina2;
      	return request(parametriZahteve3);
    }
	)
	.then(function(vsebina3) {
      	data3 = vsebina3;
      	//console.log('data1', data1, 'data2', data2, 'data3', data3);
      	res.render('homepage', {podatkiKoledar: data1, podatkiUrnik: data2, podatkiToDo: data3 });
    }
	);
	*/
	prikaziZacetniSeznam(req, res);
};

module.exports.vnosUrnik = function(req, res) {
	res.render('dodajVnosUrnik', {title: 'Dodaj vnos v urnik', url: req.originalUrl});
};
module.exports.vnosUrnikDodaj = function(req, res) {
	var path = "/api/vnosUrnik";
	var posredovaniPodatki = {
		"email": "testni.mail@gmail.com",
		"Name": req.body.Name,
		"CourseDuration": req.body.CourseDuration,
		"Colour": req.body.Colour,
	};
	var parametriZahteve = {
		url: apiParametri.streznik + path,
	  method: 'POST',
	  json: posredovaniPodatki
	};
	request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      if (odgovor.statusCode === 201) {
        res.redirect('/');
      } else {
        prikaziNapako(req, res, odgovor.statusCode);
      }
    }
  );
};
module.exports.vnosUrnikIzbrisi = function(req, res) {
  var pot = '/api/urnik/' + req.params.idVnos;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'DELETE',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      if (odgovor.statusCode === 204) {
        res.redirect('/');
      } else {
        prikaziNapako(req, res, odgovor.statusCode);
      }
    }
  );
};





module.exports.vnosTODO = function(req, res) {
	res.render('dodajVnosTODO', {title: 'Dodaj vnos v TODO seznam', rootUrl: apiParametri.streznik});
};


module.exports.vnosKoledar = function(req, res) {
	res.render('dodajVnosKoledar', {title: 'Dodaj vnos v koledar', url: req.originalUrl});
};

module.exports.vnosKoledarDodaj = function(req, res) {
	var path = "/api/vnosKoledar";
	var posredovaniPodatki = {
		"email": "testni.mail@gmail.com",
		"Name": req.body.Name,
		"Date": req.body.Date,
		"Duration": req.body.Duration,
		"Description": req.body.Description
	};
	var parametriZahteve = {
		url: apiParametri.streznik + path,
	    method: 'POST',
	    json: posredovaniPodatki
	};
	request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      if (odgovor.statusCode === 201) {
        res.redirect('/');
      } else {
        prikaziNapako(req, res, odgovor.statusCode);
      }
    }
  );
};
module.exports.vnosKoledarIzbrisi = function(req, res) {
	var path = "/api/koledar/" + req.params.idVnos;
	var parametriZahteve = {
		url: apiParametri.streznik + path,
	    method: 'DELETE',
	    json: {}
	};
	request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      if (odgovor.statusCode === 204) {
        res.redirect('/');
      } else {
        prikaziNapako(req, res, odgovor.statusCode);
      }
    }
  );
};
module.exports.vnosKoledarPosodobiPrikazi = function(req, res) {
  if (!req.params.idVnos) {
    prikaziNapako(req, res, 400);
  } else {
  var path = "/api/koledar/" + req.params.idVnos;
  	var parametriZahteve = {
  		url: apiParametri.streznik + path,
	    method: 'GET',
	    json: {}
  	};
  	request(
      parametriZahteve,
      function(napaka, odgovor, vsebina) {
        if (odgovor.statusCode === 200) {
          var dat = moment(vsebina.datum).format("YYYY-MM-DD");
          res.render('urediVnosKoledar', {
            podatki : vsebina,
            podatkiDatum : dat
          });
        } else {
          prikaziNapako(req, res, odgovor.statusCode);
        }
      }
    );
  }
  
};
module.exports.vnosKoledarPosodobi = function (req, res) {
  const idVnosa = req.params.idVnos;
  if (!idVnosa) {
    
  } else {
    var path = "/api/koledar/" + idVnosa;
    var posredovaniPodatki = {
  		"Name": req.body.Name,
  		"Date": req.body.Date,
  		"Duration": req.body.Duration,
  		"Description": req.body.Description
	  };
	  var parametriZahteve = {
		  url: apiParametri.streznik + path,
	    method: 'PUT',
	    json: posredovaniPodatki
	  };
	  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      if (odgovor.statusCode === 200) {
        res.redirect('/');
      } else {
        prikaziNapako(req, res, odgovor.statusCode);
      }
    }
  );
  }
};


var prikaziNapako = function(req, res, statusCode) {
  var naslov, vsebina;
  if (statusCode == 404) {
    naslov = "404, strani ni mogoče najti.";
    vsebina = "Hmm, kako je to mogoče? Nekaj je šlo narobe.";
  } else {
    naslov = statusCode + ", nekaj je šlo narobe.";
    vsebina = "Nekaj nekje očitno ne deluje.";
  }
  res.status(statusCode);
  res.render('genericno-besedilo', {
    title: naslov,
    vsebina: vsebina
  });
};
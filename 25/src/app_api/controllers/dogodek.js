var mongoose = require('mongoose');
var Dogodek = mongoose.model('dogodek');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vnosiDogodki = function(zahteva, odgovor) {
  Dogodek
	.find()
	.exec(function(napaka, vsiVnosi) {
		if(!vsiVnosi) {
			vrniJsonOdgovor(odgovor, 404, {"sporočilo" : "Ne najdem vnosov!"});
		} else if(napaka) {
			vrniJsonOdgovor(odgovor, 500, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 200, vsiVnosi);
		}
	});
};

module.exports.dodajVnosDogodki = function(zahteva, odgovor) {
  Dogodek.create({
	  ime: zahteva.body.ime,
	  datum: zahteva.body.datum,
	  organizator: zahteva.body.organizator,
	  opis: zahteva.body.opis
  }, function(napaka, vnosDogodki) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 201, vnosDogodki);
		}
  });
};
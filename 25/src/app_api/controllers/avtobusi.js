var axios = require('axios');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vsiPodatkiPostaje = function(zahteva, odgovor) {
    axios.get('http://data.lpp.si/stations/getAllStations')
    .then(response => {
        	vrniJsonOdgovor(odgovor, 200, response.data);
    })
    .catch(error => {
        vrniJsonOdgovor(odgovor, 500, error);
    });
    
};


module.exports.podatkiOPrihodih = function(zahteva, odgovor) {
    var idPostaje = zahteva.params.idPostaje;
    axios.get('http://data.lpp.si/timetables/liveBusArrival?station_int_id=' + idPostaje)
    .then(response => {
        	vrniJsonOdgovor(odgovor, 200, response.data);
    })
    .catch(error => {
        vrniJsonOdgovor(odgovor, 500, error);
    });
    
};

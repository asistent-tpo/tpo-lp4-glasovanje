var mongoose = require('mongoose');
var ToDo = mongoose.model('TODO');
var Uporabnik = mongoose.model('Uporabnik');

var vrniUporabnika = function(zahteva, odgovor, povratniKlic) {
  if (zahteva.payload && zahteva.payload.email) {
    Uporabnik
      .findOne({
        email: zahteva.payload.email
      })
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        povratniKlic(zahteva, odgovor, uporabnik.email);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Ni podatka o uporabniku"
    });
    return;
  }
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vnosiTODO = function(zahteva, odgovor) {
  if (zahteva.query.email != undefined) {
    ToDo
  	.find({'email': zahteva.query.email})
  	.exec(function(napaka, vsiVnosi) {
  		if(!vsiVnosi) {
  			vrniJsonOdgovor(odgovor, 404, {"sporočilo" : "Ne najdem vnosov!"});
  		} else if(napaka) {
  			vrniJsonOdgovor(odgovor, 500, napaka);
  		} else {
  			vrniJsonOdgovor(odgovor, 200, vsiVnosi);
  		}
  	});
  }
};

module.exports.dodajVnosTODO = function(zahteva, odgovor) {
    ToDo.create({
      email: zahteva.body.email,
  	  opis: zahteva.body.opis,
      }, function(napaka, vnosToDo) {
  		if (napaka) {
  			vrniJsonOdgovor(odgovor, 400, napaka);
  		} else {
  			vrniJsonOdgovor(odgovor, 201 , vnosToDo);
  		}
  });
};

module.exports.todoPreberiIzbrano= function(zahteva, odgovor) {
  ToDo
    .findById(zahteva.params.idVnos)
    .exec(function(napaka, vnos) {
      vrniJsonOdgovor(odgovor, 200, vnos);
    });
};

module.exports.todoPosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idVnos) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem vnosa, idVnosa je obvezen parameter"
    });
    return;
  }
  ToDo
    .findById(zahteva.params.idVnos)
    .select('-email')
    .exec(
      function(napaka, vnos) {
        if (!vnos) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem vnosa."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        vnos.opis = zahteva.body.opis;
        vnos.save(function(napaka, vnos2) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, vnos2);
          }
        });
      }
    );
};


module.exports.todoIzbrisiIzbrano = function(zahteva, odgovor) {
  var idVnos = zahteva.params.idVnos;
  if (idVnos) {
    ToDo
      .findByIdAndRemove(idVnos)
      .exec(
        function(napaka, toDoVnos) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem vnosa, idVnos je obvezen parameter."
    });
  }
};
var mongoose = require('mongoose');
var Koledar = mongoose.model('koledarVnos');
var Uporabnik = mongoose.model('Uporabnik');

var vrniUporabnika = function(zahteva, odgovor, povratniKlic) {
  if (zahteva.payload && zahteva.payload.email) {
    Uporabnik
      .findOne({
        email: zahteva.payload.email
      })
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        povratniKlic(zahteva, odgovor, uporabnik.email);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Ni podatka o uporabniku"
    });
    return;
  }
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vnosiKoledar = function(zahteva, odgovor) {
  console.log("Zahteva.query.email: " + zahteva.query.email)
  if (zahteva.query.email != undefined) {
    Koledar
    	.find({'email': zahteva.query.email})
    	.exec(function(napaka, vsiVnosi) {
    		if(!vsiVnosi) {
    			vrniJsonOdgovor(odgovor, 404, {"sporočilo" : "Ne najdem vnosov!"});
    		} else if(napaka) {
    			vrniJsonOdgovor(odgovor, 500, napaka);
    		} else {
    			vrniJsonOdgovor(odgovor, 200, vsiVnosi);
    		}
  	});
  }
};

module.exports.dodajVnosKoledar = function(zahteva, odgovor) {
    Koledar.create({
      email: zahteva.body.email,
  	  ime: zahteva.body.Name,
  	  datum: zahteva.body.Date,
  	  trajanje: zahteva.body.Duration,
  	  opis: zahteva.body.Description
    }, function(napaka, vnosKoledar) {
  		if (napaka) {
  			vrniJsonOdgovor(odgovor, 400, napaka);
  		} else {
  			vrniJsonOdgovor(odgovor, 201, vnosKoledar);
  		}
    });
};

module.exports.koledarPreberiIzbrano= function(zahteva, odgovor) {
  Koledar
    .findById(zahteva.params.idVnos)
    .exec(function(napaka, vnos) {
      if(!vnos) {
  			vrniJsonOdgovor(odgovor, 404, {"sporočilo" : "Ne najdem vnosa!"});
  		} else if(napaka) {
  			vrniJsonOdgovor(odgovor, 500, napaka);
  		} else {
  			vrniJsonOdgovor(odgovor, 200, vnos);
  		}
    });
};

module.exports.koledarPosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idVnos) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem vnosa, idVnosa je obvezen parameter"
    });
    return;
  }
  Koledar
    .findById(zahteva.params.idVnos)
    .select('-email')
    .exec(
      function(napaka, koledarVnos) {
        if (!koledarVnos) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem vnosa."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        koledarVnos.ime = zahteva.body.Name;
        koledarVnos.datum = zahteva.body.Date;
        koledarVnos.trajanje = zahteva.body.Duration;
        koledarVnos.opis = zahteva.body.Description;
        koledarVnos.save(function(napaka, koledarVnos) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, koledarVnos);
          }
        });
      }
    );
};

module.exports.koledarIzbrisiIzbrano = function(zahteva, odgovor) {
  var idVnosa = zahteva.params.idVnos;
  if (idVnosa) {
    Koledar
      .findByIdAndRemove(idVnosa)
      .exec(
        function(napaka, vnos) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204 , null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem naprave, idVnosa je obvezen parameter." + idVnosa
    });
  }
};
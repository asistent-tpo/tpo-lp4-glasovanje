var mongoose = require('mongoose');
var Urnik = mongoose.model('urnikVnos');
var Uporabnik = mongoose.model('Uporabnik');

var vrniUporabnika = function(zahteva, odgovor, povratniKlic) {
  if (zahteva.payload && zahteva.payload.email) {
    Uporabnik
      .findOne({
        email: zahteva.payload.email
      })
      .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem uporabnika"
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        povratniKlic(zahteva, odgovor, uporabnik.email);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Ni podatka o uporabniku"
    });
    return;
  }
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vnosiUrnik = function(zahteva, odgovor) {
  if (zahteva.query.email != undefined) {
  Urnik
	.find({'email': zahteva.query.email})
	.exec(function(napaka, vsiVnosi) {
		if(!vsiVnosi) {
			vrniJsonOdgovor(odgovor, 404, {"sporočilo" : "Ne najdem vnosov!"});
		} else if(napaka) {
			vrniJsonOdgovor(odgovor, 500, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 200, vsiVnosi);
		}
	});
  }
};

module.exports.dodajVnosUrnik = function(zahteva, odgovor) {
    Urnik.create({
    email: zahteva.body.email,
	  ime: zahteva.body.Name,
	  trajanje: zahteva.body.CourseDuration,
	  barva: zahteva.body.Colour
    }, function(napaka, vnosUrnik) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 201, vnosUrnik);
		}
  });
};

module.exports.urnikPreberiIzbrano = function(zahteva, odgovor) {
  Urnik
    .findById(zahteva.params.idVnos)
    .exec(function(napaka, vnos) {
      vrniJsonOdgovor(odgovor, 200, vnos);
    });
};

module.exports.urnikPosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idVnos) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem vnosa, idVnosa je obvezen parameter"
    });
    return;
  }
  Urnik
    .findById(zahteva.params.idVnos)
    .select('-email')
    .exec(
      function(napaka, urnikVnos) {
        if (!urnikVnos) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem vnosa."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        urnikVnos.ime = zahteva.body.Name;
        urnikVnos.trajanje = zahteva.body.CourseDuration;
        urnikVnos.barva = zahteva.body.Colour;
        urnikVnos.save(function(napaka, urnikVnos) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, urnikVnos);
          }
        });
      }
    );
};

module.exports.urnikIzbrisiIzbrano = function(zahteva, odgovor) {
  var idVnos = zahteva.params.idVnos;
  if (idVnos) {
    Urnik
      .findByIdAndRemove(idVnos)
      .exec(
        function(napaka, urnikVnos) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem vnosa, idVnos je obvezen parameter."
    });
  }
};
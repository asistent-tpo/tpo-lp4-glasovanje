var passport = require('passport');
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.registracija = function(zahteva, odgovor) {
  if (!zahteva.body.email || !zahteva.body.password || !zahteva.body.repeatPassword) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  if (zahteva.body.password === zahteva.body.repeatPassword) {
	  var uporabnik = new Uporabnik();
	  uporabnik.email = zahteva.body.email;
	  uporabnik.nastaviGeslo(zahteva.body.password);
	  
	  // treba je nastavit še dostopniZeton!!!
	  
	  uporabnik.save(function(napaka) {
	   if (napaka) {
	     vrniJsonOdgovor(odgovor, 500, napaka);
	   } else {
	     vrniJsonOdgovor(odgovor, 200, {
	       "zeton": uporabnik.generirajJwt()
	     });
	   }
	  });
  }
};

module.exports.prijava = function(zahteva, odgovor) {
  if (!zahteva.body.email || !zahteva.body.password) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  passport.authenticate('local', function(napaka, uporabnik, podatki) {
    if (napaka) {
      console.log(napaka)
      vrniJsonOdgovor(odgovor, 404, napaka);
      return;
    }
    if (uporabnik) {
      vrniJsonOdgovor(odgovor, 200, {
        "zeton": uporabnik.generirajJwt()
      });
    } else {
      console.log("TU pride do napake")
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
};

module.exports.spremembaGesla = function(zahteva, odgovor) {
  if (!zahteva.body.oldPassword || !zahteva.body.repeatOldPassword || !zahteva.body.newPassword) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  if (zahteva.body.oldPassword === zahteva.body.repeatOldPassword) {
    var posodobljenUporabnik = new Uporabnik();
    posodobljenUporabnik.email = zahteva.body.email;
    posodobljenUporabnik.nastaviGeslo(zahteva.body.newPassword);
    
    Uporabnik
      .findOneAndRemove({"email" : zahteva.body.email})
      .exec(function(napaka, starUporabnik) {
        if (napaka) {
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        } else {
          posodobljenUporabnik.save(function(napaka) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, {
            "zeton": posodobljenUporabnik.generirajJwt()
	     });
	   }
	  });
        }
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporocilo": "Gesli se ne ujemata"
    });
  }
};
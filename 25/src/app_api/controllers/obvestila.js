var mongoose = require('mongoose');
var Obvestila = mongoose.model('obvestila');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.vnosiObvestila = function(zahteva, odgovor) {
  Obvestila
	.find()
	.exec(function(napaka, vsiVnosi) {
		if(!vsiVnosi) {
			vrniJsonOdgovor(odgovor, 404, {"sporočilo" : "Ne najdem vnosov!"});
		} else if(napaka) {
			vrniJsonOdgovor(odgovor, 500, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 200, vsiVnosi);
		}
	});
};

module.exports.dodajVnosObvestila = function(zahteva, odgovor) {
  Obvestila.create({
	  sporocilo: zahteva.body.sporocilo
  }, function(napaka, vnosObvestila) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 201, vnosObvestila);
		}
  });
};
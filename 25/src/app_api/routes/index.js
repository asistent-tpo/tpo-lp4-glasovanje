var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var avtentikacija = jwt({
  secret: process.env.JWT_GESLO,
  userProperty: 'payload'
});

var ctrlUrnik = require('../controllers/urnik.js');
var ctrlTODO = require('../controllers/todo.js');
var ctrlKoledar = require('../controllers/koledar.js');
var ctrlDogodki = require('../controllers/dogodek.js');
var ctrlAvtentikacija = require('../controllers/avtentikacija.js');
var ctrlAvtobusi = require('../controllers/avtobusi.js');
var ctrlHrana = require('../controllers/hrana.js');
var ctrlObvestila = require('../controllers/obvestila.js');

/* Urnik */
router.get('/urnik', ctrlUrnik.vnosiUrnik); //dela
router.post('/vnosUrnik', ctrlUrnik.dodajVnosUrnik); //dela
router.get('/urnik/:idVnos', ctrlUrnik.urnikPreberiIzbrano); //dela
router.put('/urnik/:idVnos', ctrlUrnik.urnikPosodobiIzbrano); //dela
router.delete('/urnik/:idVnos', ctrlUrnik.urnikIzbrisiIzbrano); //dela

/* TODO */
router.get('/todo', ctrlTODO.vnosiTODO);
router.post('/vnosTODO', ctrlTODO.dodajVnosTODO);
router.get('/todo/:idVnos', ctrlTODO.todoPreberiIzbrano);
router.put('/todo/:idVnos', ctrlTODO.todoPosodobiIzbrano);
router.delete('/todo/:idVnos', ctrlTODO.todoIzbrisiIzbrano);

/* Koledar */
router.get('/koledar', ctrlKoledar.vnosiKoledar); //dela
router.post('/vnosKoledar', ctrlKoledar.dodajVnosKoledar); //dela
router.get('/koledar/:idVnos', ctrlKoledar.koledarPreberiIzbrano); //dela
router.put('/koledar/:idVnos', ctrlKoledar.koledarPosodobiIzbrano);
router.delete('/koledar/:idVnos', ctrlKoledar.koledarIzbrisiIzbrano); //dela

/* Avtentikacija */
router.post('/registracija', ctrlAvtentikacija.registracija);
router.post('/prijava', ctrlAvtentikacija.prijava);
router.post('/spremembaGesla', ctrlAvtentikacija.spremembaGesla);


/* avtobusi */

router.get('/avtobusi', ctrlAvtobusi.vsiPodatkiPostaje); //dela
router.get('/avtobusi/:idPostaje', ctrlAvtobusi.podatkiOPrihodih);


/* hrana */

router.get('/hrana', ctrlHrana.vsiPodatkiRestavracij); //dela


/*Dogodki */
router.get('/dogodki', ctrlDogodki.vnosiDogodki); 
router.post('/vnosDogodki', ctrlDogodki.dodajVnosDogodki); 

/*Obvestila */

router.get('/obvestila', ctrlObvestila.vnosiObvestila); 
router.post('/vnosObvestila', ctrlObvestila.dodajVnosObvestila);

module.exports = router;
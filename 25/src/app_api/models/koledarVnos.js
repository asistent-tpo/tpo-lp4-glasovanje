var mongoose = require('mongoose');

var koledarVnosShema = new mongoose.Schema({
  email: {type: String, required: true},
  ime: String,
  datum: Date,
  trajanje: Number,
  opis: String
});


mongoose.model('koledarVnos', koledarVnosShema, 'koledarVnosi');
var mongoose = require('mongoose');

var dogodekShema = new mongoose.Schema({
  ime: {type: String, required: true},
  datum: Date,
  organizator: String,
  opis: String
});


mongoose.model('dogodek', dogodekShema, 'dogodki');
var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var uporabnikShema = new mongoose.Schema({
  email: {type: String, required: true, unique: true},
  hashed_geslo: String,
  dostopni_zeton: String,
  nakljucna_vrednost: String
});

uporabnikShema.methods.nastaviGeslo = function(geslo) {
  this.nakljucnaVrednost = "saltinsol";
  this.hashed_geslo = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

uporabnikShema.methods.preveriGeslo = function(geslo) {
  var hashed_geslo = crypto.pbkdf2Sync(geslo, "saltinsol", 1000, 64, 'sha512').toString('hex');
  return this.hashed_geslo == hashed_geslo;
};

uporabnikShema.methods.generirajJwt = function() {
  var datumPoteka = new Date();
  datumPoteka.setDate(datumPoteka.getDate() + 7);
  
  return jwt.sign({
    _id: this._id,
    email: this.email,
    datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
  }, process.env.JWT_GESLO);
};

mongoose.model('Uporabnik', uporabnikShema, 'Uporabniki');
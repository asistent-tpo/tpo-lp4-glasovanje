var mongoose = require('mongoose');

var urnikVnosShema = new mongoose.Schema({
  email: {type: String, required: true},
  ime: String,
  trajanje: Number,
  barva: String
});


mongoose.model('urnikVnos', urnikVnosShema, 'urnikVnosi');
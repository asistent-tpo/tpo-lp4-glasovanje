function prijavaCtrl($location, avtentikacija) {
    var vm = this;
    vm.glavaStrani = {
        naslov: 'Login'
    };
    
    vm.prijavniPodatki = {
      email: "",
      password: ""
    };

    vm.prvotnaStran = $location.search().stran || '/';

    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.email || !vm.prijavniPodatki.password) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } else {
        vm.izvediPrijavo();
      }
    };
    

    vm.izvediPrijavo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .prijava(vm.prijavniPodatki)
        .then(
          function(success) {
            $location.search('stran', null);
            $location.path(vm.prvotnaStran);
          },
          function(napaka) {
            //console.log(napaka)
            vm.napakaNaObrazcu = napaka.data.sporocilo;
          }
        );
    };
}
  
/* global straightAs */
straightAs
    .controller('prijavaCtrl', prijavaCtrl);

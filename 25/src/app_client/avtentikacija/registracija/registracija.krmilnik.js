/* global $ */
function registracijaCtrl($location, avtentikacija) {
    var vm = this;
    
    vm.glavaStrani = {
        naslov: 'Registration'
    };


    vm.prijavniPodatki = {
      email: "",
      password: "", 
      repeatPassword: ""
    };
    
    
    vm.prvotnaStran = $location.search().stran || '/';
    
    vm.posiljanjePodatkov = function() {
      
      var ustrezenVnos = true;
      var error;
      $('.alert').remove();
      
      var geslo1 = document.getElementById("password1").value;
      var geslo1dolzina = geslo1.length;
      console.log(geslo1 + " " + geslo1dolzina);
      
      if(geslo1dolzina < 8){
        ustrezenVnos = false;
        error = document.getElementById('password1');
        error.insertAdjacentHTML('afterend', '<div role="alert" class="alert alert-info">Geslo mora biti dolgo vsaj 8 znakov!</div>');
  
      }
      
      var geslo2 = document.getElementById("password2").value;
      console.log(geslo2);
      
      if(geslo1 != geslo2){
        ustrezenVnos = false;
        error = document.getElementById('password2');
        error.insertAdjacentHTML('afterend', '<div role="alert" class="alert alert-info">Gesli se ne ujemata!</div>');
      }
      
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.email || !vm.prijavniPodatki.password || !vm.prijavniPodatki.repeatPassword) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } 
      else if(!ustrezenVnos){
        return false;
      }  
      else {
        vm.izvediRegistracijo();
      }
    };
    
      vm.izvediRegistracijo = function() {
        vm.napakaNaObrazcu = "";
        avtentikacija
          .registracija(vm.prijavniPodatki)
          .then(
            function(success) {
              $location.search('stran', null);
              $location.path(vm.prvotnaStran);
            },
            function(napaka) {
              vm.napakaNaObrazcu = napaka.data.sporocilo;
            }
          );
        };

}
  
/* global straightAs */
straightAs
    .controller('registracijaCtrl', registracijaCtrl);
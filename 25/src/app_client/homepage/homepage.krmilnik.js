/* global straightAs, $q */

var homepageCtrl = function( $window, $uibModal, $q, straightAsKoledar, straightAsUrnik, straightAsTodo, straightAsPodatki, avtentikacija, $location) {
    var vm = this;  
    
    //K O L E D A R  
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    vm.prvotnaStran = $location.path();
    
    if (avtentikacija.jeAdmin()) {
      $location.path('/admin');
    }

    if (avtentikacija.jeEventManager()) {
      $location.path('/dogodkiUpravljanje');
    }
    
    vm.prikaziPojavnoOknoKoledarVnos = function() {
      
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/koledarVnosModalnoOkno/koledarVnosModalnoOkno.pogled.html',
        controller: 'koledarVnosModalnoOkno',
        controllerAs: 'vm'
      });
      
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.data.koledarVnos.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
      
    };
    
    vm.prikaziPojavnoOknoKoledarUredi = function(koledar) {
      console.log(koledar);
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/koledarUrediModalnoOkno/koledarUrediModalnoOkno.pogled.html',
        controller: 'koledarUrediModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiVnosa: function() {
            return {
              idVnosa: koledar._id,
              ime: koledar.ime,
              datum: koledar.datum,
              trajanje: koledar.trajanje,
              opis: koledar.opis
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          $window.location.reload();
        }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
      
      
    };
    
    vm.odstraniIzKoledarja = function(id) {
      var idx = -1;
      for (var i = 0; i < vm.data.koledarVnos.length; i++) {
        if (id == vm.data.koledarVnos[i]._id)
					idx = i;
      }
      if (idx != -1) {
        straightAsPodatki.odstraniVnosKoledar(vm.data.koledarVnos[idx]._id).then(
          function success(odgovor) {
            vm.data.koledarVnos.splice(idx, 1);
            console.log("Uspešen izbris iz koledarja!");
          }, function error(odgovor) {
            console.log("Napaka pri izbrisu iz koledarja!");
          }
        );
      }
    };
    
    // U R N I K
    
    vm.prikaziPojavnoOknoUrnikVnos = function() {
      
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/urnikVnosModalnoOkno/urnikVnosModalnoOkno.pogled.html',
        controller: 'urnikVnosModalnoOkno',
        controllerAs: 'vm'
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.data.urnikVnos.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
    };
    
    vm.prikaziPojavnoOknoUrnikUredi = function(urnik) {
      console.log(urnik);
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/urnikUrediModalnoOkno/urnikUrediModalnoOkno.pogled.html',
        controller: 'urnikUrediModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiVnosa: function() {
            return {
              idVnosa: urnik._id,
              ime: urnik.ime,
              trajanje: urnik.trajanje,
              barva: urnik.barva
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          $window.location.reload();
        }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
      
      
    };
    
    vm.odstraniIzUrnika = function(id) {
      var idx = -1;
      for (var i = 0; i < vm.data.urnikVnos.length; i++) {
        if (id == vm.data.urnikVnos[i]._id)
					idx = i;
      }
      if (idx != -1) {
        straightAsPodatki.odstraniVnosUrnik(vm.data.urnikVnos[idx]._id).then(
          function success(odgovor) {
            vm.data.urnikVnos.splice(idx, 1);
            console.log("Uspešen izbris iz urnika!");
          }, function error(odgovor) {
            console.log("Napaka pri izbrisu iz urnika!");
          }
        );
      }
    };
    
    // T O D O
    vm.prikaziPojavnoOknoTodoVnos = function() {
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/todoVnosModalnoOkno/todoVnosModalnoOkno.pogled.html',
        controller: 'todoVnosModalnoOkno',
        controllerAs: 'vm'
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.data.todoVnos.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
    };
    
    vm.prikaziPojavnoOknoTodoUredi = function(todo) {
      console.log(todo);
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/todoUrediModalnoOkno/todoUrediModalnoOkno.pogled.html',
        controller: 'todoUrediModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiVnosa: function() {
            return {
              idVnosa: todo._id,
              opis: todo.opis
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          $window.location.reload();
        }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
      
      
    };
    
    vm.odstraniIzTodo = function(id) {
      var idx = -1;
      for (var i = 0; i < vm.data.todoVnos.length; i++) {
        if (id == vm.data.todoVnos[i]._id)
					idx = i;
      }
      if (idx != -1) {
        straightAsPodatki.odstraniVnosTodo(vm.data.todoVnos[idx]._id).then(
          function success(odgovor) {
            vm.data.todoVnos.splice(idx, 1);
            console.log("Uspešen izbris iz todo!");
          }, function error(odgovor) {
            console.log("Napaka pri izbrisu iz todo!");
          }
        );
      }
    };
  
    
    
    if(vm.jePrijavljen != false) {
      vm.sporocilo1 = "Iščem vnose.";
      vm.sporocilo2 = "Iščem vnose.";
      vm.sporocilo3 = "Iščem vnose.";
      $q.all([straightAsKoledar, straightAsUrnik, straightAsTodo]).then(function success(odgovor) {
        if (odgovor[0].data && odgovor[1].data && odgovor[2].data) {
          vm.sporocilo1 = odgovor[0].data.length > 0 ? "" : "Ne najdem vnosov.";
          vm.sporocilo2 = odgovor[1].data.length > 0 ? "" : "Ne najdem vnosov.";
          vm.sporocilo3 = odgovor[2].data.length > 0 ? "" : "Ne najdem vnosov.";
          vm.data = {
              koledarVnos: odgovor[0].data,
              urnikVnos: odgovor[1].data,
              todoVnos: odgovor[2].data
          };
        } else {
          vm.sporocilo1 = "Ni vnosov.";
          vm.sporocilo2 = "Ni vnosov.";
          vm.sporocilo3 = "Ni vnosov.";
        }
      },  function error(odgovor) {
              vm.sporocilo1 = "Prišlo je do napake pri pridobivanju podatkov!";
              vm.sporocilo2 = "Prišlo je do napake pri pridobivanju podatkov!";
              vm.sporocilo3 = "Prišlo je do napake pri pridobivanju podatkov!";
      });
    }
}; 

straightAs
  .controller('homepageCtrl', homepageCtrl);
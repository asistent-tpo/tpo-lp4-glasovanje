function todoUrediModalnoOkno($uibModalInstance, $http, straightAsPodatki, podrobnostiVnosa) {
    var vm = this;

    vm.id = podrobnostiVnosa.idVnosa;

    vm.podatkiObrazca = {
        Description: podrobnostiVnosa.opis,
    };
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal");
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    vm.posljiPodatkePUTTodo = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Description ) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.uredi(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.uredi = function(podatki) {
      straightAsPodatki.urediVnosTodo(vm.id, {
        "opis" : podatki.Description,
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('todoUrediModalnoOkno', todoUrediModalnoOkno);
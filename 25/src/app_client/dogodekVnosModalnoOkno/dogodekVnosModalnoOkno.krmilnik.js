function dogodekVnosModalnoOkno ($uibModalInstance, $http, straightAsDogodki) {
    var vm = this;
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal")
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posljiPodatkePOSTDogodek = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Name || !vm.podatkiObrazca.Date || !vm.podatkiObrazca.Organizer || !vm.podatkiObrazca.Description) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.dodaj(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.dodaj = function(podatki) {
      console.log(podatki.Date)
      straightAsDogodki.dodajVnosDogodki({
        "ime" : podatki.Name,
        "datum" : podatki.Date,
        "organizator" : podatki.Organizer,
        "opis" : podatki.Description
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
    
}

  /* global straightAs, $http */
straightAs
    .controller('dogodekVnosModalnoOkno', dogodekVnosModalnoOkno);

function obvestilaCtrl(straightAsObvestila) {
    var vm = this;
    vm.naslov = "System messages";
    
    straightAsObvestila.vrniObvestila().then(function success(odgovor) {
        vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem vnosov.";
        vm.data = {
            obvestila : odgovor.data
        };
    },  function error(odgovor) {
            vm.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
    });
    
}
  
/* global straightAs */
straightAs
    .controller('obvestilaCtrl', obvestilaCtrl);
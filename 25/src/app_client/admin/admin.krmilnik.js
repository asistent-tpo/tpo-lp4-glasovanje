
function adminCtrl($uibModal) {
    var vm = this;
    vm.naslov = "Admin";
    
    vm.prikaziPojavnoOknoObvestiloVnos = function() {
     
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/obvestilaVnosModalnoOkno/obvestilaVnosModalnoOkno.pogled.html',
        controller: 'obvestilaVnosModalnoOkno',
        controllerAs: 'vm'
      });
      
      
    };
}
  
/* global straightAs */
straightAs
    .controller('adminCtrl', adminCtrl);
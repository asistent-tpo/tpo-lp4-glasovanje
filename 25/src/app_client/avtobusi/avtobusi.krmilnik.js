
function avtobusiCtrl(straightAsPodatki) {
    var vm = this;
    vm.glavaStrani = {
        naslov: 'Bus'
    };
    vm.izbranaPostaja=null;
    vm.podatkiOPrihodih = [];
    vm.uspeh=false;

    straightAsPodatki.pridobiVsePostaje().then(
        function success(odgovor) {
            vm.vsePostaje = odgovor.data.data;
            console.log(vm.vsePostaje);
        }, function error(odgovor) {
            vm.napakaPridobivanjePostaj = "Napaka pri pridobivanju postaj, poskusite znova!";
            console.log(vm.napakaPridobivanjePostaj);
        } 
    );
    
    vm.posodobiPodatkePrihodih = function() {
        straightAsPodatki.pridobiPodatkeOPrihodih(vm.izbranaPostaja).then(
            function success(odgovor) {
                vm.podatkiOPrihodih = odgovor.data.data;
                console.log(vm.podatkiOPrihodih);
                vm.uspeh=true;
            }, function error(odgovor) {
                vm.napakaPodatkiOprihodih = "Napaka pri pridobivanju podatkov za postajo, poskusite znova!";
                console.log(vm.napakaPodatkiOprihodih);
            } 
        );        
    }
    
    vm.aliSoPodatkiOPrihodihNaVoljo = function() {
        return (vm.podatkiOPrihodih.length == 0);       
    }
    vm.aliJePostajaIzbrana = function() {
        return (vm.izbranaPostaja != null);       
    }
    vm.aliSoBiliPodatkiVrnjeni = function() {
        return vm.uspeh;
    }
    
    
}
  
/* global straightAs */
straightAs
    .controller('avtobusiCtrl', avtobusiCtrl);

function urnikVnosModalnoOkno($uibModalInstance, $http, straightAsPodatki, avtentikacija) {
    var vm = this;
    
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal")
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posljiPodatkePOSTUrnik = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Name ||  !vm.podatkiObrazca.Duration || !vm.podatkiObrazca.Colour) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.dodaj(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.dodaj = function(podatki) {
      straightAsPodatki.dodajVnosUrnik({
        "email" : vm.trenutniUporabnik.email,
        "Name" : podatki.Name,
        "CourseDuration" : podatki.Duration,
        "Colour" : podatki.Colour
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('urnikVnosModalnoOkno', urnikVnosModalnoOkno);
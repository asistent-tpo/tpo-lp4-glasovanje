function urnikUrediModalnoOkno($uibModalInstance, $http, straightAsPodatki, podrobnostiVnosa) {
    var vm = this;

    vm.id = podrobnostiVnosa.idVnosa;

    vm.podatkiObrazca = {
        Name: podrobnostiVnosa.ime,
        Duration: podrobnostiVnosa.trajanje,
        Colour : podrobnostiVnosa.barva
    };
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal");
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    vm.posljiPodatkePUTUrnik = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Name || !vm.podatkiObrazca.Duration ) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.uredi(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.uredi = function(podatki) {
      straightAsPodatki.urediVnosUrnik(vm.id, {
        "Name" : podatki.Name,
        "CourseDuration" : podatki.Duration,
        "Colour" : podatki.Colour
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('urnikUrediModalnoOkno', urnikUrediModalnoOkno);
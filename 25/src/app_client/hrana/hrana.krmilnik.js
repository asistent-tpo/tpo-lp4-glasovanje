
function hranaCtrl( $anchorScroll, straightAsPodatki) {
    var vm = this;
    vm.glavaStrani = {
        naslov: 'Food'
    };
    
    vm.scrollTo = function(scrollLocation){
        //$location.hash(scrollLocation);
        //console.log("want to access location: ", scrollLocation);
        $anchorScroll(scrollLocation);
    }
    
    vm.uspesnaGeolokacija = false;
    
    initMap = function() {
        
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition( showPosition = function(position) {
                vm.uspesnaGeolokacija = true;
                vm.napakaLokacija = "Geolokacija uspešna!"
                vm.lat = parseFloat(position.coords.latitude);
                vm.lng = parseFloat(position.coords.longitude);                
                vm.map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude)},
                    zoom: 10
                });  
                
                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude)},
                    map: vm.map,
                    icon:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                });
                var infoWindow = new google.maps.InfoWindow({
                    content: "<p>Tvoja lokacija</p>"
                });
                marker.addListener('click', function(){
                    infoWindow.open(vm.map, marker);
                });                
                
                vm.vrniPodatkeORestavracijah();
                
            }, showError = function(error){
                switch(error.code) {
                    case error.PERMISSION_DENIED:
                        vm.napakaLokacija = "Dostop do lokacije je bil onemogočen."
                         vm.setDefaultLocation();
                        break;
                    case error.POSITION_UNAVAILABLE:
                        vm.napakaLokacija = "Lokacija ni na voljo."
                         vm.setDefaultLocation();
                        break;
                    case error.TIMEOUT:
                        vm.napakaLokacija = "Poizvedba za lokacijo se je iztekla."
                         vm.setDefaultLocation();
                        break;
                    case error.UNKNOWN_ERROR:
                        vm.napakaLokacija = "Neznana napaka."
                         vm.setDefaultLocation();
                        break;
                }
            });
        }
        else { 
            vm.napakaLokacija = "Geolokacija ni podprta v tem brskalniku.";
            vm.setDefaultLocation();
        }
        
        vm.setDefaultLocation = function(){
            vm.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: parseFloat(46.056946), lng: parseFloat(14.505751)},
                zoom: 10
            });
            vm.vrniPodatkeORestavracijah();
        }

        
    }
    
    vm.vrniPodatkeORestavracijah = function() {
        straightAsPodatki.pridobiPodatkeORestavracijah().then(
            function success(odgovor) {
                vm.vseRestavracije = odgovor.data;
                console.log(vm.vseRestavracije);
                vm.izrisiRestavracije();
            }, function error(odgovor) {
                vm.napakaPridobivanjeRestavracij = "Napaka pri pridobivanju restavracij, poskusite znova!";
                console.log(vm.napakaPridobivanjeRestavracij);
            } 
        ); 
    }
    
    vm.izrisiRestavracije = function() {

        for (var i = 0; i < vm.vseRestavracije.length ; i++) {
            vm.addMarker({
                coords: {lat: parseFloat(vm.vseRestavracije[i].latitude_sirina), lng: parseFloat(vm.vseRestavracije[i].longitude_dolzina)},
                content: vm.vseRestavracije[i].ime,
                id: vm.vseRestavracije[i].id
            });
        }
        if(vm.uspesnaGeolokacija){
            vm.razvrstiRestavracije();
        }
    }

    vm.addMarker = function(restavracija){
        var marker = new google.maps.Marker({
            position: restavracija.coords,
            map: vm.map,
        });
        var infoWindow = new google.maps.InfoWindow({
            content: "<a id='button" + restavracija.id + "' >" + restavracija.content + "</a>"
        });
        
        infoWindow.addListener('domready', () => {
            document.getElementById("button"+ restavracija.id).addEventListener("click", () => { vm.scrollTo(restavracija.id)})
        });        
        
        marker.addListener('click', function(){
            infoWindow.open(vm.map, marker);
        });
    }
    
    vm.razvrstiRestavracije = function() {
        
        // izracunaj za vsako restavracijo lokacijo do nase lokacije
        for(var i = 0; i<vm.vseRestavracije.length; i++){
            //lng
            var x1 = vm.lng;
            //lat
            var y1 = vm.lat;
            
            //lng
            var x2 = vm.vseRestavracije[i].longitude_dolzina;
            //lat
            var y2 = vm.vseRestavracije[i].latitude_sirina;
            
            var razdalja = Math.sqrt((x2 -x1) * (x2 - x1) + (y2 -y1) * (y2 - y1));
            vm.vseRestavracije[i].razdalja = razdalja;
            
        } 
        
        // razvrsti restavracije po razdalji
        vm.vseRestavracije.sort(function(a, b){return a.razdalja - b.razdalja});
    
    }
    
}    

/* global straightAs */
straightAs
    .controller('hranaCtrl', hranaCtrl);

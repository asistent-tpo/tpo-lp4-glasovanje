function obvestilaVnosModalnoOkno ($uibModalInstance, $http, straightAsObvestila) {
    var vm = this;
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal")
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posljiPodatkePOSTObvestila = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Message) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.dodaj(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.dodaj = function(podatki) {
      straightAsObvestila.dodajVnosObvestila({
        "sporocilo" : podatki.Message
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
    
}

  /* global straightAs, $http */
straightAs
    .controller('obvestilaVnosModalnoOkno', obvestilaVnosModalnoOkno);
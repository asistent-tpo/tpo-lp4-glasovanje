function todoVnosModalnoOkno($uibModalInstance, $http, straightAsPodatki, avtentikacija) {
    var vm = this;
    
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal")
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posljiPodatkePOSTTodo = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Description) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        console.log(vm.podatkiObrazca);
        vm.dodaj(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.dodaj = function(podatki) {
      straightAsPodatki.dodajVnosTodo({
        "email" : vm.trenutniUporabnik.email,
        "opis" : podatki.Description
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('todoVnosModalnoOkno', todoVnosModalnoOkno);
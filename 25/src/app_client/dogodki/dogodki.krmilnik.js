function dogodkiCtrl(straightAsDogodki) {
    var vm = this;
    straightAsDogodki.vrniDogodke().then(function success(odgovor) {
        vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem vnosov.";
        vm.data = {
            dogodki : odgovor.data
        };
    },  function error(odgovor) {
            vm.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
    });
}
  
/* global straightAs */
straightAs
    .controller('dogodkiCtrl', dogodkiCtrl);
function avtentikacija($window, $http, $route) {
  var b64Utf8 = function (niz) {
    return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  };
  
  var shraniZeton = function(zeton) {
    $window.localStorage['tpo-group25-zeton'] = zeton;
  };
  
  var vrniZeton = function() {
    return $window.localStorage['tpo-group25-zeton'];
  };
  
  var registracija = function(uporabnik) {
    return $http.post('/api/registracija', uporabnik).then(
      function success(odgovor) {
        shraniZeton(odgovor.data.zeton);
      });
  };

  var prijava = function(uporabnik) {
    return $http.post('/api/prijava', uporabnik).then(
      function success(odgovor) {
        shraniZeton(odgovor.data.zeton);
        $route.reload();
      });
  };
  
  var spremeniGeslo = function(uporabnik) {
    return $http.post('/api/spremembaGesla', uporabnik).then(
      function success(odgovor) {
        shraniZeton(odgovor.data.zeton);
      });
  };

  var odjava = function() {
    $window.localStorage.removeItem('tpo-group25-zeton');
  };
  
  var jePrijavljen = function() {
    var zeton = vrniZeton();
    if (zeton) {
      var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
      return koristnaVsebina.datumPoteka > Date.now() / 1000;
    } else {
      return false;
    }
  };
  
  var trenutniUporabnik = function() {
    if (jePrijavljen()) {
      var zeton = vrniZeton();
      var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
      return {
        email: koristnaVsebina.email
      };
    }
  };
  
  var jeAdmin = function() {
    if (jePrijavljen()) {
      var email = trenutniUporabnik().email;
      var adminEmail = /@admin.com/i;
      
      if (adminEmail.test(email)) {
        return true;
      }
      return false;
    }
  };
  
  var jeEventManager = function() {
    if (jePrijavljen()) {
      var email = trenutniUporabnik().email;
      var managerEmail = /@eventmanager.com/i;
      
      if (managerEmail.test(email)) {
        return true;
      }
      return false;
    }
  };

  return {
    shraniZeton: shraniZeton,
    vrniZeton: vrniZeton,
    registracija: registracija,
    prijava: prijava,
    odjava: odjava,
    jePrijavljen: jePrijavljen,
    trenutniUporabnik: trenutniUporabnik,
    jeAdmin: jeAdmin,
    jeEventManager: jeEventManager,
    spremeniGeslo: spremeniGeslo
  };
}

/* global straightAs */
straightAs
  .service('avtentikacija', avtentikacija);
var straightAsPodatki = function($http, avtentikacija) {
  var dodajVnosKoledar = function(podatki) {
    return $http.post("/api/vnosKoledar", podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var urediVnosKoledar = function(idVnos, podatki) {
    return $http.put("/api/koledar/" + idVnos, podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var odstraniVnosKoledar = function(id) {
     return $http.delete("/api/koledar/" + id, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var dodajVnosUrnik = function(podatki) {
    return $http.post("/api/vnosUrnik", podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var urediVnosUrnik = function(idVnos, podatki) {
    return $http.put("/api/urnik/" + idVnos, podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var odstraniVnosUrnik = function(id) {
    return $http.delete("/api/urnik/" + id, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var dodajVnosTodo = function(podatki) {
    return $http.post("/api/vnosTODO", podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var urediVnosTodo = function(idVnos, podatki) {
    return $http.put("/api/todo/" + idVnos, podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var odstraniVnosTodo = function(id) {
    return $http.delete("/api/todo/" + id, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
  };
  var pridobiVsePostaje = function(){
    return $http.get("/api/avtobusi");
  };
  
  var pridobiPodatkeOPrihodih = function(idPostaje){
    return $http.get("/api/avtobusi/" + idPostaje);  
  };

  var pridobiPodatkeORestavracijah = function(){
    return $http.get("/api/hrana");  
  };  
  
  
  return {
    dodajVnosKoledar: dodajVnosKoledar,
    odstraniVnosKoledar: odstraniVnosKoledar,
    urediVnosKoledar: urediVnosKoledar,
    dodajVnosUrnik: dodajVnosUrnik,
    urediVnosUrnik : urediVnosUrnik,
    odstraniVnosUrnik: odstraniVnosUrnik,
    dodajVnosTodo: dodajVnosTodo,
    urediVnosTodo: urediVnosTodo,
    odstraniVnosTodo: odstraniVnosTodo,
    pridobiVsePostaje: pridobiVsePostaje,
    pridobiPodatkeOPrihodih: pridobiPodatkeOPrihodih,
    pridobiPodatkeORestavracijah: pridobiPodatkeORestavracijah,      
  };
};

var straightAsKoledar = function($http, avtentikacija) {
  if (avtentikacija.jePrijavljen()) {
    return $http.get("/api/koledar", {
    	params: {email: avtentikacija.trenutniUporabnik().email}
    });
  }
};

  var straightAsUrnik = function($http, avtentikacija) {
    if (avtentikacija.jePrijavljen()) {
      return $http.get("/api/urnik", {
      	params: {email: avtentikacija.trenutniUporabnik().email}
      });
    }
  };
  
  var straightAsTodo = function($http, avtentikacija) {
   if (avtentikacija.jePrijavljen()) {
    return $http.get("/api/todo", {
    	params: {email: avtentikacija.trenutniUporabnik().email}
    });
  }
  };
  

// Dogodki
var straightAsDogodki = function($http) {
  var vrniDogodke = function() {
    return $http.get("/api/dogodki");
  };
  
  var dodajVnosDogodki = function(podatki) {
    return $http.post("/api/vnosDogodki", podatki);
  };
  return{
    vrniDogodke : vrniDogodke,
    dodajVnosDogodki : dodajVnosDogodki
  };
};

var straightAsObvestila = function($http) {
  var vrniObvestila = function() {
    return $http.get("/api/obvestila");
  };
  
  var dodajVnosObvestila = function(podatki) {
    return $http.post("/api/vnosObvestila", podatki);
  };
  return{
    vrniObvestila : vrniObvestila,
    dodajVnosObvestila : dodajVnosObvestila
  };
};

/* global straightAs */

straightAs
  .service('straightAsPodatki', straightAsPodatki)
  .service('straightAsKoledar', straightAsKoledar)
  .service('straightAsUrnik', straightAsUrnik)
  .service('straightAsTodo', straightAsTodo)
  .service('straightAsDogodki', straightAsDogodki)
  .service('straightAsObvestila', straightAsObvestila);
var navigacija = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/navigacija/navigacija.predloga.html',
      controller: 'navigacijaCtrl',
      controllerAs: 'navvm'
    };
};

/* global straightAs */
straightAs
    .directive('navigacija', navigacija);
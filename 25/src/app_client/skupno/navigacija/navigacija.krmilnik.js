function navigacijaCtrl($location, avtentikacija, $route, $uibModal) {
  var navvm = this;
  
  navvm.trenutnaLokacija = $location.path();
  
  navvm.jePrijavljen = avtentikacija.jePrijavljen();
    
  navvm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
  
  navvm.jeAdmin = avtentikacija.jeAdmin();
  
  navvm.jeEventManager = avtentikacija.jeEventManager();
  
  navvm.prikaziPojavnoOknoSpremembaGesla = function() {
      $uibModal.open({
        animation: true,
        templateUrl: '/spremembaGeslaModalnoOkno/spremembaGeslaModalnoOkno.pogled.html',
        controller: 'spremembaGeslaModalnoOkno',
        controllerAs: 'vm',
        resolve : {
          podrobnostiVnosa: function(){
            return {
              email : navvm.trenutniUporabnik.email
            }
          }
        }
      });
    }

  navvm.odjava = function() {
    avtentikacija.odjava();
    $location.path('/');
    $route.reload();
  };
}

/* global straightAs */
straightAs
	.controller('navigacijaCtrl', navigacijaCtrl);
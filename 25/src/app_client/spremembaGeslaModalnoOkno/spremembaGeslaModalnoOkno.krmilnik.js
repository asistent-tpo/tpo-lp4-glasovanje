function spremembaGeslaModalnoOkno($uibModalInstance, $http, avtentikacija) {
    var vm = this;
    
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal")
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posljiPodatke = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.oldPass || !vm.podatkiObrazca.repeatOldPass || !vm.podatkiObrazca.newPass) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        if (vm.podatkiObrazca.oldPass !== vm.podatkiObrazca.repeatOldPass) {
          vm.napakaNaObrazcu = "Gesli se ne ujemata!";
        } else {
          vm.spremeni(vm.podatkiObrazca);
          
        }
        return false;
      }
    };
    
    vm.spremeni = function(podatki) {
      avtentikacija.spremeniGeslo({
        "email" : vm.trenutniUporabnik.email,
        "oldPassword" : podatki.oldPass,
        "repeatOldPassword" : podatki.repeatOldPass,
        "newPassword" : podatki.newPass
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('spremembaGeslaModalnoOkno', spremembaGeslaModalnoOkno);
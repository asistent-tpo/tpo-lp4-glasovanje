/* global angular */
var straightAs = angular.module('straightAs', ['ngRoute', 'ui.bootstrap']);

function nastavitev($routeProvider,$locationProvider, $sceDelegateProvider) {
  $routeProvider
    .when('/', {
        templateUrl: 'homepage/homepage.pogled.html',
        controller: 'homepageCtrl',
        controllerAs: 'vm'
    })
    .when('/prijava', {
        templateUrl: '/avtentikacija/prijava/prijava.pogled.html',
        controller: 'prijavaCtrl',
        controllerAs: 'vm'
      })
    .when('/registracija', {
        templateUrl: '/avtentikacija/registracija/registracija.pogled.html',
        controller: 'registracijaCtrl',
        controllerAs: 'vm'
      })
    .when('/dogodki', {
      templateUrl: '/dogodki/dogodki.pogled.html',
      controller: 'dogodkiCtrl',
      controllerAs: 'vm'
    })
    .when('/dogodkiUpravljanje', {
        templateUrl: '/dogodkiUpravljanje/dogodkiUpravljanje.pogled.html',
        controller: 'dogodkiUpravljanjeCtrl',
        controllerAs: 'vm'
      })
    .when('/avtobusi', {
        templateUrl: '/avtobusi/avtobusi.pogled.html',
        controller: 'avtobusiCtrl',
        controllerAs: 'vm'
      })
    .when('/hrana', {
        templateUrl: '/hrana/hrana.pogled.html',
        controller: 'hranaCtrl',
        controllerAs: 'vm'
      })
      .when('/obvestila', {
        templateUrl: '/obvestila/obvestila.pogled.html',
        controller: 'obvestilaCtrl',
        controllerAs: 'vm'
      })
      .when('/admin', {
        templateUrl: '/admin/admin.pogled.html',
        controller: 'adminCtrl',
        controllerAs: 'vm'
      })
    .otherwise({redirectTo: '/'});
    $locationProvider.html5Mode(true);
    
    $sceDelegateProvider.resourceUrlWhitelist([
      'self'
    ]);
}

straightAs
  .config(['$routeProvider','$locationProvider', '$sceDelegateProvider', nastavitev]);
function koledarVnosModalnoOkno($uibModalInstance, $http, straightAsPodatki, avtentikacija) {
    var vm = this;
    
    vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal")
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posljiPodatkePOSTKoledar = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Name || !vm.podatkiObrazca.Date || !vm.podatkiObrazca.Duration || !vm.podatkiObrazca.Description) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.dodaj(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.dodaj = function(podatki) {
      straightAsPodatki.dodajVnosKoledar({
        "email" : vm.trenutniUporabnik.email,
        "Name" : podatki.Name,
        "Date" : podatki.Date,
        "Duration" : podatki.Duration,
        "Description" : podatki.Description
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('koledarVnosModalnoOkno', koledarVnosModalnoOkno);

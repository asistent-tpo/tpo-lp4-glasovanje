function koledarUrediModalnoOkno($uibModalInstance, $http, straightAsPodatki, podrobnostiVnosa) {
    var vm = this;

    vm.id = podrobnostiVnosa.idVnosa;

    vm.podatkiObrazca = {
        Name: podrobnostiVnosa.ime,
        Date : podrobnostiVnosa.datum,
        Duration: podrobnostiVnosa.trajanje,
        Description : podrobnostiVnosa.opis
    };
    
    vm.modalnoOkno = {
      preklici: function() {
        console.log("close the modal");
        $uibModalInstance.close();
      },
      
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    vm.posljiPodatkePUTKoledar = function () {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.Name || !vm.podatkiObrazca.Date || !vm.podatkiObrazca.Duration || !vm.podatkiObrazca.Description) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.uredi(vm.podatkiObrazca);
        return false;
      }
    };
    
    vm.uredi = function(podatki) {
      straightAsPodatki.urediVnosKoledar(vm.id, {
        "Name" : podatki.Name,
        "Date" : podatki.Date,
        "Duration" : podatki.Duration,
        "Description" : podatki.Description
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        }, function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju, poskusite znova!";
        } 
      );
    };
}

  /* global straightAs, $http */
straightAs
    .controller('koledarUrediModalnoOkno', koledarUrediModalnoOkno);
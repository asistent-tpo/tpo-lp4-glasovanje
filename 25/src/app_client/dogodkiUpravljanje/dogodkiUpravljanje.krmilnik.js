
function dogodkiUpravljanjeCtrl($uibModal, straightAsDogodki) {
    var vm = this;
    vm.naslov = "Event Manager";
    
     vm.prikaziPojavnoOknoDogodekVnos = function() {
     
      var primerekModalnegaOkna = $uibModal.open({
        animation: true,
        templateUrl: '/dogodekVnosModalnoOkno/dogodekVnosModalnoOkno.pogled.html',
        controller: 'dogodekVnosModalnoOkno',
        controllerAs: 'vm'
      });
      
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.data.dogodki.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        console.log(napaka);
      });
      
      
    };
    
    
    straightAsDogodki.vrniDogodke().then(function success(odgovor) {
        vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem vnosov.";
        vm.data = {
            dogodki : odgovor.data
        };
    },  function error(odgovor) {
            vm.sporocilo = "Prišlo je do napake!";
            console.log(odgovor.e);
    });
}
  
/* global straightAs */
straightAs
    .controller('dogodkiUpravljanjeCtrl', dogodkiUpravljanjeCtrl);
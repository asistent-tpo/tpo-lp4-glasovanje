require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var port = process.env.PORT || 3000;

var passport = require('passport');

require('./src/app_api/models/db');
require('./src/app_api/konfiguracija/passport');

//var indexRouter = require('./src/app_server/routes/index');
var indexApi = require('./src/app_api/routes/index');
var usersRouter = require('./src/app_server/routes/users');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'src', 'app_server', 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'src', 'public')));
app.use(express.static(path.join(__dirname, 'src', 'app_client')));
app.use(passport.initialize());

//app.use('/', indexRouter);
app.use('/api', indexApi);
app.use('/users', usersRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'src','app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "sporočilo": err.name + ": " + err.message
    });
  }
});

app.listen(port, function () {
  console.log('Strežnik je pognan na portu ' + port + '!');
});

module.exports = app;

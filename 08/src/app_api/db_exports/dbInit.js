//  TEST PASSWORDS
//  jv: rbPASS12345
//  ah: ahPASS12345
//  av: avPASS12345
//  rm: rmPASS12345
//
////  USERS
//

db.Users.save({
  "_id" : ObjectId("5c0d165a29324abcde4e2e89"),
  username: "robi_markac",
  name: "Robi",
  surname: "Markac",
  email: "rm@gmail.com",
  password : "cec94f6c2234192920e781adc64f24757f756eb59e39160e18c33777aa514f38509cb9b343057b4e69bb4e5ed9bc6f90cf9395d5b40433dd1faab561b597b3e3",
  salt : "32a850f0114dc2ef062e0887acfabb66",
  userType: "admin"
});
db.Users.save({
  "_id" : ObjectId("5c0d165a29324abcde4e2e8a"),
  username: "aleksandar_hristov",
  name: "Aleksandar",
  surname: "Hristov",
  email: "ah@gmail.com",
  password : "6819cbba1499a0a60e604711a676a7ce04ecb1ebaafc8b8ca32820bd5c9a85f4b7b8b732ff6bf20c689556f3db97aac7b14d03af1f7c7c4c2a0042170a67e510",
  salt : "d550bea24425283c43668a253cbb43ad",
  userType: "admin"
});
db.Users.save({
  "_id" : ObjectId("5c0d165a29324abcde4e2e8b"),
  identity: ObjectId("5c0d0e2329324abcde4e2e84"),
  username: "andrija_vučković",
  name: "Andrija",
  surname: "Vučković",
  email: "av@gmail.com",
  password : "c08c56840dbfd85028904afa47191273ec52860f32fbee631c542ee19e648ffc40b56a3eb1546ca39a41f89e56a10dcc65816e7942a0808cb5a920b157e64bc4",
  salt : "09e49d646b7ce35a5962a4f5fe3350fe",
  userType: "uprava"
});
db.Users.save({
  "_id" : ObjectId("5c0d165a29324abcde4e2e8c"),
  username: "jernej_vrhunc",
  name: "Jernej",
  surname: "Vrhunc",
  email: "jv@gmail.com",
  password : "72984d93835e8730ffa6ff544e99220bc584af89028655798c63886bcc5d43b6aa167655a95376165cac8bbedd1afb545245d1c2041066bf56f60370094098f3",
  salt : "03b926c2fc1c6275a845e3283389076a",
  userType: "uprava"
});

db.adminMessages.save({
  "_id" : ObjectId("5cd71433a5824922601e1a55"),
  message: "To je Verzija ena brttttt"
});




db.Todo.save({
    "_id" : ObjectId("5c3b5bc9cc55f80c1f4464d2"),
    owner: ObjectId("5c0d165a29324abcde4e2e89"),
    opis: "Generic inspirational description..."
});
db.Todo.save({
    "_id" : ObjectId("5c3b5bc9cc55f80c1f4464d3"),
    owner: ObjectId("5c0d165a29324abcde4e2e89"),
    opis: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
});
db.Todo.save({
    "_id" : ObjectId("5c3b5bc9cc55f80c1f4464d4"),
    owner: ObjectId("5c0d165a29324abcde4e2e89"),
    opis: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
});



db.Bus.save({
    "_id" : ObjectId("4c3b5bc9cc55f80c1f4464d1"),
    stationName: "Bavarski dvor",
    stationId: "1908"
});

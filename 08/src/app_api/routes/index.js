var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var authentication = jwt({
  secret: process.env.JWT_GESLO,
  userProperty: 'payload'
});

var ctrlUsers = require('../controllers/users');
var ctrlTodo = require('../controllers/todo');
var ctrlDbInit = require("../controllers/dbInit");
var ctrlAuthentication = require('../controllers/authentication');
var ctrlAdminMessages = require('../controllers/adminMessages');
var ctrlBuses = require('../controllers/buses');
var ctrlEvent = require('../controllers/event');
var ctrlCalendarEvent = require('../controllers/calendarEvent');
var ctrlUrnik = require('../controllers/urnik');
var ctrlRestavracija = require('../controllers/restavracije');

// avtentikacija
router.post('/registracija', ctrlAuthentication.registracija);
router.post('/prijava', ctrlAuthentication.prijava);

// users
router.get("/users", ctrlUsers.getAll);
router.get("/users/:userId", ctrlUsers.getById);
router.delete("/users/:userId", ctrlUsers.deleteById);
router.post("/users", ctrlUsers.addNew);
router.put("/users/:userId", ctrlUsers.updateObject);

// bus
router.get("/buses", ctrlBuses.getAll);
router.get("/buses/:busId", ctrlBuses.getById);

// admin sporocila
router.get("/adminMessage", ctrlAdminMessages.getAll);
router.post("/adminMessage", ctrlAdminMessages.addNew);
router.delete("/adminMessage", ctrlAdminMessages.deleteById);

//todoo
router.get("/todo", ctrlTodo.getAll);
router.get("/todo/:todoId", ctrlTodo.getById);
router.delete("/todo/:todoId", ctrlTodo.deleteById);
router.post("/todo", ctrlTodo.addNew);
router.put("/todo/:todoId", ctrlTodo.updateObject);

//event
router.get("/event", ctrlEvent.getAll);
router.get("/event/:eventId", ctrlEvent.getById);
router.delete("/event/:eventId", ctrlEvent.deleteById);
router.post("/event", ctrlEvent.addNew);
router.put("/event/:eventId", ctrlEvent.updateObject);


//calendar event
router.get("/calendar/event", ctrlCalendarEvent.getAll);
router.get("/calendar/event/:eventId", ctrlCalendarEvent.getById);
router.delete("/calendar/event/:eventId", ctrlCalendarEvent.deleteById);
router.post("/calendar/event", ctrlCalendarEvent.addNew);
router.put("/calendar/event/:eventId", ctrlCalendarEvent.updateObject);

//calendar event
router.get("/urnik", ctrlUrnik.getAll);
router.delete("/urnik/:urnikId", ctrlUrnik.deleteById);
router.post("/urnik", ctrlUrnik.addNew);
router.put("/urnik/:urnikId", ctrlUrnik.updateObject);

//restavracije
router.get("/restavracije", ctrlRestavracija.getAll);
router.get("/restavracije/:restavracijaId", ctrlRestavracija.getById);

// init
router.get("/db/init", ctrlDbInit.init);
router.get("/db/drop", ctrlDbInit.drop);



module.exports = router;
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var restavracijaShema = new mongoose.Schema({
    naziv: {type: String, required: true},
    naslov: {type: String},
    cena: {type: Number},
});

mongoose.model('Restavracija', restavracijaShema, 'Restavracije');
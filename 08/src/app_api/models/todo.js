var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var todoShema = new mongoose.Schema({
    opis: {type: String, required: true},
    owner: {type: ObjectId, required: true}
}, { versionKey: false });


mongoose.model("Todo", todoShema, "Todo");
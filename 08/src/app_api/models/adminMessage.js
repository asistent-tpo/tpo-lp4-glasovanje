var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var adminMessageShema = new mongoose.Schema({
  message: {type: String, required: true},
  title: {type: String, required: true},
  dateCreated: {type: Date, required: false, default: new Date()}

}, { versionKey: false });

mongoose.model("adminMessage", adminMessageShema, "adminMessages");
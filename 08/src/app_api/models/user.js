var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var userShema = new mongoose.Schema({
  username: {type: String, unique: true, required: true},
  name: {type: String, required: false},
  surname: {type: String, required: false},
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  salt: {type: String, required: true},
  userType: {type: String, required: false, default: "user"}
}, { versionKey: false });

//generiranje JWT
userShema.methods.generirajJwt = function() {
  var datumPoteka = new Date();
  datumPoteka.setDate(datumPoteka.getDate() + 7);
  
  return jwt.sign({
    _id: this._id,
    name: this.name,
    surname: this.surname,
    email: this.email,
    userType: this.userType,
    datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
  }, process.env.JWT_GESLO);
};

userShema.methods.nastaviGeslo = function(geslo) {
    console.log('Nastavi geslo');
    this.salt = crypto.randomBytes(16).toString('hex');
    this.password = crypto.pbkdf2Sync(geslo, this.salt, 1000, 64, 'sha512').toString('hex');
};

//preveri geslo tako da dešifrira shranjeno geslo
userShema.methods.preveriGeslo = function(geslo) {
    console.log("Preveri geslo");
    var password = crypto.pbkdf2Sync(geslo, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.password === password;
};

mongoose.model("User", userShema, "Users");
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var CalendarEventShema = new mongoose.Schema({
    name: {type: String, required: true},
    date: {type: String, required: true},
    duration: {type: Number, required: true},
    owner: {type: ObjectId, required: true},
    description: {type: String, required: true}
}, { versionKey: false });


mongoose.model("CalendarEvent", CalendarEventShema, "CalendarEvent");
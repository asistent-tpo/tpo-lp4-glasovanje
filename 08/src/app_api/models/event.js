var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var eventShema = new mongoose.Schema({
    name: {type: String, required: true},
    date: {type: Date, required: true},
    organizer: {type: String, required: true},
    description: {type: String, required: true}
}, { versionKey: false });


mongoose.model("Event", eventShema, "Event");
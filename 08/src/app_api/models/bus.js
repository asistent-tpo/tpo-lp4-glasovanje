var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var busSchema = new mongoose.Schema({
    name: {type: String, required: true},
    int_id: {type: Number, required: true},
    ref_id: {type: Array, required: true}
}, { versionKey: false });

mongoose.model("Bus", busSchema, "Buses");
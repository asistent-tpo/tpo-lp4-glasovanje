var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var urnikShema = new mongoose.Schema({
  title: {type: String, required: true},
  ura: {type: String, required: true},
  dan: {type: String, required: true},
  userId: {type: ObjectId, required: true}
}, { versionKey: false });


mongoose.model("Urnik", urnikShema, "Urnik");
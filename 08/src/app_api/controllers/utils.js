module.exports = {
    isValidDate: function(date) {
        return !isNaN(Date.parse(date));
    }
};
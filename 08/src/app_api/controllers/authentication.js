var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/user');
var User = mongoose.model('User');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

//implementacija metode za registracijo uporabnika TODO: ko se registrira nastavi privzeto vlogo!
module.exports.registracija = function(zahteva, odgovor) {
  if (!zahteva.body.email || 
  !zahteva.body.password || 
  !zahteva.body.username ||
  !zahteva.body.name ||
  !zahteva.body.surname ||
    !zahteva.body.userType

  ) {
    console.log(zahteva.body);
    vrniJsonOdgovor(odgovor, 400, {
      "message": "Missing data"
    });
  }
  
  var user = new User();
  var id = mongoose.Types.ObjectId();
  user._id = id;
  user.email = zahteva.body.email;
  user.username = zahteva.body.username;
  user.name = zahteva.body.name;
  user.surname = zahteva.body.surname;
  user.nastaviGeslo(zahteva.body.password);
  user.userType = zahteva.body.userType;
  console.log("iz apija: " + user);
  console.log("iz apija: " + zahteva.body.userType);

  console.log("New user:\n" + user + "\n");
  
  user.save(function(napaka) {
    if (napaka) {
     vrniJsonOdgovor(odgovor, 500, napaka);
    } else {
        vrniJsonOdgovor(odgovor, 200, {
            "zeton": user.generirajJwt()
        });
    }
  });
};

//implementacija metode za prijavo
module.exports.prijava = function(zahteva, odgovor) {
  if (!zahteva.body.email || !zahteva.body.password) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
  }
  console.log('Prisel v prijavo');
  console.log(zahteva.body);

  passport.authenticate('local', function(error, user, podatki) {
    if (error) {
      vrniJsonOdgovor(odgovor, 404, error);
      return;
    }
    if (user) {
      vrniJsonOdgovor(odgovor, 200, {
        "zeton": user.generirajJwt()
      });
    } else {
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
};

var me = module.exports;

var mongoose = require('mongoose');
var User = require('../models/user');
var User = mongoose.model('User');
var Todo = require('../models/todo');
var Todo = mongoose.model('Todo');

var execSync = require('child_process').execSync;
var cmdRestore = "mongorestore --db tpo src/app_api/db_exports/tpo/";
var cmdDrop = "mongo tpo --eval 'db.dropDatabase()'";

var options = {
  encoding: 'utf8'
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

me.init = function(req, res) {
  console.log("INIT");
  
  console.log(execSync(cmdRestore, options));
  
  vrniJsonOdgovor(res, 200, "Uspeh");
};

me.drop = function(req, res) {
  console.log("DROP");
  
  // console.log(execSync(cmdDrop, options));
    var db = mongoose.connection;
    if(!db) {
        mongoose.connect('mongodb://localhost/tpo');
        db = mongoose.connection;
    }
    db.dropDatabase(function (err, result) {
      if(err){
          console.log("Error : "+err);
          vrniJsonOdgovor(err, 500, "Napaka")
      }
      else
          vrniJsonOdgovor(res, 200, "Uspeh");
    });
};
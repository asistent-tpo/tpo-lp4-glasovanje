var me = module.exports;
var mongoose = require('mongoose');
var CalendarEvent = require('../models/calendarEvent');
var CalendarEvent = mongoose.model('CalendarEvent');
var util = require('./utils');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

// Basic CRUD
me.getAll = function(req, res) {
    if(req.params) { // && other checks like req.params.someId
        try {
            CalendarEvent
                .find()
                .exec(function(err, data) {
                    if(!data) {
                        console.log("Napaka: Not found");
                        vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    }
                    else if (err) {
                        console.log("Napaka:\n" + err.stack);
                        vrniJsonOdgovor(res, 500, { "message": "Internal error.", "error": err });
                    }
                    else {
                        // SUCCESS
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
        } catch(ex) { console.log("Evade server crash:\n" + ex); }
    }
    else {
        console.log("Napaka: Missing parameters");
        vrniJsonOdgovor(res, 400, { "message": "Missings parameters." });
    }
};

me.getById = function(req, res) {
    if(req.params) { // && other checks like req.params.someId
        try {
            CalendarEvent
                .findById(req.params.eventId)
                .exec(function(err, data) {
                    if(!data) {
                        console.log("Napaka: Not found");
                        vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    }
                    else if (err) {
                        console.log("Napaka:\n" + err.stack);
                        vrniJsonOdgovor(res, 500, { "message": "Internal error.", "error": err });
                    }
                    else {
                        // SUCCESS
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
        } catch(ex) { console.log("Evade server crash:\n" + ex); }
    }
    else {
        console.log("Napaka: Missing parameters");
        vrniJsonOdgovor(res, 400, { "message": "Missings parameters." });
    }
};

me.deleteById = function(req, res) {
    if(req.params) { // && other checks like req.params.someId
        try {
            CalendarEvent.findByIdAndRemove(req.params.eventId)
                .exec(function(err, data) {
                    if(!data) {
                        console.log("Napaka: Not found");
                        vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    }
                    else if (err) {
                        console.log("Napaka:\n" + err.stack);
                        vrniJsonOdgovor(res, 500, { "message": "Internal error.", "error": err });
                    }
                    else {
                        // SUCCESS
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
        } catch(ex) { console.log("Evade server crash:\n" + ex); }
    }
    else {
        console.log("Napaka: Missing parameters");
        vrniJsonOdgovor(res, 400, { "message": "Missings parameters." });
    }
};

me.addNew = function(req, res) {
    //console.log(req.body);
    var s = req.body;
    var newObject = {
        name: s.name,
        date: s.date,
        duration: s.duration,
        owner: s.owner,
        description: s.description
    };

    if(!util.isValidDate(newObject.date)){
        vrniJsonOdgovor(res,400,'Invalid date!')
    }

    CalendarEvent.create(newObject,
        function(err, data) {
            if(err) {
                vrniJsonOdgovor(res, 400, err);
            }
            else {
                vrniJsonOdgovor(res, 201, data);
            }
        }
    );
};

me.updateObject = function(req, res) {
    var ids = req.params.eventId;
    if (!ids) {
        vrniJsonOdgovor(res, 400, { "message": "Missing _id parameter."});
        return;
    }
    var s = req.body;

    CalendarEvent
        .findById(ids)
        .exec(
            function(err, data) {
                if (!data) {
                    vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    return;
                }
                else if (err) {
                    vrniJsonOdgovor(res, 500, err);
                    return;
                }
                // preveri ce je uporabnikov event
                if(data.owner == s.owner){
                    data.name= s.name;
                    data.date= s.date;
                    data.duration = s.duration;
                    data.owner= s.owner;
                    data.description= s.description;
                } else {
                    vrniJsonOdgovor(res, 403, 'User with id='+data.owner._id+' is not owner of this calendar event!');
                }

                if(!util.isValidDate(data.date)){
                    vrniJsonOdgovor(res,400,'Invalid date!')
                }

                data.save(function(err, data) {
                    if (err) {
                        vrniJsonOdgovor(res, 400, err);
                    }
                    else {
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
            }
        );
}
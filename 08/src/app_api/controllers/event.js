var me = module.exports;

var mongoose = require('mongoose');
var Event = require('../models/event');
var Event = mongoose.model('Event');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

// Basic CRUD
me.getAll = function(req, res) {
    if(req.params) { // && other checks like req.params.someId
        try {
            Event
                .find()
                .exec(function(err, data) {
                    if(!data) {
                        console.log("Napaka: Not found");
                        vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    }
                    else if (err) {
                        console.log("Napaka:\n" + err.stack);
                        vrniJsonOdgovor(res, 500, { "message": "Internal error.", "error": err });
                    }
                    else {
                        // SUCCESS
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
        } catch(ex) { console.log("Evade server crash:\n" + ex); }
    }
    else {
        console.log("Napaka: Missing parameters");
        vrniJsonOdgovor(res, 400, { "message": "Missings parameters." });
    }
};

me.getById = function(req, res) {
    if(req.params) { // && other checks like req.params.someId
        try {
            Event
                .findById(req.params.eventId)
                .exec(function(err, data) {
                    if(!data) {
                        console.log("Napaka: Not found");
                        vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    }
                    else if (err) {
                        console.log("Napaka:\n" + err.stack);
                        vrniJsonOdgovor(res, 500, { "message": "Internal error.", "error": err });
                    }
                    else {
                        // SUCCESS
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
        } catch(ex) { console.log("Evade server crash:\n" + ex); }
    }
    else {
        console.log("Napaka: Missing parameters");
        vrniJsonOdgovor(res, 400, { "message": "Missings parameters." });
    }
};

me.deleteById = function(req, res) {
    if(req.params) { // && other checks like req.params.someId
        try {
            Event
                .findByIdAndRemove(req.params.eventId)
                .exec(function(err, data) {
                    if(!data) {
                        console.log("Napaka: Not found");
                        vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    }
                    else if (err) {
                        console.log("Napaka:\n" + err.stack);
                        vrniJsonOdgovor(res, 500, { "message": "Internal error.", "error": err });
                    }
                    else {
                        // SUCCESS
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
        } catch(ex) { console.log("Evade server crash:\n" + ex); }
    }
    else {
        console.log("Napaka: Missing parameters");
        vrniJsonOdgovor(res, 400, { "message": "Missings parameters." });
    }
};

me.addNew = function(req, res) {
    //console.log(req.body);
    var s = req.body;
    var newObject = {
        name: s.name,
        date: s.date,
        organizer: s.organizer,
        description: s.description
    };
    Event.create(newObject,
        function(err, data) {
            if(err) {
                vrniJsonOdgovor(res, 400, err);
            }
            else {
                vrniJsonOdgovor(res, 201, data);
            }
        }
    );
};

me.updateObject = function(req, res) {
    var ids = req.params.eventId;
    if (!ids) {
        vrniJsonOdgovor(res, 400, { "message": "Missing _id parameter."});
        return;
    }
    var s = req.body;

    Event
        .findById(ids)
        .exec(
            function(err, data) {
                if (!data) {
                    vrniJsonOdgovor(res, 404, { "message": "Data not found" });
                    return;
                }
                else if (err) {
                    vrniJsonOdgovor(res, 500, err);
                    return;
                }

                data.name= s.name;
                data.date= s.date;
                data.organizer= s.organizer;
                data.description= s.description;

                data.save(function(err, data) {
                    if (err) {
                        vrniJsonOdgovor(res, 400, err);
                    }
                    else {
                        vrniJsonOdgovor(res, 200, data);
                    }
                });
            }
        );
}
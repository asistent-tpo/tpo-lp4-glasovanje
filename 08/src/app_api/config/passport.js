var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');


passport.use(new LokalnaStrategija({
    usernameField: 'email',
    passwordField: 'password'
  }, 
  function(email, password, koncano) {
    console.log('prisel v passport preverjanje');
    console.log(email);
    console.log(password);
      User.findOne(
      {
        email: email
      },
      function(napaka, user) {
        console.log(user);
        if (napaka) {
            console.log('Napaka pri pridobivanju uporabnika');
            return koncano(napaka);
        }
        if (!user) {
          return koncano(null, false, {
            sporocilo: 'Napačno uporabniško ime'
          });
        }
        if (!user.preveriGeslo(password)) {
          return koncano(null, false, {
            sporocilo: 'Napačno geslo'
          });
        }
        else {
            return koncano(null, user);
        }
      }
    );
  }
));
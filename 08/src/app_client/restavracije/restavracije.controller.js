(function() {
    function restavracijeCtrl($scope, $rootScope, $uibModal, aggAppRestavracije) {
        var vm = this;

        vm.lat = 46.04987;
        vm.long = 14.46889;

        vm.data = [];

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(setPosition);
        } else {
            alert("Geolocation is now supported in your browser. Defualt location set to Fri univeristy");
        }


        aggAppRestavracije.getRestavracije().then(
            function success(res) {
                // V res.data so shranjene vse restavracije iz baze
                // V vm.lat in vm.long je shranjen current position
                for(i in res.data) {
                    vm.data.push(res.data[i]);
                }
            },
            function error(err) {
                console.log(err);
            }
        );
        
        function setPosition(position) {
            // Get currecnt position
            //vm.lat = position.coords.latitude;
            //vm.long = position.coords.longitude;
        }
    }

    restavracijeCtrl.$inject = ['$scope', '$rootScope', '$uibModal', 'aggAppRestavracije'];

    /* global angular */
    angular
        .module('aggApp')
        .controller('restavracijeCtrl', restavracijeCtrl);
})();
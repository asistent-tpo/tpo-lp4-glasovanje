(function(){
        function dogodkiCtrl($scope,$rootScope,$uibModal, aggAppEvent,auth,$location){
          var vm = this;
          if(!auth.isLoggedIn()) {
            $location.path("/");
          } else {
            if(auth.currentUser().userType !== 'uprava') {
              $location.path("/");
            }
          }
            aggAppEvent.getEvents().then(
                function success(res){
                    vm.dogodki=res.data;
                    console.log(vm.dogodki)
                }
            );

            vm.prikaziPojavnoOknoObrazca= function(){
                $uibModal.open({
                    templateUrl:'/modalnaOkna/addEvent/addEvent.controller.html',
                    controller: 'addEventCtrl',
                    controllerAs: 'vm'
                }).result.then(function(){
                    aggAppEvent.getEvents().then(
                        function success(res){
                            vm.dogodki=res.data;
                            console.log(vm.dogodki)
                        }
                    );
                }).catch(function (resp) {
                    aggAppEvent.getEvents().then(
                        function success(res){
                            vm.dogodki=res.data;
                            console.log(vm.dogodki)
                        }
                    );
                    if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1) throw resp;
                    console.log("cancel");
                });
            };
        }

        dogodkiCtrl.$inject = ['$scope','$rootScope','$uibModal','aggAppEvent','auth','$location'];

        /* global angular */
        angular
            .module('aggApp')
            .controller('dogodkiCtrl', dogodkiCtrl);
    }
)();
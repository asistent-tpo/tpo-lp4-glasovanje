(function(){
    function systemMessagesCtrl($scope,$rootScope,$uibModal, aggAppAdminMessages){
      var vm=this;
      vm.objave=[];


      aggAppAdminMessages.getAdminMessages().then(
        function success(res) {
          vm.objave = res.data;
          for (var i = 0; i<vm.objave.length; i++) {
            vm.objave[i].dateCreated = new Date(vm.objave[i].dateCreated).toLocaleDateString();
          }

        }
      );
    }
    systemMessagesCtrl.$inject = ['$scope','$rootScope','$uibModal', 'aggAppAdminMessages'];

    /* global angular */
    angular
      .module('aggApp')
      .controller('systemMessagesCtrl', systemMessagesCtrl);
  }
)();
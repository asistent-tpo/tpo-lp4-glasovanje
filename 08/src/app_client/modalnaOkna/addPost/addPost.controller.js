(function() {
  function addPostCtrl($uibModalInstance, aggAppAdminMessages) {
    var vm = this;
    vm.suc="";
    vm.podatki = "";
    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
    };
    vm.podatkiObrazca = {
      title: "",
      message : ""
    };

    vm.posiljanjePodatkov = function() {
        console.log("eeeeee");
        console.log(vm.podatkiObrazca);

      aggAppAdminMessages.addAdminMessage(vm.podatkiObrazca).then(
        function success(res) {
          console.log(res);
          vm.suc="success";
        },
        function error(er) {
          console.log(er);
          vm.suc="error";
        }
      );
      return false;
    };
  }
  addPostCtrl.$inject = ['$uibModalInstance', 'aggAppAdminMessages'];

  /* global angular */
  angular
    .module('aggApp')
    .controller('addPostCtrl', addPostCtrl);
})();
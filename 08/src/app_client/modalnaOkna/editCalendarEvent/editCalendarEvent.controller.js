(function() {
    function editCalendarEventCtrl($uibModalInstance, aggAppCalendarEvent,calendarEvent) {
        var vm = this;
        vm.modalTitle = "Event";
        // enum ["Event"=1,"Edit"=2,"Delete"=3, "Finished"=4] za ustrezen prikaz
        vm.modalType = 1;

        vm.finishMessage = "";

        vm.podatkiObrazca = calendarEvent;
        vm.podatkiObrazca.date = new Date(calendarEvent.date);
        // nastavitve koledarja
        vm.options = {
            minDate: new Date(),
            showWeeks: true
        };
        var tmp = new Date();
        tmp.setHours(calendarEvent.date.getHours());
        tmp.setMinutes(calendarEvent.date.getMinutes());
        vm.mytime = tmp;

        vm.podatki = "";
        vm.modalnoOkno = {
            preklici: function() {
                $uibModalInstance.close();
            },
            edit: function () {
                vm.modalTitle = "Edit Event";
                vm.modalType = 2;
            },
            delete: function () {
                vm.modalTitle = "Delete Event";
                vm.modalType = 3
            }
        };

        vm.deleteCalendarEvent = function() {
          aggAppCalendarEvent.deleteCalendarEvent(vm.podatkiObrazca._id).then(
                function success(res) {
                    vm.finishMessage = "Successfully deleted calendar event "+vm.podatkiObrazca.name;
                    vm.modalType = 4;
                },
                function error(er) {
                    vm.finishMessage = er;
                    vm.modalType = 4;
                });
        };

        vm.posiljanjePodatkov = function() {
            vm.podatkiObrazca.date.setHours(vm.mytime.getHours());
            vm.podatkiObrazca.date.setMinutes(vm.mytime.getMinutes());
            vm.podatkiObrazca.date = vm.podatkiObrazca.date.toUTCString();
            aggAppCalendarEvent.editCalendarEvent(
                vm.podatkiObrazca._id,
                vm.podatkiObrazca.name,
                vm.podatkiObrazca.date,
                vm.podatkiObrazca.duration,
                vm.podatkiObrazca.description).then(
                function success(res) {
                    vm.finishMessage = "Successfully edited calendar event "+vm.podatkiObrazca.name;
                    vm.modalType = 4;
                },
                function error(er) {
                    vm.finishMessage = er;
                    vm.modalType = 4;
                }
            );
            return false;
        };
    }
    editCalendarEventCtrl.$inject = ['$uibModalInstance', 'aggAppCalendarEvent','calendarEvent'];

    /* global angular */
    angular
        .module('aggApp')
        .controller('editCalendarEventCtrl', editCalendarEventCtrl);
})();
(function() {
  function addUrnikCtrl($uibModalInstance,aggAppUrnik, urnikData) {
    var vm = this;
    vm.hashDan = {
      "1": "Monday",
      "2": "Tuesday",
      "3": "Wednesday",
      "4": "Thursday",
      "5": "Friday"
    };
    vm.hashUra = {
      "7": "07:00",
      "8": "08:00",
      "9": "09:00",
      "10": "10:00",
      "11": "11:00",
      "12": "12:00",
      "13": "13:00",
      "14": "14:00",
      "15": "15:00",
      "16": "16:00",
      "17": "17:00",
      "18": "18:00",
      "19": "19:00"
    };

    vm.urnikData = urnikData;
    console.log(urnikData);

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
    };

    vm.podatkiObrazca = {
      userId: vm.urnikData.userId,
      title: "",
      dan: vm.urnikData.dan,
      ura: vm.urnikData.ura
    };

    vm.posiljanjePodatkov = function() {
      console.log("eeeeee");
      console.log(vm.podatkiObrazca);

      aggAppUrnik.addUrnik(vm.podatkiObrazca.title,vm.podatkiObrazca.ura, vm.podatkiObrazca.dan, vm.podatkiObrazca.userId).then(
        function success(res) {
          console.log(res);
          vm.suc="success";
        },
        function error(er) {
          console.log(er);
          vm.suc="error";
        }
      );
      return false;
    };

    vm.Deletaj = function () {
      console.log("OOOOOOOOOOOJ");
      aggAppUrnik.deleteUrnik(vm.urnikData.idUrnika).then(
        function success(res) {
          console.log(res);
          vm.suc="success";
          var tabela = document.getElementById('tabelaUrnik');
          tabela.rows[parseInt(vm.urnikData.ura)-6].children[parseInt(vm.urnikData.dan)].innerHTML = "";
        },
        function error(er) {
          console.log(er);
          vm.suc="error";
        }
      );
      return false;
    }
  }
  addUrnikCtrl.$inject = ['$uibModalInstance','aggAppUrnik','urnikData'];

  /* global angular */
  angular
    .module('aggApp')
    .controller('addUrnikCtrl', addUrnikCtrl);
})();
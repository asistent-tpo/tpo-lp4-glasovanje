(function() {
    function addCalendarEventCtrl($uibModalInstance, aggAppCalendarEvent) {
        var vm = this;
        // enum ["Add"=1,"Finished"=2]
        vm.modalType = 1;
        vm.finishMessage = "";

        vm.podatkiObrazca = {
            date: new Date(),
            name: "",
            duration: 0,
            description: ""
        };
        // nastavitve koledarja
        vm.options = {
            minDate: new Date(),
            showWeeks: true
        };

        vm.mytime = new Date();

        vm.podatki = "";
        vm.modalnoOkno = {
            preklici: function() {
                $uibModalInstance.close();
            }
        };


        vm.posiljanjePodatkov = function() {
            vm.podatkiObrazca.date.setHours(vm.mytime.getHours());
            vm.podatkiObrazca.date.setMinutes(vm.mytime.getMinutes());
            // vm.podatkiObrazca.date = vm.podatkiObrazca.date.toUTCString();
            aggAppCalendarEvent.addCalendarEvent(
                vm.podatkiObrazca.name,
                vm.podatkiObrazca.date,
                vm.podatkiObrazca.duration,
                vm.podatkiObrazca.description).then(
                function success(res) {
                    vm.finishMessage = "Successfully added calendar event "+vm.podatkiObrazca.name;
                    vm.modalType = 2;
                },
                function error(er) {
                    vm.finishMessage =er;
                    vm.modalType = 2;
                }
            );
            return false;
        };
    }
    addCalendarEventCtrl.$inject = ['$uibModalInstance', 'aggAppCalendarEvent'];

    /* global angular */
    angular
        .module('aggApp')
        .controller('addCalendarEventCtrl', addCalendarEventCtrl);
})();
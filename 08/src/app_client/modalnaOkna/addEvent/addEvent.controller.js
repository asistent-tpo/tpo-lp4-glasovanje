(function() {
    function addEventCtrl($uibModalInstance, aggAppEvent) {
        var vm = this;
        vm.response = 'a';
        vm.podatki = "";
        vm.modalnoOkno = {
            preklici: function() {
                $uibModalInstance.close();
            },
        };

        vm.posiljanjePodatkov = function() {
            aggAppEvent.addEvent(vm.podatkiObrazca.name, vm.podatkiObrazca.date, vm.podatkiObrazca.organizer,vm.podatkiObrazca.description).then(
                function success(res) {
                    console.log(res);
                    vm.response = 'success';
                },
                function error(er) {
                    console.log(er);
                    vm.response = 'error';
                }
            );
            return false;
        };

    }
    addEventCtrl.$inject = ['$uibModalInstance', 'aggAppEvent'];

    /* global angular */
    angular
        .module('aggApp')
        .controller('addEventCtrl', addEventCtrl);
})();
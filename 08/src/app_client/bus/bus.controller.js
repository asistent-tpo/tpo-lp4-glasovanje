(function() {
    function busCtrl($scope,$rootScope,$uibModal,aggAppBuses) {
        var vm = this;

        vm.najdiIdPostaje = function() {

            // Variables for:
            //      - EmptyMessage
            //      - idPostaj
            //      - routes
            //      - selectedDropdownItems
            //      - allRoutesForLine
            vm.emptyMessage = "";
            vm.idPostaj = [];
            vm.routes = [];
            vm.selectedDropdownItems = {};
            vm.allRoutesForLine = [];
            vm.selectRoute = "";
            vm.noData = "";

            // Get the station name that user typed in
            var stationName = vm.postaja;

            aggAppBuses.getBuses().then(
                function success(res) {
                    // In res data there are all Bus Stations.

                    // Iterate through station and found one that has same
                    // name as the one that user typed in
                    for(var station in res.data) {
                        if(res.data[station].name.toLowerCase() === stationName.toLowerCase()) {
                            vm.idPostaj.push(res.data[station].int_id);
                        }
                    }

                    // If there are no hits for typed in station show empty message, else leave it empty
                    if(vm.idPostaj.length === 0) {
                        vm.routesInfo = [];
                        vm.emptyMessage = "No bus station with this name found!";
                        vm.selectRoute = "";
                    } else {
                        vm.routesInfo = [];
                        vm.emptyMessage = "";
                        vm.selectRoute = "Please select your bus route."

                        // Get routes for this IDs
                        for(var id in vm.idPostaj) {
                            getRoutes(vm.idPostaj[id]);
                        }
                    }
                },
                function error(err) {
                    console.log(err);
                }
            );
        };

        function getRoutes(stationId) {
            var http_request;
            http_request = new XMLHttpRequest();
            http_request.onreadystatechange = function () {
                if (http_request.readyState === 4) {

                    // GET All routes out of json format into one array(exportedRoutes)
                    var data = JSON.parse(http_request.response);
                    var exportedRoutes = data.data;

                    // Union of all routes
                    vm.routes = union_arrays(vm.routes, exportedRoutes);
                }
            };
            http_request.open("GET", "https://data.lpp.si/stations/getRoutesOnStation?station_int_id=" + stationId);
            http_request.withCredentials = true;
            http_request.setRequestHeader("Content-Type", "application/json");
            http_request.send({ 'request': "authentication token" });

        }

        function union_arrays (x, y) {
            var obj = {};
            for (var i = x.length-1; i >= 0; -- i)
                obj[x[i]] = x[i];
            for (var i = y.length-1; i >= 0; -- i)
                obj[y[i]] = y[i];
            var res = []
            for (var k in obj) {
                if (obj.hasOwnProperty(k))  // <-- optional
                    res.push(obj[k]);
            }
            return res;
        }

        vm.loadOptions = function () {
            // Load options in dropdown selection
            if(vm.routes.length !== 0) {
                vm.selectedDropdownItems = [vm.routes];
            }
        };

        vm.prikaziOdhode = function() {

            // Pridobimo izbrano linijo
            var routeNum = vm.selected;

            // Pridobi vsa imena in id-je linij s tem routeNumber-jem
            getAllRoutes(routeNum);


            vm.selectRoute = "";
        };

        function getAllRoutes(routeNumber) {
            var http_request;
            http_request = new XMLHttpRequest();
            http_request.onreadystatechange = function () {
                if (http_request.readyState === 4) {

                    // GET All routes for this routeNumber
                    var data = JSON.parse(http_request.response);
                    var routesForRouteNumber = data.data;

                    // GET all information about arrivals for this route on all recognized stations
                    getData(routesForRouteNumber, vm.idPostaj);
                }
            };
            http_request.open("GET", "https://data.lpp.si/routes/getRoutes?route_name=" + routeNumber);
            http_request.withCredentials = true;
            http_request.setRequestHeader("Content-Type", "application/json");
            http_request.send({ 'request': "authentication token" });
        }

        function getData(routes, postaje) {

            // Saves data for
            vm.routesInfo = [];

            for(var postaja in postaje) {
                for(var route in routes) {

                    // Route id and name for route we are looking into
                    var routeId = routes[route].id;
                    var routeName = routes[route].parent_name;

                    getRouteData(postaje[postaja], routeId, routeName);

                    vm.noData = "No data for arrivals on this station!";
                }
            }
        }

        function getRouteData(postajaId, routeId, routeName) {
            var http_request;
            http_request = new XMLHttpRequest();
            http_request.onreadystatechange = function () {

                if (http_request.readyState === 4) {
                    if(http_request.status === 200) {

                        vm.noData = "";

                        // GET All routes for this routeNumber
                        var data = JSON.parse(http_request.response);
                        var dataExtracted = data.data;

                        vm.routesInfo.push({
                            name: routeName,
                            times: dataExtracted
                        });
                    }
                }
            };
            http_request.open("GET", "https://data.lpp.si/timetables/getArrivalsOnStation?route_id=" + routeId + "&station_int_id=" + postajaId );
            http_request.withCredentials = true;
            http_request.setRequestHeader("Content-Type", "application/json");
            http_request.send({ 'request': "authentication token" });
        }
    }

    busCtrl.$inject = ['$scope','$rootScope','$uibModal','aggAppBuses'];

    /* global angular */
    angular
        .module('aggApp')
        .controller('busCtrl', busCtrl);
}
)();
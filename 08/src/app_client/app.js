(function(){
  function config($routeProvider, $locationProvider, $sceDelegateProvider){
    $routeProvider
      .when('/', {
        templateUrl: "home/home.controller.html",
        controller: "homeCtrl",
        controllerAs: "vm"

      })
        .when('/login', {
            templateUrl:"login/login.controller.html",
            controller:"loginCtrl",
            controllerAs:"vm",

        })
        .when('/register',{
            templateUrl:"register/register.controller.html",
            controller: "registerCtrl",
            controllerAs:"vm"
        })
        .when('/editProfile/:id',{
            templateUrl:"editProfile/editProfile.controller.html",
            controller: "editProfileCtrl",
            controllerAs:"vm"
        })
        .when('/bus', {
            templateUrl:"bus/bus.controller.html",
            controller: "busCtrl",
            controllerAs:"vm"
        })
        .when('/admin',{
          templateUrl:"admin/admin.controller.html",
          controller: "adminCtrl",
          controllerAs:"vm",

        })
        .when('/dogodki',{
            templateUrl:"dogodki/dogodki.controller.html",
            controller: "dogodkiCtrl",
            controllerAs:"vm"
        })
        .when('/systemMessages',{
          templateUrl:"systemMessages/systemMessages.controller.html",
          controller: "systemMessagesCtrl",
          controllerAs:"vm"
        })
        .when('/events',{
            templateUrl:"events/events.controller.html",
            controller: "eventsCtrl",
            controllerAs:"vm"
        })
        .when('/restavracije', {
            templateUrl:"restavracije/restavracije.controller.html",
            controller: "restavracijeCtrl",
            controllerAs:"vm"
        })
      .otherwise({ redirectTo: "/" });
      
    $locationProvider.html5Mode(true);
     $sceDelegateProvider.resourceUrlWhitelist([
      'self', 'https://www.youtube.com/**', 'https://www.mixcloud.com/**', 'https://www.soundcloud.com/**', 'https://w.soundcloud.com/**', 'https://www.google.com/maps/**'
    ]);
  }
   /* global angular */
  angular
    .module('aggApp', ['ngRoute', 'ngSanitize' , 'ui.bootstrap', 'angular-fullcalendar','720kb.datepicker'])
    .config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', config]);

})();
(function(){
  function editProfileCtrl($rootScope,$routeParams,$location,$route, aggAppUsers,auth,$window){
    var vm = this;
    vm.rootUser = auth.currentUser();
    vm.userID = vm.rootUser._id;

    aggAppUsers.getUserByID(vm.userID).then(
      function success(res){
        vm.user = res.data;
      },
      function error(er){
        console.error(er);
      } 
      );
      
            
    vm.editP = function() {
      
      //var regUsr = new RegExp("^(?=.{4,20}$)[a-žA-Ž0-9-(\ )]+$");
      //username = med 4 in 32 znakov, zgoraj nasteti znaki
      //var regEm = new RegExp("^(?![.])(?!.*[.]{2})[a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+(?<![.])@(?![-])[a-zA-Z0-9-]+(?<![-])\.(?![.])(?!.*[.]{2})[a-zA-Z0-9.]+(?<![.])$");
      //email = (. ni na zaceetku in koncu, in se ne sme podvajat znotraj gor nastetih znakov v []) @ (- ni na zactku in koncu) . (. ni na zactku, koncu in se ne podvaja)
      //full name = crke, presledki, -
      vm.usernameRes = 'false';
      //vm.emailRes = 'false';
      vm.passRes = 'false';
      vm.pass2Res = 'false';
      vm.nameRes = 'false';
      vm.surnameRes = 'false';
      vm.response = 'false';
      
    vm.noviUser = {
      //userId: vm.userID,
      username: vm.novUsername, //sssss
      name: vm.novName,
      surname: vm.novSurname,
      email: vm.rootUser.email,
      password: vm.novPassword
    };

    var checkEr=0;
    if(vm.novUsername && vm.novName && vm.novSurname && vm.noviUser.email && vm.noviUser.password){
      /*if(!regUsr.test(vm.novUsername)){
        vm.usernameRes = 'true';
        checkEr=1;
      }
      if(!regEm.test(vm.noviIdentity.email)){
        vm.emailRes = 'true';
        checkEr=1;
      }
      if(!regPass.test(vm.noviIdentity.password)){
        vm.passRes = 'true';
        checkEr=1;
      } else {
        if(vm.noviUser.password != vm.novPassword2){
          vm.pass2Res = 'true';
          checkEr=1;
        }
      } */
     /*  if(!regFN.test(vm.novName)){
        vm.nameRes = 'true';
        checkEr=1;
      }
      if(!regFN.test(vm.novSurname)){
        vm.surnameRes = 'true';
        checkEr=1;
      } */

      if(vm.noviUser.password != vm.novPassword2){
        vm.pass2Res = 'true';
        checkEr=1;
      }

        if(checkEr===0){
        aggAppUsers.updateUser(vm.userID, vm.noviUser).then(
        function success(res) {
            aggAppUsers.getUserByID(vm.userID).then(
              function success (ress) {
                auth.logout();
                auth.login(vm.noviUser);
                setTimeout(function() {
                  $location.path('/');
                  $route.reload();
                },2000);

               // $window.location.reload();
                //$location.path("/");
                //$route.reload();
              },
              function error (errr) {
                vm.response = 'errorAdd';
              }
            );

            vm.response = 'success';
            console.log(res);
          },
          function error(err) {
            console.error(err);
            vm.response = 'errorAdd'
          }
        );
      }
    } else {
      vm.response= 'brezp'
    }
      
    
      
    }
   
  }
  
  editProfileCtrl.$inject = ['$rootScope','$routeParams','$location','$route','aggAppUsers','auth','$window'];
  
  /* global angular */
  angular
    .module('aggApp')
    .controller('editProfileCtrl', editProfileCtrl);
})();
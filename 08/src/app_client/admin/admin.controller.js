(function(){
  function adminCtrl($rootScope,$location,$uibModal, aggAppAdminMessages, auth){

    var vm = this;
    if(!auth.isLoggedIn()) {
      $location.path("/");
    } else {
      if(auth.currentUser().userType !== 'admin') {
        $location.path("/");
      }
    }

    vm.getMessages = function() {
      aggAppAdminMessages.getAdminMessages().then(
        function success(res) {
          console.log(res.data);
        },
        function error(er) {
          console.error(er);
        }
      );

    };

    vm.prikaziPojavnoOknoObrazca = function(){
      $uibModal.open({
        templateUrl:'/modalnaOkna/addPost/addPost.controller.html',
        controller: 'addPostCtrl',
        controllerAs: 'vm'
      }).result.catch(function (resp) {
        if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1) throw resp;
      });
    };

    vm.dodajAdminaAliReferenta = function() {
      // TA FUNKCIJA SE NE UPORALJA SAM SE MI JENEDA ZBRISAAAAAAAAAAAAAAAT
      $rootScope.adminJePoslau = true;
      $location.path("/register");
    }
  }
  
  adminCtrl.$inject = ['$rootScope','$location','$uibModal','aggAppAdminMessages','auth'];
  
  /* global angular */
  angular
    .module('aggApp')
    .controller('adminCtrl', adminCtrl);
}
)();
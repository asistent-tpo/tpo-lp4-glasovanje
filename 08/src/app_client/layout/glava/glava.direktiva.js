(function() {
  var glava = function() {
    return {
      restrict: 'EA',
      templateUrl: '/layout/glava/glava.controller.html',
      controller: 'glavaCtrl',
      controllerAs: 'vma'

    };
  };

  /* global angular */
  angular
    .module("aggApp")
    .directive("glava", glava);
})();
(function() {

    var glavaCtrl = function ($rootScope, $location, $route, auth) {

        var vma = this;
        vma.currentUser = auth.currentUser();
        console.log(vma.currentUser);

        vma.logout = function () {
            //$rootScope.rootUser = null;
            auth.logout();
            $location.path("/");
            $route.reload();
        };

        vma.profileClick = function () {
          if(vma.currentUser.userType == 'admin') {
              $location.path("/admin");

          } else if(vma.currentUser.userType =='uprava') {
              $location.path("/dogodki");

          }
        };

        /* return {
            restrict: "EA",
            templateUrl: "/layout/glava/glava.controller.html"
        }; */
    };

    glavaCtrl.$inject = ['$rootScope','$location','$route','auth'];

    /* global angular */
    angular
        .module("aggApp")
        .controller("glavaCtrl", glavaCtrl);
})();
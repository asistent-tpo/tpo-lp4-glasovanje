(function(){
        function homeCtrl($scope,$rootScope,$uibModal, aggAppTodo,auth, aggAppCalendarEvent,aggAppUrnik){
            var vm=this;
            vm.todoList=[];
            vm.events = [];
            vm.gumb=0; //1=brisi
            vm.trenId=0;
            var tempNots=[];
            vm.description="";
            vm.descriptionEd="";
            vm.rootUser = auth.currentUser();
            //console.log("us "+vm.rootUser._id);
            vm.response = 'a';
            vm.response2 = 'a';


            var getEvents = function (){
                aggAppCalendarEvent.getCalendarEvents().then(
                    function success(res){
                        vm.events = [];
                        var tmp = res.data;
                        for(var i=0; i<tmp.length; i++) {
                            if(tmp[i].owner == auth.currentUser()._id){
                                vm.events.push(tmp[i])
                            }
                        }
                    }
                );
            };

            getEvents();

        if(vm.rootUser){
            aggAppTodo.getTodo().then(
                function success(res) {
                    tempNots=res.data;
                    for(var i=0; i<tempNots.length; i++){
                        if(tempNots[i].owner == vm.rootUser._id){
                            vm.todoList.push(tempNots[i]);
                        }
                    }
                }
            );
        }

        vm.doubleClick = function(dcId){
            console.log("duble");
            vm.trenId=dcId;
            $('#myModal3').modal('show');
        };

            vm.gumbBrisi = function () {
                console.log(vm.trenId);
                if(vm.gumb==0){
                    vm.gumb=1;
                } else {
                    vm.gumb=0;
                }
            };

            vm.izbrisiTodo=function () {
                aggAppTodo.deleteTodo(vm.trenId).then(
                    function success(res) {
                        console.log(res);
                        vm.response2 = 'success';
                        for(var i=0; i<vm.todoList.length; i++){
                            if(vm.todoList[i]._id == res.data._id){
                                vm.todoList.splice(i,1);
                            }
                        }
                    },
                function error(err) {
                    vm.response2 = 'error'
                }
                );
            };

            vm.dodajTodo=function () {
                console.log(vm.description);
                aggAppTodo.addTodo(vm.description, vm.rootUser._id).then(
                  function success(res) {
                      console.log(res);
                      vm.response = 'success';
                      // dodaj v trenutni view za primer ce kline x in ni preusmeritve
                      vm.todoList.push({
                          "opis" : vm.description,
                          "owner" : vm.rootUser._id,
                          "_id" : res.data._id
                      })
                  },
                    function error(e){
                        vm.response = 'error';
                        console.log(e);
                    }
                );
            };

            vm.editTodo = function () {
                aggAppTodo.editTodo(vm.trenId, vm.descriptionEd, vm.rootUser._id).then(
                    function success(res) {
                        console.log(res);
                        for(var i=0; i<vm.todoList.length; i++){
                            if(vm.todoList[i]._id == res.data._id){
                                vm.todoList[i].opis=res.data.opis;
                            }
                        }
                        vm.response3 = 'success';
                    },
                    function error(err) {
                        vm.response3 = 'error';
                        console.log(err);
                    }
                );
            };

        vm.conAdd = function(){
            vm.response = 'a';
            vm.response3 = 'a';
            vm.description="";
            $('#myModal3').modal('hide');
        };
        vm.conDel = function(){
            vm.response2 = 'a';
            $('#myModal').modal('hide');
        };

        // ****** Calendar *********
            /*
            * Calendar event object:
            * email: string
            * name: string
            * date: Date
            * duration: number
            * description: String
            * */

            alertOnClick = function(event, jsEvent, view) {
                alert('Clicked ' + event.name);
            };

            vm.calendarOptions = {
                eventClick: function(event){
                    $uibModal.open({
                        templateUrl:'/modalnaOkna/editCalendarEvent/editCalendarEvent.controller.html',
                        controller: 'editCalendarEventCtrl',
                        controllerAs: 'vm',
                        backdrop: false,
                        resolve: {
                            calendarEvent: function () {
                                return event
                            }
                        }
                    }).closed.then(function(){
                        getEvents();
                    });
                }, // Event callback fn
                customButtons: {
                    addCalendarEvent: {
                        text: 'Add Event',
                        click: function(){
                            $uibModal.open({
                                templateUrl:'/modalnaOkna/addCalendarEvent/addCalendarEvent.controller.html',
                                controller: 'addCalendarEventCtrl',
                                controllerAs: 'vm',
                                backdrop: false
                            }).closed.then(function(){
                                getEvents();
                            });
                        }
                    }
                },
                timezone: 'local',
                header: {
                    left: 'prev,next today addCalendarEvent',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                }
            };

            var date1 = new Date();
            date1.setHours(22);
            date1.setMinutes(22);

            var date2 = new moment();
            date2.set({hour:22,minute:22,second:0,millisecond:0});
            date2.add(1,'days');
            date2 = date2.toDate();


            // vm.events = [
            //     {
            //         email: 'test@gmail.com',
            //         name: 'My Event',
            //         date: date1,
            //         duration: 4,
            //         description: 'neki opis'
            //     },
            //     {
            //         email: 'test@gmail.com',
            //         name: 'This is a cool event',
            //         date: date2,
            //         duration: 1,
            //         description: 'This is a cool event'
            //     }
            // ]
          if(vm.rootUser) {
            vm.hashDan = {
              "1": "Monday",
              "2": "Tuesday",
              "3": "Wednesday",
              "4": "Thursday",
              "5": "Friday"
            };
            vm.hashUra = {
              "7": "07:00",
              "8": "08:00",
              "9": "09:00",
              "10": "10:00",
              "11": "11:00",
              "12": "12:00",
              "13": "13:00",
              "14": "14:00",
              "15": "15:00",
              "16": "16:00",
              "17": "17:00",
              "18": "18:00",
              "19": "19:00"
            };

            var getUrnik = function () {
              aggAppUrnik.getUrnik().then(
                function success(res) {
                  vm.urnikito = res.data
                  vm.urnik = []
                  for (var j = 0; j < vm.urnikito.length; j++) {
                    console.log(vm.urnikito[j].userId + " " + vm.rootUser._id);
                    if(vm.urnikito[j].userId === vm.rootUser._id) {

                        vm.urnik.push(vm.urnikito[j]);
                    }
                  }

                  //console.log(vm.urnik);
                  var table = document.getElementById('tabelaUrnik');
                  for (var i = 0; i < vm.urnik.length; i++) {
                    //console.log(vm.urnik[i]);
                    table.rows[parseInt(vm.urnik[i].ura) - 6].children[parseInt(vm.urnik[i].dan)].innerHTML = vm.urnik[i].title;
                  }
                }
              );
            };

            getUrnik();

            vm.clicked = function (data) {
              var jeZeData = false;
              var idUrnika = "";
              var table = document.getElementById('tabelaUrnik');
              if (table.rows[parseInt(data.split(" ")[0]) - 6].children[parseInt(data.split(" ")[1])].innerHTML != "") {
                console.log("OJLA");
                jeZeData = true;
                for (var i = 0; i < vm.urnik.length; i++) {
                  //console.log(vm.urnik[i]);
                  if (vm.urnik[i].ura == data.split(" ")[0] && vm.urnik[i].dan == data.split(" ")[1]) {
                    idUrnika = vm.urnik[i]._id;
                  }
                }
              }


              $uibModal.open({
                templateUrl: '/modalnaOkna/addUrnik/addUrnik.controller.html',
                controller: 'addUrnikCtrl',
                controllerAs: 'vm',
                backdrop: false,
                resolve: {
                  urnikData: function () {
                    return {
                      userId: vm.rootUser._id,
                      ura: data.split(" ")[0],
                      dan: data.split(" ")[1],
                      jeZeData: jeZeData,
                      idUrnika: idUrnika
                    }
                  }
                }
              }).closed.then(function () {
                getUrnik();
                // console.log("njki");
              });
            }
          }

        }

        homeCtrl.$inject = ['$scope','$rootScope','$uibModal', 'aggAppTodo', 'auth','aggAppCalendarEvent','aggAppUrnik'];

        /* global angular */
        angular
            .module('aggApp')
            .controller('homeCtrl', homeCtrl);
    }
)();

(function(){
    function loginCtrl($rootScope,$location,aggAppUsers, auth){
        var vm = this;

        vm.prijavniPodatki = {
            email: "",
            password: ""
        };

        var loginan = false;
        vm.login = function(){
            // var regEm = new RegExp("^(?![.])(?!.*[.]{2})[a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+(?<![.])@(?![-])[a-zA-Z0-9-]+(?<![-])\.(?![.])(?!.*[.]{2})[a-zA-Z0-9.]+(?<![.])$");
            // var regPass = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})(?![\s])");
            if(vm.prijavniPodatki.password && vm.prijavniPodatki.email){
                auth.login(vm.prijavniPodatki).then(
                    //po uspesni prijavi, klici servis za pridobitev uporabnika, ki se je identificiral
                    function(success) {
                        vm.response = 1;
                        var user = auth.currentUser();
                        if(!user){
                            console.log('NI PRIJAVLJENEGA UPORABNIKA!')
                        }else {
                            //$rootScope.rootUser = user;

                            //loginan = true;
                            //console.log('Nastavil uporabnika v root scope---------------');
                            console.log(user.userType);
                            console.log(user.userType == 'admin');
                            if(user.userType == 'admin'){
                                $location.path("/admin");
                            } else if(user.userType == 'uprava') {
                                $location.path("/dogodki");
                            } else {
                                $location.path("/");
                            }
                        }
                    },
                    function(napaka) {
                        vm.response= -1;
                        vm.napakaNaObrazcu = napaka.data.sporocilo;
                    }
                );
            }
            if(!loginan){
                vm.response='failure'
                //$location.path("/login");
            }
        };

    }

    loginCtrl.$inject = ['$rootScope','$location','aggAppUsers', 'auth'];

    /* global angular */
    angular
        .module('aggApp')
        .controller('loginCtrl', loginCtrl);
})();
(function(){
  function aggAppAdminMessages($http, auth) {
    function getAdminMessages(){
      return $http.get("/api/adminMessage")
    };

    function addAdminMessage(message){
      return $http.post("/api/adminMessage",message);
      /* {
        headers: {
          Authorization: 'Bearer ' + auth.getToken()
        }
      }); */
    };

    function deleteAdminMessage(id){
      return $http.delete("/api/adminMessage/" + id)
      /*{
        headers: {
          Authorization: 'Bearer ' + auth.getToken()
        }
      } */

    };
    
    return {
      getAdminMessages: getAdminMessages,
      addAdminMessage: addAdminMessage,
      deleteAdminMessage: deleteAdminMessage
    }
  }
  aggAppAdminMessages.$inject = ['$http','auth'];
  
  /* global angular */
  angular
    .module('aggApp')
    .service('aggAppAdminMessages', aggAppAdminMessages);
})();
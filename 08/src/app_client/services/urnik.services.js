(function(){
  function aggAppUrnik($http, auth) {
    function getUrnik(){
      return $http.get("/api/urnik")
    };
    function getUrnikById(id){
      return $http.get("/api/urnik/" + id)
    };
    function addUrnik(title,ura,dan,userId){
      return $http.post("/api/urnik",{
        title:title,
        ura:ura,
        dan:dan,
        userId: userId

      });
    };
    function editUrnik(id,title,ura,dan,userId){
      return $http.put("/api/urnik/"+id,{
          title:title,
          ura:ura,
          dan:dan,
          userId: userId
        },
        {
          headers: {
            Authorization: 'Bearer ' + auth.getToken()
          }
        });
    };
    function deleteUrnik(id){
      return $http.delete("/api/urnik/" + id,
        {
          headers: {
            Authorization: 'Bearer ' + auth.getToken()
          }
        })
    };

    return {
      getUrnik:getUrnik,
      addUrnik:addUrnik,
      editUrnik:editUrnik,
      deleteUrnik:deleteUrnik
    }
  }
  aggAppUrnik.$inject = ['$http','auth'];

  /* global angular */
  angular
    .module('aggApp')
    .service('aggAppUrnik', aggAppUrnik);
})();
(function(){
    function aggAppEvent($http, auth) {
        function getEvents(){
            return $http.get("/api/event")
        };
        function getEventByID(id){
            return $http.get("/api/event/" + id)
        };
        function addEvent(name,date,organizer,description){
            return $http.post("/api/event",{name:name,date:date,organizer:organizer,description:description});
        };
        function editEvent(id,name,date,organizer,description){
            return $http.put("/api/event/"+id,{name:name,date:date,organizer:organizer,description:description},
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        };
        function deleteEvent(id){
            return $http.delete("/api/event/" + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                })
        };

        return {
            getEvents: getEvents,
            getEventByID: getEventByID,
            addEvent:addEvent,
            editEvent: editEvent,
            deleteEvent: deleteEvent
        }
    }
    aggAppEvent.$inject = ['$http','auth'];

    /* global angular */
    angular
        .module('aggApp')
        .service('aggAppEvent', aggAppEvent);
})();
(function() {
  function auth($window, $http) {
    var tokenName = 'StraightAs-token';

    var saveToken = function(zeton) {
      $window.localStorage[tokenName] = zeton;
    };
    
    var getToken = function() {
     return $window.localStorage[tokenName];
    };
    
    var registration = function(user) {
      return $http.post('/api/registracija', user).then(
        function success(odgovor) {
          console.log('Shranim zeton po registraciji: ');
          console.log(odgovor.data.zeton);
          saveToken(odgovor.data.zeton);
        });
    };

    var login = function(user) {
      return $http.post('/api/prijava', user).then(
        function success(odgovor) {
          console.log('Shranim zeton po prijavi: ');
          console.log(odgovor.data.zeton);
          saveToken(odgovor.data.zeton);
        });
    };

    var logout = function() {
      $window.localStorage.removeItem(tokenName);
    };
    
      var isLoggedIn = function() {
        var token = getToken();
        if (token) {
          var koristnaVsebina = JSON.parse($window.atob(token.split('.')[1]));
          return koristnaVsebina.datumPoteka > Date.now() / 1000;
        } else {
          return false;
        }
     };
     
        var currentUser = function() {
        if (isLoggedIn()) {
          var zeton = getToken();
          var koristnaVsebina = JSON.parse($window.atob(zeton.split('.')[1]));
          return {
            _id: koristnaVsebina._id,
            name: koristnaVsebina.name,
            surname: koristnaVsebina.surname,
            email: koristnaVsebina.email,
            userType: koristnaVsebina.userType
          };
        }
      };
    
    return {
      saveToken: saveToken,
      getToken: getToken,
      registration: registration,
      login: login,
      logout: logout,
      isLoggedIn: isLoggedIn,
      currentUser: currentUser
    }
  }
  auth.$inject = ['$window', '$http'];
  
  /* global angular */
  angular
    .module('aggApp')
    .service('auth', auth);
})();
(function() {
    function aggAppRestavracije($http, auth) {

        function getRestavracije() {
            return $http.get("/api/restavracije",
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        }

        function getRestavracijaById(id) {
            return $http.get("/api/restavracije/" + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        }

        return {
            getRestavracije: getRestavracije,
            getRestavracijaById: getRestavracijaById
        }

    }

    aggAppRestavracije.$inject = ['$http', 'auth'];

    /* global angular */
    angular
        .module('aggApp')
        .service('aggAppRestavracije', aggAppRestavracije);
})();
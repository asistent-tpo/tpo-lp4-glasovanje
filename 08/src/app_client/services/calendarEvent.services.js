(function(){
    function aggAppCalendarEvent($http, auth) {
        function getCalendarEvents(){
            return $http.get("/api/calendar/event")
        };
        function getCalendarEventByID(id){
            return $http.get("/api/calendar/event/" + id)
        };
        function addCalendarEvent(name,date,duration,description){
            return $http.post("/api/calendar/event",{
                name:name,
                date:date,
                duration:duration,
                description:description,
                owner: auth.currentUser()._id
            });
        };
        function editCalendarEvent(id,name,date,duration,description){
            return $http.put("/api/calendar/event/"+id,{
                    name:name,
                    date:date,
                    duration:duration,
                    description:description,
                    owner: auth.currentUser()._id
                },
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        };
        function deleteCalendarEvent(id){
            return $http.delete("/api/calendar/event/" + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                })
        };

        return {
            getCalendarEvents: getCalendarEvents,
            getCalendarEventByID: getCalendarEventByID,
            addCalendarEvent:addCalendarEvent,
            editCalendarEvent: editCalendarEvent,
            deleteCalendarEvent: deleteCalendarEvent
        }
    }
    aggAppCalendarEvent.$inject = ['$http','auth'];

    /* global angular */
    angular
        .module('aggApp')
        .service('aggAppCalendarEvent', aggAppCalendarEvent);
})();
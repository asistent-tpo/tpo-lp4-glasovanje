(function(){
    function aggAppTodo($http, auth) {
        function getTodo(){
            return $http.get("/api/todo")
        };
        function getTodoByID(id){
            return $http.get("/api/todo/" + id)
        };
        function addTodo(opis,owner){
            return $http.post("/api/todo",{opis:opis,owner:owner},
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        };
        function editTodo(id,opis,owner){
            return $http.put("/api/todo/"+id,{opis:opis,owner:owner},
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        };
        function deleteTodo(id){
            return $http.delete("/api/todo/" + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                })
        };

        return {
            getTodo: getTodo,
            getTodoByID: getTodoByID,
            addTodo:addTodo,
            editTodo: editTodo,
            deleteTodo: deleteTodo
        }
    }
    aggAppTodo.$inject = ['$http','auth'];

    /* global angular */
    angular
        .module('aggApp')
        .service('aggAppTodo', aggAppTodo);
})();
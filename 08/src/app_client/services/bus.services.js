(function() {
    function aggAppBuses($http, auth) {
        function getBuses() {
            return $http.get("/api/buses",
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                });
        }

        function getBusById(id) {
            return $http.get("/api/buses/" + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + auth.getToken()
                    }
                })
        }
        
        return {
            getBuses: getBuses,
            getBusById: getBusById,
        }
    }

    aggAppBuses.$inject = ['$http', 'auth'];

    /* global angular */
    angular
        .module('aggApp')
        .service('aggAppBuses', aggAppBuses);

})();
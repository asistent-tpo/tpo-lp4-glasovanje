require('dotenv').load();
require('log-timestamp');

var express = require('express');
var streznik = express();
var port = process.env.PORT || 3000;
var path = require('path');
var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require('body-parser');

var uglifyJs = require('uglify-js');
var fs = require('fs');
var zdruzeno = uglifyJs.minify({
    'app.js': fs.readFileSync('src/app_client/app.js', 'utf-8'),
    'index.controller.js' : fs.readFileSync('src/app_client/components/index.controller.js', 'utf-8'),
    'login.controller.js' : fs.readFileSync('src/app_client/login/login.controller.js', 'utf-8'),
    'bus.controller.js' : fs.readFileSync('src/app_client/bus/bus.controller.js', 'utf-8'),
    'users.services.js' : fs.readFileSync('src/app_client/services/users.services.js', 'utf-8'),
    'home.controller.js' : fs.readFileSync('src/app_client/home/home.controller.js', 'utf-8'),
    'restavracije.controller.js' : fs.readFileSync('src/app_client/restavracije/restavracije.controller.js', 'utf-8'),
    'glava.controller.js' : fs.readFileSync('src/app_client/layout/glava/glava.controller.js', 'utf-8'),
    'todo.services.js' : fs.readFileSync('src/app_client/services/todo.services.js', 'utf-8'),
    'auth.services.js' : fs.readFileSync('src/app_client/services/auth.services.js', 'utf-8'),
    'admin.controller.js' : fs.readFileSync('src/app_client/admin/admin.controller.js', 'utf-8'),
    'adminMessages.service.js' : fs.readFileSync('src/app_client/services/adminMessages.services.js', 'utf-8'),
    'addPost.controller.js': fs.readFileSync('src/app_client/modalnaOkna/addPost/addPost.controller.js', 'utf-8'),
    'register.controller.js': fs.readFileSync('src/app_client/register/register.controller.js','utf-8'),
    'dogodki.controller.js': fs.readFileSync('src/app_client/dogodki/dogodki.controller.js','utf-8'),
    'addEvent.controller.js': fs.readFileSync('src/app_client/modalnaOkna/addEvent/addEvent.controller.js', 'utf-8'),
    'addCalendarEvent.controller.js': fs.readFileSync('src/app_client/modalnaOkna/addCalendarEvent/addCalendarEvent.controller.js', 'utf-8'),
    'editCalendarEvent.controller.js': fs.readFileSync('src/app_client/modalnaOkna/editCalendarEvent/editCalendarEvent.controller.js', 'utf-8'),
    'calendarEvent.service.js' : fs.readFileSync('src/app_client/services/calendarEvent.services.js', 'utf-8'),
    'event.service.js' : fs.readFileSync('src/app_client/services/event.services.js', 'utf-8'),
    'systemMessages.controller.js': fs.readFileSync('src/app_client/systemMessages/systemMessages.controller.js','utf-8'),
    'events.controller.js': fs.readFileSync('src/app_client/events/events.controller.js','utf-8'),
    'bus.services.js' : fs.readFileSync('src/app_client/services/bus.services.js', 'utf-8'),
    'editProfile.controller.js' : fs.readFileSync('src/app_client/editProfile/editProfile.controller.js', 'utf-8'),
    'glava.direktiva.js' : fs.readFileSync('src/app_client/layout/glava/glava.direktiva.js', 'utf-8'),
    'addUrnik.controller.js': fs.readFileSync('src/app_client/modalnaOkna/addUrnik/addUrnik.controller.js', 'utf-8'),
    'urnik.services.js' : fs.readFileSync('src/app_client/services/urnik.services.js', 'utf-8'),
    'restavracije.services.js' : fs.readFileSync('src/app_client/services/restavracije.services.js', 'utf-8')

});
fs.writeFile('src/public/angular/aggApp.min.js', zdruzeno.code, function(napaka) {
    if (napaka)
        console.log(napaka);
    else
        console.log('Skripta je zgenerirana in shranjena v "aggApp.min.js".');
});
streznik.set('view engine', 'html');
// parse application/x-www-form-urlencoded
streznik.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
streznik.use(bodyParser.json());

// Odprava varnostnih pomanjkljivosti
streznik.use(function(req, res, next) {
    res.setHeader('X-Frame-Options', 'DENY');
    res.setHeader('X-XSS-Protection', '1; mode=block');
    res.setHeader('X-Content-Type-Options', 'nosniff');
    next();
});
//povezovanje na bazo
require('./app_api/models/db');

// passport za login (mora bit za povezovanjem na bazo)
require('./app_api/config/passport');


streznik.use(logger('dev'));
streznik.use(express.json());
streznik.use(express.urlencoded({ extended: true }));
streznik.use(cookieParser());

streznik.use(session({
    secret:"user_sid",
    name:"Aggregator-Session-Cookie",
    saveUninitialized: true,
    resave: false,
    cookie:{maxAge:3600000}
}));

streznik.use(function (req, res, next) {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');
    }
    next();
});

streznik.use(express.static(path.join(__dirname, 'public')));
streznik.use(express.static(path.join(__dirname, '../node_modules','bootstrap','dist','css')));
streznik.use(express.static(path.join(__dirname, '../node_modules','angular-fullcalendar','dist')));
streznik.use(express.static(path.join(__dirname, '../node_modules','angularjs-datepicker','dist')));
streznik.use(express.static(path.join(__dirname, '../node_modules','ui-bootstrap4','dist')));
streznik.use(express.static(path.join(__dirname, 'app_client')));

//passport
streznik.use(passport.initialize());
streznik.use(passport.session());

// Obvladovanje napak zaradi avtentikacije
streznik.use(function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({
            "sporočilo": err.name + ": " + err.message
        });
    }
});

// REST API
var indexApi = require('./app_api/routes/index');
streznik.use('/api', indexApi);

streznik.use(function(req, res) {
    res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});


// catch 404 and forward to error handler
streznik.use(function(req, res, next) {
    next(createError(404));
});

// error handler
streznik.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render(err);
});

/**
 * Poženi strežnik
 */
streznik.listen(port, function () {
    console.log('Strežnik je pognan na portu ' + port + '!');
});

module.exports = streznik;
# 08. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Bitbucket:  https://bitbucket.org/number8brt/tpo-lp4/commits/1b8db6c835d196602ee7cac2e3cd9a35633a578c

Heroku:     https://straight-as-8.herokuapp.com/

Testni user:

     -email: user@user.si

     -pass: PASS12345

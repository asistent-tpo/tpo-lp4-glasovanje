var request = require('supertest');
var app = require('../src/streznik.js');
var expect = require('chai').expect;


function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low)
}

describe('Login', function () {
    describe('Login API', function () {
        it('Should success if credential is valid', function (done) {
            this.timeout(5000);
            request(app)
                .post('/api/prijava')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .send({"email": "ah@gmail.com", "password": "ahPASS12345"})
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function (response) {
                    expect(response.body).not.to.be.empty;
                    expect(response.body).to.be.an('object');
                })
                .end(done);
        });
        it('Should success if credential is wrong', function (done) {
            this.timeout(5000);
            request(app)
                .post('/api/prijava')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .send({"email": "test@gmail.com", "password": "FAKENEWS"})
                .expect(401)
                /* .expect('Content-Type', /json/)
                 .expect(function(response) {
                   expect(response.body).not.to.be.empty;
                   expect(response.body).to.be.an('object');
                 }) */
                .end(done);
        });
    });
});

describe('Register API', function () {
    var randomNumber = randomInt(0,10000000);
    it('Should success if credential is valid! sucess only once after initDB!', function (done) {
        var email = 'janez' + randomNumber + '@novak';
        this.timeout(5000);
        request(app)
            .post('/api/registracija')
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .send({
                "name": "Janez"+randomNumber,
                "surname": "Novak"+randomNumber,
                "username": "Brt"+randomNumber,
                "email": "janez"+randomNumber+"@novak",
                "password": "passPass",
                "userType": "user"
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(function (response) {
                expect(response.body).not.to.be.empty;
                expect(response.body).to.be.an('object');
            })
            .end(done);
    });
    it('Should sucess if credential is wrong', function (done) {
        this.timeout(5000);
        request(app)
            .post('/api/registracija')
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .send({
                "name": "Janez",
                "surname": "Novak",
                "username": "Brt",
                "email": "janez"+randomNumber+"@novak",
                "password": "passPass",
                "password2": "passPass",
                "userType": "user"
            })
            .expect(500)
            /* .expect('Content-Type', /json/)
             .expect(function(response) {
               expect(response.body).not.to.be.empty;
               expect(response.body).to.be.an('object');
             }) */
            .end(done);
    });
});

// it ('Prikaži odstavek "A veš, da ti verjetno ne veš ..."', function(done) {
//     zahteva(streznik).get('/').expect(/<p>A veš, da ti verjetno ne veš, da jaz vem, da ti uporabljaš naslednji spletni brskalnik\?<\/p>/i, done);
// });

describe('Todo', function () {
    describe('Todo API', function () {
        it('Should post a todo event', function () {
            this.timeout(5000);
            request(app)
                .post('/api/todo')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .send({
                    "opis": "Upamo, da bo ocenjevalcu/ocenjevalki všeč naš test. Zanj smo se zelo potrudili.",
                    "owner": "Skupina 8"
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function (response) {
                    expect(response.body).not.to.be.empty;
                    expect(response.body).to.be.an('object');
                })
        });

        it('Todo list should not be empty', function () {
            this.timeout(5000);
            request(app)
                .get('api/todo')
                .send()
                .expect(200)
                .expect(function (response) {
                    expect(response.body).not.to.be.empty;
                })

        });
    });
});

describe('Admin Messages', function () {
    describe('Admin Messages API', function () {
        it('Should post a admin message', function () {
            this.timeout(5000);
            request(app)
                .post('api/adminMessage')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .send({
                    "message": "Message from admin, test",
                })
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function (response) {
                    expect(response.body).not.to.be.empty;
                    expect(response.body).to.be.an('object');
                })
        });

        it('Admin Messages should not be empty', function () {
            this.timeout(5000);
            request(app)
                .get('api/adminMessage')
                .send()
                .expect(200)
                .expect(function (response) {
                    expect(response.body).not.to.be.empty;
                })

        });
    });
});
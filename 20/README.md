# 20. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Commit: https://bitbucket.org/63150115/tpo-lp4/commits/ed90dca73d5504d9080f28c8b38127afeed50fee

Stran dostopna na: https://straightas20.herokuapp.com/

Backup stran: https://www.pavlic.si

Backup2: https://rgti-webgl-sajtrga.c9users.io

Backup3: https://tpo-lp4-th1224.c9users.io

Testni uporabnik:

    ime: ayy.lmao12345679@gmail.com

    geslo: test1234

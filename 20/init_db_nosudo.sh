# dodajanje userja, gesla in baze: moral bi ze vse to bit, ce ze kej obstaja, bo samo failal, ni panike. Nisem testiral ce dejansko dela
useradd -m tpo -p tpo4fri
echo "added linux user tpo"
su - postgres -c "createuser tpo -s"
echo "created psql user tpo"
su - postgres -c "createdb straightas"
echo "created db straightas"
su - tpo -c "psql -d straightas -c \"ALTER USER tpo WITH PASSWORD 'tpo4fri'\""
echo "changed password for tpo"

# dodajanje shem: napiste svoje sheme spodej
su - tpo -c "psql -d straightas -c 'CREATE TABLE \"uporabnik\" (Id SERIAL PRIMARY KEY, email TEXT, hashed_geslo TEXT, dostopni_zeton TEXT, potrjen boolean)'"
su - tpo -c "psql -d straightas -c \"INSERT INTO uporabnik (Id, email, hashed_geslo, dostopni_zeton, potrjen) VALUES (1234,'testni.uporabnik@gmail.com',1234,1234,true)\""
su - tpo -c "psql -d straightas -c 'CREATE TABLE \"koledar\" (Id SERIAL PRIMARY KEY, email TEXT, datum TEXT, ime TEXT, opis TEXT)'"
su - tpo -c "psql -d straightas -c 'CREATE TABLE \"todo\" (Id SERIAL PRIMARY KEY, email TEXT, opis TEXT)'"
su - tpo -c "psql -d straightas -c 'CREATE TABLE \"urnik\" (Id SERIAL PRIMARY KEY, email TEXT, ime TEXT, trajanje INT, barva TEXT)'"
su - tpo -c "psql -d straightas -c 'CREATE TABLE \"adminmsgs\" (Id SERIAL PRIMARY KEY, sporocilo TEXT)'"
su - tpo -c "psql -d straightas -c 'CREATE TABLE \"dogodki\" (Id SERIAL PRIMARY KEY, ime TEXT, date DATE, organizator TEXT, opis TEXT)'"

echo "tables now in database: "
su - tpo -c "psql -d straightas -c \"\\dt\""
var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

/**
 * Testiranje modela
 */

describe('Test Modela Administratorska obvestila', function() {

	it('Testiranje dodajObvestilo', (done) => {
		this.timeout(5000);

		var msgData = {
			msg: "Test message"
		};
		chai.request(app)
			.post('/api/adminmsgs')
			.send(msgData)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('Testiranje dodajObvestilo brez podatkov', (done) => {
		this.timeout(5000);

		chai.request(app)
			.post('/api/adminmsgs')
			.end((err, res) => {
				res.should.have.status(500);
				done();
			});
	});

	it('Testiranje pridobiObvestila', (done) => {
		this.timeout(5000);

		chai.request(app)
			.get('/api/adminmsgs')
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
});

/**
 * Testiranje krmilnika
 */

describe('Test Krmilnika za administratorska obvestila', function() {
	it('Testiranje posljiSporocilo', (done) => {
		this.timeout(5000);

		var msgData = {
			msg: "test message"
		};

		chai.request(app)
			.post('/admin/msg')
			.send(msgData)
			.end((err, res) => {
				res.should.have.status(500);
				done();
			});
	});
 });
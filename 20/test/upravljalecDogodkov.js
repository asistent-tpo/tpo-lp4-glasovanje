var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

describe('Test dodajanja dogodka pogled', function() {
    it ('dodajanje dogodka osnovni tok', (done) => {
        this.timeout(5000);
        var eventData = {
            ime: "test event",
			date: new Date("01-01-2020"),
			organizator: "test organizator",
			opis: "test descripton"
		}
		
        chai.request(app)
            .post('/manageevents')
            .send(eventData)
            .end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
	
	it ('dodajanje dogodka manjkajoči podatki', (done) => {
		this.timeout(5000);
		var eventData = {
			ime: "test fail",
			date: new Date("01-01-2020"),
			organizator: "test organizator"
		}

		chai.request(app)
			.post('/manageevents')
			.send(eventData)
			.end((err, res) => {
				res.should.have.status(500);
				done();
			});
	});

	it ('dodajanje dogodka zastarel datum', (done) => {
		this.timeout(5000);
		var eventData = {
			ime: "test fail date",
			date: new Date("01-01-2000"),
			organizator: "test organizator",
			opis: "test descripton"
		}

		chai.request(app)
			.post('/manageevents')
			.send(eventData)
			.end((err, res) => {
				res.should.have.status(500);
				done();
			});
	});
});
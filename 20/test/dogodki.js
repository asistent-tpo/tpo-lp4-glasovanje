var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

/**
 * Testiranje modela
 */

 describe('Test Modela Dogodki', function() {

	it('Testiranje dodaj', (done) => {
		this.timeout(5000);

        var eventData = {
            ime: "test event",
			date: new Date("01-01-2020"),
			organizator: "test organizator",
			opis: "test descripton"
		}
		chai.request(app)
			.post('/api/events')
			.send(eventData)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('Testiranje dodaj z manjkajočimi podatki', (done) => {
		this.timeout(5000);

		var eventData = {
            ime: "test event",
			date: new Date("01-01-2020"),
			organizator: "test organizator",
		}
		chai.request(app)
			.post('/api/events')
			.send(eventData)
			.end((err, res) => {
				res.should.have.status(500);
				done();
			});
	});

	it('Testiranje vrniVseDogodke', (done) => {
		this.timeout(5000);

		chai.request(app)
			.get('/api/events')
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
 });

 /**
  *  Testiranje krmilnika in pogleda
  */

describe('Test Krmilnika za dogodke', function() {
	it('Testiraj generirajInPosljiStran', (done) => {
		this.timeout(5000);

		chai.request(app)
			.get('/events')
			.end((err, res) => {
				res.should.have.status(200);
				done();
			})
	});
});
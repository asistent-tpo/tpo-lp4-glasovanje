var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

/**
 * Testiranje modela
 */
describe('Testiranje Modela Koledar', function() {
	

	it('Test vrniVseVnose', (done) => {
		this.timeout(5000);

		var data = {
			email: "test@test.com"
		};

		chai.request(app)
			.post('/api/koledar/vrniVseVnose')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('Test posodobiVnos', (done) => {
		this.timeout(5000);

		var data = {
			email: "test@test.com",
			id: 100,
			ime: "New Name",
			datum: "26-6-2019",
			opis: "New description"
		};

		chai.request(app)
			.post('/api/koledar/posodobiVnos')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('test vrniNaslednjiId', (done) => {
		this.timeout(5000);

		var data = {
			email: "test@test.com"
		};

		chai.request(app)
			.get('/api/koledar/vrniNaslednjiId')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('test preveriVeljavnostDatuma', (done) => {
		this.timeout(5000);

		var data = {
			datum: new Date()
		}

		chai.request(app)
			.get('/api/koledar/preveriVeljavnostDatuma')
			.send(data)
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});

	it('test preveriVeljavnostDatuma slab datum', (done) => {
		this.timeout(5000);

		var data = {
			datum: new Date("01-01-1999")
		}

		chai.request(app)
			.get('/api/koledar/preveriVeljavnostDatuma')
			.send(data)
			.end((err, res) => {
				res.should.have.status(418);
				done();
			});
	});


});
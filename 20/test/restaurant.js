var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

/**
 * Test model
 */

describe('Test Model Restavracija', function() {
	it('Test vrni_podatke_o_restavracijah', (done) => {
		this.timeout(5000);

		chai.request(app)
			.get('/api/restaurants')
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
});

/**
 * Test krmilnika in pogleda
 */

describe('Test pogleda Restavracija', function() {
	it('Test pogleda restavracija', (done) => {
		this.timeout(5000);

		chai.request(app)
			.get('/restaurants')
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
});

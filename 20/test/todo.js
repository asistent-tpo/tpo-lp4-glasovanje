var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

/**
 * Test model
 */

describe('Test Modela TODO', function() {
	it('Test dodajVnosVTodo', (done) => {
		this.timeout(5000);

		var data = {
			id: 100,
			email: "testni.uporabnik@gmail.com",
			opis: "test opis"	
		};
		chai.request(app)
			.post('/api/todo/dodajVnosVTodo')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('Test vrniVseVnose', (done) => {
		this.timeout(5000);

		var data = {
			email: "testni.uporabnik@gmail.com"
		};

		chai.request(app)
			.post('/api/todo/vrniVseVnose')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('Test posodobiVnos', (done) => {
		this.timeout(5000);

		var data = {
			id: 100,
			email: "testni.uporabnik@gmail.com",
			opis: "nov opis"	
		};

		chai.request(app)
			.post('/api/todo/posodobiVnos')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('Test izbrisiVnos', (done) => {
		this.timeout(5000);

		var data = {
			id: 100,
			email: "testni.uporabnik@gmail.com"
		};

		chai.request(app)
			.post('/api/todo/izbrisiVnos')
			.send(data)
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
});

 /**
  * Test krmilnik
  */

describe('Test Krmilnik TODO', function() {
	it('Test dodajVnosVTodo', (done) => {
		this.timeout(5000);

		var data = {
			id: 100,
			email: "testni.uporabnik@gmail.com",
			opis: "test opis"	
		};

		chai.request(app)
			.post('/dodajVnosVTodo')
			.send(data)
			.end((err, res) => {
				res.should.have.status(418);
				done();
			});
	});
});
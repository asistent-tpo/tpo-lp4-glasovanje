var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

var email = "testni.uporabnik@gmail.com";
var geslo = "asdffdsa";
var geslo2 = "qwerrewq";
var potrditveniZeton = "a5bd52fcfbc2502d7ffdce62ed1863d08";
var cookie = "asdf";

describe('Test registracije', function() 
{
    it ('Registracija testniuporabnik Izjemni tok', (done) =>
    {
        this.timeout(5000);
        var authData =
        {
            key: "BADBEEF",
            email: email
        }
        
        var testData = 
        {
            email: email,
            geslo: "asdf",
            geslo2: "asdf"
        }
        
        chai.request(app)
            .post('/api/uporabnik/deleteUserFromTable')
            .send(authData)
            .end((err, res) => {
                res.should.have.status(200);
                
                chai.request(app)
                    .post('/register')
                    .send(testData)
                    .end((err, res) => {
                        res.should.have.status(406);
                        
                    });
                    
                setTimeout(function(){
                    done();
                }, 2000);
                
            });
        
    });
    
    it ('Registracija testniuporabnik Osnovni tok', (done) =>
    {
        
        var authData =
        {
            key: "BADBEEF",
            email: email
        }
        
        var testData = 
        {
            email: email,
            geslo: geslo,
            geslo2: geslo
        }
        
        chai.request(app)
            .post('/api/uporabnik/deleteUserFromTable')
            .send(authData)
            .end((err, res) => {
                res.should.have.status(200);
                
                chai.request(app)
                    .post('/register')
                    .send(testData)
                    .end((err, res) => {
                        res.should.have.status(200);
                        
                        chai.request(app)
                            .get('/api/uporabnik/preveriVeljavnostPotrditvenegaZetona?email='+email+'&zeton='+potrditveniZeton)
                            .end((err, res) => {
                                res.should.have.status(200);
                                done();
                            });
                            
                    });
                    
            });
        
    });
    
});

describe('Test prijave', function() 
{
    it ('Prijava testniuporabnik Izjemni tok', (done) =>
    {
        var testData = 
        {
            email: email,
            geslo: "asdf"
        }
        
        chai.request(app)
            .post('/login')
            .send(testData)
            .end((err, res) => {
                res.should.have.status(418);
                done();
            });
    });
    
    it ('Prijava testniuporabnik Osnovni tok', (done) =>
    {
        var testData = 
        {
            email: email,
            geslo: geslo
        }
        
        chai.request(app)
            .post('/login')
            .send(testData)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});

describe('Test spremembe gesla', function() 
{
    it ('Sprememba gesla testniuporabnik Izjemni tok', (done) =>
    {
        var testData = 
        {
            email: email,
            staro_geslo: geslo,
            novo_geslo: geslo2,
            novo_geslo2: "asdf"
        }
        
        var authData =
		{
			email: email,
			date: new Date().toISOString()
		}
        
        chai.request.agent(app)
            .post('/api/uporabnik/shraniDostopniZeton')
            .send(authData)
            .then((err, res) => {
                cookie = err.text;
                console.log("got kukiue");
                console.log(cookie);
                chai.request.agent(app)
                    .post('/password')
                    .set('Cookie','StraightAs='+cookie)
                    .send(testData)
                    .end((err, res) => {
                        res.should.have.status(418);
                        done();
                    });
            });
            
    });
    
    it ('Sprememba gesla testniuporabnik Osnovni tok', (done) =>
    {
        var testData = 
        {
            email: email,
            staro_geslo: geslo,
            novo_geslo: geslo2,
            novo_geslo2: geslo2
        }
        
        var testData2 = 
        {
            email: email,
            geslo: geslo2
        }
        
        
        
        chai.request.agent(app)
            .post('/password')
            .set('Cookie','StraightAs='+cookie)
            .send(testData)
            .end((err, res) => {
                res.should.have.status(200);
                
                chai.request.agent(app)
                    .post('/login')
                    .send(testData2)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
                    
            });
    });
});

describe('Test krmilnika za avtentikacijo uporabnika', function() 
{
    it ('Prikaz pogleda Password', (done) =>
    {
        chai.request(app)
            .get('/password')
            .set('Cookie','StraightAs='+cookie)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
    it ('Prikaz pogleda Login', (done) =>
    {
        chai.request(app)
            .get('/login')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
    it ('Prikaz pogleda Register', (done) =>
    {
        chai.request(app)
            .get('/register')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
    
});

describe('Test modela Uporabnik', function() 
{
    it ('Test avtentikacije iz cookija Should fail', (done) =>
    {
        var testData =
        {
            email: email,
            zeton: cookie+"fafafafa"
        }
        
        chai.request(app)
            .post('/api/uporabnik/preveriDostopniZeton')
            .set('Cookie','StraightAs='+cookie)
            .send(testData)
            .end((err, res) => {
                res.should.have.status(418);
                done();
            });
    });
    
    it ('Test avtentikacije iz cookija Should pass', (done) =>
    {
        var testData =
        {
            email: email,
            zeton: cookie
        }
        
        var authData =
		{
			email: email,
			date: new Date().toISOString()
		}
        
        chai.request.agent(app)
            .post('/api/uporabnik/shraniDostopniZeton')
            .send(authData)
            .then((err, res) => {
                cookie = err.text;
                console.log("got kukiue");
                console.log(cookie);
                testData.zeton = cookie;
                chai.request(app)
                    .post('/api/uporabnik/preveriDostopniZeton')
                    .set('Cookie','StraightAs='+cookie)
                    .send(testData)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
        
        
    });
    
    it ('Test posiljanja maila', (done) =>
    {
        var testData =
        {
            email: email,
            zeton: cookie
        }
        
        chai.request(app)
            .post('/api/uporabnik/posljiPotrditveniEmail')
            .set('Cookie','StraightAs='+cookie)
            .send(testData)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
});

describe('Test prikaza pogleda Admin', function() 
{
    it ('Prikaz pogleda Admin', (done) =>
    {
        chai.request(app)
            .get('/admin')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
    it ('Prikaz pogleda Admin Msg', (done) =>
    {
        chai.request(app)
            .get('/admin/msg')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
});

describe('Test posiljanja sporocila na Admin', function() 
{
    it ('Poslji sporocilo', (done) =>
    {
        var testData =
        {
            msg: "asdf"
        }
        
        chai.request(app)
            .post('/admin/msg')
            .set('Cookie','StraightAs='+cookie)
            .send(testData)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    
});
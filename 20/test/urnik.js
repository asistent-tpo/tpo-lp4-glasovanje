var chai = require('../src/node_modules/chai');
var request = require('../src/node_modules/request');
var chaiHttp = require('../src/node_modules/chai-http');
var should = chai.should();
var app = require('../src/app');

chai.use(chaiHttp);

/**
 * Testiranje modela
 */

describe('Test Modela Urnik', function() {

	it('testiranje dodajVnosVUrnik', (done) => {
		this.timeout(5000);

		var data = {
			email: "test@test.com",
			id: 100,
			ime: "test name",
			trajanje: 1,
			barva: "blue"
		};
		chai.request(app)
			.post('/api/urnik/dodajVnosVUrnik')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});



	it('testiranje posodobiVnos', (done) => {
		this.timeout(5000);

		var data = {
			email: "test@test.com",
			id: 100,
			ime: "change",
			trajanje: 2,
			barva: "red"
		};
		chai.request(app)
			.post('/api/urnik/posodobiVnos')
			.send(data)
			.end((err, res) => {
				res.should.have.status(201);
				done();
			});
	});

	it('testiranje izbrisiVnos', (done) => {
		this.timeout(5000);

		var data = {
			id: 100
		};
		chai.request(app)
			.post('/api/urnik/izbrisiVnos')
			.send(data)
			.end((err, res) => {
				res.should.have.status(200);
				done();
			});
	});
});


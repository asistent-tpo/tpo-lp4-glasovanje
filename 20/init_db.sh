# dodajanje userja, gesla in baze: moral bi ze vse to bit, ce ze kej obstaja, bo samo failal, ni panike. Nisem testiral ce dejansko dela
sudo adduser tpo
sudo -u postgres createuser tpo -s
sudo -u tpo psql -c "ALTER USER tpo WITH PASSWORD 'tpo4fri'"
sudo -u postgres createdb straightas

# dodajanje shem: napiste svoje sheme spodej
sudo -u tpo psql -d straightas -c 'CREATE TABLE "uporabnik" (Id SERIAL PRIMARY KEY, email TEXT, hashed_geslo TEXT, dostopni_zeton TEXT, potrjen boolean)'
sudo -u tpo psql -d straightas -c 'CREATE TABLE "koledar" (Id SERIAL PRIMARY KEY, email TEXT, datum TEXT, ime TEXT, opis TEXT)'
sudo -u tpo psql -d straightas -c 'CREATE TABLE "todo" (Id SERIAL PRIMARY KEY, email TEXT, opis TEXT)'
sudo -u tpo psql -d straightas -c 'CREATE TABLE "urnik" (Id SERIAL PRIMARY KEY, email TEXT, ime TEXT, trajanje INT, barva TEXT)'
sudo -u tpo psql -d straightas -c 'CREATE TABLE "adminmsgs" (Id SERIAL PRIMARY KEY, sporocilo TEXT)'
sudo -u tpo psql -d straightas -c 'CREATE TABLE "dogodki" (Id SERIAL PRIMARY KEY, ime TEXT, date DATE, organizator TEXT, opis TEXT)'
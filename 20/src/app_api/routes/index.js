"use strict"

var express = require('express');
var router = express.Router();
var ctrlAdminMsg = require('../controllers/adminMsg');
var ctrlEvents = require('../controllers/events');
var ctrlUrnik = require('../controllers/urnik');
var ctrlKoledar = require('../controllers/koledar');
var ctrlUporabnik = require('../controllers/uporabnik');
var ctrlRestaurants = require('../controllers/restaurants');
var ctrlTodo = require('../controllers/todo');

// uporabnik
router.post('/uporabnik/preveriGeslo', ctrlUporabnik.preveriGeslo);
router.post('/uporabnik/preveriVeljavnostGesla', ctrlUporabnik.preveriVeljavnostGesla);
router.post('/uporabnik/preveriVeljavnostEmaila', ctrlUporabnik.preveriVeljavnostEmaila);
router.post('/uporabnik/preveriUporabnika', ctrlUporabnik.preveriUporabnika);
router.get('/uporabnik/preveriVeljavnostPotrditvenegaZetona', ctrlUporabnik.preveriVeljavnostPotrditvenegaZetona);
router.post('/uporabnik/dodajUporabnika', ctrlUporabnik.dodajUporabnika);
router.post('/uporabnik/posljiPotrditveniEmail', ctrlUporabnik.posljiPotrditveniEmail);
router.post('/uporabnik/shraniDostopniZeton', ctrlUporabnik.shraniDostopniZeton);
router.post('/uporabnik/spremeniGeslo', ctrlUporabnik.spremeniGeslo);
router.post('/uporabnik/preveriDostopniZeton', ctrlUporabnik.preveriDostopniZeton);
router.post('/uporabnik/deleteUserFromTable', ctrlUporabnik.deleteUserFromTable);

// koledar
router.post('/koledar/vrniVseVnose', ctrlKoledar.vrniVseVnose);
router.get('/koledar/vrniNaslednjiId', ctrlKoledar.vrniNaslednjiId);
router.get('/koledar/preveriVeljavnostDatuma', ctrlKoledar.preveriVeljavnostDatuma);
router.post('/koledar/dodajVnos', ctrlKoledar.dodajVnos);
router.post('/koledar/izbrisiVnos', ctrlKoledar.izbrisiVnos);
router.post('/koledar/posodobiVnos', ctrlKoledar.posodobiVnos);

// urnik
router.post('/urnik/vrniVseVnose', ctrlUrnik.vrniVseVnose);
router.post('/urnik/dodajVnosVUrnik', ctrlUrnik.dodajVnosVUrnik);
router.post('/urnik/izbrisiVnos', ctrlUrnik.izbrisiVnos);
router.post('/urnik/posodobiVnos', ctrlUrnik.posodobiVnos);

// todo
router.post('/todo/vrniVseVnose', ctrlTodo.vrniVseVnose);
router.post('/todo/dodajVnosVTodo', ctrlTodo.dodajVnosVTodo);
router.post('/todo/izbrisiVnos', ctrlTodo.izbrisiVnos);
router.post('/todo/posodobiVnos', ctrlTodo.posodobiVnos);

// admin messages
router.post('/adminMsgs', ctrlAdminMsg.dodajObvestilo);
router.get('/adminMsgs', ctrlAdminMsg.pridobiObvestila);

// restaurants
router.get('/restaurants', ctrlRestaurants.vrni_podatke_o_restavracijah);

// events
router.get('/events', ctrlEvents.vrniVseDogodke);
router.post('/events', ctrlEvents.dodaj);

module.exports = router;
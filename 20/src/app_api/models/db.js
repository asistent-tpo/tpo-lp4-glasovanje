"use strict"

const Pool = require('pg').Pool;

var dbUri = "postgresql://tpo:tpo4fri@localhost:5432/straightas";
if (process.env.NODE_ENV === 'production') {
	dbUri = process.env.DATABASE_URL;
}
var pool = new Pool({
	connectionString: dbUri
});

module.exports.getPool = function() {
	return pool;
}

pool.connect(function(err, client, done) {
	if (err) {
		console.log("not able to get connection " + err);
	}
	console.log("connected to db on address " + dbUri);
});
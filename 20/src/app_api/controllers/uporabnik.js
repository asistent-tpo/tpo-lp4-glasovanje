"use strict"

var nodemailer = require('nodemailer'); 
var crypto = require('crypto'); 

var pool = require('../models/db').getPool();

//var nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
var nakljucnaVrednost = "ADFD1234FDAD4321";

/**
 * MODEL UPORABNIK
 * tabela:
 * 		- id:int => primarni ključ
 * 		- email:text => email uporabnika
 * 		- hashed_geslo:text => shranjeno geslo
 * 		- dostopni_zeton:text => žeton
 * 		- potrjen:boolean => ali je dobil potrditveni email
 */

 /**
  * Metoda izbriše uporabnika
  */
module.exports.deleteUserFromTable = function(req,res)
{
	if(req.body.key === "BADBEEF")
	{
		
		pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "CREATE TABLE \"uporabnik\" (Id SERIAL PRIMARY KEY, email TEXT, hashed_geslo TEXT, dostopni_zeton TEXT, potrjen boolean)",
			    [],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to delete uporabnik data!  " + error);
    					
    				}
    				
			    	pool.connect(function(error, client, done) {
					if (error) {
						console.log("Could not get connection " + error);
						res.status(500).send();
					}
					else {
						client.query(
						    "DELETE FROM uporabnik WHERE uporabnik.email = $1",
						    [req.body.email],
						    function(error, result) {
			    				if (error) {
			    					console.log("Failed to delete uporabnik data!  " + error);
			    					res.status(500).send();
			    				}
			    				else {
			    					done();
			    			   		res.status(200).send();
			    				}
							});
						}
					});
    				
				});
			}
		});
		
		
	}
}

/**
 * Metoda preveri, če je podan uporabnik potrjen
 */
module.exports.preveriUporabnika = function(req, res) {
	var email = req.body.email;
	
	console.log("email: " + email);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT potrjen FROM uporabnik WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to get user with email! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					if(result.rows[0] !== undefined)
    					{
    						if (result.rows[0].potrjen){
    					    	res.status(200).send();
    						} else {
    					    	res.status(418).send();
    						}
    					}
    					else
    					{
    						res.status(418).send();
    					}
    				}
			    });
		}
	});
};

/**
 * Metoda preveri veljavnost emaila
 */
module.exports.preveriVeljavnostEmaila = function(req, res) {
	var email = req.body.email;
	
	console.log("email: " + email);
	
	function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
	
	if (validateEmail(email)) {
	    pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT * FROM uporabnik WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					
    					if (result.length > 0){
    					    res.status(418).send();
    					} else {
    					    res.status(200).send();
    					}
    				}
			    });
		}
	});
	} else {
	    res.status(418).send();
	}
};

/**
 * Metoda preveri veljavnost gesla
 */
module.exports.preveriVeljavnostGesla = function(req, res) {
	var geslo1 = req.body.geslo1;
	var geslo2 = req.body.geslo2;
	
	console.log("geslo1, geslo2: " + geslo1 + ", " + geslo2);
	
	if (geslo1 === geslo2){
		if(geslo1.length < 8)
		{
			res.status(406).send();
		}
		else
		{
	    	res.status(200).send();
		}
	} else {
	    res.status(418).send();
	}
};

/**
 * Metoda preveri, če se podano geslo ujema z uporabnikovim geslom
 */
module.exports.preveriGeslo = function(req, res) {
	var email = req.body.email;
	var geslo = req.body.geslo;
	
	console.log("email, geslo: " + email + ", " + geslo);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT hashed_geslo FROM uporabnik WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					console.log(result);
    					var baza_geslo = result.rows[0].hashed_geslo;
    					//var check_geslo = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
    					var check_geslo = crypto.createHash('md5').update(geslo).digest('hex');
    					if (check_geslo === baza_geslo){
    					    res.status(200).send();
    					} else {
    					    res.status(418).send();
    					}
    				}
			    });
		}
	});
};

/**
 * Metoda doda novega uporabnika
 */
module.exports.dodajUporabnika = function(req, res) {
	var email = req.body.email;
	var geslo = req.body.geslo;
	var zeton = "a" + crypto.createHash('md5').update(email).digest('hex');
	var potrjen = false;
	var Id = Math.floor(Math.random() * 10000);
	
	console.log("email, geslo: " + email + ", " + geslo);
	console.log("potrditveni zeton: " + zeton);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "INSERT INTO uporabnik (Id, email, hashed_geslo, dostopni_zeton, potrjen) VALUES ($1,$2,$3,$4,$5)",
			    [Id,email,crypto.createHash('md5').update(geslo).digest('hex'),zeton,potrjen],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send(zeton);
    				}
			    });
		}
	});
};

/**
 * Metoda pošlje potrditveni email
 */
module.exports.posljiPotrditveniEmail = function(req, res)
{
	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
    		user: 'asstraight.20@gmail.com',
    		pass: 'ass455ass'
		}
	});

	var mailOptions = {
		from: 'asstraight.20@gmail.com',
		to: req.body.email,
		subject: 'StraightAs confirmation email',
		text: req.protocol + "://" + req.get('host') + "/api/uporabnik/preveriVeljavnostPotrditvenegaZetona" +"?email=" + req.body.email + "&zeton=" + req.body.zeton
	};

	transporter.sendMail(mailOptions, function(error, info){
		if (error) {
    		console.log(error);
    		res.status(500).send();
		} else {
    		console.log('Email sent: ' + info.response);
    		res.status(200).send();
		}
	}); 
	
};

/**
 * Metoda preveri veljavnost žetona
 */
module.exports.preveriVeljavnostPotrditvenegaZetona = function(req, res)
{
	var email = req.query.email;
	var zeton = req.query.zeton;
	
	console.log("email: " + email);
	console.log("zeton: " + zeton);
	
	function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
	
	if (validateEmail(email)) {
	    pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT * FROM uporabnik WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					
    					
   						if (result.length > 0){
   					    	res.status(418).send();
   						} else {
   							if(result.rows[0].dostopni_zeton === zeton)
   							{
   								console.log("tokenmatch");
   								client.query(
			    				"UPDATE uporabnik SET potrjen = $1 WHERE email = $2",
			    				[true,email],
			    				function(error, result) {
    								if (error) {
    									console.log("Failed to insert into urnik! " + error);
    									res.status(500).send();
    								}
    								else {
    									done();
    									res.status(200).send("Successful email confirmation");
    								}
			    				});
   							}
   							else
   							{
   								res.status(418).send();
   							}

    					}
    					
    				}
			    });
		}
	});
	} else {
	    res.status(418).send();
	}
};

/**
 * Metoda shrani žeton
 */
module.exports.shraniDostopniZeton = function(req, res)
{
	var token = req.body.email + "|" + req.body.date + "|" + crypto.createHmac('sha1', nakljucnaVrednost).update(req.body.email + "|" + req.body.date).digest('hex');
	console.log(token);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "UPDATE uporabnik SET dostopni_zeton = $1 WHERE email = $2",
			    [token,req.body.email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(200).send(token);
    				}
			    });
		}
	});
};

/**
 * Metoda spremeni geslo uporabnika
 */
module.exports.spremeniGeslo = function(req, res)
{
	console.log("uporabnik.js spremeniGeslo()");
	console.log(req.body.email);
	console.log(req.body.geslo);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "UPDATE uporabnik SET hashed_geslo = $1 WHERE email = $2",
			    [crypto.createHash('md5').update(req.body.geslo).digest('hex'),req.body.email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(200).send();
    				}
			    });
		}
	});
};

/**
 * Metoda preveri žeton uporabnika
 */
module.exports.preveriDostopniZeton = function(req, res)
{
	var email = req.body.email;
	var zeton = req.body.zeton;
	
	console.log("email: " + email);
	console.log("zeton: " + zeton);
	
	function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
	
	if (validateEmail(email)) {
	    pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT * FROM uporabnik WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					
    					
   						if (result.length > 0){
   					    	res.status(418).send();
   						} else {
   							var zetonDate = Date.parse(zeton.split("|")[1]);
   							console.log("zetonDate " + zetonDate + " today " + (new Date().getTime()));
   							console.log(result.rows[0]);
   							if((result.rows[0].dostopni_zeton === zeton) && ((new Date().getTime()) - zetonDate < 60 * 60 * 1000 * 24 * 2))
   							{
   								console.log("tokenmatch");
    							res.status(200).send("Successful zeton confirmation");
   							}
   							else
   							{
   								res.status(418).send();
   							}

    					}
    					
    				}
			    });
		}
	});
	} else {
	    res.status(418).send();
	}
};
"use strict"
var pool = require('../models/db').getPool();


/*
* PREVERI VELJAVNOST DATUME
* Pomen: 
*	- Preveri, ce je datum veljaven
* Parametri:
*	- datum
* Return:
*	- status
*/
module.exports.preveriVeljavnostDatuma = function(req, res) {
	var datum = new Date(req.body.datum);
	var podanDatum = datum.getTime();
	var d = new Date('January 1, 2000 00:00:00').getTime();
	
	if (podanDatum > d) {
	    res.status(200).send();
	} else {
	    res.status(418).send();
	}
};

/*
* VRNI NASLEDNJI ID
* Pomen: 
*	- Vrne naslednji prost id za vnos v koledar
* Parametri:
*	- email
* Return:
*	- status
*/
module.exports.vrniNaslednjiId = function(req, res) {
	var email = req.body.email;
	
	console.log("email: " + email);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT MAX(id) FROM koledar",
			    [],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to read all from koledar! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					var naslednjiProsti = result + 1;
    					res.status(201).send(naslednjiProsti);
    				}
			    });
		}
	});
};


/*
* VRNI VSE VNOSE
* Pomen: 
*	- Vrne vse vnose
* Parametri:
*	- email
* Return:
*	- status
*/
module.exports.vrniVseVnose = function(req, res) {
	var email = req.body.email;
	
	console.log("email: " + email);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT * FROM koledar WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to read all from koledar! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send(result);
    				}
			    });
		}
	});
};

/*
* POSODOBI VNOS
* Pomen: 
*	- Posodobi vnos v koledarju uporabnika
* Parametri:
*	- email
*	- id
*	- ime
*	- datum
*	- opis
* Return:
*	- status
*/
module.exports.posodobiVnos = function(req, res) {
	var email = req.body.email;
	var id = req.body.id;
	var ime = req.body.ime;
	var datum = req.body.datum;
	var opis = req.body.opis;
	var cas = req.body.cas;
	
	console.log("id + email: " + email + " + " + id);
	console.log("updating entry koledar");
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "UPDATE koledar SET ime = ($1), datum = ($2), opis = ($3), email = ($5) WHERE id = ($4) AND ime = ($1)",
			    [ime, datum+"~"+cas, opis, id, email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					console.log(result);
    					res.status(201).send(result);
    				}
			    });
		}
	});
};

/*
* IZBRISI VNOS
* Pomen: 
*	- Izbrise vnos iz koledarja
* Parametri:
*	- email
*	- id
*	- ime
* Return:
*	- status
*/
module.exports.izbrisiVnos = function(req, res) {
	var id = req.body.id;
	var email = req.body.email;
	var ime = req.body.ime;
	console.log("izbrisiVnos()");
	
	console.log("id: " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "DELETE FROM koledar WHERE id = ($1) AND email = ($2) AND ime = ($3)",
			    [id, email,ime],
			    function(error, result) {
    				if (error) {
    					console.log("Deleted");
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(200).send("Deleted.");
    				}
			    });
		}
	});
};

/*
* DODAJ VNOS
* Pomen: 
*	- Doda vnos v koledar uporabnika
* Parametri:
*	- email
*	- id
*	- ime
*	- datum
*	- opis
* Return:
*	- status
*/
module.exports.dodajVnos = function(req, res) {
	var email = req.body.email;
	var id = req.body.id;
	var ime = req.body.ime;
	var datum = req.body.datum;
	var opis = req.body.opis;
	var cas = req.body.cas;
	
	console.log("id + email: " + email + " + " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "INSERT INTO koledar (email, id, ime, datum, opis) VALUES ($1,$2,$3,$4,$5)",
			    [email,id,ime,datum+"~"+cas,opis],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send();
    				}
			    });
		}
	});
};
"use strict"
/**
 * MODEL DOGODKI
 * struktura tabele:
 * 		- Id:int => primarni ključ
 * 		- ime:Text => ime dogodka
 * 		- organizator:Text => organizator dogodka
 * 		- date:Date => datum dogodka
 * 		- opis:Text => opis dogodka
 */
var pool = require('../models/db').getPool();

/**
 * Metoda vrne vse dogodke iz podatkovne baze
 * @param req - zahteva
 * @param res - odgovor
 * 
 * @returns v primeru uspeha: status 200 in podatke / v primeru neuspeha: status 500
 */
module.exports.vrniVseDogodke = function(req, res) {
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not connect to db! " + error);
			res.status(500).send();
		}
		else {
			client.query("SELECT * FROM dogodki",
			null,
			function(error, result) {
				if (error) {
					console.log("Query failed! " + error);
					res.status(500).send();
				}
				else {
					done();
					res.status(200).send(result);
				}
			});
		}
	});
};

/**
 * Metoda doda nov vnos v tabelo
 * @param req - zahteva. Vsebovati mora 'body', ki mora vsebovati podatke o novem vnosu
 * @param res - odgovor
 * 
 * @returns v primeru uspeha: status 201 / v primeru nesupeha: status 500
 */
module.exports.dodaj = function(req, res) {
	if (req.body.ime === undefined ||
		req.body.date === undefined ||
		req.body.organizator === undefined ||
		req.body.opis === undefined) {
		res.status(500).send();
	}
	else {
		pool.connect(function(error, client, done) {
			if (error) {
				console.log("Failed connecting to db! " + error);
				res.status(500).send();
			}
			else {
				client.query("INSERT INTO dogodki (ime, date, organizator, opis) VALUES ($1,$2,$3,$4)",
							[req.body.ime, req.body.date, req.body.organizator, req.body.opis],
							function(error, result) {
								if (error) {
									console.log("Failed query! " + error);
									res.status(500).send();
								}
								else {
									done();
									res.status(201).send();
								}
							});
			}
		});
	}
};
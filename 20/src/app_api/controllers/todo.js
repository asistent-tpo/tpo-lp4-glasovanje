"use strict"
var pool = require('../models/db').getPool();


/*
* DODAJ VNOS V TODO
* Pomen: 
*	- Doda nov element na seznam Todo
* Parametri:
*	- email
*	- id
*	- opis
* Return:
*	- status
*/
module.exports.dodajVnosVTodo = function(req, res) {
	var email = req.body.email;
	var id = req.body.id;
	var opis = req.body.opis;

	console.log("id + email: " + email + " + " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "INSERT INTO todo (email, id, opis) VALUES ($1,$2,$3)",
			    [email,id,opis],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send("Successfully added row into urnik.");
    				}
			    });
		}
	});
};

/*
* IZBRISI VNOS
* Pomen: 
*	- Izbrise vnos iz seznama Todo
* Parametri:
*	- email
*	- id
* Return:
*	- status
*/
module.exports.izbrisiVnos = function(req, res) {
	var id = req.body.id;
	var email = req.body.email;
	
	console.log("id | email: " + id + " | " + email);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "DELETE FROM todo WHERE id = ($1) AND email = ($2)",
			    [id,email],
			    function(error, result) {
    				if (error) {
    					console.log("Error");
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(200).send("Deleted");
    				}
			    });
		}
	});
};

/*
* POSODOBI VNOS
* Pomen: 
*	- Doda nov element na seznam Todo
* Parametri:
*	- email
*	- id
*	- opis
* Return:
*	- status
*/
module.exports.posodobiVnos = function(req, res) {
	var email = req.body.email;
	var id = req.body.id;
	var opis = req.body.opis;
	
	console.log("id + email: " + email + " + " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "UPDATE todo SET opis = ($3) WHERE email = ($1) AND id = ($2)",
			    [email, id, opis],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send();
    				}
			    });
		}
	});
};

/*
* VRNI VSE VNOSE
* Pomen: 
*	- Vrne vse vnose iz seznama Todo
* Parametri:
*	- email
* Return:
*	- status
*/
module.exports.vrniVseVnose = function(req, res) {
	var email = req.body.email;
	
	console.log("email: " + email);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT * FROM todo WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to read from urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send(result);
    				}
			    });
		}
	});
};
"use strict"
var pool = require('../models/db').getPool();


/*
* DODAJ VNOS V URNIK
* Pomen: 
*	- Doda vnos v urnik uporabnika
* Parametri:
*	- email
*	- id
*	- ime
*	- trajanje
*	- barva
* Return:
*	- status
*/
module.exports.dodajVnosVUrnik = function(req, res) {
	var email = req.body.email;
	var id = req.body.id;
	var ime = req.body.ime;
	var trajanje = req.body.trajanje;
	var barva = req.body.barva;
	var datum = req.body.datum + "~" + req.body.cas;
	
	console.log("id + email: " + email + " + " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "INSERT INTO urnik (email, id, ime, trajanje, barva) VALUES ($1,$2,$3,$4,$5)",
			    [email,id,ime,trajanje,datum],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to insert into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send({msg: "Successfully added row into urnik."});
    				}
			    });
		}
	});
};


/*
* IZBRISI VNOS
* Pomen: 
*	- Izbrise vnos iz urnika
* Parametri:
*	- id
* Return:
*	- status
*/
module.exports.izbrisiVnos = function(req, res) {
	var id = req.body.id;
	
	console.log("id: " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "DELETE FROM urnik WHERE id = ($1)",
			    [id],
			    function(error, result) {
    				if (error) {
    					console.log("Error");
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(200).send({msg: "Deleted"});
    				}
			    });
		}
	});
};


/*
* POSODOBI VNOS
* Pomen: 
*	- Posodobi vnos v urniku uporabnika
* Parametri:
*	- email
*	- id
*	- ime
*	- trajanje
*	- barva
* Return:
*	- status
*/
module.exports.posodobiVnos = function(req, res) {
	var email = req.body.email;
	var id = req.body.id;
	var ime = req.body.ime;
	var trajanje = req.body.trajanje;
	var barva = req.body.barva;
	var datum = req.body.datum + "~" + req.body.cas;
	
	console.log("id + email: " + email + " + " + id);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "UPDATE urnik SET ime = ($1), trajanje = ($2), barva = ($3) WHERE id = ($4)",
			    [ime, trajanje, datum, id],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to update into urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send(result);
    				}
			    });
		}
	});
};


/*
* VRNI VSE VNOSE
* Pomen: 
*	- Vrne vse vnose uporabnika s podanim email
* Parametri:
*	- email
* Return:
*	- status
*/
module.exports.vrniVseVnose = function(req, res) {
	var email = req.body.email;
	
	console.log("email: " + email);
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
			res.status(500).send();
		}
		else {
			client.query(
			    "SELECT * FROM urnik WHERE email = ($1)",
			    [email],
			    function(error, result) {
    				if (error) {
    					console.log("Failed to return all rows with specified email from urnik! " + error);
    					res.status(500).send();
    				}
    				else {
    					done();
    					res.status(201).send(result);
    				}
			    });
		}
	});
};
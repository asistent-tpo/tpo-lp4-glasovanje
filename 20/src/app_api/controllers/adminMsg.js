"use strict"
/**
 * MODEL ADMINISTRATORSKA OBVESTILA
 * struktura tabele:
 * 		-Id:int => primarni ključ
 * 		-msg:Text => vsebina sporočila
 */
var pool = require('../models/db').getPool();

/**
 * Metoda doda nov vnos v tabelo
 * @param req - zahteva, vsebovati mora podakte o sporočilu
 * @param res - odgovor
 * 
 * @returns v primeru uspeha: status 201 / v primeru neuspeha: status 500
 */
module.exports.dodajObvestilo = function(req, res) {
	if (req !== undefined &&
		req.body !== undefined &&
		req.body.msg !== undefined) {

		var msg = req.body.msg;
		pool.connect(function(error, client, done) {
			if (error) {
				console.log("Could not get connection " + error);
				res.status(500).send();
			}
			else {
				client.query("INSERT INTO adminmsgs (sporocilo) VALUES ($1)",
							[msg],
							function(error, result) {
								if (error) {
									console.log("Failed to insert into adminmsgs! " + error);
									res.status(500).send();
								}
								else {
									done();
									res.status(201).send("Successfully added an admin message");
									console.log("adminmsg " + msg);
								}
							});
			}
		});
	}
	else {
		res.status(500).send();
	}
};

/**
 * Metoda vrne vsa obvestila iz podatkovne baze
 * @param req - zahteva
 * @param res - odgovor
 * 
 * @returns v primeru uspeha: status 200 in podatke / v primeru neuspeha: status 500
 */
module.exports.pridobiObvestila = function(req, res) {
	pool.connect(function(error, client, done) {
		if (error) {
			console.log("Could not get connection " + error);
		}
		else {
			client.query("SELECT * FROM adminmsgs",
						function(error, result) {
							if (error) {
								console.log("Could not read from adminmsgs! " + error);
								res.status(500).send();
							} else {
								done();
								res.status(200).send(result.rows);
							}
						});
		}
	});
};
"use strict"

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// connection to the database
require('./app_api/models/db');

// connection to the REST API
var indexApi = require('./app_api/routes/index');
var indexRouter = require('./app_server/routes/index');
var usersRouter = require('./app_server/routes/users');
var busRouter = require('./app_server/routes/bus');
var restaurantsRouter = require('./app_server/routes/restaurnats');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// REST API
app.use('/api', indexApi);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bus', busRouter);
app.use('/restaurants', restaurantsRouter);

// catch 404 and forward to error handler

app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
// DISABLE THIS WHEN DEBUGGING
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
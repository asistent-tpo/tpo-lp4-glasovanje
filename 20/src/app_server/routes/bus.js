var express = require('express');
var router = express.Router();
var request=require('request');

var ctrlBus = require('../controllers/bus');

router.get('/', function(req, res, next) {
    let query = req.query.q;
    console.log("Query: " + query);

    if(typeof query === "undefined") {
        res.render('bus', {query: query})
    }
    
    // preveri ce je bilo kaj vneseno v okencek za avtobusno postajo
        
    let postajaOk = ctrlBus.potrdi_vnos_avtobusne_postaje(query);

    if(postajaOk) {
        ctrlBus.prikazi_podatke_o_prihodih(query, req, res, next);
    } else {
        ctrlBus.prikazi_obvestilo_o_napaki(query, req, res, next);
    }
});


module.exports = router;
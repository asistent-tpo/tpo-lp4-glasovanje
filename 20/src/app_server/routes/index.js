"use strict"

var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');
var ctrlKrmilnikZaAvtentikacijoUporabnika = require('../controllers/KrmilnikZaAvtentikacijoUporabnika');
var ctrlRegistracijskiObrazec = require('../controllers/RegistracijskiObrazec');
var ctrlPrijavniObrazec = require('../controllers/PrijavniObrazec');
var ctrlSpremembaGesla = require('../controllers/SpremembaGesla');
var ctrlAdmin = require('../controllers/admin');
var ctrlTodo = require('../controllers/todo');
var ctrlDogodki = require('../controllers/events');
var ctrlKoledar = require('../controllers/koledar');
var ctrlUserDogodki = require('../controllers/userEvents');
var ctrlUrnik = require('../controllers/urnik');
var ctrlKoledar = require('../controllers/koledar');

/* GET home page. */
router.get('/', ctrlMain.index);
router.post('/dodajVnosVTodo',ctrlTodo.dodajVnosVTodo);
router.post('/izbrisiVnosVTodo',ctrlTodo.izbrisiVnos);
router.post('/posodobiVnosVTodo',ctrlTodo.posodobiVnosVTodo);

router.post('/dodajVnosVUrnik', ctrlUrnik.dodajVnosVUrnik);
router.post('/izbrisiVnosUrnik', ctrlUrnik.izbrisiVnos);

router.post('/dodajVnosVKoledar', ctrlKoledar.dodajVnos);
router.post('/izbrisiVnosKoledar', ctrlKoledar.izbrisiVnos);

/* Administrator */
router.get('/admin', ctrlAdmin.get);
router.get('/admin/msg', ctrlAdmin.msg);
router.post('/admin/msg', ctrlAdmin.posljiSporocilo);

/* KrmilnikZaAvtentikacijoUporabnika */
router.get('/login/', ctrlKrmilnikZaAvtentikacijoUporabnika.PrijavniObrazec);
router.post('/login/', ctrlPrijavniObrazec.prijaviSe);
router.get('/register/', ctrlKrmilnikZaAvtentikacijoUporabnika.RegistracijskiObrazec);
router.post('/register/', ctrlRegistracijskiObrazec.registrirajSe);
router.get('/password/', ctrlKrmilnikZaAvtentikacijoUporabnika.SpremembaGeslaObrazec);
router.post('/password/', ctrlSpremembaGesla.spremeniGeslo);
router.get('/logout/', function(req, res) {
	res.redirect('/login');
});

/* Pogled za upravljanje z dogodki */
router.get('/manageevents/', ctrlDogodki.pridobiDogodke);
router.post('/manageevents/', ctrlDogodki.objaviDogodek);

/* Prikaz dogodkov uporabniku */
router.get('/events/', ctrlUserDogodki.generirajInPosljiStran);

module.exports = router;

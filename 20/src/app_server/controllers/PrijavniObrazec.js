var ctrlKrmilnikZaAvtentikacijoUporabnika = require('../controllers/KrmilnikZaAvtentikacijoUporabnika');

var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};

module.exports.prijaviSe = function(req, res)
{
    console.log("prijaviSe()");
    var retVal = ctrlKrmilnikZaAvtentikacijoUporabnika.izvediPrijavo(req,res,prijaviSeDone);
    
};

var prijaviSeDone = function(req,res,retVal)
{
	if(retVal !== 200)
    {
    	console.log("napakaa");
    	prikaziObvestiloONapaki(req,res,retVal);
    }
    else
    {
    	console.log("returned,redirectinr");
    	res.redirect("/");
    }
}
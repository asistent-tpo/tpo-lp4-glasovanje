"use strict"

var ctrlKrmilnikZaAvtentikacijoUporabnika = require("./KrmilnikZaAvtentikacijoUporabnika");

var request = require('request');

module.exports.get = function(req, res) {
	res.render('admin');
};

module.exports.msg = function(req, res) {
	res.render('adminMsg');
};

// TODO - pošlji ostalim
module.exports.posljiSporocilo = function(req, res) {
	if ((req.body === undefined) ||
		(req.body.msg === undefined)) {
		prikaziObvestiloONapaki(req, res, 500);
	}
	else {
		if (req.body.msg !== undefined) {
			ctrlKrmilnikZaAvtentikacijoUporabnika.avtenticirajSejo(req,res,posljiSporociloDone);
		}
		else {
			prikaziObvestiloONapaki(req, res, 500);
			
		}
	}
};

var posljiSporociloDone = function(req,res,retVal)
{

	if(retVal === 200)
	{

		var path = '/api/adminmsgs'
		var data = {
			msg: req.body.msg
		}
		var params = {
			url: req.protocol + "://" + req.get('host') + path,
			method: 'POST',
			json: data
		}
		request(params, function(error, response, content) {
			if (response.statusCode === 201) {
				res.redirect('/admin');
			}
			else {
				prikaziObvestiloONapaki(req, res, response.statusCode);
			}
		});
	}
	else
	{
		prikaziObvestiloONapaki(req, res, retVal);
	}
};

var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};

"use strict"

var ctrlKrmilnikZaAvtentikacijoUporabnika = require("./KrmilnikZaAvtentikacijoUporabnika");

var request = require('request');

/*
* IZBRISI VNOS
* Pomen: 
*	- Verificira uporabnika in klice metodo, ki izbrise obstojec vnos iz seznama Todo
* Parametri:
*	- email
*	- id
* Return:
*	- void
*/
module.exports.izbrisiVnos = function(req, res) {
	if (req !== null && req.body !== null && 
	    req.body.id !== null && req.body.email !== null) {
    		ctrlKrmilnikZaAvtentikacijoUporabnika.avtenticirajSejo(req,res,izbrisiVnosDone);
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

/*
* IZBRISI VNOS DONE
* Pomen: 
*	- Izbrise obstojec vnos iz seznama Todo
* Parametri:
*	- email
*	- id
* Return:
*	- status
*/
var izbrisiVnosDone = function(req, res, retVal)
{
	console.log("izbrisiVnosDone()");
	if(retVal === 200)
	{
		var params = {
			url: req.protocol + "://" + req.get('host') + '/api/todo/izbrisiVnos',
			method: 'POST',
			json: {
				id: req.body.id,
				email: req.cookies.StraightAs.split("|")[0]
			}
		}
		request(params, function(error, response, content) {
			if (response.statusCode === 200) {
				res.status(200).redirect('/');
			}
			else {
				prikaziObvestiloONapaki(req, res, 500);
			}
		});
	}
	else
	{
		
	}
};

/*
* DODAJ VNOS V TODO
* Pomen: 
*	- Verificira uporabnika in klice metodo, ki doda vnos na seznam Todo
* Parametri:
*	- email
*	- id
*	- opis
* Return:
*	- void
*/
module.exports.dodajVnosVTodo = function(req, res) {
	console.log("dodajVnosVTodo()");
	if (req !== null && req.body !== null && 
	    req.body.email !== null &&
	    req.body.id !== null &&
	    req.body.opis !== null ) {
    		ctrlKrmilnikZaAvtentikacijoUporabnika.avtenticirajSejo(req,res,dodajVnosVTodoDone);
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};


/*
* DODAJ VNOS V TODO DONE
* Pomen: 
*	- Doda vnos v seznam Todo
* Parametri:
*	- opis
*	- id
* Return:
*	- status
*/
var dodajVnosVTodoDone = function(req,res,retVal)
{
	console.log("dodajVnosVTodoDone()");
	if(retVal === 200)
	{
		var params = {
    			url: req.protocol + "://" + req.get('host') + '/api/todo/dodajVnosVTodo',
    			method: 'POST',
    			json: {
    				email: req.cookies.StraightAs.split("|")[0],
    				id: Math.floor(Math.random() * 10000),
    				opis: req.body.opis
    			}
    		}
    		request(params, function(error, response, content) {
    			if (response.statusCode === 201) {
    				console.log(res.content);
    				res.status(201).redirect('/');
    			}
    			else {
    				console.log("napaka requesta");
    				console.log(res.content);
    				prikaziObvestiloONapaki(req, res, 500);
    			}
    		});
	}
	else
	{
		console.log("unauthenticated");
		prikaziObvestiloONapaki(req,res,418);
	}
};

/*
* POSODOBI VNOS V TODO
* Pomen: 
*	- Verificira uporabnika in klice metodo, ki posodobi obstojec vnos na seznamu Todo
* Parametri:
*	- opis
*	- id
* Return:
*	- void
*/
module.exports.posodobiVnosVTodo = function(req, res) {
	console.log("posodobiVnosVTodo()");
	   console.log(req.body);
	if (req !== null && req.body !== null && 
	    req.body.id !== null &&
	    req.body.opis !== null ) {
    		ctrlKrmilnikZaAvtentikacijoUporabnika.avtenticirajSejo(req,res,posodobiVnosVTodoDone);
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

/*
* POSODOBI VNOS V TODO DONE
* Pomen: 
*	- Posodobi obstojec vnos na seznamu Todo
* Parametri:
*	- opis
*	- id
*	- email
* Return:
*	- status
*/
var posodobiVnosVTodoDone = function(req, res, retVal)
{
	console.log("pospdobiVnosVTodoDone");
	if(retVal === 200)
	{
		var params = {
			url: req.protocol + "://" + req.get('host') + '/api/todo/posodobiVnos',
			method: 'POST',
			json: {
				email: req.cookies.StraightAs.split("|")[0],
				id: req.body.id,
				opis: req.body.opis
			}
		}
		request(params, function(error, response, content) {
			if (response.statusCode === 201) {
				res.status(201).redirect('/');
			}
			else {
				prikaziObvestiloONapaki(req, res, 500);
			}
		});
	}
	else
	{
		prikaziObvestiloONapaki(req, res, 500);
	}
}

/*
* PRIKAZI OBVESTILO O NAPAKI
* Pomen: 
*	- Posreduje sporocilo o napaki s statusom
* Parametri:
*	- req
*	- res
*	- statusCode
* Return:
*	- objekt
*/
var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};
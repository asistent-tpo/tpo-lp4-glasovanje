"use strict"

var request = require('request');

module.exports.pridobiDogodke = function(req, res) {
	if (req !== undefined && 
		req.body !== undefined) {
		var params = {
			url: req.protocol + "://" + req.get('host') + '/api/events',
			method: 'GET',
			json: {
				email: req.body.email
			}
		}
		request(params, function(error, response, content) {
			if (response.statusCode === 200) {
				res.status(200).render('events', content);
			}
			else {
				prikaziObvestiloONapaki(req, res, 500);
			}
		});
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

module.exports.objaviDogodek = function(req, res) {
	if (req === null ||
		req.body === null ||
		req.body.ime === null ||
		req.body.date === null ||
		req.body.organizator === null ||
		req.body.opis === null) {
			
		prikaziObvestiloONapaki(req, res, 500);
	}
	else {
		if (sensibleDate(req.body.date)) {
			var data = {
				ime: req.body.ime,
				date: req.body.date,
				organizator: req.body.organizator,
				opis: req.body.opis
			}
			var params = {
				url: req.protocol + "://" + req.get('host') + '/api/events',
				method: 'POST',
				json: data
			}
			request(params, function(error, response, content){
				if (error) {
					prikaziObvestiloONapaki(req, res, 500);
				}
				else {
					if (response.statusCode === 201) {
						res.status(200).redirect('/manageevents');
					}
					else {
						prikaziObvestiloONapaki(req, res, 500);
					}
				}
			});
		}
		else {
			prikaziObvestiloONapaki(req, res, 500);
		}
	}
}

var sensibleDate = function(date) {
	var currentDate = new Date();
	var inDate = new Date(date);
	return currentDate.getTime() <= inDate.getTime();
}

var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};

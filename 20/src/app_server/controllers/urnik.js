"use strict"

var request = require('request');

/*
* IZBRISI VNOS IZ URNIKA
* Pomen: 
*	- Klice metodo modela urnik, ki izbrise vnos iz bazo
* Parametri:
*	- id
* Return:
*	- status
*/
module.exports.izbrisiVnos = function(req, res) {
	if (req !== null && req.body !== null && 
	    req.body.id !== null) {
	    	if(req.body.izbrisi === "delete")
	    	{
    		var params = {
    			url: req.protocol + "://" + req.get('host') + '/api/urnik/izbrisiVnos',
    			method: 'POST',
    			json: {
    				id: req.body.id,
    			}
    		}
    		request(params, function(error, response, content) {
    			if (response.statusCode === 200) {
    				res.status(200).redirect('/');
    			}
    			else {
    				prikaziObvestiloONapaki(req, res, 500);
    			}
    		});
	    	}
	    	else
	    	{
	    		console.log(req.body);
	    		var params = {
    			url: req.protocol + "://" + req.get('host') + '/api/urnik/posodobiVnos',
    			method: 'POST',
    			json: {
    				email: req.cookies.StraightAs.split("|")[0],
    				id: req.body.id,
    				ime: req.body.ime,
    				trajanje: req.body.trajanje,
    				datum: req.body.datum,
    				cas: req.body.cas,
    				barva: req.body.barva
    			}
    		}
    		request(params, function(error, response, content) {
    			if (response.statusCode === 201) {
    				res.status(200).redirect('/');
    			}
    			else {
    				prikaziObvestiloONapaki(req, res, 500);
    			}
    		});
	    	}
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

/*
* DODAJ VNOS V URNIK
* Pomen: 
*	- Klice metodo modela urnik, ki doda vnos v bazo
* Parametri:
*	- email
*	- id
*	- ime
*	- trajanje
*	- barva
* Return:
*	- status
*/
module.exports.dodajVnosVUrnik = function(req, res) {
	if (req !== null && req.body !== null && 
	    req.body.email !== null &&
	    req.body.ime !== null &&
	    req.body.trajanje !== null) {
    		var params = {
    			url: req.protocol + "://" + req.get('host') + '/api/urnik/dodajVnosVUrnik',
    			method: 'POST',
    			json: {
    				email: req.cookies.StraightAs.split("|")[0],
    				id: Math.floor(Math.random() * 1000),
    				ime: req.body.ime,
    				trajanje: req.body.trajanje,
    				datum: req.body.datum,
    				cas: req.body.cas,
    				barva: req.body.barva
    			}
    		}
    		request(params, function(error, response, content) {
    			if (response.statusCode === 201) {
    				res.status(200).redirect('/');
    			}
    			else {
    				prikaziObvestiloONapaki(req, res, 500);
    			}
    		});
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

/*
* PRIKAZI OBVESTILO O NAPAKI
* Pomen: 
*	- Posreduje sporocilo o napaki s statusom
* Parametri:
*	- req
*	- res
*	- statusCode
* Return:
*	- objekt
*/
var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};
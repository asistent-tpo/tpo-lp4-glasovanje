"use strict"

var request = require('request');

module.exports.generirajInPosljiStran = function(req, res) {
	var params = {
		url: req.protocol + "://" + req.get('host') + '/api/events',
		method: 'GET',
		json: {
			email: req.body.email,
		}
	}
	request(params, function(error, response, content) {
		if (response.statusCode === 200) {
			res.status(200).render('userEvents', content);
		}
		else {
			prikaziObvestiloONapaki(req, res, 500);
		}
	});
};


var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};
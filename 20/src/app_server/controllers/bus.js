"use strict"

var request = require('request');

// posredovanje iskalnega niza na trola.si/api, ta mu vrne podatke formatirane v obliki JSON

let getStations = function(query, callback) {
    let options = {
        url: 'https://www.trola.si/' + query,
        headers: {
            'Accept': 'application/json'
        }
    };

    request.get(options, callback)
}


module.exports.potrdi_vnos_avtobusne_postaje = function(query) {
   return !!query;  // vrne true or false
};

//seznam avtobusov

module.exports.prikazi_podatke_o_prihodih = function(query, req, res, next) {
    getStations(query, function(error, response, body) {
        let d = {query: query}; // data to be returned

        if (!error && response.statusCode === 200) {  // status je success
            let data = JSON.parse(body);  // parsanje

            if (data.error) {   // ce API vrnil error potem prikazemo napako (neveljavna postaja)
                d.error = data.error;
            } else {  // vse vredu s podatki
                d.data = data;
            }
        } else {  // ce ne moramo dostopati do trola.si
            d.error = "There was an error accessing Trola API." + error;
        }

        res.render('bus', d);  // prikaz seznama
    });
};

// prikaz obvestila o napaki, ce vnesena postaja ne obstaja

module.exports.prikazi_obvestilo_o_napaki= function(query, req, res, next) {
    res.render('bus', {
        error: "Unable to find station with a name: " + query
    });
};


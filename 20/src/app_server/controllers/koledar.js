"use strict"

var request = require('request');
var ctrlKrmilnikZaAvtentikacijoUporabnika = require("./KrmilnikZaAvtentikacijoUporabnika");

/*
* IZBRISI VNOS
* Pomen: 
*	- Preveri vrednost parametra req.body.izbrisi in glede na vrednost klice
*	  metodo, ki izbrise vnos iz baze ali posodobi vnos v bazi
* Parametri:
*	- email
*	- id
*	- ime
*	- datum
*	- opis
* Return:
*	- status
*/
module.exports.izbrisiVnos = function(req, res) {
	if (req !== null && req.body !== null && 
	    req.body.email !== null &&
	    req.body.id !== null) {
	    	if(req.body.izbrisi === "delete")
	    	{
	    		var params = {
	    			url: req.protocol + "://" + req.get('host') + '/api/koledar/izbrisiVnos',
	    			method: 'POST',
	    			json: {
	    				email: req.cookies.StraightAs.split("|")[0],
	    				id: req.body.trajanje,
	    				ime: req.body.ime
	    			}
	    		}
	    		request(params, function(error, response, content) {
	    			if (response.statusCode === 200) {
	    				res.status(200).redirect('/');
	    			}
	    			else {
	    				prikaziObvestiloONapaki(req, res, 500);
	    			}
	    		});
	    	}
	    	else
	    	{
	    		var params = {
	    			
	    			url: req.protocol + "://" + req.get('host') + '/api/koledar/posodobiVnos',
	    			method: 'POST',
	    			json: {
	    				email: req.cookies.StraightAs.split("|")[0],
	    				id: req.body.trajanje,
	    				ime: req.body.ime,
	    				datum: req.body.datum,
	    				opis: req.body.opis,
	    				cas: req.body.cas
	    			}
	    		}
	    		request(params, function(error, response, content) {
	    			if (response.statusCode === 201) {
	    				res.status(200).redirect('/');
	    			}
	    			else {
	    				prikaziObvestiloONapaki(req, res, 500);
	    			}
	    		});
	    	}
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

/*
* DODAJ VNOS DONE
* Pomen: 
*	- Avtenticira uporabnika in ob uspesni avtentikaciji klice metodo DODAJ VNOS DONE
* Parametri:
*	- email
*	- id
*	- ime
*	- datum
*	- opis
* Return:
*	- void
*/
module.exports.dodajVnos = function(req, res) {
	console.log("dodajVnosVKoledar()");
	if (req !== null && req.body !== null && 
	    req.body.email !== null &&
	    req.body.id !== null &&
	    req.body.ime !== null &&
	    req.body.datum !== null &&
	    req.body.opis !== null) {
    		ctrlKrmilnikZaAvtentikacijoUporabnika.avtenticirajSejo(req,res,dodajVnosDone);
	}
	else {
		prikaziObvestiloONapaki(req, res, 500);
	}
};

/*
* DODAJ VNOS DONE
* Pomen: 
*	- Klice metodo modela koledar, ki doda vnos v bazo
* Parametri:
*	- email
*	- id
*	- ime
*	- datum
*	- opis
* Return:
*	- status
*/
var dodajVnosDone = function(req, res, retVal)
{
	if(retVal === 200)
	{
	var params = {
    			url: req.protocol + "://" + req.get('host') + '/api/koledar/dodajVnos',
    			method: 'POST',
    			json: {
    				email: req.cookies.StraightAs.split("|")[0],
    				id: req.body.trajanje,
    				ime: req.body.ime,
    				datum: req.body.datum,
    				opis: req.body.opis,
    				cas: req.body.cas
    			}
    		}
    		request(params, function(error, response, content) {
    			if (response.statusCode === 201) {
    				res.status(201).redirect("/");
    			}
    			else {
    				prikaziObvestiloONapaki(req, res, 500);
    			}
    		});
	}
	else
	{
		prikaziObvestiloONapaki(req, res, 500);
	}
}

/*
* PRIKAZI OBVESTILO O NAPAKI
* Pomen: 
*	- Posreduje sporocilo o napaki s statusom
* Parametri:
*	- req
*	- res
*	- statusCode
* Return:
*	- objekt
*/
var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};
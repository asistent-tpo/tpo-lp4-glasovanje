var ctrlKrmilnikZaAvtentikacijoUporabnika = require('../controllers/KrmilnikZaAvtentikacijoUporabnika');

var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	if(statusCode === 406)
	{
		content = "Password is too short: 8 chars min";
	}
	res.render('error', {
		title: title,
		content: content
	});
};

module.exports.registrirajSe = function(req, res)
{
    console.log("registrirajSe()");
    var retVal = ctrlKrmilnikZaAvtentikacijoUporabnika.izvediRegistracijo(req,res,registrirajSeDone);
    
};

var registrirajSeDone = function(req,res,retVal)
{
	if(retVal !== 201)
    {
    	console.log("napakaa");
    	prikaziObvestiloONapaki(req,res,retVal);
    }
    else
    {
    	console.log("returned,redirectinr");
    	res.redirect("/register");
    }
}
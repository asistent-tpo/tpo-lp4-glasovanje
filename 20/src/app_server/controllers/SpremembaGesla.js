var ctrlKrmilnikZaAvtentikacijoUporabnika = require('../controllers/KrmilnikZaAvtentikacijoUporabnika');

var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};

module.exports.spremeniGeslo = function(req, res)
{
    console.log("spremeniGeslo()");
    var retVal = ctrlKrmilnikZaAvtentikacijoUporabnika.spremeniGeslo(req,res,spremeniGesloDone);
    
};

var spremeniGesloDone = function(req,res,retVal)
{
	if(retVal !== 200)
    {
    	console.log("napakaa");
    	prikaziObvestiloONapaki(req,res,retVal);
    }
    else
    {
    	console.log("returned,redirectinr");
    	res.redirect("/");
    }
}
"use strict"

var request = require('request');
var ctrlKrmilnikZaAvtentikacijoUporabnika = require("./KrmilnikZaAvtentikacijoUporabnika");

module.exports.index = function(req, res) {
    ctrlKrmilnikZaAvtentikacijoUporabnika.avtenticirajSejo(req,res,indexDone);
};

var indexDone = function(req, res, retVal)
{
    if(retVal === 200)
    {
        
        console.log("getting TODOs");
        var path = '/api/todo/vrniVseVnose'
        if(req.cookies.StraightAs === undefined)
        {
        	res.redirect("/login");
        }
		var data = {
		    email: req.cookies.StraightAs.split("|")[0]
		}
		var params = {
			url: req.protocol + "://" + req.get('host') + path,
			method: 'POST',
			json: data
		}
		request(params, function(error, response, content) {
			if (response.statusCode === 201) {
			    console.log("response todo get");
			    console.log(response.body.rows);
				var todoRows = response.body.rows;
				var path = '/api/koledar/vrniVseVnose';
				var data = {
				    email: req.cookies.StraightAs.split("|")[0]
				}
				var params = {
					url: req.protocol + "://" + req.get('host') + path,
					method: 'POST',
					json: data
				}
				request(params, function(error, response, content) {
					if (response.statusCode === 201) {
					    console.log("response koledar get");
					    console.log(response.body.rows);
					    var koledarRows = response.body.rows;
					    
						var path = '/api/urnik/vrniVseVnose';
						var data = {
						    email: req.cookies.StraightAs.split("|")[0]
						}
						var params = {
							url: req.protocol + "://" + req.get('host') + path,
							method: 'POST',
							json: data
						}
						request(params, function(error, response, content) {
							if (response.statusCode === 201) {
							    console.log("response urnik get");
							    console.log(response.body.rows);
							    var urnikRows = response.body.rows;
							    
								res.render('HomePogled', {todoRows: todoRows, koledarRows: koledarRows, urnikRows: urnikRows});
								
								
								
							}
							else {
								prikaziObvestiloONapaki(req, res, response.statusCode);
							}
						});
						
					}
					else {
						prikaziObvestiloONapaki(req, res, response.statusCode);
					}
				});
				
			}
			else {
				prikaziObvestiloONapaki(req, res, response.statusCode);
			}
		});
        
	    
    }
    else
    {
        res.redirect("/login");
    }
}

var prikaziObvestiloONapaki = function(req, res, statusCode) {
	var title, content;
	title = statusCode + ", something went wrong!";
	content = "Something is not working";
	res.status(statusCode);
	res.render('error', {
		title: title,
		content: content
	});
};
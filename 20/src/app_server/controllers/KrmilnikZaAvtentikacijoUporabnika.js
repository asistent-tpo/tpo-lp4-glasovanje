"use strict"

var request = require('request');

/**
 * Metoda odpre obrazec za prijavo
 */
module.exports.PrijavniObrazec = function(req, res) {
	res.render('PrijavniObrazec', {title: 'hello'});
};

/**
 * Metoda odpre obrazec za registracijo
 */
module.exports.RegistracijskiObrazec = function(req, res) {
	res.render('RegistracijskiObrazec', {title: 'hello'});
};

/**
 * Metoda odpre obrazec za spremembo gesla
 */
module.exports.SpremembaGeslaObrazec = function(req, res) {
	var email = req.cookies.StraightAs.split("|")[0];
	res.render('SpremembaGeslaObrazec', {email: email});
};

/**
 * Metoda izvede registracijo novega uporabnika
 */
module.exports.izvediRegistracijo = function(req, res, callbck)
{
	console.log("izvediRegistracijo()");
	console.log(req.body);
    if (req.body.email === undefined) {
        console.log("undefinedddd");
		callbck(req,res,500);
		return 500;
	}
	else 
	{
		if (req.body.email !== undefined) 
		{
			var path = '/api/uporabnik/preveriVeljavnostEmaila';
			var data =
			{
				email: req.body.email
			}
			var params = 
			{
				url: req.protocol + "://" + req.get('host') + path,
				method: 'POST',
				json: data
			}
			console.log("making request to /preveriVeljavnostEmaila");
			request(params, function(error, response, content) 
			{
			    console.log(params);
			    if(response !== undefined)
			    {
					if (response.statusCode === 200) 
					{
						console.log("done,preveriVeljavnostGesla");
						var path = '/api/uporabnik/preveriVeljavnostGesla';
						var data =
						{
							geslo1: req.body.geslo,
							geslo2: req.body.geslo2
						}
						var params = 
						{
							url: req.protocol + "://" + req.get('host') + path,
							method: 'POST',
							json: data
						}
						console.log("making request to /preveriVeljavnostGesla");
						request(params, function(error, response, content) 
						{
			    			console.log(params);
			    			if(response !== undefined)
			    			{
								if (response.statusCode === 200) 
								{
									console.log("done,dodajUporabnikaVSeznamNepotrjenih");
									var path = '/api/uporabnik/dodajUporabnika'
									var data = 
									{
										email: req.body.email,
										geslo: req.body.geslo
									}
									var params = 
									{
										url: req.protocol + "://" + req.get('host') + path,
										method: 'POST',
										json: data
									}
									console.log("making request to /register");
									request(params, function(error, response, content) 
									{
			    						console.log(params);
			    						if(response !== undefined)
			    						{
											if (response.statusCode === 201) 
											{
												console.log("done,redirect");
												var zeton1 = response.body;
												
												
												console.log("done,preveriposljiiPotrditveniEmail");
												var path = '/api/uporabnik/posljiPotrditveniEmail';
												var data =
												{
													email: req.body.email,
													zeton: zeton1
												}
												var params = 
												{
													url: req.protocol + "://" + req.get('host') + path,
													method: 'POST',
													json: data
												}
												console.log("making request to /posljiPotrditveniEmail");
												request(params, function(error, response, content) 
												{
									    			console.log(params);
									    			if(response !== undefined)
									    			{
														if (response.statusCode === 200) 
														{
															console.log("done, sent mail");
															callbck(req,res,201);
														}
														else
														{
															return response.statusCode;
														}
									    			}
									    			else
									    			{
									    			    console.log("request undefined");
									    			}
												});
												
												
												
												return 201;
											}
											else
											{
												callbck(req,res,response.statusCode);
												return response.statusCode;
											}
			    						}
			    						else
			    						{
			        						console.log("request undefined");
			    						}
									});
								}
								else
								{
									callbck(req,res,response.statusCode);
									return response.statusCode;
								}
			    			}
			    			else
			    			{
								callbck(req,res,500);
			    			    console.log("request undefined");
			    			}
						});
						
					}
					else
					{
						callbck(req,res,response.statusCode);
						return response.statusCode;
					}
			    }
			    else
			    {
					callbck(req,res,500);
			        console.log("request undefined");
			    }
			});
			
		}
		else 
		{
			callbck(req,res,500);
			return 500;
		}
	}
}

/**
 * Metoda izvede prijavo uporabnika
 */
module.exports.izvediPrijavo = function(req, res, callbck)
{
	console.log("izvediPrijavo()");
	console.log(req.body);
    if (req.body.email === undefined) {
        console.log("undefinedddd");
		callbck(req,res,500);
		return 500;
	}
	else {
		if (req.body.email !== undefined) {
			
			var path = '/api/uporabnik/preveriUporabnika'
			var data = {
				email: req.body.email
			}
			var params = {
				url: req.protocol + "://" + req.get('host') + path,
				method: 'POST',
				json: data
			}
			console.log("making request to /preveriUporabnika");
			request(params, function(error, response, content) {
			    console.log(params);
			    if(response !== undefined)
			    {
				if (response.statusCode === 200) {
					console.log("done,redirect");
					var path = '/api/uporabnik/preveriGeslo'
					var data = {
						email: req.body.email,
						geslo: req.body.geslo
					}
					var params = {
						url: req.protocol + "://" + req.get('host') + path,
						method: 'POST',
						json: data
					}
					console.log("making request to /preveriGeslo");
					request(params, function(error, response, content) {
					    console.log(params);
					    if(response !== undefined)
					    {
						if (response.statusCode === 200) {
							console.log("done,redirect");
							
							var path = '/api/uporabnik/shraniDostopniZeton';
							var data =
							{
								email: req.body.email,
								date: new Date().toISOString()
							}
							var params = 
							{
								url: req.protocol + "://" + req.get('host') + path,
								method: 'POST',
								json: data
							}
							console.log("making request to /shraniDostopniZeton");
							request(params, function(error, response, content) 
							{
				    			console.log(params);
				    			if(response !== undefined)
				    			{
									if (response.statusCode === 200) 
									{
										console.log("done, saved zeton");
										var token = response.body;
										console.log("got nice little token " + token);
										res.cookie('StraightAs',token);
										callbck(req,res,200);
									}
									else
									{
										return response.statusCode;
									}
				    			}
				    			else
				    			{
				    			    console.log("request undefined");
				    			}
							});
							
							return 200;
						}
						else {
							callbck(req,res,response.statusCode);
							return response.statusCode;
						}
					    }
					    else
					    {
					        console.log("request undefined");
					    }
					});
					return 200;
				}
				else {
					callbck(req,res,response.statusCode);
					return response.statusCode;
				}
			    }
			    else
			    {
			        console.log("request undefined");
			    }
			});
			
			
		}
		else {
			callbck(req,res,500);
			return 500;
		}
	}
};

/**
 * Metoda uporabniku spremeni geslo
 */
module.exports.spremeniGeslo = function(req,res,callbck)
{
	console.log("spremeniGeslo()");
	console.log(req.body);
    if (req.body.staro_geslo === undefined) {
        console.log("undefinedddd");
		callbck(req,res,500);
		return 500;
	}
	else {
		if (req.body.staro_geslo !== undefined) {
			console.log("kukies");
			console.log(req.cookies);
			console.log(req.cookies.StraightAs);
			var email = req.cookies.StraightAs.split("|")[0];
			console.log(email);
			
			var path = '/api/uporabnik/preveriGeslo'
			var data = {
				email: email,
				geslo: req.body.staro_geslo
			}
			var params = {
				url: req.protocol + "://" + req.get('host') + path,
				method: 'POST',
				json: data
			}
			console.log("making request to /preveriGeslo");
			request(params, function(error, response, content) {
			    console.log(params);
			    if(response !== undefined)
			    {
				if (response.statusCode === 200) {
					console.log("done,redirect");
					var path = '/api/uporabnik/preveriVeljavnostGesla'
					var data = {
						geslo1: req.body.novo_geslo,
						geslo2: req.body.novo_geslo2
					}
					var params = {
						url: req.protocol + "://" + req.get('host') + path,
						method: 'POST',
						json: data
					}
					console.log("making request to /preveriVeljavnostGesla");
					request(params, function(error, response, content) {
					    console.log(params);
					    if(response !== undefined)
					    {
						if (response.statusCode === 200) {
							console.log("done,redirect");
							
							var path = '/api/uporabnik/spremeniGeslo';
							var data =
							{
								email: email,
								geslo: req.body.novo_geslo
							}
							var params = 
							{
								url: req.protocol + "://" + req.get('host') + path,
								method: 'POST',
								json: data
							}
							console.log("making request to /spremeniGeslo");
							request(params, function(error, response, content) 
							{
				    			console.log(params);
				    			if(response !== undefined)
				    			{
									if (response.statusCode === 200) 
									{
										console.log("done, spremenil geslo");
										callbck(req,res,200);
									}
									else
									{
										return response.statusCode;
									}
				    			}
				    			else
				    			{
				    			    console.log("request undefined");
				    			}
							});
							
							return 200;
						}
						else {
							callbck(req,res,response.statusCode);
							return response.statusCode;
						}
					    }
					    else
					    {
					        console.log("request undefined");
					    }
					});
					return 200;
				}
				else {
					callbck(req,res,response.statusCode);
					return response.statusCode;
				}
			    }
			    else
			    {
			        console.log("request undefined");
			    }
			});
			
			
		}
		else {
			callbck(req,res,500);
			return 500;
		}
	}
};

/**
 * Metoda avtenticira sejo s preverjanjem dostopnega žetona v piškotku
 */
module.exports.avtenticirajSejo = function(req,res,callbck)
{
	console.log("avtenticirajSejo()");
	console.log(req.body);
    if (req.cookies.StraightAs === undefined) {
        console.log("undefinedddd");
		callbck(req,res,500);
		return 500;
	}
	else {
		if (req.cookies.StraightAs !== undefined) {
			console.log("kukies");
			console.log(req.cookies);
			console.log(req.cookies.StraightAs);
			var email = req.cookies.StraightAs.split("|")[0];
			console.log(email);
			
			var path = '/api/uporabnik/preveriDostopniZeton'
			var data = {
				email: email,
				zeton: req.cookies.StraightAs
			}
			var params = {
				url: req.protocol + "://" + req.get('host') + path,
				method: 'POST',
				json: data
			}
			console.log("making request to /preveriDostopniZeton");
			request(params, function(error, response, content) {
			    console.log(params);
			    if(response !== undefined)
			    {
				if (response.statusCode === 200) {
					console.log("done, authenticated");
					callbck(req,res,200);
					return 200;
				}
				else {
					callbck(req,res,response.statusCode);
					return response.statusCode;
				}
			    }
			    else
			    {
					callbck(req,res,500);
			        console.log("request undefined");
			    }
			});
			
			
		}
		else {
			callbck(req,res,500);
			return 500;
		}
	}
};
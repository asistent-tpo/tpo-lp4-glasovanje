document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid', 'interaction' ],
          
          dateClick: function(info) {
            
    // change the day's background color just for fun
    $('#calendarFormDate').val(info.dateStr);
    info.dayEl.style.backgroundColor = 'red';
    $('#calendarForm').collapse('show');
  },
  
          eventClick: function(info) {

    // change the border color just for fun
    info.el.style.borderColor = 'red';
    console.log(info.event);
    $('#calendarFormEditIme').val(info.event.title);
    $('#calendarFormEditDatum').val(info.event.extendedProps.datum.split("~")[0]);
    $('#calendarFormEditOpis').val(info.event.extendedProps.opis);
    $('#calendarFormEditTrajanje').val(info.event.extendedProps.trajanje);
    $('#calendarFormEditTime').val(info.event.extendedProps.cas + ":00");
    console.log(info.event.extendedProps);
    $('#calendarFormEdit').collapse('show');
  }
  
        });
        
        for(var nI = 0; nI < koledarRows.length; nI++)
        {
          console.log("row");
          console.log(koledarRows[nI]);
          if(koledarRows[nI].datum === null)
          {
            koledarRows[nI].datum = "2019-05-05~12:12";
          }
          var cascas = koledarRows[nI].datum.split("~")[1];
          var datumdatum = koledarRows[nI].datum.split("~")[0];
          if(cascas === undefined)
          {
            cascas = "00:00";
          }
          var newEvent = 
          {
            title: koledarRows[nI].ime,
            start: datumdatum,
            end: Date.parse(datumdatum) + koledarRows[nI].id*3600000 + (cascas.split(":")[0] * 3600000) + (cascas.split(":")[1] * 60000),
            id: koledarRows[nI].id,
            extendedProps: {opis: koledarRows[nI].opis, datum: koledarRows[nI].datum, trajanje: koledarRows[nI].id, cas: cascas}
          }
          calendar.addEvent(newEvent);
        }

        calendar.render();
        
         var calendarEl2 = document.getElementById('calendar2');

        var calendar2 = new FullCalendar.Calendar(calendarEl2, {
          plugins: [ 'dayGrid', 'interaction' ],
          defaultView: 'dayGridWeek',
          
          dateClick: function(info) {
            
    // change the day's background color just for fun
    $('#calendar2FormDate').val(info.dateStr);
    info.dayEl.style.backgroundColor = 'red';
    $('#calendar2Form').collapse('show');
  },
  
          eventClick: function(info) {

    // change the border color just for fun
    info.el.style.borderColor = 'red';
    console.log(info.event);
    $('#calendar2FormEditIme').val(info.event.title);
    $('#calendar2FormEditDatum').val(info.event.extendedProps.barva.split("~")[0]);
    $('#calendar2FormEditTrajanje').val(info.event.extendedProps.trajanje);
    $('#calendar2FormEditTime').val(info.event.extendedProps.cas);
    $('#calendar2FormEditId').val(info.event.id);
    console.log(info.event.extendedProps);
    $('#calendar2FormEdit').collapse('show');
  }
          
        });
        
        for(var nI = 0; nI < urnikRows.length; nI++)
        {
          console.log("rowu");
          console.log(urnikRows[nI]);
          if(urnikRows[nI].barva === null)
          {
            urnikRows[nI].barva = "2019-05-05~12:12";
          }
          var cascas = urnikRows[nI].barva.split("~")[1];
          var datumdatum = urnikRows[nI].barva.split("~")[0];
          if(cascas === undefined)
          {
            cascas = "00:00";
          }
          var newEvent = 
          {
            title: urnikRows[nI].ime,
            start: new Date(Date.parse(datumdatum) + (cascas.split(":")[0] * 3600000) + (cascas.split(":")[1] * 60000)).toISOString(),
            end: new Date(Date.parse(datumdatum) + urnikRows[nI].trajanje*3600000 + (cascas.split(":")[0] * 3600000) + (cascas.split(":")[1] * 60000)).toISOString(),
            id: urnikRows[nI].id,
            extendedProps: {barva: urnikRows[nI].barva, trajanje: urnikRows[nI].trajanje, cas: cascas}
          }
          console.log(newEvent);
          console.log(Date.parse(datumdatum));
          console.log(Date.parse(datumdatum) + urnikRows[nI].trajanje*3600000);
          calendar2.addEvent(newEvent);
        }

        calendar2.render();
      });
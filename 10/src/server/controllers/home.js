var request = require('request');
var helpers = require('../helpers/helpers');
var CheckAdmin = require('../helpers/verifyToken');
var CheckUpravljalec = require('../helpers/verifyToken');

// Server se spremeni za REST klice
var paramsApi = {
	server: 'http://localhost:3000',
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production')
	paramsApi.server = 'https://tpo-ci-cd-team10.herokuapp.com';

// Render login page
module.exports.home = function(req, res) {
	CheckAdmin.isUserAdmin(req, function(isAdmin){
		CheckUpravljalec.isUserUpravljalec(req, function(isUserUrednik){
			var reqParams = {
				url: paramsApi.server + paramsApi.apiURI + "/currentUser",
				method: 'get',
				headers: {
					'x-access-token': req.cookies['token']
				},
				json: {}
			};
			
			request(reqParams, function(err, response, content) {
				if (err || response.statusCode != 200) {
					helpers.renderError(res, helpers.error400);
				} else {
					var content1 = [];
					var content2 = [];
					var content3 = [];
					
					for(var i = 0; i < content.todo.length; i++){
						if(i % 3 == 0){
							content1.push(content.todo[i]);
						}else if(i % 3 == 1){
							content2.push(content.todo[i]);
						}else if(i % 3 == 2){
							content3.push(content.todo[i]);
						}
					}
					
					res.render('home', {
						title: 'Home',
						isUserAdmin: isAdmin,
						isUserUrednik: isUserUrednik,
						content: content,
						content1Todo: content1,
						content2Todo: content2,
						content3Todo: content3
					});
				}
			});
		});
	});
};

// Send admin message 
module.exports.sendMessageToAll = function(req, res) {
	CheckAdmin.isUserAdmin(req, function(isAdmin){
		if(isAdmin){
			if(req.body == null || req.body.message == null || req.body.message.length < 6){
				helpers.renderError(res, helpers.error400);
				return;	
			}
			
			var reqParams = {
				url: paramsApi.server + paramsApi.apiURI + "/adminMessage",
				method: 'post',
				headers: {
					'x-access-token': req.cookies['token']
				},
				json: {
					'message': req.body.message
				}
			};
			
			request(reqParams, function(err, response, content) {
				if (err) {
					helpers.renderError(res, helpers.error400);
				} else {
					if (response.statusCode == 200) {
						res.redirect('/home');
					}
				}
			});
		}else{
			helpers.renderError(res, helpers.error403);
		}
	});	
};
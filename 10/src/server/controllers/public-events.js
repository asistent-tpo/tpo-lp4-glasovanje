var request = require('request');
var helpers = require('../helpers/helpers');
var CheckUpravljalec = require('../helpers/verifyToken');
var VerifyToken = require('../helpers/verifyToken');

// Server se spremeni za REST klice
var paramsApi = {
	server: 'http://localhost:3000',
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://tpo-ci-cd-team10.herokuapp.com';
}

// Render add page for upravljalec
module.exports.previewEventsUpravljalec = function(req, res) {
	CheckUpravljalec.isUserUpravljalec(req, function(isUpravljalec){
		if(isUpravljalec){
			var reqParams = {
				url: paramsApi.server + paramsApi.apiURI + "/publicEvents",
				method: 'get',
				headers: {
					'x-access-token': req.cookies['token']
				},
				json: {}
			};
			
			request(reqParams, function(err, response, content) {
				if (err) {
					console.log(err);
					helpers.renderError(res, helpers.error400);
				} else {
					if (response.statusCode == 200) {
							res.render('urednik_events', {
							title: 'Urednik events',
							content: content
						});
					} else {
						helpers.renderError(res, helpers.error400);
					}
				}
			});
		}else{
			helpers.renderError(res, helpers.error403);
		}
	});
};

// Event preview for upravljalec
module.exports.upravljalecAdd = function(req, res) {
	CheckUpravljalec.isUserUpravljalec(req, function(isUserUrednik){
		if(isUserUrednik){
			if(req.body == null || req.body.name == null || req.body.organizer == null || req.body.description == null || req.body.date == null || req.body.time == null){
				helpers.renderError(res, helpers.error400);
				return;	
			}

			try{
				var dateArr = req.body.date.split("-");
				var timeArr = req.body.time.split(":");
				var d = new Date();
				d.setYear(parseInt(dateArr[0]));
				d.setMonth(parseInt(dateArr[1])-1);
				d.setDate(parseInt(dateArr[2]));
				d.setHours(parseInt(timeArr[0]));
				d.setMinutes(parseInt(timeArr[1]));
				req.body.date = d;
			}catch(err){
				helpers.renderError(res, helpers.error400);
				return;
			}
			
			var reqParams = {
				url: paramsApi.server + paramsApi.apiURI + "/publicEvents",
				method: 'post',
				headers: {
					'x-access-token': req.cookies['token']
				},
				json: req.body
			};
			
			request(reqParams, function(err, response, content) {
				if (err) {
					console.log(err);
					helpers.renderError(res, helpers.error400);
				} else {
					if (response.statusCode == 200) {
						res.redirect('/events/preview');
					} else {
						helpers.renderError(res, helpers.error400);
					}
				}
			});
		}else{
			helpers.renderError(res, helpers.error403);
		}
	});	
};

// Render all events
module.exports.allPublicEvents = function(req, res) {
	VerifyToken.isUserLoggedIn(req, function(bool){
		CheckUpravljalec.isUserUpravljalec(req, function(isUserUrednik){
			var reqParams = {
					url: paramsApi.server + paramsApi.apiURI + "/publicEvents",
					method: 'get',
					headers: {
						'x-access-token': req.cookies['token'],
					},
					json: {}
				};
				
			request(reqParams, function(err, response, content) {
				if (err) {
					console.log(err);
					helpers.renderError(res, helpers.error400);
				} else {
					var content1 = [];
					var content2 = [];
					var content3 = [];
					
					for(var i = 0; i < content.length; i++){
						if(i % 3 == 0){
							content1.push(content[i]);
						}else if(i % 3 == 1){
							content2.push(content[i]);
						}else if(i % 3 == 2){
							content3.push(content[i]);
						}
					}
					
					if (response.statusCode == 200) {
							res.render('public_events', {
							title: 'Public events',
							content1: content1,
							content2: content2,
							content3: content3,
							isUserUrednik: isUserUrednik,
							isUserLoggedIn: bool
						});
					} else {
						helpers.renderError(res, helpers.error400);
					}
				}
			});
		});
	});
};
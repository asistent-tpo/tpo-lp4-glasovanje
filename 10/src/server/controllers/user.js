var request = require('request');
var helpers = require('../helpers/helpers');
var CheckUpravljalec = require('../helpers/verifyToken');

// Server se spremeni za REST klice
var paramsApi = {
	server: 'http://localhost:3000',
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://tpo-ci-cd-team10.herokuapp.com';
}

// Render login page
module.exports.login = function(req, res) {
	res.render('entry', {
		title: 'Entry'
	});
};

// Check if post is valid from login
module.exports.loginCheck = function(req, res) {
	if (isLoginValid(req)) {
		var reqParams = {
			url: paramsApi.server + paramsApi.apiURI + "/users/login",
			method: 'post',
			json: {
				email: req.body.email,
				password: req.body.password
			}
		};

		request(reqParams, function(err, response, content) {
			if (err) {
				helpers.renderError(res, helpers.error400);
			} else {
				if (response.statusCode == 200) {
					res.cookie("token", content.token, {
						maxAge: 900000,
						httpOnly: false
					});

					res.redirect('/home');
				} else {
					helpers.renderError(res, helpers.error400);
				}
			}
		});
	} else {
		helpers.renderError(res, helpers.error400);
	}
};

// Check if post is valid from login
module.exports.registerCheck = function(req, res) {
	if(req.body.password_repeat == null || req.body.password != req.body.password_repeat){
		helpers.renderError(res, helpers.error400);
		return;	
	}
	
	if (isLoginValid(req)) {
		var reqParams = {
			url: paramsApi.server + paramsApi.apiURI + "/users/register",
			method: 'post',
			json: {
				email: req.body.email,
				password: req.body.password
			}
		};

		request(reqParams, function(err, response, content) {
			if (err) {
				helpers.renderError(res, helpers.error400);
			} else {
				if (response.statusCode == 200) {

					res.cookie("token", content.token, {
						maxAge: 900000,
						httpOnly: false
					});

					res.redirect('/home');
				} else {
					helpers.renderError(res, helpers.error400);
				}
			}
		});
	} else {
		helpers.renderError(res, helpers.error400);
	}
};

// Render Settings page
module.exports.settings = function(req, res) {
	CheckUpravljalec.isUserUpravljalec(req, function(isUserUrednik){
		res.render('settings', {
			title: 'Settings',
			isUserUrednik: isUserUrednik
		});
	});
};

// Check if post is valid for password change
module.exports.settingsCheck = function(req, res) {
	if(isChangePasswordValid(req)) {
		var reqParams = {
			headers: {
				'x-access-token': req.cookies['token']
			},
			url: paramsApi.server + paramsApi.apiURI + "/users/updatepassword",
			method: 'post',
			json: {
				oldpassword: req.body.oldpassword,
				newpassword: req.body.newpassword
			}
		};
		
		request(reqParams, function(err, response, content) {
			if (err) {
				helpers.renderError(res, helpers.error500);
			} else {
				if (response.statusCode == 200) {
					res.redirect('/home');
				} else {
					helpers.renderError(res, helpers.error400);
				}
			}
		});
	}else{
		helpers.renderError(res, helpers.error400);
	}
};

// User requested logout -> destroy token
module.exports.logout = function(req, res) {
	res.cookie("token", '', {
		maxAge: 900000,
		httpOnly: false
	});
	res.redirect('/entry');
};


// Helper methods

function isLoginValid(req) {
	if (req.body == null || req.body.email == null || req.body.password == null) {
		return false;
	}

	if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(req.body.email))) {
		return false;
	}

	return true;
}

function isChangePasswordValid(req) {
	if (req.body == null || req.body.oldpassword == null || 
		req.body.newpassword == null || req.body.newpassword_repeat == null || 
		req.body.newpassword_repeat != req.body.newpassword) {
			
		return false;
	}
	
	return true;
}
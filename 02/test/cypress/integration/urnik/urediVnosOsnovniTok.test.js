describe('Edit timetable entry', function () {
    const subject = 'TPO';
    const duration = 3;
    const color = '#FF851B';
    const colorInRgb = 'rgb(255, 133, 27)';

    const subjectNew = 'SP';
    const colorNew = '#7FDBFF';
    const colorInRgbNew = 'rgb(127, 219, 255)';

    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';

    it ('Edit timetable entry', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        cy.visit('localhost:3000/prijava');
                        cy.get('#emailInput').type(loginEmail);
                        cy.get('#passwordInput').type(loginPassword);
                        cy.get('.btn-primary').click();

                        // delete all entries in timetable
                        cy
                            .request('DELETE', 'localhost:3000/api/urniki/vnosi/izbrisiVse')
                            .then(function() {
                                cy.visit('localhost:3000');
                                cy.wait(1000);

                                // click on first element
                                cy.get('tbody > :nth-child(1) > :nth-child(2)').click();

                                // check if modal window contains correct values
                                cy.get('.modal-header').contains('Course');
                                cy.get('.modal-body').contains('Name');
                                cy.get('.modal-body').contains('Duration');
                                cy.get('.modal-body').contains('Colour');
                                cy.get('.modal-footer').contains('Close');
                                cy.get('.modal-footer').contains('Save changes');

                                // type required fields
                                cy.get(':nth-child(2) > .col-sm-10 > .form-control').type(subject);
                                cy.get(':nth-child(3) > .col-sm-10 > .form-control').type(duration);
                                cy.get(':nth-child(4) > .col-sm-10 > .form-control').select(color);

                                // confirm
                                cy.get('.btn-success').click();

                                // check if data is present on timetable
                                cy
                                    .get(':nth-child(1) > [style="cursor: pointer; background-color: rgb(255, 133, 27);"]')
                                    .contains(subject)
                                    .should('have.css', 'background-color')
                                    .and('eq', colorInRgb);

                                // try to delete entry from timetable
                                cy
                                    .get(':nth-child(1) > [style="cursor: pointer; background-color: rgb(255, 133, 27);"]')
                                    .dblclick();

                                // form is opened, edit fields
                                cy.get(':nth-child(2) > .col-sm-10 > .form-control').clear().type(subjectNew);
                                cy.get(':nth-child(4) > .col-sm-10 > .form-control').select(colorNew);

                                // confirm
                                cy.get('.btn-success').click();

                                // check if edited is present on timetable
                                cy
                                    .get(':nth-child(1) > [style="cursor: pointer; background-color: rgb(127, 219, 255);"]')
                                    .contains(subjectNew)
                                    .should('have.css', 'background-color')
                                    .and('eq', colorInRgbNew);
                            });
                    });
            });
    });
});
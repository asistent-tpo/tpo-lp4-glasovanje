describe('Check bus query by id', function () {
    const queryId = 501021;
    const stationName = "Zmajski most";

    it('Check bus query by id', function() {
        cy
            .request('DELETE', 'localhost:3000/api/avtobusi/postajalisca')
            .then(function() {
                cy
                    .request('POST', 'localhost:3000/api/avtobusi/postajalisca')
                    .then(function() {
                        cy.visit('localhost:3000');
                        cy.get('#navbarNavAltMarkup > :nth-child(1) > :nth-child(4)').click();

                        // Query for the given bus station
                        cy.get('input').type(queryId);
                        cy.get('#iskanje').click();

                        // Check if the list is shown and not empty
                        cy.get('#prihodi .list-group-item').its('length').should('be.gt', 0);

                        // Check that there is only one station displayed
                        cy.get('#prihodi h3').its('length').should('eq', 1);

                        // Check the station data
                        cy.get('#prihodi h3')
                            .contains(stationName);
                        cy.get('#prihodi h3')
                            .contains(queryId);
                    });
            });
    });
});




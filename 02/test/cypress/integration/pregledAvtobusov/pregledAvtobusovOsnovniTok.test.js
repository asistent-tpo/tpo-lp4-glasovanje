describe('Check bus query by name', function () {
    const queryName = "Zmajski most";
    const stationIds = [501021, 501022];

    it('Check bus query by name', function() {

        cy
            .request('DELETE', 'localhost:3000/api/avtobusi/postajalisca')
            .then(function() {
                cy
                    .request('POST', 'localhost:3000/api/avtobusi/postajalisca')
                    .then(function() {
                        cy.visit('localhost:3000');
                        cy.get('#navbarNavAltMarkup > :nth-child(1) > :nth-child(4)').click();

                        // Query for the given bus station
                        cy.get('input').type(queryName);
                        cy.get('#iskanje').click();

                        // Check if the list is shown and not empty
                        cy.get('#prihodi .list-group-item').its('length').should('be.gt', 0);

                        // Check the first station
                        cy.get('#prihodi h3')
                            .first()
                            .contains(queryName);
                            
                        var isInArray = false;
                        for(var id in stationIds) {
                            if(cy.get('#prihodi h3').first().contains(id)) {
                                isInArray = true;
                            }
                        }
                        expect(isInArray).to.be.equal(true);

                        // Check the second station
                        cy.get('#prihodi h3')
                            .last()
                            .contains(queryName);
                            
                        var isInArray = false;
                        for(var id in stationIds) {
                            if(cy.get('#prihodi h3').last().contains(id)) {
                                isInArray = true;
                            }
                        }
                        expect(isInArray).to.be.equal(true);
                    });
            });
    });
});
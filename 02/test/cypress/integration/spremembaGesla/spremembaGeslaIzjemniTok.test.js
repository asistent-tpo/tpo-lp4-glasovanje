describe('Change password izjemni tok', function () {
    const subject = 'TPO';
    const duration = 3;
    const color = '#FF851B';
    const colorInRgb = 'rgb(255, 133, 27)';

    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';
    const newPassword = 'newCypressTestPassword';

    it ('Change password izjemni tok', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        cy.visit('localhost:3000/prijava');
                        cy.get('#emailInput').type('cypress@test.com');
                        cy.get('#passwordInput').type('cypressTestPassword');
                        cy.get('.btn-primary').click();

                        // wait for redirect
                        cy.location('pathname', {timeout: 1000}).should('eq', '/home');

                        // click on user icon
                        cy.get('.ml-auto > .nav-item').click();

                        // select functionality to change password
                        cy.get('[href="/spremeniGeslo"]').click();

                        // check if correct email address is displayed
                        cy.get(':nth-child(2)').contains(loginEmail);

                        // enter WRONG old password and new passwords
                        cy.get(':nth-child(1) > .form-control').type(loginPassword + 'abc');
                        cy.get(':nth-child(2) > .form-control').type(newPassword);
                        cy.get(':nth-child(3) > .form-control').type(newPassword);

                        // confirm changing password
                        cy.get('.btn').click();

                        // check if error message is displayed
                        cy.get('.alert-danger').should('exist');
                    });
            });
    });
});
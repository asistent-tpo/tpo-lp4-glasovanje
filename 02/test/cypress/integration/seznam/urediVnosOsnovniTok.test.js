describe('Uredi vnos v seznamu', function () {
    const description = 'opis1';
    const descriptionNew = 'nov opis1';

    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';

    it ('Uredi vnos v seznamu', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        cy.visit('localhost:3000/prijava');
                        cy.get('#emailInput').type(loginEmail);
                        cy.get('#passwordInput').type(loginPassword);
                        cy.get('.btn-primary').click();

                        // delete all entries in timetable
                        cy
                            .request('DELETE', 'localhost:3000/api/seznami/vnosi/izbrisiVse')
                            .then(function() {
                                cy.visit('localhost:3000');
                                cy.wait(1000);

                                cy.get('#dodajVnosVSeznam').click();

                                // check if modal window contains correct values
                                cy.get('.modal-body').contains('Description: ');
                                cy.get('.modal-footer').contains('Close');
                                cy.get('.modal-footer').contains('Save changes');

                                // type required fields
                                cy.get('.modal-body > :nth-child(2) > :nth-child(2) > .form-control').type(description);

                                // confirm
                                cy.get('.btn-success').click();

                                cy
                                    .get('.seznamPrikazanihOpravil')
                                    .contains(description);

                                cy.get('.card-body > .ml-auto > .fas').click();

                                cy.get('.modal-body').contains('Description: ');
                                cy.get('.modal-footer').contains('Close');
                                cy.get('.modal-footer').contains('Save changes');

                                // type required fields
                                cy.get('.modal-body > :nth-child(2) > :nth-child(2) > .form-control').clear().type(descriptionNew);

                                // confirm
                                cy.get('.btn-success').click();
                            });
                    });
            });
    });
});
describe('Register as a normal user', function() {
    const loginEmail = "imaginary@email.com";
    const loginPassword = "imaginaryPassword";

    it('Register as a normal user', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy.visit('localhost:3000');
                cy.get('#navbarNavAltMarkup > :nth-child(2) > a').first().click();
                cy.get('a[href="/registracija"]').click();
                cy.wait(1000).then(function() {
                    cy.get('#emailInput').type(loginEmail);
                    cy.get('#passwordInput').type(loginPassword);
                    cy.get('#passwordRepeatInput').type(loginPassword);
                    cy.get('input[type="submit"]').click();
                    cy.get('input[type="submit"]').its('disabled').should('be', true);
                    cy.get('div.alert-success').its('length').should('be.gt', 0);
                });
            });
    });
});
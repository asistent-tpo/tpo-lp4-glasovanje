describe('Register as a normal user alternativni tok', function() {
    const name = 'vnos v koledar';
    const duration = 3;
    const time = '23:52';
    const description = 'opis';
    const loginEmail = "imaginary@email.com";
    const loginPassword = "imaginaryPassword";

    it('Register as a normal user alternativni tok', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {

                // Go to home
                cy.visit('localhost:3000');
                cy.get('#navbarNavAltMarkup > :nth-child(1) > a').first().click();

                // Try to add an entry to the calendar
                cy.get('.entry-list > :nth-child(10)').click();
                cy.get('.calendar-footer a').first().click();
                cy.get('.form-group').eq(0).find('input.form-control').type(name);
                cy.get('.form-group').eq(1).find('input.form-control').type(time);
                cy.get('.form-group').eq(2).find('input.form-control').type(duration);
                cy.get('.form-group').eq(3).find('input.form-control').type(description);
                cy.get('button[type="submit"]').click();
                cy.get('div.alert-danger').its('length').should('be.gt', 0);
                cy.get('#register').click();

                // Perform registration
                cy.get('#emailInput').type(loginEmail);
                cy.get('#passwordInput').type(loginPassword);
                cy.get('#passwordRepeatInput').type(loginPassword);
                cy.get('input[type="submit"]').click();
                cy.get('input[type="submit"]').its('disabled').should('be', true);
                cy.get('div.alert-success').its('length').should('be.gt', 0);
            });
    });
});
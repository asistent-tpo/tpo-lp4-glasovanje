describe('Izbrisi vnos iz koledarja', function () {
    const name = 'vnos v koledar';
    const duration = 3;
    const time = '23:52';
    const description = 'opis1';

    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';

    it ('Izbrisi vnos iz koledarja', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        cy.visit('localhost:3000/prijava');
                        cy.get('#emailInput').type(loginEmail);
                        cy.get('#passwordInput').type(loginPassword);
                        cy.get('.btn-primary').click();

                        // delete all entries in timetable
                        cy
                            .request('DELETE', 'localhost:3000/api/koledarji/vnosi/izbrisiVse')
                            .then(function() {
                                cy.visit('localhost:3000');
                                cy.wait(1000);

                                // click on first element
                                cy.get('.entry-list > :nth-child(13)').click();

                                cy.get('#dodajVnosVKoledar').click();

                                // check if modal window contains correct values
                                cy.get('.modal-header').contains('Calendar Event');
                                cy.get('.modal-body').contains('Name: ');
                                cy.get('.modal-body').contains('Time: ');
                                cy.get('.modal-body').contains('Duration: ');
                                cy.get('.modal-body').contains('Description: ');
                                cy.get('.modal-footer').contains('Close');
                                cy.get('.modal-footer').contains('Save changes');

                                // type required fields
                                cy.get(':nth-child(2) > .col-sm-10 > .form-control').type(name);
                                cy.get(':nth-child(3) > .col-sm-10 > .form-control').type(time);
                                cy.get(':nth-child(4) > .col-sm-10 > .form-control').type(duration);
                                cy.get(':nth-child(5) > .col-sm-9 > .form-control').type(description);

                                // confirm
                                cy.get('.btn-success').click();

                                cy
                                    .get('.seznamPrikazanihDogodkov')
                                    .contains(name)
                                    .contains(duration)
                                    .contains(time)
                                    .contains(description);

                                cy.get('.seznamPrikazanihDogodkov > :nth-child(2)').click();

                                cy.get('.btn-success').click();

                                cy.wait(1000);

                                cy
                                    .get('#theRealFooter > :nth-child(1)')
                                    .contains("No events scheduled for this day.");

                            });
                    });
            });
    });
});
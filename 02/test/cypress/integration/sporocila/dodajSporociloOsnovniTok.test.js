describe('Dodaj sporocilo', function () {
    const description = 'Obvestilo uporabniku';

    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';

    it ('Dodaj dogodek', function() {
        cy
            .request('DELETE', 'localhost:3000/api/sporocila/vnosi/izbrisiVse')
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        cy
                            .request('POST','localhost:3000/api/spremeniTip', {
                                "email": "cypress@test.com",
                                "tip": 2
                            })
                            .then(function() {
                                cy.visit('localhost:3000/prijava');
                                cy.get('#emailInput').type(loginEmail);
                                cy.get('#passwordInput').type(loginPassword);
                                cy.get('.btn-primary').click();

                                cy.wait(1000);

                                cy.get('.btn-primary').click();

                                // check if modal window contains correct values
                                cy.get('.modal-body').contains('Message:');
                                cy.get('.modal-footer').contains('Close');
                                cy.get('.modal-footer').contains('Save changes');

                                cy.get(':nth-child(2) > .col-sm-10 > .form-control').type(description);

                                // confirm
                                cy.get('.btn-success').click();

                                cy
                                    .get('#seznam-sporocil > :nth-child(1)')
                                    .contains(description);

                            });
                    });
            });
    });
});
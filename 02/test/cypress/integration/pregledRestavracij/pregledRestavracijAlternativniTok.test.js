describe('Check restaurants without location', function () {

    it('Check restaurants without location', function() {

        cy.visit('localhost:3000');
        
        cy.fixture('location.json').as('fakeLocation');
        cy.get('@fakeLocation').then(function(fakeLocation) {
            
            // Deny location 
            cy
                .visit('localhost:3000/food', {
                    onBeforeLoad (win) {
                        cy
                            .stub(win.navigator.geolocation, 'getCurrentPosition')
                            .callsFake(function(_, error) {
                                    throw error({ code: 1 });
                            });
                    }
                });


            // Reset database for restaurants
            cy
                .request('DELETE', 'localhost:3000/api/restavracije')
                .then(function() {
                    cy
                        .request('POST', 'localhost:3000/api/restavracije')
                        .then(function() {
                            // Get all locations from api
                            cy
                                .request('localhost:3000/api/restavracije')
                                .then(function(data) {

                                    var restaurants = data.body;

                                    // Sort alphabetically
                                    restaurants.sort(function(r1, r2) {
                                        return r1.naziv.localeCompare(r2.naziv)
                                    });

                                    // Compare sorted list and displayed select list
                                    cy.wait(2000);
                                    cy.get('#restaurant-list option')
                                        .each(function(r, ix) {
                                            if(ix > 0) {
                                                expect(r.text()).to.be.equal(restaurants[ix-1].naziv);
                                            }
                                        });

                                });
                        });
                });     

        });

    });

});
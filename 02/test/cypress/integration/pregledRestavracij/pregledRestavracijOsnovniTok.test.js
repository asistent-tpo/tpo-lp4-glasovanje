describe('Check restaurants with location', function () {

    it('Check restaurants with location', function() {

        cy.visit('localhost:3000');
        
        cy.fixture('location.json').as('fakeLocation');
        cy.get('@fakeLocation').then(function(fakeLocation) {
            
            // Accept location 
            cy
                .visit('localhost:3000/food', {
                    onBeforeLoad (win) {
                        cy
                            .stub(win.navigator.geolocation, 'getCurrentPosition')
                            .callsFake(function(callback) {
                                    return callback(fakeLocation);
                            });
                    }
                });


            // Reset database for restaurants
            cy
                .request('DELETE', 'localhost:3000/api/restavracije')
                .then(function() {
                    cy
                        .request('POST', 'localhost:3000/api/restavracije')
                        .then(function() {
                            // Get all locations from api
                            cy
                                .request('localhost:3000/api/restavracije')
                                .then(function(data) {

                                    var restaurants = data.body;

                                    // HELPER FUNCTION
                                    var rad = function(x) {
                                        return x * Math.PI / 180;
                                    };
                                    
                                    // HELPER FUNCTION                    
                                    var getDistance = function(p1, p2) {
                                        var R = 6378137;
                                        var dLat = rad(p2.lat - p1.lat);
                                        var dLong = rad(p2.lng - p1.lng);
                                        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                            Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
                                            Math.sin(dLong / 2) * Math.sin(dLong / 2);
                                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                                        var d = R * c;
                                        return d;
                                    };

                                    // Sort by distance
                                    var fakeLocationTmp = {lat: fakeLocation.coords.latitude, lng: fakeLocation.coords.longitude};
                                    restaurants.sort(function(r1, r2) {
                                        var r1dist = getDistance(r1, fakeLocationTmp);
                                        var r2dist = getDistance(r2, fakeLocationTmp);

                                        if(r1dist > r2dist) {
                                            return 1;
                                        } else if(r2dist > r1dist) {
                                            return -1;
                                        }
                                        return 0;
                                    });

                                    // Compare sorted list and displayed select list
                                    cy.wait(2000);
                                    cy.get('#restaurant-list option')
                                        .each(function(r, ix) {
                                            if(ix > 0) {
                                                expect(r.text()).to.be.equal(restaurants[ix-1].naziv);
                                            }
                                        });

                                });
                        });
                });     

        });

    });

});
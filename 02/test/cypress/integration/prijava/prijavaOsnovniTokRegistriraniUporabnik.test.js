describe('Login as a registered user', function() {
    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';
    const userType = 0;

    it('Login as a registered user', function() {
        cy
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        // Set user type to 0
                        cy
                            .request({
                                method: 'POST',
                                url: 'localhost:3000/api/spremeniTip',
                                form: true,
                                body: {
                                    email: loginEmail,
                                    tip: userType
                                }
                            })
                            .then(function() {
                                // Go to login page
                                cy.visit('localhost:3000');
                                cy.get('#navbarNavAltMarkup > :nth-child(2) > a').first().click();
                                        
                                // Enter credentials
                                cy.get('#emailInput').type(loginEmail);
                                cy.get('#passwordInput').type(loginPassword);

                                // Press login
                                cy.get('input[type="submit"]').click();

                                cy.window().then(function(window) {
                                    var b64Utf8 = function (niz) {
                                        return decodeURIComponent(Array.prototype.map.call(window.atob(niz), function(c) {
                                            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                                        }).join(''));
                                    };
    
                                    cy.wait(2000).then(function() {
                                        // Fetch token info and check it
                                        var token = window.localStorage.getItem('straightas-zeton');
                                        var usefulData = JSON.parse(b64Utf8(token.split('.')[1]));
                                        expect(usefulData.email).to.eq(loginEmail);
                                        expect(usefulData.tipUporabnika).to.eq(userType);
                                    });

                                });
                            });
                    });
            });
    });
});
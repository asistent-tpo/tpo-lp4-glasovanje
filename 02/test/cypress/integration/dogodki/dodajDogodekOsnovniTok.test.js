describe('Dodaj dogodek', function () {
    const name = 'Lavbic Meet & Greet';
    const date = '2019-09-26';
    const dateFormated = '26. 9. 2019';
    const organiser = 'Lavbic';
    const description = '13. vsakoletni Lavbic Meet & Greet v Mercator centru Šiška';

    const loginEmail = 'cypress@test.com';
    const loginPassword = 'cypressTestPassword';

    it ('Dodaj dogodek', function() {
        cy
            .request('DELETE', 'localhost:3000/api/dogodki/vnosi/izbrisiVse')
            .request('localhost:3000/api/izbrisiVseUporabnike')
            .then(function() {
                cy
                    .request('localhost:3000/api/registracija/test')
                    .then(function() {
                        cy
                            .request('POST','localhost:3000/api/spremeniTip', {
                                "email": "cypress@test.com",
                                "tip": 1
                            })
                            .then(function() {
                                cy.visit('localhost:3000/prijava');
                                cy.get('#emailInput').type(loginEmail);
                                cy.get('#passwordInput').type(loginPassword);
                                cy.get('.btn-primary').click();

                                cy.wait(1000);

                                cy.get('.btn-primary').click();

                                // check if modal window contains correct values
                                cy.get('.modal-header').contains('Create Event');
                                cy.get('.modal-body').contains('Name: ');
                                cy.get('.modal-body').contains('Date: ');
                                cy.get('.modal-body').contains('Organiser: ');
                                cy.get('.modal-body').contains('Description: ');
                                cy.get('.modal-footer').contains('Close');
                                cy.get('.modal-footer').contains('Save changes');

                                // type required fields
                                cy.get(':nth-child(2) > .col-sm-10 > .form-control').type(name);
                                cy.get(':nth-child(3) > .col-sm-10 > .form-control').type(date);
                                cy.get(':nth-child(4) > .col-sm-10 > .form-control').type(organiser);
                                cy.get(':nth-child(5) > .col-sm-10 > .form-control').type(description);

                                // confirm
                                cy.get('.btn-success').click();

                                cy
                                    .get('#seznam-dogodkov > :nth-child(1) > .mt-2 > .card-header > .row > :nth-child(1) > :nth-child(1)')
                                    .contains(name);

                                cy
                                    .get('#seznam-dogodkov > :nth-child(1) > .mt-2 > .card-header > .row > :nth-child(2) > :nth-child(1)')
                                    .contains(dateFormated);

                                cy
                                    .get('#seznam-dogodkov > :nth-child(1) > .mt-2 > .card-body > :nth-child(1)')
                                    .contains(organiser);

                                cy
                                    .get('#seznam-dogodkov > :nth-child(1) > .mt-2 > .card-body > .row > :nth-child(2) > :nth-child(1)')
                                    .contains(description);

                            });
                    });
            });
    });
});
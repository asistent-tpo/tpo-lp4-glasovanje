# 02. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Bitbucket repozitorij: https://bitbucket.org/dkalsan/tpo-lp4/commits/0fff11c6c937929da0d5236b182eb5213a9ea037

Produkcijska verzija: https://straight-as2.herokuapp.com/

Podatki za prijavo:

email: prijavljeni@straightas.com

geslo: password

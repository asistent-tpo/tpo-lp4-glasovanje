(function() {
    function urediVnosVUrnikuModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, barveKonstanta, izbranaCelica, avtentikacija) {
        var vm = this;

        vm.barve = barveKonstanta;
        vm.podatkiObrazca = {
            'ime': izbranaCelica.vnos.ime,
            'trajanje': izbranaCelica.vnos.trajanje,
            'barva': izbranaCelica.vnos.barva
        };

        vm.modalnoOkno = {
            preklici_urejanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.posodobi_vnos_v_urniku = function() {
            vm.napakaNaObrazcu = "";

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za urejanje vnosa v urniku morate biti prijavljeni!";
                return false;
            }

            if (!vm.podatkiObrazca || !vm.podatkiObrazca.ime || !vm.podatkiObrazca.trajanje || !vm.podatkiObrazca.barva) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            vm.podatkiObrazca.id = izbranaCelica.vnos.id;
            vm.podatkiObrazca.email = avtentikacija.trenutniUporabnik().email;

            console.log(vm.podatkiObrazca);

            $rootScope.$broadcast('urediVnosVUrniku', vm.podatkiObrazca);
            vm.modalnoOkno.zapri(null);
        };
    }

    urediVnosVUrnikuModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'barveKonstanta', 'izbranaCelica', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('urediVnosVUrnikuModalnoOkno', urediVnosVUrnikuModalnoOkno);
})();
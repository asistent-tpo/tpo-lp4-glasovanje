(function() {
    function restavracijeCtrl($scope, straightasPodatki) {
        var vm = this;

        var markRestaurantLocations = function(sortPreference) {
            straightasPodatki.vrniPodatkeVsehRestavracij().then(
                function success(odgovor) {

                    vm.restaurants = odgovor.data;

                    if(sortPreference === 'alphabetically') {
                        vm.restaurants.sort(function(r1, r2) {
                            return r1.naziv.localeCompare(r2.naziv)
                        });
                    } else if(sortPreference === 'distance') {
                        vm.restaurants.sort(function(r1, r2) {
                            var r1dist = google.maps.geometry.spherical.computeDistanceBetween(
                                vm.currentLocation, 
                                new google.maps.LatLng(r1.lat, r1.lng)
                            )
                            var r2dist = google.maps.geometry.spherical.computeDistanceBetween(
                                vm.currentLocation, 
                                new google.maps.LatLng(r2.lat, r2.lng)
                            )

                            if(r1dist > r2dist) {
                                return 1;
                            } else if(r2dist > r1dist) {
                                return -1;
                            }
                            return 0;
                        });
                    }

                    vm.restaurants.forEach(function(restaurant, ix) {
                        var position = {lat: restaurant.lat, lng: restaurant.lng};
                        var marker = new google.maps.Marker({position: position, icon: "https://www.studentska-prehrana.si/Images/iconApple.png", map: vm.map});
                        marker.addListener('click', function() {
                            $scope.$apply(function() {
                                vm.selectedRestaurant = ix;
                            });
                            vm.map.setZoom(16);
                            vm.map.panTo(position);
                        });
                    });

                },
                function error(odgovor) {
                    console.log(odgovor);
                }
            );
        }

        var initMap = function(lat, lng, zoom) {
            vm.currentLocation = new google.maps.LatLng(lat, lng);
            vm.mapOptions = {
                zoom: zoom,
                center: vm.currentLocation
            }
            vm.map = new google.maps.Map(document.getElementById('map'), vm.mapOptions);
        }

        var geoSuccess = function(position) {
            clearTimeout(locationRequestTimeout);
            initMap(position.coords.latitude, position.coords.longitude, 15);
            markRestaurantLocations('distance');
        }

        var geoError = function(error) {
            clearTimeout(locationRequestTimeout);
            showDefaultLocation();
        }

        var showDefaultLocation = function() {
            initMap(46.0489424, 14.5103400, 12);
            markRestaurantLocations('alphabetically');
        }

        var locationRequestTimeout = setTimeout(showDefaultLocation, 2000);

        vm.selectRestaurant = function(ix) {
            var position = {lat: vm.restaurants[ix].lat, lng: vm.restaurants[ix].lng};
            vm.selectedRestaurant = ix;
            vm.map.setZoom(16);
            vm.map.panTo(position);
        }

        navigator.geolocation.getCurrentPosition(geoSuccess, geoError);

        navigator.permissions.query({name:'geolocation'}).then(function(result) {
            if (result.state == 'granted' || result.state == 'denied') {
                clearTimeout(locationRequestTimeout);
            }
        });

    }

    restavracijeCtrl.$inject = ['$scope', 'straightasPodatki'];

    /* global angular */
    angular
        .module('straightas')
        .controller('restavracijeCtrl', restavracijeCtrl);
})();
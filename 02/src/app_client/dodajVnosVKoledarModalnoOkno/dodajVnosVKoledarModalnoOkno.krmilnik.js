(function() {
    function dodajVnosVKoledarModalnoOkno($rootScope, $uibModalInstance, $location, straightasPodatki, izbranDatum, avtentikacija) {
        var vm = this;

        console.log("krmilnik koledar modalno okno");

        vm.jePrijavljen = false;

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_dodajanje = function() {
            vm.napakaNaObrazcu = "";

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za dodajanje vnosa v koledar morate biti prijavljeni!";
                return false;
            } else {
                vm.jePrijavljen = true;
            }

            if (!vm.podatkiObrazca || !vm.podatkiObrazca.ime || !vm.podatkiObrazca.cas || !vm.podatkiObrazca.trajanje || !vm.podatkiObrazca.opis) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            var datum = new Date();
            console.log("tralal cas " + vm.podatkiObrazca.cas);
            datum.setFullYear(izbranDatum.leto, izbranDatum.mesec, izbranDatum.dan);
            datum.setHours(vm.podatkiObrazca.cas.getHours(), vm.podatkiObrazca.cas.getMinutes(),0,0);

            console.log("dodajVnosVKoledarModalnoOkno " + datum);

            var podatki = {
                email: avtentikacija.trenutniUporabnik().email,
                ime: vm.podatkiObrazca.ime,
                datum: datum,
                opis: vm.podatkiObrazca.opis,
                trajanje: vm.podatkiObrazca.trajanje
            };

            straightasPodatki.dodajVnosVKoledar(podatki).then(
                function success(odgovor) {
                    console.log("success dodaj vnos v koledar modlano okno");
                    console.log(odgovor.data);
                    $rootScope.$broadcast('dodajVnosVKoledar', odgovor.data);
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
            vm.modalnoOkno.zapri(null);
        };

        vm.preusmeri_na_registracijo = function() {
            vm.modalnoOkno.zapri(null);
            $location.path('/registracija');
        };
    
        vm.preusmeri_na_prijavo = function() {
            vm.modalnoOkno.zapri(null);
            $location.path('/prijava');
        };
    }

    dodajVnosVKoledarModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', '$location', 'straightasPodatki', 'izbranDatum', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('dodajVnosVKoledarModalnoOkno', dodajVnosVKoledarModalnoOkno);
})();
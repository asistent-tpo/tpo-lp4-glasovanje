(function() {
    function sporocilaCtrl($scope, straightasPodatki) {
        var vm = this;

        vm.seznamSporocil= [];

        straightasPodatki.vrniVsaSporocila({}).then(
            function success(odgovor) {
                console.log("success");
                vm.seznamSporocil = odgovor.data;

                console.log(vm.seznamSporocil);


                }, function error(odgovor) {
                    console.log("fail");
                });

    }

    sporocilaCtrl.$inject = ['$scope', 'straightasPodatki'];
    /* global angular */
    angular
        .module('straightas')
        .controller('sporocilaCtrl', sporocilaCtrl);
})();
(function() {
    function urediVnosVKoledarModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, vnosKoledarja, avtentikacija) {
        var vm = this;

        console.log("krmilnik uredi koledar modalno okno");

        vm.podatkiObrazca = {
            ime: null,
            cas: null,
            opis: null,
            trajanje: null
        };

        vm.podatkiObrazca.ime = vnosKoledarja.vnos.ime;
        vm.podatkiObrazca.opis = vnosKoledarja.vnos.opis;
        vm.podatkiObrazca.cas = new Date(vnosKoledarja.vnos.datum);
        vm.podatkiObrazca.trajanje = vnosKoledarja.vnos.trajanje;

        console.log("!!!!!!!!!!!!!!!!!!!!!!");
        console.log(vnosKoledarja.vnos.ime);


        vm.modalnoOkno = {
            preklici_urejanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_urejanje = function() {
            console.log("potrdi urejanje");
            vm.napakaNaObrazcu = "";

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za urejanje vnosa v koledarju morate biti prijavljeni!";
                return false;
            }

            console.log(vm.podatkiObrazca.ime + " " + vm.podatkiObrazca.cas);

            if (!vm.podatkiObrazca || !vm.podatkiObrazca.ime || !vm.podatkiObrazca.cas || !vm.podatkiObrazca.trajanje || !vm.podatkiObrazca.opis) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            var temp = new Date(vnosKoledarja.vnos.datum);
            temp.setHours(vm.podatkiObrazca.cas.getHours());
            temp.setMinutes(vm.podatkiObrazca.cas.getMinutes());

            var podatki = {
                id: vnosKoledarja.vnos._id,
                email: avtentikacija.trenutniUporabnik().email,
                ime: vm.podatkiObrazca.ime,
                datum: temp,
                opis: vm.podatkiObrazca.opis,
                trajanje: vm.podatkiObrazca.trajanje
            };

            straightasPodatki.urediVnosVKoledar(podatki).then(
                function success(odgovor) {
                    console.log("success");
                    $rootScope.$broadcast('urediVnosVKoledar', podatki);
                }, function error(odgovor) {
                    console.log("fail");
                    console.log(odgovor);
                }
            );
            vm.modalnoOkno.zapri(null);
        };
    }

    urediVnosVKoledarModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'vnosKoledarja', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('urediVnosVKoledarModalnoOkno', urediVnosVKoledarModalnoOkno);
})();
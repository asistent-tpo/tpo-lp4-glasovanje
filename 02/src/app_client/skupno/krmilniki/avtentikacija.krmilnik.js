(function() {
    function avtentikacijaCtrl(straightasPodatki, avtentikacija, $location, $routeParams, $rootScope) {
        var vm = this;

        vm.obrazec = {};
        vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();

        vm.izvedi_registracijo = function() {

            if(vm.obrazec.geslo1 !== vm.obrazec.geslo2) {
                vm.error = "Gesli se ne ujemata";
            } else if(!vm.obrazec.email || !vm.obrazec.geslo1 || !vm.obrazec.geslo2) {
                vm.error = "Vsa polja so obvezna";
            } else {
                vm.message = "Registracija v teku";
                straightasPodatki.izvediRegistracijo({email: vm.obrazec.email, geslo: vm.obrazec.geslo1}).then(
                    function success(odgovor) {
                        vm.message = "";
                        vm.error = "";
                        vm.success = odgovor.data.message;
                    }, function error(odgovor) {
                        vm.success = "";
                        vm.message = "";
                        vm.error = odgovor.data.error;
                    }
                );
            }

        };

        vm.izvedi_prijavo = function() {

            if(!vm.obrazec.email || !vm.obrazec.geslo) {
                vm.error = "Vsa polja so obvezna";
            } else {
                vm.message = "Prijava v teku";
                straightasPodatki.izvediPrijavo(vm.obrazec).then(
                    function success(odgovor) {
                        vm.message = "";
                        vm.error = "";
                        vm.success = "Prijava uspešna.";
                        avtentikacija.shraniZeton(odgovor.data.zeton);
                        vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();

                        $rootScope.$broadcast('updateNavbar', {});

                        switch(vm.trenutniUporabnik.tipUporabnika) {
                            // Registrirani uporabnik
                            case 0:
                                $location.path('/home');
                                break;
                            // Upravljalec dogodkov
                            case 1:
                                $location.path('/dogodkiNadzornaPlosca');
                                break;
                            // Administrator
                            case 2:
                                $location.path('/sporocilaNadzornaPlosca');
                                break;    
                        }

                    }, function error(odgovor) {
                        vm.success = "";
                        vm.message = "";
                        vm.error = odgovor.data.error;
                    }
                );
            }

        };

        vm.spremeni_geslo = function() {

            vm.obrazec.uporabnikov_email = avtentikacija.trenutniUporabnik().email;

            if (!vm.obrazec.uporabnikov_email || !vm.obrazec.staro_geslo || !vm.obrazec.novo_geslo || !vm.obrazec.novo_geslo_repeat) {
                vm.error = "Vsa polja so obvezna";
            } else {
                if (vm.obrazec.novo_geslo !== vm.obrazec.novo_geslo_repeat) {
                    vm.error = "Novi gesli se ne ujemata!";
                    return;
                }

                if (vm.obrazec.novo_geslo.length < 8) {
                    vm.error = "Novo geslo mora imeti vsaj 8 znakov!";
                    return;
                }

                vm.message = "Sprememba gesla v teku";

                straightasPodatki.preveriGeslo({
                    'email': vm.obrazec.uporabnikov_email,
                    'geslo': vm.obrazec.staro_geslo
                }).then(
                    function success(odgovor) {
                        straightasPodatki.preveriVeljavnostGesla({
                            'novo_geslo': vm.obrazec.novo_geslo,
                            'novo_geslo_repeat': vm.obrazec.novo_geslo_repeat
                        }).then(
                            function success(odgovor) {
                                straightasPodatki.spremeniGeslo({
                                    'email': vm.obrazec.uporabnikov_email,
                                    'geslo': vm.obrazec.novo_geslo
                                }).then(
                                    function success(odgovor) {
                                        vm.message = "";
                                        vm.error = "";
                                        vm.success = "Sprememba gesla uspešna.";
                                        vm.obrazec = {};
                                    }, function error(odgovor) {
                                        prikazi_obvestilo_o_napaki(odgovor.data);
                                    }
                                );
                            }, function error(odgovor) {
                                prikazi_obvestilo_o_napaki(odgovor.data);
                            }
                        );
                    }, function error(odgovor) {
                        prikazi_obvestilo_o_napaki('Email in geslo se ne ujemata!');
                    }
                );
            }
        };

        if($routeParams.potrditveniZeton) {
            straightasPodatki.potrdiElektronskiNaslov($routeParams.potrditveniZeton).then(
                function success(odgovor){
                    vm.success = odgovor.data.message;
                },
                function error(odgovor) {
                    vm.error = odgovor.data.error;
                });
        }

        var prikazi_obvestilo_o_napaki = function(napaka) {
            vm.error = napaka;
        };

        /**
         * Metoda avtenticiraj_sejo je nepotrebna, saj se ta logika
         * izvaja na strani strežnika. Pošiljamo zgolj JWT token
         */

    }

    // TODO: inject $location
    avtentikacijaCtrl.$inject = ['straightasPodatki', 'avtentikacija', '$location', '$routeParams', '$rootScope'];

    /* global angular */
    angular
        .module('straightas')
        .controller('avtentikacijaCtrl', avtentikacijaCtrl);
})();
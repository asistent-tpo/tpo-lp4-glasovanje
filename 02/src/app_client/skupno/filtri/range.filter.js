(function() {

    var range = function() {
        return function(input, from, to) {
            to = parseInt(to,10);
            from = parseInt(from,10);
            for (var i = from; i < to; i++){
                input.push(i);
            }
            return input;
        };
    };

    /* global angular */
    angular
        .module('straightas')
        .filter('range', range);
})();
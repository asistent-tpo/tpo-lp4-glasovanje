(function(){
    var scopeRefresh = function() {
        var safeApply = function ($scope, fn) {
            var phase = $scope.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && typeof fn === 'function') {
                    fn();
                }
            } else {
                $scope.$apply(fn);
            }
        };

        return {
            safeApply: safeApply
        };
    };

    scopeRefresh.$inject = [];

    /* global angular */
    angular
        .module('straightas')
        .service('scopeRefresh', scopeRefresh);
})();
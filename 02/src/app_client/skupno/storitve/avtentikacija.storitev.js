(function() {
    function avtentikacija($window) {
        var b64Utf8 = function (niz) {
            return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        };

        var shraniZeton = function(zeton) {
            $window.localStorage['straightas-zeton'] = zeton;
        };
        
        var vrniZeton = function() {
            return $window.localStorage['straightas-zeton'];
        };

        var jePrijavljen = function() {
            var zeton = vrniZeton();
            if (zeton) {
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                return koristnaVsebina.datumPoteka > Date.now() / 1000;
            } else {
                return false;
            }
        };

        var trenutniUporabnik = function() {
            if (jePrijavljen()) {
                var zeton = vrniZeton();
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                console.log(koristnaVsebina);
                return {
                    email: koristnaVsebina.email,
                    tipUporabnika: koristnaVsebina.tipUporabnika,
                    id: koristnaVsebina._id
                };
            }
        };

        var odjava = function() {
            $window.localStorage.removeItem('straightas-zeton');
        };
        
        return {
            shraniZeton: shraniZeton,
            vrniZeton: vrniZeton,
            jePrijavljen: jePrijavljen,
            trenutniUporabnik: trenutniUporabnik,
            odjava: odjava
        };
    }
    avtentikacija.$inject = ['$window'];
    
    /* global angular */
    angular
        .module('straightas')
        .service('avtentikacija', avtentikacija);
  })();
(function(){
    var straightasPodatki = function($http, avtentikacija) {
        var vnosiVUrniku = function(podatki) {
            return $http.post('/api/urniki', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var dodajVnosVUrnik = function(podatki) {
            return $http.post('/api/urniki/vnosi', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var posodobiVnosVUrniku = function(podatki) {
            return $http.put('/api/urniki/vnosi', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var izbrisiVnosIzUrnika = function(podatki) {
            return $http.delete('/api/urniki/vnosi', {
                data: podatki,
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var dodajVnosVKoledar = function (podatki) {
            console.log("straightas dodajVnosVKoledar");
            return $http.post('/api/koledarji/vnosi', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var vrniVnoseUporabnikaKoledar = function (podatki) {
            console.log("straightas dodajVnoseUporabnika");
            return $http.post('/api/koledarji', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var izbrisiVnosIzKoledarja = function (podatki) {
            console.log("straightas izbrisiVnosIzKoledarja");
            console.log(podatki);
            return $http.delete('/api/koledarji/vnosi', {
                data: podatki,
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var urediVnosVKoledar = function(podatki) {
            console.log("straightas urediVnosVKoledar");
            return $http.put('/api/koledarji/vnosi', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var izvediRegistracijo = function(podatki) {
            return $http.post('/api/registracija', podatki);
        };
        
        var izvediPrijavo = function(podatki) {
            return $http.post('/api/prijava', podatki);
        };

        var preveriGeslo = function(podatki) {
            return $http.post('/api/preveriGeslo', podatki);
        };

        var preveriVeljavnostGesla = function(podatki) {
            return $http.post('/api/preveriVeljavnostGesla', podatki);
        };

        var spremeniGeslo = function(podatki) {
            return $http.post('/api/spremeniGeslo', podatki);
        };

        var vrniVnoseUporabnikaSeznam = function (podatki) {
            console.log("straightas dodajVnoseUporabnika");
            return $http.post('/api/seznami', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var vrniPodatkeVsehRestavracij = function() {
            return $http.get('/api/restavracije');
        };

        var vnosiVSeznamu = function(podatki) {
            return $http.post('/api/seznami', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var dodajVnosVSeznam = function(podatki) {
            return $http.post('/api/seznami/vnosi', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var izbrisiVnosIzSeznama = function (podatki) {
            console.log("straightas izbrisiVnosIzSeznama");
            console.log(podatki);
            return $http.delete('/api/seznami/vnosi', {
                data: podatki,
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var urediVnosVSeznam = function(podatki) {
            console.log("straightas urediVnosVSeznam");
            podatki._id = podatki.id;

            console.log("da vidim kaj so podatki v urediVnosSeznam funkciji preden gre v backend");
            console.log(podatki);

            return $http.put('/api/seznami/vnosi', podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var potrdiElektronskiNaslov = function(podatki) {
            return $http.get('/api/potrditev/' + podatki);
        };

        var pridobiPodatkePostaje = function(podatki) {
            return $http.post('/api/avtobusi', podatki);
        };

        var vrniVseDogodke = function (podatki) {
            return $http.post('/api/dogodki',  podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var dodajDogodek = function (podatki) {
            return $http.post('/api/dogodki/vnosi',  podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };


        var vrniVsaSporocila = function (podatki) {
            return $http.post('/api/sporocila',  podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        var dodajSporocilo = function (podatki) {
            return $http.post('/api/sporocila/vnosi',  podatki, {
                headers: {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                }
            });
        };

        return {
            dodajSporocilo: dodajSporocilo,
            vrniVsaSporocila: vrniVsaSporocila,
            dodajDogodek: dodajDogodek,
            vrniVseDogodke: vrniVseDogodke,
            vnosiVUrniku: vnosiVUrniku,
            dodajVnosVUrnik: dodajVnosVUrnik,
            posodobiVnosVUrniku: posodobiVnosVUrniku,
            dodajVnosVKoledar: dodajVnosVKoledar,
            izbrisiVnosIzUrnika: izbrisiVnosIzUrnika,
            vrniVnoseUporabnikaKoledar: vrniVnoseUporabnikaKoledar,
            izbrisiVnosIzKoledarja: izbrisiVnosIzKoledarja,
            urediVnosVKoledar: urediVnosVKoledar,
            izvediRegistracijo: izvediRegistracijo,
            izvediPrijavo: izvediPrijavo,
            preveriGeslo: preveriGeslo,
            preveriVeljavnostGesla: preveriVeljavnostGesla,
            spremeniGeslo: spremeniGeslo,
            vrniVnoseUporabnikaSeznam: vrniVnoseUporabnikaSeznam,
            vrniPodatkeVsehRestavracij: vrniPodatkeVsehRestavracij,
            vnosiVSeznamu: vnosiVSeznamu,
            dodajVnosVSeznam: dodajVnosVSeznam,
            potrdiElektronskiNaslov: potrdiElektronskiNaslov,
            izbrisiVnosIzSeznama: izbrisiVnosIzSeznama,
            pridobiPodatkePostaje: pridobiPodatkePostaje,
            urediVnosVSeznam: urediVnosVSeznam
        };
    };

    straightasPodatki.$inject = ['$http', 'avtentikacija'];
    
    /* global angular */
    angular
        .module('straightas')
        .service('straightasPodatki', straightasPodatki);
})();
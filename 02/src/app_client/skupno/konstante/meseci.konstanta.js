(function(){

    var meseciKonstanta = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ]
    
    /* global angular */
    angular
        .module('straightas')
        .constant('meseciKonstanta', meseciKonstanta);
})();
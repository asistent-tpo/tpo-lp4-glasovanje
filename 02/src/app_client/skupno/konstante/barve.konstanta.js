(function(){
    var barveKonstanta = [
        '#001F3F',
        '#0074D9',
        '#7FDBFF',
        '#39CCCC',
        '#FFDC00',
        '#DDDDDD',
        '#FF851B',
        '#3D9970',
        '#AAAAAA',
        '#B10DC9'
    ];

    /* global angular */
    angular
        .module('straightas')
        .constant('barveKonstanta', barveKonstanta);
})();
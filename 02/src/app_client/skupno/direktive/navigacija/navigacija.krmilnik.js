(function() {

    var navigacija = function ($window) {

        return {
            restrict: 'EA',
            templateUrl: '/skupno/direktive/navigacija/navigacija.predloga.html',
            controller: ['$scope', '$location', '$window', 'avtentikacija', function ($scope, $location, $window, avtentikacija) {

                if (avtentikacija.trenutniUporabnik() == undefined) {
                    $scope.tipUserja = 0;
                } else {
                    $scope.tipUserja = avtentikacija.trenutniUporabnik().tipUporabnika;
                }

                $scope.isOpen = false;
                
                $scope.isActive = function(location) {
                    return location === $location.path()
                }
                $scope.isLoggedIn = function() {
                    return avtentikacija.jePrijavljen();
                }
                $scope.logOut = function() {
                    avtentikacija.odjava();
                    $window.location.assign('/prijava');
                }
                $scope.toggle = function() {
                    $scope.isOpen = !$scope.isOpen;
                }


                $scope.$on('updateNavbar', function(event, data) {
                    console.log("updateNavbar");
                    if (avtentikacija.trenutniUporabnik() == undefined) {
                        $scope.tipUserja = 0;
                    } else {
                        $scope.tipUserja = avtentikacija.trenutniUporabnik().tipUporabnika;
                    }
                });

            }]
        };
    };

    navigacija.$inject = ['$window'];

    /* global angular */
    angular
        .module('straightas')
        .directive('navigacija', navigacija);
})();
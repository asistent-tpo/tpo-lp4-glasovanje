(function() {
    var ngRightClick = function ($parse) {
        return function(scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function(event) {
                scope.$apply(function() {
                    event.preventDefault();
                    fn(scope, {$event:event});
                });
            });
        };
    };

    ngRightClick.$inject = ['$parse'];

    /* global angular */
    angular
        .module('straightas')
        .directive('ngRightClick', ngRightClick);
})();
(function() {
    function dogodkiCtrl($scope, straightasPodatki) {
        var vm = this;

        vm.seznamDogodkov = [];

        straightasPodatki.vrniVseDogodke({}).then(
            function success(odgovor) {
                console.log("success");
                vm.seznamDogodkov = odgovor.data;

                console.log(vm.seznamDogodkov);

                for (var i = 0; i < vm.seznamDogodkov.length; i++) {
                    var datum = new Date(vm.seznamDogodkov[i].datum);
                    var dan = datum.getDate();
                    var mesec = datum.getMonth() + 1;
                    var leto = datum.getFullYear();
                    vm.seznamDogodkov[i].urejenDatum = dan + ". " + mesec + ". " + leto;
                }

                console.log(vm.seznamDogodkov);
                }, function error(odgovor) {
                    console.log("fail");
                });

    }

    dogodkiCtrl.$inject = ['$scope', 'straightasPodatki'];
    /* global angular */
    angular
        .module('straightas')
        .controller('dogodkiCtrl', dogodkiCtrl);
})();
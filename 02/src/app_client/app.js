(function(){
    function nastavitev($routeProvider, $locationProvider) {
        $routeProvider
            .when('/home', {
                templateUrl: '/home/home.pogled.html',
                controller: 'homeCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: true,
                    allowLoggedIn: true
                }
            })
            .when('/prijava', {
                templateUrl: '/prijava/prijava.pogled.html',
                controller: 'avtentikacijaCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: true,
                    allowLoggedIn: false
                }
            })
            .when('/prijava/:potrditveniZeton', {
                templateUrl: '/prijava/prijava.pogled.html',
                controller: 'avtentikacijaCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: true,
                    allowLoggedIn: false
                }
            })
            .when('/registracija', {
                templateUrl: '/registracija/registracija.pogled.html',
                controller: 'avtentikacijaCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: true,
                    allowLoggedIn: false
                }
            })
            .when('/food', {
                templateUrl: '/pregledRestavracij/pregledRestavracij.pogled.html',
                controller: 'restavracijeCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: true,
                    allowLoggedIn: true
                }
            })
            .when('/spremeniGeslo', {
                templateUrl: '/spremeniGeslo/spremeniGeslo.pogled.html',
                controller: 'avtentikacijaCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: false,
                    allowLoggedIn: true
                }
            })
            .when('/bus', {
                templateUrl: '/pregledAvtobusov/pregledAvtobusov.pogled.html',
                controller: 'avtobusiCtrl',
                controllerAs: 'vm'
            })
            .when('/dogodki', {
                templateUrl: '/dogodki/dogodki.pogled.html',
                controller: 'dogodkiCtrl',
                controllerAs: 'vm'
            })
            .when('/dogodkiNadzornaPlosca', {
                templateUrl: '/upravljalecDogodkovDomacaStran/dogodkiNadzornaPlosca.pogled.html',
                controller: 'dogodkiNadzornaPloscaCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: false,
                    allowLoggedIn: true
                }
            })
            .when('/sporocila', {
                templateUrl: '/sporocila/sporocila.pogled.html',
                controller: 'sporocilaCtrl',
                controllerAs: 'vm'
            })
            .when('/sporocilaNadzornaPlosca', {
                templateUrl: '/upravljalecSporocilDomacaStran/sporocilaNadzornaPlosca.pogled.html',
                controller: 'sporocilaNadzornaPloscaCtrl',
                controllerAs: 'vm',
                access: {
                    allowLoggedOut: false,
                    allowLoggedIn: true
                }
            })
            .otherwise({redirectTo: '/home'});
        $locationProvider.html5Mode(true);
    }

    run.$inject = ['$rootScope', '$location', '$window', 'avtentikacija'];
    function run($rootScope, $location, $window, avtentikacija) {

        $rootScope.$on('$routeChangeStart', function(event, next, current) {
            if (!next.$$route || !next.$$route.access) {
                return;
            }

            var access = next.$$route.access;

            if (avtentikacija.jePrijavljen() && !access.allowLoggedIn) {
                $location.path("/home");
            } else if (!avtentikacija.jePrijavljen() && !access.allowLoggedOut) {
                $location.path("/prijava");
            }
        });
    }

    /* global angular */
    angular
        .module('straightas', ['ngRoute', 'ui.bootstrap'])
        .config(['$routeProvider', '$locationProvider', nastavitev])
        .run(run);
})();
(function() {
    function sporocilaModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, avtentikacija) {
        var vm = this;

        console.log("krmilnik sporocilaModalnoOkno");

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_dodajanje = function() {
            vm.napakaNaObrazcu = "";



            if (!vm.podatkiObrazca ||  !vm.podatkiObrazca.opis) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            var podatki = {
                opis: vm.podatkiObrazca.opis
            };

            straightasPodatki.dodajSporocilo(podatki).then(
                function success(odgovor) {
                    console.log("success sporocilo uspesno dodan");
                    console.log(odgovor.data);
                    $rootScope.$broadcast('dodajVnosMedSporocila', odgovor.data);
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
            vm.modalnoOkno.zapri(null);
            console.log(podatki);

        };

    }

    sporocilaModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('sporocilaModalnoOkno', sporocilaModalnoOkno);
})();
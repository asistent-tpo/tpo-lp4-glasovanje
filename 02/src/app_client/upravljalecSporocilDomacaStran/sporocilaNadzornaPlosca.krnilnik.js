(function() {
    function sporocilaNadzornaPloscaCtrl($scope, straightasPodatki, $uibModal, avtentikacija, $location) {
        var vm = this;

        switch(avtentikacija.trenutniUporabnik().tipUporabnika) {
            // Registrirani uporabnik
            case 0:
                $location.path('/home');
                break;
            // Administrator
            case 1:
                $location.path('/dogodkiNadzornaPlosca');
                break;
        }

        vm.seznamSporocil = [];

        console.log(avtentikacija.trenutniUporabnik());

        if (avtentikacija.jePrijavljen() && avtentikacija.trenutniUporabnik().tipUporabnika == 2) {

            straightasPodatki.vrniVsaSporocila({}).then(
                function success(odgovor) {
                    console.log("success");
                    vm.seznamSporocil = odgovor.data;

                    console.log(vm.seznamSporocil);

                }, function error(odgovor) {
                    console.log("fail");
                }
            );
        }

        vm.dodaj_sporocilo = function () {
            console.log("dodaj_sporocilo");

            $uibModal.open({
                templateUrl: '/upravljalecSporocilDomacaStran/sporocilaModalnoOkno.pogled.html',
                controller: 'sporocilaModalnoOkno',
                controllerAs: 'vm'
            });

        }

        $scope.$on('dodajVnosMedSporocila', function(event, data) {
            console.log('dodajVnosMedSporocila');
            console.log(data);
            vm.seznamSporocil.push(data);
        });

    }

    sporocilaNadzornaPloscaCtrl.$inject = ['$scope', 'straightasPodatki', '$uibModal', 'avtentikacija', '$location'];
    /* global angular */
    angular
        .module('straightas')
        .controller('sporocilaNadzornaPloscaCtrl', sporocilaNadzornaPloscaCtrl);
})();
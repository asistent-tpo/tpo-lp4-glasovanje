(function() {
    function dodajVnosVSeznamModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, izbranDatum, avtentikacija) {
        var vm = this;

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_dodajanje = function() {
            vm.napakaNaObrazcu = "";

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za dodajanje vnosa v seznam morate biti prijavljeni!";
                return false;
            }

            if (!vm.podatkiObrazca || !vm.podatkiObrazca.opis) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            var podatki = {
                email: avtentikacija.trenutniUporabnik().email,
                opis: vm.podatkiObrazca.opis
            };

            straightasPodatki.dodajVnosVSeznam(podatki).then(
                function success(odgovor) {
                    $rootScope.$broadcast('dodajVnosVSeznam', odgovor.data);
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
            vm.modalnoOkno.zapri(null);
        };
    }

    dodajVnosVSeznamModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'izbranDatum', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('dodajVnosVSeznamModalnoOkno', dodajVnosVSeznamModalnoOkno);
})();
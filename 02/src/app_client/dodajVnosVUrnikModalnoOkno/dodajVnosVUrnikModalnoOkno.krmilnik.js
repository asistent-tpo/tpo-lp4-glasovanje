(function() {
    function dodajVnosVUrnikModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, izbranaCelica, barveKonstanta, avtentikacija, $location) {
        var vm = this;

        vm.barve = barveKonstanta;
        vm.jePrijavljen = false;

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_dodajanje = function() {
            vm.napakaNaObrazcu = "";

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za dodajanje vnosa v urnik morate biti prijavljeni!";
                return false;
            } else {
                vm.jePrijavljen = true;
            }

            if (!vm.podatkiObrazca || !vm.podatkiObrazca.ime || !vm.podatkiObrazca.trajanje || !vm.podatkiObrazca.barva) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            vm.podatkiObrazca.id = izbranaCelica.izbranaCelica;
            vm.podatkiObrazca.email = avtentikacija.trenutniUporabnik().email;

            $rootScope.$broadcast('dodajVnosVUrnik', vm.podatkiObrazca);
            vm.modalnoOkno.zapri(null);
        };

        vm.preusmeri_na_registracijo = function() {
            vm.modalnoOkno.zapri(null);
            $location.path('/registracija');
        };

        vm.preusmeri_na_prijavo = function() {
            vm.modalnoOkno.zapri(null);
            $location.path('/prijava');
        };
    }

    dodajVnosVUrnikModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'izbranaCelica', 'barveKonstanta', 'avtentikacija', '$location'];

    /* global angular */
    angular
        .module('straightas')
        .controller('dodajVnosVUrnikModalnoOkno', dodajVnosVUrnikModalnoOkno);
})();
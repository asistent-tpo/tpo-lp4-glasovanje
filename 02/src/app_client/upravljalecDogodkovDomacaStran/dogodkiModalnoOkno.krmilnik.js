(function() {
    function dogodkiModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, avtentikacija) {
        var vm = this;

        console.log("krmilnik dogodkiModalnoOkno");

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_dodajanje = function() {
            vm.napakaNaObrazcu = "";



            if (!vm.podatkiObrazca || !vm.podatkiObrazca.ime || !vm.podatkiObrazca.datum || !vm.podatkiObrazca.organizator || !vm.podatkiObrazca.opis) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            var today = new Date();
            console.log(today);

            if (vm.podatkiObrazca.datum.getFullYear() < today.getFullYear()) {
                vm.napakaNaObrazcu = "Neveljaven datum";
                return false;
            } else if (vm.podatkiObrazca.datum.getFullYear() == today.getFullYear()) {
                if (vm.podatkiObrazca.datum.getMonth() < today.getMonth()) {
                    vm.napakaNaObrazcu = "Neveljaven datum";
                    return false;
                } else if (vm.podatkiObrazca.datum.getMonth() == today.getMonth()) {
                    if (vm.podatkiObrazca.datum.getDate() < today.getDate()) {
                        vm.napakaNaObrazcu = "Neveljaven datum";
                        return false;
                    }
                }
            }

            var podatki = {
                ime: vm.podatkiObrazca.ime,
                datum: vm.podatkiObrazca.datum,
                organizator: vm.podatkiObrazca.organizator,
                opis: vm.podatkiObrazca.opis
            };

            straightasPodatki.dodajDogodek(podatki).then(
                function success(odgovor) {
                    console.log("success dogodek uspesno dodan");
                    console.log(odgovor.data);
                    $rootScope.$broadcast('dodajVnosMedDogodke', odgovor.data);
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
            vm.modalnoOkno.zapri(null);
            console.log(podatki);

        };

    }

    dogodkiModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('dogodkiModalnoOkno', dogodkiModalnoOkno);
})();
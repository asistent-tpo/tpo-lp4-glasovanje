(function() {
    function dogodkiNadzornaPloscaCtrl($scope, straightasPodatki, $uibModal, avtentikacija, $location) {
        var vm = this;

        switch(avtentikacija.trenutniUporabnik().tipUporabnika) {
            // Registrirani uporabnik
            case 0:
                $location.path('/home');
                break;
            // Administrator
            case 2:
                $location.path('/sporocilaNadzornaPlosca');
                break;
        }

        vm.seznamDogodkov = [];

        console.log(avtentikacija.trenutniUporabnik());

        if (avtentikacija.jePrijavljen() && avtentikacija.trenutniUporabnik().tipUporabnika == 1) {

            straightasPodatki.vrniVseDogodke({}).then(
                function success(odgovor) {
                    console.log("success");
                    vm.seznamDogodkov = odgovor.data;

                    console.log(vm.seznamDogodkov);

                    for (var i = 0; i < vm.seznamDogodkov.length; i++) {
                        var datum = new Date(vm.seznamDogodkov[i].datum);
                        var dan = datum.getDate();
                        var mesec = datum.getMonth() + 1;
                        var leto = datum.getFullYear();
                        vm.seznamDogodkov[i].urejenDatum = dan + ". " + mesec + ". " + leto;
                    }

                    console.log(vm.seznamDogodkov);
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
        }

        vm.dodaj_dogodek = function () {
            console.log("dodaj_dogodek");

            $uibModal.open({
                templateUrl: '/upravljalecDogodkovDomacaStran/dogodkiModalnoOkno.pogled.html',
                controller: 'dogodkiModalnoOkno',
                controllerAs: 'vm'
            });

        }

        $scope.$on('dodajVnosMedDogodke', function(event, data) {
            console.log('dodajVnosMedDogodke');
            console.log(data);
            var datum = new Date(data.datum);
            var dan = datum.getDate();
            var mesec = datum.getMonth() + 1;
            var leto = datum.getFullYear();
            data.urejenDatum= dan + ". " + mesec + ". " + leto;
            vm.seznamDogodkov.push(data);
        });

    }

    dogodkiNadzornaPloscaCtrl.$inject = ['$scope', 'straightasPodatki', '$uibModal', 'avtentikacija', '$location'];
    /* global angular */
    angular
        .module('straightas')
        .controller('dogodkiNadzornaPloscaCtrl', dogodkiNadzornaPloscaCtrl);
})();
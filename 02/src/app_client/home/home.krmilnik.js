(function() {
    function homeCtrl($uibModal, avtentikacija) {
        var vm = this;

        vm.jePrijavljen = avtentikacija.jePrijavljen();
        vm.trenutniUporabnik = avtentikacija.trenutniUporabnik();

        vm.izbranaCelica = 0;
        vm.vnos = null;
        vm.izberi_celico_v_urniku = function(id, mode, vnos) {
            vm.izbranaCelica = id;
            vm.vnos = vnos;

            switch (mode) {
                case 0:
                    vm.dodaj_vnos_v_urnik();
                    break;
                case 1:
                    vm.odpri_vnos_urnika();
                    break;
                case 2:
                    vm.izbrisi_vnos_iz_urnika();
                    break;
            }
        };

        vm.dodaj_vnos_v_urnik = function() {
            $uibModal.open({
                templateUrl: '/dodajVnosVUrnikModalnoOkno/dodajVnosVUrnikModalnoOkno.pogled.html',
                controller: 'dodajVnosVUrnikModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    izbranaCelica: function() {
                        return {
                            izbranaCelica: vm.izbranaCelica
                        };
                    }
                }
            });
        };


        vm.dodaj_vnos_v_seznam = function() {
                 $uibModal.open({
                    templateUrl: '/dodajVnosVSeznamModalnoOkno/dodajVnosVSeznamModalnoOkno.pogled.html',
                    controller: 'dodajVnosVSeznamModalnoOkno',
                    controllerAs: 'vm',
                    resolve: {
                        izbranDatum: function() {
                            return {
                                dan: izbranDan
                            };
                        }
                    }
                });
        };



        vm.odpri_vnos_urnika = function() {
            $uibModal.open({
                templateUrl: '/urediVnosVUrnikuModalnoOkno/urediVnosVUrnikuModalnoOkno.pogled.html',
                controller: 'urediVnosVUrnikuModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    izbranaCelica: function() {
                        return {
                            vnos: vm.vnos
                        };
                    }
                }
            });
        };


        vm.izbrisi_vnos_iz_urnika = function() {
            $uibModal.open({
                templateUrl: '/izbrisiVnosIzUrnikaModalnoOkno/izbrisiVnosIzUrnikaModalnoOkno.pogled.html',
                controller: 'izbrisiVnosIzUrnikaModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    izbranaCelica: function() {
                        return {
                            vnos: vm.vnos
                        };
                    }
                }
            });
        };

    }

    homeCtrl.$inject = ['$uibModal', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('homeCtrl', homeCtrl);
})();
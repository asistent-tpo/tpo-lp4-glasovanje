(function() {
    function koledarCtrl(meseciKonstanta, $uibModal, $scope, $rootScope, $route, straightasPodatki, avtentikacija) {
        var vm = this;

        // A better modulo operator
        var mod = function(x, n) {
            return (x % n + n) % n
        }

        // Variable initialization
        vm.seznamDogodkov = null;
        var today = new Date();
        var monthOffset = 0;
        var izbranDan = -1;

        var uporabnikoviDogodki = null;

        if (avtentikacija.jePrijavljen()) {
            console.log("Uporabnik je prijavljen");
            straightasPodatki.vrniVnoseUporabnikaKoledar({email: avtentikacija.trenutniUporabnik().email}).then(
                function success(odgovor) {
                    console.log("success");
                    console.log(odgovor.data);
                    uporabnikoviDogodki = odgovor.data;
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
        } else {
            console.log("Uporabnik ni prijavljen");
        }

        var generateCalendar = function(date) {
            vm.firstDayIx = mod(new Date(date.getFullYear(), date.getMonth()).getDay() - 1, 7);
            vm.numberOfDays = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
            vm.monthName = meseciKonstanta[date.getMonth()];
            vm.year = date.getFullYear();
        };

        var recalculateShownMonth = function(v) {
            monthOffset += v;

            var tmpDate = new Date();
            tmpDate.setDate(1);
            tmpDate.setMonth((today.getMonth() + monthOffset) % 12);
            tmpDate.setFullYear(today.getFullYear() + (today.getMonth() + monthOffset) / 12);
            generateCalendar(tmpDate);
        }

        // Initialize calendar view
        generateCalendar(today);

        vm.generateCalendar = function(date) {
            generateCalendar(date);
        }

        vm.nextMonth = function() {
            recalculateShownMonth(1);
        }

        vm.previousMonth = function() {
            recalculateShownMonth(-1);
        }

        vm.izbran_dan = function (dan) {
            console.log("dan " + (dan + 1));
            // omogoca vnos
            izbranDan = dan;
            vm.seznamDogodkov = [];

            if (uporabnikoviDogodki != null) {
                for (var i = 0; i < uporabnikoviDogodki.length; i++) {

                    var datum = new Date(uporabnikoviDogodki[i].datum);
                    console.log("datum "+ datum);
                    var tempDan = datum.getDate();
                    var tempMesec = datum.getMonth();
                    var tempLeto = datum.getFullYear();

                    //console.log("prebrano dan: " + tempDan + " mesec: " + tempMesec + " leto: " + tempLeto);
                    if (tempDan == dan && tempMesec == (today.getMonth() + monthOffset) % 12 && tempLeto == Math.floor(today.getFullYear() + (today.getMonth() + monthOffset) / 12)) {
                        temp = uporabnikoviDogodki[i];
                        var ure = datum.getHours();
                        var minute = datum.getMinutes();
                        if (ure < 10) {
                            ure = "0" + ure;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        temp.datumStr = ure + ":" + minute;
                        vm.seznamDogodkov.push(temp);
                    } else {
                        //console.log("ni enako dan: " + dan + " mesec: " + (today.getMonth() + monthOffset) % 12 + " leto: " + (today.getFullYear() + (today.getMonth() + monthOffset) / 12));
                    }
                }

                console.log(vm.seznamDogodkov);
            }

        };

        vm.dodaj_vnos_v_koledar = function () {
            console.log("dodaj_vnos_v_koledar");

            var mesec = (today.getMonth() + monthOffset) % 12;
            var leto = Math.floor(today.getFullYear() + (today.getMonth() + monthOffset) / 12);


                $uibModal.open({
                    templateUrl: '/dodajVnosVKoledarModalnoOkno/dodajVnosVKoledarModalnoOkno.pogled.html',
                    controller: 'dodajVnosVKoledarModalnoOkno',
                    controllerAs: 'vm',
                    resolve: {
                        izbranDatum: function() {
                            return {
                                dan: izbranDan,
                                mesec: mesec,
                                leto: leto
                            };
                        }
                    }
                });


        };

        vm.uredi_vnos = function (data) {
            console.log("uredi_vnos_v_koledarju");
            console.log(data);

            $uibModal.open({
                templateUrl: '/urediVnosVKoledarModalnoOkno/urediVnosVKoledarModalnoOkno.pogled.html',
                controller: 'urediVnosVKoledarModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    vnosKoledarja: function() {
                        return {
                            vnos: data
                        };
                    }
                }
            });
        };

        vm.izbrisi_vnos = function (data) {
            console.log("izbrisi vnos");
            console.log(data);

            $uibModal.open({
                templateUrl: '/izbrisiVnosIzKoledarModalnoOkno/izbrisiVnosIzKoledarModalnoOkno.pogled.html',
                controller: 'izbrisiVnosIzKoledarModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    izbrisPodatki: function() {
                        return {
                            id: data._id
                        };
                    }
                }
            });

        };


        $scope.$on('dodajVnosVKoledar', function(event, data) {
            console.log('dodajVnosVKoledar');
            console.log(data);
            //dodamo v tabelo uporabnikovih dogodkov, da ne rabmo se enkrat api-ja klicat
            uporabnikoviDogodki.push(data);

            temp = data;
            //dodamo v tabelo prikazanih dogodkov
            var datum = new Date(data.datum);
            var tempDan = datum.getDate();
            var tempMesec = datum.getMonth();
            var tempLeto = datum.getFullYear();

            var ure = datum.getHours();
            var minute = datum.getMinutes();
            if (ure < 10) {
                ure = "0" + ure;
            }
            if (minute < 10) {
                minute = "0" + minute;
            }
            temp.datumStr = ure + ":" + minute;
            vm.seznamDogodkov.push(temp);

        });

        $scope.$on('urediVnosVKoledar', function(event, data) {
            console.log("urediVnosVKoledar tntntnt");
            console.log(data);

            for (var i = 0; i < uporabnikoviDogodki.length; i++) {
                if (uporabnikoviDogodki[i]._id == data.id) {
                    console.log("prvi update");
                    uporabnikoviDogodki[i].ime = data.ime;
                    uporabnikoviDogodki[i].opis = data.opis;
                    uporabnikoviDogodki[i].datum = data.datum;
                    uporabnikoviDogodki[i].trajanje = data.trajanje;
                    break;
                }
            }
            for (var i = 0; i < vm.seznamDogodkov.length; i++) {
                if (vm.seznamDogodkov[i]._id == data.id) {
                    console.log("drugi update");
                    vm.seznamDogodkov[i].ime = data.ime;
                    vm.seznamDogodkov[i].opis = data.opis;
                    vm.seznamDogodkov[i].datum = data.datum;
                    vm.seznamDogodkov[i].trajanje = data.trajanje;

                    var ure = data.datum.getHours();
                    var minute = data.datum.getMinutes();
                    if (ure < 10) {
                        ure = "0" + ure;
                    }
                    if (minute < 10) {
                        minute = "0" + minute;
                    }
                    vm.seznamDogodkov[i].datumStr = ure + ":" + minute;
                    break;
                }
            }
        });

        $scope.$on('izbrisiVnosVKoledar', function(event, data) {
            console.log("izbrisiVnosVKoledar");

            for (var i = 0; i < uporabnikoviDogodki.length; i++) {
                if (uporabnikoviDogodki[i]._id == data._id) {
                    uporabnikoviDogodki.splice(i, 1);
                    console.log("prvi delete");
                    break;
                }
            }
            for (var i = 0; i < vm.seznamDogodkov.length; i++) {
                if (vm.seznamDogodkov[i]._id == data._id) {
                    vm.seznamDogodkov.splice(i, 1);
                    console.log("drugi delete");
                    break;
                }
            }

        });

    }

    koledarCtrl.$inject = ['meseciKonstanta', '$uibModal', '$scope', '$rootScope', '$route', 'straightasPodatki', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('koledarCtrl', koledarCtrl);
})();
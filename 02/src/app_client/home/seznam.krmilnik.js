(function() {
    function seznamCtrl($uibModal, $scope, $rootScope, $route, straightasPodatki, avtentikacija) {
        var vm = this;

        // Variable initialization
        vm.seznamOpravil = null;
        var izbranDan = -1;
        vm.uporabnikovaOpravila = null;


        if (avtentikacija.jePrijavljen()) {
            console.log("Uporabnik je prijavljen");
            straightasPodatki.vrniVnoseUporabnikaSeznam({email: avtentikacija.trenutniUporabnik().email}).then(
                function success(odgovor) {
                    console.log("success");
                    console.log(odgovor.data);
                    vm.uporabnikovaOpravila = odgovor.data;
                }, function error(odgovor) {
                    console.log("fail");
                }
            );
        } else {
            console.log("Uporabnik ni prijavljen");
        }


        vm.dodaj_vnos_v_seznam = function () {

            $uibModal.open({
                templateUrl: '/dodajVnosVSeznamModalnoOkno/dodajVnosVSeznamModalnoOkno.pogled.html',
                controller: 'dodajVnosVSeznamModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    izbranDatum: function() {
                        return {
                            dan: izbranDan
                        };
                    }
                }
            });


        };


        vm.izbrisi_vnos = function (data) {
            console.log("izbrisi vnos wiii");
            data.id = data._id;
            console.log(data);


            $uibModal.open({
                templateUrl: '/izbrisiVnosIzSeznamModalnoOkno/izbrisiVnosIzSeznamModalnoOkno.pogled.html',
                controller: 'izbrisiVnosIzSeznamModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    izbrisPodatki: function() {
                        return {
                            id: data._id,
                            opis: data.opis
                        };
                    }
                }
            });

        };

        vm.uredi_vnos = function (data) {
            console.log("uredi_vnos_v_seznamu");
            console.log(data);
            data.id = data._id;

            $uibModal.open({
                templateUrl: '/urediVnosVSeznamModalnoOkno/urediVnosVSeznamModalnoOkno.pogled.html',
                controller: 'urediVnosVSeznamModalnoOkno',
                controllerAs: 'vm',
                resolve: {
                    vnosSeznama: function() {
                        return {
                            id: data._id,
                            opis: data.opis
                        };
                    }
                }
            });
        };


        $scope.$on('dodajVnosVSeznam', function(event, data) {

            console.log(vm.uporabnikovaOpravila);
            vm.uporabnikovaOpravila.push(data);


        });


        $scope.$on('urediVnosVSeznam', function(event, data) {
            console.log("urediVnosVSeznam v krmilniku");
            console.log(data);

            for (var i = 0; i < vm.uporabnikovaOpravila.length; i++) {
                if (vm.uporabnikovaOpravila[i].id == data._id) {
                    console.log("update");
                    vm.uporabnikovaOpravila[i].opis = data.opis;

                    console.log(data);
                    break;
                }
            }

            var podatki = {
                'email': data.email,
                'id': data.id,
                'opis': data.opis
            };
            console.log(podatki);

            straightasPodatki.urediVnosVSeznam(podatki).then(
                function success(odgovor) {
                    $route.reload();
                },
                function error(odgovor) {
                    vm.napakaNaObrazcu = "Napaka pri urejanju vnosa, poskusite znova!";
                }
            );


        });


        $scope.$on('izbrisiVnosVSeznam', function(event, data) {
            for (var i = 0; i < vm.uporabnikovaOpravila.length; i++) {
                if (vm.uporabnikovaOpravila[i].id == data._id) {
                    vm.uporabnikovaOpravila.splice(i, 1);
                    console.log("delete opravilo");
                    break;
                }
            }

            var podatki = {
                'email': data.email,
                'id': data.id,
                'opis': data.opis
            };

            console.log(podatki);

            straightasPodatki.izbrisiVnosIzSeznama(podatki).then(
                function success(odgovor) {
                    $route.reload();
                },
                function error(odgovor) {
                    vm.napakaNaObrazcu = "Napaka pri brisanju vnosa, poskusite znova!";
                }
            );

        });


    }

    seznamCtrl.$inject = [ '$uibModal', '$scope', '$rootScope', '$route', 'straightasPodatki', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('seznamCtrl', seznamCtrl);
})();
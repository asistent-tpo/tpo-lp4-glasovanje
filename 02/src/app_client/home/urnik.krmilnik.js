(function() {
    function urnikCtrl($scope, $rootScope, $route, straightasPodatki, scopeRefresh, avtentikacija) {
        var vm = this;

        var podatki = {
            'email': avtentikacija.jePrijavljen() ? avtentikacija.trenutniUporabnik().email : ''
        };
        straightasPodatki.vnosiVUrniku(podatki).then(
            function success(odgovor) {
                console.log('success', odgovor. data);
                scopeRefresh.safeApply($rootScope, pripraviVnoseVUrniku(odgovor.data));
            },
            function error(odgovor) {
                console.log('error', odgovor.data);
                scopeRefresh.safeApply($rootScope, pripraviVnoseVUrniku(null));
            }
        );

        vm.dodaj_vnos_v_urnik = function(email, id, ime, trajanje, barva) {
            var podatki = {
                'email': email,
                'id': id,
                'ime': ime,
                'trajanje': trajanje,
                'barva': barva,
            };

            straightasPodatki.dodajVnosVUrnik(podatki).then(
                function success(odgovor) {
                    console.log('success', odgovor.data);
                    $route.reload();
                }, function error(odgovor) {
                    vm.napakaNaObrazcu = "Napaka pri dodajanju vnosa v urnik, poskusite znova!";
                }
            );
        };

        vm.posodobi_vnos_v_urniku = function(email, id, ime, trajanje, barva) {
            var podatki = {
                'email': email,
                'id': id,
                'ime': ime,
                'trajanje': trajanje,
                'barva': barva,
            };

            straightasPodatki.posodobiVnosVUrniku(podatki).then(
                function success(odgovor) {
                    $route.reload();
                },
                function error(odgovor) {
                    vm.napakaNaObrazcu = "Napaka pri posodabljanju vnosa, poskusite znova!";
                }
            );
        };

        vm.izbrisi_vnos_iz_urnika = function(email, id) {
            var podatki = {
                'email': email,
                'id': id
            };

            console.log(podatki);

            straightasPodatki.izbrisiVnosIzUrnika(podatki).then(
                function success(odgovor) {
                    $route.reload();
                },
                function error(odgovor) {
                    vm.napakaNaObrazcu = "Napaka pri brisanju vnosa, poskusite znova!";
                }
            );
        };

        var pripraviVnoseVUrniku = function(vnosi) {
            vm.vnosi = Array(60).fill({
                'ime': '',
                'barva': '#FFFFFF'
            });

            if (!vnosi) {
                return null;
            }

            for (var i = 0; i < vnosi.length; i++) {
                for (var j = 0; j < vnosi[i].trajanje; j++) {
                    vm.vnosi[vnosi[i].id + j*5] = {
                        'id': vnosi[i].id,
                        'ime': vnosi[i].ime,
                        'barva': vnosi[i].barva,
                        'trajanje': vnosi[i].trajanje,
                        'email': vnosi[i].email
                    };
                }
            }
        };

        $scope.$on('dodajVnosVUrnik', function(event, data) {
            console.log('dodajVnosVUrnik');
            if (data) {
                vm.dodaj_vnos_v_urnik(data.email, data.id, data.ime, data.trajanje, data.barva);
            }
        });

        $scope.$on('urediVnosVUrniku', function(event, data) {
            if (data) {
                vm.posodobi_vnos_v_urniku(data.email, data.id, data.ime, data.trajanje, data.barva);
            }
        });

        $scope.$on('izbrisiVnosIzUrnika', function(event, data) {
            if (data) {
                vm.izbrisi_vnos_iz_urnika(data.email, data.id);
            }
        });
    }

    urnikCtrl.$inject = ['$scope', '$rootScope', '$route', 'straightasPodatki', 'scopeRefresh', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('urnikCtrl', urnikCtrl);
})();
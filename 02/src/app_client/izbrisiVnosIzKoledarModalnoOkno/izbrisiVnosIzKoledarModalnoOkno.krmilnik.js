(function() {
    function izbrisiVnosIzKoledarModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, izbrisPodatki, avtentikacija) {
        var vm = this;

        console.log("krmilnik koledar izbrisi modalno okno");

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_izbris = function() {
            console.log("potrdi izbris");

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za brisanje vnosa iz koledarja morate biti prijavljeni!";
                return false;
            }

            straightasPodatki.izbrisiVnosIzKoledarja({id: izbrisPodatki.id}).then(
                function success(odgovor) {
                    console.log("success");
                    $rootScope.$broadcast('izbrisiVnosVKoledar', {_id: izbrisPodatki.id});
                }, function error(odgovor) {
                    console.log("fail");
                }
            );

            vm.modalnoOkno.zapri(null);
        };
    }

    izbrisiVnosIzKoledarModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'izbrisPodatki', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('izbrisiVnosIzKoledarModalnoOkno', izbrisiVnosIzKoledarModalnoOkno);
})();
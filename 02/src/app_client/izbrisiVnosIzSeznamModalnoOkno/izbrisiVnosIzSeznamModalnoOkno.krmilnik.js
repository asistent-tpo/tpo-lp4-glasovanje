(function() {
    function izbrisiVnosIzSeznamModalnoOkno($rootScope, $scope, $uibModalInstance, straightasPodatki, izbrisPodatki, avtentikacija) {
        var vm = this;
        vm.neki = null;

        console.log("krmilnik seznam izbrisi modalno okno");

        vm.modalnoOkno = {
            preklici_dodajanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        $scope.opisIzbrisanega = izbrisPodatki.opis;


        vm.potrdi_izbris = function() {


            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = 'Za brisanje vnosa v seznam se prijavi!';

                return false;
            }

            var podatki = {
                'id': izbrisPodatki.id,
                'opis': izbrisPodatki.opis,
                'email': avtentikacija.jePrijavljen() ? avtentikacija.trenutniUporabnik().email : ''
            };

            $rootScope.$broadcast('izbrisiVnosVSeznam', podatki);
            vm.modalnoOkno.zapri(null);
        };

    }

    izbrisiVnosIzSeznamModalnoOkno.$inject = ['$rootScope','$scope', '$uibModalInstance', 'straightasPodatki', 'izbrisPodatki', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('izbrisiVnosIzSeznamModalnoOkno', izbrisiVnosIzSeznamModalnoOkno);
})();
(function() {
    function urediVnosVSeznamModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, vnosSeznama, avtentikacija) {
        var vm = this;

        console.log("krmilnik uredi seznam modalno okno");

        vm.podatkiObrazca = {
            opis: null
        };

        vm.modalnoOkno = {
            preklici_urejanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_urejanje = function() {
            console.log("potrdi urejanje");
            vm.napakaNaObrazcu = "";

            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = "Za urejanje vnosa v seznamu morate biti prijavljeni!";
                return false;
            }

            if (!vm.podatkiObrazca || !vm.podatkiObrazca.opis) {
                vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja.";
                return false;
            }

            var podatki = {
                'id': vnosSeznama.id,
                'email': avtentikacija.jePrijavljen() ? avtentikacija.trenutniUporabnik().email : '',
                'opis': vm.podatkiObrazca.opis
            };


            $rootScope.$broadcast('urediVnosVSeznam', podatki);

            vm.modalnoOkno.zapri(null);
        };
    }

    urediVnosVSeznamModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'vnosSeznama', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('urediVnosVSeznamModalnoOkno', urediVnosVSeznamModalnoOkno);
})();
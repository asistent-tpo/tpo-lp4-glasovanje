(function() {
    function izbrisiVnosIzUrnikaModalnoOkno($rootScope, $uibModalInstance, straightasPodatki, izbranaCelica, avtentikacija) {
        var vm = this;

        vm.modalnoOkno = {
            preklici_brisanje_vnosa: function() {
                $uibModalInstance.close();
            },
            zapri: function(odgovor) {
                $uibModalInstance.close(odgovor);
            }
        };

        vm.potrdi_brisanje_vnosa_v_urniku = function() {


            if (!avtentikacija.jePrijavljen()) {
                vm.napakaNaObrazcu = 'Za brisanje vnosa v urniku se prijavi!';

                return false;
            }

            var podatki = {
                'id': izbranaCelica.vnos.id,
                'email': avtentikacija.jePrijavljen() ? avtentikacija.trenutniUporabnik().email : ''
            };

            $rootScope.$broadcast('izbrisiVnosIzUrnika', podatki);
            vm.modalnoOkno.zapri(null);
        };
    }

    izbrisiVnosIzUrnikaModalnoOkno.$inject = ['$rootScope', '$uibModalInstance', 'straightasPodatki', 'izbranaCelica', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .controller('izbrisiVnosIzUrnikaModalnoOkno', izbrisiVnosIzUrnikaModalnoOkno);
})();
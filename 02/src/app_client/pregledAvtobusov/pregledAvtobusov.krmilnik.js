(function() {
    function avtobusiCtrl(straightasPodatki) {
        var vm = this;

        vm.prikaziSeznamAvtobusov = function() {
            if(vm.ime_postaje) {
                vm.podatki = [];
                vm.error="";
                vm.loading = true;
                straightasPodatki.pridobiPodatkePostaje({poizvedba: vm.ime_postaje}).then(
                    function succes(odgovor) {
                        vm.loading = false;
                        vm.podatki = odgovor.data;
                    },
                    function error(odgovor) {
                        vm.loading = false;
                        vm.error = odgovor.data.error;
                    }
                );
            } else {
                vm.error = "Polje ne more biti prazno.";
            }
        }

    }

    avtobusiCtrl.$inject = ['straightasPodatki'];

    /* global angular */
    angular
        .module('straightas')
        .controller('avtobusiCtrl', avtobusiCtrl);
})();
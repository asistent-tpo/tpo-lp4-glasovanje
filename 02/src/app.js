require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var uglifyJs = require('uglify-js');
var uglifyCss = require('uglifycss');
var fs = require('fs');

var minifiedJs = uglifyJs.minify({
    'app.js': fs.readFileSync('app_client/app.js', 'utf-8'),
    'home.krmilnik.js': fs.readFileSync('app_client/home/home.krmilnik.js', 'utf-8'),
    'koledar.krmilnik.js': fs.readFileSync('app_client/home/koledar.krmilnik.js', 'utf-8'),
    'urnik.krmilnik.js': fs.readFileSync('app_client/home/urnik.krmilnik.js', 'utf-8'),
    'seznam.krmilnik.js': fs.readFileSync('app_client/home/seznam.krmilnik.js', 'utf-8'),

    'dodajVnosVUrnikModalnoOkno.krmilnik.js': fs.readFileSync('app_client/dodajVnosVUrnikModalnoOkno/dodajVnosVUrnikModalnoOkno.krmilnik.js', 'utf-8'),
    'urediVnosVUrnikuModalnoOkno.krmilnik.js': fs.readFileSync('app_client/urediVnosVUrnikuModalnoOkno/urediVnosVUrnikuModalnoOkno.krmilnik.js', 'utf-8'),

    'dodajVnosVKoledarModalnoOkno.krmilnik.js': fs.readFileSync('app_client/dodajVnosVKoledarModalnoOkno/dodajVnosVKoledarModalnoOkno.krmilnik.js', 'utf-8'),
    'urediVnosVKoledarModalnoOkno.krmilnik.js': fs.readFileSync('app_client/urediVnosVKoledarModalnoOkno/urediVnosVKoledarModalnoOkno.krmilnik.js', 'utf-8'),
    'izbrisiVnosIzKoledarModalnoOkno.krmilnik.js': fs.readFileSync('app_client/izbrisiVnosIzKoledarModalnoOkno/izbrisiVnosIzKoledarModalnoOkno.krmilnik.js', 'utf-8'),

    'izbrisiVnosIzUrnikaModalnoOkno.krmilnik.js': fs.readFileSync('app_client/izbrisiVnosIzUrnikaModalnoOkno/izbrisiVnosIzUrnikaModalnoOkno.krmilnik.js', 'utf-8'),

    'avtentikacija.krmilnik.js': fs.readFileSync('app_client/skupno/krmilniki/avtentikacija.krmilnik.js', 'utf-8'),
    'dogodki.krmilnik.js': fs.readFileSync('app_client/dogodki/dogodki.krmilnik.js', 'utf-8'),

    'pregledRestavracij.krmilnik.js': fs.readFileSync('app_client/pregledRestavracij/pregledRestavracij.krmilnik.js', 'utf-8'),
    'pregledAvtobusov.krmilnik.js': fs.readFileSync('app_client/pregledAvtobusov/pregledAvtobusov.krmilnik.js', 'utf-8'),

    'dogodkiNadzornaPlosca.krmilnik.js': fs.readFileSync('app_client/upravljalecDogodkovDomacaStran/dogodkiNadzornaPlosca.krnilnik.js', 'utf-8'),
    'dogodkiModalnoOkno.krmilnik.js': fs.readFileSync('app_client/upravljalecDogodkovDomacaStran/dogodkiModalnoOkno.krmilnik.js', 'utf-8'),


    'sporocilaNadzornaPlosca.krmilnik.js': fs.readFileSync('app_client/upravljalecSporocilDomacaStran/sporocilaNadzornaPlosca.krnilnik.js', 'utf-8'),
    'sporocilaModalnoOkno.krmilnik.js': fs.readFileSync('app_client/upravljalecSporocilDomacaStran/sporocilaModalnoOkno.krmilnik.js', 'utf-8'),

    'straightasPodatki.storitev.js': fs.readFileSync('app_client/skupno/storitve/straightasPodatki.storitev.js', 'utf-8'),
    'scopeRefresh.storitev.js': fs.readFileSync('app_client/skupno/storitve/scopeRefresh.storitev.js', 'utf-8'),
    'avtentikacija.storitev.js': fs.readFileSync('app_client/skupno/storitve/avtentikacija.storitev.js', 'utf-8'),
    'navigacija.krmilnik.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.krmilnik.js', 'utf-8'),
    'ngRightClick.direktiva.js': fs.readFileSync('app_client/skupno/direktive/helpers/ngRightClick.direktiva.js', 'utf-8'),
    'meseci.konstanta.js': fs.readFileSync('app_client/skupno/konstante/meseci.konstanta.js', 'utf-8'),
    'barve.konstanta.js': fs.readFileSync('app_client/skupno/konstante/barve.konstanta.js', 'utf-8'),
    'range.filter.js': fs.readFileSync('app_client/skupno/filtri/range.filter.js', 'utf-8'),



    'dodajVnosVSeznamModalnoOkno.krmilnik.js': fs.readFileSync('app_client/dodajVnosVSeznamModalnoOkno/dodajVnosVSeznamModalnoOkno.krmilnik.js', 'utf-8'),
    'izbrisiVnosIzSeznamModalnoOkno.krmilnik.js': fs.readFileSync('app_client/izbrisiVnosIzSeznamModalnoOkno/izbrisiVnosIzSeznamModalnoOkno.krmilnik.js', 'utf-8'),
    'urediVnosVSeznamModalnoOkno.krmilnik.js': fs.readFileSync('app_client/urediVnosVSeznamModalnoOkno/urediVnosVSeznamModalnoOkno.krmilnik.js', 'utf-8'),

    'sporocila.krmilnik.js': fs.readFileSync('app_client/sporocila/sporocila.krmilnik.js', 'utf-8'),

});

fs.writeFile('public/angular/straightas.min.js', minifiedJs.code, function(napaka) {
    if (napaka) {
        console.log(napaka);
    } else {
        console.log('Skripta je zgenerirana in shranjena v "straightas.min.js".');
    }
});

var minifiedCss = uglifyCss.processFiles(['public/stylesheets/styles.css']);

fs.writeFile('public/stylesheets/styles.min.css', minifiedCss, function(napaka) {
    if (napaka) {
        console.log(napaka);
    } else {
        console.log('CSS stili so zgenerirani in shranjeni v "public/stylesheets/styles.min.css"');
    }
});

require('./app_api/models/db');

var passport = require('passport');
require('./app_api/konfiguracija/passport');

var indexApi = require('./app_api/routes/index');

var app = express();

// odprava varnostnih pomanjkljivosti
app.use(function(req, res, next) {
    res.setHeader('X-Frame-Options', 'DENY');
    res.setHeader('X-XSS-Protection', '1; mode=block');
    res.setHeader('X-Content-Type-Options', 'nosniff');
    next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

app.use('/api', indexApi);
app.use(function(req, res) {
    res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({
            "sporocilo": err.name + ": " + err.message
        });
    }
});

// error handler
app.use(function(err, req, res, next) {    
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

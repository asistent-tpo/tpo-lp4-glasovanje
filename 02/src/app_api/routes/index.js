var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var avtentikacija = jwt({
  secret: process.env.JWT_GESLO,
  userProperty: 'payload'
});

var ctrlUrniki = require('../controllers/urniki');
var ctrlUporabniki = require('../controllers/uporabniki');
var ctrlKoledarji = require('../controllers/koledarji');
var ctrlAvtentikacija = require('../controllers/avtentikacija');
var ctrlSeznami = require('../controllers/seznami');
var ctrlRestavracije = require('../controllers/restavracije');
var ctrlAvtobusi = require('../controllers/avtobusi');
var ctrlDogodki = require('../controllers/dogodki');
var ctrlSporocila = require('../controllers/sporocila');

// UPORABNIKI
router.post('/preveriGeslo', ctrlUporabniki.preveri_geslo);
router.post('/preveriVeljavnostGesla', ctrlUporabniki.preveri_veljavnost_gesla);
router.post('/spremeniGeslo', ctrlUporabniki.spremeni_geslo);

// URNIKI
router.post('/urniki', avtentikacija, ctrlUrniki.vrni_vse_vnose);
router.post('/urniki/vnosi', avtentikacija, ctrlUrniki.dodaj_vnos);
router.put('/urniki/vnosi', avtentikacija, ctrlUrniki.posodobi_vnos);
router.delete('/urniki/vnosi', avtentikacija, ctrlUrniki.izbrisi_vnos);
router.delete('/urniki/vnosi/izbrisiVse', ctrlUrniki.izbrisi_vse_vnose);

// KOLEDARJI
router.post('/koledarji', avtentikacija, ctrlKoledarji.vrni_vse_vnose);
router.post('/koledarji/vnosi', avtentikacija, ctrlKoledarji.dodaj_vnos);
router.put('/koledarji/vnosi', avtentikacija, ctrlKoledarji.posodobi_vnos);
router.delete('/koledarji/vnosi', avtentikacija, ctrlKoledarji.izbrisi_vnos);
router.delete('/koledarji/vnosi/izbrisiVse', ctrlKoledarji.izbrisi_vse_vnose);

// AVTENTIKACIJA
router.post('/registracija', ctrlAvtentikacija.registracija);
router.post('/prijava', ctrlAvtentikacija.prijava);
router.get('/potrditev/:potrditveni_zeton', ctrlAvtentikacija.potrditev_registracije);

// TEMPORARY
router.get('/registracija/test', ctrlAvtentikacija.generiraj_testni_vnos);
router.get('/izbrisiVseUporabnike', ctrlAvtentikacija.izbrisi_vse_uporabnike);
router.post('/spremeniTip', ctrlAvtentikacija.spremeni_tip_uporabnika);

//SEZNAMI
router.post('/seznami', avtentikacija, ctrlSeznami.vrni_vse_vnose);
router.post('/seznami/vnosi', avtentikacija, ctrlSeznami.dodaj_vnos);
router.delete('/seznami/vnosi', avtentikacija, ctrlSeznami.izbrisi_vnos);
router.put('/seznami/vnosi', avtentikacija, ctrlSeznami.posodobi_vnos);
router.delete('/seznami/vnosi/izbrisiVse', ctrlSeznami.izbrisi_vse_vnose);

// RESTAVRACIJE
router.get('/restavracije', ctrlRestavracije.vrni_podatke_vseh_restavracij);
router.post('/restavracije', ctrlRestavracije.shrani_podatke_vseh_restavracij);
router.delete('/restavracije', ctrlRestavracije.izbrisi_podatke_vseh_restavracij);

// AVTOBUSI
router.get('/avtobusi/postajalisca', ctrlAvtobusi.vrni_podatke_vseh_postajalisc);
router.post('/avtobusi/postajalisca', ctrlAvtobusi.shrani_podatke_vseh_postajalisc);
router.delete('/avtobusi/postajalisca', ctrlAvtobusi.izbrisi_podatke_vseh_postajalisc);
router.post('/avtobusi', ctrlAvtobusi.pridobi_podatke_postaje);

// DOGODKI
router.post('/dogodki', avtentikacija,  ctrlDogodki.vrni_vse_dogodke);
router.post('/dogodki/vnosi', avtentikacija,  ctrlDogodki.dodaj_dogodek);
router.delete('/dogodki/vnosi/izbrisiVse', ctrlDogodki.izbrisi_vse_vnose);

// SPOROCILA
router.post('/sporocila', avtentikacija,  ctrlSporocila.vrni_vsa_sporocila);
router.post('/sporocila/vnosi', avtentikacija,  ctrlSporocila.dodaj_sporocilo);
router.delete('/sporocila/vnosi/izbrisiVse', ctrlSporocila.izbrisi_vse_vnose);

module.exports = router;
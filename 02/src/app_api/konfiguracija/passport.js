var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

passport.use(new LokalnaStrategija({
    usernameField: 'email',
    passwordField: 'geslo'
  }, 
  function(email, geslo, koncano) {
    Uporabnik.findOne(
      {
        email: email
      },
      function(napaka, uporabnik) {
        if (napaka)
          return koncano(napaka);
        if (!uporabnik) {
          return koncano(null, false, {
            error: 'Napačno uporabniško ime'
          });
        }
        if (!uporabnik.preveri_geslo(geslo)) {
          return koncano(null, false, {
            error: 'Napačno geslo'
          });
        }
        return koncano(null, uporabnik);
      }
    );
  }
));
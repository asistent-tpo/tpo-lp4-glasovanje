var mongoose = require('mongoose');
var Restavracija = mongoose.model('Restavracija');

var TAG = "app_api/controllers/restavracije.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.vrni_podatke_vseh_restavracij = function(zahteva, odgovor) {
    vrniPodatkeVsehRestavracij(function(vnosi) {
        vrniJsonOdgovor(odgovor, 200, vnosi);
    });
};

module.exports.shrani_podatke_vseh_restavracij = function(zahteva, odgovor) {
    var restaurants = require('../files/restaurants.json');

    Restavracija
        .create(restaurants, function(napaka, restavracije) {
            if(napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 200, restavracije);
            }
        })
}

module.exports.izbrisi_podatke_vseh_restavracij = function(zahteva, odgovor) {
    Restavracija
        .deleteMany({}, function(napaka, restavracije) {
            if(napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 204, null);
            }
        })
}

var vrniPodatkeVsehRestavracij = function(callback) {
    Restavracija
        .find({}, function(napaka, restavracije) {
            if(napaka) {
                callback([]);
            } else {
                callback(restavracije);
            }
        })
};
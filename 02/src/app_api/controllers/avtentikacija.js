var passport = require('passport');
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

/**
 * Vkljucuje funkcionalnost iz modela uporabnik
 * -> preveri_veljavnost_emaila(email)
 * -> dodaj_uporabnika_v_seznam_nepotrjenih(email, zeton)
 * -> poslji_potrditveni_email(email)
 * 
 * Funkcionalnost preveri_veljavnost_gesla(geslo1, geslo2) je nesmiselna saj je 
 * to pomembno zgolj na front-endu da se uporabnik ne zatipka
 */
module.exports.registracija = function(zahteva, odgovor) {

    if (!zahteva.body.email || !zahteva.body.geslo) {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Zahtevani so vsi podatki"
        });
        return;
    } else if (zahteva.body.email.split(" ").length > 1) {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Elektronski naslov je neustrezen!"
        });
        return;
    } else if (!(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(zahteva.body.email))) {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Elektronski naslov je neustrezen!"
        });
        return;
    }

    Uporabnik.findOne({email: zahteva.body.email}, function(napaka, uporabnik) {
        if(napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
        } else if(uporabnik) {
            vrniJsonOdgovor(odgovor, 409, {
                'error': 'Račun s tem emailom že obstaja.'
            });
        } else {
            var uporabnik = new Uporabnik();
            uporabnik.email = zahteva.body.email;
            uporabnik.tip_uporabnika = 0;
            uporabnik.nastavi_geslo(zahteva.body.geslo);
            uporabnik.dodaj_uporabnika_v_seznam_nepotrjenih();
            uporabnik.save(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                } else {
                    uporabnik.poslji_potrditveni_email(
                        function() {
                            vrniJsonOdgovor(odgovor, 200, {
                                "message": "Email za potrditev elektronskega naslova je bil uspešno poslan. Prosimo sledite navodilom v emailu, to stran pa lahko zaprete."
                            });
                        },
                        function() {
                            vrniJsonOdgovor(odgovor, 500, {
                                "error": "Prišlo je do napake pri pošiljanju emaila za potrditev elektronskega naslova."
                            });
                        }
                    );
                }
            });
        }
    });
};

module.exports.prijava = function(zahteva, odgovor) {
    if (!zahteva.body.email || !zahteva.body.geslo) {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Zahtevani so vsi podatki"
        });
        return;
    }
    passport.authenticate('local', function(napaka, uporabnik, podatki) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if (uporabnik) {

            if(typeof uporabnik.potrditveni_zeton !== 'undefined') {
                vrniJsonOdgovor(odgovor, 401, {
                    "error": "Elektronski naslov še ni potrjen"
                });
            } else {
                console.log(uporabnik);
                uporabnik.shrani_dostopni_zeton();
                uporabnik.save(function(napaka) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                    } else {
                        vrniJsonOdgovor(odgovor, 200, {
                            "zeton": uporabnik.dostopni_zeton
                        });
                    }
                });
            }
        } else {
            vrniJsonOdgovor(odgovor, 401, podatki);
        }
    })(zahteva, odgovor);
};

module.exports.potrditev_registracije = function(zahteva, odgovor) {

    var potrditveni_zeton = zahteva.params.potrditveni_zeton;

    if(!potrditveni_zeton) {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Zahtevani so vsi podatki"
        });
        return;
    }
    
    Uporabnik.findOne({potrditveni_zeton: potrditveni_zeton}, function(napaka, uporabnik) {
        if(napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
            return;
        } else if(!uporabnik) {
            vrniJsonOdgovor(odgovor, 404, {
                'error': 'Uporabnik s tem žetonom ni bil najden!'
            });
            return;
        } 
        
        if(uporabnik.preveri_veljavnost_potrditvenega_zetona() == true){
            uporabnik.odstrani_uporabnika_iz_seznama_nepotrjenih();
            uporabnik.save(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                } else {
                    vrniJsonOdgovor(odgovor, 200, {
                        'message': 'Registracija uspešno dokončana. Sedaj se lahko prijavite.'
                    });
                }
            });
        } else {
            vrniJsonOdgovor(odgovor, 401, {
                'error': 'Potrditveni žeton več ni veljaven!'
            });
        }
        
    });
};

module.exports.generiraj_testni_vnos = function(zahteva, odgovor) {
    var uporabnik = new Uporabnik();
    uporabnik.email = "cypress@test.com";
    uporabnik.tip_uporabnika = 0;
    uporabnik.nastavi_geslo("cypressTestPassword");
    uporabnik.save(function(napaka) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 200, {
                'message': 'Registracija uspešno dokončana'
            });
        }
    });
};

module.exports.izbrisi_vse_uporabnike = function(zahteva, odgovor) {
    Uporabnik
        .find({})
        .remove()
        .exec(function() {
            vrniJsonOdgovor(odgovor, 204, null);
        });
};

module.exports.spremeni_tip_uporabnika = function(zahteva, odgovor) {

    if(!zahteva.body.email || !zahteva.body.tip) {
        vrniJsonOdgovor(odgovor, 400, {
            error: "Vsi podatki so zahtevani!"
        });
        return;
    }

    if(zahteva.body.tip < 0 || zahteva.body.tip > 2) {
        vrniJsonOdgovor(odgovor, 400, {
            error: "Tip mora biti število med 0 in vključno 2"
        });
        return;
    }

    Uporabnik
        .findOne({email: zahteva.body.email}, function(napaka, uporabnik) {
            if(napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else if(!uporabnik) {
                vrniJsonOdgovor(odgovor, 404, {
                    error: "Uporabnik s tem emailom ne obstaja."
                });
            } else {
                uporabnik.tip_uporabnika = zahteva.body.tip;
                uporabnik.save(function(napaka, uporabnik) {
                    if(napaka) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                    } else {
                        vrniJsonOdgovor(odgovor, 200, uporabnik);
                    }
                });
            }
        });
};
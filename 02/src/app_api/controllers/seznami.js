var mongoose = require('mongoose');
var Seznam = mongoose.model('Seznam');

var TAG = "app_api/controllers/seznami.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};


module.exports.vrni_vse_vnose = function(zahteva, odgovor) {
    if (zahteva.body && zahteva.body.email) {
        console.log(zahteva.body.email);
        vrniVseVnoseUporabnika(zahteva.body.email, function(vnosi) {
            vrniJsonOdgovor(odgovor, 200, vnosi);
        });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

var vrniVseVnoseUporabnika = function(email, callback) {
    Seznam
        .findOne()
        .exec(function(error, seznam) {
            if (error) {
                callback([]);
            } else {
                if (seznam && seznam.vnosi_v_seznamu) {
                    var vnosi = [];

                    for (var i = 0; i < seznam.vnosi_v_seznamu.length; i++) {
                        if (seznam.vnosi_v_seznamu[i].email === email) {
                            vnosi.push(seznam.vnosi_v_seznamu[i]);
                        }
                    }
                    callback(vnosi);
                } else {
                    callback([]);
                }
            }
        });
};


module.exports.dodaj_vnos = function(zahteva, odgovor) {
    var methodTAG = "dodaj_vnos_v_seznam";
    console.log(TAG + methodTAG);

    if (zahteva.body && zahteva.body.email && zahteva.body.opis) {

            var vnosVSeznam = {
                email: zahteva.body.email,
                opis: zahteva.body.opis,

            };

            Seznam.find().countDocuments({}, function (napaka, count) {
                if (count === 0) {
                    kreirajSeznam(odgovor, vnosVSeznam, dodajVnosVSeznam);
                } else {
                    dodajVnosVSeznam(odgovor, vnosVSeznam);
                }
            });


    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

var kreirajSeznam = function(odgovor, vnosVSeznam, dodajVnosVSeznam) {
    Seznam.create({}, function(napaka, seznam) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            dodajVnosVSeznam(odgovor, vnosVSeznam);
        }
    });
};

var dodajVnosVSeznam = function(odgovor, vnosVSeznam) {
    Seznam
        .findOne()
        .exec(function(err, seznam) {
            seznam.vnosi_v_seznamu.push(vnosVSeznam);
            seznam.save(function(err) {
                if (err) {
                    vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                    vnosVSeznam._id = seznam.vnosi_v_seznamu.pop()._id;
                    console.log(vnosVSeznam);
                    vrniJsonOdgovor(odgovor, 200, vnosVSeznam);
                }
            });
        });
};


module.exports.izbrisi_vnos = function(zahteva, odgovor) {


    if (zahteva.body && (zahteva.body.id != null) && zahteva.body.email) {
        Seznam
            .findOne()
            .exec(function(err, seznam) {
                if (err) {
                    vrniJsonOdgovor(odgovor, 500, err);
                    return;
                } else if (!seznam) {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Seznam ni bil najden!'
                    });
                    return;
                }

                if (seznam.vnosi_v_seznamu) {

                    var index = -1;
                    for (var i = 0; i < seznam.vnosi_v_seznamu.length; i++) {
                        console.log(seznam.vnosi_v_seznamu[0].email);
                        console.log(zahteva.body.email);



                        if (seznam.vnosi_v_seznamu[i]._id == zahteva.body.id) {
                            index = i;
                            break;
                        }
                    }

                    if (index === -1) {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Vnos ni bil najden!'
                        });
                        return;
                    }

                    // delete entry from array
                    seznam.vnosi_v_seznamu.splice(index, 1);

                    seznam.save(function(err) {
                        if (err) {
                            vrniJsonOdgovor(odgovor, 400, err);
                        } else {
                            vrniJsonOdgovor(odgovor, 204, null);
                        }
                    });
                } else {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Seznam nima vnosov!'
                    });
                }
            });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.izbrisi_vse_vnose = function(zahteva, odgovor) {
    Seznam
        .findOneAndRemove({}, function(error, seznam) {
            if (error) {
                console.log('error');
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                console.log('success');
                vrniJsonOdgovor(odgovor, 204, null);
            }
        });
};


module.exports.posodobi_vnos = function(zahteva, odgovor) {
    console.log(zahteva.body);

    if (zahteva.body && zahteva.body.email && zahteva.body.id  && zahteva.body.opis) {


            Seznam
                .findOne()
                .exec(function (err, seznam) {
                    if (err) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                        return;
                    } else if (!seznam) {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Seznam ni bil najden!'
                        });
                        return;
                    }

                    if (seznam.vnosi_v_seznamu) {
                        var index = -1;

                        //poiscemo ustrezen vnos
                        for (var i = 0; i < seznam.vnosi_v_seznamu.length; i++) {
                            seznam.vnosi_v_seznamu[i].id = seznam.vnosi_v_seznamu[i]._id
                            if (seznam.vnosi_v_seznamu[i]._id == zahteva.body._id) {
                                index = i;
                                break;
                            }
                        }

                        //ali je bil vnos najden
                        if (index === -1) {
                            vrniJsonOdgovor(odgovor, 404, {
                                'error': 'Vnos ni bil najden!'
                            });
                            return;
                        }

                        // posodobimo spremenljivke
                        seznam.vnosi_v_seznamu[index].opis = zahteva.body.opis;


                        seznam.save(function (napaka) {
                            if (napaka) {
                                vrniJsonOdgovor(odgovor, 400, napaka);
                            } else {
                                vrniJsonOdgovor(odgovor, 200, seznam.vnosi_v_seznamu[index]);
                            }
                        });
                    } else {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Seznam nima vnosov!'
                        });
                    }
                });


    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};
var mongoose = require('mongoose');
var Urnik = mongoose.model('Urnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

/*
    MAIN METHODS
*/

module.exports.vrni_vse_vnose = function(zahteva, odgovor) {
    if (zahteva.body && zahteva.body.email) {
        vrniVseVnoseUporabnika(zahteva.body.email, function(vnosi) {
            vrniJsonOdgovor(odgovor, 200, vnosi);
        });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.dodaj_vnos = function(zahteva, odgovor) {
    if (zahteva.body && zahteva.body.email && zahteva.body.id != null && zahteva.body.ime && zahteva.body.trajanje != null && zahteva.body.barva) {
        var vnosVUrniku = {
            email: zahteva.body.email,
            id: zahteva.body.id,
            ime: zahteva.body.ime,
            trajanje: zahteva.body.trajanje,
            barva: zahteva.body.barva
        };

        Urnik.find().countDocuments({}, function(napaka, count){
            if (count === 0) {
                kreirajUrnik(odgovor, vnosVUrniku, dodajVnosVUrnik);
            } else {
                dodajVnosVUrnik(odgovor, vnosVUrniku);
            }
        });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.posodobi_vnos = function(zahteva, odgovor) {
    if (zahteva.body && zahteva.body.email && zahteva.body.id != null && zahteva.body.ime && zahteva.body.trajanje != null && zahteva.body.barva) {
        Urnik
            .findOne()
            .exec(function(err, urnik) {
                if (err) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                    return;
                } else if (!urnik) {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Urnik ni bil najden!'
                    });
                    return;
                }

                if (urnik.vnosi_v_urniku) {
                    var index = -1;
                    for (var i = 0; i < urnik.vnosi_v_urniku.length; i++) {
                        if (urnik.vnosi_v_urniku[i].id == zahteva.body.id && urnik.vnosi_v_urniku[i].email == zahteva.body.email) {
                            index = i;
                            break;
                        }
                    }

                    if (index === -1) {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Vnos ni bil najden!'
                        });
                        return;
                    }

                    urnik.vnosi_v_urniku[index].ime = zahteva.body.ime;
                    urnik.vnosi_v_urniku[index].trajanje = zahteva.body.trajanje;
                    urnik.vnosi_v_urniku[index].barva = zahteva.body.barva;

                    urnik.save(function(napaka, urnikOdgovor) {
                        if (napaka) {
                            vrniJsonOdgovor(odgovor, 400, napaka);
                        } else {
                            vrniJsonOdgovor(odgovor, 200, null);
                        }
                    });
                } else {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Urnik nima vnosov!'
                    });
                }
            });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.izbrisi_vnos = function(zahteva, odgovor) {
    if (zahteva.body && zahteva.body.id != null && zahteva.body.email) {
        Urnik
            .findOne()
            .exec(function(err, urnik) {
                if (err) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                    return;
                } else if (!urnik) {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Urnik ni bil najden!'
                    });
                    return;
                }

                if (urnik.vnosi_v_urniku) {
                    var index = -1;
                    for (var i = 0; i < urnik.vnosi_v_urniku.length; i++) {
                        if (urnik.vnosi_v_urniku[i].id == zahteva.body.id && urnik.vnosi_v_urniku[i].email == zahteva.body.email) {
                            index = i;
                            break;
                        }
                    }

                    if (index === -1) {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Vnos ni bil najden!'
                        });
                        return;
                    }

                    // delete entry from array
                    urnik.vnosi_v_urniku.splice(index, 1);

                    urnik.save(function(napaka, urnikOdgovor) {
                        if (napaka) {
                            vrniJsonOdgovor(odgovor, 400, napaka);
                        } else {
                            vrniJsonOdgovor(odgovor, 204, null);
                        }
                    });
                } else {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Urnik nima vnosov!'
                    });
                }
            });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.izbrisi_vse_vnose = function(zahteva, odgovor) {
    Urnik
        .findOneAndRemove({}, function(error, urnik) {
            if (error) {
                console.log('error');
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                console.log('success');
                vrniJsonOdgovor(odgovor, 204, null);
            }
        });
};

/*
    HELPER METHODS
 */

var kreirajUrnik = function(odgovor, vnosVUrniku, dodajVnosVUrnik) {
    Urnik.create({}, function(napaka, urnik) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            dodajVnosVUrnik(odgovor, vnosVUrniku);
        }
    });
};

var dodajVnosVUrnik = function(odgovor, vnosVUrniku) {
    Urnik
        .findOne()
        .exec(function(err, urnik) {
            if (err) {
                vrniJsonOdgovor(odgovor, 500, napaka);
                return;
            } else if (!urnik) {
                vrniJsonOdgovor(odgovor, 404, {
                    'error': 'Urnik ni bil najden!'
                });
                return;
            }

            urnik.vnosi_v_urniku.push(vnosVUrniku);
            urnik.save(function(napaka, urnikOdgovor) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                    vrniJsonOdgovor(odgovor, 200, null);
                }
            });
        });
};

var vrniVseVnoseUporabnika = function(email, callback) {
    Urnik
        .findOne()
        .exec(function(error, urnik) {
            if (error) {
                callback([]);
            } else {
                if (urnik && urnik.vnosi_v_urniku) {
                    var vnosi = [];
                    for (var i = 0; i < urnik.vnosi_v_urniku.length; i++) {
                        if (urnik.vnosi_v_urniku[i].email === email) {
                            vnosi.push(urnik.vnosi_v_urniku[i]);
                        }
                    }
                    callback(vnosi);
                } else {
                    callback([]);
                }
            }
        });
};

var mongoose = require('mongoose');
var Sporocilo = mongoose.model('Sporocilo');

var TAG = "app_api/controllers/sporocila.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

/*
    MAIN METHODS
*/

module.exports.izbrisi_vse_vnose = function(zahteva, odgovor) {
    Sporocilo
        .findOneAndRemove({}, function(error, sporocilo) {
            if (error) {
                console.log('error');
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                console.log('success');
                vrniJsonOdgovor(odgovor, 204, null);
            }
        });
};

module.exports.vrni_vsa_sporocila = function(zahteva, odgovor) {
    Sporocilo
        .findOne()
        .exec(function(error, sporocilo) {
            if (error) {
                vrniJsonOdgovor(odgovor, 400, {"error": "Napaka streznika"})
            } else {
                if (sporocilo && sporocilo.sporocila) {
                    vrniJsonOdgovor(odgovor, 200, sporocilo.sporocila)
                } else {
                    vrniJsonOdgovor(odgovor, 400, {"error": "Napaka streznika"})
                }
            }
        });
};

module.exports.dodaj_sporocilo = function(zahteva, odgovor) {
    var methodTAG = "dodaj_sporocilo";
    console.log(TAG + methodTAG);

    if (zahteva.body && zahteva.body.opis) {



            var vnos = {
                opis: zahteva.body.opis
            };

            Sporocilo.find().countDocuments({}, function (napaka, count) {
                if (count === 0) {
                    kreirajSporocilo(odgovor, vnos, dodajVnosMedSporocila);
                } else {
                    dodajVnosMedSporocila(odgovor, vnos);
                }
            });


    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

/*
    HELPER METHODS
 */

var dodajVnosMedSporocila = function(odgovor, vnos) {
    Sporocilo
        .findOne()
        .exec(function(err, sporocilo) {
            console.log("dodajVnosMedSporocila");
            console.log(sporocilo);
            sporocilo.sporocila.push(vnos);
            sporocilo.save(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                    // responsu dodamo se id, ki je bil pravkar kreiran
                    vnos._id = sporocilo.sporocila.pop()._id;
                    vrniJsonOdgovor(odgovor, 200, vnos);
                }
            });
        });
};

var kreirajSporocilo = function(odgovor, vnos, dodajVnosMedSporocila) {
    var methodTAG = "kreirajSporocilo ";

    console.log(TAG + methodTAG);

    Sporocilo.create({}, function (napaka, sporocilo) {


        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            console.log("sporocilo " + sporocilo);
            console.log("napaka " + napaka);
            dodajVnosMedSporocila(odgovor, vnos);
        }
    });
};



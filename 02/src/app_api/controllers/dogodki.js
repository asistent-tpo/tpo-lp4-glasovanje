var mongoose = require('mongoose');
var Dogodek = mongoose.model('Dogodek');

var TAG = "app_api/controllers/dogodki.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

/*
    MAIN METHODS
*/

module.exports.izbrisi_vse_vnose = function(zahteva, odgovor) {
    Dogodek
        .findOneAndRemove({}, function(error, dogodek) {
            if (error) {
                console.log('error');
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                console.log('success');
                vrniJsonOdgovor(odgovor, 204, null);
            }
        });
};

module.exports.vrni_vse_dogodke = function(zahteva, odgovor) {
    Dogodek
        .findOne()
        .exec(function(error, dogodek) {
            if (error) {
                vrniJsonOdgovor(odgovor, 400, {"error": "Napaka streznika"})
            } else {
                if (dogodek && dogodek.dogodki) {
                    vrniJsonOdgovor(odgovor, 200, dogodek.dogodki)
                } else {
                    vrniJsonOdgovor(odgovor, 400, {"error": "Napaka streznika"})
                }
            }
        });
};

module.exports.dodaj_dogodek = function(zahteva, odgovor) {
    var methodTAG = "dodaj_dogodek";
    console.log(TAG + methodTAG);

    if (zahteva.body && zahteva.body.ime && zahteva.body.datum && zahteva.body.organizator && zahteva.body.opis) {

        var datum = new Date(zahteva.body.datum);

        if (preveriVeljavnostDatuma(datum)) {
            var vnos = {
                ime: zahteva.body.ime,
                datum: datum,
                organizator: zahteva.body.organizator,
                opis: zahteva.body.opis
            };

            Dogodek.find().countDocuments({}, function (napaka, count) {
                if (count === 0) {
                    kreirajDogodek(odgovor, vnos, dodajVnosMedDogodke);
                } else {
                    dodajVnosMedDogodke(odgovor, vnos);
                }
            });
        } else {
            vrniJsonOdgovor(odgovor, 400, {
                "error": "Neveljaven datum!"
            });
        }

    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

/*
    HELPER METHODS
 */

var dodajVnosMedDogodke = function(odgovor, vnos) {
    Dogodek
        .findOne()
        .exec(function(err, dogodek) {
            console.log("dodajVnosMedDogodke");
            console.log(dogodek);
            dogodek.dogodki.push(vnos);
            dogodek.save(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                    // responsu dodamo se id, ki je bil pravkar kreiran
                    vnos._id = dogodek.dogodki.pop()._id;
                    vrniJsonOdgovor(odgovor, 200, vnos);
                }
            });
        });
};

var kreirajDogodek = function(odgovor, vnos, dodajVnosMedDogodke) {
    var methodTAG = "kreirajDogodek ";

    console.log(TAG + methodTAG);

    Dogodek.create({}, function (napaka, dogodek) {


        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            console.log("dogodek " + dogodek);
            console.log("napaka " + napaka);
            dodajVnosMedDogodke(odgovor, vnos);
        }
    });
};

var preveriVeljavnostDatuma = function(datum) {
    return datum instanceof Date && !isNaN(datum);
};

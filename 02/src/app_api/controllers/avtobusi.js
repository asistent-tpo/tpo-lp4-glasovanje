var mongoose = require('mongoose');
var Avtobus = mongoose.model('Avtobus');
var rp = require('request-promise');
var $ = require('cheerio');
var url = 'https://www.trola.si/';

var TAG = "app_api/controllers/avtobusi.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.shrani_podatke_vseh_postajalisc = function(zahteva, odgovor) {
    var postajalisca = require('../files/stations.json');

    Avtobus
        .create(postajalisca, function(napaka, postajalisca) {
            if(napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 200, postajalisca);
            }
        })
};

module.exports.vrni_podatke_vseh_postajalisc = function(zahteva, odgovor) {
    Avtobus
        .find({}, function(napaka, postajalisca){
            if(napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 200, postajalisca)
            }
        })
};

module.exports.izbrisi_podatke_vseh_postajalisc = function(zahteva, odgovor) {
    Avtobus
        .deleteMany({}, function(napaka, avtobusi) {
            if(napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 204, null);
            }
        })
};

module.exports.pridobi_podatke_postaje = function(zahteva, odgovor) {
    if(zahteva.body && zahteva.body.poizvedba) {
        preveri_iskalno_zahtevo(zahteva.body.poizvedba, function(obstaja) {
            if(obstaja) {
                rp(url + zahteva.body.poizvedba)
                    .then(function(html){
                        filtriraj_podatke(html, function(prihodiAvtobusov) {
                            if(prihodiAvtobusov != null) {
                                vrniJsonOdgovor(odgovor, 200, prihodiAvtobusov);
                            } else {
                                vrniJsonOdgovor(odgovor, 500, {
                                    "error": "Prišlo je do napake pri filtriranju podatkov spletne strani trola.si"
                                });
                            }
                        })
                    })
                    .catch(function(err){
                        console.log(err);
                        vrniJsonOdgovor(odgovor, 500, {
                            "error": "Prišlo je do napake pri dostopu do spletne strani trola.si"
                        });
                    });
            } else {
                vrniJsonOdgovor(odgovor, 400, {
                    "error": "Postajališče s tem imenom ali idjem ne obstaja!"
                });
            }
        })
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

var preveri_iskalno_zahtevo = function(poizvedba, callback) {

    if(isNaN(poizvedba)) {
        Avtobus
            .find({ "naziv": { $regex: new RegExp(poizvedba, "i") } }, function(napaka, postajalisca) {
                if(napaka || postajalisca.length <= 0) {
                    callback(false);
                } else {
                    callback(true);
                }
            })
    } else {
        Avtobus
            .find({ "id_postaje": poizvedba }, function(napaka, postajalisca) {
                if(napaka || postajalisca.length <= 0) {
                    callback(false);
                } else {
                    callback(true);
                }
            })
            
    }
};

var filtriraj_podatke = function(html, callback) {

    var data = [];

    $("table", html).each(function(_, seznam) {

        var postajalisceTmp = $(seznam).prev().prev().text();
        var naziv_postajalisca = postajalisceTmp.substring(postajalisceTmp.indexOf(' ') + 1);
        var id_postajalisca = postajalisceTmp.substring(0, postajalisceTmp.indexOf(' '));
        var prihodi = [];

        $("tr", seznam).each(function(_, vrstica) {
            var bus = $("td", vrstica).eq(0).text().trim();
            var smer = $("td", vrstica).eq(1).text().trim();
            var naslednjiCez = []
            $("td", vrstica).eq(2).text().trim().split(',').forEach(function(casPrihoda, ix) {
                casPrihoda = casPrihoda.trim();
                if(casPrihoda !== "Ni prihodov.") {
                    casPrihoda = casPrihoda.slice(0, -1);
                    naslednjiCez.push(casPrihoda);
                }
            });

            if(!isNaN(bus)) {
                prihodi.push({
                    bus: bus,
                    smer: smer,
                    naslednjiCez: naslednjiCez
                })
            }
        });

        data.push({
            id: id_postajalisca,
            naziv: naziv_postajalisca,
            prihodi: prihodi
        });
    });

    callback(data);
};
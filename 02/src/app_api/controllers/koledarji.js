var mongoose = require('mongoose');
var Koledar = mongoose.model('Koledar');

var TAG = "app_api/controllers/koledarji.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

/*
    MAIN METHODS
*/

module.exports.vrni_vse_vnose = function(zahteva, odgovor) {
    if (zahteva.body && zahteva.body.email) {
        console.log(zahteva.body.email);
        vrniVseVnoseUporabnika(zahteva.body.email, function(vnosi) {
            vrniJsonOdgovor(odgovor, 200, vnosi);
        });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.dodaj_vnos = function(zahteva, odgovor) {
    var methodTAG = "dodaj_vnos_v_koledar";
    console.log(TAG + methodTAG);

    if (zahteva.body && zahteva.body.email && zahteva.body.ime && zahteva.body.datum && zahteva.body.opis) {

        var datum = new Date(zahteva.body.datum);

        if (preveriVeljavnostDatuma(datum)) {
            var vnosVKoledar = {
                email: zahteva.body.email,
                ime: zahteva.body.ime,
                datum: datum,
                opis: zahteva.body.opis,
                trajanje: zahteva.body.trajanje
            };

            Koledar.find().countDocuments({}, function (napaka, count) {
                if (count === 0) {
                    kreirajKoledar(odgovor, vnosVKoledar, dodajVnosVKoledar);
                } else {
                    dodajVnosVKoledar(odgovor, vnosVKoledar);
                }
            });
        } else {
            vrniJsonOdgovor(odgovor, 400, {
                "error": "Neveljaven datum!"
            });
        }

    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.posodobi_vnos = function(zahteva, odgovor) {
    console.log(zahteva.body);

    if (zahteva.body && zahteva.body.email && zahteva.body.id && zahteva.body.ime && zahteva.body.datum && zahteva.body.opis) {

        var datum = new Date(zahteva.body.datum);

        if (preveriVeljavnostDatuma(datum)) {
            Koledar
                .findOne()
                .exec(function (err, koledar) {
                    if (err) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                        return;
                    } else if (!koledar) {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Koledar ni bil najden!'
                        });
                        return;
                    }

                    if (koledar.vnosi_v_koledarju) {
                        var index = -1;

                        //poiscemo ustrezen vnos
                        for (var i = 0; i < koledar.vnosi_v_koledarju.length; i++) {
                            if (koledar.vnosi_v_koledarju[i]._id == zahteva.body.id) {
                                index = i;
                                break;
                            }
                        }

                        //ali je bil vnos najden
                        if (index === -1) {
                            vrniJsonOdgovor(odgovor, 404, {
                                'error': 'Vnos ni bil najden!'
                            });
                            return;
                        }

                        // posodobimo spremenljivke
                        koledar.vnosi_v_koledarju[index].ime = zahteva.body.ime;
                        koledar.vnosi_v_koledarju[index].datum = datum;
                        koledar.vnosi_v_koledarju[index].opis = zahteva.body.opis;
                        koledar.vnosi_v_koledarju[index].trajanje = zahteva.body.trajanje;

                        koledar.save(function (napaka) {
                            if (napaka) {
                                vrniJsonOdgovor(odgovor, 400, napaka);
                            } else {
                                vrniJsonOdgovor(odgovor, 200, koledar.vnosi_v_koledarju[index]);
                            }
                        });
                    } else {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Koledar nima vnosov!'
                        });
                    }
                });
        } else {
            vrniJsonOdgovor(odgovor, 400, {
                "error": "Neveljaven datum."
            });
        }

    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.izbrisi_vnos = function(zahteva, odgovor) {
    console.log("brisem iz koledarja");
    if (zahteva.body.id) {
        Koledar
            .findOne()
            .exec(function(err, koledar) {
                if (err) {
                    vrniJsonOdgovor(odgovor, 500, napaka);
                    return;
                } else if (!koledar) {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Koledar ni bil najden!'
                    });
                    return;
                }

                if (koledar.vnosi_v_koledarju) {

                    var index = -1;
                    for (var i = 0; i < koledar.vnosi_v_koledarju.length; i++) {
                        if (koledar.vnosi_v_koledarju[i]._id == zahteva.body.id) {
                            index = i;
                            break;
                        }
                    }

                    if (index === -1) {
                        vrniJsonOdgovor(odgovor, 404, {
                            'error': 'Vnos ni bil najden!'
                        });
                        return;
                    }

                    // delete entry from array
                    koledar.vnosi_v_koledarju.splice(index, 1);
                    console.log("brisem iz koledarja");

                    koledar.save(function(napaka) {
                        if (napaka) {
                            vrniJsonOdgovor(odgovor, 400, napaka);
                        } else {
                            vrniJsonOdgovor(odgovor, 204, null);
                        }
                    });
                } else {
                    vrniJsonOdgovor(odgovor, 404, {
                        'error': 'Koledar nima vnosov!'
                    });
                }
            });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "error": "Vsi parametri so obvezni!"
        });
    }
};

module.exports.izbrisi_vse_vnose = function(zahteva, odgovor) {
    Koledar
        .findOneAndRemove({}, function(error, koledar) {
            if (error) {
                console.log('error');
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                console.log('success');
                vrniJsonOdgovor(odgovor, 204, null);
            }
        });
};

/*
    HELPER METHODS
 */

var kreirajKoledar = function(odgovor, vnosVKoledar, dodajVnosVKoledar) {
    var methodTAG = "kreirajKoledar ";

    Koledar.create({}, function (napaka, koledar) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            dodajVnosVKoledar(odgovor, vnosVKoledar);
        }
    });
};

var dodajVnosVKoledar = function(odgovor, vnosVKoledar) {
    Koledar
        .findOne()
        .exec(function(err, koledar) {
            koledar.vnosi_v_koledarju.push(vnosVKoledar);
            koledar.save(function(napaka) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 400, napaka);
                } else {
                    // responsu dodamo se id, ki je bil pravkar kreiran
                    vnosVKoledar._id = koledar.vnosi_v_koledarju.pop()._id;
                    console.log(vnosVKoledar);
                    vrniJsonOdgovor(odgovor, 200, vnosVKoledar);
                }
            });
        });
};

var vrniVseVnoseUporabnika = function(email, callback) {
    Koledar
        .findOne()
        .exec(function(error, koledar) {
            if (error) {
                callback([]);
            } else {
                if (koledar && koledar.vnosi_v_koledarju) {
                    var vnosi = [];

                    for (var i = 0; i < koledar.vnosi_v_koledarju.length; i++) {
                        if (koledar.vnosi_v_koledarju[i].email === email) {
                            vnosi.push(koledar.vnosi_v_koledarju[i]);
                        }
                    }
                    callback(vnosi);
                } else {
                    callback([]);
                }
            }
        });
};

var preveriVeljavnostDatuma = function(datum) {
    return datum instanceof Date && !isNaN(datum);
};
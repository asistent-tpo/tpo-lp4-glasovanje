var passport = require('passport');
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var TAG = "app_api/controllers/uporabniki.js ";

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

/*
    MAIN METHODS
*/
module.exports.preveri_geslo = function(zahteva, odgovor) {
    if (!zahteva.body.email || !zahteva.body.geslo) {
        vrniJsonOdgovor(odgovor, 400, {
            'error': 'Zahtevani so vsi podatki'
        });
        return;
    }
    passport.authenticate('local', function(napaka, uporabnik, podatki) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }

        if (uporabnik) {
            if(typeof uporabnik.potrditveni_zeton !== 'undefined') {
                vrniJsonOdgovor(odgovor, 401, {
                    'error': 'Elektronski naslov še ni potrjen'
                });
            } else {
                vrniJsonOdgovor(odgovor, 200, {
                    'info': 'Email in geslo sta pravilna'
                });
            }
        } else {
            vrniJsonOdgovor(odgovor, 401, podatki);
        }
    })(zahteva, odgovor);
};

module.exports.preveri_veljavnost_gesla = function(zahteva, odgovor) {
    if (!zahteva.body.novo_geslo || !zahteva.body.novo_geslo_repeat) {
        vrniJsonOdgovor(odgovor, 400, {
            'error': 'Zahtevani so vsi podatki'
        });
        return;
    }

    if (zahteva.body.novo_geslo !== zahteva.body.novo_geslo_repeat) {
        vrniJsonOdgovor(odgovor, 400, {
            'error': 'Gesli se ne ujemata!'
        });
        return;
    }

    if (zahteva.body.novo_geslo.length < 8) {
        vrniJsonOdgovor(odgovor, 400, {
            'error': 'Novo geslo je prekratko!'
        });
        return;
    }

    vrniJsonOdgovor(odgovor, 200, {
        'info': 'Novo geslo je ustrezno!'
    });
};

module.exports.spremeni_geslo = function(zahteva, odgovor) {
    Uporabnik.findOne({email: zahteva.body.email}, function(napaka, uporabnik) {
        if(napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
            return;
        } else if (!uporabnik) {
            vrniJsonOdgovor(odgovor, 404, {
                'error': 'Račun s tem emailom ne obstaja.'
            });
            return;
        }

        uporabnik.nastavi_geslo(zahteva.body.geslo);
        uporabnik.save(function(napaka) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 200, {
                    'info': 'Geslo je bilo uspešno spremenjeno!'
                });
            }
        });
    });
};

/*
    HELPER METHODS
 */

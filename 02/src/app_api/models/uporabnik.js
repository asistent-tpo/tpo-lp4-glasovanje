var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');

var uporabnikShema = new mongoose.Schema({
    email: {type: String, unique: true, required: true},
    hashed_geslo: String,
    dostopni_zeton: String,
    // potrebni parametri, ki jih ni v nacrtu
    potrditveni_zeton: String,
    potrditveni_zeton_datum_izdaje: Date,
    nakljucna_vrednost: String,
    tip_uporabnika: Number
});

uporabnikShema.methods.nastavi_geslo = function(geslo) {
    this.nakljucna_vrednost = crypto.randomBytes(16).toString('hex');
    this.hashed_geslo = crypto.pbkdf2Sync(geslo, this.nakljucna_vrednost, 1000, 64, 'sha512').toString('hex');
}

uporabnikShema.methods.preveri_geslo = function(geslo) {
    var hashed_geslo = crypto.pbkdf2Sync(geslo, this.nakljucna_vrednost, 1000, 64, 'sha512').toString('hex');
    return this.hashed_geslo == hashed_geslo;
}

/**
 * Načrt zahteva argumenta 'email' in 'datumPoteka'
 * -> 'email' je že dostopen na objektu, zato smo argument odstranili
 * -> 'datumPoteka' se lahko generira v metodi. S tem se zmanjša duplikacija kode pri klicu metode 
 */
uporabnikShema.methods.shrani_dostopni_zeton = function() {
    var datumPoteka = new Date();
    datumPoteka.setDate(datumPoteka.getDate() + 7);

    this.dostopni_zeton = jwt.sign({
        _id: this._id,
        email: this.email,
        tipUporabnika: this.tip_uporabnika,
        datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
    }, process.env.JWT_GESLO);
};

uporabnikShema.methods.dodaj_uporabnika_v_seznam_nepotrjenih = function() {
    this.potrditveni_zeton_datum_izdaje = new Date();
    this.potrditveni_zeton = crypto.randomBytes(48).toString('hex');
}

/**
 * Dodana callbacka za uspešno ali neuspešno pošiljanje emaila
 */
uporabnikShema.methods.poslji_potrditveni_email = function(successCb, errorCb) {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.GMAIL_USERNAME,
            pass: process.env.GMAIL_PASSWORD
        }
    });

    var emailText = `<h1>Klikni <a href="${process.env.BASE_URL}/prijava/${this.potrditveni_zeton}">TUKAJ</a> da aktiviras svoj StraightAs racun!</h1>`;

    var mailOptions = {
        from: process.env.GMAIL_USERNAME,
        to: this.email,
        subject: 'Aktiviraj racun',
        html: emailText
    };
 
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            errorCb();
        } else {
            successCb();
        }
    });
}

uporabnikShema.methods.preveri_veljavnost_potrditvenega_zetona = function() {
    var danes = new Date()
    return (Math.abs(danes - this.potrditveni_zeton_datum_izdaje) / 3.6e6) < 48
}

uporabnikShema.methods.odstrani_uporabnika_iz_seznama_nepotrjenih = function() {
    this.potrditveni_zeton = undefined;
    this.potrditveni_zeton_datum_izdaje = undefined;
}

/**
 * Funkcionalnost funkcij
 * -> preveri_dostopni_zeton(email, dostopni_zeton)
 * -> preveri_veljavnost_potrditvenega_zetona(potrditveni_zeton)
 * se implicitno izvede z vsakim klicom na API, na vmesni avtentikacijski ravni (express-jwt)
 */

/**
 * Funkcionalnost nazaj_poslji_dostopni_piskotek() je izpuscena, saj se
 * piskotek oziroma dostopni zeton poslje takoj ob prijavi
 */ 

mongoose.model('Uporabnik', uporabnikShema, 'Uporabniki');
var mongoose = require('mongoose');

var vnosVKoledarjuShema = new mongoose.Schema({
    email: {type: String, required: true},
    ime: {type: String, required: true},
    datum: {type: Date, required: true},
    opis: {type: String, required: true},
    trajanje: {type: Number, required: true}
});

var koledarShema = new mongoose.Schema({
    vnosi_v_koledarju: [vnosVKoledarjuShema]
});

mongoose.model('Koledar', koledarShema, 'Koledarji');
var mongoose = require('mongoose');

var dogodkiVnos = new mongoose.Schema({
    ime: {type: String, required: true},
    datum: {type: Date, required: true},
    organizator: {type: String, required: true},
    opis: {type: String, required: true}
});

var dogodekShema = new mongoose.Schema({
    dogodki: [dogodkiVnos]
});

mongoose.model('Dogodek', dogodekShema, 'Dogodki');
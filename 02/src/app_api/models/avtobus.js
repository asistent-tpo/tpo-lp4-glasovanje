var mongoose = require('mongoose');

var avtobusShema = new mongoose.Schema({
    naziv: {type: String, required: true},
    id_postaje: {type: Number, required: true}
});

mongoose.model('Avtobus', avtobusShema, 'Avtobusi');
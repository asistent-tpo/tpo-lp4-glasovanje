var mongoose = require('mongoose');

var vnosVSeznamuShema = new mongoose.Schema({
    email: {type: String, required: true},
    opis: {type: String, required: true}

});

var seznamShema= new mongoose.Schema({
    vnosi_v_seznamu: [vnosVSeznamuShema]
});


mongoose.model('Seznam', seznamShema, 'Seznami');
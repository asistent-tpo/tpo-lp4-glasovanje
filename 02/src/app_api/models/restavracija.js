var mongoose = require('mongoose');

var restavracijaShema = new mongoose.Schema({
    naziv: {type: String, required: true},
    naslov: {type: String, required: true},
    lat: {type: Number, required: true},
    lng: {type: Number, required: true},
    cena: {type: Number, required: true},
    doplacilo: {type: Number, required: true}
});

mongoose.model('Restavracija', restavracijaShema, 'Restavracije');
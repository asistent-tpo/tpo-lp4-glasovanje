var mongoose = require('mongoose');

var sporocilaVnos = new mongoose.Schema({
    opis: {type: String, required: true}
});

var sporociloShema = new mongoose.Schema({
    sporocila: [sporocilaVnos]
});

mongoose.model('Sporocilo', sporociloShema, 'sporocila');
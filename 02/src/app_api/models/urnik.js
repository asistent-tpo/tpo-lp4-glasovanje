var mongoose = require('mongoose');

var vnosVUrnikuShema = new mongoose.Schema({
    email: {type: String, required: true},
    id: {type: Number, required: true},
    ime: {type: String, required: true},
    trajanje: {type: Number, required: true},
    barva: {type: String, required: true}
});

var urnikShema = new mongoose.Schema({
    vnosi_v_urniku: [vnosVUrnikuShema]
});

mongoose.model('Urnik', urnikShema, 'Urniki');
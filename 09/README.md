# 09. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Uveljavitev: https://bitbucket.org/tpomajstri/tpo-lp4/commits/d945eb29b56686493717674f5129acdbfa2ac85c

Spletna povezava do produkcijske različice: https://str4ight4s.herokuapp.com

Uporabniško ime testnega študenta: asistent-tpo

Geslo testnega študenta: supergeslo12345

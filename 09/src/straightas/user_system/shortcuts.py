from .common import Context


def serializer_error_list(serializer_errors):
    """ Returns a list of serializer errors. """

    lst = []
    keys = serializer_errors.keys()
    for key in keys:
        lst.append({"field": key, "msg": serializer_errors[key]})

    return lst


def serializer_error_resp(serializer_errors, additional_fields={}):
    """
    Returns a dictionary that can be used as Response data to return the list of
    serializer errors. If the dict additional_fields is provided we merge the two
    dicts and return them.
    """

    resp = {"success": False, "errors": serializer_error_list(serializer_errors)}

    return {**resp, **additional_fields}


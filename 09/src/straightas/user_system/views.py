from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from user_system.serializers import *
from user_system.shortcuts import serializer_error_resp
from .common import response_success, response_failure
from .auth.decorators import *
from .auth.jwtutil import create_token_for_user
from .email import confirmation_email, reset_password_email
from .models import *


class StudentRegistration(APIView):

    def post(self, request):

        serializer = AccountRegistrationSerializer(data=request.data)

        if serializer.is_valid():
            username = serializer.data['username']
            email = serializer.data['email']
            first_name = serializer.data['firstName']
            last_name = serializer.data['lastName']
            password = serializer.data['password']

            print("Registering student", {"username": username})

            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name

            user.account.email_verified = False
            user.account.save()
            user.account.account_type = Account.STUDENT_ACCOUNT_TYPE
            user.save()

            # Send confirmation email
            verification_code = user.account.generate_email_verification_code()

            # TODO - do this asynchronously
            confirmation_email(email, verification_code)

            response = {
                "success": True,
                "msg": "user registered successfully"
            }

            return Response(response, status=status.HTTP_201_CREATED)

        print("Failed to register a student", {"errors": serializer.errors})

        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class AccountView(APIView):

    @method_decorator(require_login_response)
    def get(self, request):

        user = request.user

        print("User is getting self data", {"userUUID": user.account.get_uuid()})

        response = {
            "uuid": user.account.get_uuid(),
            "username": user.username,
            "firstName": user.first_name,
            "lastName": user.last_name,
            "email": user.email,
            "emailVerified": user.account.email_verified,
            "accountType": user.account.account_type
        }

        return Response(response, status=status.HTTP_200_OK)


class AccountConfirmEmail(APIView):

    def post(self, request):
        """
        Confirms a account's email.
        """

        serializer = AccountConfirmEmailSerializer(data=request.data)

        if serializer.is_valid():
            verification_code = serializer['emailVerificationCode']

            account = Account.get_by_email_verification_code(verification_code)

            if account:
                account.confirm_email()
                resp = {
                    "token": create_token_for_user(account.user),
                    "msg": "Successfully confirmed email"
                }
                return Response(response_success(resp))

            # No accounts
            resp = {
                "error": "Bad verification code"
            }
            return Response(response_failure(resp), status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class AccountChangePassword(APIView):

    @method_decorator(require_login_response)
    def post(self, request):

        user = request.user
        serializer = AccountChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            print("Attempting to change users password", {"userUUID": user.account.get_uuid()})

            oldPass = serializer.data['oldPassword']
            newPass = serializer.data['newPassword']

            validPass = user.check_password(oldPass)

            if validPass:
                user.set_password(newPass)
                user.save()
                print("Successfully changed users password", {"userUUID": user.account.get_uuid()})

                response = {
                    "success": True,
                    "msg": "successfully changed password"
                }

                return Response(response, status=status.HTTP_200_OK)

            # Bad password
            print("Failed to authenticate users password for password change", {"userUUID": user.account.get_uuid()})

            response = {
                "success": False,
                "error": "wrong password"
            }

            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        print("User sent a bad change password request", {"userUUID": user.account.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class AccountResetPasswordEmail(APIView):

    def post(self, request):
        """
        Send the reset password link.
        """
        serializer = AccountResetPasswordEmailSerializer(data=request.data)

        if serializer.is_valid():
            email = serializer.data['email']

            print("Sending password reset link to the user", {"email": email})

            # Find user
            user = User.objects.filter(email=email)

            if len(user) > 1:
                print("Found multiple users with the same email", {"email": email})
                resp = {
                    "error": "internal server error"
                }
                return Response(response_failure(resp), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            elif len(user) == 1:
                # We found the user
                user = user[0]
                password_reset_code = user.account.generate_password_reset_code()

                reset_password_email(user.email, password_reset_code)

            resp = {
                "msg": "Email with reset link sent"
            }

            return Response(response_success(resp), status=status.HTTP_200_OK)

        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class AccountResetPassword(APIView):

    def post(self, request):
        """
        Uses the password reset code to reset the account's password.
        """
        serializer = AccountResetPasswordSerializer(data=request.data)

        if serializer.is_valid():
            password_reset_code = serializer.data['passwordResetCode']
            new_password = serializer.data['newPassword']

            accounts = Account.objects.filter(password_reset_code=password_reset_code)

            if len(accounts) == 0:
                print("No account's with the password reset code found",
                            {"passwordResetCode": password_reset_code})
                resp = {
                    "error": "Wrong password reset code"
                }
                return Response(response_failure(resp), status=status.HTTP_400_BAD_REQUEST)

            elif len(accounts) == 1:
                account = accounts[0]
                print("Resetting the user's password", {"userId": account.get_uuid()})

                account.password_reset(new_password)

                resp = {
                    "msg": "Successfully changed password"
                }
                return Response(response_success(resp), status=status.HTTP_200_OK)

            else:
                print("Found multiple accounts with the same password reset code",
                               {"passwordResetCode": password_reset_code})
                resp = {
                    "error": "internal server error"
                }
                return Response(response_failure(resp), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
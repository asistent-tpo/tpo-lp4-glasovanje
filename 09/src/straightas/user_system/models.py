import uuid
import random
import string
from uuid import UUID

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    uuid = models.UUIDField(default=uuid.uuid4)
    email_verified = models.BooleanField(default=False)
    email_verification_code = models.CharField(max_length=256, blank=True, default="")
    password_reset_code = models.CharField(max_length=256, blank=True, default="")

    STUDENT_ACCOUNT_TYPE = "STUDENT"
    EVENT_MANAGER_ACCOUNT_TYPE = "EVENT-MANGER"
    ADMINISTRATOR_ACCOUNT_TYPE = "ADMINISTRATOR"

    ACCOUNT_TYPE_CHOICES = (
        (STUDENT_ACCOUNT_TYPE, 'student'),
        (EVENT_MANAGER_ACCOUNT_TYPE, 'event-manager'),
        (ADMINISTRATOR_ACCOUNT_TYPE, 'administrator')
    )
    account_type = models.CharField(max_length=256, choices=ACCOUNT_TYPE_CHOICES, default=STUDENT_ACCOUNT_TYPE)

    def __str__(self):
        return self.user.username

    def get_uuid(self):
        return str(self.uuid)

    @staticmethod
    def username_available(username):
        """
        Returns False if username exists and True if it does not.
        """

        return not User.objects.filter(username__iexact=username).exists()

    @staticmethod
    def email_available(email):
        """
        Returns False if the email exists and True if it does not.
        """

        return not User.objects.filter(email__iexact=email).exists()
    @staticmethod
    def valid_uuid(uuid):
        try:
            UUID(uuid, version=4)
            return True
        except:
            return False

    def generate_email_verification_code(self):
        """
        Generates a random 46 a-Z0-9 characters and save it as the email
        verification code.
        """

        LENGTH = 46

        alphabet = string.ascii_letters + string.digits
        self.email_verification_code = ''.join(random.choice(alphabet) for i in range(LENGTH))
        self.save()

        return self.email_verification_code

    def confirm_email(self):
        """
        Activates the user's account.
        """

        print("Confirming the user's email", {"userId": self.get_uuid()})

        self.email_verified = True
        self.email_verification_code = ""
        self.save()

    @staticmethod
    def get_by_email_verification_code(email_verification_code):
        """
        Return's the account that the email verification belongs to.
        """

        if email_verification_code == "":
            print("Tried to find account using empty email verification code")
            return None

        accounts = Account.objects.filter(email_verified=False, email_verification_code=email_verification_code.value)

        if len(accounts) == 1:
            return accounts[0]

        elif len(accounts) > 1:
            print("Multiple accounts have the same verification code",
                           {"emailVerificationCode": email_verification_code})
        else:
            print("No account has the email verification code",
                        {"emailVerificationCode": email_verification_code})
        return None

    def generate_password_reset_code(self):
        """
        Generates a random 46 a-Z0-9 characters and save it as the
        password reset code.
        """

        LENGTH = 46

        alphabet = string.ascii_letters + string.digits
        self.password_reset_code = ''.join(random.choice(alphabet) for i in range(LENGTH))
        self.save()

        return self.password_reset_code

    def password_reset(self, new_password):
        """
        Resets the user's password
        """

        print("Resetting the account's password", {"userId": self.get_uuid()})

        self.user.set_password(new_password)
        #self.password_reset_code = ""
        self.user.save()


# Registers hook so that when we create a new User (django.contrib.auth.models) we automatically
# create a Account instance.
@receiver(post_save, sender=User)
def create_account(sender, instance, created, **kwargs):
    if created:
        Account.objects.create(user=instance)


# Registers hook so that when we save a User (django.contrib.auth.models) we automatically
# save the Account instance.
@receiver(post_save, sender=User)
def save_account(sender, instance, **kwargs):
    instance.account.save()

from rest_framework.serializers import ValidationError

from user_system.models import Account


def username_available_validator(username):
    """
    Validator to check if the username is already taken or not.
    """

    username_available = Account.username_available(username)

    if not username_available:
        message = "Username {} is not available".format(username)
        raise ValidationError(message)


def email_available_validator(email):
    """
    Validator to check if the email is already taken or not.
    """

    email_available = Account.email_available(email)

    if not email_available:
        message = "Email {} is not available".format(email)
        raise ValidationError(message)

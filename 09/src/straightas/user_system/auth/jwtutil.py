import jwt
import uuid
from datetime import datetime
from django.conf import settings


def create_token_for_user(user):
    """ Returns a string token for the inputted user type. """

    print("Creating token for user", {"userUUID": user.account.get_uuid()})

    payload = {
        "jti": str(uuid.uuid4()),
        "exp": datetime.utcnow() + settings.JWT_EXP,
        "iat": datetime.utcnow(),
        "iss": settings.JWT_ISS,
        "data": {
            "uuid": user.account.get_uuid(),
            "username": user.username,
        }
    }

    return jwt.encode(payload, settings.JWT_SECRET, algorithm='HS256').decode("utf-8")

import traceback

from rest_framework import status
from rest_framework.response import Response


# Custom exception handler to wrap the original response
# into a custom json response.
def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    # response = exception_handler(exc, context)

    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

    try:
        err_resp = {
            "success": False,
            "error": exc.detail
        }

    except Exception as exception:
        stack_trace = traceback.format_exc()
        print("Unexpected exception was triggered",
                     {"stackTrace": stack_trace})

        err_resp = {
            "success": False,
            "error": "internal server error"
        }

    return Response(err_resp, status=status_code)

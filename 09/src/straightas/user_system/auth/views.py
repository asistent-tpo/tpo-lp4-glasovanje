from django.contrib.auth import authenticate
from django.utils.decorators import method_decorator
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from ..shortcuts import serializer_error_resp
from .serializers import TokenSerializer
from .decorators import require_login_response
from .jwtutil import create_token_for_user


class Token(APIView):

    def put(self, request):
        """ Issues a JWT token to the user upon successful authentication. """

        serializers = TokenSerializer(data=request.data)

        if serializers.is_valid():
            username = serializers.data['username']
            password = serializers.data['password']
            user = authenticate(username=username, password=password)

            if user is not None:
                # Successful authentication

                print("User successfully logged in, creating token", {"userUUID": user.account.get_uuid()})

                token = create_token_for_user(user)

                response = {
                    "success": True,
                    "token": token
                }
                return Response(response, status=status.HTTP_201_CREATED)

            print("Failed login for token issue for username", {"username": username})

            response = {
                "success": False,
                "error": "failed to login"
            }
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)

        print("Bad login request", {"errors": serializers.errors})

        return Response(serializer_error_resp(serializers.errors), status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(require_login_response)
    def post(self, request):
        """ Responds with successful: True if the users token is valid. """

        resp = {
            "success": True,
            "msg": "successful authentication"
        }

        return Response(resp, status=status.HTTP_200_OK)

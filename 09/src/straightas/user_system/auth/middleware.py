import re
import jwt
import logging
from django.contrib.auth.models import User
from django.conf import settings
from rest_framework import authentication
from rest_framework import exceptions


class JWTAuthentication(authentication.BaseAuthentication):
    """
    Middleware that checks if there is a JWT authorization HTTP header in the format of:
        Authorization: Bearer <JWT_TOKEN>

    If there is then using the payload username find the user object and attach it to
    request.user
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.bearer_regex = re.compile('^Bearer ([a-zA-Z0-9+\/_\-.]+)$')

    def authenticate(self, request):

        if "HTTP_AUTHORIZATION" in request.META:
            self.logger.debug("Request contains authorization HTTP header, attempting to verify JWT token",
                              extra={"meta": {}})

            token = self.bearer_regex.match(request.META['HTTP_AUTHORIZATION'])

            if token:
                self.logger.debug("Request contains valid authorization JWT HTTP header", extra={"meta": {}})
                token = token.group(1)

                payload = None
                try:
                    # Decode and verify the JWT token
                    payload = jwt.decode(token, settings.JWT_SECRET, algorithms=['HS256'])

                except jwt.ExpiredSignature:
                    _payload = jwt.decode(token, verify=False)

                    username = None
                    try:
                        username = _payload['data']['username']
                    except KeyError:
                        self.logger.warning("Expired JWT payload does not contain the expected payload format structure for the users username",
                                            extra={"meta":{}})

                    self.logger.info("JWT token expired for user", extra={"meta": {"username": username}})
                    raise exceptions.AuthenticationFailed("token expired")

                except Exception as e:
                    self.logger.info("Failed to decode JWT token", extra={"meta": {"error": e}})
                    raise exceptions.AuthenticationFailed("failed to decode JWT")

                if payload:
                    # Payload has been successfully decoded and verified
                    try:
                        username = payload['data']['username']
                    except KeyError:
                        self.logger.warning("Valid JWT payload does not contain the expected payload format structure for the users username",
                                            extra={"meta": {}})
                        username = None

                    if username:
                        # Try to attach a user object to the request using the username information from the JWT payload
                        try:
                            return (User.objects.get(username=username), payload)
                        except User.DoesNotExist:
                            self.logger.warning("Valid JWT payload contains username of a user that does not exist",
                                                extra={"meta": {}})

            else:
                raise exceptions.AuthenticationFailed("bad authorization format")

        return None

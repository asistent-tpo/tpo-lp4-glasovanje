from functools import wraps
from django.http import JsonResponse

from user_system.models import Account


def require_login_response(view_func):
    """
    Decorator that ensures the user is authenticated when accessing the view. Otherwise
    return a HTTP 401 error with the error message: "unauthenticated request".
    """

    def _decorator(request, *args, **kwargs):

        if not request.user.is_authenticated:
            response = {
                "success": False,
                "error": "unauthenticated request"
            }

            return JsonResponse(response, status=401)

        else:
            return view_func(request, *args, **kwargs)

    return wraps(view_func)(_decorator)


def require_login_student_response(view_func):
    return require_login_account_type_response(view_func, [Account.STUDENT_ACCOUNT_TYPE])


def require_login_event_manager_response(view_func):
    return require_login_account_type_response(view_func, [Account.EVENT_MANAGER_ACCOUNT_TYPE])


def require_login_administrator_response(view_func):
    return require_login_account_type_response(view_func, [Account.ADMINISTRATOR_ACCOUNT_TYPE])


def require_login_student_event_manager_response(view_func):
    return require_login_account_type_response(view_func, [Account.STUDENT_ACCOUNT_TYPE,
                                                           Account.EVENT_MANAGER_ACCOUNT_TYPE])


def require_login_account_type_response(view_func, account_types):
    """
    Decorator that ensures the user is authenticated when accessing the view. Otherwise
    return a HTTP 401 error with the error message: "unauthenticated request".
    """

    def _decorator(request, *args, **kwargs):

        if not request.user.is_authenticated \
                or request.user.account.account_type not in account_types:
            response = {
                "success": False,
                "error": "unauthenticated request"
            }

            return JsonResponse(response, status=401)

        else:
            return view_func(request, *args, **kwargs)

    return wraps(view_func)(_decorator)

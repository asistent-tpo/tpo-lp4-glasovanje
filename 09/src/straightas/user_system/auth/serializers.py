from rest_framework import serializers

# please check the django auth User model to make sure that the model fields support these sizes
username_length = 32
password_length = 64


class TokenSerializer(serializers.Serializer):
    username = serializers.CharField(required=True, allow_blank=False, max_length=username_length)
    password = serializers.CharField(required=True, allow_blank=False, max_length=password_length)

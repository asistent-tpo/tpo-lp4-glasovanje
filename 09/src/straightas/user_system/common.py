import uuid


def get_request_tracking_id(request, generate_new_if_does_not_exist=True):
    """
    Get the tracking_id from the HTTP request (header field X-TRACKING-ID).
    In case the HTTP request does not contain a tracking_id, then
    generate a new one.
    """

    try:
        return request.META["HTTP_X_TRACKING_ID"]
    except KeyError:
        if generate_new_if_does_not_exist:
            print("Request did not contain a tracking id, generating a new one")
            return generate_tracking_id()
        return None


def generate_tracking_id():
    return str(uuid.uuid4())


class Context:
    """ Used to represent a request/tracking context. """

    tracking_id = None

    def __init__(self, tracking_id=None):
        if tracking_id is None:
            tracking_id = generate_tracking_id()
        self.tracking_id = tracking_id

    def get_tracking_id(self):
        return self.tracking_id


def response_success(extra={}):
    r = {'success': True}
    return {**r, **extra}


def response_failure(extra={}):
    r = {'success': False}
    return {**r, **extra}


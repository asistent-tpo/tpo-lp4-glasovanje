from rest_framework import serializers

from user_system.models import Account
from user_system.validators import username_available_validator, email_available_validator

username_length = 32
username_length_min = 3
password_length = 64
password_length_min = 6
email_length = 200
first_name_length = 30
last_name_length = 50

password_reset_code_length = Account._meta.get_field('password_reset_code').max_length
email_verification_code_length = Account._meta.get_field('email_verification_code').max_length


class AccountRegistrationSerializer(serializers.Serializer):

    username = serializers.CharField(required=True, allow_blank=False, min_length=username_length_min,
                                     max_length=username_length, validators=[username_available_validator])
    email = serializers.EmailField(required=True, allow_blank=False, max_length=email_length,
                                   validators=[email_available_validator])
    firstName = serializers.CharField(required=True, allow_blank=False, max_length=first_name_length)
    lastName = serializers.CharField(required=True, allow_blank=False, max_length=last_name_length)
    password = serializers.CharField(required=True, allow_blank=False, min_length=password_length_min,
                                     max_length=password_length)

class AccountConfirmEmailSerializer(serializers.Serializer):
    emailVerificationCode = serializers.CharField(required=True, allow_blank=False,
                                             max_length=email_verification_code_length, validators=[])


class AccountChangePasswordSerializer(serializers.Serializer):
    oldPassword = serializers.CharField(required=True, allow_blank=False, min_length=password_length_min,
                                        max_length=password_length)
    newPassword = serializers.CharField(required=True, allow_blank=False, min_length=password_length_min,
                                        max_length=password_length)

class AccountResetPasswordEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, allow_blank=False, max_length=email_length)


class AccountResetPasswordSerializer(serializers.Serializer):
    passwordResetCode = serializers.CharField(required=True, allow_blank=False, max_length=password_reset_code_length)
    newPassword = serializers.CharField(required=True, allow_blank=False, min_length=password_length_min,
                                        max_length=password_length)

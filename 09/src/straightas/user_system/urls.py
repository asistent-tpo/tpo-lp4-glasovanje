from django.urls import re_path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *
from .auth.views import Token

urlpatterns = [
    re_path('^auth/$', Token.as_view(), name='token'),
    re_path('^user/register-student/$', StudentRegistration.as_view(), name='student-registration'),
    re_path('^user/change-password/$', AccountChangePassword.as_view(), name='account-change-password'),
    re_path('^user/reset-password/email/$', AccountResetPasswordEmail.as_view(), name='account-reset-password-email'),
    re_path('^user/reset-password/$', AccountResetPassword.as_view(), name='account-reset-password'),
    re_path('^user/confirm-email/$', AccountConfirmEmail.as_view(),
            name='account-confirm-email'),
    re_path('^user/$', AccountView.as_view(), name='account'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

from django.core.mail import send_mail
from django.template.loader import get_template
from smtplib import SMTPException


def confirmation_email(email, verification_code):
    """
    Sends a confirmation email to the user which contains the verification
    code (as a link), which when visited confirms the user's email.
    """

    print("Sending confirmation email to user", {"email": email})

    SUBJECT = "Email Confirmation"
    FROM_EMAIL = ""  # Use default DEFAULT_FROM_EMAIL
    verification_url = "https://str4ight4s.herokuapp.com/#/account/verify?code=" + verification_code # TODO dynamic

    body_template_plain = get_template('user_system/email/email_confirmation.txt')
    body_template_html = get_template('user_system/email/email_confirmation.html')

    body_plain = body_template_plain.render({
        "verification_url": verification_url,
    })

    body_html = body_template_html.render({
        "verification_url": verification_url,
    })

    try:
        send_mail(SUBJECT, body_plain, FROM_EMAIL, [email], fail_silently=False, html_message=body_html)
    except SMTPException:
        print("Failed to send confirmation email to user", {"email": email})
        return False

    return True


def reset_password_email(email, reset_password_code):
    """
    Sends a reset password email to the user which contains the reset password
    code (as a link), which when visited enables the user to reset their password.
    """

    print("Sending reset password email to user", {"email": email})

    SUBJECT = "Reset Password"
    FROM_EMAIL = ""  # Use default DEFAULT_FROM_EMAIL
    password_reset_url = "https://str4ight4s.herokuapp.com/#/account/reset-password?code=" + reset_password_code  # TODO dynamic

    body_template_plain = get_template('user_system/email/reset_password.txt')
    body_template_html = get_template('user_system/email/reset_password.html')

    body_plain = body_template_plain.render({
        "password_reset_url": password_reset_url,
    })

    body_html = body_template_html.render({
        "password_reset_url": password_reset_url,
    })

    try:
        send_mail(SUBJECT, body_plain, FROM_EMAIL, [email], fail_silently=False, html_message=body_html)
    except SMTPException:
        print("Failed to send reset password email to user", {"email": email})
        return False

    return True

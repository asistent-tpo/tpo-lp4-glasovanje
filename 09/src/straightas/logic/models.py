import uuid

from django.db import models

from user_system.models import Account


class TodoElement(models.Model):
    p_key = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    description = models.TextField(blank=True, default="")

    def __str__(self):
        return str(self.get_uuid())

    def get_uuid(self):
        return str(self.uuid)

    def get_username(self):
        return self.account.user.username


class Event(models.Model):
    p_key = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, blank=False, default="")
    time = models.DateTimeField(blank=False)
    durationMinutes = models.IntegerField(default=60)
    description = models.TextField(blank=True, default="")

    def __str__(self):
        return str(self.get_uuid())

    def get_uuid(self):
        return str(self.uuid)

    def get_username(self):
        return self.account.user.username


class Schedule(models.Model):
    p_key = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, blank=False, default="")

    MONDAY_TIME_TYPE = "MON"
    TUESDAY_TIME_TYPE = "TUE"
    WEDNESDAY_TIME_TYPE = "WED"
    THURSDAY_TIME_TYPE = "THU"
    FRIDAY_TIME_TYPE = "FRI"
    SATURDAY_TIME_TYPE = "SAT"
    SUNDAY_TIME_TYPE = "SUN"

    TIME_TYPE_CHOICES = (
        (MONDAY_TIME_TYPE, "Mon"),
        (TUESDAY_TIME_TYPE, "Tue"),
        (WEDNESDAY_TIME_TYPE, "Wed"),
        (THURSDAY_TIME_TYPE, "Thu"),
        (FRIDAY_TIME_TYPE, "Fri"),
        (SATURDAY_TIME_TYPE, "Sat"),
        (SUNDAY_TIME_TYPE, "Sun")
    )

    day = models.CharField(max_length=16, choices=TIME_TYPE_CHOICES, default=MONDAY_TIME_TYPE)
    hours = models.IntegerField(default=12)
    minutes = models.IntegerField(default=30)
    durationMinutes = models.IntegerField(default=60)

    BLUE_COLOR_TYPE = "#35F"
    RED_COLOR_TYPE = "#F33"
    YELLOW_COLOR_TYPE = "#EF4"
    GREEN_COLOR_TYPE = "#4F4"
    PURPLE_COLOR_TYPE = "#B0B"
    LIGHT_BLUE_COLOR_TYPE = "#5FF"
    BLACK_COLOR_TYPE = "#000"
    WHITE_COLOR_TYPE = "#FFF"

    COLOR_TYPE_CHOICES = (
        (BLUE_COLOR_TYPE, "blue"),
        (RED_COLOR_TYPE, "red"),
        (YELLOW_COLOR_TYPE, "yellow"),
        (GREEN_COLOR_TYPE, "green"),
        (PURPLE_COLOR_TYPE, "purple"),
        (LIGHT_BLUE_COLOR_TYPE, "light blue"),
        (BLACK_COLOR_TYPE, "black"),
        (WHITE_COLOR_TYPE, "white")
    )

    color = models.CharField(max_length=7, choices=COLOR_TYPE_CHOICES, default=GREEN_COLOR_TYPE)

    def __str__(self):
        return str(self.get_uuid())

    def get_uuid(self):
        return str(self.uuid)

    def get_username(self):
        return self.account.user.username


class Restaurant(models.Model):
    p_key = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, blank=False, default="")
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    price = models.FloatField(default=0.0)

    def __str__(self):
        return str(self.get_uuid())

    def get_uuid(self):
        return str(self.uuid)

    def get_username(self):
        return self.account.user.username


class Alert(models.Model):
    p_key = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    message = models.TextField(blank=True, default="")
    receivers = models.ManyToManyField(Account,
                                       through='AlertMessage',
                                       related_name='receive')

    def __str__(self):
        return str(self.get_uuid())

    def get_uuid(self):
        return str(self.uuid)

    def get_username(self):
        return self.account.user.username


class AlertMessage(models.Model):
    alert = models.ForeignKey(Alert, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)


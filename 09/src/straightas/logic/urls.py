from django.urls import re_path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns = [
    re_path('^todo/$', TodoView.as_view(), name='todo'),
    re_path('^todo/(?P<uuid>[a-f0-9-]+)/$', TodoByIdView.as_view(), name='todo-by-id'),
    re_path('^event/$', EventView.as_view(), name='event'),
    re_path('^event/(?P<uuid>[a-f0-9-]+)/$', EventByIdView.as_view(), name='event-by-id'),
    re_path('^event/event-manager/$', EventManagerView.as_view(), name='event-manager'),
    re_path('^schedule/$', ScheduleView.as_view(), name='schedule'),
    re_path('^schedule/(?P<uuid>[a-f0-9-]+)/$', ScheduleByIdView.as_view(), name='schedule-by-id'),
    re_path('^restaurant-list/$', RestaurantView.as_view(), name='restaurant'),
    re_path('^alert/$', AlertView.as_view(), name='alert'),
    re_path('^alert/(?P<uuid>[a-f0-9-]+)/$', AlertByIdView.as_view(), name='alert-by-id'),
    re_path('^user/alert/$', AlertUserView.as_view(), name='alert-user'),
    re_path('^user/alert/(?P<uuid>[a-f0-9-]+)/$', AlertUserByIdView.as_view(), name='alert-user-by-id'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

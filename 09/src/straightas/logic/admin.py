from django.contrib import admin

from .models import *

admin.site.register(TodoElement)
admin.site.register(Event)
admin.site.register(Schedule)
admin.site.register(Restaurant)
admin.site.register(Alert)

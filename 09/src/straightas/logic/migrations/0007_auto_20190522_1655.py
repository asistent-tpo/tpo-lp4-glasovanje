# Generated by Django 2.2.1 on 2019-05-22 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logic', '0006_auto_20190522_1553'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='schedule',
            name='description',
        ),
        migrations.AddField(
            model_name='schedule',
            name='color',
            field=models.CharField(choices=[('#35F', 'blue'), ('#F33', 'red'), ('#EF4', 'yellow'), ('#4F4', 'green'), ('#B0B', 'purple'), ('#5FF', 'light blue'), ('#000', 'black'), ('#FFF', 'white')], default='#4F4', max_length=4),
        ),
    ]

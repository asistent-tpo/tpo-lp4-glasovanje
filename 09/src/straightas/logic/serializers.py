from rest_framework import serializers

todo_element_description_max_length = 512
todo_element_description_min_length = 0

alert_message_max_length = 256
alert_message_min_length = 0


class TodoElementSerializer(serializers.Serializer):
    description = serializers.CharField(required=True, allow_blank=False,
                                        min_length=todo_element_description_min_length,
                                        max_length=todo_element_description_max_length)


class EventSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256, allow_blank=False)
    time = serializers.DateTimeField()
    durationMinutes = serializers.IntegerField(allow_null=False)
    description = serializers.CharField(required=True, allow_blank=False,
                                        min_length=todo_element_description_min_length,
                                        max_length=todo_element_description_max_length)


class ScheduleSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256, allow_blank=False)
    day = serializers.CharField(max_length=10, allow_blank=False)
    hours = serializers.IntegerField(allow_null=False)
    minutes = serializers.IntegerField(allow_null=False)
    durationMinutes = serializers.IntegerField(allow_null=False)
    color = serializers.CharField(allow_blank=False)


class RestaurantSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256, allow_blank=False)
    latitude = serializers.FloatField(allow_null=False)
    longitude = serializers.FloatField(allow_null=False)
    price = serializers.FloatField(allow_null=False)


class AlertSerializer(serializers.Serializer):
    message = serializers.CharField(required=True, allow_blank=False,
                                    min_length=alert_message_min_length,
                                    max_length=alert_message_max_length)

from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from logic.models import TodoElement, Event, Schedule, Restaurant, Alert
from logic.serializers import TodoElementSerializer, EventSerializer, ScheduleSerializer, AlertSerializer
from user_system.auth.decorators import *
from user_system.shortcuts import serializer_error_resp


class TodoView(APIView):

    @method_decorator(require_login_student_response)
    def get(self, request):

        user = request.user

        print("User is getting todo list", {"userUUID": user.account.get_uuid()})

        todo_list = TodoElement.objects.filter(account=user.account).order_by('-p_key')

        response = {
            "todoList": list(map(lambda todo: {
                "id": todo.get_uuid(),
                "ownerId": todo.account.get_uuid(),
                "description": todo.description,
            }, todo_list))
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_student_response)
    def put(self, request):
        user = request.user

        serializer = TodoElementSerializer(data=request.data)

        if serializer.is_valid():
            print("User is creating a todo", {"userUUID": user.account.get_uuid()})

            todo = TodoElement()
            todo.description = serializer.data['description']
            todo.account = user.account
            todo.save()

            response = {
                "success": True,
                "todoElementId": todo.get_uuid(),
                "msg": "successfully created a todo element"
            }

            return Response(response, status=status.HTTP_201_CREATED)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class TodoByIdView(APIView):

    @method_decorator(require_login_student_response)
    def get(self, request, uuid):

        user = request.user

        print("User is getting a todo", {"userUUID": user.account.get_uuid()})

        todo = TodoElement.objects.get(uuid=uuid)

        response = {
            "id": todo.get_uuid(),
            "description": todo.description,
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_student_event_manager_response)
    def post(self, request, uuid):
        user = request.user

        serializer = TodoElementSerializer(data=request.data)

        if serializer.is_valid():
            print("User is updating a todo", {"userUUID": user.account.get_uuid()})

            todo = TodoElement.objects.get(uuid=uuid)
            todo.description = serializer.data['description']
            todo.save()

            response = {
                "success": True,
                "todoElementId": todo.get_uuid(),
                "msg": "successfully updated a todo element"
            }

            return Response(response, status=status.HTTP_200_OK)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(require_login_student_event_manager_response)
    def delete(self, request, uuid):
        user = request.user

        print("User is deleting a todo", {"userUUID": user.account.get_uuid()})

        todo = TodoElement.objects.get(uuid=uuid)
        todo.delete()

        response = {
            "success": True,
            "msg": "successfully deleted a todo element"
        }

        return Response(response, status=status.HTTP_200_OK)


class EventView(APIView):

    @method_decorator(require_login_student_event_manager_response)
    def get(self, request):
        user = request.user

        print("User is getting an event list", {"userUUID": user.account.get_uuid()})

        event_list = Event.objects.filter(account=user.account).order_by('-p_key')

        response = {
            "eventList": list(map(lambda event: {
                "id": event.get_uuid(),
                "ownerId": event.account.get_uuid(),
                "name": event.name,
                "time": event.time,
                "durationMinutes": event.durationMinutes,
                "description": event.description,
            }, event_list))
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_student_event_manager_response)
    def put(self, request):
        user = request.user

        serializer = EventSerializer(data=request.data)

        if serializer.is_valid():
            print("User is creating an event", {"userUUID": user.account.get_uuid()})

            event = Event()
            event.account = user.account
            event.name = serializer.data['name']
            event.time = serializer.data['time']
            event.durationMinutes = serializer.data['durationMinutes']
            event.description = serializer.data['description']
            event.save()

            response = {
                "success": True,
                "eventId": event.get_uuid(),
                "msg": "successfully created an event"
            }

            return Response(response, status=status.HTTP_201_CREATED)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class EventManagerView(APIView):

    @method_decorator(require_login_student_event_manager_response)
    def get(self, request):
        user = request.user

        print("User is getting an event list", {"userUUID": user.account.get_uuid()})

        event_list = Event.objects.filter(account__account_type=Account.EVENT_MANAGER_ACCOUNT_TYPE).order_by('-p_key')

        response = {
            "eventList": list(map(lambda event: {
                "id": event.get_uuid(),
                "ownerId": event.account.get_uuid(),
                "name": event.name,
                "time": event.time,
                "durationMinutes": event.durationMinutes,
                "description": event.description,
            }, event_list))
        }

        return Response(response, status=status.HTTP_200_OK)


class EventByIdView(APIView):

    @method_decorator(require_login_student_event_manager_response)
    def get(self, request, uuid):
        user = request.user

        print("User is getting an event", {"userUUID": user.account.get_uuid()})

        event = Event.objects.get(uuid=uuid)

        response = {
            "id": event.get_uuid(),
            "name": event.name,
            "time": event.time,
            "durationMinutes": event.durationMinutes,
            "description": event.description,
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_student_event_manager_response)
    def post(self, request, uuid):
        user = request.user

        serializer = EventSerializer(data=request.data)

        if serializer.is_valid():
            print("User is updating an event", {"userUUID": user.account.get_uuid()})

            event = Event.objects.get(uuid=uuid)
            event.account = user.account
            event.name = serializer.data['name']
            event.time = serializer.data['time']
            event.durationMinutes = serializer.data['durationMinutes']
            event.description = serializer.data['description']
            event.save()

            response = {
                "success": True,
                "eventId": event.get_uuid(),
                "msg": "successfully updated an event"
            }

            return Response(response, status=status.HTTP_200_OK)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(require_login_student_event_manager_response)
    def delete(self, request, uuid):
        user = request.user

        print("User is deleting an event", {"userUUID": user.account.get_uuid()})

        event = Event.objects.get(uuid=uuid)
        event.delete()

        response = {
            "success": True,
            "msg": "successfully deleted an event"
        }

        return Response(response, status=status.HTTP_200_OK)


class ScheduleView(APIView):

    @method_decorator(require_login_student_event_manager_response)
    def get(self, request):
        user = request.user

        print("User is getting a schedule list", {"userUUID": user.account.get_uuid()})

        schedule_list = Schedule.objects.filter(account=user.account).order_by('-p_key')

        response = {
            "scheduleList": list(map(lambda schedule: {
                "id": schedule.get_uuid(),
                "ownerId": schedule.account.get_uuid(),
                "name": schedule.name,
                "day": schedule.day,
                "hours": schedule.hours,
                "minutes": schedule.minutes,
                "durationMinutes": schedule.durationMinutes,
                "color": schedule.color,
            }, schedule_list))
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_student_event_manager_response)
    def put(self, request):
        user = request.user

        serializer = ScheduleSerializer(data=request.data)

        if serializer.is_valid():
            print("User is creating a schedule", {"userUUID": user.account.get_uuid()})

            schedule = Schedule()
            schedule.account = user.account
            schedule.name = serializer.data['name']
            schedule.day = serializer.data['day']
            schedule.hours = serializer.data['hours']
            schedule.minutes = serializer.data['minutes']
            schedule.durationMinutes = serializer.data['durationMinutes']
            schedule.color = serializer.data['color']
            schedule.save()

            response = {
                "success": True,
                "scheduleId": schedule.get_uuid(),
                "msg": "successfully created a schedule"
            }

            return Response(response, status=status.HTTP_201_CREATED)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class ScheduleByIdView(APIView):

    @method_decorator(require_login_student_event_manager_response)
    def get(self, request, uuid):
        user = request.user

        print("User is getting a schedule", {"userUUID": user.account.get_uuid()})

        schedule = Schedule.objects.get(uuid=uuid)

        response = {
            "id": schedule.get_uuid(),
            "ownerId": schedule.account.get_uuid(),
            "name": schedule.name,
            "day": schedule.day,
            "hours": schedule.hours,
            "minutes": schedule.minutes,
            "durationMinutes": schedule.durationMinutes,
            "color": schedule.color,
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_student_event_manager_response)
    def post(self, request, uuid):
        user = request.user

        serializer = ScheduleSerializer(data=request.data)

        if serializer.is_valid():
            print("User is updating a schedule", {"userUUID": user.account.get_uuid()})

            schedule = Schedule.objects.get(uuid=uuid)
            schedule.account = user.account
            schedule.name = serializer.data['name']
            schedule.day = serializer.data['day']
            schedule.hours = serializer.data['hours']
            schedule.minutes = serializer.data['minutes']
            schedule.durationMinutes = serializer.data['durationMinutes']
            schedule.color = serializer.data['color']
            schedule.save()

            response = {
                "success": True,
                "scheduleId": schedule.get_uuid(),
                "msg": "successfully updated a schedule"
            }

            return Response(response, status=status.HTTP_200_OK)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(require_login_student_event_manager_response)
    def delete(self, request, uuid):
        user = request.user

        print("User is deleting a schedule", {"userUUID": user.account.get_uuid()})

        schedule = Schedule.objects.get(uuid=uuid)
        schedule.delete()

        response = {
            "success": True,
            "msg": "successfully deleted a schedule"
        }

        return Response(response, status=status.HTTP_200_OK)


class RestaurantView(APIView):

    def get(self, request):

        print("User is getting a restaurant list")

        restaurant_list = Restaurant.objects.order_by('-p_key')

        response = {
            "restaurantList": list(map(lambda restaurant: {
                "id": restaurant.get_uuid(),
                "ownerId": restaurant.account.get_uuid(),
                "name": restaurant.name,
                "latitude": restaurant.latitude,
                "longitude": restaurant.longitude,
                "price": restaurant.price,
            }, restaurant_list))
        }

        return Response(response, status=status.HTTP_200_OK)


class AlertView(APIView):

    @method_decorator(require_login_administrator_response)
    def get(self, request):
        user = request.user

        print("User is getting an alert list", {"userUUID": user.account.get_uuid()})

        alert_list = Alert.objects.filter(account=user.account).order_by('-p_key')

        response = {
            "alertList": list(map(lambda alert: {
                "id": alert.get_uuid(),
                "ownerId": alert.account.get_uuid(),
                "message": alert.message,
            }, alert_list))
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_administrator_response)
    def put(self, request):
        user = request.user

        serializer = AlertSerializer(data=request.data)

        if serializer.is_valid():
            print("User is creating an alert", {"userUUID": user.account.get_uuid()})

            alert = Alert()
            alert.account = user.account
            alert.message = serializer.data['message']
            alert.save()

            response = {
                "success": True,
                "alertId": alert.get_uuid(),
                "msg": "successfully created an alert"
            }

            return Response(response, status=status.HTTP_201_CREATED)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


class AlertByIdView(APIView):

    @method_decorator(require_login_administrator_response)
    def get(self, request, uuid):
        user = request.user

        print("User is getting an alert", {"userUUID": user.account.get_uuid()})

        alert = Alert.objects.get(uuid=uuid)

        response = {
            "id": alert.get_uuid(),
            "ownerId": alert.account.get_uuid(),
            "message": alert.message,
        }

        return Response(response, status=status.HTTP_200_OK)

    @method_decorator(require_login_administrator_response)
    def post(self, request, uuid):
        user = request.user

        serializer = AlertSerializer(data=request.data)

        if serializer.is_valid():
            print("User is updating an alert", {"userUUID": user.account.get_uuid()})

            alert = Alert.objects.get(uuid=uuid)
            alert.account = user.account
            alert.message = serializer.data['message']
            alert.save()

            response = {
                "success": True,
                "alertId": alert.get_uuid(),
                "msg": "successfully updated an alert"
            }

            return Response(response, status=status.HTTP_200_OK)

        print("User sent a bad body", {"userUUID": user.player.get_uuid()})

        # Serializer failed
        return Response(serializer_error_resp(serializer.errors), status=status.HTTP_400_BAD_REQUEST)

    @method_decorator(require_login_administrator_response)
    def delete(self, request, uuid):
        user = request.user

        print("User is deleting an alert", {"userUUID": user.account.get_uuid()})

        alert = Alert.objects.get(uuid=uuid)
        alert.delete()

        response = {
            "success": True,
            "msg": "successfully deleted an alert"
        }

        return Response(response, status=status.HTTP_200_OK)


class AlertUserView(APIView):

    @method_decorator(require_login_student_response)
    def get(self, request):
        user = request.user

        print("User is getting an alert list", {"userUUID": user.account.get_uuid()})

        alert_list = Alert.objects.filter(account=user.account).order_by('-p_key')

        response = {
            "alertList": list(map(lambda alert: {
                "id": alert.get_uuid(),
                "ownerId": alert.account.get_uuid(),
                "message": alert.message,
            }, alert_list))
        }

        return Response(response, status=status.HTTP_200_OK)


class AlertUserByIdView(APIView):

    @method_decorator(require_login_student_response)
    def delete(self, request, uuid):
        user = request.user

        print("User is deleting an alert", {"userUUID": user.account.get_uuid()})

        alert = Alert.objects.get(uuid=uuid)
        alert.delete()

        response = {
            "success": True,
            "msg": "successfully deleted an alert"
        }

        return Response(response, status=status.HTTP_200_OK)

from django.http import JsonResponse
from rest_framework import status
from rest_framework.views import APIView


class HomepageView(APIView):

    def get(self, request):
        resp_dict = {
            "message": "Hello World 6.0"
        }
        return JsonResponse(resp_dict, status=status.HTTP_200_OK)

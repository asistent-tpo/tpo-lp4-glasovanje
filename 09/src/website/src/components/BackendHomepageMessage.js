import React, { Component } from 'react';
import { STRAIGHT_AS_BACKEND_URL } from '../utils/constants';


class BackendHomepageMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
  }

  componentDidMount() {
    fetch(STRAIGHT_AS_BACKEND_URL)
      .then( resp => resp.json() )
      .then( text => {
        this.setState({
          message: text["message"]
        });
      });
  }

  render() {
    const { message } = this.state;

    return (
      <div>
        <p>StraightAs backend: {message}</p>
      </div>
    );
  }
}

export default BackendHomepageMessage;

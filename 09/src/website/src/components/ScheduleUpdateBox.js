import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import ModalBox from './ModalBox';
import { CirclePicker } from 'react-color';

class ScheduleUpdateBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            ime: "",
            dan: "",
            zacetek: "",

            modal: {
				show: false,
				title: "",
				body: ""
			}
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
            ime: newProps.values.item.name,
            dan: newProps.values.item.day,
            zacetek: newProps.values.item.hours+":"+newProps.values.item.minutes+"0"
        });
    }

    closeModal() {
        this.setState({
            show: false
        });
        this.props.closeModalUpdate();
    }

    closeAlertModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

    updateClass() {
        console.log(this.state.color)
        const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendUpdatePost();	
    }

    validateInput() {
    	if(this.state.ime === "") {
      		return "Ime";
    	}

    	if(this.state.zacetek === "") {
      		return "Začetek";
        }
        
        const zacetek = this.state.zacetek;
        if(zacetek.split(":").length === 1 || zacetek.split(":").length > 2) {
            return "Začetek";
        }
        if(isNaN(zacetek.split(":")[0])) {
            return "Začetek";
        }
        if(isNaN(zacetek.split(":")[1])) {
            return "Začetek";
        }

    	return "Ok";
    }
    
    sendUpdatePost() {
        fetch('https://sk9-straightas.herokuapp.com/schedule/'+this.props.values.item.id+"/", {
      		method: 'POST',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
				'name': this.state.ime,
                'day': this.state.dan,
                'hours': this.state.zacetek.split(":")[0],
                'minutes': this.state.zacetek.split(":")[1],
                'durationMinutes': 45,
                'color': this.state.color
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndUpdateScheduleItem(this.props.values.item.id);

            this.closeModal();
    	});
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    deleteClass() {
        fetch('https://sk9-straightas.herokuapp.com/schedule/'+this.props.values.item.id, {
      		method: 'DELETE',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
		})
		.then(response => response.json())
		.then(data => {
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndDeleteScheduleItem(this.props.values.item.id);

            this.closeModal();
    	});
    }

    handleChangeComplete = (color) => {
        this.setState({
            color: color.hex
        });
    }

    render() {
        return (
            <div>
                <ModalBox values={this.state.modal} closeModal={this.closeAlertModal.bind(this)}/>
                <Modal dialogClassName="SceduleNewModal" show={this.state.show} centered={true} onHide={e => this.closeModal(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Dodaj predmet</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control 
                            name="ime"
                            value={this.state.ime || ''}
                            onChange={this.handleChange.bind(this)}
                            type="text" 
                            placeholder="Ime predmeta"/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                                as="select"
                                name="dan"
                                value={this.state.dan || ''}
                                onChange={this.handleChange.bind(this)}>
                                <option>Pon</option>
                                <option>Tor</option>
                                <option>Sre</option>
                                <option>Čet</option>
                                <option>Pet</option>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                            name="zacetek"
                            type="text" 
                            placeholder="Začetek (npr. 13:00)"
                            value={this.state.zacetek || ''}
                            onChange={this.handleChange.bind(this)}>
                            </Form.Control>
                        </Form.Group>

                        <CirclePicker 
                            color={this.state.color}
                            onChangeComplete={this.handleChangeComplete} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.updateClass(e)}>
                            Posodobi
                        </Button>

                        <Button variant="danger" onClick={e => this.deleteClass(e)}>
                            Odstrani
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default ScheduleUpdateBox;
import React, { Component } from 'react';
import { Table, Button} from "react-bootstrap";
import CalendarAddBox from "./CalendarAddBox";
import CalendarUpdateBox from "./CalendarUpdateBox";

const month = ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"];

const weeks = [
    0,
    1,
    2,
    3,
    4,
    5
];

const days = [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
];

class Calendar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            events: [],
            monthSelected: new Date().getMonth(),
            yearSelected: new Date().getFullYear(),

            modalAdd: {
                show: false
            },

            modalUpdate: {
                show: false,
                item: {}
            }
        }
    }

    componentDidMount() {
        this.getEvents();
    }

    getEvents() {
        fetch('https://sk9-straightas.herokuapp.com/event/', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
        })
        .then(response => response.json())
        .then(data => {
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }
            this.setState({
            events: data.eventList
            });
        });
    }

    prevMonth() {
        this.setState({
            monthSelected: this.state.monthSelected-1
        });
    }

    nextMonth() {
        this.setState({
            monthSelected: this.state.monthSelected+1
        });
    }

    addEvent() {
        this.setState({
            modalAdd: {
              show: true
            }
        });
    }

    closeModalAdd() {
        this.setState({
          modalAdd: {
            show: false
          }
        });
      }
    
      closeModalUpdate() {
        this.setState({
          modalUpdate: {
            show: false,
            item: {}
          }
        });
      }

    getAndAddEvent(id) {
        fetch('https://sk9-straightas.herokuapp.com/event/'+id+"/", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
        })
        .then(response => response.json())
        .then(event => {
            if(event.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            let currentCalendar = this.state.events;
            currentCalendar.push(event);

            this.setState({
                events: currentCalendar
            });
        });
    }

    updateEvent(e, event) {
        //console.log(id);
        this.setState({
            modalUpdate: {
              show: true,
              item: event
            }
        });
    }

    getAndUpdateEvent(id) {
        let currentEvents = this.state.events.filter(item => {
            return item.id !== id;
        });
        this.setState({
            events: currentEvents
        }, () => {this.getAndAddEvent(id)});
    }

    getAndDeleteEvent(id) {
        let currentEvents = this.state.events.filter(item => {
            return item.id !== id;
        });
        this.setState({
            events: currentEvents
        });
    }

    render() {
        const thisYear = this.state.yearSelected;
        const thisMonth = this.state.monthSelected;

        let dayIterator = 1;
        const lastDayDate = new Date(thisYear, thisMonth+1, 0);
        let currentDayDate = new Date(thisYear, thisMonth, dayIterator);
        return (
        <React.Fragment>
            <CalendarAddBox 
                values={this.state.modalAdd} 
                closeModalAdd={this.closeModalAdd.bind(this)}
                getAndAddEvent={this.getAndAddEvent.bind(this)} />

            <CalendarUpdateBox 
                values={this.state.modalUpdate} 
                closeModalUpdate={this.closeModalUpdate.bind(this)} 
                getAndUpdateEvent={this.getAndUpdateEvent.bind(this)} 
                getAndDeleteEvent={this.getAndDeleteEvent.bind(this)} />

                <Table striped bordered className="ScheduleContainer">
                    <thead>
                    <tr className="SmallMain Center">
                        <th colSpan="1">
                            <Button
                                variant="danger"
                                onClick={e => this.addEvent(e)}>
                                    Dodaj
                            </Button>    
                        </th>
                        <th colSpan="1"></th>
                        <th colSpan="1">
                            <Button 
                                className="CalendarTitleMargin" 
                                variant="danger"
                                onClick={e => this.prevMonth(e)}>
                                    &lt;
                                </Button>
                        </th>
                        <th colSpan="2">
                            Koledar za {month[this.state.monthSelected]}
                        </th>
                        <th colSpan="1">
                            <Button 
                            className="CalendarTitleMargin" 
                            variant="danger"
                            onClick={e => this.nextMonth(e)}>
                                &gt;
                            </Button>
                        </th>
                        <th colSpan="1"></th>
                    </tr>

                    <tr className = "ScheduleRow Center">
                        <th>Pon</th>
                        <th>Tor</th>
                        <th>Sre</th>
                        <th>Čet</th>
                        <th>Pet</th>
                        <th>Sob</th>
                        <th>Ned</th>
                    </tr>

                    </thead>
                    <tbody>

                    {
                        weeks.map(week => (
                            <tr key={week} className="CalendarRow Center">
                                {
                                    days.map((day, index) => {
                                        if(lastDayDate - currentDayDate >= 0) {
                                            if(currentDayDate.toString().split(" ")[0] === day) {
                                                currentDayDate = new Date(thisYear, thisMonth, dayIterator+1);
                                                let eventName = "";
                                                let divs = [];
                                                
                                                for(let i = 0; i < this.state.events.length; i++) {
                                                    const event = this.state.events[i];

                                                    if(event.time.split("T")[0] === currentDayDate.toISOString().split("T")[0]) {
                                                        divs.push(
                                                            <Button style={{margin: "2px"}} variant="outline-danger" key={event.id} onClick={e => this.updateEvent(e, event)}>{event.name}</Button>
                                                        );
                                                    }
                                                }

                                                return (
                                                    <td key={day+" "+index}>
                                                        <div className="CalendarDayNumber">{dayIterator++}</div>
                                                        {divs}
                                                    </td>
                                                )
                                            } else {
                                                return(
                                                    <td key={day+" "+index}></td>
                                                )
                                            }
                                        } else {
                                            return(
                                                <td key={day+" "+index}></td>
                                            )
                                        }
                                    })
                                }
                            </tr>
                        ))
                    }

                    </tbody>
            </Table>
        </React.Fragment>
        );
    }
}
export default Calendar;

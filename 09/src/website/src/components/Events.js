import React, { Component } from 'react';

class Events extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.updateNav();
  }

  render() {
    return (
      <div className="Large">
        React component: Events
      </div>
    );
  }
}
export default Events;

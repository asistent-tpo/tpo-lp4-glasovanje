import React, { Component } from 'react';
import RestaurantMap from './RestaurantMap';
import { Card } from "react-bootstrap";

class Food extends Component {
  constructor(props) {
    super(props);

    this.state = {
      restaurants: [],
      currentLocation: {
        lat: 1,
        lng: 1
      },
      show: true
    }
  }

  componentDidMount() {
    this.props.updateNav();
    navigator.geolocation.getCurrentPosition((location) => {
      this.setState({
        currentLocation: {
          lat: location.coords.latitude,
          lng: location.coords.longitude
        }
      });
    });
    fetch('https://sk9-straightas.herokuapp.com/restaurant-list/', {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        this.setState({
            restaurants: data.restaurantList
        });
    });
  }

  render() {
    return (
      <div className="Center">
        <div style={foodStyle}>Ker ni mogoče dobiti JavaScript API Key-a brezplačno smo se morali 
          zadovoljiti z developer Mapsi. Če ti prikaže morje pojdi na zavihek Trola in 
          spet nazaj na Hrana. To ponavljaj dokler ti v ozadju ne prikaže Slovenije.
          Ko v ozdaju končno dobiš Slovenijo pritiskaj na gumb OK dokler ne izgine.
          Čestitke! Sedaj si prišel do željenih restavracij.</div>
          <RestaurantMap 
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
            restaurants={this.state.restaurants}
            currentLocation={this.state.currentLocation}
          />
      </div>
    );
  }
}

const foodStyle = {
  color: "red",
  marginBottom: 15,
  marginTop: 10,
  marginRight: 10,
  marginLeft: 10,
}

export default Food;

import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import DatePicker from 'react-datepicker';
import ModalBox from './ModalBox';
import "react-datepicker/dist/react-datepicker.css";

class CalendarAddBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            ime: React.createRef(),
            cas: "",
            trajanje: React.createRef(),
            opis: React.createRef(),

            modal: {
				show: false,
				title: "",
				body: ""
			}
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
        });
    }

    closeModal() {
        this.setState({
            show: false
        });
        this.props.closeModalAdd();
    }

    closeAlertModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

    addEvent() {
        const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendAddPost();	
    }

    validateInput() {
    	if(this.state.ime.current.value === "") {
      		return "Ime Dogodka";
    	}

    	if(this.state.cas === "") {
      		return "Čas Dogodka";
        }

        if(this.state.trajanje.current.value === "") {
            return "Trajanje Dogodka";
        }

        if(this.state.opis.current.value === "") {
            return "Opis Dogodka";
        }

        if(isNaN(this.state.trajanje.current.value)) {
            return "Trajanje Dogodka";
        }

    	return "Ok";
    }
    
    sendAddPost() {
        fetch('https://sk9-straightas.herokuapp.com/event/', {
      		method: 'PUT',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
				'name': this.state.ime.current.value,
                'time': this.state.cas,
                'durationMinutes': this.state.trajanje.current.value,
                'description': this.state.opis.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndAddEvent(data.eventId);
            this.closeModal();
    	});
    }

    handleChange(date) {
        this.setState({
            cas: date
        });
    }

    render() {
        return (
            <div>
                <ModalBox values={this.state.modal} closeModal={this.closeAlertModal.bind(this)}/>
                <Modal dialogClassName="SceduleNewModal" show={this.state.show} centered={true} onHide={e => this.closeModal(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Dodaj Dogodek</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control 
                            type="text" 
                            placeholder="Ime Dogodka"
                            ref={this.state.ime} />
                        </Form.Group>
                        <DatePicker
                            className= "CalendarDateBox"
                            selected={this.state.cas}
                            onChange={this.handleChange.bind(this)}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                            placeholderText="Izberi datum" 
                        />

                        <Form.Group>
                            <Form.Control 
                            type="text" 
                            placeholder="Trajanje Dogodka v Minutah (60)"
                            ref={this.state.trajanje} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                            as="textarea" 
                            rows="4" 
                            placeholder="Opis Dogodka"
                            ref={this.state.opis} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.addEvent(e)}>
                            Dodaj
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default CalendarAddBox;
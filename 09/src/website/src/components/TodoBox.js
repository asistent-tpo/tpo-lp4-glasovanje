import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";

class TodoBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            vsebina: React.createRef()
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
        });
    }
    
    addTodo() {
        fetch('https://sk9-straightas.herokuapp.com/todo/', {
      		method: 'PUT',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
				'description': this.state.vsebina.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }
            this.setState({
                show: false
            });
            this.props.addCreatedTodo(data.todoElementId);
    	});
    }

    closeModal() {
        this.setState({
            show: false
        });
    }

    render() {
        return (
            <div>
                <Modal show={this.state.show} centered={true} onHide={e => this.closeModal(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Dodaj TODO</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control 
                            as="textarea" 
                            rows="4" 
                            placeholder="Vsebina" 
                            ref={this.state.vsebina} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.addTodo(e)}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default TodoBox;
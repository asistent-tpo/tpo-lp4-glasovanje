import React, { Component } from 'react';
import { Row, Col } from "react-bootstrap";
import TodoList from './TodoList';
import Schedule from './Schedule';
import Calendar from './Calendar';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false,
      Bus: React.createRef(),
      todos: []
    };
  }

  componentDidMount() {
    this.props.updateNav();
    this.checkAdminMessage();
  }

  checkAdminMessage() {
    /*fetch('https://sk9-straightas.herokuapp.com/user/alert/', {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+localStorage.getItem("tokenId")
      }
    })
    .then(response => response.json())
    .then(data => {
        if(data.error === "failed to decode JWT") {
            this.props.history.push("/Login");
            return;
        }
        console.log(data);
    });*/
  }

  render() {
    return (
      <div>

        <Row>
          <Col>
            <Calendar history={this.props.history}/>
            <Schedule history={this.props.history}/>
          </Col>
          <Col>
            <TodoList history={this.props.history}/>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Home;

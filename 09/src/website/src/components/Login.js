import React, { Component } from 'react';
import { Button, Form, Col, InputGroup, Card } from "react-bootstrap";
import ModalBox from './ModalBox';

class Login extends Component {
	constructor(props) {
    	super(props);

    	this.state = {
 	     	success: false,
    	  	Username: React.createRef(),
      		Password: React.createRef(),
		
    		modal: {
        		show: false,
        		title: "",
        		body: ""
      		}
		};

		if(this.props.location.pathname === "/account/verify") {
			if(this.props.location.search.includes("?code=")) {
				let token = this.props.location.search.split("=")[1];
				token = token.split("#")[0];
				this.sendConfirmationPost(token);
			}
		}
	}

	componentDidMount() {
		this.props.updateNav();
	}

	sendConfirmationPost(token) {
    	fetch('https://sk9-straightas.herokuapp.com/user/confirm-email/', {
      		method: 'POST',
      		headers: {
        		'Accept': 'application/json',
        		'Content-Type': 'application/json',
      		},
      		body: JSON.stringify({ 
        		'emailVerificationCode': token
      		})
    	})
    	.then(response => response.json())
    	.then(data => {
      		console.log(data);
      		if(!data.success) {
        		this.setState({
					modal: {
						show: true,
						title: "Napaka pri potrditvi registracije!",
						body: "Potrditvena koda ni veljavna."
					}
				});
      		} else {
				this.setState({
					modal: {
						show: true,
						title: "Registracija uspešna!",
						body: "Sedaj se lahko v aplikacijo prijavite."
					}
				});
			}
    	});
  	}

	handleSubmit() {
    	const validation = this.validateInput();
    	if(!(validation === "Ok")) {
        	this.setState({
						modal: {
							show: true,
							title: "Prišlo je do napake!",
							body: "Polje "+validation+" vsebuje napačno vrednost!"
						}
					});

      		return;
    	}

	    this.sendLoginPost();
	}

	sendLoginPost() {
    	fetch('https://sk9-straightas.herokuapp.com/auth/', {
      		method: 'PUT',
      		headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				'username': this.state.Username.current.value,
				'password': this.state.Password.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
				if(!data.success) {
					this.setState({
						modal: {
							show: true,
							title: "Napaka pri prijavi!",
							body: "Napačno uporabniško ime ali geslo."
						}
					});
				} else {
					localStorage.setItem("tokenId", data.token);
					this.props.history.push("/Bus");
				}
    	});
	}

	validateInput() {
    	if(this.state.Username.current.value === "") {
      		return "Uporabniško Ime";
    	}

    	if(this.state.Password.current.value === "") {
 	     	return "Geslo";
    	}

    	return "Ok";
	  }
	  
	  closeModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

	render() {
		return (
			<div>
				<ModalBox values={this.state.modal} closeModal={this.closeModal.bind(this)}/>

				<Card body className="FormContainer">
						<div className = "LargeMain Center"> Prijava </div>
						<Form.Group as={Col} md="12">
							<InputGroup>
								<InputGroup.Prepend>
									<InputGroup.Text id="inputGroupPrepend" className="EmoteSmall">
										<span role="img" aria-label="username">👨</span>
									</InputGroup.Text>
								</InputGroup.Prepend>
								<Form.Control
									type="text"
									placeholder="Uporabniško ime"
									ref={this.state.Username}
								/>
							</InputGroup>
						</Form.Group>
						<Form.Group as={Col} md="12">
							<InputGroup>
								<InputGroup.Prepend>
									<InputGroup.Text id="inputGroupPrepend" className="EmoteSmall">
									<span role="img" aria-label="key">🔑</span>
									</InputGroup.Text>
								</InputGroup.Prepend>
								<Form.Control
									placeholder="Geslo"
									type="password"
									ref={this.state.Password}
								/>
							</InputGroup>
						</Form.Group>
						<div className="Center">
							<Button variant="danger" type="button" onClick={e => this.handleSubmit(e)}>
								Potrdi
							</Button>
						</div>
				</Card>;
			</div>
		);
  	}
}

export default Login;

import React, { Component } from 'react';
import { Button, Modal } from "react-bootstrap";

class ModalBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            title: "",
            body: ""
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
            title: newProps.values.title,
            body: newProps.values.body
        });
    }
    
    handleClose() {
        this.setState({
            show: false
        });
        this.props.closeModal();
    }

    render() {
        return (
            <div>
                <Modal show={this.state.show} onHide={e => this.handleClose(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.body}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.handleClose(e)}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default ModalBox;
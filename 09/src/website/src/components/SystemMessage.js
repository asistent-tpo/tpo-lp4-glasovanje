import React, { Component } from 'react';
import { Button, Form, Col, InputGroup, Card } from "react-bootstrap";
import ModalBox from './ModalBox';

class SystemMessage extends Component {
	constructor(props) {
    	super(props);

    	this.state = {
			vsebina: React.createRef(),
			modal: {
				show: false,
				title: "",
				body: ""
			}
		};
    }

	componentDidMount() {
		this.props.updateNav();
	}

    addMessage() {
		const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendAddPost();	
	}

	sendAddPost() {
        fetch('https://sk9-straightas.herokuapp.com/alert/', {
      		method: 'PUT',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
                'message': this.state.vsebina.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }
    	});
    }

    validateInput() {
    	if(this.state.vsebina.current.value === "") {
      		return "Vsebina";
    	}

    	return "Ok";
	}
	
	closeAlertModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

	render() {
		return (
			<div>
				<ModalBox values={this.state.modal} closeModal={this.closeAlertModal.bind(this)}/>
				<Card body className="SystemMessageContainer">
						<div className = "LargeMain Center"> Sistemsko sporočilo </div>
                        <Form.Group>
                            <Form.Control 
                            as="textarea" 
                            rows="12" 
                            placeholder="Vsebina" 
                            ref={this.state.vsebina} />
                        </Form.Group>
						<div className="Center">
							<Button variant="danger" type="button" onClick={e => this.addMessage(e)}>
								Pošlji sporočilo
							</Button>
						</div>
				</Card>;
			</div>
		);
  	}
}

export default SystemMessage;

import React, { Component } from 'react';
import { Table, Button } from "react-bootstrap";
import ScheduleAddBox from './ScheduleAddBox';
import ScheduleUpdateBox from './ScheduleUpdateBox';

const hours = [
  "7:00",
  "8:00",
  "9:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00"
]

const days = [
  "Pon",
  "Tor",
  "Sre",
  "Čet",
  "Pet"
]

class Schedule extends Component {
  constructor(props) {
    super(props);

    this.state = {
      schedule: [],
      modalAdd: {
        show: false
      },
      modalUpdate: {
        show: false,
        item: {}
      }
    }
  }

  componentDidMount() {
    this.getSchedule();
  }

  getSchedule() {
    fetch('https://sk9-straightas.herokuapp.com/schedule/', {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+localStorage.getItem("tokenId")
      }
    })
    .then(response => response.json())
    .then(data => {
        if(data.error === "failed to decode JWT") {
            this.props.history.push("/Login");
            return;
        }
        this.setState({
          schedule: data.scheduleList
        });
    });
  }

  addClass() {
    this.setState({
      modalAdd: {
        show: true
      }
    });
  }

  closeModalAdd() {
    this.setState({
      modalAdd: {
        show: false
      }
    });
  }

  closeModalUpdate() {
    this.setState({
      modalUpdate: {
        show: false,
        item: {}
      }
    });
  }

  getAndAddScheduleItem(id) {
    fetch('https://sk9-straightas.herokuapp.com/schedule/'+id, {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+localStorage.getItem("tokenId")
      }
    })
    .then(response => response.json())
    .then(scheduleItem => {
        if(scheduleItem.error === "failed to decode JWT") {
          this.props.history.push("/Login");
          return;
        }

        let currentSchedule = this.state.schedule;
        currentSchedule.push(scheduleItem);

        console.log(currentSchedule);
        this.setState({
          schedule: currentSchedule
        });
    });
  }

  updateItem(e, item) {
    this.setState({
      modalUpdate: {
        show: true,
        item: item
      }
    });
  }

  getAndUpdateScheduleItem(id) {
    let currentSchedule = this.state.schedule.filter(item => {
      return item.id !== id;
    });
    this.setState({
      schedule: currentSchedule
    }, () => {this.getAndAddScheduleItem(id)});
  }

  getAndDeleteScheduleItem(id) {
    let currentSchedule = this.state.schedule.filter(item => {
      return item.id !== id;
    });
    this.setState({
      schedule: currentSchedule
    });
  }

  render() {
    return (
      <React.Fragment>
        <ScheduleAddBox 
          values={this.state.modalAdd} 
          closeModalAdd={this.closeModalAdd.bind(this)}
          getAndAddScheduleItem={this.getAndAddScheduleItem.bind(this)} />

        <ScheduleUpdateBox 
          values={this.state.modalUpdate} 
          closeModalUpdate={this.closeModalUpdate.bind(this)} 
          getAndUpdateScheduleItem={this.getAndUpdateScheduleItem.bind(this)} 
          getAndDeleteScheduleItem={this.getAndDeleteScheduleItem.bind(this)} />

        <Table striped bordered className="ScheduleContainer">
          <thead>

            <tr className = "ScheduleDay Center">
              <th>
                <Button variant="danger" onClick={e => this.addClass(e)}>Dodaj</Button>
              </th>
              <th>Pon</th>
              <th>Tor</th>
              <th>Sre</th>
              <th>Čet</th>
              <th>Pet</th>
            </tr>

          </thead>
          <tbody>

            {
              hours.map(hour => (
                <tr key={hour} className="ScheduleRow ScheduleHour Center">
                  <td>{hour}</td>
                  {
                    days.map(day => {
                      let tdValue = "";
                      for(let i = 0; i < this.state.schedule.length; i++) {
                        const item = this.state.schedule[i];
                        if(item.hours == hour.split(":")[0] && item.day == day) {
                          return <td style={{color: item.color}} fontkey={hour+" "+day} onClick={e => this.updateItem(e, item)}>{item.name}</td>
                        }
                      }
                      return <td key={hour+" "+day}></td>
                    })
                  }
                </tr>
              ))
            }

          </tbody>
        </Table>
      </React.Fragment>
    );
  }
}
export default Schedule;

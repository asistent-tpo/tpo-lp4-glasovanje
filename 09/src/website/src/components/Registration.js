import React, { Component } from 'react';
import { Button, Form, Col, InputGroup, Card } from "react-bootstrap";
import ModalBox from './ModalBox'

class Registration extends Component {
	constructor(props) {
    	super(props);

    	this.state = {
      		success: false,
      		Name: React.createRef(),
			Surname: React.createRef(),
			Username: React.createRef(),
			Email: React.createRef(),
			PasswordOne: React.createRef(),
			PasswordTwo: React.createRef(),
			
			modal: {
				show: false,
				title: "",
				body: ""
			}
		};
	}   

	handleSubmit() {
		const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendRegistrationPost();	
	}

	sendRegistrationPost() {
		fetch('https://sk9-straightas.herokuapp.com/user/register-student/', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ 
				'username': this.state.Username.current.value,
				'email': this.state.Email.current.value,
				'firstName': this.state.Name.current.value,
				'lastName': this.state.Surname.current.value,
				'password': this.state.PasswordOne.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(!data.success) {
				data.errors.forEach(element => {
					console.log(element);
					if(element.field === "username") {
						this.setState({
							modal: {
								show: true,
								title: "Napaka pri registraciji!",
								body: "Izbrano uporabniško ime je že zasedeno."
							}
						});
					} else if(element.field === "email") {
						this.setState({
							modal: {
								show: true,
								title: "Napaka pri registraciji!",
								body: "Izbran email naslov je že zaseden."
							}
						});
					}
				});
			} else {
				this.setState({
					modal: {
						show: true,
						title: "Registracija uspešna!",
						body: "Na vaš email naslov smo poslali aktivacijsko povezavo."
					}
				});
			}
		});
	}

	validateInput() {
    	if(this.state.Name.current.value === "") {
      		return "Ime";
    	}

	    if(this.state.Surname.current.value === "") {
      		return "Priimek";
    	}

    	if(this.state.Username.current.value === "") {
      		return "Uporabniško ime";
    	}

    	if(this.state.Email.current.value === "") {
      		return "Email";
    	}

    	if(this.state.PasswordOne.current.value === "") {
      		return "Geslo";
    	}

    	if(!(this.state.PasswordOne.current.value === this.state.PasswordTwo.current.value)) {
      		return "Geslo";
    	}

    	return "Ok";
	  }
	  
	  componentDidMount() {
		this.props.updateNav();
	  }

	  closeModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

	render() {
		return (
			<div>			
			<ModalBox values={this.state.modal} closeModal={this.closeModal.bind(this)}/>
				<Card body className="FormContainer">                    
					<div className = "LargeMain Center"> Registracija </div>
					<Form.Group as={Col} md="12" controlId="formPlaintextName">
						<InputGroup>                    
							<InputGroup.Prepend>
								<InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
									<span role="img" aria-label="name">👨‍🎓</span>
								</InputGroup.Text>
							</InputGroup.Prepend>
							<Form.Control
								type="text"
								placeholder="Ime"
								ref={this.state.Name}
							/>
						</InputGroup>
					</Form.Group>
					<Form.Group as={Col} md="12" controlId="formPlaintextSurname">
						<InputGroup>
							<InputGroup.Prepend>
								<InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
									<span role="img" aria-label="surname">👨‍🎓</span>
								</InputGroup.Text>
							</InputGroup.Prepend>

							<Form.Control
								type="text"
								placeholder="Priimek"
								ref={this.state.Surname}
							/>
						</InputGroup>
					</Form.Group>
					<Form.Group as={Col} md="12" controlId="formPlaintextUsername">
						<InputGroup>
							<InputGroup.Prepend>
								<InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
									<span role="img" aria-label="username">👨</span>
								</InputGroup.Text>
							</InputGroup.Prepend>
							<Form.Control
								type="text"
								placeholder="Uporabniško ime"
								ref={this.state.Username}
							/>
						</InputGroup>
					</Form.Group>
					<Form.Group as={Col} md="12" controlId="formPlaintextEmail">
						<InputGroup>                    
							<InputGroup.Prepend>
								<InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
									<span role="img" aria-label="mail">📧</span>
								</InputGroup.Text>
							</InputGroup.Prepend>
							<Form.Control
								type="text"
								placeholder="Email"
								ref={this.state.Email}
							/>
						</InputGroup>
					</Form.Group>
					<Form.Group as={Col} md="12" controlId="formPlaintextPasswordOne">
						<InputGroup>                    
							<InputGroup.Prepend>
								<InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
									<span role="img" aria-label="key">🔑</span>
								</InputGroup.Text>
							</InputGroup.Prepend>
							<Form.Control
								placeholder="Geslo"
								type="password"
								ref={this.state.PasswordOne}
							/>
						</InputGroup>
					</Form.Group>
					<Form.Group as={Col} md="12" controlId="formPlaintextPasswordTwo">
						<InputGroup>                    
							<InputGroup.Prepend>
								<InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
									<span role="img" aria-label="key">🔑</span>
								</InputGroup.Text>
							</InputGroup.Prepend>
							<Form.Control
								placeholder="Ponovi geslo"
								type="password"
								ref={this.state.PasswordTwo}
							/>
						</InputGroup>
					</Form.Group>
					<div className="Center RegisterButton">
						<Button variant="danger" type="button" onClick={e => this.handleSubmit(e)}>
							Registriraj
						</Button>
					</div>
				</Card>
			</div>
		);
	}
}

export default Registration;

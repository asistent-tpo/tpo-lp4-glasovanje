import React, { Component } from 'react';
import { Button, Form, Col, InputGroup, Card } from "react-bootstrap";
import ModalBox from './ModalBox';

class PasswordResetNew extends Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false,
      Password: React.createRef(),
      PasswordRepeat: React.createRef(),
      passwordResetCode: "",

      modal: {
        show: false,
        title: "",
        body: ""
      }
    };
  }   

  componentDidMount() {
    this.props.updateNav();
    if(this.props.location.pathname === "/account/reset-password") {
        if(this.props.location.search.includes("?code=")) {
            let token = this.props.location.search.split("=")[1];
            token = token.split("#")[0];
            this.setState({
                passwordResetCode: token
            });
        }
    }
  }

  confirmReset() {
    const validation = this.validateInput();
    if(!(validation === "Ok")) {
      this.setState({
        modal: {
          show: true,
          title: "Prišlo je do napake!",
          body: "Polje "+validation+" vsebuje napačno vrednost!"
        }
      });

      return;
    }

    this.makeChangePost();
  }

    makeChangePost() {
        fetch('https://sk9-straightas.herokuapp.com/user/reset-password/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'passwordResetCode': this.state.passwordResetCode,
                'newPassword': this.state.Password.current.value
            })
        })
        .then(response => response.json())
        .then(data => {
            if(!data.success) {
                this.setState({
                    modal: {
                        show: true,
                        title: "Napaka!",
                        body: data.errors[0].msg
                    }
                });
            } else {
                this.setState({
                    modal: {
                        show: true,
                        title: "Uspeh!",
                        body: "Geslo uspešno posodobljeno. Preusmeritev na prijavo..."
                    }
                });

                setTimeout(function () {
                    this.props.history.push('/Login');
                }.bind(this), 3000);
            }
        });
    }

  validateInput() {
    if(this.state.Password.current.value === "") {
      return "Novo Geslo";
    }
    if(this.state.PasswordRepeat.current.value === "") {
        return "Ponovi Geslo";
    }
    if(this.state.Password.current.value !== this.state.PasswordRepeat.current.value) {
        return "Ponovi Geslo";
    }
    return "Ok";
  }

  closeModal() {
    this.setState({
        modal: {
            show: false
        }
    });
}

  render() {
    return (
        <div>
            <ModalBox values={this.state.modal} closeModal={this.closeModal.bind(this)}/>
            <Card body className="FormContainer">

                    <div className = "LargeMain Center"> Novo geslo </div>
                    <div className="BottomSpacing-sm">
                        Vnesi novo geslo, s katerim želiš dostopati do svojega računa.
                    </div>

                    <Form.Group as={Col} md="12" controlId="formPlaintextPassword">
                        <InputGroup>
                        
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
                                    <span role="img" aria-label="key">🔑</span>
                                </InputGroup.Text>
                            </InputGroup.Prepend>

                            <Form.Control
                                type="password"
                                placeholder="Novo Geslo"
                                ref={this.state.Password}
                            />
                        </InputGroup>
                    </Form.Group>

                    <Form.Group as={Col} md="12" controlId="formPlaintextPasswordRepeat">
                        <InputGroup>
                        
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
                                    <span role="img" aria-label="key">🔑</span>
                                </InputGroup.Text>
                            </InputGroup.Prepend>

                            <Form.Control
                                type="password"
                                placeholder="Ponovi Geslo"
                                ref={this.state.PasswordRepeat}
                            />
                        </InputGroup>
                    </Form.Group>

                    <div className="Center">
                        <Button variant="danger" type="button" onClick={e => this.confirmReset(e)}>
                            Potrdi spremembe
                        </Button>
                    </div>
            </Card>;
        </div>
    );
  }
}

export default PasswordResetNew;
import React, { Component } from 'react';
import { Table, Button } from "react-bootstrap";
import EventManagerAddBox from "./EventManagerAddBox";
import EventManagerUpdateBox from "./EventManagerUpdateBox";

class EventManager extends Component {
  state = {
    modalAdd: {
      show: false
    },
    modalUpdate: {
      show: false,
      item: {}
    },
    events: []
  }

  componentDidMount() {
    this.props.updateNav();
    this.getEvents();
  }

  addEvent() {
    console.log("New click");
    this.setState({
        modalAdd: {
          show: true
      }
    });
  }

  getEvents() {
    fetch('https://sk9-straightas.herokuapp.com/event/', {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+localStorage.getItem("tokenId")
      }
    })
    .then(response => response.json())
    .then(data => {
        if(data.error === "failed to decode JWT") {
            this.props.history.push("/Login");
            return;
        }
        console.log(data.eventList);

        this.setState({
          events: data.eventList
        });
    });
  }

  getAndAddEvent(id) {
    fetch('https://sk9-straightas.herokuapp.com/event/'+id+"/", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer "+localStorage.getItem("tokenId")
        }
    })
    .then(response => response.json())
    .then(event => {
        if(event.error === "failed to decode JWT") {
            this.props.history.push("/Login");
            return;
        }

        let currentCalendar = this.state.events;
        currentCalendar.push(event);

        this.setState({
            events: currentCalendar
        });
    });
  }

  updateEvent(e, event) {
    //console.log(id);
    this.setState({
        modalUpdate: {
          show: true,
          item: event
        }
    });
  }

  deleteEvent(e, event) {

  }

  closeModalAdd() {
    this.setState({
      modalAdd: {
        show: false
      }
    });
  }

  closeModalUpdate() {
    this.setState({
      modalUpdate: {
        show: false,
        item: {}
      }
    });
  }

  getAndUpdateEvent(id) {
    let currentEvents = this.state.events.filter(item => {
        return item.id !== id;
    });
    this.setState({
        events: currentEvents
    }, () => {this.getAndAddEvent(id)});
  }

getAndDeleteEvent(id) {
    let currentEvents = this.state.events.filter(item => {
        return item.id !== id;
    });
    this.setState({
        events: currentEvents
    });
  }

  render() {
    return (
      <React.Fragment>
        <EventManagerAddBox 
          values={this.state.modalAdd}
          getAndAddEvent={this.getAndAddEvent.bind(this)}
          closeModalAdd={this.closeModalAdd.bind(this)} />

        <EventManagerUpdateBox 
          values={this.state.modalUpdate} 
          closeModalUpdate={this.closeModalUpdate.bind(this)} 
          getAndUpdateEvent={this.getAndUpdateEvent.bind(this)} 
          getAndDeleteEvent={this.getAndDeleteEvent.bind(this)} />

            <Table striped bordered className="EventManagerContainer">
                <thead>
                <tr className="ScheduleRow SmallMain Center">
                    <th colSpan="4">
                        Upravitelj dogodkov 
                    </th>
                </tr>

                <tr className = "ScheduleRow Center">
                    <th>Ime</th>
                    <th>Datum</th>
                    <th>Opis</th>
                    <th>Posodobi/Odstrani</th>
                </tr>

                </thead>
                <tbody>

                  {
                    this.state.events.map(event => (
                      <tr key={event.id} className="EventManagerRow Center">
                        <td>
                            <div>{event.name}</div>
                        </td>
                        <td>
                            <div>{event.time}</div>
                        </td>
                        <td>
                            <div>{event.description}</div>
                        </td>
                        <td>
                          <Button variant="danger" onClick={e => this.updateEvent(e, event)}>Posodobi/Odstrani</Button>
                        </td>
                      </tr>
                    ))
                  }

                <tr className="ScheduleRow SmallMain Center">
                    <th colSpan="4">
                        <Button variant="danger" onClick={e => this.addEvent(e)}>Dodaj dogodek</Button>
                    </th>
                </tr>

                </tbody>
                
        </Table>
      </React.Fragment>
    );
  }
}
export default EventManager;

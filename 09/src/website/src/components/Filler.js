import React, { Component } from 'react';

class Filler extends Component {
  
  componentDidMount() {
    this.props.updateNav();
  }

  render() {
    return (
      <div className="Large Center">
        React component: Filler
      </div>
    );
  }
}
export default Filler;

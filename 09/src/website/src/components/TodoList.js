import React, { Component } from 'react'
import { Button, Card, CardColumns } from "react-bootstrap";
import TodoBox from './TodoBox';

export class TodoList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            update: true,
            todos: [],
            todo: {
                show: false
            }
        }
    }

    componentDidMount() {
        this.getTodoList();
    }

    getTodoList() {
        fetch('https://sk9-straightas.herokuapp.com/todo/', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
        })
        .then(response => response.json())
        .then(data => {
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }
            //console.log(data);
            this.setState({
                todos: data.todoList
            });
        });
    }

    getAndAddTodo(todoId) {
        fetch('https://sk9-straightas.herokuapp.com/todo/'+todoId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
        })
        .then(response => response.json())
        .then(todo => {
            if(todo.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }
            this.setState({ 
                todos: this.state.todos.push(todo)
            });
        });
    }

    deleteTodo(e, todoId) {
        console.log(todoId);
        fetch('https://sk9-straightas.herokuapp.com/todo/'+todoId, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
        })
        .then(response => response.json())
        .then(data => {
            //console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }
            this.setState({
                todos: this.state.todos.filter(todo => {
                    return todo.id !== todoId;
                })
            });
        });
    }

    addTodo() {
        this.setState({
            todo: {
                show: true,
            }
        });
    }

    addCreatedTodo(todoId) {
        this.setState({
            todo: {
                show: false,
            }
        });
        this.getTodoList();
    }

    render() {
        return (
            <React.Fragment>
                <TodoBox 
                    values={this.state.todo} 
                    addCreatedTodo={this.addCreatedTodo.bind(this)}
                    history={this.props.history} />
                    <Card className="TodoContainer">
                        <Card.Header className = "SmallMain TodoMainTitlePadding"> 
                            Seznam TODO
                            <Button className="TodoContainerButton" variant="danger" onClick={e => this.addTodo(e)}>Dodaj</Button>
                        </Card.Header>
                        <CardColumns className="TodoCardCollumn">
                        {
                            this.state.todos.map((todo) => (
                                <Card border="danger" bg="white" key={todo.id}>
                                    <Card.Header className = "SmallMain Center Border-none"> TODO
                                        <Button className="TodoContainerButton" variant="danger" onClick={e => this.deleteTodo(e, todo.id)}>Opravil</Button> 
                                    </Card.Header>
                                    <Card.Body className="Pad-sm">
                                        <Card.Text>{todo.description}</Card.Text>
                                    </Card.Body>
                                </Card>
                            ))
                        }
                        </CardColumns>
                    </Card>
            </React.Fragment>
        )
    }
}

export default TodoList;

/* eslint-disable no-undef */
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import { MarkerWithLabel } from 'react-google-maps/lib/components/addons/MarkerWithLabel';
import React from 'react';

const RestaurantMap = withScriptjs(withGoogleMap((props) => (
    <GoogleMap
        defaultZoom={8}
        defaultCenter={{ lat: props.currentLocation.lat, lng: props.currentLocation.lng }}>

        <MarkerWithLabel
            position={{ lat: props.currentLocation.lat, lng: props.currentLocation.lng }} 
            labelAnchor={new google.maps.Point(46, 55)}
            labelStyle={{backgroundColor: "white", fontSize: "12px", padding: "3px", borderRadius: "3px"}}>

            <div>Current Location</div>
        </MarkerWithLabel>

        {props.restaurants.map(restaurant => (
            <MarkerWithLabel 
            position={{ lat: restaurant.latitude, lng: restaurant.longitude }} 
            labelAnchor={new google.maps.Point(46, 55)}
            labelStyle={{backgroundColor: "white", fontSize: "12px", padding: "3px", borderRadius: "3px"}}>
                <div>{restaurant.name}</div>
            </MarkerWithLabel>
        ))}
    </GoogleMap>))
);

export default RestaurantMap;
import React, { Component } from 'react';
import { Card, CardColumns } from "react-bootstrap";

class EventViewer extends Component {
  state = {
      events: []
  }

  componentDidMount() {
    this.props.updateNav();
    this.getEvents();
  }

  getEvents() {
    fetch('https://sk9-straightas.herokuapp.com/event/event-manager/', {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+localStorage.getItem("tokenId")
      }
    })
    .then(response => response.json())
    .then(data => {
        if(data.error === "failed to decode JWT") {
            this.props.history.push("/Login");
            return;
        }
        console.log(data.eventList);

        this.setState({
          events: data.eventList
        });
    });
  }

  render() {
    return (
      <React.Fragment>
            <Card className="EventViewerContainer">
                <Card.Header className = "MediumMain TodoMainTitlePadding Center"> 
                    Dogodki
                </Card.Header>
                <CardColumns className="TodoCardCollumn">

                    {
                        this.state.events.map(event => (
                            <Card border="danger" bg="white">
                                <Card.Header className = "SmallMain Center Border-none"> 
                                    {event.name} | {event.time.split("T")[0]} | {event.time.split("T")[1].split("Z")[0]}
                                </Card.Header>
                                <Card.Body className="Pad-sm">
                                    <Card.Text>
                                        {event.description}
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        ))
                    }

                </CardColumns>
            </Card>
      </React.Fragment>
    );
  }
}
export default EventViewer;

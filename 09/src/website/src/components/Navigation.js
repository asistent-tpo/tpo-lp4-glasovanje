import React, { Component } from 'react';
import {Navbar, Nav, Button} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

class Navigation extends Component {
  constructor(props) {
    super(props);

    /*
      Roles = [Unregistered, Registered, EventManager, Admin]
    */
    
    this.state = {
      Role: "Unregistered",
      redirect: false,
      user: {}
    };
  }

  setNavItems = () => {
    if(localStorage.getItem("tokenId")) {
      if(Object.entries(this.state.user).length === 0 && this.state.user.constructor === Object) {
        this.setState({
          Role: "Registered"
        }, () => this.getUserDetails());
      } else {
        if(this.state.user.accountType === "STUDENT") {
          this.setState({
            Role: "Registered"
          });
          //Registered account
          localStorage.setItem("tokenizer", "iIzYWVjMDd");
        }

        if(this.state.user.accountType === "ADMINISTRATOR") {
          this.setState({
            Role: "Admin"
          });
          //Admin account
          localStorage.setItem("tokenizer", "r4MbVLmmWi");
        }

        if(this.state.user.accountType === "EVENT-MANGER") {
          this.setState({
            Role: "EventManager"
          });
          //EventManager account
          localStorage.setItem("tokenizer", "DczMTEwZiI");
        }
      }
    } else {
      this.setState({
        Role: "Unregistered"
      });
    }
  }

  componentWillReceiveProps(newProps) {
    this.getUserDetails();
  }

  getUserDetails() {
    if(localStorage.getItem("tokenId") === null) {
      return;
    }
    if(Object.entries(this.state.user).length === 0 && this.state.user.constructor === Object && this.state.Role !== "Unregistered") {
      fetch('https://sk9-straightas.herokuapp.com/user/', {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+localStorage.getItem("tokenId")
        }
      })
      .then(response => response.json())
      .then(data => {

          this.setState({
            user: data
          }, () => this.setNavItems());
      }); 
    } else {
      this.setNavItems();
    }
  }

  odjava() {
    this.setState({
      user: {},
      Role: "Unregistered"
    });
  }

  render() {
    return (
      <div>
        {this.state.redirect && <Redirect to="/Login" />}
        <Navbar bg="danger" variant="dark">
          <Navbar.Brand>StraightAs</Navbar.Brand>

          <Nav className="Width-100">
          <Nav.Link key="Food" href="#Food">Hrana</Nav.Link>
          <Nav.Link key="Bus" href="#Bus">Trola</Nav.Link>
            {
              this.state.Role === "Unregistered" &&
              [
                <Nav.Link key="Login" href="#Login">Prijava</Nav.Link>, 
                <Nav.Link key="Register" href="#Register">Registracija</Nav.Link>,
                <Nav.Link key="PasswordReset" className="Right" href="#PasswordReset">Ponastavi geslo</Nav.Link>
              ]
            }

            {
              this.state.Role === "Registered" && 
              [
                <Nav.Link key="Home" href="#Home">Domov</Nav.Link>,
                <Nav.Link key="Events" href="#Events">Dogodki</Nav.Link>,
                <Nav.Link key="Logout" onClick={e => this.odjava(e)} href="#Logout" className="Right" variant="danger">Odjava</Nav.Link>
              ]
            }

            {
              this.state.Role === "EventManager" && 
              [
                <Nav.Link key="EventManager" href="#EventsManager">Upravljanje z dogodki</Nav.Link>,
                <Nav.Link key="Logout" onClick={e => this.odjava(e)} href="#Logout" className="Right" variant="danger">Odjava</Nav.Link>
              ]
            }

            {
              this.state.Role === "Admin" && 
              [
                <Nav.Link key="SystemMessage" href="#SystemMessage">Sporočilo Za Uporabnike</Nav.Link>,
                <Nav.Link key="Logout" onClick={e => this.odjava(e)} href="#Logout" className="Right" variant="danger">Odjava</Nav.Link>
              ]
            }
          </Nav>
        </Navbar>
      </div>
    );
  }
}
export default Navigation;

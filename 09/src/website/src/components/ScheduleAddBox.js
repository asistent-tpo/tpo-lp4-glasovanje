import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import ModalBox from './ModalBox';
import { CirclePicker } from 'react-color';

class ScheduleAddBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            ime: React.createRef(),
            dan: React.createRef(),
            zacetek: React.createRef(),
            color: "#ffffff",

            modal: {
				show: false,
				title: "",
				body: ""
			}
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
        });
    }

    closeModal() {
        this.setState({
            show: false
        });
        this.props.closeModalAdd();
    }

    closeAlertModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

    addClass() {
        const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendAddPost();	
    }

    validateInput() {
    	if(this.state.ime.current.value === "") {
      		return "Ime";
    	}

    	if(this.state.zacetek.current.value === "") {
      		return "Začetek";
        }
        
        const zacetek = this.state.zacetek.current.value;
        if(zacetek.split(":").length === 1 || zacetek.split(":").length > 2) {
            return "Začetek";
        }
        if(isNaN(zacetek.split(":")[0])) {
            return "Začetek";
        }
        if(isNaN(zacetek.split(":")[1])) {
            return "Začetek";
        }

    	return "Ok";
    }
    
    sendAddPost() {
        fetch('https://sk9-straightas.herokuapp.com/schedule/', {
      		method: 'PUT',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
				'name': this.state.ime.current.value,
                'day': this.state.dan.current.value,
                'hours': this.state.zacetek.current.value.split(":")[0],
                'minutes': this.state.zacetek.current.value.split(":")[1],
                'durationMinutes': 45,
                'color': this.state.color
			})
		})
		.then(response => response.json())
		.then(data => {
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndAddScheduleItem(data.scheduleId);
            this.closeModal();
    	});
    }

    handleChangeComplete = (color) => {
        this.setState({
            color: color.hex
        });
    }

    render() {
        return (
            <div>
                <ModalBox values={this.state.modal} closeModal={this.closeAlertModal.bind(this)}/>
                <Modal dialogClassName="SceduleNewModal" show={this.state.show} centered={true} onHide={e => this.closeModal(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Dodaj Predmet</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control 
                            type="text" 
                            placeholder="Ime Predmeta"
                            ref={this.state.ime} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                                as="select"
                                ref={this.state.dan}>
                                <option>Pon</option>
                                <option>Tor</option>
                                <option>Sre</option>
                                <option>Čet</option>
                                <option>Pet</option>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                            type="text" 
                            placeholder="Začetek (npr. 13:00)"
                            ref={this.state.zacetek}>
                            </Form.Control>
                        </Form.Group>

                        <CirclePicker 
                            color={this.state.color}
                            onChangeComplete={this.handleChangeComplete} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.addClass(e)}>
                            Dodaj
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default ScheduleAddBox;
import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import ModalBox from "./ModalBox";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker';

class EventManagerAddBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            name: React.createRef(),
            date: "",
            duration: React.createRef(),
            description: React.createRef(),

            modal: {
				show: false,
				title: "",
				body: ""
			}
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
        });
    }

    closeModal() {
        this.setState({
            show: false
        });
        this.props.closeModalAdd();
    }

    addEvent() {
        const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendAddPost();	
    }

    sendAddPost() {
        fetch('https://sk9-straightas.herokuapp.com/event/', {
      		method: 'PUT',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
				'name': this.state.name.current.value,
                'time': this.state.date,
                'durationMinutes': this.state.duration.current.value,
                'description': this.state.description.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndAddEvent(data.eventId);
            this.closeModal();
    	});
    }

    validateInput() {
    	if(this.state.name.current.value === "") {
      		return "Ime Dogodka";
    	}

        if(this.state.description.current.value === "") {
            return "Opis Dogodka";
        }

        if(this.state.duration.current.value === "") {
            return "Trajanje";
        }

        if(isNaN(this.state.duration.current.value)) {
            return "Trajanje Dogodka";
        }

    	return "Ok";
    }

    closeAlertModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

    handleChange(date) {
        this.setState({
            date: date
        });
    }

    render() {
        return (
            <div>        
                <ModalBox values={this.state.modal} closeModal={this.closeAlertModal.bind(this)}/>
                <Modal dialogClassName="SceduleNewModal" show={this.state.show} centered={true} onHide={e => this.closeModal(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Dogodek</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control 
                            type="text" 
                            placeholder="Ime"
                            ref={this.state.name} />
                        </Form.Group>

                        <DatePicker
                            selected={this.state.date}
                            onChange={this.handleChange.bind(this)}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />

                        <Form.Group>
                            <Form.Control 
                            type="text" 
                            placeholder="Trajanje v minutah (npr. 60)"
                            ref={this.state.duration} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                            as="textarea" 
                            rows="3" 
                            placeholder="Opis" 
                            ref={this.state.description} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.addEvent(e)}>
                            Dodaj dogodek
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default EventManagerAddBox;
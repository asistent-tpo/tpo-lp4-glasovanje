import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import ModalBox from './ModalBox';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker';

class CalendarUpdateBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            ime: "",
            cas: "",
            trajanje: "",
            opis: "",

            modal: {
				show: false,
				title: "",
				body: ""
			}
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            show: newProps.values.show,
            ime: newProps.values.item.name,
            //cas: new Date(newProps.values.item.time),
            trajanje: newProps.values.item.durationMinutes,
            opis: newProps.values.item.description
        });
    }

    closeModal() {
        this.setState({
            show: false
        });
        this.props.closeModalUpdate();
    }

    closeAlertModal() {
        this.setState({
            modal: {
                show: false
            }
        });
    }

    updateEvent() {
        const validation = this.validateInput();
		if(!(validation === "Ok")) {
			this.setState({
				modal: {
					show: true,
					title: "Prišlo je do napake!",
					body: "Polje "+validation+" vsebuje napačno vrednost!"
				}});
			return;
		}

		this.sendUpdatePost();	
    }

    validateInput() {
    	if(this.state.ime === "") {
      		return "Ime Dogodka";
    	}

    	if(this.state.cas === "") {
      		return "Čas Dogodka";
        }

        if(this.state.trajanje === "") {
            return "Trajanje Dogodka";
        }

        if(this.state.opis === "") {
            return "Opis Dogodka";
        }

        if(isNaN(this.state.trajanje)) {
            return "Trajanje Dogodka";
        }

    	return "Ok";
    }
    
    sendUpdatePost() {
        fetch('https://sk9-straightas.herokuapp.com/event/'+this.props.values.item.id+"/", {
      		method: 'POST',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }, 
            body: JSON.stringify({ 
				'name': this.state.ime,
                'time': this.state.cas,
                'durationMinutes': this.state.trajanje,
                'description': this.state.opis
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data);
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndUpdateEvent(this.props.values.item.id);

            this.closeModal();
    	});
    }

    handleChange = (date) => {
        this.setState({
            cas: date
        });
    }

    handleChangeOther(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    deleteEvent() {
        fetch('https://sk9-straightas.herokuapp.com/event/'+this.props.values.item.id+"/", {
      		method: 'DELETE',
      		headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer "+localStorage.getItem("tokenId")
            }
		})
		.then(response => response.json())
		.then(data => {
            if(data.error === "failed to decode JWT") {
                this.props.history.push("/Login");
                return;
            }

            this.props.getAndDeleteEvent(this.props.values.item.id);

            this.closeModal();
    	});
    }

    render() {
        return (
            <div>
                <ModalBox values={this.state.modal} closeModal={this.closeAlertModal.bind(this)}/>
                <Modal dialogClassName="SceduleNewModal" show={this.state.show} centered={true} onHide={e => this.closeModal(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Preglej/Posodobi Dogodek</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control 
                            name="ime"
                            type="text" 
                            placeholder="Ime Dogodka"
                            value={this.state.ime || ''}
                            onChange={this.handleChangeOther.bind(this)} />
                        </Form.Group>

                        <DatePicker
                            className= "CalendarDateBox"
                            selected={this.state.cas}
                            onChange={this.handleChange.bind(this)}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                            placeholderText="Izberi datum" 
                        />

                        <Form.Group>
                            <Form.Control 
                            name="trajanje"
                            type="text" 
                            placeholder="Trajanje Dogodka v Minutah (60)"
                            value={this.state.trajanje || ''}
                            onChange={this.handleChangeOther.bind(this)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control 
                            name="opis"
                            as="textarea" 
                            rows="4" 
                            placeholder="Opis Dogodka"
                            value={this.state.opis || ''}
                            onChange={this.handleChangeOther.bind(this)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={e => this.updateEvent(e)}>
                            Posodobi
                        </Button>
                        <Button variant="danger" onClick={e => this.deleteEvent(e)}>
                            Odstrani
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default CalendarUpdateBox;
import React, { Component } from 'react';
import { Button, Form, Col, InputGroup, Card } from "react-bootstrap";
import ModalBox from './ModalBox';

class PasswordReset extends Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false,
      Email: React.createRef(),

      modal: {
        show: false,
        title: "",
        body: ""
      }
    };
  }   

  sendEmail() {
    console.log(this.state.Email.current.value);
    const validation = this.validateInput();
    if(!(validation === "Ok")) {
      this.setState({
        modal: {
          show: true,
          title: "Prišlo je do napake!",
          body: "Polje "+validation+" vsebuje napačno vrednost!"
        }
      });

      return;
    }

    this.makeEmailPost();
  }

  validateInput() {
    if(this.state.Email.current.value === "") {
      return "Email";
    }
    return "Ok";
  }

  makeEmailPost() {
    fetch('https://sk9-straightas.herokuapp.com/user/reset-password/email/', {
      method: 'POST',
      headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				'email': this.state.Email.current.value
			})
		})
		.then(response => response.json())
		.then(data => {
      if(!data.success) {
        this.setState({
          modal: {
            show: true,
            title: "Napaka!",
            body: "Prišlo je do napake pri pošiljanju e-mail sporočila."
          }
        });
      } else {
        this.setState({
          modal: {
            show: true,
            title: "Uspeh!",
            body: "Na vnesen E-mail naslov smo poslali povezavo za spremebo gesla."
          }
        });
      }
    });
  }

  componentDidMount() {
    this.props.updateNav();
  }

  closeModal() {
    this.setState({
        modal: {
            show: false
        }
    });
}

  render() {
    return (
        <div>
          <ModalBox values={this.state.modal} closeModal={this.closeModal.bind(this)}/>
            <Card body className="FormContainer">

                    <div className = "LargeMain Center"> Ponastavi geslo </div>
                    <div className="BottomSpacing-sm">
                        Za ponastavitev gesla računa vnesi pripadajoči email naslov, na katerega bo poslana povezava za nadaljevanje.
                    </div>

                    <Form.Group as={Col} md="12" controlId="formPlaintextPassword">
                        <InputGroup>
                        
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroupPrepend" className="Emote-sm">
                                  <span role="img" aria-label="mail">📧</span>
                                </InputGroup.Text>
                            </InputGroup.Prepend>

                            <Form.Control
                                type="text"
                                placeholder="Email"
                                ref={this.state.Email}
                            />
                        </InputGroup>
                    </Form.Group>

                    <div className="Center">
                        <Button variant="danger" type="button" onClick={e => this.sendEmail(e)}>
                            Pošlji zahtevo
                        </Button>
                    </div>
            </Card>;
        </div>
    );
  }
}

export default PasswordReset;

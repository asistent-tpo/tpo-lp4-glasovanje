import React, { Component } from 'react';
import './App.css';

import { Route, HashRouter, Redirect } from "react-router-dom";

import Navigation from "./components/Navigation";
import Registration from "./components/Registration";
import Login from "./components/Login";
import PasswordReset from "./components/PasswordReset";
import PasswordResetNew from "./components/PasswordResetNew";
import Bus from "./components/Bus";
import Home from "./components/Home";
import Food from "./components/Food";
import EventViewer from "./components/EventViewer";
import EventManager from "./components/EventManager";
import SystemMessage from "./components/SystemMessage";
import Logout from "./components/Logout";

// import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
  state = {
    updateNav: true
  }

  //Update the state so the new props get sent to navigation(For the sake of nav items)
  updateNav = () => {
    this.setState({ updateNav: !this.state.updateNav });
  }

  protect(props, Component) {
    //Prevermo ce je prjavljen uporabnik
    if(localStorage.getItem("tokenId")) {
      if(Component === EventManager) {
        if(localStorage.getItem("tokenizer") === "DczMTEwZiI") {
          return <Component {...props} updateNav={this.updateNav} />;    
        } else {
          return <Redirect
            to={{
              pathname: "/Bus",
              state: {
                from: props.location
              }
            }}
          />
        }
      } else if(Component === SystemMessage) {
        if(localStorage.getItem("tokenizer") === "r4MbVLmmWi") {
          return <Component {...props} updateNav={this.updateNav} />;    
        } else {
          return <Redirect
            to={{
              pathname: "/Bus",
              state: {
                from: props.location
              }
            }}
          />
        }
      } else if(Component === Home || Component === EventViewer) {
        if(localStorage.getItem("tokenizer") === "iIzYWVjMDd") {
          return <Component {...props} updateNav={this.updateNav} />;    
        } else {
          return <Redirect
            to={{
              pathname: "/Bus",
              state: {
                from: props.location
              }
            }}
          />
        }
      } else {
        return <Component {...props} updateNav={this.updateNav} />; 
      }
      //Vrnemo taprav render
      
    } else {
      return <Redirect
        to={{
          pathname: "/Bus",
          state: {
            from: props.location
          }
        }}
      />
    }
  }

  render() {
    return (
      <HashRouter>
        <div className="App">
          <div className="App-content">
            <Navigation 
              updateNav={this.state.updateNav}
              />
            <Route exact path="/Food" render={props => <Food {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/Bus" render={props => <Bus {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/Login" render={props => <Login {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/Register" render={props => <Registration {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/account/verify" render={props => <Login {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/PasswordReset" render={props => <PasswordReset {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/account/reset-password" render={props => <PasswordResetNew {...props} updateNav={this.updateNav}/>}/>
            <Route exact path="/" render={props => <Bus {...props} updateNav={this.updateNav}/>}/>
            
            {/* Not availible if user not logged in */}
            <Route exact path="/Home" render={props => this.protect(props, Home)}/>
            <Route exact path="/Events" render={props => this.protect(props, EventViewer)}/>
            <Route exact path="/EventsManager" render={props => this.protect(props, EventManager)}/>
            <Route exact path="/SystemMessage" render={props => this.protect(props, SystemMessage)}/>
            <Route exact path="/Logout" render={props => this.protect(props, Logout)}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
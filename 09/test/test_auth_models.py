from django.test import TestCase


# Create your tests here.
from user_system.models import Account


class AccountTests(TestCase):

    fixtures = ['Account.yaml']

    def test_username_available(self):
        username_exists = "admin"
        username_does_not_exist = "admin1"

        self.assertFalse(Account.username_available(username_exists))
        self.assertTrue(Account.username_available(username_does_not_exist))

    def test_username_available_case(self):
        username = "ADMin"

        self.assertFalse(Account.username_available(username))

    def test_email_available(self):
        email_exists = "admin@example.com"
        email_does_not_exist = "admin.alt@example.com"

        self.assertFalse(Account.email_available(email_exists))
        self.assertTrue(Account.email_available(email_does_not_exist))

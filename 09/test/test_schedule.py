import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

# Create your tests here.
from logic.models import Schedule
from user_system.auth.jwtutil import create_token_for_user


class ScheduleTests(TestCase):

    fixtures = ['Account.yaml', 'Schedule.yaml']

    def test_schedule_create_ok(self):
        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
            "name": "TPO",
            "day": "Tue",
            "hours": 8,
            "minutes": 24,
            "durationMinutes": 30,
            "color": "#35F"
        }

        c = Client()
        resp = c.put(reverse('schedule'), data=json.dumps(data), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 201)

        schedule = Schedule.objects.last()

        json_resp = resp.json()
        self.assertEqual(json_resp['scheduleId'], schedule.get_uuid())

    def test_schedule_get_ok(self):
        schedule = Schedule.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('schedule-by-id', args=[schedule.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['id'], schedule.get_uuid())
        self.assertEqual(schedule.name, json_resp["name"])
        self.assertEqual(json_resp['day'], schedule.day)
        self.assertEqual(schedule.hours, json_resp["hours"])
        self.assertEqual(json_resp['minutes'], schedule.minutes)
        self.assertEqual(schedule.durationMinutes, json_resp["durationMinutes"])
        self.assertEqual(json_resp['color'], schedule.color)

    def test_schedule_get_not_authorized(self):
        schedule = Schedule.objects.get(pk=1)

        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('schedule-by-id', args=[schedule.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 401)

    def test_schedule_update(self):
        schedule = Schedule.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
            "name": "PUI",
            "day": "Mon",
            "hours": 2,
            "minutes": 21,
            "durationMinutes": 77,
            "color": "#FFF"
        }

        c = Client()
        resp = c.post(reverse('schedule-by-id', args=[schedule.get_uuid()]), data=json.dumps(data),
                      HTTP_AUTHORIZATION="Bearer {}".format(token), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['scheduleId'], schedule.get_uuid())
        schedule_updated = Schedule.objects.get(pk=1)
        self.assertEqual(schedule_updated.name, data['name'])
        self.assertEqual(schedule_updated.day, data['day'])
        self.assertEqual(schedule_updated.hours, data['hours'])
        self.assertEqual(schedule_updated.minutes, data['minutes'])
        self.assertEqual(schedule_updated.durationMinutes, data['durationMinutes'])
        self.assertEqual(schedule_updated.color, data['color'])

    def test_schedule_delete(self):
        schedule = Schedule.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.delete(reverse('schedule-by-id', args=[schedule.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token))
        self.assertEqual(resp.status_code, 200)
        self.assertRaises(Schedule.DoesNotExist, Schedule.objects.get, pk=1)

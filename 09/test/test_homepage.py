from rest_framework.test import APITestCase


class HomepageTestCase(APITestCase):

    def test_homepage(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Hello World")

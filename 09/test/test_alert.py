import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

# Create your tests here.
from logic.models import Alert
from user_system.auth.jwtutil import create_token_for_user


class AlertTests(TestCase):

    fixtures = ['Account.yaml', 'Alert.yaml']

    def test_alert_create_ok(self):
        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        data = {
            "message": "TPO odpade"
        }

        c = Client()
        resp = c.put(reverse('alert'), data=json.dumps(data), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 201)

        alert = Alert.objects.last()

        json_resp = resp.json()
        self.assertEqual(json_resp['alertId'], alert.get_uuid())

    def test_alert_create_not_authorized(self):
        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
            "message": "TPO odpade"
        }

        c = Client()
        resp = c.put(reverse('alert'), data=json.dumps(data), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 401)

    def test_alert_get_ok(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('alert-by-id', args=[alert.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['id'], alert.get_uuid())
        self.assertEqual(alert.message, json_resp["message"])

    def test_alert_get_not_authorized(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('alert-by-id', args=[alert.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 401)

    def test_alert_update(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        data = {
                "message": "Hello!! Time to study now! :D"
        }

        c = Client()
        resp = c.post(reverse('alert-by-id', args=[alert.get_uuid()]), data=json.dumps(data),
                      HTTP_AUTHORIZATION="Bearer {}".format(token), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['alertId'], alert.get_uuid())
        alert_updated = Alert.objects.get(pk=1)
        self.assertEqual(alert_updated.message, data['message'])

    def test_alert_update_not_authorized(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
                "message": "Hello!! Time to study now! :D"
        }

        c = Client()
        resp = c.post(reverse('alert-by-id', args=[alert.get_uuid()]), data=json.dumps(data),
                      HTTP_AUTHORIZATION="Bearer {}".format(token), content_type="application/json")
        self.assertEqual(resp.status_code, 401)

    def test_alert_delete(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.delete(reverse('alert-user-by-id', args=[alert.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token))
        self.assertEqual(resp.status_code, 200)
        self.assertRaises(Alert.DoesNotExist, Alert.objects.get, pk=1)

    def test_alert_admin_delete(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        c = Client()
        resp = c.delete(reverse('alert-by-id', args=[alert.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token))
        self.assertEqual(resp.status_code, 200)
        self.assertRaises(Alert.DoesNotExist, Alert.objects.get, pk=1)

    def test_alert_admin_delete_not_authorized(self):
        alert = Alert.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.delete(reverse('alert-by-id', args=[alert.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token))
        self.assertEqual(resp.status_code, 401)

import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

from user_system.auth.jwtutil import create_token_for_user


class AccountRegistrationTest(TestCase):

    fixtures = ['Account.yaml']

    def test_endpoint_new_registration(self):
        """ Tests the endpoint if we create a new registration. """

        c = Client()

        data = {
            "username": "foobar",
            "email": "foobar@example.com",
            "firstName": "Foo",
            "lastName": "Bar",
            "password": "secretpassword"
        }

        resp = c.post(reverse('student-registration'), data=json.dumps(data), content_type="application/json")
        self.assertEqual(resp.status_code, 201)

    def test_endpoint_existing_username(self):
        """ Tests the endpoint if we create a new registration using an existing username. """

        c = Client()

        data = {
            "username": "admin",
            "email": "admin.alt@example.com",
            "firstName": "Foo",
            "lastName": "Bar",
            "password": "secretpassword"
        }

        resp = c.post(reverse('student-registration'), data=json.dumps(data), content_type="application/json")
        self.assertEqual(resp.status_code, 400)

    def test_endpoint_existing_email(self):
        """ Tests the endpoint if we create a new registration using an existing email. """

        c = Client()

        data = {
            "username": "random_username",
            "email": "admin@example.com",
            "firstName": "Foo",
            "lastName": "Bar",
            "password": "secretpassword"
        }

        resp = c.post(reverse('student-registration'), data=json.dumps(data), content_type="application/json")
        self.assertEqual(resp.status_code, 400)

    def test_endpoint_username_case_insensitive(self):
        """ Tests the endpoint if we can create a new registration using a different registered username case. """

        c = Client()

        data = {
            "username": "AdMIN",
            "email": "admin_alt@example.com",
            "firstName": "Foo",
            "lastName": "Bar",
            "password": "secretpassword"
        }

        resp = c.post(reverse('student-registration'), data=json.dumps(data), content_type="application/json")
        self.assertEqual(resp.status_code, 400)


class AccountViewGetTest(TestCase):

    fixtures = ['Account.yaml']

    def test_endpoint(self):
        """ Tests the endpoint if we can get self data. """

        user = User.objects.all().first()
        token = create_token_for_user(user)
        authorization_value = "Bearer {}".format(token)

        c = Client()
        resp = c.get(reverse('account'), HTTP_AUTHORIZATION=authorization_value)

        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()

        self.assertEqual(json_resp['uuid'], user.account.get_uuid())
        self.assertEqual(json_resp['firstName'], user.first_name)
        self.assertEqual(json_resp['lastName'], user.last_name)
        self.assertEqual(json_resp['email'], user.email)
        self.assertEqual(json_resp['emailVerified'], user.account.email_verified)

    def test_endpoint_no_auth(self):
        """ Tests the endpoint if there is no JWT token. """

        c = Client()
        resp = c.get(reverse('account'))

        self.assertEqual(resp.status_code, 401)

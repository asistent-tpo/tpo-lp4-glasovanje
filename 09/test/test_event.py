import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

# Create your tests here.
from logic.models import Event
from user_system.auth.jwtutil import create_token_for_user


class EventTests(TestCase):

    fixtures = ['Account.yaml', 'Event.yaml']

    def test_event_create_ok(self):
        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
            "name": "Zabava",
            "time": "2019-05-09T10:32:17.256Z",
            "durationMinutes": 60,
            "description": "Zabava pri Dejanu."
        }

        c = Client()
        resp = c.put(reverse('event'), data=json.dumps(data), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 201)

        json_resp = resp.json()
        uuid = json_resp["eventId"]
        event = Event.objects.get(uuid=uuid)

        # print(event)
        self.assertEqual(event.name, data["name"])
        self.assertEqual(event.description, data["description"])
        self.assertEqual(event.durationMinutes, data["durationMinutes"])

    def test_event_get_ok(self):
        event = Event.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('event-by-id', args=[event.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['id'], event.get_uuid())
        self.assertEqual(event.name, json_resp["name"])
        self.assertEqual(json_resp['description'], event.description)
        self.assertEqual(event.durationMinutes, json_resp["durationMinutes"])

    def test_event_get_not_authorized(self):
        event = Event.objects.get(pk=1)

        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('event-by-id', args=[event.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 401)

    def test_event_update(self):
        event = Event.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
                "name": "Parteey",
                "time": "2019-05-09T10:33:17.256Z",
                "durationMinutes": 199,
                "description": "Party pri Rupotu."
        }

        c = Client()
        resp = c.post(reverse('event-by-id', args=[event.get_uuid()]), data=json.dumps(data),
                      HTTP_AUTHORIZATION="Bearer {}".format(token), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['eventId'], event.get_uuid())
        event_updated = Event.objects.get(pk=1)
        self.assertEqual(event_updated.name, data['name'])
        self.assertEqual(event_updated.durationMinutes, data['durationMinutes'])
        self.assertEqual(event_updated.description, data['description'])

    def test_event_delete(self):
        event = Event.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.delete(reverse('event-by-id', args=[event.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token))
        self.assertEqual(resp.status_code, 200)
        self.assertRaises(Event.DoesNotExist, Event.objects.get, pk=1)

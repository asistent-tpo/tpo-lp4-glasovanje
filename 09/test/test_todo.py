import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

# Create your tests here.
from logic.models import TodoElement
from user_system.auth.jwtutil import create_token_for_user


class TodoTests(TestCase):

    fixtures = ['Account.yaml', 'Todo.yaml']

    def test_todo_get_ok(self):
        todo = TodoElement.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('todo-by-id', args=[todo.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['id'], todo.get_uuid())
        self.assertEqual(json_resp['description'], todo.description)

    def test_todo_get_not_authorized(self):
        todo = TodoElement.objects.get(pk=1)

        user = User.objects.get(pk=1)
        token = create_token_for_user(user)

        c = Client()
        resp = c.get(reverse('todo-by-id', args=[todo.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 401)

    def test_todo_delete(self):
        todo = TodoElement.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        c = Client()
        resp = c.delete(reverse('todo-by-id', args=[todo.get_uuid()]), HTTP_AUTHORIZATION="Bearer {}".format(token))
        self.assertEqual(resp.status_code, 200)
        self.assertRaises(TodoElement.DoesNotExist, TodoElement.objects.get, pk=1)

    def test_todo_list(self):
        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
            'description': 'Super todo',
        }

        c = Client()
        resp = c.put(reverse('todo'), data=json.dumps(data), HTTP_AUTHORIZATION="Bearer {}".format(token),
                     content_type="application/json")
        self.assertEqual(resp.status_code, 201)

        todo = TodoElement.objects.last()

        json_resp = resp.json()
        self.assertEqual(json_resp['todoElementId'], todo.get_uuid())

    def test_todo_update(self):
        todo = TodoElement.objects.get(pk=1)

        user = User.objects.get(pk=2)
        token = create_token_for_user(user)

        data = {
            'description': 'Number 2.'
        }

        c = Client()
        resp = c.post(reverse('todo-by-id', args=[todo.get_uuid()]), data=json.dumps(data),
                      HTTP_AUTHORIZATION="Bearer {}".format(token), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        json_resp = resp.json()
        self.assertEqual(json_resp['todoElementId'], todo.get_uuid())
        todo_updated = TodoElement.objects.get(pk=1)
        self.assertEqual(todo_updated.description, data['description'])

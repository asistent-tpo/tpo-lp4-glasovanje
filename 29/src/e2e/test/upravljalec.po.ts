import { browser, by, element } from 'protractor';

export class UpravljalecPage {
  navigateTo() {
    return browser.get('/upravljalec') as Promise<any>;
  }
}

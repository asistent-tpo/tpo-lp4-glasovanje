import { Injectable } from '@angular/core';
import { AdministratorskaObvestila } from '../modeli/AdministratorskaObvestila';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaObvestila {

  private ObvestilaCollection: AngularFirestoreCollection<AdministratorskaObvestila>;
  obvestila: Observable<AdministratorskaObvestila[]>;

  constructor(private readonly afs: AngularFirestore) {
    this.ObvestilaCollection = afs.collection<AdministratorskaObvestila>('administratorskaObvestila');
    this.obvestila = this.ObvestilaCollection.valueChanges();
  }

  vrni_obvestila(): Observable<AdministratorskaObvestila[]> {
    return this.afs.collection<AdministratorskaObvestila>('administratorskaObvestila').valueChanges();
  }

  dodaj_obvestilo(sporocilo: string): Promise<void> {
    const id = this.afs.createId();
    const obvestilo: AdministratorskaObvestila = { id, sporocilo };
    return this.ObvestilaCollection.doc(id).set(obvestilo);
  }

  izbrisi_obvestilo(id: string) {
    this.afs.collection<AdministratorskaObvestila>('administratorskaObvestila').doc(id).delete();
  }

}

import { Injectable } from '@angular/core';
import { Postaja } from '../modeli/Postaja';
import { Route } from '../modeli/Route';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap, take, filter, switchMap, first, shareReplay } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Arrival } from '../modeli/Arrival';
import { LiveArrival } from '../modeli/LiveArrival';


@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaAvtobuse {

  private urlPostaje = 'https://cors-anywhere.herokuapp.com/http://data.lpp.si/stations/getAllStations';
  // private urlArrivals = 'https://cors-anywhere.herokuapp.com/http://data.lpp.si/timetables/getArrivalsOnStation?station_int_id=';
  private urlLiveArrivals = 'https://cors-anywhere.herokuapp.com/http://data.lpp.si/timetables/liveBusArrival?station_int_id=';
  private urlRoutes = 'https://cors-anywhere.herokuapp.com/http://data.lpp.si/routes/getRoutes';
  public routes$: Observable<Route[]> = this.vrni_seznam_avtobusov().pipe(shareReplay(1));
  public stations$: Observable<Postaja[]> = this.getAllStations().pipe(shareReplay(1));

  constructor(private http: HttpClient, private readonly afs: AngularFirestore) {}

  private getAllStations(): Observable<Postaja[]> {
    return this.http.get<{
      success: boolean;
      data: Postaja[];
    }>(this.urlPostaje).pipe(
        map(postajaKlic => postajaKlic.data)
      );
  }

  public vrni_podatke_o_prihodih(station_int_id : number): Observable<LiveArrival[]> {
    return this.http.get<{
      success: boolean;
      data: LiveArrival[];
    }>(this.urlLiveArrivals + station_int_id).pipe(
        map( liveArrivalKlic => liveArrivalKlic.data)
      );
    /* return this.http.get<{
      success: boolean;
      data: Arrival[];
    }>(this.urlArrivals + station_int_id).pipe(
        switchMap(arrivalKlic =>
            this.routes$.pipe(
              map((routes: Route[]) => {
                let arrivals: Arrival[] = [];
                for (let arrival of arrivalKlic.data) {
                  arrival.route = routes.filter((route: Route) => route.int_id === arrival.route_departure_int_id)[0];
                  arrivals.push(arrival);
                }
                return arrivals;
              })
            )
        )
      ); */
  }

  /* private pridobi_podatke_postaje(int_id: number): Observable<Postaja> {
    return this.stations$.pipe(
      map((postaje: Postaja[]) => postaje.filter(
        (postaja: Postaja) => postaja.int_id === int_id
      )[0])
    );
  } */

  private vrni_seznam_avtobusov(): Observable<Route[]> {
    return this.http.get<{
      success: boolean;
      data: Route[];
    }>(this.urlRoutes).pipe(
      map(routeKlic => routeKlic.data)
    );
  }

}

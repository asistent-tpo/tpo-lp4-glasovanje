import { Injectable } from '@angular/core';
import { Dogodek } from '../modeli/Dogodek';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaDogodke {

  /**
   * VARIABLES
   */

  private DogodekCollection: AngularFirestoreCollection<Dogodek>;

  /**
   * CONSTRUCTOR
   */

  constructor(private readonly afs: AngularFirestore) {
    this.DogodekCollection = afs.collection<Dogodek>('dogodek');
  }

  /**
   * METHODS
   */

  /**
   * @description Modelu posreduje podatke za ustvarjanje novega dogodka. ~Cimirotič
   *
   * @param ime Naslov dogodkta
   * @param organizator Organizator dogodka
   * @param opis Opis dogodka
   */
  objavi_dogodek(ime: string, datum: string, organizator: string, opis: string ): Promise<void> {
    const id = this.afs.createId();
    const dogodek: Dogodek = { id, ime, datum, organizator, opis };
    return this.DogodekCollection.doc(id).set(dogodek);
  }

  pridobi_dogodke(): Observable<Dogodek[]> {
    return this.afs.collection<Dogodek>('dogodek').valueChanges();
  }
}

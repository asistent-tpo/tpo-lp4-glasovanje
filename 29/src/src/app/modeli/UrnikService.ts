import { Injectable } from '@angular/core';
import { Urnik, Dan } from './Urnik';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UrnikService {
  private urnikCollection: AngularFirestoreCollection<Urnik>;
  private afs: AngularFirestore;

  constructor(private readonly a: AngularFirestore) {
    this.afs = a;
    this.urnikCollection = a.collection<Urnik>('urnik');
  }

  dodaj_vnos_v_urnik(email: string, ime: string, trajanje: number, barva: string, dan: Dan, ura: number): Promise<void> {
    const id = this.afs.createId();
    const urnik: Urnik = { id, email, ime, barva, trajanje, dan, ura };
    return this.urnikCollection.doc(id).set(urnik);
  }

  posodobi_vnos_v_urniku(urnik: Urnik): Promise<void> {
    return this.urnikCollection.doc(urnik.id).update(urnik);
  }

  izbrisi_vnos_iz_urnika(urnik: Urnik): Promise<void> {
    return this.urnikCollection.doc(urnik.id).delete();
  }

  vrni_vse_vnose(email: string): Observable<{}[]> {
    return this.afs.collection('urnik', ref => ref.where('email', '==', email)).valueChanges();
  }

}

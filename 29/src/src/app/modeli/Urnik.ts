export class Urnik {
    id: string;
    email: string;
    ime: string;
    barva: string;
    trajanje: number;
    dan: Dan;
    ura: number;
}

export enum Dan {
    ponedeljek = 0,
    torek = 1,
    sreda = 2,
    cetrtek = 3,
    petek = 4,
}

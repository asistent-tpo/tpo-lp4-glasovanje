export class Restavracija {
    id: string;
    doplacilo: number;
    ime: string;
    lat: number;
    lng: number;
    naslov: string;
}

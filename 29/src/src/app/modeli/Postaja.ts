export class Postaja {
    int_id: number;
    ref_id: string;
    name: string;
    geomerty: {
        type: string;
        coordinates: number[];
    };
}

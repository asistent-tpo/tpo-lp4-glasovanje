import { Route } from './Route';

export class Arrival {
    _id: string;
    id: string;
    int_id: number;
    station_id: string;
    station_int_id: number;
    route_departure_id: string;
    route_departure_int_id: number;
    arrival_time: string;
    route?: Route;
}

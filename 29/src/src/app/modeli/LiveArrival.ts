export class LiveArrival {
    route_number: string;
    route_name: string;
    eta: number;
    validity: number;
    generated: string;
}
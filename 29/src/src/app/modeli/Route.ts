export class Route {
    id: string;
    route_parent_id: string;
    int_id: number;
    opposite_route_int_id: number;
    lenght: number;
    group_name: string;
    parent_name: string;
    route_name: string;
}

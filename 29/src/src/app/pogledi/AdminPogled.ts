import { KrmilnikZaObvestila } from './../krmilniki/KrmilnikZaObvestila';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AdministratorskaObvestila } from '../modeli/AdministratorskaObvestila';

@Component({
  selector: 'AdminPogled',
  templateUrl: './AdminPogled.html'
})
export class AdminPogled implements OnInit {

  obvestiloObrazec: FormGroup;
  obvestila$: Observable<AdministratorskaObvestila[]>;

  constructor(
    private fb: FormBuilder,
    private obvestilaService: KrmilnikZaObvestila
  ) {
    this.createForm();
    this.obvestila$ = obvestilaService.vrni_obvestila();
  }

  ngOnInit() {
  }


  createForm() {
    this.obvestiloObrazec = this.fb.group({
       sporocilo: ['', Validators.required ]
    });
  }

  dodaj_obvestilo() {
    this.obvestilaService.dodaj_obvestilo(this.obvestiloObrazec.controls['sporocilo'].value);
    this.obvestiloObrazec.reset();
  }

  izbrisi_obvestilo(obvestilo: AdministratorskaObvestila) {
    this.obvestilaService.izbrisi_obvestilo(obvestilo.id);
  }

}
import { Component, OnInit } from '@angular/core';

import { Dogodek } from '../modeli/Dogodek';
import { Observable } from 'rxjs';
import { KrmilnikZaDogodke } from '../krmilniki/KrmilnikZaDogodke';

@Component({
  selector: 'EventsPogled',
  templateUrl: './EventsPogled.html'
})
export class EventsPogled implements OnInit {

  dogodki$: Observable<Dogodek[]>;

  constructor(
    private krmilnik: KrmilnikZaDogodke
  ) { }

  ngOnInit() {
    this.dogodki$ = this.krmilnik.pridobi_dogodke();
  }

}
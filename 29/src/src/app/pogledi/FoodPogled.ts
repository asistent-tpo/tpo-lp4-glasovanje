import { Component, OnInit } from '@angular/core';

import { Restavracija } from '../modeli/Restavracija';
import { Observable } from 'rxjs';

import { KrmilnikZaRestavracije } from '../krmilniki/KrmilnikZaRestavracije';
import { QuerySnapshot } from '@angular/fire/firestore';

@Component({
  selector: 'FoodPogled',
  templateUrl: './FoodPogled.html'
})
export class FoodPogled implements OnInit {

  restavracije: any;

  selectedValue:any;

  show: any;

  labelOptions = {
    color: 'white',
    fontFamily: '',
    fontSize: '14px',
    fontWeight: 'bold',
    text: "some text"
  }

  // google maps zoom level
  zoom: number = 16;
  
  // initial center position for the map
  lat: number = this.selectedValue ?  this.selectedValue.lat : 46.0501557;
  lng: number =  this.selectedValue ?  this.selectedValue.lng : 14.4689205;
  pos_lat: number;
  pos_lng: number;

  constructor(
    private krmilnik: KrmilnikZaRestavracije
  ) { }

  ngOnInit() {
    this.getLocation();
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {

          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.pos_lat = position.coords.latitude;
          this.pos_lng = position.coords.longitude;

          this.restavracije = this.krmilnik.vrni_podatke_o_restavracijah(this.lat, this.lng);
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Brskalnik ne podpira geolociranja");
    }
  }

  updateLocation(newValue) {
    this.selectedValue = newValue;
    this.lat = this.selectedValue ?  this.selectedValue.lat : 46.0501557;
    this.lng =  this.selectedValue ?  this.selectedValue.lng : 14.4689205;
  }
}



import { Dan, Urnik } from './../modeli/Urnik';
import { Koledar } from './../modeli/Koledar';
import { AdministratorskaObvestila } from './../modeli/AdministratorskaObvestila';
import { KrmilnikZaObvestila } from './../krmilniki/KrmilnikZaObvestila';
import { UporabnikService } from './../modeli/UporabnikService';
import { KrmilnikZaKoledar } from './../krmilniki/KrmilnikZaKoledar';
import { KrmilnikZaUrnik } from './../krmilniki/KrmilnikZaUrnik';
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  ChangeDetectorRef,
  ViewEncapsulation,
  OnInit
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  addMinutes,
  endOfWeek
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTitleFormatter,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { KrmilnikZaTodo } from '../krmilniki/KrmilnikZaTodo';
import { Todo } from '../modeli/Todo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { auth, firestore } from 'firebase';
import { DayViewHourSegment } from 'calendar-utils';
import { fromEvent } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { Slovenian } from 'flatpickr/dist/l10n/sl';
import flatpickr from 'flatpickr';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  weekTooltip(event: CalendarEvent, title: string) {
    if (!event.meta.tmpEvent) {
      return super.weekTooltip(event, title);
    }
  }

  dayTooltip(event: CalendarEvent, title: string) {
    if (!event.meta.tmpEvent) {
      return super.dayTooltip(event, title);
    }
  }
}

@Component({
  selector: 'HomePogled',
  templateUrl: './HomePogled.html',
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter
    }
  ],
  encapsulation: ViewEncapsulation.None
})

export class HomePogled implements OnInit {

  email: string;
  todos$: Observable<Todo[]>;
  dogodekZaUrejanje: Koledar;
  urnikZaUrejanje: Urnik;
  obvestila$: Observable<AdministratorskaObvestila[]>;

  constructor(
    private modal: NgbModal,
    private todoService: KrmilnikZaTodo,
    private krmilnikKoledar: KrmilnikZaKoledar,
    private krmilnikUrnik: KrmilnikZaUrnik,
    public uporabnikService: UporabnikService,
    private obvestilaService: KrmilnikZaObvestila,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    flatpickr.localize(Slovenian);
    //console.log(this.uporabnikService.pridobi_uporabnika().email);
    this.email = (this.uporabnikService.pridobi_uporabnika()) ? this.uporabnikService.pridobi_uporabnika().email : null;
    if(this.email != null){
      let dogodki = this.krmilnikKoledar.vrni_vnose_iz_koledarja(this.email);
      dogodki.subscribe((dogodki: Koledar[]) => {
        this.events = [];
        for(let dogodek of dogodki){
          this.addToCalendar(dogodek);
        }
      });
      let predmeti = this.krmilnikUrnik.vrni_vnose_iz_urnika(this.email);
      predmeti.subscribe((predmeti: Urnik[]) => {
        this.eventsUrnik = [];
        for(let predmet of predmeti){
          this.addToSchedule(predmet);
        }
      });
      this.todos$ = this.todoService.vrni_vnose_iz_todo(this.email).pipe(map((data) => {
        data.sort((a: Todo, b: Todo) => {
            return a.datum.toMillis() < b.datum.toMillis() ? -1 : 1;
         });
        return data;
     }));
      this.obvestila$ = this.obvestilaService.vrni_obvestila();
    }
  }

  @ViewChild('modalAdd') modalAdd: TemplateRef<any>;
  @ViewChild('modalDelete') modalDelete: TemplateRef<any>;
  @ViewChild('modalAddTodo') modalAddTodo: TemplateRef<any>;
  @ViewChild('modalUpdateTodo') modalUpdateTodo: TemplateRef<any>;
  @ViewChild('modalDeleteTodo') modalDeleteTodo: TemplateRef<any>;
  @ViewChild('modalAddUrnik') modalAddUrnik: TemplateRef<any>;
  @ViewChild('modalDeleteUrnik') modalDeleteUrnik: TemplateRef<any>;
  @ViewChild('modalError') modalError: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  locale: string = 'sl';

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  addForm: {
    naziv: string;
    opis: string;
    zacetniDatum: Date;
    koncniDatum: Date;
    barva: string;
    isAdd: boolean;
    isValid: boolean;
  };

  formTodo: {
    opis: string,
    id: string
  };

  formUrnik: {
    ime: string,
    dan: Dan,
    trajanje: Date,
    zacetek: Date,
    barva: string,
    isAdd: boolean,
    errorMsg: string
  }

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.dogodekZaUrejanje = event.meta;
        this.modal.open(this.modalDelete, { size: 'lg' });
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[];

  activeDayIsOpen: boolean = false;
  dragIsActive : boolean = false;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else if (!isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) {
        this.activeDayIsOpen = false;
        this.activeDayIsOpen = true;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.dragIsActive = true;
    this.dogodekZaUrejanje = event.meta;
    this.dogodekZaUrejanje.zacetniDatum = firestore.Timestamp.fromDate(newStart);
    this.dogodekZaUrejanje.koncniDatum = firestore.Timestamp.fromDate(newEnd);
    this.krmilnikKoledar.posodobi_vnos_v_koledarju(this.dogodekZaUrejanje);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  prikazi_obrazec_za_dodajanje_vnosa_v_koledar(dan) {
    const x = new Date();
    if (dan.date < x && dan.date.toDateString() !== x.toDateString()){
      this.modal.open(this.modalError, { size: 'lg' });
      return;
    }
    if (this.dragIsActive) {
      this.dragIsActive = false;
      return;
    }
    setTimeout(() => {
      this.addForm = { naziv: '', opis: '' , zacetniDatum: dan.date, koncniDatum: dan.date, barva: colors.red.primary, isAdd: true, isValid: true};
      this.modalKoledar = this.modal.open(this.modalAdd, { size: 'lg' });
    });
  }

  modalKoledar;

  prikazi_obrazec_za_urejanje_vnosa_v_koledarju(dogodek) {
    this.dogodekZaUrejanje = dogodek.meta;
    this.addForm = {
      naziv: this.dogodekZaUrejanje.ime,
      opis: this.dogodekZaUrejanje.opis ,
      zacetniDatum: this.dogodekZaUrejanje.zacetniDatum.toDate(),
      koncniDatum: this.dogodekZaUrejanje.koncniDatum.toDate(),
      barva: this.dogodekZaUrejanje.barva,
      isAdd: false,
      isValid: true
    };
    this.modalKoledar = this.modal.open(this.modalAdd, { size: 'lg' });
  }

  addToCalendar(dogodek: Koledar) {
    this.events = [
      ...this.events,
      {
        title: '<b>' + dogodek.ime + '</b>:' + ' <i>' + dogodek.opis + '</i>',
        start: dogodek.zacetniDatum.toDate(),
        end: dogodek.koncniDatum.toDate(),
        color: {
          primary: dogodek.barva,
          secondary: '#fdf1ba'
        },
        draggable: true,
        actions: this.actions,
        allDay: false,
        resizable: {
          beforeStart: true,
          afterEnd: true
        },
        meta: dogodek
      }
    ];
  }

  potrdi_dodajanje_vnosa_v_koledar() {
    if (!this.validirajVnosKoledarja()) {
      return;
    }
    this.krmilnikKoledar.dodaj_vnos_v_koledar(
      this.addForm.naziv,
      this.email,
      this.addForm.zacetniDatum,
      this.addForm.koncniDatum,
      this.addForm.barva,
      this.addForm.opis);
  }

  potrdi_urejanje_vnosa_v_koledarju() {
    this.dogodekZaUrejanje.ime = this.addForm.naziv;
    this.dogodekZaUrejanje.opis = this.addForm.opis;
    this.dogodekZaUrejanje.barva = this.addForm.barva;
    this.dogodekZaUrejanje.zacetniDatum = firestore.Timestamp.fromDate(this.addForm.zacetniDatum);
    this.dogodekZaUrejanje.koncniDatum = firestore.Timestamp.fromDate(this.addForm.koncniDatum);
    if (!this.validirajVnosKoledarja()) {
      return;
    }
    this.krmilnikKoledar.posodobi_vnos_v_koledarju(this.dogodekZaUrejanje);
  }

  validirajVnosKoledarja() {
    if (this.addForm.zacetniDatum > new Date() && this.addForm.zacetniDatum < this.addForm.koncniDatum) {
      this.modalKoledar.close();
      return true;
    } else {
      this.addForm.isValid = false;
    }
    return false;
  }

  potrdi_brisanje_vnosa_iz_koledarja(){
    this.krmilnikKoledar.izbrisi_vnos_iz_koledarja(this.dogodekZaUrejanje);
    this.activeDayIsOpen = false;
  }

  counterClick(day) {
    event.stopPropagation();
    this.dayClicked(day);
  }

  dodaj_vnos_v_todo() {
    this.todoService.dodaj_vnos_v_todo(this.uporabnikService.pridobi_uporabnika().email, this.formTodo.opis);
  }

  posodobi_vnos_v_todo() {
    this.todoService.posodobi_vnos_v_todo(this.uporabnikService.pridobi_uporabnika().email, this.formTodo.opis, this.formTodo.id);
  }

  prikaziFormoZaAddTodo() {
      this.formTodo = { opis: '', id: '' };
      this.modal.open(this.modalAddTodo, { size: 'lg' });
  }

  prikaziFormoZaUpdateTodo(todo: Todo){
    this.formTodo = { opis: todo.opis, id: todo.id };
    this.modal.open(this.modalUpdateTodo, { size: 'lg' });
  }

  deleteTodo() {
    this.todoService.izbrisi_vnos_iz_todo(this.formTodo.id);
  }

  potrdi_brisanje_vnosa_v_todo(todo: Todo) {
    this.formTodo = { opis: todo.opis, id: todo.id };
    this.modal.open(this.modalDeleteTodo, { size: 'lg' });
  }

  viewDateUrnik = new Date('1950-11-13');

  eventsUrnik: CalendarEvent[] = [];

  addToSchedule(predmet){
    this.eventsUrnik = [
      ...this.eventsUrnik,
      {
        id: this.eventsUrnik.length,
        title: predmet.ime,
        start: new Date(1950, 10, 13 + predmet.dan, Math.floor(predmet.ura), Math.ceil((predmet.ura - Math.floor(predmet.ura)) * 60)),
        end: new Date(1950, 10, 13 + predmet.dan, Math.floor(predmet.ura + predmet.trajanje),
                      Math.ceil((predmet.ura + predmet.trajanje - Math.floor(predmet.ura + predmet.trajanje)) * 60)),
        color: {
          primary: predmet.barva,
          secondary: predmet.barva
        },
        meta: predmet
      }
    ];
  }

  dodaj_vnos_v_urniku() {
      const predmet = this.eventsUrnik[this.eventsUrnik.length - 1];
      const trajanje = this.formUrnik.trajanje.getHours() + (this.formUrnik.trajanje.getMinutes() / 60);
      const zacetek = predmet.start.getHours() + (predmet.start.getMinutes() / 60);
      if (!this.validirajVnosUrnika(zacetek, trajanje)) {
        return;
      }
      this.krmilnikUrnik.dodaj_vnos_v_urnik(this.email, this.formUrnik.ime, trajanje, this.formUrnik.barva,
        predmet.start.getDay() - 1, zacetek);
  }

  prekini_vnos_v_urnik(){
    this.eventsUrnik.pop();
    this.refreshUrnik();
  }

  modalUrnik;

  odpri_vnos_urnika(event){
    this.urnikZaUrejanje = event.meta;
    setTimeout(() => {
      const predmet = event.meta;
      this.formUrnik = {ime: predmet.ime, dan: predmet.dan, trajanje:
        new Date(1950, 10, 13, Math.floor(predmet.trajanje), Math.ceil((predmet.trajanje - Math.floor(predmet.trajanje)) * 60)),
        zacetek: event.start, barva: predmet.barva, isAdd: false, errorMsg: ''};
      this.modalUrnik = this.modal.open(this.modalAddUrnik, { size: 'lg' });
    });
  }

  posodobi_vnos_v_urniku() {
    this.urnikZaUrejanje.dan = Dan[Dan[this.formUrnik.dan]];
    this.urnikZaUrejanje.barva = this.formUrnik.barva;
    this.urnikZaUrejanje.ura = this.formUrnik.zacetek.getHours() + (this.formUrnik.zacetek.getMinutes() / 60);
    this.urnikZaUrejanje.trajanje = this.formUrnik.trajanje.getHours() + (this.formUrnik.trajanje.getMinutes() / 60);
    this.urnikZaUrejanje.ime = this.formUrnik.ime;
    if (!this.validirajVnosUrnika(this.urnikZaUrejanje.ura, this.urnikZaUrejanje.trajanje)) {
      return;
    }
    this.krmilnikUrnik.posodobi_vnos_v_urniku(this.urnikZaUrejanje);
  }

  validirajVnosUrnika(zacetek: number, trajanje: number){
      if (zacetek > 7 && zacetek + trajanje <= 22) {
        this.modalUrnik.close();
        return true;
      } else if (zacetek < 7){
        this.formUrnik.errorMsg = 'Tako zgodaj nima smisla imeti rednih obveznosti. Pred sedmo raje počivaj.';
      } else {
        this.formUrnik.errorMsg = 'Po deseti zvečer najbrž ni rednih aktivnosti. Morda vnesi dogodek v koledar.';
      }
      return false;
  }

  odpri_brisanje_vnosa_iz_urnika() {
    this.modal.open(this.modalDeleteUrnik, { size: 'lg' });
  }

  potrdi_brisanje_vnosa_iz_urnika(){
    this.krmilnikUrnik.izbrisi_vnos_iz_urnika(this.urnikZaUrejanje);
  }

  dragToCreateActive = false;

  startDragToCreate(
    segment: DayViewHourSegment,
    mouseDownEvent: MouseEvent,
    segmentElement: HTMLElement
  ) {
    const dragToSelectEvent: CalendarEvent = {
      id: this.eventsUrnik.length,
      title: 'New event',
      start: segment.date,
      meta: {
        tmpEvent: true
      },
      color: {
        primary: colors.red.primary,
        secondary: colors.red.primary,
      },
    };
    this.eventsUrnik = [...this.eventsUrnik, dragToSelectEvent];
    const segmentPosition = segmentElement.getBoundingClientRect();
    this.dragToCreateActive = true;
    const endOfView = endOfWeek(this.viewDateUrnik);

    fromEvent(document, 'mousemove')
      .pipe(
        finalize(() => {
          delete dragToSelectEvent.meta.tmpEvent;
          this.dragToCreateActive = false;
          this.refreshUrnik();
          setTimeout(() => {
            if (dragToSelectEvent.end === undefined) {
              const st = dragToSelectEvent.start;
              let min = st.getMinutes();
              let ur = st.getHours();
              if (min === 30) {
                min = 0;
                ur++;
              }
              dragToSelectEvent.end = new Date(1950, 10, st.getDate(), ur, min);
            }
            let trajanjeUra = dragToSelectEvent.end.getHours() - dragToSelectEvent.start.getHours();
            let trajanjeMinute = dragToSelectEvent.end.getMinutes() - dragToSelectEvent.start.getMinutes();
            if (trajanjeMinute < 0) {
              trajanjeUra--;
              trajanjeMinute += 60;
            }
            this.formUrnik = {ime: '', dan: Dan.ponedeljek, trajanje: new Date(1950, 10, 10, trajanjeUra, trajanjeMinute),
                              zacetek: new Date(), barva: colors.red.primary, isAdd: true, errorMsg: ''};
            this.modalUrnik = this.modal.open(this.modalAddUrnik, { size: 'lg' });
          });
        }),
        takeUntil(fromEvent(document, 'mouseup'))
      )
      .subscribe((mouseMoveEvent: MouseEvent) => {
        const minutesDiff = ceilToNearest(
          mouseMoveEvent.clientY - segmentPosition.top,
          30
        );

        const daysDiff =
          floorToNearest(
            mouseMoveEvent.clientX - segmentPosition.left,
            segmentPosition.width
          ) / segmentPosition.width;

        const newEnd = addDays(addMinutes(segment.date, minutesDiff), daysDiff);
        if (newEnd > segment.date && newEnd < endOfView) {
          dragToSelectEvent.end = newEnd;
        }
        this.refreshUrnik();
      });
  }

  private refreshUrnik() {
    this.eventsUrnik = [...this.eventsUrnik];
    this.cdr.detectChanges();
  }
}

function floorToNearest(amount: number, precision: number) {
  return Math.floor(amount / precision) * precision;
}

function ceilToNearest(amount: number, precision: number) {
  return Math.ceil(amount / precision) * precision;
}

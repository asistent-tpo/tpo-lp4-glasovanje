import { browser, by, element } from 'protractor';

export class EventPage {
  navigateTo() {
    return browser.get('/event') as Promise<any>;
  }
}

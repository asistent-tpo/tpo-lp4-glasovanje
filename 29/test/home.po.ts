import { browser, by, element } from 'protractor';

export class HomePage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  getTitleText() {
    return element(by.css('#landing-title')).getText();
  }

  getTodoAddButton() {
    return element(by.css('#todo-add-btn'));
  }

  getTodoAddinput() {
    return element(by.css('#todo-add-input'));
  }

  getTodoAddConfirm() {
    return element(by.css('#todo-add-confirm'));
  }

  getTodoLastItem() {
    return element.all(by.css('.list-group-item.list-group-item-warning strong')).last();
  }
}

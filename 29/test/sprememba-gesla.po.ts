import { browser, by, element } from 'protractor';

export class Sprememba-GeslaPage {
  navigateTo() {
    return browser.get('/sprememba-gesla') as Promise<any>;
  }
}

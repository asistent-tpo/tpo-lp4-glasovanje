import { browser, by, element } from 'protractor';

export class RegistracijaPage {
  navigateTo() {
    return browser.get('/registracija') as Promise<any>;
  }

  getEmailInput() {
    return element(by.css('[formcontrolname="email"]'));
  }

  getPassword1Input() {
    return element(by.css('[formcontrolname="geslo1"]'));
  }

  getPassword2Input() {
    return element(by.css('[formcontrolname="geslo2"]'));
  }

  getSubmitButton() {
    return element(by.css('#register-submit'));
  }
}

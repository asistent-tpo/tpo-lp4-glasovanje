import { BusPage } from './bus.po';
import { HomePage } from './home.po';
import { browser, logging, ExpectedConditions, element, by } from 'protractor';
import { AppPage } from './app.po';
import { RegistracijaPage } from './registracija.po';
import { PrijavaPage } from './prijava.po';

describe('workspace-project App', () => {
  let appPage: AppPage;
  let registerPage: RegistracijaPage;
  let homePage: HomePage;
  let prijavaPage: PrijavaPage;
  let busPage: BusPage;

  beforeEach(() => {
    appPage = new AppPage();
    registerPage = new RegistracijaPage();
    homePage = new HomePage();
    prijavaPage = new PrijavaPage();
    busPage = new BusPage();

    browser.driver.manage().window().maximize();
    appPage.navigateTo();
    browser.waitForAngularEnabled(true);
  });

  it('REGISTRACIJA', () => {
    appPage.getLoginDropdown().click();
    appPage.getRegisterButton().click();

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + '/registracija');
    registerPage.getEmailInput().sendKeys('ag4332@student.uni-lj.si');
    registerPage.getPassword1Input().sendKeys('password');
    registerPage.getPassword2Input().sendKeys('password');
    registerPage.getSubmitButton().click();

    browser.waitForAngularEnabled(false);
    browser.wait(ExpectedConditions.presenceOf(element(by.css('#landing-title'))), 5000, 'Element taking too long to appear in the DOM');
    expect(homePage.getTitleText()).toEqual('StraightAs');
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + '/');
  });

  it('PRIJAVA', () => {
    appPage.getLoginDropdown().click();
    appPage.getLoginButton().click();

    prijavaPage.getEmailInput().sendKeys('uporabnik@uporabnik.com');
    prijavaPage.getPassword1Input().sendKeys('uporabnik');
    prijavaPage.getSubmitButton().click();

    browser.waitForAngularEnabled(false);
    browser.wait(ExpectedConditions.textToBePresentInElement(appPage.getLoginDropdown(), 'uporabnik@uporabnik.com'),
      5000, 'Element taking too long to appear in the DOM');
  });

  it('AVTOBUSI', () => {
    appPage.getBusMenu().click();
    busPage.getTypeaheadInput().sendKeys('kolo');
    browser.wait(ExpectedConditions.presenceOf(busPage.getFirstTypeAheadSuggestion()),
      5000, 'Element taking too long to appear in the DOM');
    expect(busPage.getFirstTypeAheadSuggestion().getText()).toEqual('Kolodvor (300011)');
  });

  afterEach(() => {
    browser.restart();
  });
});

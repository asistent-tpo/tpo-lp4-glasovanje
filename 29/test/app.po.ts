import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getLoginDropdown() {
    return element(by.css('#login-dropdown'));
  }

  getChangePasswordButton() {
    return element(by.css('#change-password'));
  }

  getLoginButton() {
    return element(by.css('#login'));
  }

  getRegisterButton() {
    return element(by.css('#register'));
  }

  getLogoutButton() {
    return element(by.css('#logout'));
  }

  getBusMenu() {
    return element(by.css('[routerlink="/bus"]'));
  }
}

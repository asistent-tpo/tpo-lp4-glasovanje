import { browser, by, element } from 'protractor';

export class PrijavaPage {
  navigateTo() {
    return browser.get('/prijava') as Promise<any>;
  }

  getEmailInput() {
    return element(by.css('[formcontrolname="email"]'));
  }

  getPassword1Input() {
    return element(by.css('[formcontrolname="geslo"]'));
  }

  getSubmitButton() {
    return element(by.css('#login-submit'));
  }
}

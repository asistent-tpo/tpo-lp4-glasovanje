var express = require('express');
var router = express.Router();

var ctrlUporabniki = require('../controllers/uporabniki');
var ctrlBaza = require('../controllers/baza');
var ctrlEvent = require('../controllers/event');
var ctrlKoledar = require('../controllers/koledar');
var ctrlBus = require('../controllers/avtobus');
var ctrlTodo = require('../controllers/todoseznam');
var ctrlUrnik = require('../controllers/urnik');
var ctrlAdmin = require('../controllers/adminobvestilo');

//Baza
//router.delete('/izbrisbaze', ctrlBaza.izbrisiVse);
router.get('/izbrisbazeuporabnik', ctrlBaza.izbrisiVseUporabnike);
router.get('/izbrisbazedogodki', ctrlBaza.izbrisiVseDogodke);
router.get('/izbrisbazekoledar', ctrlBaza.izbrisiVseKoledar);
router.get('/izbrisbazeurnik', ctrlBaza.izbrisiVseUrnik);
router.get('/izbrisbazetodo', ctrlBaza.izbrisiVseTodo);
router.get('/izbrisbazeobvestilo', ctrlBaza.izbrisiVseObvestilo);
//router.post('/zacetnabaza', ctrlBaza.zacetnivnos);
router.get('/zacetnabazauporabnik', ctrlBaza.zacetniVnosUporabnika);
router.get('/zacetnabazadogodki', ctrlBaza.zacetniVnosDogodkov);
router.get('/zacetnabazakoledar', ctrlBaza.zacetniVnosKoledar);
router.get('/zacetnabazaurnik', ctrlBaza.zacetniVnosUrnik);
router.get('/zacetnabazatodo', ctrlBaza.zacetniVnosTodo);
router.get('/zacetnabazaobvestilo', ctrlBaza.zacetniVnosObvestilo);

// Uporabniki
router.post('/prijava', ctrlUporabniki.uporabnikiPrijava);
router.get('/uporabniki', ctrlUporabniki.uporabnikiSeznam);
router.post('/uporabniki', ctrlUporabniki.uporabnikKreiraj);
router.get('/uporabniki/:idUporabnika', ctrlUporabniki.uporabnikiPreberiIzbrano);
router.put('/uporabniki/:idUporabnika', ctrlUporabniki.uporabnikiPosodobiIzbrano);
router.delete('/uporabniki/:idUporabnika', ctrlUporabniki.uporabnikiIzbrisiIzbrano);
router.post('/spremenigeslo', ctrlUporabniki.uporabnikSpremeniGeslo);

// Admin
router.get('/obvestila', ctrlAdmin.obvestilaSeznam);
router.post('/obvestilo', ctrlAdmin.obvestiloKreiraj);

/* Bus */
router.post('/isciBus', ctrlBus.seznamUrnikaAvtobusa);

/* Events */
router.get('/events', ctrlEvent.eventSeznam);
router.post('/events', ctrlEvent.eventKreiraj);
router.delete('/events/delete/:id', ctrlEvent.delete);

// Koledar
router.get('/koledar/:idUporabnika/:mesecLeto', ctrlKoledar.koledarUporabnikMesec); // :mesecLeto je npr. 05-2019
router.post('/koledar', ctrlKoledar.koledarKreiraj);
router.delete('/koledar/:idKoledar', ctrlKoledar.koledarDelete);
router.put('/koledar/:idKoledar', ctrlKoledar.koledarPosodobi);

// Urnik
router.get('/urnik/:idUporabnika', ctrlUrnik.urnikUporabnik);
router.post('/urnik/', ctrlUrnik.urnikKreiraj);
router.delete('/urnik/:idUrnik', ctrlUrnik.urnikDelete);
router.put('/urnik/:idUrnik', ctrlUrnik.urnikPosodobi);

// Todo
router.get('/todo/:idUporabnika', ctrlTodo.todoSeznam);
router.post('/todo/:idUporabnika', ctrlTodo.kreirajTodo);
router.delete('/todo/:idTodo', ctrlTodo.todoIzbrisiIzbrano);


module.exports = router;
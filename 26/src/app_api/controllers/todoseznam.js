var mongoose = require('mongoose');

var Todo = mongoose.model('Todo');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.todoSeznam = function (zahteva, odgovor) {
    var stZadetkov = 100;
    if(zahteva.params && zahteva.params.idUporabnika){
        Todo.find({userId: zahteva.params.idUporabnika}).limit(stZadetkov).exec(function(napaka,rezultati) {
            seznam = [];
            if(napaka){
                vrniJsonOdgovor(odgovor,500, napaka);
            }
            else if( rezultati.length === 0 ){
                vrniJsonOdgovor(odgovor, 404, {
                    "sporocilo": "Ni todo"
                });
            }
            else {
                rezultati.forEach( function(dokument){
                    seznam.push({
                        id : dokument._id,
                        idUporabnika: dokument.userId,
                        opis : dokument.opis,
                        email : dokument.email
                    });
                });
                vrniJsonOdgovor(odgovor, 200, seznam);
            }
        });
    }
    else{
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Manjka enolični identifikator idUporabnika!"
        });
    }
};

module.exports.kreirajTodo = function(zahteva, odgovor){

    if(zahteva.params && zahteva.params.idUporabnika){
        Todo.create({
            userId: zahteva.body.userId,
            email: zahteva.body.email,
            opis: zahteva.body.opis
        }, function(napaka, todo) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 400, napaka);
            }
            else {
                vrniJsonOdgovor(odgovor, 201, todo);
            }
        });
    }
    else{
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Manjka enolični identifikator idUporabnika!"
        });
    }
};

module.exports.todoIzbrisiIzbrano = function(zahteva, odgovor) {
    var idTodo = zahteva.params.idTodo;
    if (idTodo) {
        Todo
            .findByIdAndRemove(idTodo)
            .exec(
                function(napaka, Uporabnik) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 404, napaka);
                        return;
                    }
                    vrniJsonOdgovor(odgovor, 204, null);
                }
            );
    } else {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo":
                "Ne najdem todo vnosa, idTodo je obvezen parameter."
        });
    }
};

var mongoose = require('mongoose');
const bcrypt = require('bcrypt');


var Uporabnik = mongoose.model('Uporabnik');
var Event = mongoose.model('Event');
var Koledar = mongoose.model('Koledar');
var Urnik = mongoose.model('Urnik');

var Todo = mongoose.model('Todo');
var Obvestilo = mongoose.model('Obvestilo');

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.zacetniVnosUporabnika = function (zahteva, odgovor){
    Uporabnik.create(
    [
    {
        _id : "5c0d3a97e7939ce013c6e33f",
        ime : "Admin",
        priimek : "Admin",
        email : "admin@gmail.com",
        geslo : bcrypt.hashSync("abc", 10),
        datumRegistracije : Date.now(),
        funkcija : 1
    },
    {
        _id : "5c0d3a95e7939ce013c6e33e",
        ime : "Študent",
        priimek : "Študent",
        email : "student@gmail.com",
        geslo : bcrypt.hashSync("abc", 10),
        datumRegistracije : Date.now(),
        funkcija : 2
    },
    {
        _id : "5ceaed584a4650250bf8f3ef",
        ime : "EventManager",
        priimek : "EventManager",
        email : "manager@gmail.com",
        geslo : bcrypt.hashSync("abc", 10),
        datumRegistracije : Date.now(),
        funkcija : 3
    }
    ]
    ,function(napaka, uporabniki){
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 201, uporabniki);
          }
    });

};


module.exports.zacetniVnosDogodkov = function (zahteva, odgovor) {
    Event.create(
        [
            {
                ime: "Zoki",
                datum: "1.1.2020",
                organizator: "zokizazokija@gmail.com",
                opis: "Organizacija kmetovanja na površini s klancom 60%.",
            },
            {
                ime: "Kmet",
                datum: "2.5.2021",
                organizator: "kriptokmet@gmail.com",
                opis: "Predavanje o vseh možnih načinih služenja denarja.",
            },
            {
                ime: "Schwarz",
                datum: "6.3.2021",
                organizator: "onceugoblackunevergoback@gmail.com",
                opis: "Kako je nastala reklama za snickers.",
            }
        ]

        , function (napaka, event) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 400, napaka);
            } else {
                vrniJsonOdgovor(odgovor, 201, event);
            }
        });
};

module.exports.izbrisiVseUporabnike = function (zahteva, odgovor){
    Uporabnik.deleteMany({},function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 204, "izbrisani uporabniki");

    });
};

module.exports.izbrisiVseDogodke = function (zahteva, odgovor){
    Event.deleteMany({},function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 204, "izbrisani dogodki");
    });
};

module.exports.zacetniVnosKoledar = function (zahteva, odgovor){
    Koledar.create({
        userId : "5c0d3a95e7939ce013c6e33e",
        ime : "Zagovor TPO",
        leto : 2019,
        mesec : 4,
        dan : 30,
        cas: "12:00",
        trajanje: "01:30",
        opis: "LP04."
    }, function(napaka, uporabniki){
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 201, uporabniki);
        }
    });
};

module.exports.izbrisiVseKoledar = function (zahteva, odgovor){
    Koledar.deleteMany({},function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 204, "izbrisani vnosi v koledar");
    });
};

module.exports.zacetniVnosUrnik = function (zahteva, odgovor){
    Urnik.create({
        userId : "5c0d3a95e7939ce013c6e33e",
        ime: "TPO",
        dan: 3,
        cas: 9,
        trajanje: 3,
        barva: "/"
    }, function(napaka, uporabniki){
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 201, uporabniki);
        }
    });
};

module.exports.izbrisiVseUrnik = function (zahteva, odgovor){
    Urnik.deleteMany({},function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 204, "izbrisani vnosi v urnik");
    });
};

module.exports.zacetniVnosTodo = function (zahteva, odgovor){
    Todo.create(
    [
        {
            userId : "5c0d3a95e7939ce013c6e33e",
            email: "student@gmail.com",
            opis: "Pospravi sobo."
        },
        {
            userId : "5c0d3a95e7939ce013c6e33e",
            email: "student@gmail.com",
            opis: "Nauči se TPO"
        }
    ]
    , function(napaka, todo){
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 201, todo);
        }
    });
};

module.exports.izbrisiVseTodo = function (zahteva, odgovor){
    Todo.deleteMany({},function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 204, "izbrisani vsi todo");
    });
};

module.exports.zacetniVnosObvestilo = function (zahteva, odgovor){
    Obvestilo.create({
        obvestilo: "Danes je bila javno objavljena spletna aplikacija StraightAs."
    }, function(napaka, obvestila){
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 201, obvestila);
        }
    });
};

module.exports.izbrisiVseObvestilo = function (zahteva, odgovor){
    Obvestilo.deleteMany({},function(napaka){
        if(napaka){
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        vrniJsonOdgovor(odgovor, 204, "izbrisana vsa obvestila");
    });
};

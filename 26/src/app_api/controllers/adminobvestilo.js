var mongoose = require('mongoose');

var Obvestilo = mongoose.model('Obvestilo');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.obvestilaSeznam = function (zahteva, odgovor) {
    var stZadetkov = 100;
    Obvestilo.find().limit(stZadetkov).exec(function(napaka,rezultati) {
        seznam = [];
        if(napaka){
            vrniJsonOdgovor(odgovor,500, napaka);
        }
        else if( rezultati.length === 0 ){
            vrniJsonOdgovor(odgovor, 404, {
                "sporocilo": "Ni obvestil"
            });
        }
        else {
            rezultati.forEach( function(dokument){
                seznam.push({
                    id : dokument._id,
                    sporocilo : dokument.obvestilo
                });
            });
            vrniJsonOdgovor(odgovor, 200, seznam);
        }
    });
};


module.exports.obvestiloKreiraj = function(zahteva, odgovor) {

    // zahteva.body
    console.log(zahteva.body.obvestilo);
    //console.log(zahteva.body.datum);

    Obvestilo.create({
      obvestilo: zahteva.body.obvestilo
      }, function(napaka, Obvestilo) {
        if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
          vrniJsonOdgovor(odgovor, 200, Obvestilo);
    }
  });
};
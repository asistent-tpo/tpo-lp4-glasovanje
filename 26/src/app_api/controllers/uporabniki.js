var mongoose = require('mongoose');
const bcrypt = require('bcrypt');

var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.uporabnikiSeznam = function (zahteva, odgovor) {
    var stZadetkov = 12;

    Uporabnik.find().limit(stZadetkov).exec(function(napaka,rezultati) {
        seznam = [];
        if(napaka){
            vrniJsonOdgovor(odgovor,500,napaka);
        } 
        else if( rezultati.length === 0 ){
            vrniJsonOdgovor(odgovor, 404, {
            "sporocilo": "Ni uporabnikov."
            });
        }
        else {
            rezultati.forEach( function(dokument){
                seznam.push({
                    ime : dokument.ime,
                    priimek : dokument.priimek,
                    email : dokument.email,
                    geslo : dokument.geslo,
                    datumRegistracije: dokument.datumRegistracije
                });
            });
            vrniJsonOdgovor(odgovor, 200, seznam);
        }
    });
};

module.exports.uporabnikKreiraj = function(zahteva, odgovor) {

    // zahteva.body

    Uporabnik.create({
        ime: zahteva.body.ime,
        priimek: zahteva.body.priimek,
        email: zahteva.body.email,
        geslo: zahteva.body.geslo
    }, function(napaka, Uporabnik) {
        if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
            vrniJsonOdgovor(odgovor, 201, Uporabnik);
        }
    });
};

module.exports.uporabnikiPrijava = function(zahteva, odgovor) {
  if(zahteva.body.email && zahteva.body.geslo){
        Uporabnik.findOne({email: zahteva.body.email}, function (napaka, uporabnik) {
            if(napaka){
                vrniJsonOdgovor(odgovor, 404, {
                    //"sporočilo": "Uporabnik ne obstaja"
                    "uspešno": false
                });
            }else{

                if(uporabnik != null && bcrypt.compareSync(zahteva.body.geslo, uporabnik.geslo)){
                    //console.log(JSON.stringify(uporabnik) + " je uporabnik prijavljam api");
                    vrniJsonOdgovor(odgovor, 200, {
                        "uspešno": true,
                        "id": uporabnik._id,
                        "ime": uporabnik.ime,
                        "priimek": uporabnik.priimek,
                        "email": uporabnik.email,
                        "funkcija" : uporabnik.funkcija
                    });

                }else{
                    vrniJsonOdgovor(odgovor, 400, {
                        "uspešno": false
                    });
                }
            }
        })

  }else{
      vrniJsonOdgovor(odgovor, 400, {
          //"sporočilo": "Napaka pri prijavi!"
          "uspešno": false
      });
  }

};

module.exports.uporabnikiPreberiIzbrano = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idUporabnika) {
    Uporabnik
      .findById(zahteva.params.idUporabnika)
      .exec(function(napaka, Uporabnik) {
        if (!Uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporocilo":
              "Ne najdem uporabnika s podanim enoličnim identifikatorjem idUporabnika."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, Uporabnik);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, { 
      "sporocilo": "Manjka enolični identifikator idUporabnika!"
    });
  }
};

module.exports.uporabnikiPosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idUporabnika) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporocilo":
        "Ne najdem uporabnika, idUporabnika je obvezen parameter"
    });
    return;
  }
  Uporabnik
    .findById(zahteva.params.idUporabnika)
    .exec(
      function(napaka, Uporabnik) {
        if (!Uporabnik) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporocilo": "Ne najdem Uporabnik."
          });
          return;            //to sm pozabu dat takrat... jao
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        Uporabnik.ime = zahteva.body.ime;
        Uporabnik.priimek = zahteva.body.priimek;
        Uporabnik.Email = zahteva.body.Email;
        Uporabnik.geslo = zahteva.body.geslo;
        Uporabnik.datumRegistracije = zahteva.body.datumRegistracije;
        Uporabnik.save(function(napaka, Uporabnik) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, Uporabnik);
          }
        });
      }
    );
};

module.exports.uporabnikiIzbrisiIzbrano = function(zahteva, odgovor) {
  var idUporabnika = zahteva.params.idUporabnika;
  if (idUporabnika) {
    Uporabnik
      .findByIdAndRemove(idUporabnika)
      .exec(
        function(napaka, Uporabnik) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporocilo":
        "Ne najdem uporabnika, idUporabnika je obvezen parameter."
    });
  }
};

module.exports.uporabnikSpremeniGeslo = function(zahteva, odgovor){

    var novoGeslo = zahteva.body.novoGeslo;
    var email = zahteva.body.email;
    var hashedGeslo = bcrypt.hashSync(novoGeslo, 10);

    Uporabnik.findOneAndUpdate({email: zahteva.body.email}, {geslo: hashedGeslo}, function (napaka, uporabnik) {
       if(napaka){
           vrniJsonOdgovor(odgovor, 400, {"sporocilo": "Prišlo je do napake"});
       }
       else{
           vrniJsonOdgovor(odgovor, 200, {"sporocilo": "Uspešno posodobljeno geslo"});
       }

    });


};

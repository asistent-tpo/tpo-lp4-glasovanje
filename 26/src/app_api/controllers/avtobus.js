var mongoose = require('mongoose');

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var jsdom = require('jsdom');
var $ = require('jquery')(new jsdom.JSDOM().window);
var DOMParser = require('domparser');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.seznamUrnikaAvtobusa = function (zahteva, odgovor) {
    var id = zahteva.body.postaja;

    const Http = new XMLHttpRequest();
    const url=encodeURI('https://www.trola.si/'+id);
    Http.open("GET", url, true);
    Http.setRequestHeader("Content-Type", "application/json");
    Http.onreadystatechange=(e)=>{
        if (Http.readyState == 4 && Http.status == 200) {
            var html = Http.responseText;

            var all_stations = $(html).find("#stations");
            if (all_stations.length == 0)
                vrniJsonOdgovor(odgovor, 404, html);
            else
                vrniJsonOdgovor(odgovor, Http.status, html);
        } else if (Http.readyState == 4) {
            vrniJsonOdgovor(odgovor, Http.status, html);
        }
    }
    Http.send();
};
var mongoose = require('mongoose');

var Koledar = mongoose.model('Koledar');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.koledarUporabnikMesec = function(zahteva, odgovor) {
    var idUporabnika = zahteva.params.idUporabnika;
    var mesecLeto = zahteva.params.mesecLeto;
    var mesec = parseInt(mesecLeto.substring(0, 2));
    var leto = parseInt(mesecLeto.substring(3, 7));
    if(isNaN(mesec) || isNaN(leto)) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo:": "Napravilna zahteva."
        });
    } else {
        Koledar.find({
            userId: idUporabnika,
            leto: leto,
            mesec: mesec,
        }, function (napaka, rezultati) {
            seznam = [];
            if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                rezultati.forEach(function (dokument) {
                    seznam.push({
                        id: dokument._id,
                        ime: dokument.ime,
                        dan: dokument.dan,
                        cas: dokument.cas,
                        trajanje: dokument.trajanje,
                        opis: dokument.opis
                    });
                });
                vrniJsonOdgovor(odgovor, 200, seznam);
            }
        });
    }
};

module.exports.koledarKreiraj = function(zahteva, odgovor) {
    Uporabnik.findById(zahteva.body.userId)
        .exec(function(napaka, Uporabnik) {
            if (!Uporabnik) {
                vrniJsonOdgovor(odgovor, 400, {
                    "sporocilo": "Uporabnik z identifikorjem idUporabnika ne obstaja."
                });
            } else if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                var daysInMonth = (new Date(zahteva.body.leto, zahteva.body.mesec + 1, 0)).getDate();
                var uraCas = parseInt(zahteva.body.cas.substring(0,2));
                var minutaCas = parseInt(zahteva.body.cas.substring(3,5));
                var uraTrajanja = parseInt(zahteva.body.trajanje.substring(0,2));
                var minutaTrajanja = parseInt(zahteva.body.trajanje.substring(3,5));

                if(zahteva.body.mesec > 11 || zahteva.body.mesec < 0) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljaven mesec. Dovoljene vrednosti: 0-11"
                    });
                } else if(zahteva.body.dan > daysInMonth || zahteva.body.dan < 1) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljaven dan. Dovoljene vrednosti: 1-" + daysInMonth
                    });
                } else if(isNaN(uraCas) || isNaN(minutaCas) || uraCas < 0 || uraCas > 23
                    || minutaCas < 0 || minutaCas > 59) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljaven cas. Mora biti v obliki 12:34"
                    });
                } else if(isNaN(uraTrajanja) || isNaN(minutaTrajanja) || uraTrajanja < 0 || uraTrajanja > 23 ||
                    minutaTrajanja < 0 || minutaTrajanja > 59) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljavno trajanje. Mora biti v obliki 12:34"
                    });
                } else Koledar.create ({
                    userId : zahteva.body.userId,
                    ime: zahteva.body.ime,
                    leto: zahteva.body.leto,
                    mesec: zahteva.body.mesec,
                    dan: zahteva.body.dan,
                    cas: zahteva.body.cas,
                    trajanje: zahteva.body.trajanje,
                    opis: zahteva.body.opis
                }, function(napaka, Koledar) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 400, napaka);
                    } else {
                        vrniJsonOdgovor(odgovor, 201, Koledar);
                    }
                });
            }
        });
};

module.exports.koledarDelete = function (req, res) {
    var id = req.params.idKoledar;
    if (id) {
        Koledar.findByIdAndRemove(id)
            .exec(
                function(error, product) {
                    if (error) {
                        vrniJsonOdgovor(res, 404, error);
                        return;
                    }
                    vrniJsonOdgovor(res, 204, null);
                }
            );
    } else {
        vrniJsonOdgovor(res, 400, {
            "message":
                "Vnos ni bil najden."
        });
    }
};

module.exports.koledarPosodobi = function(zahteva, odgovor) {
    var ime = zahteva.body.ime;
    var cas = zahteva.body.cas;
    var trajanje = zahteva.body.trajanje;
    var opis = zahteva.body.opis;
    var id = zahteva.params.idKoledar;

    var uraCas = parseInt(cas.substring(0,2));
    var minutaCas = parseInt(cas.substring(3,5));
    var uraTrajanja = parseInt(trajanje.substring(0,2));
    var minutaTrajanja = parseInt(trajanje.substring(3,5));

    if(!(ime.trim()))
        ime = "/";
    if(!(opis.trim()))
        opis = "/";


    if(isNaN(uraTrajanja) || isNaN(minutaTrajanja) || uraTrajanja < 0 || uraTrajanja > 23
        || minutaTrajanja < 0 || minutaTrajanja > 59) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Neveljavno trajanje. Mora biti v obliki 12:34"
        });
    } else if(isNaN(uraCas) || isNaN(minutaCas) || uraCas < 0 || uraCas > 23 || minutaCas < 0 || minutaCas > 59) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Neveljaven cas. Mora biti v obliki 12:34"
        });
    } else Koledar.findByIdAndUpdate(id,
        {
            ime: ime,
            cas: cas,
            trajanje: trajanje,
            opis: opis
        },
        {new: true},
        function (napaka, uporabnik) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 400, {"sporocilo": "Prišlo je do napake"});
            } else {
                vrniJsonOdgovor(odgovor, 200, {"sporocilo": "Uspešno posodobljen vnos v koledarju."});
            }
    });
};
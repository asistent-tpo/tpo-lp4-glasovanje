var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var avtobusSchema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    imena_postaj: {type: [String], required: true},
    id_postaj: {type: [int], required: true}
});

module.exports = avtobusSchema;

mongoose.model('Avtobus', avtobusSchema, 'avtobus');
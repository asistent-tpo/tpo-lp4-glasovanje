var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var koledarSchema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    userId : {type: Schema.Types.ObjectId, ref: 'Uporabniki'},
    ime: {type: String, required: true},
    leto: {type: Number, require: true},
    mesec: {type: Number, require: true},
    dan: {type: Number, require: true},
    cas: {type: String, require: true},
    trajanje: {type: String, require: true},
    opis: {type: String, required: true},
});

module.exports = koledarSchema;

mongoose.model('Koledar', koledarSchema, 'Koledar');

var mongoose = require('mongoose');
var crypto = require('crypto');

//var artikliShema = require("./artikli");

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var uporabnikiShema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    ime: {type: String, required: true},
    priimek: {type: String, required: true},
    email: {type: String, unique:true, required: true},
    geslo: {type: String, required: true},
    datumRegistracije: {type: Date, "default": Date.now},
    funkcija: {type: Number, "default": 2, required:true} // 1 - admin, 2 - student, 3 - event manager
});

module.exports = uporabnikiShema;

mongoose.model('Uporabnik', uporabnikiShema, 'Uporabniki');


var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');
var ctrlPrijava = require('../controllers/prijava');
var ctrlEvents = require('../controllers/events');
var ctrlBus = require('../controllers/bus');
var ctrlFood = require('../controllers/food');
var ctrlAdmin = require('../controllers/admin');

//var ctrlDb = require('../controllers/db');

/* Admin */
router.get('/admin', ctrlAdmin.admin);
router.post('/obvestilo', ctrlAdmin.posljiObvestilo);

/* Webpage tabs */
router.get('/', ctrlMain.index);

router.get('/home', ctrlMain.index);
router.get('/food', ctrlFood.food);

router.get('/prijava', ctrlPrijava.prijava);
router.get('/registracija', ctrlPrijava.registracija);

router.post('/prijava', ctrlPrijava.prijaviUporabnika);
router.post('/registracija', ctrlPrijava.registrirajUporabnika);

router.get('/uporabnik', ctrlPrijava.urejanjeUporabnika);
router.post('/spremenigeslo', ctrlPrijava.spremeniGeslo);

router.get('/db', ctrlMain.baza);
router.get('/izbrisitodo/:idTodo', ctrlMain.izbrisTodo);


/* Bus tabs */
router.get('/bus', ctrlBus.bus);
router.post('/isciBus', ctrlBus.isciBus);


router.post('/odjava', ctrlPrijava.odjaviUporabnika);



/* Event tabs */
router.get('/events', ctrlEvents.events);
router.get('/events/manager', ctrlEvents.eventsManager)
router.get('/events/manager/add', ctrlEvents.eventsAdd);
router.post('/events', ctrlEvents.eventsNew);
router.get('/events/manager/delete/:id', ctrlEvents.deleteEvent);



/* Database router */
//router.get('/db', ctrlDb.db);
//router.get('/db/deleteAll', ctrlDb.deleteAll);
//router.get('/db/init', ctrlDb.init);


module.exports = router;

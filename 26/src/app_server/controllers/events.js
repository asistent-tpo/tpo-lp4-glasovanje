var api = require("./api");
var eventsJson = require('../models/events.json');

module.exports.events = function(req, res) {
    console.log(req.originalUrl);
    api.list(req.originalUrl.replace("/", "")).then(e => {
        eventsJson.events = e.data;
        var dovoljenje = req.session.funkcija === 3;
        var podatki = {
            ime: req.session.ime,
            priimek: req.session.priimek,
            email: req.session.email,
            dovoljenje: dovoljenje
        }
        res.render('events', Object.assign({},JSON.parse(JSON.stringify(eventsJson)), JSON.parse(JSON.stringify(podatki))));
    });
};

module.exports.eventsManager = function(req, res) {
    console.log(req.originalUrl);
    api.list('events').then(e => {
        eventsJson.events = e.data;
        var dovoljenje = req.session.funkcija === 3;
        if (!dovoljenje) {
            res.redirect("/events?access=denied");
            return;
        }
        var podatki = {
            ime: req.session.ime,
            priimek: req.session.priimek,
            email: req.session.email,
            dovoljenje: dovoljenje
        }
        res.render('events_manager', Object.assign({},JSON.parse(JSON.stringify(eventsJson)), JSON.parse(JSON.stringify(podatki))));
    });
};

module.exports.eventsAdd = function(req, res) {
    var dovoljenje = req.session.funkcija === 3;
            if (!dovoljenje) {
                res.redirect("/events?access=denied");
                return;
            }
            var podatki = {
                ime: req.session.ime,
                priimek: req.session.priimek,
                email: req.session.email,
                dovoljenje: dovoljenje
            }
    res.render('events_create', eventsJson);
};

module.exports.eventsNew = function(req, res) {
    api.post("events", req.body).then(e =>{
        res.redirect('/events?uspeh=true');
    }).catch(e =>{
        res.redirect('/events?uspeh=false');
    });
};

module.exports.deleteEvent = function (req, res) {
    var dovoljenje = req.session.funkcija === 3;
            if (!dovoljenje) {
                res.redirect("/events?access=denied");
                return;
            }
            var podatki = {
                ime: req.session.ime,
                priimek: req.session.priimek,
                email: req.session.email,
                dovoljenje: dovoljenje
            }
    api.delete('events/delete', req.params.id, null)
        .then(data => {
            res.redirect('/events?uspeh=true');
        })
        .catch(error => {
            res.redirect('/events?uspeh=false');
        })
};

var axios = require('axios');

var apiParametri = {
    streznik: 'http://localhost:7000'
};
if (process.env.NODE_ENV === 'production') {
    apiParametri.streznik = 'https://tpo-straightas.herokuapp.com';
}

module.exports.list = function (resource){
    return axios.get(apiParametri.streznik+"/api/"+resource);
};

module.exports.init = function (resource){
    return axios.get(apiParametri.streznik+"/api/"+resource);
};

module.exports.get = function (resource, id){
    return axios.get(apiParametri.streznik+"/api/"+resource+"/"+id);
};

module.exports.put = function (resource, id, obj){
    return axios.put(apiParametri.streznik+"/api/"+resource+"/"+id, obj);
};

module.exports.post = function (resource, obj){
    return axios.post(apiParametri.streznik+"/api/"+resource, obj);
};

module.exports.delete = function (resource, id, obj){
    return axios.delete(apiParametri.streznik+"/api/"+resource+'/'+id, obj);
};

module.exports.deleteAll = function (resource){
    return axios.delete(apiParametri.streznik+"/api/"+resource);
};
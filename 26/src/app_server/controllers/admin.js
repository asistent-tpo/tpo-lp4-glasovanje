const request = require('request');

var port = process.env.PORT || 7000;
var apiParametri = {
    streznik: 'http://localhost:'+port
};

module.exports.admin= function(req, res) {
    if(req.session.prijavljen !== 'undefined' && req.session.prijavljen && req.session.funkcija == 1){
        res.render('admin');
    }
    else{
        res.redirect('/prijava');
    }
};

module.exports.posljiObvestilo = function(req, res) {
    var obvestilo = req.body.obvestilo;
    
    if(!(req.body.obvestilo) || req.body.obvestilo.length < 1) {
        res.render('admin', { napaka: true });
        return;
    }

    var pot = '/api/obvestilo';

    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json: {
            obvestilo: req.body.obvestilo
        }
    };

    request(parametriZahteve, function(napaka, odgovor, vsebina){
        if(vsebina.obvestilo == "" || odgovor.statusCode == 200){
            res.render('admin', { success: true });
        }
        else{
            res.render('admin', { napaka: true });
        }
    });
};
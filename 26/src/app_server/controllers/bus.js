var api = require("./api");
var busJson = require('../models/bus.json');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var jsdom = require('jsdom');
var $ = require('jquery')(new jsdom.JSDOM().window);
var DOMParser = require('domparser');

module.exports.bus = function(req, res) {
    var podatki = {
        ime: req.session.ime,
        priimek: req.session.priimek,
        email: req.session.email
    }
    res.render('bus', Object.assign(busJson, podatki));
};

module.exports.isciBus = function(req, res) {
    var podatki = {
        ime: req.session.ime,
        priimek: req.session.priimek,
        email: req.session.email
    }

    busJson['bus_info'] = [];

    api.post((req.originalUrl).replace("/", ""), req.body).then(e => {
        var stations = $(e.data).find("h1").find("a");
        var all_stations = $(e.data).find("#stations");
        for (var i = 1; i < stations.length; i++) {
            var find_fields = $(all_stations[i-1]).find("tbody").find("td");

            busJson['bus_info'].push({'ime':stations[i].innerHTML, 'postaja':[]});
            for (var j = 0; j < find_fields.length-2; j++) {
                busJson['bus_info'][i-1]['postaja'].push({'bus':$(find_fields[j]).find("a").text(), 'smer':find_fields[j+1].innerHTML, 'naslednji':find_fields[j+2].innerHTML});
                j += 2;
            }
        }
        if (all_stations.length == 0)
            busJson['bus_info'].push({'ime':"postaja ne obstaja", 'postaja':[]});

        res.render('bus', Object.assign(busJson, podatki));
    })
    .catch(e =>{
        busJson['bus_info'].push({'ime':"Postaja z imenom/id ni bila najdena!", 'postaja':[]});

        res.render('bus', Object.assign(busJson, podatki));
    });
};


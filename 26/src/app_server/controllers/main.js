var mainJson = require('../models/home.json');

const request = require('request');

var port = process.env.PORT || 7000;
var apiParametri = {
    streznik: 'http://localhost:'+port
};

module.exports.index = function(req, res) {

    var podatki = {
        ime: req.session.ime,
        priimek: req.session.priimek,
        email: req.session.email,
        idUporabnika: req.session.idUporabnika
    };

    if(req.session.prijavljen !== 'undefined' && req.session.prijavljen && (req.session.funkcija == 2 || req.session.funkcija == 3)){
        var pot = '/api/todo/'+req.session.idUporabnika;
        var parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'GET',
            json: {
                ime: req.body.ime,
                priimek: req.body.priimek,
                email: req.body.email
            }
        };

        var potObvestila = '/api/obvestila';
        var parametriZahteveObvestila = {
            url: apiParametri.streznik + potObvestila,
            method: 'GET'
        };

        request(parametriZahteve, function(napaka, odgovor, vsebina){
            // je todo
            if(odgovor.statusCode == 200){
                var todoji = {
                    todoji: vsebina
                };
                request(parametriZahteveObvestila, function(napaka2, odgovor2, vsebina2){
                    // so obvestila
                    if(odgovor2.statusCode == 200){
                        var obvestila = {
                            obvestila: vsebina2
                        };

                        console.log(JSON.stringify(obvestila.obvestila) + " so obvestila ");
                        res.render('home', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki)), JSON.parse(JSON.stringify(todoji)), JSON.parse(JSON.stringify(obvestila)) ));
                    }
                    else{
                        res.render('home', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki)), JSON.parse(JSON.stringify(todoji))));
                    }
                });

            }
            //ni TODO
            else{
                request(parametriZahteveObvestila, function(napaka2, odgovor2, vsebina2){
                    // so obvestila
                    if(odgovor2.statusCode == 200){
                        var obvestila = {
                            obvestila: vsebina2
                        };
                        res.render('home', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki)), JSON.parse(JSON.stringify(obvestila))) );
                    }
                    else{
                        res.render('home', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki))));
                    }
                });

            }
        });
    } else if(req.session.prijavljen !== 'undefined' && req.session.prijavljen && (req.session.funkcija == 1)){
        /*
        var pot = '/api/todo/'+req.session.idUporabnika;
        var parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'GET',
            json: {
                ime: req.body.ime,
                priimek: req.body.priimek,
                email: req.body.email
            }
        };

        request(parametriZahteve, function(napaka, odgovor, vsebina){
            if(odgovor.statusCode == 200){
                var todoji = {
                    todoji: vsebina
                }
                res.redirect('admin', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki)), JSON.parse(JSON.stringify(todoji))));
            }
            else{
                res.redirect('admin', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki))));
            }
        });*/
        res.redirect('/admin');
    }
    else{
        res.render('home-gost', Object.assign({}, JSON.parse(JSON.stringify(mainJson)), JSON.parse(JSON.stringify(podatki))));
    }

};

module.exports.izbrisTodo = function(req,res){
    var pot = '/api/todo/'+req.params.idTodo;
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'DELETE',
        json: {
        }
    };

    request(parametriZahteve, function(napaka, odgovor, vsebina){

        if(odgovor.statusCode == 204){
            res.redirect('/');

        }
        else{
            res.render('genericno-besedilo', {
                vsebina: "prišlo je do napake pri brisanju todo vnosa."
            });
        }

    });


}


module.exports.baza = function(req, res) {
    res.render('baza', {
        port: process.env.PORT || 7000
    });
};

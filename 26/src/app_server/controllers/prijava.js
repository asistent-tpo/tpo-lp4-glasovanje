const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const request = require('request');

var port = process.env.PORT || 7000;
var apiParametri = {
    streznik: 'http://localhost:'+port
};

module.exports.prijava= function(req, res) {
    res.render('prijava');
};

module.exports.registracija= function(req, res) {
    res.render('registracija');
};

module.exports.prijaviUporabnika= function(req, res) {
    var email = req.body.email;
    var geslo = req.body.geslo;


    var pot = '/api/prijava';

    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json: {
            email: req.body.email,
            geslo: req.body.geslo
        }
    };

    request(parametriZahteve, function(napaka, odgovor, vsebina){

        if(odgovor.statusCode == 200){
            //prijavi uporabnika
            //console.log("prijavljam uporabnika" + JSON.stringify(vsebina));


            req.session.prijavljen = true;
            req.session.idUporabnika = vsebina.id;
            req.session.ime = vsebina.ime;
            req.session.priimek= vsebina.priimek;
            req.session.email = vsebina.email;
            req.session.funkcija = vsebina.funkcija;

            console.log(vsebina.funkcija + " je funkcija");

            if(req.session.funkcija == 2) {
                res.redirect('/');
            } else if(req.session.funkcija == 1) {
                res.redirect('/admin');
            }else if(req.session.funkcija == 3){
                res.redirect('/events')
            }
        }
        else{
            res.render('prijava', { napaka: true });
            //res.redirect('/prijava');
        }
    });

};

module.exports.odjaviUporabnika= function(req, res) {

    req.session.prijavljen = false;
    req.session.idUporabnika = undefined;
    req.session.email = undefined;
    req.session.ime = undefined;
    req.session.priimek = undefined;

    req.session.destroy(function(err){

    });

    res.redirect('/prijava');

};

module.exports.registrirajUporabnika= function(req, res) {

    if(!(req.body.ime) || !(req.body.priimek) || !(req.body.email) ) {
        res.render('genericno-besedilo', {
            vsebina: 'Prosimo vnesite vse zahtevane podatke!'
        });
        return;
    }

    if(req.body.geslo !== req.body.geslo_potrdi) {
        res.render('genericno-besedilo', {
            vsebina: 'Gesli se ne ujemata!'
        });
        return;
    }

    if(!(req.body.geslo) || req.body.geslo.length < 8) {
        res.render('genericno-besedilo', {
            vsebina: 'Geslo mora vsebovati vsaj osem znakov.'
        });
        return;
    }

    var pot = '/api/uporabniki';
    let hash = bcrypt.hashSync(req.body.geslo, 10);

    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json: {
            ime: req.body.ime,
            priimek: req.body.priimek,
            email: req.body.email,
            geslo: hash
        }
    };

    module.exports.post = function (resource, obj){
        return axios.post(apiParametri.streznik+"/api/"+resource, obj);
    };

    request(parametriZahteve, function(napaka, odgovor, vsebina){
        if(odgovor.statusCode != 201){
            res.render('genericno-besedilo', {
                vsebina: 'Napaka ' + odgovor.statusCode + ":" + vsebina.sporocilo + " ... " + napaka
            });
            return;
        }
        res.render('genericno-besedilo', {
            vsebina: "Uporabnik uspešno dodan."
        });

    });
/*
    res.render('genericno-besedilo', {
        vsebina: 'Registracija še ni implemenitrana.'
    });
*/
};

module.exports.urejanjeUporabnika = function(req, res){

    res.render('urejanjepodatkov',
        {
            ime: req.session.ime,
            priimek: req.session.priimek,
            email: req.session.email
        }
        );

};

module.exports.spremeniGeslo = function(req, res){

    if(req.body.geslonovo !== req.body.geslonovo_potrdi){
        res.render('urejanjepodatkov.pug',{napaka: true})
    }else{

        var pot = '/api/spremenigeslo';
        var parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'POST',
            json: {
                email: req.session.email,
                novoGeslo: req.body.geslonovo
            }
        };

        var potPrijava = '/api/prijava';
        var parametriZahtevePrijava = {
            url: apiParametri.streznik + potPrijava,
            method: 'POST',
            json: {
                email: req.session.email,
                geslo: req.body.geslostaro
            }
        };

        //console.log(JSON.stringify(parametriZahteve) + " v kontrolerju");

        //preveri geslo
        request(parametriZahtevePrijava, function(napaka, odgovor, vsebina){
            if(odgovor.statusCode == 200){

                //spremeni geslo
                request(parametriZahteve, function(napaka, odgovor, vsebina){

                    if(napaka){
                        res.render('genericno-besedilo', {
                            vsebina: "Prišlo je do napake pri spreminjanju gesla."
                        });
                    }
                    else{
                        res.redirect('/');
                    }
                });
            }
            else{
                res.render('urejanjepodatkov.pug',{napaka: true})
            }
        });
    }

};
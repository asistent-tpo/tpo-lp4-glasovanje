var map;
var service;
var infowindow;

console.log("tralalalal food.js");

function createMarker(place) {

    new google.maps.Marker({
        position: place.geometry.location,
        map: map
    });
}


function initMap() {
    console.log("notriiii V INITMAP");
    var lokacija = new google.maps.LatLng(46.056946,14.505751);

    map = new google.maps.Map(document.getElementById('map'), {
        center: lokacija,
        zoom: 15
    });

    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(function (position) {
            lokacija = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(lokacija);
            pridobiRestavracije(lokacija);
        },function(error){
            pridobiRestavracije(lokacija);
        });
    }else{
        pridobiRestavracije(lokacija);
    }

}

function pridobiRestavracije(lok){
    var request = {
        location: lok,
        radius: '1500',
        type: ['restaurant']
    };
    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, callback);
}

function callback(results, status) {
    console.log("sem že v callbacku");

    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            var place = results[i];
            createMarker(results[i]);
        }
    }
}
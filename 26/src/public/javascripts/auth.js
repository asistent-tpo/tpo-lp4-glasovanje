
var userId = "userId";
var userEmail = "userEmail";

var setAuth = function () {
    let id = $('#userId').val();
    let email = $('#userEmail').val();

    // console.log("Set auth");
    if (id != null && id !== "") {
        window.localStorage.setItem(userId, id);
        console.log("local storage for user id updated");
    }
    if (email != null && email !== "") {
        window.localStorage.setItem(userEmail, email);
        console.log("local storage for email updated");
    }
};

var logOut = function() {
    console.log("LogOut");
    window.localStorage.removeItem(userId);
    window.localStorage.removeItem(userEmail);
};

var welcomeUser = function () {
    let id = window.localStorage.getItem(userId);
    let email = window.localStorage.getItem(userEmail);
    if (email != null && email !== "") {
        console.log("welcome ", email);
        let text = $('#welcome').text().toLowerCase();
        $('#welcome').text(email + " " + text);
        $('#jumbo').append('<a class="btn btn-default" id="user" href="/user/'+id+'"><img src="/images/settings.svg"  height="30" width="30"> ' + "</a>");
        // $('#user').text(email);
    }
};

window.onload = function () {
    setAuth();
    welcomeUser();
};

var getUserEmail = function () {
    return window.localStorage.getItem(userEmail);
};

var getUserId = function () {
    return window.localStorage.getItem(userId);
};

$(document).ready(function(){

    $("#create-todo").on("click", function() {

        $("#todo-create-modal").modal();

        $("#create-todo-button").on("click", function(){

            var uid = $("input[name=userId]").val();
            var opis = $("#todo-create-opis").val();
            var mejl = $("input[name=email]").val();

            $("#todo-create-modal").modal("hide");

            $.ajax({
                type: 'POST',
                url: url + "/api/todo/"+uid,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    "userId": uid,
                    "opis": opis,
                    "email": mejl
                }),
                success: function( data, textStatus, jQxhr ){

                    $("#info-modal-body").text("Vnos ustvarjen.");

                    $("#todo-create-modal").modal("hide");
                    location.reload();
                },
                error: function( jqXhr, textStatus, errorMessage ){
                    $("#info-modal-body").text('Napaka: ' + errorMessage);
                    document.querySelectorAll("#exit-info-modal")
                        .forEach(function(btn) {
                            $(btn).off();
                            $(btn).on("click", function() {
                                $("#info-modal").modal("hide");
                            });
                        });
                    $("#todo-create-modal").modal("hide");
                    $("#info-modal").modal();
                }
            });

        });

    });



});


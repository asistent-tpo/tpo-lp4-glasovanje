
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

/* global $ */
$('#loginForm').submit(function(dogodek) {
    $('.alert.alert-danger').remove();
    if (!$('input#email_login').val() || !$('input#password').val()) {
        $(this).prepend('<div role="alert" class="alert alert-danger">' +
            'Prosim izpolnite vsa vnosna polja!' + '</div>');
        return false;
    } else if (!validateEmail($('input#email_login').val())) {
        $(this).prepend('<div role="alert" class="alert alert-danger">' +
            'Prosim vnesite veljaven E-poštni naslov!' + '</div>');
        return false;
    }
});

$('#registerForm').submit(function(dogodek) {
    $('.alert.alert-danger').remove();
    let isCorrect = true;
    if (!$('input#email_register').val() || !$('input#password1').val() ||  !$('input#password2').val()) {
        $(this).prepend('<div role="alert" class="alert alert-danger">' +
            'Prosim izpolnite vsa vnosna polja!' + '</div>');
        isCorrect = false;
    }  else {
        if ($('input#password1').val() !== $('input#password2').val()) {
            $(this).prepend('<div role="alert" class="alert alert-danger">' +
                'Gesli se ne ujemata!' + '</div>');
            isCorrect = false;
        }
        if (!validateEmail($('input#email_register').val())) {
            $(this).prepend('<div role="alert" class="alert alert-danger">' +
                'Prosim vnesite veljaven E-poštni naslov!' + '</div>');
            isCorrect = false;
        }
    }
    return isCorrect;
});

$('#newsForm').submit(function(dogodek) {
    $('.alert.alert-danger').remove();
    if (!$('input#title').val() || !$('textarea#text').val()) {
        $(this).prepend('<div role="alert" class="alert alert-danger">' +
            'Prosim izpolnite naslov in vsebino!' + '</div>');
        return false;
    }
});

$('#productNew').submit(function(dogodek) {
    $('.alert.alert-danger').remove();
    if (!$('input#title').val() || !$('textarea#description').val() || !$('input#price').val()) {
        $(this).prepend('<div role="alert" class="alert alert-danger">' +
            'Prosim izpolnite naslov, vsebino ter ceno izdelka!' + '</div>');
        return false;
    }
});

$('#userForm').submit(function(dogodek) {
    $('.alert.alert-danger').remove();

    // Če so vsi prazni, je kul
    if (!$('input#state').val()
        && !$('textarea#municipality').val()
        && !$('textarea#city').val()
        && !$('textarea#postcode').val()
        && !$('textarea#street').val()
        && !$('input#house_number').val()) {
        return true;
    }

    if (!$('input#state').val()
            || !$('input#municipality').val()
            || !$('input#city').val()
            || !$('input#postcode').val()
            || !$('input#street').val()
            || !$('input#house_number').val()) {

        $(this).prepend('<div role="alert" class="alert alert-danger">' +
            'Prosim izpolnite vsa polja za naslov!' + '</div>');
        return false;

    }
});

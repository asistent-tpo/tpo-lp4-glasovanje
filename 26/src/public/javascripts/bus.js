var map;
var service;
var infowindow;

var stations_closest;

function initMap() {
  var lat = 46.056946;
  var lon = 14.505751;

  initialLocation = new google.maps.LatLng(lat, lon);

  map = new google.maps.Map(document.getElementById('map'), {
      center: initialLocation,
      zoom: 13
  });

  if (navigator.geolocation) {
       navigator.geolocation.getCurrentPosition(function (position) {
           initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
           lat = position.coords.latitude;
           lon = position.coords.longitude;
           map.setCenter(initialLocation);
           pridobiAvtobuse(initialLocation);
       },function(error){
            pridobiAvtobuse(initialLocation);
       });
  }else{
      pridobiAvtobuse(initialLocation);
  }
}

function pridobiAvtobuse(lok){
    var request = {
        location: lok,
        radius: '3000',
        type: ['bus_station']
    };
    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, callback);
}

function createMarker(place) {
    new google.maps.Marker({
        position: place.geometry.location,
        map: map
    });
}

var station_names = [];
function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            var place = results[i];
            station_names[i] = place['name'];
            var html = "<a onclick=\"selected(\'"+station_names[i]+"\')\">"+station_names[i]+"</a>";
            $('#predloge').append(html);
            createMarker(results[i]);
        }
    }
}

function selected(name) {
    console.log($('#myInput').val(name));
    myFunction();
    $('#imunique').trigger('click');
}

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
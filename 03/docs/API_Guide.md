# API Guide

## Uporabniške vloge
Uporabniki imajo različne stopnje privilegijev. 

| #    | Naziv                 |
| ---- | --------------------- |
| 0    | Registriran uporabnik |
| 50   | Upravljalec z dogodki |
| 100  | Admin                 |


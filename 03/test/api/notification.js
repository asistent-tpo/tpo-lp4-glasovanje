const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app');
const User = require('../../src/app_api/models/db').User;
const SystemNotification = require('../../src/app_api/models/db').SystemNotification;
const sequelize = require('../../src/app_api/models/db').sequelize;
const permissionLevels = require('../../src/app_api/util/permissionLevels');

chai.use(chaiHttp);
chai.should();

let user = {
    normalUser: {
        email: "user@test.si",
        password: "123",
        active: true
    },
    adminUser: {
        email: "admin@test.si",
        password: "123",
        active: true,
        permissionLevel: permissionLevels.ADMINISTRATOR
    },
    editorUser: {
        email: "editor@test.si",
        password: "1234",
        active: true,
        permissionLevel: permissionLevels.EVENT_EDITOR
    }
};

let notifications = [{
    notification : "Test notification 1",
    active : true
}, {
    notification : "Test notification 2",
    active : false
}, {
    notification : "Test notification 3"
}];

describe("Notifications", () => {
    before(async () => {
        await sequelize.sync();
    });

    beforeEach(async () => {
        let u = new User();
        u.email = user.normalUser.email;
        u.active = user.normalUser.active;
        u.setPassword(user.normalUser.password);
        await u.save();

        user.normalUser.authToken = u.generateToken();

        let u2 = new User();
        u2.email = user.adminUser.email;
        u2.active = user.adminUser.active;
        u2.setPassword(user.adminUser.password);
        u2.permissionLevel = user.adminUser.permissionLevel;
        await u2.save();

        user.adminUser.authToken = u2.generateToken();

        let u3 = new User();
        u3.email = user.editorUser.email;
        u3.active = user.editorUser.active;
        u3.setPassword(user.editorUser.password);
        u3.permissionLevel = user.editorUser.permissionLevel;
        await u3.save();

        user.editorUser.authToken = u3.generateToken();

        let n1 = new SystemNotification();
        n1.notification = notifications[0].notification;
        n1.active = notifications[0].active;
        await n1.save();

        notifications[0].id = n1.id;

        let n2 = new SystemNotification();
        n2.notification = notifications[1].notification;
        n2.active = notifications[1].active;
        await n2.save();

        notifications[1].id = n2.id;
    });

    afterEach(async () => {
        await SystemNotification.destroy({where : {}});
        await User.destroy({where : {}});
    });

    describe("GET /notifications", () => {
       it("As admin", (done) => {
           chai.request(app)
               .get("/api/notifications")
               .set("Authorization", "Bearer " + user.adminUser.authToken)
               .end((err, res) => {
                   if(err) done(err);

                   res.should.have.status(200);
                   res.body.should.be.a('array');
                   res.body.length.should.equal(1);
                   res.body[0].should.have.property('notification');
                   res.body[0].should.haveOwnProperty('active');


                   done();
               });
       });

        it("As normal", (done) => {
            chai.request(app)
                .get("/api/notifications")
                .set("Authorization", "Bearer " + user.normalUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property('notification');
                    res.body[0].should.haveOwnProperty('active');


                    done();
                });
        });

        it("As editor", (done) => {
            chai.request(app)
                .get("/api/notifications")
                .set("Authorization", "Bearer " + user.editorUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property('notification');
                    res.body[0].should.haveOwnProperty('active');

                    done();
                });
        });

        it("No auth", (done) => {
            chai.request(app)
                .get("/api/notifications")
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);

                    done();
                });
        });

        it("All", (done) => {
            chai.request(app)
                .get("/api/notifications?all=true")
                .set("Authorization", "Bearer " + user.normalUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(2);

                    done();
                });
        });

    });

    describe("POST /notifications", () => {
       it("As admin", done => {
           chai.request(app)
               .post("/api/notifications")
               .set("Authorization", "Bearer " + user.adminUser.authToken)
               .send(notifications[2])
               .end((err, res) => {
                   if(err) done(err);

                   res.should.have.status(201);
                   res.body.should.be.a('object');

                   done();
           });
       });

        it("As editor", done => {
            chai.request(app)
                .post("/api/notifications")
                .set("Authorization", "Bearer " + user.editorUser.authToken)
                .send(notifications[2])
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);

                    done();
                });
        });

        it("As normalUser", done => {
            chai.request(app)
                .post("/api/notifications")
                .set("Authorization", "Bearer " + user.editorUser.authToken)
                .send(notifications[2])
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);

                    done();
                });
        });

        it("As no user", done => {
            chai.request(app)
                .post("/api/notifications")
                .send(notifications[2])
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);

                    done();
                });
        });

        it("No data", done => {
            chai.request(app)
                .post("/api/notifications")
                .set("Authorization", "Bearer " + user.adminUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);

                    done();
                });
        });
    });

    describe("PATCH /notifications", () => {
        it("As admin", done => {
            chai.request(app)
                .patch("/api/notifications/" + notifications[0].id)
                .set("Authorization", "Bearer " + user.adminUser.authToken)
                .send(notifications[1])
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);

                    done();
                });
        });

        it("Nonexistant", done => {
            chai.request(app)
                .patch("/api/notifications/0")
                .set("Authorization", "Bearer " + user.adminUser.authToken)
                .send(notifications[1])
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(402);

                    done();
                });
        });

        it("Empty", done => {
            chai.request(app)
                .patch("/api/notifications/0")
                .set("Authorization", "Bearer " + user.adminUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);

                    done();
                });
        });
    });

    describe("DELETE /notifications", () => {
        it("As admin", done => {
            chai.request(app)
                .delete("/api/notifications/" + notifications[0].id)
                .set("Authorization", "Bearer " + user.adminUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('deleted');
                    res.body.deleted.should.equal(true);

                    done();
                });
        });

        it("Nonexistant", done => {
            chai.request(app)
                .delete("/api/notifications/0")
                .set("Authorization", "Bearer " + user.adminUser.authToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('deleted');
                    res.body.deleted.should.equal(false);

                    done();
                });
        });
    });
});
var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../../app');


chai.use(chaiHttp);
let should = chai.should();

describe("Restaurants", () => {

    it("Get all restaurants A-Z", (done) => {
        chai.request(app)
            .get("/api/restaurants")
            .end((err, res) => {
                
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.should.not.be.empty;

                res.body.forEach(item => {
                    
                    ["lat", "lon", "naslov", "cena", "doplacilo", 
                    "posid", "detailslink", "lokal", "city"].forEach((key) =>{
                        item.should.have.property(key)
                        item[key].should.be.a("string");
                    });

                });
                
                done();
        })
    });

    it("Get near restaurants", (done) => {
        chai.request(app)
            .get("/api/restaurants/46.07421/14.51732")
            .end((err, res) => {
                
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.should.not.be.empty;

                res.body.forEach(item => {
                    
                    // strings
                    ["lat", "lon", "naslov", "cena", "doplacilo", 
                    "posid", "detailslink", "lokal", "city"].forEach((key) =>{
                        item.should.have.property(key)
                        item[key].should.be.a("string");
                    });

                    // nums
                    ["distance"].forEach(key => {
                        item.should.have.property(key);
                        item[key].should.be.a("number");
                    });

                });

                done();
        })
    });

});
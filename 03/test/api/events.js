var chai = require('chai');
var assert = chai.assert;
var chaiHttp = require('chai-http');
var app = require('../../app');
const User = require('../../src/app_api/models/db').User;
const Event = require('../../src/app_api/models/db').Event;
const permissionLevels = require('../../src/app_api/util/permissionLevels');
const sequelize = require('../../src/app_api/models/db').sequelize;

chai.use(chaiHttp);
let should = chai.should();

let user = {
    normalUser: {
        email: "user@test.si",
        password: "123",
        active: true
    },
    adminUser: {
        email: "admin@test.si",
        password: "123",
        active: true,
        permissionLevel: permissionLevels.ADMINISTRATOR
    },
    editorUser: {
        email: "editor@test.si",
        password: "1234",
        active: true,
        permissionLevel: permissionLevels.EVENT_EDITOR
    }
};

let tokens = {
    normalUser: "",
    adminUser: "",
    editorUser: ""
}

var updateOrCreate = function(userKey) {

    return User.findOne({where: {email: user[userKey].email}}).then(item => {
        
        var u = item;
        u.setPassword(user[userKey].password)
        u.active = user[userKey].active;

        if(user[userKey].permissionLevel){
            u.permissionLevel = user[userKey].permissionLevel;
        }

        return u.save();

    }).catch(() => {

        var u = new User();
        u.email = user[userKey].email;
        u.setPassword(user[userKey].password);
        u.active = user[userKey].active;

        if(user[userKey].permissionLevel){
            u.permissionLevel = user[userKey].permissionLevel;
        }

        return u.save();

    }).then(u => {

        tokens[userKey] = u.generateToken();

    });

}

var clearData = function(userKey, done) {
    return User.destroy({
        where: {
            email: user[userKey].email
        }
    })
    .then(() => {
        if(done) done();
    });
}

var checkEventBody = function(res){
    
    res.body.should.have.property("id");
    res.body.id.should.be.a("number");

    res.body.should.have.property("name");
    res.body.name.should.be.a("string");

    res.body.should.have.property("date");
    res.body.date.should.be.a("string");

    res.body.should.have.property("organizer");
    res.body.organizer.should.be.a("string");

    res.body.should.have.property("description");
    res.body.description.should.be.a("string");

}

describe("Events tests", () => {

    let num = 0;
    
    before(done => {

        sequelize.sync()
        .then(() => { return updateOrCreate("normalUser") })
        .then(() => { return updateOrCreate("editorUser") })
        .then(() => { return updateOrCreate("adminUser")  })
        .then(() => { done() });

    });

    after(done => {

        clearData("normalUser")
        .then(() => { return clearData("editorUser") })
        .then(() => { return clearData("adminUser")  })
        .then(() => { done() });

    })

    it("[normalUser] Events fetch success at the start", (done) => {
        chai.request(app)
            .get("/api/events")
            .set("Authorization", "Bearer "+tokens.normalUser)
            .end((err, res) => {
                if(err) done(err);
                
                res.should.have.status(200);
                res.body.should.be.a('array');
                num = res.body.length;
                done();
            });
    });

    it("[editorUser] Events fetch success at the start", (done) => {
        chai.request(app)
            .get("/api/events")
            .set("Authorization", "Bearer "+tokens.editorUser)
            .end((err, res) => {
                if(err) done(err);

                res.should.have.status(200);
                res.body.should.be.a('array');
                num = res.body.length;
                done();
            });
    });

    it("[adminUser] Events fetch success at the start", (done) => {
        chai.request(app)
            .get("/api/events")
            .set("Authorization", "Bearer "+tokens.adminUser)
            .end((err, res) => {
                if(err) done(err);
                
                res.should.have.status(200);
                res.body.should.be.a('array');
                num = res.body.length;
                done();
            });
    });

    let ids = {};

    // Normal user: fail
    it("[normalUser] Event create - should fail due to insufficient privileges ", (done) => {
        chai.request(app)
            .post("/api/events")
            .set("Authorization", "Bearer "+tokens.normalUser)
            .send({
                    "date": new Date().toISOString(),
                    "name": "Večerja "+(new Date().getTime()),
                    "organizer": "Janez Velkavrh",
                    "description": "blue",
            })
            .end((err, res) => {
                if(err) done(err);

                res.should.have.status(401);
                res.body.should.be.a('object');
                res.body.should.have.property("error");
                res.body.error.should.equal("Insufficient permissions");

                ids.normalUser = res.body.id;
    
                done();
            })
    });

    it("[editorUser] Event create - successful ", (done) => {
        chai.request(app)
            .post("/api/events")
            .set("Authorization", "Bearer "+tokens.editorUser)
            .send({
                    "date": new Date().toISOString(),
                    "name": "Večerja "+(new Date().getTime()),
                    "organizer": "Janez Velkavrh",
                    "description": "blue",
            })
            .end((err, res) => {
                if(err) done(err);

                res.should.have.status(200);
                res.body.should.be.a('object');
                checkEventBody(res);
                ids.editorUser = res.body.id;
                done();
            })
    });

    it("[adminUser] Event create - successful ", (done) => {
        chai.request(app)
            .post("/api/events")
            .set("Authorization", "Bearer "+tokens.adminUser)
            .send({
                    "date": new Date().toISOString(),
                    "name": "Večerja "+(new Date().getTime()),
                    "organizer": "Janez Velkavrh",
                    "description": "blue",
            })
            .end((err, res) => {
                if(err) done(err);
                
                res.should.have.status(200);
                res.body.should.be.a('object');
                checkEventBody(res);
                ids.adminUser = res.body.id;
                done();
            })
    });
    
    let newText = "[TEST] A new todo CHANGE!";

    it("[editorUser] Event update", (done) => {

        chai.request(app)
            .patch("/api/events/"+ids.editorUser)
            .set("Authorization", "Bearer " + tokens.editorUser)
            .send({
                name: newText
            })
            .end((err, res) => {
                if(err) done(err);
                
                checkEventBody(res);
                res.body.should.have.property("changed", true);
                done();
            });
    });

    it("[adminUser] Event update", (done) => {

        chai.request(app)
            .patch("/api/events/"+ids.adminUser)
            .set("Authorization", "Bearer " + tokens.adminUser)
            .send({
                name: newText
            })
            .end((err, res) => {
                if(err) done(err);

                checkEventBody(res);
                res.body.should.have.property("changed", true);
                done();
            });
    });


    it("[editorUser] Event delete", (done) => {
        chai.request(app)
            .delete("/api/events/"+ids.editorUser)
            .set("Authorization", "Bearer "+tokens.editorUser)
            .end((err, res) => {
                if(err) done(err);

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("deleted", true);
                done();
            })
    });

    it("[adminUser] Event delete", (done) => {
        chai.request(app)
            .delete("/api/events/"+ids.adminUser)
            .set("Authorization", "Bearer "+tokens.adminUser)
            .end((err, res) => {
                if(err) done(err);

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("deleted", true);
                done();
            })
    });


    it("There is the same number of events as in the beginning", (done) => {
        chai.request(app)
            .get("/api/events")
            .set("Authorization", "Bearer "+tokens.normalUser)
            .end((err, res) => {
                if(err) { done(err); }

                res.should.have.status(200);
                res.body.should.be.a('array');        
                res.body.should.have.lengthOf(num);
                done();
            });
    });


});

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app');
const User = require('../../src/app_api/models/db').User;
const Activation = require('../../src/app_api/models/db').Activation;
const sequelize = require('../../src/app_api/models/db').sequelize;

chai.use(chaiHttp);
chai.should();


let invalidData = {
    "email": "invalid@invalid.invalid",
    "password": "invalid"
};

let invalidPassword = {
    "email": "a@b.si",
    "password": "pard"
};

let testUserdata = {
    "email": "test@test.si",
    "password": "password"
};

let existingActive = {
    "email": "a@b.si",
    "password": "password",
    "active" : true
};

let existingInactive1 = {
    "id" : -1,
    "email": "a@c.si",
    "password": "password",
    "active" : false
};

let activation1 = {
    userId : existingInactive1.id,
    hash : "abcdf",
};

let existingInactive2 = {
    "id" : -2,
    "email": "a@k.si",
    "password": "password",
    "active" : false
};

let activation2 = {
    userId : existingInactive2.id,
    hash : "agsdb",
};

let loginToken = "";

describe("Users", () => {

    before(async () => {
       await sequelize.sync();
    });

    beforeEach(async () => {
        let u = new User();
        u.email = existingActive.email;
        u.active = existingActive.active;
        u.setPassword(existingActive.password);
        await u.save();

        loginToken = u.generateToken();

        let u2 = new User();
        u2.id = existingInactive1.id;
        u2.email = existingInactive1.email;
        u2.active = existingInactive1.active;
        u2.setPassword(existingInactive1.password);
        await u2.save();

        let d = new Date();
        let a1 = new Activation();
        a1.userId = activation1.userId;
        a1.hash = activation1.hash;
        d.setDate(d.getDate() + 1);
        a1.valid = d;
        await a1.save();

        let u3 = new User();
        u3.id = existingInactive2.id;
        u3.email = existingInactive2.email;
        u3.active = existingInactive2.active;
        u3.setPassword(existingInactive2.password);
        await u3.save();

        d = new Date();
        let a2 = new Activation();
        a2.userId = activation2.userId;
        a2.hash = activation2.hash;
        d.setDate(d.getDate() - 2);
        a2.valid = d;
        await a2.save();
    });

    afterEach(async () => {
        await Activation.destroy({where : {}});
        await User.destroy({where : {}});
    });

    describe("POST /login", () => {
       it("Login empty fields", (done) => {
           chai.request(app)
               .post("/api/users/login")
               .end((err, res) => {
                   if(err) done(err);

                  res.should.have.status(400);
                  res.body.should.be.a('object');
                  res.body.should.have.property("msg");
                  res.body.msg.should.equal("Missing field email or password");
                  done();
               });
       });
        it("Login invalid email", (done) => {
            chai.request(app)
                .post("/api/users/login")
                .send(invalidData)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Invalid email or password");
                    done();
                });
        });
        it("Login invalid password", (done) => {
            chai.request(app)
                .post("/api/users/login")
                .send(invalidPassword)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Invalid email or password");
                    done();
                });
        });
        it("Login active", (done) => {
            chai.request(app)
                .post("/api/users/login")
                .send(existingActive)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property("authToken");
                    done();
                });
        });
        it("Login inactive", (done) => {
            chai.request(app)
                .post("/api/users/login")
                .send(existingInactive1)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal('Email not activated');
                    done();
                });
        });
    });
    describe("POST /register", () => {
        it("Register empty fields", (done) => {
            chai.request(app)
                .post("/api/users/register")
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Missing field email or password");
                    done();
                });
        });
        it("Register", (done) => {
            chai.request(app)
                .post("/api/users/register")
                .send(testUserdata)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("User added");
                    done();
                });
        });
        it("Register existing", (done) => {
            chai.request(app)
                .post("/api/users/register")
                .send(existingActive)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("User exists");
                    done();
                });
        });
        it("Register short password", (done) => {
            testUserdata.password = "1";
            chai.request(app)
                .post("/api/users/register")
                .send(testUserdata)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Password too short");
                    done();
                });
        });
    });

    describe("GET /activate", () => {
        it("Activation valid", (done) => {
            chai.request(app)
                .get("/api/users/activate/" + activation1.hash)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property("authToken");
                    done();
                });
        });

        it("Activation invalid", (done) => {
            chai.request(app)
                .get("/api/users/activate/" + activation2.hash)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Invalid activation");
                    done();
                });
        });
    });

    describe("POST /changePassword", () => {
        it("Change no passwd", (done) => {
            chai.request(app)
                .post("/api/users/changePassword")
                .set("Authorization", "Bearer " + loginToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Missing password field");
                    done();
                });
        });
        it("Change passwd", (done) => {
            chai.request(app)
                .post("/api/users/changePassword")
                .send({password : "itsADifferentPass"})
                .set("Authorization", "Bearer " + loginToken)
                .end((err, res) => {
                    if(err) done(err);

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property("msg");
                    res.body.msg.should.equal("Password changed");
                    done();
                });
        });
    });
});
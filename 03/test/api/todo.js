// Preberi TODO-je

// Dodaj nov TODO

// Še enkrat preberi TODO-je
// Poglej kateri ID-ji so v novem setu
// Poglej, če so vsi, ki so v originalnem in če je tudi novi
// Izbriši novega


var chai = require('chai');
var assert = chai.assert;
var chaiHttp = require('chai-http');
var app = require('../../app');
const User = require('../../src/app_api/models/db').User;
const Todo = require('../../src/app_api/models/db').ToDo;
const sequelize = require('../../src/app_api/models/db').sequelize;

chai.use(chaiHttp);
chai.should();

let theUser = {
    email: "user@test.si",
    password: "123",
    active: true
}

describe("TODO Basic operations", () => {
    
    let num = 0;
    let token = "";

    before(async () => {
        await sequelize.sync();

        var u = new User();
        u.email = theUser.email;
        u.setPassword(theUser.password);
        u.active = theUser.active;
        await u.save();

        token = u.generateToken();
    });

    after(async () => {

        await  Todo.destroy({where: {}});

        await User.destroy({where : {}});

    });

    it("TODOs fetch success at the start", (done) => {
        chai.request(app)
            .get("/api/todo")
            .set("Authorization", "Bearer "+token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                
                num = res.body.length;

                done();
        })/*.catch(e => {
            assert(false, "ERROR RETRIEVE: "+e.message);
        });*/
    });

    let id = -1;

    it("TODOs create", (done) => {
    chai.request(app)
        .post("/api/todo")
        .set("Authorization", "Bearer "+token)
        .send({
            text: "[TEST] A new todo ("+(new Date().getTime())+")"
        })
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            
            res.body.should.have.property("id");
            res.body.id.should.be.a("number");

            res.body.should.have.property("name");
            res.body.name.should.be.a("string");

            res.body.should.have.property("userId");
            res.body.userId.should.be.a("number");

            id = res.body.id;

            done();
        })/*.catch(e => {
            assert(false,"ERROR CREATE: "+e.message);
        });*/
    });

    it("TODOs update", (done) => {

        let newText = "[TEST] A new todo CHANGE!";

        chai.request(app)
            .patch("/api/todo/"+id)
            .set("Authorization", "Bearer "+token)
            .send({
                text: newText
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                
                res.body.should.have.property("id");
                res.body.id.should.be.a("number");

                res.body.should.have.property("name", newText);

                res.body.should.have.property("userId");
                res.body.userId.should.be.a("number");

                res.body.should.have.property("userId");
                res.body.userId.should.be.a("number");

                res.body.should.have.property("changed", true);

                done();
            });/*.catch(e => {
                assert(false,"ERROR UPDATE: "+e.message);
            });*/
        });


    it("TODOs delete", (done) => {
        chai.request(app)
            .delete("/api/todo/"+id)
            .set("Authorization", "Bearer "+token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');

                res.body.should.have.property("deleted", true);

                done();
            })/*.catch(e => {
                assert(false,"ERROR DELETE: "+e.message);
            });;*/
    });

    it("TODO has same number of entries as in the beginning", (done) => {
        chai.request(app)
            .get("/api/todo")
            .set("Authorization", "Bearer "+token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                
                res.body.should.have.lengthOf(num);

                done();
            })/*.catch(e => {
                assert(false, "ERROR RETRIEVE 2: "+e.message);
            });;*/
    });


});
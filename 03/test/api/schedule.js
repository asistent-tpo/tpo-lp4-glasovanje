var chai = require('chai');
var assert = chai.assert;
var chaiHttp = require('chai-http');
var app = require('../../app');
const User = require('../../src/app_api/models/db').User;
const Schedule = require('../../src/app_api/models/db').Schedule;
const sequelize = require('../../src/app_api/models/db').sequelize;


chai.use(chaiHttp);
let should = chai.should();

let theUser = {
    email: "user@test.si",
    password: "123",
    active: true
}

describe("Schedule Basic operations", () => {

    let num = 0;
    let token = "";


    before(async () => {
        await sequelize.sync();

        var u = new User();
        u.email = theUser.email;
        u.setPassword(theUser.password);
        u.active = theUser.active;
        await u.save();

        token = u.generateToken();
    });

    after(async () => {

        await  Schedule.destroy({where: {}});

        await User.destroy({where : {}});

    });

    it("Schedule entries fetch success at the start", (done) => {
        chai.request(app)
            .get("/api/schedule")
            .set("Authorization", "Bearer "+token)
            .end((err, res) => {
                
                res.should.have.status(200);
                res.body.should.be.a('array');
                
                num = res.body.length;

                done();
        })/*.catch(e => {
            console.log("ERROR RETRIEVE: "+e.message);
            done();
        });*/
    });

    let id = -1;

    it("Schedule entries create", (done) => {
    chai.request(app)
        .post("/api/schedule")
        .set("Authorization", "Bearer "+token)
        .send({
                "startTime": "23:00:00",
                "endTime": "23:30:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek" : "0"
        })
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            
            res.body.should.have.property("id");
            res.body.id.should.be.a("number");

            res.body.should.have.property("name");
            res.body.name.should.be.a("string");

            res.body.should.have.property("startTime");
            res.body.startTime.should.be.a("string");

            res.body.should.have.property("endTime");
            res.body.endTime.should.be.a("string");

            res.body.should.have.property("color");
            res.body.color.should.be.a("string");

            res.body.should.have.property("userId");
            res.body.userId.should.be.a("number");

            res.body.should.have.property("dayOfWeek");
            res.body.dayOfWeek.should.be.a("number");

            id = res.body.id;

            done();
        })/*.catch(e => {
            console.log("ERROR CREATE: "+e.message);
            done();
        });;*/
    });

    it("Schedule entries update", (done) => {

        let newText = "[TEST] A new todo CHANGE!";

        chai.request(app)
            .patch("/api/schedule/"+id)
            .set("Authorization", "Bearer "+token)
            .send({
                name: newText
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                
                res.body.should.have.property("id");
                res.body.id.should.be.a("number");

                res.body.should.have.property("name", newText);

                res.body.should.have.property("userId");
                res.body.userId.should.be.a("number");

                res.body.should.have.property("startTime");
                res.body.startTime.should.be.a("string");

                res.body.should.have.property("endTime");
                res.body.endTime.should.be.a("string");

                res.body.should.have.property("color");
                res.body.color.should.be.a("string");

                res.body.should.have.property("userId");
                res.body.userId.should.be.a("number");

                res.body.should.have.property("dayOfWeek");
                res.body.dayOfWeek.should.be.a("number");

                res.body.should.have.property("changed", true);

                done();
            })/*.catch(e => {
                console.log("ERROR UPDATE: "+e.message);
                done();
            });*/
        });


    it("Schedule entry delete", (done) => {
        chai.request(app)
            .delete("/api/schedule/"+id)
            .set("Authorization", "Bearer "+token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');

                res.body.should.have.property("deleted", true);

                done();
            })/*.catch(e => {
                console.log("ERROR DELETE: "+e.message);
                done();
            });;*/
    });

    it("There is the same number of schedule entries as in the beginning", (done) => {
        chai.request(app)
            .get("/api/schedule")
            .set("Authorization", "Bearer "+token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                
                res.body.should.have.lengthOf(num);

                done();
            });/*.catch(e => {
                console.log("ERROR RETRIEVE 2: "+e.message);
                done();
            });*/
    });


});
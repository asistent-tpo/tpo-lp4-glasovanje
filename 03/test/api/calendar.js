var chai = require('chai');
var assert = chai.assert;
var chaiHttp = require('chai-http');
var app = require('../../app');
const User = require('../../src/app_api/models/db').User;
const Calendar = require('../../src/app_api/models/db').Calendar;
const permissionLevels = require('../../src/app_api/util/permissionLevels');
const sequelize = require('../../src/app_api/models/db').sequelize;


chai.use(chaiHttp);
let should = chai.should();

let user = {
    user1: {
        email: "user1@test.si",
        password: "12345",
        active: true
    },
    user2: {
        email: "admin2@test.si",
        password: "1234",
        active: true,
        permissionLevel: permissionLevels.ADMINISTRATOR
    },
    adminUser: {
        email: "admin@test.si",
        password: "123",
        active: true,
        permissionLevel: permissionLevels.ADMINISTRATOR
    },
    editorUser: {
        email: "editor@test.si",
        password: "1234",
        active: true,
        permissionLevel: permissionLevels.EVENT_EDITOR
    }
};

let tokens = {};

var updateOrCreate = function(userKey) {

    return User.findOne({where: {email: user[userKey].email}}).then(item => {
        
        var u = item;
        u.setPassword(user[userKey].password)
        u.active = user[userKey].active;

        if(user[userKey].permissionLevel){
            u.permissionLevel = user[userKey].permissionLevel;
        }

        return u.save();

    }).catch(() => {

        var u = new User();
        u.email = user[userKey].email;
        u.setPassword(user[userKey].password);
        u.active = user[userKey].active;

        if(user[userKey].permissionLevel){
            u.permissionLevel = user[userKey].permissionLevel;
        }

        return u.save();

    }).then(u => {

        user[userKey].id = u.id;

        tokens[userKey] = u.generateToken();

    });

}

var clearData = function(userKey, done) {

    return Calendar.destroy({
        where: {
            userId: user[userKey].id
        }
    }).then(() => {
        return User.destroy({
            where: {
                email: user[userKey].email
            }
        })
    })
    .then(() => {
        if(done) done();
    });
}

var checkCalendarEntryBody = function(res){

    /*
        {
            "startDate": DateTime
            "endDate": DateTime
            "title": string
            "description": string
        }
    */
    res.body.should.have.property("id");
    res.body.id.should.be.a("number");

    res.body.should.have.property("userId");
    res.body.userId.should.be.a("number");

    res.body.should.have.property("startDate");
    res.body.startDate.should.be.a("string");

    res.body.should.have.property("endDate");
    res.body.endDate.should.be.a("string");

    res.body.should.have.property("title");
    res.body.title.should.be.a("string");

    res.body.should.have.property("description");
    res.body.description.should.be.a("string");

}

let ids = {};

describe("Calendar", function() {


    before(done => {
        sequelize.sync()
        .then(() => { return updateOrCreate("user1"); })
        .then(() => { return updateOrCreate("user2"); })
        .then(() => { done() });
    });

    after(done => {
        clearData("user1")
        .then(() => { return clearData("user2"); })
        .then(() => { done() });
    });

    let num = 0;
    it("Fetch all calendar entries of user1", function(done) {

        chai.request(app)
        .get("/api/calendar")
        .set("Authorization", "Bearer "+tokens.user1)
            .end((err, res) => {
                if(err) done(err);
                
                res.should.have.status(200);
                res.body.should.be.a('array');

                num = res.body.length;
                done();
            });

    });
    it("Create an event [user1]", function(done){

        chai.request(app)
        .post("/api/calendar")
        .set("Authorization", "Bearer "+tokens.user1)
        .send({
            startDate: "2019-05-27 00:00:00",
            endDate: "2019-05-27 00:00:01",
            title: "Hello1",
            description: "Hello!"
        })
        .end((err, res) => {
            
            if(err) done(err);
            
            res.should.have.status(200);
            res.body.should.be.a('object');
    
            checkCalendarEntryBody(res);

            ids.evU1 = res.body.id;

            done();
        });

    });
    it("Create an event [user2]", function(done){

        chai.request(app)
        .post("/api/calendar")
        .set("Authorization", "Bearer "+tokens.user2)
        .send({
            startDate: "2019-05-27 00:00:00",
            endDate: "2019-05-27 00:00:00",
            title: "Hello from user 2",
            description: "Hello from user 2!!"
        })
        .end((err, res) => {
            
            if(err) done(err);
            
            res.should.have.status(200);
            res.body.should.be.a('object');
    
            checkCalendarEntryBody(res);

            ids.evU2 = res.body.id;

            done();
        });

    });
    it("Update an event [user1]", function(done){

        chai.request(app)
        .patch("/api/calendar/"+ids.evU1)
        .set("Authorization", "Bearer "+tokens.user1)
        .send({
            startDate: "2019-05-27 00:00:00",
            endDate: "2019-05-27 00:00:00",
            title: "I'm sad now :(",
            description: "HAHAHAHA!"
        })
        .end((err, res) => {
            
            if(err) done(err);
            
            res.should.have.status(200);
            res.body.should.be.a('object');
    
            res.body.should.have.property("changed", true);

            checkCalendarEntryBody(res);

            done();
        });

    });
    it("User 1 try to update cal entry of user2", function(done){
        
        chai.request(app)
        .patch("/api/calendar/"+ids.evU2)
        .set("Authorization", "Bearer "+tokens.user1)
        .send({
            startDate: "2019-05-27 00:00:00",
            endDate: "2019-05-27 00:00:00",
            title: "I'm gay",
            description: "User2 is gay!"
        })
        .end((err, res) => {
            
            if(err) done(err);

            res.should.have.status(402);
            res.body.should.be.a('object');
    
            res.body.should.have.property("error", "You do not have permission to update this entry!");

            done();
        });

    });
    
    it("User1 try to delete user2's event", function(done){

        chai.request(app)
        .delete("/api/calendar/"+ids.evU2)
        .set("Authorization", "Bearer "+tokens.user1)
        .end((err, res) => {
            
            if(err) done(err);
            
            res.should.have.status(200);
            res.body.should.be.a('object');
    
            res.body.should.have.property("deleted", false);

            done();
        });

    });
    
    it("Delete an event [user1]", function(done){

        chai.request(app)
        .delete("/api/calendar/"+ids.evU1)
        .set("Authorization", "Bearer "+tokens.user1)
        .end((err, res) => {
            
            if(err) done(err);
            
            res.should.have.status(200);
            res.body.should.be.a('object');
    
            res.body.should.have.property("deleted", true);

            done();
        });

    });

    it("Fetch all and check if there's the same number of cal entries as in the beginning", function(done){
        chai.request(app)
        .get("/api/calendar")
        .set("Authorization", "Bearer "+tokens.user1)
            .end((err, res) => {
                if(err) done(err);
                
                res.should.have.status(200);
                res.body.should.be.a('array');        
                res.body.should.have.lengthOf(num);

                done();
            });
    })

});
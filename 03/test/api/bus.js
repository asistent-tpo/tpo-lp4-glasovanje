var chai = require('chai');
var assert = chai.assert;
var chaiHttp = require('chai-http');
var app = require('../../app');

chai.use(chaiHttp);
let should = chai.should();

describe("BUS", function(){

    it("Get all stations", function(done){

        chai.request(app)
        .get("/api/bus")
        .end(function(err, res){

            if(err) done(err);

            res.should.have.status(200);
            res.body.should.be.a('array');

            res.body[0].should.be.a("object");
            res.body[0].should.have.property("name");
            res.body[0].should.have.property("ascii");

            res.body.should.have.lengthOf(898);

            done();

        });

    });

    it("Get all station coords", function(done){

        chai.request(app)
        .get("/api/bus/coords")
        .end(function(err, res){

            if(err) done(err);

            res.should.have.status(200);
            res.body.should.be.a('array');

            res.body[0].should.be.a("object");
            res.body[0].should.have.property("lat");
            res.body[0].should.have.property("lon");
            res.body[0].should.have.property("name");

            res.body.should.have.lengthOf(1069);

            done();

        });

    });

    it("Get 'vščina' station coords (bus coords filter)", function(done){

        chai.request(app)
        .get("/api/bus/coords/vscina")
        .end(function(err, res){

            if(err) done(err);

            res.should.have.status(200);
            res.body.should.be.a('array');

            res.body[0].should.be.a("object");
            res.body[0].should.have.property("lat", "46.0534515");
            res.body[0].should.have.property("lon", "14.5039799");
            res.body[0].should.have.property("name", "600022 ~ Ajdovščina");

            res.body.should.have.lengthOf(2);

            done();

        });

    });

    it("Get arrival for Ajdovščina", function(done){
        
        this.timeout(6000);

        chai.request(app)
        .get("/api/bus/Ajdovscina")
        .end((err, res) => {
            if(err) done(err);

            res.should.have.status(200);
            res.body.should.be.a("array");

            res.body.should.have.lengthOf(2);

            res.body[0].should.be.a("object");

            done();
        });
    });

    it("Get arrival for Bavarski dvor", function(done){
        
        this.timeout(6000);

        chai.request(app)
        .get("/api/bus/Bavarski dvor")
        .end((err, res) => {
            if(err) done(err);

            res.should.have.status(200);
            res.body.should.be.a("array");

            res.body.should.have.lengthOf(2);

            res.body[0].should.be.a("object");

            done();
        });
    })

});
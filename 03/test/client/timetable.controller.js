/*describe('timetableCtrl', function () {
    // define variables for the services we want to access in tests
    var timetableCtrl,
        scheduleService;

    var singleScheduleItem = {
        "startTime": "23:00:00",
        "endTime": "23:30:00",
        "name": "Večerja",
        "color": "red",
        "dayOfWeek": "0"        
    }

    var unsorted = [
        {
            "startTime": "23:00:00",
            "endTime": "23:30:00",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "0",
            "id": "15",
            "userId": "1337"
        },
        {
            "startTime": "16:00:00",
            "endTime": "16:30:00",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "0",
            "id": "15",
            "userId": "1337"            
        },
        {
            "startTime": "16:31:00",
            "endTime": "16:50:00",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "0",
            "id": "15",
            "userId": "1337"            
        },
        {
            "startTime": "12:00:00",
            "endTime": "15:59:59",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "2",
            "id": "15",
            "userId": "1337"            
        },
        {
            "startTime": "23:00:00",
            "endTime": "23:30:00",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "2",
            "id": "15",
            "userId": "1337"
        },
        {
            "startTime": "16:00:00",
            "endTime": "16:30:00",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "2",
            "id": "15",
            "userId": "1337"            
        },
        {
            "startTime": "16:31:00",
            "endTime": "16:50:00",
            "name": "Večerja",
            "color": "red",
            "dayOfWeek": "2",
            "id": "15",
            "userId": "1337"            
        }        
    ];

    var sorted = {
        "0": [
            {
                "startTime": "16:00:00",
                "endTime": "16:30:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "0",
                "id": "15",
                "userId": "1337",
                "timeDiff": 0
            },
            {
                "startTime": "16:31:00",
                "endTime": "16:50:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "0",
                "id": "15",
                "userId": "1337",
                "timeDiff": 60
            },
            {
                "startTime": "23:00:00",
                "endTime": "23:30:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "0",
                "id": "15",
                "userId": "1337",
                "timeDiff": 22200
            }
        ],
        "1": [],
        "2": [
            {
                "startTime": "12:00:00",
                "endTime": "15:59:59",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "2",
                "id": "15",
                "userId": "1337",
                "timeDiff": 0
            },
            {
                "startTime": "16:00:00",
                "endTime": "16:30:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "2",
                "id": "15",
                "userId": "1337",
                "timeDiff": 1
            },
            {
                "startTime": "16:31:00",
                "endTime": "16:50:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "2",
                "id": "15",
                "userId": "1337",
                "timeDiff": 60
            },
            {
                "startTime": "23:00:00",
                "endTime": "23:30:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "2",
                "id": "15",
                "userId": "1337",
                "timeDiff": 22200
            }            
        ],
        "3": [],
        "4": [],
        "5": [],
        "6": []
    };

    
    beforeEach(function () {
        // load the module we want to test
        module('straightas');

        // get services from injector
        inject(function ($controller, _scheduleService_) {
            scheduleService = _scheduleService_;
          
            // spy on service method to check if it gets called later
            sinon.spy(scheduleService, 'schedule');
            sinon.spy(scheduleService, 'deleteScheduleItem')
            sinon.spy(scheduleService, 'addToSchedule')            
            // get controller instance from $controller service
            timetableCtrl = $controller('timetableCtrl');
        });
    });
    
    afterEach(function(){
        // remove spy from service
        scheduleService.schedule.restore();
        scheduleService.deleteScheduleItem.restore();
        scheduleService.addToSchedule.restore();
    });

    describe('constructor', function () {
        it('should call scheduleService', function () {
            // Assert
            assert(scheduleService.schedule.calledOnce);
        });
    });

    describe('operations on schedule', function () {
        it('should call scheduleService fot adding', function () {
            timetableCtrl.addItem(singleScheduleItem);
            assert(scheduleService.addToSchedule.calledOnce);
        });

        it('should sort correctly', function () {
            function addFunctions(item) {
                item.getTimeStartValue = function() {return this.startTime.replace(/:/g, "");};
                item.getTimeEndValue   = function() {return this.endTime.replace(/:/g, "");};
                return item;
            }

            Object.keys(sorted).map(function(objectKey, index) {
                sorted[objectKey].forEach(function (item) {
                    item = addFunctions(item);
                });
            });

            var actual = JSON.stringify(timetableCtrl.sortedSchedule(unsorted));
            var expected = JSON.stringify(sorted);
            assert.equal(actual, expected);
        });
    });
});*/

let unidecode = require("unidecode");
let allBus = require("./src/app_api/models/allBusStations");

let k = [];

allBus.forEach(st => {
    
    k.push({
        name: st,
        ascii: unidecode(st)
    });

});

var fs = require('fs');

fs.writeFile('./src/app_api/models/allBusStationsObj.js', JSON.stringify(k), 'utf8', function(){
    console.log("I did it!!");
});
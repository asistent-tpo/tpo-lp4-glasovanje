var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('./app');
const User = require('./src/app_api/models/db').User;
const sequelize = require('./src/app_api/models/db').sequelize;
const permissionLevels = require('./src/app_api/util/permissionLevels');

let tokens = {};

var updateOrCreate = function(userKey) {

    return User.findOne({where: {email: user[userKey].email}}).then(item => {
        
        var u = item;
        u.setPassword(user[userKey].password)
        u.active = user[userKey].active;

        if(user[userKey].permissionLevel){
            u.permissionLevel = user[userKey].permissionLevel;
        }

        return u.save();

    }).catch(() => {

        var u = new User();
        u.email = user[userKey].email;
        u.setPassword(user[userKey].password);
        u.active = user[userKey].active;

        if(user[userKey].permissionLevel){
            u.permissionLevel = user[userKey].permissionLevel;
        }

        return u.save();

    }).then(u => {

        tokens[userKey] = u.generateToken();

    });

}

let user = {
    user1: {
        email: "user1@test.si",
        password: "12345",
        active: true
    },
    user2: {
        email: "admin2@test.si",
        password: "1234",
        active: true,
        permissionLevel: permissionLevels.ADMINISTRATOR
    },
    adminUser: {
        email: "admin@test.si",
        password: "123",
        active: true,
        permissionLevel: permissionLevels.ADMINISTRATOR
    },
    editorUser: {
        email: "editor@test.si",
        password: "1234",
        active: true,
        permissionLevel: permissionLevels.EVENT_EDITOR
    }
};


updateOrCreate("user1").then(() => {
    console.log(tokens.user1);
});

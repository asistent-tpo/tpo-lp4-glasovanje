require('dotenv').config();

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var Terser = require("terser");
var fs = require('fs');
var createError = require('http-errors');

var zdruzeno = Terser.minify({
  'app.js': fs.readFileSync('src/app_client/app.js', 'utf-8'),
  'navigation.directive.js': fs.readFileSync('src/app_client/directives/navigation/navigation.directive.js', 'utf-8'),
  'todo.directive.js': fs.readFileSync('src/app_client/directives/todo/todo.directive.js', 'utf-8'),
  'navigation.controller.js': fs.readFileSync('src/app_client/directives/navigation/navigation.controller.js', 'utf-8'),
  'login.controller.js': fs.readFileSync('src/app_client/login/login.controller.js', 'utf-8'),
  'todo.controller.js': fs.readFileSync('src/app_client/directives/todo/todo.controller.js', 'utf-8'),
  'registration.controller.js': fs.readFileSync('src/app_client/registration/registration.controller.js', 'utf-8'),
  'restaurants.controller.js': fs.readFileSync('src/app_client/restaurants/restaurants.controller.js', 'utf-8'),
  'activation.controller.js': fs.readFileSync('src/app_client/mail_activation/activation.controller.js', 'utf-8'),
  'auth.service.js': fs.readFileSync('src/app_client/services/auth.service.js', 'utf-8'),
  'restaurant.service.js': fs.readFileSync('src/app_client/services/restaurant.service.js', 'utf-8'),
  'todo.service.js': fs.readFileSync('src/app_client/services/todo.service.js', 'utf-8'),
  'notification.service.js': fs.readFileSync('src/app_client/services/notification.service.js', 'utf-8'),
  'events.service.js': fs.readFileSync('src/app_client/services/events.service.js', 'utf-8'),
  'timetable.controller.js': fs.readFileSync('src/app_client/directives/timetable/timetable.controller.js', 'utf-8'),
  'timetable.directive.js': fs.readFileSync('src/app_client/directives/timetable/timetable.directive.js', 'utf-8'),
  'schedule.service.js': fs.readFileSync('src/app_client/services/schedule.service.js', 'utf-8'),
  'timetable.filter.js': fs.readFileSync('src/app_client/filters/timetable.filter.js', 'utf-8'),
  'admin.controller.js': fs.readFileSync('src/app_client/admin/admin.controller.js', 'utf-8'),
  'bus.service.js': fs.readFileSync('src/app_client/services/bus.service.js', 'utf-8'),
  'bus.controller.js': fs.readFileSync('src/app_client/directives/bus/bus.controller.js', 'utf-8'),    
  'bus.directive.js': fs.readFileSync('src/app_client/directives/bus/bus.directive.js', 'utf-8'),
  'content.controller.js': fs.readFileSync('src/app_client/content/content.controller.js', 'utf-8'),
  'settings.controller.js': fs.readFileSync('src/app_client/settings/settings.controller.js', 'utf-8')
}/*, options*/);

if (zdruzeno.error) {
  console.log("error:");
  console.log(zdruzeno.error);
}
if (zdruzeno.warnings) {
  console.log("warnings:");
  console.log(zdruzeno.warnings);
}

fs.writeFile('public/angular/straightas.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "straightas.min.js".');
});

//init DB
let passport = require('passport');
require('./src/app_api/models/db');
require('./src/app_api/configuration/passport');

var restRouter = require('./src/app_api/routes/index');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'src/app_client')));
app.use(express.static(path.join(__dirname, 'public')));

//favicon
app.use('/favicon.ico', express.static(path.join(__dirname, 'public/images/favicon.ico')));

app.use(passport.initialize());

app.use('/api', restRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'src/app_client', 'index.html'));
});

app.use(function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({
            "msg": err.name + ": " + err.message
        });
    }
});

app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    //res.render('error');
});

module.exports = app;

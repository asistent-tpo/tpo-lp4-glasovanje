(function() {
    function activationCtrl($routeParams, $scope, $rootScope, $location, authentication) {
        var vm = this;
        
        vm.hashValue = $routeParams.hash;
        
        var activateHash = function() {
            authentication.mailAuthentication(vm.hashValue).then(
                function success(res) {
                    vm.message = "Aktivacija uspesna, preusmerjam...";
                    $location.path("/login");
                },
                function error(res) {
                    vm.message = res.data.msg;
                    $location.path("/registration");
                }
            );
        };
        
        activateHash();
    }
        
    activationCtrl.$inject = ['$routeParams', '$scope', '$rootScope', '$location', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('activationCtrl', activationCtrl);
})();

(function() {
    function registrationCtrl($scope, $rootScope, $location, authentication) {
        var vm = this;
        
        vm.tryRegister = function() {
            
            if(vm.password1 == vm.password2){
                var data = {
                    email : vm.email,
                    password : vm.password1
                };
                
                authentication.register(data).then(
                    function success(res) {
                        $location.path("/login");
                    },
                    function error(res) {
                        vm.failedLogin = res.data.msg;
                    }
                );
            }else{
               vm.failedLogin = "Gesli se ne ujemata" 
            }
            
        };
    }
        
    registrationCtrl.$inject = ['$scope', '$rootScope', '$location', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('registerCtrl', registrationCtrl);
})();

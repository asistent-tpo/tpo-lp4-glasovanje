(function() {
    function contentCtrl($routeParams, $scope, $rootScope, events) {
        var vm = this;
        vm.width = 100;
        vm.input = false;

        vm.getAllEvents = function() {
            events.getAllEvents().then(
                function success(res) {
                    vm.message = res.data.length > 0 ? "" : "Ne najdem dogodkov.";
                    vm.data = {
                        events: res.data
                    };
                },
                function error(res) {
                    vm.message = res.data.msg;
                }
            );
        };
        
        vm.showEditable = function(event){
            if(event){
                vm.isEdit = true;
                vm.data.event = event;
                vm.data.event.date = new Date(vm.data.event.date);
            }else{
                vm.isEdit = false;
                vm.data.event = null;
            }
            vm.setWidth();
        };
        
        vm.setWidth = function(){
            if (!vm.input){
                vm.all_width = '35%';
            }else{
                vm.all_width = '';
            }
            vm.input = !vm.input;
        };
        
        vm.addEvent = function(){
          var event = vm.data.event;
          if(event.name && event.description && event.organizer && event.date){
              vm.message = "";
              events.addEvent(event).then(
                function success(res) {
                    vm.getAllEvents();
                },
                function error(res) {
                    vm.message = res.data.msg;
                }
            );
            vm.setWidth();
          }else{
              vm.message = "Prosimo izpolnite vsa polja.";
          }
        };
        
        vm.editEvent = function(){
          var event = vm.data.event;
          if(event.name && event.description && event.organizer && event.date){
              vm.message = "";
              events.updateEvent(event.id,event).then(
                function success(res) {
                },
                function error(res) {
                    vm.message = res.data.msg;
                }
            );
            vm.setWidth();
          }else{
              vm.message = "Prosimo izpolnite vsa polja.";
          }
        };
        
        vm.removeEvent = function(id){
            vm.message = "";
            events.removeEvent(id).then(
                function success(res) {
                },
                function error(res) {
                    vm.message = res.data.msg;
                }
            );
            vm.getAllEvents();
        };


        vm.getAllEvents();
    }

    contentCtrl.$inject = ['$routeParams', '$scope', '$rootScope', 'events'];

    /* global angular */
    angular
        .module('straightas')
        .controller('contentCtrl', contentCtrl);
})();
//ng-click="myStyle={'background-color':'blue'}" ng-style="myStyle"
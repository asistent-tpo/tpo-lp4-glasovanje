(function() {
    function notification($rootScope, $window, $http, authentication) {

        var headers = function () {
            return {
                headers: {
                    Authorization: 'Bearer ' + authentication.getAuthToken()
                }
            };
        };

        var getActiveNotifications = function() {
            return $http.get("/api/notifications", headers());
        };

        var getAllNotifications = function() {
            return $http.get("/api/notifications?all=true", headers());
        };

        var updateNotification = function(id, data) {
            return $http.patch("/api/notifications/" + id, data, headers());
        };

        var deleteNotification = function(id) {
            return $http.delete("/api/notifications/" + id, headers());
        };

        var createNotification = function(data) {
            return $http.post("/api/notifications", data, headers());
        };


        return {
            getAllNotifications: getAllNotifications,
            updateNotification: updateNotification,
            deleteNotification: deleteNotification,
            createNotification: createNotification,
            getActiveNotifications: getActiveNotifications
        };
    };
    notification.$inject = ['$rootScope', '$window', '$http', 'authentication'];

    /* global angular */
    angular
        .module('straightas')
        .service('notification', notification);
})();

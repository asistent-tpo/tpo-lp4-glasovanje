(function() {
    function todo($rootScope, $window, $http, authentication) {
        
        var headers = function () {
          return {
            headers: {
              Authorization: 'Bearer ' + authentication.getAuthToken()
            }
          };
        };
        /*
        router.post('/api/todo',       authentication, todoController.createUserTodo); // C
        router.get('/api/todo',        authentication, todoController.getUserTodos);   // R
        router.delete('/api/todo/:id', authentication, todoController.deleteUserTodo); // U
        router.patch('/api/todo/:id',  authentication, todoController.editUserTodo);   // D
        */
        
        var getTodos = function() {
            return $http.get("/api/todo", headers());
        };
        
        var deleteTodo = function(todoId, new_txt) {
            return $http.delete("/api/todo/" + todoId, headers());
        };
        
        var editTodo = function (todoId, reqTodo) {
            return $http.patch("/api/todo/" + todoId, reqTodo, headers());
        };
        
        var createTodo = function (reqTodo) {
            return $http.post("/api/todo", reqTodo, headers());
        };

        return {
            getTodos: getTodos,
            deleteTodo: deleteTodo,
            editTodo: editTodo,
            createTodo: createTodo
        };
    };
    todo.$inject = ['$rootScope', '$window', '$http', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .service('todo', todo);
})();

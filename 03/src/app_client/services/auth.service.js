(function() {
    function authentication($rootScope, $window, $http) {
        var b64Utf8 = function (niz) {
            return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        };
        
        var register = function(registerData) {
            return $http.post("/api/users/register", registerData).then(
                function success(reply) {
                    $rootScope.$broadcast('authStateChange');
                }
            );
        };

        var login = function(loginData) {
            return $http.post("/api/users/login", loginData).then(
                function success(reply) {
                    saveAuthToken(reply.data.authToken)
                    $rootScope.$broadcast('authStateChange');
                }
            );
        };

        var logout = function() {
            $window.localStorage.removeItem('straightas-auth-token');
            $rootScope.$broadcast('authStateChange');
        };
        
        var loggedIn = function() {
            var zeton = getAuthToken();
            if(zeton != "undefined" && zeton != undefined) {
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                //console.log(koristnaVsebina);
                return koristnaVsebina.expireDate > Date.now() / 1000;
            } else {
                return false;
            }
        };
        
        var admin = function() {
            if (loggedIn()) {
                var zeton = getAuthToken();
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                return koristnaVsebina.permissionLevel >= 100;
            }
            return false;
        };
        
        var contentEditor = function() {
            var zeton = getAuthToken();
            if(zeton != "undefined" && zeton != undefined) {
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                return ((koristnaVsebina.permissionLevel >= 50 && koristnaVsebina.permissionLevel < 100));
            } else {
                return false;
            }
        };
        
        var loggedIn = function() {
            var zeton = getAuthToken();
            if(zeton != "undefined" && zeton != undefined) {
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                //console.log(koristnaVsebina);
                return koristnaVsebina.expireDate > Date.now() / 1000;
            } else {
                return false;
            }
        };
        
        var getUserId = function() {
            var zeton = getAuthToken();
            if(zeton != "undefined" && zeton != undefined) {
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                return koristnaVsebina.id;
            } else {
                return false;
            }
        };

        var eventManager = function() {
            if (loggedIn()) {
                var zeton = getAuthToken();
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                return koristnaVsebina.permissionLevel >= 50 && koristnaVsebina.permissionLevel < 100;
            }
            return false;
        };
        
        var mailAuthentication = function(hashValue) {
            return $http.get('/api/users/activate/' + hashValue).then(
                function success(reply) {
                    saveAuthToken(reply.data.authToken);
                    $rootScope.$broadcast('authStateChange');
                    //console.log(reply.data.authToken);
                }
            );
        }

        var saveAuthToken = function(token) {
            $window.localStorage['straightas-auth-token'] = token;
        };

        var getAuthToken = function() {
            return $window.localStorage['straightas-auth-token'];
        };

        var authHeader = function () {
            return {
                headers: {
                    Authorization: 'Bearer ' + getAuthToken()
                }
            };
        };
        
        var currentData = function() {
            if (loggedIn()) {
                var zeton = getAuthToken();
                var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
                return {
                    id: koristnaVsebina.id,
                    email: koristnaVsebina.email,
                    permissionLevel: koristnaVsebina.permissionLevel
                };
            }
        };
        
        var validatePassword = function(password) {
            var userdata = currentData();
            var logindata = {
              email : userdata.email,
              password : password
            };
            return $http.post("/api/users/login", logindata).then(
                function success(reply) {
                    return userdata;
                }, function error(err) {
                    return false;
                }
            );
        };
        
        var changePassword = function(newdata) {
            return $http.post("api/users/changePassword", newdata, authHeader()).then(
                function success(reply) {
                    return true;
                }
            );
        };

        return {
            register: register,
            login: login,
            logout: logout,
            loggedIn: loggedIn,
            admin: admin,
            contentEditor: contentEditor,
            eventManager: eventManager,
            mailAuthentication: mailAuthentication,
            saveAuthToken: saveAuthToken,
            getAuthToken: getAuthToken,
            authHeader: authHeader,
            getUserId: getUserId,
            currentData: currentData,
            validatePassword: validatePassword,
            changePassword: changePassword
        };
    };
    authentication.$inject = ['$rootScope', '$window', '$http'];
    
    /* global angular */
    angular
    .module('straightas')
    .service('authentication', authentication);
})();

(function() {
    function events($rootScope, $window, $http, authentication) {

        var headers = function () {
            return {
                headers: {
                    Authorization: 'Bearer ' + authentication.getAuthToken()
                }
            };
        };
        
        /*
        router.post('/events',       [authentication, editorPermission] , eventsController.createEvent);  // C
        router.get('/events',         authentication,                     eventsController.getAllEvents); // R
        router.get('/events/:id',     authentication,                     eventsController.getEvent);     // R
        router.delete('/events/:id', [authentication, editorPermission],  eventsController.deleteEvent);  // U
        router.patch('/events/:id',  [authentication, editorPermission],  eventsController.editEvent);    // D
        */
        
        var getAllEvents = function() {
            return $http.get("/api/events", headers());
        };

        var updateEvent = function(id, data) {
            return $http.patch("/api/events/" + id, data, headers());
        };

        var removeEvent = function(id) {
            return $http.delete("/api/events/" + id, headers());
        };

        var addEvent = function(data) {
            return $http.post("/api/events", data, headers());
        };


        return {
            getAllEvents: getAllEvents,
            updateEvent: updateEvent,
            removeEvent: removeEvent,
            addEvent: addEvent
        };
    };
    events.$inject = ['$rootScope', '$window', '$http', 'authentication'];

    /* global angular */
    angular
        .module('straightas')
        .service('events', events);
})();

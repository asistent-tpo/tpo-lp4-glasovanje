(function() {
    function restaurant($rootScope, $window, $http, authentication) {
        
        var headers = function () {
          return {
            headers: {
              Authorization: 'Bearer ' + authentication.getAuthToken()
            }
          };
        };
        
        var getRestaurants = function() {
            return $http.get("/api/restaurants", headers());
        };
        
        var getNearRestaurants = function(lat,lon) {
            return $http.get("/api/restaurants/" + lat + "/" + lon, headers());
        };

        return {
            getRestaurants: getRestaurants,
            getNearRestaurants: getNearRestaurants
        };
    };
    restaurant.$inject = ['$rootScope', '$window', '$http', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .service('restaurant', restaurant);
})();

(function() {
    function busService($http, authentication) {
        var busStations = function() {
            return $http.get("/api/bus", authentication.authHeader());
        };

        var arrivalsForStation = function(stationName) {
            return $http.get("/api/bus/" + stationName, authentication.authHeader());
        };

        return {
            busStations: busStations,
            arrivalsForStation: arrivalsForStation
        };
    };
    busService.$inject = ['$http', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .service('busService', busService);
})();

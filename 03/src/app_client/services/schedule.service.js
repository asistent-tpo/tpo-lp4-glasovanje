(function() {
    function scheduleService($http, authentication) {
        var schedule = function() {
            return $http.get("/api/schedule", authentication.authHeader());
        };

        var addToSchedule = function(scheduleItem) {
            return $http.post("/api/schedule", scheduleItem,authentication.authHeader());
        };

        var deleteScheduleItem = function(scheduleItemId) {
            return $http.delete("/api/schedule/" + scheduleItemId, authentication.authHeader());            
        };

        var updateScheduleItem = function(scheduleItem) {
            return $http.patch("/api/schedule/" + scheduleItem.id, scheduleItem, authentication.authHeader());
        };

        return {
            schedule: schedule,
            addToSchedule: addToSchedule,
            deleteScheduleItem: deleteScheduleItem,
            updateScheduleItem: updateScheduleItem
        };
    };
    scheduleService.$inject = ['$http', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .service('scheduleService', scheduleService);
})();

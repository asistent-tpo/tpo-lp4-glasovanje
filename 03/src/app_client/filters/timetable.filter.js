(function() {

    function itemDateFilter() {
        return function(item) {
            return item.startTime.match(/\d\d:\d\d/g) + " - " + item.endTime.match(/\d\d:\d\d/g); 
        }
    }

    function timeHour() {
        return function(startTime) {
            return startTime.match(/\d\d/g)[0];
        }
    }

    function timeMinute() {
        return function(startTime) {
            return startTime.match(/\d\d/g)[1];
        }
    }
    
    angular
        .module('straightas')
        .filter('timeHour', timeHour);

    angular
        .module('straightas')
        .filter('timeMinute', timeMinute);
    
    angular
        .module('straightas')
        .filter('itemDateFilter', itemDateFilter);
    
})();

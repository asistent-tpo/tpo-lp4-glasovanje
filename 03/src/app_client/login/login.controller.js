(function() {
    function loginCtrl($scope, $rootScope, $location, authentication) {
        var vm = this;
        
        vm.tryLogin = function() {
            var data = {
              email : vm.email,
              password : vm.password
            };
            authentication.login(data).then(
                function success(res) {
                    console.log("Login successful");
                    $location.path("/events");
                },
                function error(res) {
                    console.log("Login failed");
                    vm.failedLogin = res.data.msg;
                }
            );
        };
    }
        
    loginCtrl.$inject = ['$scope', '$rootScope', '$location', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('loginCtrl', loginCtrl);
})();

(function() {
    function navigationCtrl($rootScope, $location, $route, authentication, notification) {
        var navvm = this;
        
        navvm.loggedIn = authentication.loggedIn();
        //console.log(navvm.loggedIn);
        navvm.admin = authentication.admin();
        navvm.eventManager = authentication.eventManager();

        notification.getActiveNotifications().then(res => {
            navvm.notifications = res.data;
        });
    
        navvm.odjava = function() {
            authentication.logout();
            navvm.loggedIn = authentication.loggedIn();
            navvm.admin = authentication.admin();
            navvm.eventManager = authentication.eventManager();
            $location.path('/login');
            $route.reload();
        };
        
        $rootScope.$on('authStateChange', function() {
            navvm.loggedIn = authentication.loggedIn();
            navvm.admin = authentication.admin();
            navvm.eventManager = authentication.eventManager();
        });
    }
    
    navigationCtrl.$inject = ['$rootScope', '$location', '$route', 'authentication', 'notification'];
    
    /* global angular */
    angular
        .module('straightas')
        .controller('navigationCtrl', navigationCtrl);
})();
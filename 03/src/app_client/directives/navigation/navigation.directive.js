(function() {
  function navigation() {
    return {
      restrict: 'EA',
      templateUrl: 'directives/navigation/navigation.view.html',
      controller: 'navigationCtrl',
      controllerAs: 'navvm'
    };
  }
  
  /* global angular */
  angular
    .module('straightas')
    .directive('navigation', navigation);
})();
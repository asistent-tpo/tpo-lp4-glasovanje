//contenteditable="true"
(function() {
    function todoCtrl($scope, $rootScope, $location, authentication, todo) {
        var todovm = this;
        
        todovm.getUserTodos = function() {
            
            todo.getTodos(authentication.getUserId()).then(
                function success(res) {
                    todovm.message = res.data.length > 0 ? "" : "Ne najdem todojev za uporabnika.";
                    todovm.data = {
                        todos: res.data  
                    };
                },
                function error(res) {
                    todovm.message = res.data.msg;
                }
            );
        };
        
        todovm.addTodo = function() {
            
            var req = todovm.data.todo;
            todovm.sporocilo = "";
            
            if(req.text != ""){
                
                todo.createTodo(req).then(function success(res) {
                    todovm.sporocilo = "Opravilo uspesno dodano!";
                    todovm.getUserTodos();
                }, function error(err) {
                    todovm.sporocilo = "Prišlo je do napake!";
                    console.log(err.e);
                });
            }else{
                todovm.sporocilo = "Prosim izpolnite vsa polja!";
            }
        };
        
        todovm.removeTodo = function(id) {
            todovm.sporocilo = "";
            
            todo.deleteTodo(id).then(function success(res) {
                todovm.sporocilo = "Opravilo uspesno dodano!";
                todovm.getUserTodos();
            }, function error(err) {
                todovm.sporocilo = "Prišlo je do napake!";
                console.log(err.e);
            });
        };
        
        todovm.setEdit = function(todo) {
            todovm.editId = todo.id;
            todovm.edittext = todo.name;
        };
        
        todovm.editTodo = function() {
            todovm.sporocilo = "";
            var req = {text: todovm.edittext};
            
            if(req.text != ""){
                todo.editTodo(todovm.editId, req).then(function success(res) {
                    todovm.sporocilo = "Opravilo uspesno dodano!";
                    todovm.getUserTodos();
                }, function error(err) {
                    todovm.sporocilo = "Prišlo je do napake!";
                    console.log(err.e);
                });
            }else{
                todovm.sporocilo = "Prosim izpolnite vsa polja!";
            }
        };
        
        todovm.getUserTodos();
        
    }
        
    todoCtrl.$inject = ['$scope', '$rootScope', '$location', 'authentication', 'todo'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('todoCtrl', todoCtrl);
})();

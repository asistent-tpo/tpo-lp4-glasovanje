(function() {
  function todo() {
    return {
      restrict: 'EA',
      templateUrl: 'directives/todo/todo.view.html',
      controller: 'todoCtrl',
      controllerAs: 'todovm'
    };
  }
  
  /* global angular */
  angular
    .module('straightas')
    .directive('todo', todo);
})();
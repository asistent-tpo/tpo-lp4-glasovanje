(function() {
    function busCtrl($sce, busService) {
        var busvm = this;

        busvm.stations = [];
        busvm.arrivals = [];

        busvm.osmUrl = $sce.trustAsResourceUrl("https://www.openstreetmap.org/export/embed.html?bbox=14.497570395469667%2C46.0496051923787%2C14.50048327445984%2C46.050794708616635&amp;layer=transportmap&amp;marker=46.05019995369992%2C14.499026834964752");
                
        function getStations() {
            busService.busStations().then(
                function success(res) {
                    busvm.stations = res.data;
                },
                function error(res) {
                    busvm.failMsg = "Napaka pri pridobivanju podatkov!";
                }
            );
        }

        busvm.getMap = function(lat, lon) {
            if (lat === null || lat === undefined || lon === null || lon === undefined) {
                return;
            }
            
            var latDiff1 = -0.000595108893095;
            var latDiff2 = 0.000595102488396;
            var lonDiff1 = -0.001456439495085;
            var lonDiff2 = 0.001456439495088;

            var bbox1Lat = parseFloat(lat) + latDiff1;
            var bbox1Lon = parseFloat(lon) + lonDiff1;

            var bbox2Lat = parseFloat(lat) + latDiff2;
            var bbox2Lon = parseFloat(lon) + lonDiff2;

            var urlString = "https://www.openstreetmap.org/export/embed.html?bbox="+bbox1Lon+"%2C"+bbox1Lat+"%2C"+bbox2Lon+"%2C"+bbox2Lat+"&amp;layer=mapnik&amp;marker="+lat+"%2C"+lon;

            busvm.osmUrl = $sce.trustAsResourceUrl(urlString);

        };

        busvm.getArrivals = function(name) {
            busService.arrivalsForStation(name).then(
                function success(res) {
                    busvm.arrivals = res.data;
                },
                function error(res) {
                    busvm.failMsg = "Napaka pri pridobavnju podatkov!";
                }
            );
        }
 
        getStations();       
    }

    busCtrl.$inject = ['$sce', 'busService'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('busCtrl', busCtrl);
})();

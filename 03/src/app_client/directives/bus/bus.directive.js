(function() {
  function bus() {
    return {
      restrict: 'EA',
      templateUrl: 'directives/bus/bus.view.html',
      controller: 'busCtrl',
      controllerAs: 'busvm'
    };
  }
  
  /* global angular */
  angular
    .module('straightas')
    .directive('bus', bus);
})();

(function() {
  function todo() {
    return {
      restrict: 'EA',
      templateUrl: 'directives/timetable/timetable.view.html',
      controller: 'timetableCtrl',
      controllerAs: 'timetablevm'
    };
  }
  
  /* global angular */
  angular
    .module('straightas')
    .directive('timetable', todo);
})();

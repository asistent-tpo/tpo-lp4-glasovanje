(function() {
    function timetableCtrl(scheduleService, timeHour, timeMinute) {
        dayStartInSeconds =  5 * 60 * 60;
        wholeDayInSeconds = 24 * 60 * 60;
        emPerSecond = 5/3600;
        
        var timetablevm = this;
        timetablevm.failMsg = "";
        timetablevm.scheduleItemArray = [];
        timetablevm.currentTimetable = {
                "0": [],
                "1": [],
                "2": [],
                "3": [],
                "4": [],
                "5": [],
                "6": []
            };
        
        // populate with data on start
        scheduleService.schedule().then(
            function success(res) {
                timetablevm.scheduleItemArray = res.data;
                updateTimetable();
            },
            function error(res) {
                timetablevm.failMsg = "Napaka pri pridobivanju podatkov: " + res.body.error;
            }
        );

        
        timetablevm.deleteItem = function(id) {
            scheduleService.deleteScheduleItem(id).then(
                function success(res) {

                    for (var index = 0; index < timetablevm.scheduleItemArray.length; index++) {
                        if (timetablevm.scheduleItemArray[index].id == id) {
                            timetablevm.scheduleItemArray.splice(index, 1);
                        }
                    }

                    updateTimetable();
                },
                function error(res) {
                    timetablevm.failMsg = res.body.error;
                }
            );
        };


        function addFunctions(item) {
            item.getTimeStartValue = function() {
                var mathes = this.startTime.match(/\d\d/g);
                var seconds = 0;
                seconds += parseInt(mathes[0]) * 60 * 60;
                seconds += parseInt(mathes[1]) * 60;
                seconds += parseInt(mathes[2]);

                seconds -= dayStartInSeconds;

                if (seconds < 0) {
                    seconds = Math.abs(seconds) + wholeDayInSeconds - dayStartInSeconds;
                }

                return seconds;
            };

            item.getTimeEndValue = function() {
                var mathes = this.endTime.match(/\d\d/g);
                var seconds = 0;
                seconds += parseInt(mathes[0]) * 60 * 60;
                seconds += parseInt(mathes[1]) * 60;
                seconds += parseInt(mathes[2]);

                seconds -= dayStartInSeconds;

                if (seconds < 0) {
                    seconds = Math.abs(seconds) + wholeDayInSeconds - dayStartInSeconds;
                }

                return seconds;
            };

            item.getDuration = function() {
                return item.getTimeEndValue() - item.getTimeStartValue();
            };

            item.style = function() {
                var styleObj = {
                    "background-color": item.color,
                    "margin-top": (item.timeDiff * emPerSecond) + "em",
                    "height": (item.getDuration() * emPerSecond) + "em"
                };

                return styleObj;
            };

            return item;
        }

        function updateTimetable() {
            timetablevm.currentTimetable = timetablevm.sortedSchedule(timetablevm.scheduleItemArray);
        }
        
        timetablevm.addItem = function() {
            /*
            {
                "startTime": "23:00:00",
                "endTime": "23:30:00",
                "name": "Večerja",
                "color": "red",
                "dayOfWeek": "1"
            }
            */
            var data = {};
            data.name = timetablevm.data.name;
            data.startTime = timetablevm.data.startTime();
            data.endTime = timetablevm.data.endTime();
            data.color = timetablevm.data.color;
            data.dayOfWeek = timetablevm.data.dayOfWeek;
            
            scheduleService.addToSchedule(data).then(
                function success(res) {
                    var updated = {};                      
                    updated.id = res.data.id;              
                    updated.name = res.data.name;          
                    updated.startTime = res.data.startTime;
                    updated.endTime = res.data.endTime;    
                    updated.userId = res.data.userId;      
                    updated.dayOfWeek = res.data.dayOfWeek;
                    updated.color = res.data.color;        
                    
                    timetablevm.scheduleItemArray.push(updated);
                    updateTimetable();
                },
                function error(res) {
                    timetablevm.failMsg = res.body.error;
                }
            );
        };

        
        timetablevm.dayNameMap = {
            "0": "Nedelja",
            "1": "Ponedeljek",
            "2": "Torek",
            "3": "Sreda",
            "4": "Četrtek",
            "5": "Petek",
            "6": "Sobota"
        };

        timetablevm.dayNameMapArray = [
            {day: 1, name: "Ponedeljek"},
            {day: 2, name: "Torek"},
            {day: 3, name: "Sreda"},
            {day: 4, name: "Četrtek"},
            {day: 5, name: "Petek"},
            {day: 6, name: "Sobota"},
            {day: 0, name: "Nedelja"}
        ];

        timetablevm.colorArray = [
            "limegreen",
            "aquamarine",
            "lightseagreen",
            "skyblue",
            "lightcoral",
            "lightpink",
            "khaki",
            "plum"
        ];

        timetablevm.colorNameToStyle = function(colorname) {
            return {
                "background-color": colorname
            };
        };

        
        timetablevm.sortedSchedule = function(nonSorted) {
            scheduleByDay = {
                "0": [],
                "1": [],
                "2": [],
                "3": [],
                "4": [],
                "5": [],
                "6": []
            };

            // group by days
            nonSorted.forEach(function(item) {
                item = addFunctions(item);
                scheduleByDay[item.dayOfWeek].push(item);
            });

            // sort by hours within a day
            Object.keys(scheduleByDay).map(function(objectKey, index) {
                var daySchedule = scheduleByDay[objectKey];
                
                if (daySchedule.length > 0) {
                    daySchedule.sort(function(item1, item2) {
                        return item1.getTimeStartValue() - item2.getTimeStartValue();
                    });
                    
                    // add time difference for margin
                    daySchedule[0].timeDiff = daySchedule[0].getTimeStartValue();
                    for (var i = 1; i < daySchedule.length; i++) {
                        prev = daySchedule[i - 1];
                        daySchedule[i].timeDiff = daySchedule[i].getTimeStartValue() - prev.getTimeEndValue();
                    }
                }
            });

            return scheduleByDay;
        };

        timetablevm.currentNew = true;
        
        timetablevm.editItem = function(item) {
            console.log(item, typeof(item));
            var data = {};
            data.startTimeHour = null;    
            data.startTimeMinute = null;
            data.endTimeHour = null;
            data.endTimeMinute = null;
            if (item !== null) {
                timetablevm.currentNew = false;
                
                data.name = item.name;                            
                data.startTimeHour = timeHour(item.startTime);    
                data.startTimeMinute = timeMinute(item.startTime);
                data.endTimeHour = timeHour(item.endTime);        
                data.endTimeMinute = timeMinute(item.endTime);    
                data.color = item.color;                          
                data.dayOfWeek = item.dayOfWeek;                  
                data.id = item.id;                                                
            } else {
                timetablevm.currentNew = true;
            }   
            
            data.startTime = function() {return data.startTimeHour + ":" + data.startTimeMinute};
            data.endTime = function() {return data.endTimeHour + ":" + data.endTimeMinute};
            timetablevm.data = data;
            console.log(timetablevm.data);
        };

        timetablevm.saveItem = function() {
            if (timetablevm.currentNew) {
                timetablevm.addItem();
            } else {
                timetablevm.saveEditedItem();
            }
        }

        timetablevm.saveEditedItem = function() {
            var data = {};
            data.name = timetablevm.data.name;
            data.startTime = timetablevm.data.startTime();
            data.endTime = timetablevm.data.endTime();
            data.color = timetablevm.data.color;
            data.dayOfWeek = timetablevm.data.dayOfWeek;
            data.id = timetablevm.data.id;
            
            scheduleService.updateScheduleItem(data).then(
                function success(res) {
                    var updated = {};
                    updated.id = res.data.id;
                    updated.name = res.data.name;
                    updated.startTime = res.data.startTime;
                    updated.endTime = res.data.endTime;
                    updated.userId = res.data.userId;
                    updated.dayOfWeek = res.data.dayOfWeek;
                    updated.color = res.data.color;
                    console.log(updated);
                    for (var index = 0; index < timetablevm.scheduleItemArray.length; index++) {
                        if (timetablevm.scheduleItemArray[index].id == updated.id) {
                            timetablevm.scheduleItemArray[index] = updated;
                        }
                    }

                    updateTimetable();
                },
                function error(res) {
                    timetablevm.failMsg = "Napaka pri posodabljanju!"
                }
            );
        };
    }
        
    timetableCtrl.$inject = ['scheduleService', 'timeHourFilter', 'timeMinuteFilter'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('timetableCtrl', timetableCtrl);
})();

(function() {
    function adminCtrl($routeParams, $scope, $rootScope, notification) {
        var vm = this;


        vm.getAllNotifications = function() {
            notification.getAllNotifications().then(
                function success(res) {
                    vm.message = res.data.length > 0 ? "" : "Ne najdem obvestil.";
                    vm.data = {
                        notifications: res.data
                    };
                },
                function error(res) {
                    vm.message = res.data.msg;
                }
            );
        };

        vm.changeStatus = function(id) {
            let nt = vm.data.notifications.filter(n => n.id == id)[0];
            notification.updateNotification(id, {notification:nt.notification, active:!nt.active}).then( res => {
                vm.data.notifications.forEach((element, index) => {
                    if(element.id === id) {
                        vm.data.notifications[index].active = !vm.data.notifications[index].active;
                    }
                });
            }).catch( err => {
                vm.message = "Ni bilo mozno spremeniti obvestila";
            });
        };

        vm.delete = function(id) {
            notification.deleteNotification(id).then(() => {
                vm.data.notifications = vm.data.notifications.filter(n => n.id != id);
            }).catch( err => {
                vm.message = "Ni bilo mozno odstraniti obvestila";
            });
        };

        vm.addNotification = function() {
            notification.createNotification({notification: vm.data.input.notification}).then( res => {
                vm.data.notifications.push(res.data);
            }).catch( err => {
                vm.message = "Ni bilo mozno dodati obvestila";
            });
        };


        vm.getAllNotifications();
    }

    adminCtrl.$inject = ['$routeParams', '$scope', '$rootScope', 'notification'];

    /* global angular */
    angular
        .module('straightas')
        .controller('adminCtrl', adminCtrl);
})();

(function() {
    function settingsCtrl($scope, $rootScope, $location, authentication) {
        var vm = this;
        vm.oldpasscorrect = false;
        
        vm.validatePassword = function() {
            vm.data = {
              email1 : vm.password1,
              email2 : vm.password2
            };
            if (vm.password1 == vm.password2){
                vm.message="";
                authentication.validatePassword(vm.password1).then(
                    function success(res) {
                        vm.oldpasscorrect = true;
                        vm.userdata = res;
                    },
                    function error(res) {
                        vm.failedLogin = res.data.msg;
                    }
                );
            }else{
                vm.message="Gesla se ne ujamata."
            }
        };
        
        vm.changePassword = function(){
            vm.data = {
                id: vm.userdata.id,
                password: vm.newpassword
            };
            authentication.changePassword(vm.data).then(
                function success(res) {
                    vm.failedLogin = "Geslo uspesno spremenjeno!";
                },
                function error(res) {
                    vm.failedLogin = res.data.msg;
                }
            );
                
            vm.oldpasscorrect = false;
        };
    }
        
    settingsCtrl.$inject = ['$scope', '$rootScope', '$location', 'authentication'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('settingsCtrl', settingsCtrl);
})();

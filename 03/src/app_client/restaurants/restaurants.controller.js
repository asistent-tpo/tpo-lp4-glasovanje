(function() {
    function restaurantsCtrl($routeParams, $scope, $rootScope, $location, $sce, restaurant) {
        var vm = this;
        vm.stations = [];
        vm.arrivals = [];
        vm.showMapFlag = false;

        vm.osmUrl = $sce.trustAsResourceUrl("https://www.openstreetmap.org/export/embed.html?bbox=14.497570395469667%2C46.0496051923787%2C14.50048327445984%2C46.050794708616635&amp;layer=transportmap&amp;marker=46.05019995369992%2C14.499026834964752");
         
        
        vm.getUserLocation = function(){
            navigator.geolocation.getCurrentPosition(function(location) {
                vm.location = location;
                //console.log(location.coords);
                vm.getMap(location.coords.latitude, location.coords.longitude);
            });
        }
        
        vm.getAllRestaurants = function() {
            restaurant.getRestaurants().then(
                function success(res) {
                    vm.message = res.data.length > 0 ? "" : "Ne najdem restavracij.";
                    vm.data = {
                        restaurants: res.data  
                    };
                },
                function error(res) {
                    vm.message = res.data.msg;
                }
            );
        };
        
        vm.getNearestRestaurants = function() {
            if(vm.location){
                restaurant.getNearRestaurants(vm.location.coords.latitude, vm.location.coords.longitude).then(
                    function success(res) {
                        vm.message = res.data.length > 0 ? "" : "Ne najdem restavracij.";
                        vm.data = {
                            restaurants: res.data  
                        };
                    },
                    function error(res) {
                        vm.message = res.data.msg;
                    }
                );
            }else{
                vm.getUserLocation();
            }
        };
        
        vm.getMap = function(lat, lon) {
            if (lat === null || lat === undefined || lon === null || lon === undefined) {
                return;
            }
            
            var latDiff1 = -0.000595108893095;
            var latDiff2 = 0.000595102488396;
            var lonDiff1 = -0.001456439495085;
            var lonDiff2 = 0.001456439495088;

            var bbox1Lat = parseFloat(lat) + latDiff1;
            var bbox1Lon = parseFloat(lon) + lonDiff1;

            var bbox2Lat = parseFloat(lat) + latDiff2;
            var bbox2Lon = parseFloat(lon) + lonDiff2;

            var urlString = "https://www.openstreetmap.org/export/embed.html?bbox="+bbox1Lon+"%2C"+bbox1Lat+"%2C"+bbox2Lon+"%2C"+bbox2Lat+"&amp;layer=mapnik&amp;marker="+lat+"%2C"+lon;

            vm.osmUrl = $sce.trustAsResourceUrl(urlString);
        };
        
        vm.showRestaurantLocation = function(restaurant){
            vm.getMap(restaurant.lat, restaurant.lon);
        };
        
        vm.getUserLocation();
        vm.getAllRestaurants();
    }
        
    restaurantsCtrl.$inject = ['$routeParams', '$scope', '$rootScope', '$location', '$sce', 'restaurant'];
    
    /* global angular */
    angular
    .module('straightas')
    .controller('restaurantsCtrl', restaurantsCtrl);
})();

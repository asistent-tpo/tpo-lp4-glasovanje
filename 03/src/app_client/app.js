(function() {
    function settings($routeProvider, $locationProvider, $sceDelegateProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'login/login.view.html',
                controller: 'loginCtrl',
                controllerAs: 'vm'
            })
            .when('/registration', {
                templateUrl: 'registration/registration.view.html',
                controller: 'registerCtrl',
                controllerAs: 'vm'
            })
            .when('/activation/:hash', {
                templateUrl: 'mail_activation/activation.view.html',
                controller: 'activationCtrl',
                controllerAs: 'vm'
            })
            .when('/restaurants', {
                templateUrl: 'restaurants/restaurants.view.html',
                controller: 'restaurantsCtrl',
                controllerAs: 'vm'
            })
            .when('/timetable', {
                templateUrl: 'timetable/timetablesite.view.html',
                controllerAs: 'vm'                                               
            })
            .when('/todo', {
                templateUrl: 'todo/todosite.view.html',
                controllerAs: 'vm'                                               
            })
            .when('/bus', {
                templateUrl: 'bus/bussite.view.html',
                controllerAs: 'vm'                                               
            })
            .when('/admin', {
                templateUrl: 'admin/admin.view.html',
                controller: 'adminCtrl',
                controllerAs: 'vm'
            })
            .when('/content', {
                templateUrl: 'content/content.view.html',
                controller: 'contentCtrl',
                controllerAs: 'vm'
            })
            .when('/events', {
                templateUrl: 'content/events.view.html',
                controller: 'contentCtrl',
                controllerAs: 'vm'
            })
            .when('/settings', {
                templateUrl: 'settings/settings.view.html',
                controller: 'settingsCtrl',
                controllerAs: 'vm'
            })
            .otherwise({redirectTo: '/login'});
        $locationProvider.html5Mode(true);
    }
    
    run.$inject = ['$rootScope', '$location', '$window', '$route', 'authentication'];
    function run($rootScope, $location, $window, $route, authentication) {
        $rootScope.$on('$locationChangeStart', function(event, newUrl) {
            if( /(\/admin\/)/.test(newUrl) || /(\/settings\/)/.test(newUrl) || /(\/seznam\/)/.test(newUrl) || /(\/home\/)/.test(newUrl) || ( /(\/restaurants\/)/.test(newUrl) || /(\/todo\/)/.test(newUrl) || /(\/timetable\/)/.test(newUrl) ) && !authentication.loggedIn() ) {
                event.preventDefault();
                $location.path('/login');
                $route.reload();
            }
            
            if( ( /(\/content\/)/.test(newUrl) ) && !authentication.contentEditor) { // prepreci dostop navadnim uporabnikom dostop do urejanja dogodkov
                event.preventDefault();
                $location.path('/');
                $route.reload();
            }
            
            if( ( /(\/admin\/)/.test(newUrl) ) && !authentication.admin) { // prepreci dostop navadnim uporabnikom dostop do administratorskih strani
                event.preventDefault();
                $location.path('/');
                $route.reload();
            }
        });
    }

    /* global angular */
    angular
        .module('straightas', ['ngRoute', 'ui.bootstrap'])
        .config(['$routeProvider', '$locationProvider', settings])
        .run(run);
})();

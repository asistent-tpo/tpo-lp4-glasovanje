var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var authentication = jwt({
   secret: process.env.JWT_PASSWORD,
   userProperty: 'payload'
});

var authorization = require('../util/permissionLevels');
var editorPermission = authorization.atLeastEditor;
var administratorPermission = authorization.atLeastAdministrator;

var usersController = require('../controllers/users');
var todoController = require('../controllers/todo');
var scheduleController = require('../controllers/schedule');
var restaurantsController = require('../controllers/restaurants');
var eventsController = require('../controllers/events');
var calendarController = require('../controllers/calendar');
var busController = require('../controllers/bus');
var notificationController = require('../controllers/notification')


router.post('/users/login',         usersController.login);
router.post('/users/register',      usersController.register);
router.get('/users/activate/:hash', usersController.activate);
router.post('/users/changePassword', authentication, usersController.changePassword);

router.post('/notifications', [authentication, administratorPermission], notificationController.createNotification);
router.get('/notifications', authentication, notificationController.getNotifications);
router.patch('/notifications/:id', [authentication, administratorPermission], notificationController.updateNotification);
router.delete('/notifications/:id', [authentication, administratorPermission], notificationController.deleteNotification);

// ToDo
router.post('/todo',       authentication, todoController.createUserTodo); // C
router.get('/todo',        authentication, todoController.getUserTodos);   // R
router.delete('/todo/:id', authentication, todoController.deleteUserTodo); // U
router.patch('/todo/:id',  authentication, todoController.editUserTodo);   // D

// Schedule
router.post('/schedule',       authentication, scheduleController.createUserScheduleEntry); // C
router.get('/schedule',        authentication, scheduleController.getUserScheduleEntry);    // R
router.delete('/schedule/:id', authentication, scheduleController.deleteUserScheduleEntry); // U
router.patch('/schedule/:id',  authentication, scheduleController.editUserScheduleEntry);   // D

// Restaurants
router.get('/restaurants',           restaurantsController.getRestaurantsSortAZ);     // R
router.get('/restaurants/:lat/:lon', restaurantsController.getRestaurantsSortNear);   // R

// Events
router.post('/events',       [authentication, editorPermission] , eventsController.createEvent);  // C
router.get('/events',         authentication,                     eventsController.getAllEvents); // R
router.get('/events/:id',     authentication,                     eventsController.getEvent);     // R
router.delete('/events/:id', [authentication, editorPermission],  eventsController.deleteEvent);  // U
router.patch('/events/:id',  [authentication, editorPermission],  eventsController.editEvent);    // D

// Calendar
router.post('/calendar',        authentication, calendarController.createUserCalendarEntry);  // C
router.get('/calendar',         authentication, calendarController.getUserCalendarEntry);     // R
router.patch('/calendar/:id',   authentication, calendarController.editUserCalendarEntry);    // U
router.delete('/calendar/:id',  authentication, calendarController.deleteUserCalendarEntry);  // D

// BUS
router.get('/bus/coords', busController.getCoordsFile); // R
router.get('/bus/coords/:name', busController.getCoordsFilter); // R
router.get('/bus/:station', busController.getArrivalsForStation); // R
router.get('/bus', busController.getAllStations); // R



module.exports = router;
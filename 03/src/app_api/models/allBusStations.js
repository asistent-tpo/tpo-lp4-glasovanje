module.exports = [
    "7. septembra",
    "Adamičev spomenik",
    "Ajdovščina",
    "Akademija",
    "Ambrožev trg",
    "Ambrus",
    "Amzs",
    "Andreja Bitenca",
    "Anžon",
    "Archinetova",
    "Astra",
    "Aškerčeva",
    "Avtomontaža",
    "Bakrc",
    "Baragova",
    "BARJE",
    "BARJE P+R",
    "BAVARSKI DVOR",
    "Belica",
    "Belinka",
    "Benat",
    "Berglezov štradon",
    "BERIČEVO",
    "Besnica ambulanta",
    "Besnica kamnolom",
    "Besnica/Lj.",
    "Besnica/Lj. Š",
    "Betajnova",
    "Bevke",
    "Bežigrad",
    "Bilečanska",
    "Bistra",
    "BIZOVIK",
    "Blatna Brezovica",
    "Blaževo",
    "Bleiweisova",
    "Blekova vas",
    "Bobrova",
    "Boga vas",
    "Bojanji Vrh",
    "BOKALCE",
    "Bolnica",
    "Bolnica P. Držaja",
    "Bonifacija",
    "Borovnica K",
    "Borovnica Merc.",
    "Borovnica most",
    "Božičeva",
    "Bratislavska",
    "Bratov Babnik",
    "Bratov Učakar",
    "Bratov Židan",
    "Brdo",
    "Breg pri Borovnici",
    "Breg pri Zagradcu",
    "Brekovice",
    "Brest",
    "Brezje pri Dobrovi",
    "Brezje pri Dobrovi And.",
    "Brezje/Dobrepolju",
    "Brezovi Dol",
    "Brezovica pošta",
    "Brezovica pri Ljubljani",
    "Brinje",
    "Briše pri Polh. G.",
    "Briše pri Polh. G. Koroš.",
    "Briše pri Polh. G. M. Voda",
    "BRNČIČEVA",
    "BROD",
    "Brodarjev trg",
    "Bruhanja vas",
    "Brvace",
    "BTC-ATLANTIS",
    "BTC-Emporium",
    "BTC-Kolosej",
    "BTC-Merkur",
    "BTC-tržnica",
    "BTC-uprava",
    "Bukovica Anzel",
    "Bukovica Plevec",
    "Bukovica/Temenici",
    "Butajnova",
    "Butajnova Čemernik",
    "Butajnova Setnik",
    "C. na Vrhovce",
    "Cankarjev dom",
    "Capuder",
    "CENTER STOŽICE P+R",
    "Center Zalog",
    "Cesta na Brod",
    "Cesta na Drenik",
    "Cesta na Drenik 1",
    "Cesta na Drenik 2",
    "Cesta na Vevče",
    "Cesta v Bizovik",
    "Cesta v Gorice",
    "Cesta v Zavoglje",
    "Cesta XV",
    "Cesta/Dobrepolju",
    "Ceste K",
    "Chengdujska",
    "Cikava",
    "Čagošče",
    "Čebelarska",
    "Čerinova",
    "Češnjice pri Zagradcu",
    "Četež",
    "ČRNI VRH",
    "Črni Vrh nad Vrhovci",
    "ČRNUČE",
    "Čušperk",
    "D. Brezovica Baša",
    "D. Brezovica Landovec",
    "D. vas pri Polh. G.",
    "Dalmatinova",
    "Debenc",
    "Dečja vas pri Zagradcu",
    "Dedni Dol",
    "Dob/Šentvidu",
    "Dobrava/Črnučah",
    "Dobrava/Stično",
    "Dobravica K",
    "Dobrepolje OŠ",
    "Dobrova",
    "Dobrova graben",
    "Dobrova Maček",
    "Dobrova Skodlar",
    "Dobrova vodomet",
    "Dobrunje",
    "Dobrunje 2",
    "Dobruša lipa",
    "Dol pri Borovnici Majer",
    "Dol pri Borovnici Pivk",
    "Dolenja vas/Temenici",
    "Dolgi most",
    "Dolgi most P+R",
    "DOLGI MOST P+R",
    "Dolgi most Rotar",
    "Dolinar",
    "Dolnice",
    "Dom obrtnikov",
    "Dom starejših",
    "Dornig",
    "Draga",
    "DRAGOMELJ",
    "Dragomer",
    "Drama",
    "Draveljska gmajna",
    "Dravlje",
    "Draževnik",
    "Drenikova",
    "Drenov Grič",
    "Drobnič",
    "Dveh cesarjev",
    "Dvor",
    "Dvor pri Polh. G.",
    "Elma",
    "Emona",
    "Erbežnik",
    "Flajšmanova",
    "Friškovec",
    "Fužina/Zagradcu",
    "FUŽINE P+R",
    "G. Brezovica",
    "G.vas/Muljavi K",
    "Gabrče",
    "Gabrje pri Dob. Brvičar",
    "Gabrje pri Dobrovi",
    "Gabrovčec K",
    "Gabrovka/Zagradcu",
    "Galjevica",
    "GAMELJNE",
    "Ganter",
    "GARAŽA",
    "Gasilska brigada",
    "Gatina",
    "GD Gameljne",
    "Gerbičeva",
    "Glince",
    "Glinek Gumnišče",
    "Glinek/Škofljici",
    "Glogovica K",
    "Glonarjeva",
    "Gmajna",
    "Golo",
    "Goluhova",
    "Goričane kopališče",
    "Goričane pod Gradom",
    "Goričica pod Krimom",
    "Gorjančeva",
    "Gornje Blato 1",
    "Gornje Blato 2",
    "Gornje Blato Center",
    "Gornje Brezovo",
    "Gornje Poljane",
    "Gornji Rudnik",
    "Gornji trg",
    "Gosposvetska",
    "Gostilna Kovač",
    "Gradišče 1",
    "Gradišče 2",
    "Gradišče 3",
    "Gradišče nad Pijavo G.",
    "Grčarevec",
    "Grintovec/Zagradcu",
    "Griže/Stični",
    "Grm",
    "GROSUPLJE",
    "Grosuplje Motel",
    "Grosuplje OŠ L.A.",
    "Grosuplje SM",
    "Grosuplje/Brezje",
    "Gumnišče 1",
    "Gumnišče center",
    "Gzl",
    "Hajdrihova",
    "Havptmance",
    "Hladnikova",
    "Hočevje",
    "Horjul",
    "Hranilniška",
    "Hrastarija",
    "Hrastenice",
    "Hrastje",
    "Hrastje pri Golem",
    "Hrastov Dol",
    "Hraše center",
    "Hraše vzhod",
    "Hraše zahod",
    "Hruševo",
    "Hruševska",
    "Hrušica",
    "Hrvatski trg",
    "I.C. Šiška",
    "Ig",
    "Ig AP",
    "Ig mizarstvo",
    "Ig Petrol",
    "Ilirska",
    "Ilovica",
    "Imp",
    "Iška",
    "Iška Loka",
    "Iška obračališče",
    "Iška vas",
    "Iška vas obr.",
    "Iška vas šola",
    "Iški most",
    "Ivančna Gorica",
    "Ivančna Gorica ŠC",
    "Ivane Kobilca",
    "Ižanska",
    "Ižica",
    "Jadranska",
    "Jamnikarjeva",
    "Jana Husa",
    "Japelj",
    "Jarše",
    "Javor pri Sostrem K.",
    "Javor/K",
    "Jerinov grič",
    "JEZERO",
    "Ježa",
    "JEŽICA",
    "Ježica",
    "JEŽICA P+R",
    "Ježkova",
    "Jurčkova",
    "Kajakaška",
    "Kajuhova",
    "Kal/Ambrusu",
    "Kal/Ambrusu K",
    "Kalce/Logatcu",
    "Kališnikov trg",
    "KAMNA GORICA",
    "Kamni Vrh",
    "Kamnik pod Krimom",
    "Kamnik pod Krimom K.",
    "Kamnogoriška",
    "Kamnolom Rovte",
    "Kardeljeva ploščad",
    "Kavčičeva",
    "Kino Šiška",
    "Kitni Vrh",
    "Klinični center",
    "Kneza Koclja",
    "Knjižnica Grosuplje",
    "Kobiljek/Zagradcu",
    "KODELJEVO",
    "Kodeljevo",
    "Kodrova",
    "KOLODVOR",
    "Kolodvor Črnuče",
    "Komanija",
    "Komenda Š",
    "Komenda vas",
    "Kompas",
    "Kompolje/Dobrepolju",
    "Kongo",
    "Kongresni trg",
    "Konzorcij",
    "Koprska",
    "Korinj",
    "Koseze",
    "Koseze Stare",
    "Kosmačeva",
    "Kot",
    "Kovinarska",
    "Kozarje",
    "Kozarje G.D.",
    "Kozler",
    "Kožljevec",
    "Kpl",
    "Kranjčeva",
    "Krimska",
    "Kriška vas",
    "Krivec",
    "Križanke",
    "Križišče Sostro",
    "Krka",
    "Krošljev grič",
    "Krožna pot",
    "Krpan",
    "Kržičeva",
    "KS Pirniče",
    "Kulturni center Vrhnika",
    "Kurešček K",
    "Kurja vas/Šentjoštu",
    "Kužljevec K",
    "Lahova pot",
    "Lahovče",
    "Lanišče",
    "Lavrica",
    "Lavrica OŠ",
    "Lavrica pri Malči",
    "Laze/Krko",
    "Leskoškova",
    "Leskovec/Viš.G.",
    "Lesno Brdo",
    "Letališče Brnik",
    "LETALIŠKA",
    "Letališka",
    "Ligojna K",
    "Lipe",
    "LITOSTROJ",
    "Litostrojska",
    "Livada",
    "Ljubgojna",
    "LJUBLJANA AP",
    "Ljubljana Dobrunje",
    "Ljubljana Tivolska",
    "Ljubljana Zadvor",
    "Ljubljanska",
    "Ljubljanske brigade",
    "Ljubljanski grad",
    "Lobček",
    "Log pri Brez.Mole",
    "Log pri Brez.vas",
    "Log pri Polh G.",
    "Log/Zavratec",
    "Logar",
    "Logatec",
    "Logatec Krpan",
    "Log-Dragomer OŠ",
    "Loka pri Mengšu",
    "Lučarjev Kal",
    "Luče Na Šoli",
    "Lukovica pri Brezovici",
    "Lutkovno gl.",
    "M. Lipljene",
    "M. St. vas",
    "M. vas pri Grosupljem",
    "M.Hudo",
    "M.Ilova GORA",
    "M.Ločnik/Turjaku",
    "M.Mlačevo",
    "M.Račna",
    "Mačkov graben",
    "Mala Dobrava",
    "Mala Ligojna",
    "Male Dole K",
    "Male Kompolje",
    "Male Vrhe",
    "MALI LIPOGLAV",
    "Mali Vrh",
    "Marinča vas",
    "Marinčev grič",
    "Maroltova",
    "Martinova",
    "Matena",
    "Medno",
    "MEDVODE",
    "Medvode A.P.",
    "Medvode na Klancu",
    "MEDVODE naselje",
    "Mekinje/Stično",
    "Mengeš Lovec",
    "Mengeš Pavovec",
    "Mengeš Petrol",
    "Mercator",
    "MESTNA HIŠA",
    "MESTNI LOG",
    "Metnaj",
    "Mevce",
    "Mihov štradon",
    "Mirje",
    "Mizni Dol",
    "Mizni Dol II",
    "Mlake",
    "Mleščevo",
    "Mlin/Rovtah",
    "Moste Petrol",
    "Mostiček",
    "Mostovna",
    "Mrzle njive",
    "Mrzlo polje",
    "Muljava",
    "Muljava OŠ",
    "Murgle",
    "Murkova",
    "Na Gmajni",
    "Na Gmajnici",
    "Na jami",
    "Na klancu",
    "Na Pili",
    "Na Žalah",
    "Nadgorica",
    "Nasovče",
    "Navje",
    "Noč",
    "NOTRANJE GORICE",
    "Nova vas/Viš.G.",
    "Nova vas/Žireh",
    "Novaki",
    "Nove Jarše",
    "NOVE JARŠE",
    "NOVE STOŽICE P+R",
    "Nove Žale",
    "Novo naselje",
    "Novo Polje",
    "NS RUDNIK",
    "NS Rudnik",
    "O.š. Brinje",
    "O.š. Jezero",
    "O.š. Maksa Pečarja",
    "O.š. Sostro",
    "Ob daljnovodu",
    "Ob sotočju",
    "Obrtna cona",
    "Omersova",
    "Opekarska",
    "Opera",
    "Orel/Žireh",
    "Orle GD",
    "Orle K",
    "Orlova",
    "Osenjakova",
    "Oslica",
    "OŠ Kopanje",
    "OŠ L.A. Tovarniška",
    "Oval",
    "Padež/Borovnici",
    "Pako",
    "Pance K",
    "Parlament",
    "Parmova",
    "Pece K",
    "Peč pri Polici",
    "Perovo",
    "Peruzzijeva",
    "Peščenik/Viš.G.",
    "Petačev graben",
    "Petkovec",
    "Petkovec Zavčan",
    "Petrol",
    "Pijava Gorica",
    "Plečnikova cerkev",
    "Plečnikova hiša",
    "Plešičeva",
    "Plešivica pri Žalni",
    "Plešivica/Logu grad",
    "Plešivica/Logu spomenik",
    "Pod Golovcem",
    "Pod gozdom",
    "Pod Hribom",
    "Pod Kamno Gorico",
    "Pod Rožnikom",
    "Pod Urhom",
    "Podboršt/Šentvidu",
    "Podgora",
    "Podgora pri Verdu",
    "Podgora/Dobrepolju K",
    "Podgorica",
    "Podgozd",
    "Podklanec/Žireh",
    "Podlesec/Žireh",
    "Podlipa",
    "Podlipa hrast",
    "Podlipa Kovtrov z.",
    "Podlipa Podlogar",
    "Podlipa pri tran.",
    "Podlipa Stope",
    "Podlipa Vičič",
    "Podlipa Zukancov z.",
    "Podlipa Županov z.",
    "Podlipoglav",
    "Podmilščakova",
    "Podmornica",
    "Podolnica",
    "Podpeč",
    "Podpeč 1",
    "Podpeč/Dobrepolju",
    "Podpeč/Lj.",
    "Podpeč/Lj. Jereb",
    "Podreber/Polh. G.",
    "Podsmreka GD",
    "Podsmreka pri Gorjancu",
    "Podsmreka pri Višnji G.",
    "Podsmreka Rotar",
    "Podtabor/Strugah",
    "PODUTIK",
    "Podvozna pot",
    "Pohorskega bataljona",
    "Pokojišče/Borovnici",
    "Pokopališka",
    "Polanškova",
    "POLHOV GRADEC",
    "POLICA/VIŠ. G.",
    "Poliklinika",
    "Poljanska",
    "Polje",
    "Polje obračališče",
    "Polje-kolodvor",
    "Polževo K",
    "Ponikve/Dobrepolju",
    "Ponikve/Dobrepolju Inv.d.",
    "Ponova vas",
    "Pošta",
    "Pošta Dobrunje",
    "Pot na Fužine",
    "Pot na Rakovo jelšo",
    "Pot R. križa",
    "Pot v Hrastovec",
    "Pot v Smrečje",
    "Potiskavec",
    "Povodje",
    "Praproče",
    "Praproče pri Temenici",
    "Predole K",
    "Predstruge",
    "Preglov trg",
    "Prekmurska",
    "Preserje pod Krimom",
    "Preska",
    "Preval",
    "Prevalje pod Krimom",
    "Prežganje Š.",
    "Prežganje Š. K.",
    "Pri Dogonivar",
    "Pri Dolencu",
    "Pri Jeršinu",
    "Pri Maranzu",
    "Pri Poku",
    "Prilesje/Ilovi Gori",
    "Primča vas",
    "Primožev ovinek",
    "Primožičeva",
    "Privoz",
    "Prušnikova",
    "PRŽAN",
    "Puhova",
    "Pungart/Temenici",
    "Pusti Javor",
    "Račeva",
    "Radanja vas",
    "Radna",
    "Radohova vas/Šentvidu",
    "Raj nad mestom",
    "RAKITNA",
    "Rakovnik",
    "Rakovnik pri Sori",
    "Rapljevo/Strugah",
    "Rast",
    "Rašica",
    "Rašica V",
    "Razdrto",
    "Razori K.",
    "Razori Rogovilc",
    "Razpotje",
    "Razstavišče",
    "Rdeči Kal/Šentvidu",
    "Reaktor",
    "Reber pri Škofljici",
    "Remiza",
    "Repnje Benk",
    "Repnje kapelica",
    "Repnje samostan",
    "Rog",
    "Rogovilc",
    "Roška",
    "Rovte",
    "Rovte K",
    "Rožičeva",
    "Rožna dolina",
    "Rudnik",
    "RUDNIK",
    "Rusjanov trg",
    "Ruski car",
    "Rutar",
    "Sad",
    "Sadinja vas",
    "Samova",
    "Sap",
    "Saturnus",
    "Sava",
    "SAVLJE",
    "Savske stolpnice",
    "Savsko naselje",
    "Sela",
    "Sela/Viš.G.",
    "Selo mali Dunaj",
    "Selo pri Pancah K",
    "Selo pri Radohovi vasi",
    "Selo pri Vodicah",
    "Selo/Dobu",
    "Senožeti/Viš.G.",
    "Silos",
    "Sinja Gorica",
    "Sinja Gorica avt.",
    "Sinja Gorica Oblak",
    "Sinja Gorica opekarna",
    "Skaručna",
    "Skaručna Jurjevc",
    "Slovenija avto",
    "Slovenijales",
    "Smelt",
    "Smlednik šola",
    "Smolnik nad hribom",
    "Smolnik Praprotnik",
    "Smolnik Setnik",
    "Smolnik Zalog",
    "Smolnik žaga",
    "Smrečje/Vrh.pri Kapelici",
    "Smrečje/Vrhniko",
    "Smrečje/Vrhniko pri Mihu",
    "Smrečje/Vrhniko Samija",
    "Smrečje/Vrhniko vrh",
    "Smrekarjevo",
    "Smrjene",
    "Smrjene 1",
    "Smrjene 2",
    "Smrjene 3",
    "Smrjene 4",
    "Sneberje",
    "Snoj",
    "Sobrače",
    "Sora",
    "SORA obračališče",
    "Sora OŠ",
    "SOSTRO",
    "Sp. Brnik",
    "Sp. Gameljne 1",
    "Sp. Gameljne 2",
    "Sp. Gameljne 3",
    "Sp. Pirniče",
    "Sp. Slivnica",
    "Sp.Blato",
    "Sp.Brezovo",
    "Sp.Duplice",
    "Spar",
    "Spodnji Rudnik",
    "Sr. vas pri Polh. G.",
    "Središka",
    "St.trg/Višnji G.",
    "St.Vrhnika K",
    "Stadion",
    "Staje",
    "Stan in dom",
    "STANEŽIČE",
    "Stara cerkev",
    "Stara pošta",
    "Stična K",
    "Stična KZ",
    "Stožice",
    "Strahomer",
    "Stranska vas/Viš.G.",
    "Strelišče",
    "Streliška",
    "Strmca 2",
    "Strmica/Vrhniki",
    "Struge OŠ",
    "Studenec",
    "Studenec/Iv.Gorici",
    "SUHI DOL/Planino",
    "Sušica/Muljavi",
    "Svetčeva",
    "Svetje",
    "Šemnikar",
    "Šentjakob",
    "ŠENTJOŠT",
    "Šentvid",
    "Šentvid pri Stični",
    "Šentvid pri Stični K",
    "Šentvid pri Stični OŠ F.V",
    "Šentvid pri Stični ŽP",
    "Šinkov Turn",
    "Šinkov Turn hrib",
    "Šišenska",
    "Škocjan",
    "Škoflje/Šentvidu",
    "ŠKOFLJICA",
    "Škofljica Javornik",
    "Škofljica Petkovšek",
    "Škofljica Špica",
    "Škofljica žaga",
    "Škrilje",
    "Škrilje 2",
    "Škrjanče/Iv.G.",
    "Škrjanka",
    "Šlandrova",
    "Šmarje",
    "Šmartno",
    "Šmartno pod Šmarno g.",
    "Šola Jarše",
    "Šolska",
    "Špica",
    "Špilar",
    "Št. Jurij",
    "Štajerska",
    "Štepanja vas",
    "ŠTEPANJSKO NASELJE",
    "Študentsko naselje",
    "Šujica",
    "Tabor",
    "Tacen",
    "Tacenski most",
    "Tavčarjeva",
    "Tbilisijska",
    "Tehnounion",
    "Temenica",
    "Temenica OŠ",
    "Tiki",
    "Tisovec/Strugah",
    "Tivoli",
    "Tivolska",
    "Tobačna",
    "TOMAČEVO",
    "Tomačevska",
    "Tomišelj",
    "Tomišelj A",
    "Tomišelj K",
    "Toplarna",
    "Topniška",
    "Topole/Mengšu",
    "TOVARNA LEK",
    "Tovorni kolodvor",
    "Trata",
    "Trbeže",
    "Trebeljevo",
    "TRNOVO",
    "Trnovski pristan",
    "Troščine",
    "Trzin",
    "Trzin ind. cona",
    "Trzin Mlake",
    "Trzin vas",
    "Tržič/Strugah",
    "Tržnica Koseze",
    "Tržnica Moste",
    "TUJI GRM",
    "Turist",
    "Turjak",
    "Turnovšče",
    "Ulovka",
    "Utik",
    "V. Lese",
    "V. Lipljene",
    "V. Loka pri Viš. G.",
    "V. Mlačevo",
    "V. Mlačevo ŽP",
    "V. St. vas",
    "V.Ligojna",
    "V.Račna",
    "Valburga",
    "Valična vas",
    "Vaše",
    "Vaše Helios",
    "Večna pot",
    "Velika Dobrava",
    "Velika Ilova gora",
    "Velike Kompolje",
    "Velike Pece",
    "Veliki Lipoglav",
    "Veliki štradon",
    "Veliko Črnelo",
    "Verd K",
    "Verje",
    "Vesca/Vodicah",
    "VEVČE",
    "Vevče papirnica",
    "Viadukt",
    "Vič",
    "Videm/Dobrepolju",
    "Vikrče",
    "Vir pri Stični",
    "Visoko/Kureščkom",
    "Viško polje",
    "Višnja Gora",
    "Višnja Gora OŠ",
    "Višnje/Ambrusu",
    "VIŽMARJE",
    "VODICE",
    "Vodice K.",
    "Vodice O.Š.",
    "Vodičar",
    "Vodovodna",
    "Vojsko/Vodicah",
    "Volavlje",
    "Vopovlje",
    "Vrbljene",
    "Vrh nad Viš.G.",
    "Vrh nad Želimljami",
    "Vrh nad Želimljami 1",
    "Vrh nad Želimljami 2",
    "Vrh nad Želimljami GD",
    "Vrhnika",
    "Vrhnika hrib",
    "Vrhnika Jazon",
    "Vrhnika OŠ AM Slomška",
    "Vrhnika OŠ Ivan Cankar Lošca",
    "Vrhnika OŠ Ivana Cankarja",
    "Vrhnika samop.",
    "Vrhnika Voljčeva",
    "Vrhnika vrt.",
    "Vrhovci",
    "Vrzdenec",
    "Yulon",
    "Zaboršt/Šentvidu",
    "Zadobrova",
    "ZADOBROVA",
    "Zadvor",
    "Zadvor 2",
    "Zadvor pri Grosupljem",
    "Zagorica/Dobrepolju",
    "Zagradec OŠ",
    "Zagradec pri Grosupljem",
    "Zagradec/Krki",
    "Zaklanec/Horjulu",
    "ZALOG",
    "Zaloška",
    "Zaplana",
    "Zaplana cerkev",
    "Zaplana Jamnik",
    "Zaplana Jazbar",
    "Zaplana Jerinov grič",
    "Zaplana ovinek",
    "Zaplana žaga",
    "Zapoge",
    "Zapoge obračališče",
    "ZAPOTOK",
    "Zapotok/Kureščkom K",
    "Zapuže",
    "Zasavska",
    "Zavrh",
    "Zavrh/Borovnici",
    "Zbilje Jezero",
    "Zbilje K",
    "Zbiljski Gaj",
    "ZD Polje",
    "Zdenska vas",
    "Zdravstveni dom Gros.",
    "ZELENA JAMA",
    "Zeleni gaj",
    "Zg. Besnica",
    "Zg. Gameljne",
    "Zg. Pirniče",
    "Zgornja Draga",
    "Zgornja Šiška",
    "Zgornji log",
    "Zidarjevec",
    "Zidarjevec-2",
    "Ziherlova",
    "Zmajski most",
    "Znojilje K",
    "Žabja vas",
    "Žale",
    "Žalna",
    "Žalna ŽP",
    "Žažar",
    "ŽELEZNA",
    "ŽELIMLJE",
    "Želimlje Nučič",
    "Želimlje Rogovilec",
    "Žibrše K",
    "Žiri",
    "Žirovnik",
    "Žito",
    "Živalski vrt"
 ]
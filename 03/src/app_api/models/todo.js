module.exports = (sequelize, type) => {
    return sequelize.define('todo', {
        id : {
            type : type.INTEGER,
            autoIncrement : true,
            primaryKey  : true
        },
        userId : {
            type : type.INTEGER,
            references : {
                model : sequelize.models.user,
                key : 'id',
                deferrable: type.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        name : {
            type : type.TEXT,
            allowNull : false
        }
    });
};
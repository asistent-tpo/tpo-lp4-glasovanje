module.exports = (sequelize, type) => {
    return sequelize.define('system_notification', {
        id : {
            type : type.INTEGER,
            autoIncrement : true,
            primaryKey  : true
        },
        notification : {
            type : type.TEXT,
            allowNull : false
        },
        active : {
            type : type.BOOLEAN,
            defaultValue : true,
            allowNull : false
        }
    });
};
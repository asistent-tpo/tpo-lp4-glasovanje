module.exports = [{
    "name": "7. septembra",
    "ascii": "7. septembra"
}, {
    "name": "Adamičev spomenik",
    "ascii": "Adamicev spomenik"
}, {
    "name": "Ajdovščina",
    "ascii": "Ajdovscina"
}, {
    "name": "Akademija",
    "ascii": "Akademija"
}, {
    "name": "Ambrožev trg",
    "ascii": "Ambrozev trg"
}, {
    "name": "Ambrus",
    "ascii": "Ambrus"
}, {
    "name": "Amzs",
    "ascii": "Amzs"
}, {
    "name": "Andreja Bitenca",
    "ascii": "Andreja Bitenca"
}, {
    "name": "Anžon",
    "ascii": "Anzon"
}, {
    "name": "Archinetova",
    "ascii": "Archinetova"
}, {
    "name": "Astra",
    "ascii": "Astra"
}, {
    "name": "Aškerčeva",
    "ascii": "Askerceva"
}, {
    "name": "Avtomontaža",
    "ascii": "Avtomontaza"
}, {
    "name": "Bakrc",
    "ascii": "Bakrc"
}, {
    "name": "Baragova",
    "ascii": "Baragova"
}, {
    "name": "BARJE",
    "ascii": "BARJE"
}, {
    "name": "BARJE P+R",
    "ascii": "BARJE P+R"
}, {
    "name": "BAVARSKI DVOR",
    "ascii": "BAVARSKI DVOR"
}, {
    "name": "Belica",
    "ascii": "Belica"
}, {
    "name": "Belinka",
    "ascii": "Belinka"
}, {
    "name": "Benat",
    "ascii": "Benat"
}, {
    "name": "Berglezov štradon",
    "ascii": "Berglezov stradon"
}, {
    "name": "BERIČEVO",
    "ascii": "BERICEVO"
}, {
    "name": "Besnica ambulanta",
    "ascii": "Besnica ambulanta"
}, {
    "name": "Besnica kamnolom",
    "ascii": "Besnica kamnolom"
}, {
    "name": "Besnica/Lj.",
    "ascii": "Besnica/Lj."
}, {
    "name": "Besnica/Lj. Š",
    "ascii": "Besnica/Lj. S"
}, {
    "name": "Betajnova",
    "ascii": "Betajnova"
}, {
    "name": "Bevke",
    "ascii": "Bevke"
}, {
    "name": "Bežigrad",
    "ascii": "Bezigrad"
}, {
    "name": "Bilečanska",
    "ascii": "Bilecanska"
}, {
    "name": "Bistra",
    "ascii": "Bistra"
}, {
    "name": "BIZOVIK",
    "ascii": "BIZOVIK"
}, {
    "name": "Blatna Brezovica",
    "ascii": "Blatna Brezovica"
}, {
    "name": "Blaževo",
    "ascii": "Blazevo"
}, {
    "name": "Bleiweisova",
    "ascii": "Bleiweisova"
}, {
    "name": "Blekova vas",
    "ascii": "Blekova vas"
}, {
    "name": "Bobrova",
    "ascii": "Bobrova"
}, {
    "name": "Boga vas",
    "ascii": "Boga vas"
}, {
    "name": "Bojanji Vrh",
    "ascii": "Bojanji Vrh"
}, {
    "name": "BOKALCE",
    "ascii": "BOKALCE"
}, {
    "name": "Bolnica",
    "ascii": "Bolnica"
}, {
    "name": "Bolnica P. Držaja",
    "ascii": "Bolnica P. Drzaja"
}, {
    "name": "Bonifacija",
    "ascii": "Bonifacija"
}, {
    "name": "Borovnica K",
    "ascii": "Borovnica K"
}, {
    "name": "Borovnica Merc.",
    "ascii": "Borovnica Merc."
}, {
    "name": "Borovnica most",
    "ascii": "Borovnica most"
}, {
    "name": "Božičeva",
    "ascii": "Boziceva"
}, {
    "name": "Bratislavska",
    "ascii": "Bratislavska"
}, {
    "name": "Bratov Babnik",
    "ascii": "Bratov Babnik"
}, {
    "name": "Bratov Učakar",
    "ascii": "Bratov Ucakar"
}, {
    "name": "Bratov Židan",
    "ascii": "Bratov Zidan"
}, {
    "name": "Brdo",
    "ascii": "Brdo"
}, {
    "name": "Breg pri Borovnici",
    "ascii": "Breg pri Borovnici"
}, {
    "name": "Breg pri Zagradcu",
    "ascii": "Breg pri Zagradcu"
}, {
    "name": "Brekovice",
    "ascii": "Brekovice"
}, {
    "name": "Brest",
    "ascii": "Brest"
}, {
    "name": "Brezje pri Dobrovi",
    "ascii": "Brezje pri Dobrovi"
}, {
    "name": "Brezje pri Dobrovi And.",
    "ascii": "Brezje pri Dobrovi And."
}, {
    "name": "Brezje/Dobrepolju",
    "ascii": "Brezje/Dobrepolju"
}, {
    "name": "Brezovi Dol",
    "ascii": "Brezovi Dol"
}, {
    "name": "Brezovica pošta",
    "ascii": "Brezovica posta"
}, {
    "name": "Brezovica pri Ljubljani",
    "ascii": "Brezovica pri Ljubljani"
}, {
    "name": "Brinje",
    "ascii": "Brinje"
}, {
    "name": "Briše pri Polh. G.",
    "ascii": "Brise pri Polh. G."
}, {
    "name": "Briše pri Polh. G. Koroš.",
    "ascii": "Brise pri Polh. G. Koros."
}, {
    "name": "Briše pri Polh. G. M. Voda",
    "ascii": "Brise pri Polh. G. M. Voda"
}, {
    "name": "BRNČIČEVA",
    "ascii": "BRNCICEVA"
}, {
    "name": "BROD",
    "ascii": "BROD"
}, {
    "name": "Brodarjev trg",
    "ascii": "Brodarjev trg"
}, {
    "name": "Bruhanja vas",
    "ascii": "Bruhanja vas"
}, {
    "name": "Brvace",
    "ascii": "Brvace"
}, {
    "name": "BTC-ATLANTIS",
    "ascii": "BTC-ATLANTIS"
}, {
    "name": "BTC-Emporium",
    "ascii": "BTC-Emporium"
}, {
    "name": "BTC-Kolosej",
    "ascii": "BTC-Kolosej"
}, {
    "name": "BTC-Merkur",
    "ascii": "BTC-Merkur"
}, {
    "name": "BTC-tržnica",
    "ascii": "BTC-trznica"
}, {
    "name": "BTC-uprava",
    "ascii": "BTC-uprava"
}, {
    "name": "Bukovica Anzel",
    "ascii": "Bukovica Anzel"
}, {
    "name": "Bukovica Plevec",
    "ascii": "Bukovica Plevec"
}, {
    "name": "Bukovica/Temenici",
    "ascii": "Bukovica/Temenici"
}, {
    "name": "Butajnova",
    "ascii": "Butajnova"
}, {
    "name": "Butajnova Čemernik",
    "ascii": "Butajnova Cemernik"
}, {
    "name": "Butajnova Setnik",
    "ascii": "Butajnova Setnik"
}, {
    "name": "C. na Vrhovce",
    "ascii": "C. na Vrhovce"
}, {
    "name": "Cankarjev dom",
    "ascii": "Cankarjev dom"
}, {
    "name": "Capuder",
    "ascii": "Capuder"
}, {
    "name": "CENTER STOŽICE P+R",
    "ascii": "CENTER STOZICE P+R"
}, {
    "name": "Center Zalog",
    "ascii": "Center Zalog"
}, {
    "name": "Cesta na Brod",
    "ascii": "Cesta na Brod"
}, {
    "name": "Cesta na Drenik",
    "ascii": "Cesta na Drenik"
}, {
    "name": "Cesta na Drenik 1",
    "ascii": "Cesta na Drenik 1"
}, {
    "name": "Cesta na Drenik 2",
    "ascii": "Cesta na Drenik 2"
}, {
    "name": "Cesta na Vevče",
    "ascii": "Cesta na Vevce"
}, {
    "name": "Cesta v Bizovik",
    "ascii": "Cesta v Bizovik"
}, {
    "name": "Cesta v Gorice",
    "ascii": "Cesta v Gorice"
}, {
    "name": "Cesta v Zavoglje",
    "ascii": "Cesta v Zavoglje"
}, {
    "name": "Cesta XV",
    "ascii": "Cesta XV"
}, {
    "name": "Cesta/Dobrepolju",
    "ascii": "Cesta/Dobrepolju"
}, {
    "name": "Ceste K",
    "ascii": "Ceste K"
}, {
    "name": "Chengdujska",
    "ascii": "Chengdujska"
}, {
    "name": "Cikava",
    "ascii": "Cikava"
}, {
    "name": "Čagošče",
    "ascii": "Cagosce"
}, {
    "name": "Čebelarska",
    "ascii": "Cebelarska"
}, {
    "name": "Čerinova",
    "ascii": "Cerinova"
}, {
    "name": "Češnjice pri Zagradcu",
    "ascii": "Cesnjice pri Zagradcu"
}, {
    "name": "Četež",
    "ascii": "Cetez"
}, {
    "name": "ČRNI VRH",
    "ascii": "CRNI VRH"
}, {
    "name": "Črni Vrh nad Vrhovci",
    "ascii": "Crni Vrh nad Vrhovci"
}, {
    "name": "ČRNUČE",
    "ascii": "CRNUCE"
}, {
    "name": "Čušperk",
    "ascii": "Cusperk"
}, {
    "name": "D. Brezovica Baša",
    "ascii": "D. Brezovica Basa"
}, {
    "name": "D. Brezovica Landovec",
    "ascii": "D. Brezovica Landovec"
}, {
    "name": "D. vas pri Polh. G.",
    "ascii": "D. vas pri Polh. G."
}, {
    "name": "Dalmatinova",
    "ascii": "Dalmatinova"
}, {
    "name": "Debenc",
    "ascii": "Debenc"
}, {
    "name": "Dečja vas pri Zagradcu",
    "ascii": "Decja vas pri Zagradcu"
}, {
    "name": "Dedni Dol",
    "ascii": "Dedni Dol"
}, {
    "name": "Dob/Šentvidu",
    "ascii": "Dob/Sentvidu"
}, {
    "name": "Dobrava/Črnučah",
    "ascii": "Dobrava/Crnucah"
}, {
    "name": "Dobrava/Stično",
    "ascii": "Dobrava/Sticno"
}, {
    "name": "Dobravica K",
    "ascii": "Dobravica K"
}, {
    "name": "Dobrepolje OŠ",
    "ascii": "Dobrepolje OS"
}, {
    "name": "Dobrova",
    "ascii": "Dobrova"
}, {
    "name": "Dobrova graben",
    "ascii": "Dobrova graben"
}, {
    "name": "Dobrova Maček",
    "ascii": "Dobrova Macek"
}, {
    "name": "Dobrova Skodlar",
    "ascii": "Dobrova Skodlar"
}, {
    "name": "Dobrova vodomet",
    "ascii": "Dobrova vodomet"
}, {
    "name": "Dobrunje",
    "ascii": "Dobrunje"
}, {
    "name": "Dobrunje 2",
    "ascii": "Dobrunje 2"
}, {
    "name": "Dobruša lipa",
    "ascii": "Dobrusa lipa"
}, {
    "name": "Dol pri Borovnici Majer",
    "ascii": "Dol pri Borovnici Majer"
}, {
    "name": "Dol pri Borovnici Pivk",
    "ascii": "Dol pri Borovnici Pivk"
}, {
    "name": "Dolenja vas/Temenici",
    "ascii": "Dolenja vas/Temenici"
}, {
    "name": "Dolgi most",
    "ascii": "Dolgi most"
}, {
    "name": "Dolgi most P+R",
    "ascii": "Dolgi most P+R"
}, {
    "name": "DOLGI MOST P+R",
    "ascii": "DOLGI MOST P+R"
}, {
    "name": "Dolgi most Rotar",
    "ascii": "Dolgi most Rotar"
}, {
    "name": "Dolinar",
    "ascii": "Dolinar"
}, {
    "name": "Dolnice",
    "ascii": "Dolnice"
}, {
    "name": "Dom obrtnikov",
    "ascii": "Dom obrtnikov"
}, {
    "name": "Dom starejših",
    "ascii": "Dom starejsih"
}, {
    "name": "Dornig",
    "ascii": "Dornig"
}, {
    "name": "Draga",
    "ascii": "Draga"
}, {
    "name": "DRAGOMELJ",
    "ascii": "DRAGOMELJ"
}, {
    "name": "Dragomer",
    "ascii": "Dragomer"
}, {
    "name": "Drama",
    "ascii": "Drama"
}, {
    "name": "Draveljska gmajna",
    "ascii": "Draveljska gmajna"
}, {
    "name": "Dravlje",
    "ascii": "Dravlje"
}, {
    "name": "Draževnik",
    "ascii": "Drazevnik"
}, {
    "name": "Drenikova",
    "ascii": "Drenikova"
}, {
    "name": "Drenov Grič",
    "ascii": "Drenov Gric"
}, {
    "name": "Drobnič",
    "ascii": "Drobnic"
}, {
    "name": "Dveh cesarjev",
    "ascii": "Dveh cesarjev"
}, {
    "name": "Dvor",
    "ascii": "Dvor"
}, {
    "name": "Dvor pri Polh. G.",
    "ascii": "Dvor pri Polh. G."
}, {
    "name": "Elma",
    "ascii": "Elma"
}, {
    "name": "Emona",
    "ascii": "Emona"
}, {
    "name": "Erbežnik",
    "ascii": "Erbeznik"
}, {
    "name": "Flajšmanova",
    "ascii": "Flajsmanova"
}, {
    "name": "Friškovec",
    "ascii": "Friskovec"
}, {
    "name": "Fužina/Zagradcu",
    "ascii": "Fuzina/Zagradcu"
}, {
    "name": "FUŽINE P+R",
    "ascii": "FUZINE P+R"
}, {
    "name": "G. Brezovica",
    "ascii": "G. Brezovica"
}, {
    "name": "G.vas/Muljavi K",
    "ascii": "G.vas/Muljavi K"
}, {
    "name": "Gabrče",
    "ascii": "Gabrce"
}, {
    "name": "Gabrje pri Dob. Brvičar",
    "ascii": "Gabrje pri Dob. Brvicar"
}, {
    "name": "Gabrje pri Dobrovi",
    "ascii": "Gabrje pri Dobrovi"
}, {
    "name": "Gabrovčec K",
    "ascii": "Gabrovcec K"
}, {
    "name": "Gabrovka/Zagradcu",
    "ascii": "Gabrovka/Zagradcu"
}, {
    "name": "Galjevica",
    "ascii": "Galjevica"
}, {
    "name": "GAMELJNE",
    "ascii": "GAMELJNE"
}, {
    "name": "Ganter",
    "ascii": "Ganter"
}, {
    "name": "GARAŽA",
    "ascii": "GARAZA"
}, {
    "name": "Gasilska brigada",
    "ascii": "Gasilska brigada"
}, {
    "name": "Gatina",
    "ascii": "Gatina"
}, {
    "name": "GD Gameljne",
    "ascii": "GD Gameljne"
}, {
    "name": "Gerbičeva",
    "ascii": "Gerbiceva"
}, {
    "name": "Glince",
    "ascii": "Glince"
}, {
    "name": "Glinek Gumnišče",
    "ascii": "Glinek Gumnisce"
}, {
    "name": "Glinek/Škofljici",
    "ascii": "Glinek/Skofljici"
}, {
    "name": "Glogovica K",
    "ascii": "Glogovica K"
}, {
    "name": "Glonarjeva",
    "ascii": "Glonarjeva"
}, {
    "name": "Gmajna",
    "ascii": "Gmajna"
}, {
    "name": "Golo",
    "ascii": "Golo"
}, {
    "name": "Goluhova",
    "ascii": "Goluhova"
}, {
    "name": "Goričane kopališče",
    "ascii": "Goricane kopalisce"
}, {
    "name": "Goričane pod Gradom",
    "ascii": "Goricane pod Gradom"
}, {
    "name": "Goričica pod Krimom",
    "ascii": "Goricica pod Krimom"
}, {
    "name": "Gorjančeva",
    "ascii": "Gorjanceva"
}, {
    "name": "Gornje Blato 1",
    "ascii": "Gornje Blato 1"
}, {
    "name": "Gornje Blato 2",
    "ascii": "Gornje Blato 2"
}, {
    "name": "Gornje Blato Center",
    "ascii": "Gornje Blato Center"
}, {
    "name": "Gornje Brezovo",
    "ascii": "Gornje Brezovo"
}, {
    "name": "Gornje Poljane",
    "ascii": "Gornje Poljane"
}, {
    "name": "Gornji Rudnik",
    "ascii": "Gornji Rudnik"
}, {
    "name": "Gornji trg",
    "ascii": "Gornji trg"
}, {
    "name": "Gosposvetska",
    "ascii": "Gosposvetska"
}, {
    "name": "Gostilna Kovač",
    "ascii": "Gostilna Kovac"
}, {
    "name": "Gradišče 1",
    "ascii": "Gradisce 1"
}, {
    "name": "Gradišče 2",
    "ascii": "Gradisce 2"
}, {
    "name": "Gradišče 3",
    "ascii": "Gradisce 3"
}, {
    "name": "Gradišče nad Pijavo G.",
    "ascii": "Gradisce nad Pijavo G."
}, {
    "name": "Grčarevec",
    "ascii": "Grcarevec"
}, {
    "name": "Grintovec/Zagradcu",
    "ascii": "Grintovec/Zagradcu"
}, {
    "name": "Griže/Stični",
    "ascii": "Grize/Sticni"
}, {
    "name": "Grm",
    "ascii": "Grm"
}, {
    "name": "GROSUPLJE",
    "ascii": "GROSUPLJE"
}, {
    "name": "Grosuplje Motel",
    "ascii": "Grosuplje Motel"
}, {
    "name": "Grosuplje OŠ L.A.",
    "ascii": "Grosuplje OS L.A."
}, {
    "name": "Grosuplje SM",
    "ascii": "Grosuplje SM"
}, {
    "name": "Grosuplje/Brezje",
    "ascii": "Grosuplje/Brezje"
}, {
    "name": "Gumnišče 1",
    "ascii": "Gumnisce 1"
}, {
    "name": "Gumnišče center",
    "ascii": "Gumnisce center"
}, {
    "name": "Gzl",
    "ascii": "Gzl"
}, {
    "name": "Hajdrihova",
    "ascii": "Hajdrihova"
}, {
    "name": "Havptmance",
    "ascii": "Havptmance"
}, {
    "name": "Hladnikova",
    "ascii": "Hladnikova"
}, {
    "name": "Hočevje",
    "ascii": "Hocevje"
}, {
    "name": "Horjul",
    "ascii": "Horjul"
}, {
    "name": "Hranilniška",
    "ascii": "Hranilniska"
}, {
    "name": "Hrastarija",
    "ascii": "Hrastarija"
}, {
    "name": "Hrastenice",
    "ascii": "Hrastenice"
}, {
    "name": "Hrastje",
    "ascii": "Hrastje"
}, {
    "name": "Hrastje pri Golem",
    "ascii": "Hrastje pri Golem"
}, {
    "name": "Hrastov Dol",
    "ascii": "Hrastov Dol"
}, {
    "name": "Hraše center",
    "ascii": "Hrase center"
}, {
    "name": "Hraše vzhod",
    "ascii": "Hrase vzhod"
}, {
    "name": "Hraše zahod",
    "ascii": "Hrase zahod"
}, {
    "name": "Hruševo",
    "ascii": "Hrusevo"
}, {
    "name": "Hruševska",
    "ascii": "Hrusevska"
}, {
    "name": "Hrušica",
    "ascii": "Hrusica"
}, {
    "name": "Hrvatski trg",
    "ascii": "Hrvatski trg"
}, {
    "name": "I.C. Šiška",
    "ascii": "I.C. Siska"
}, {
    "name": "Ig",
    "ascii": "Ig"
}, {
    "name": "Ig AP",
    "ascii": "Ig AP"
}, {
    "name": "Ig mizarstvo",
    "ascii": "Ig mizarstvo"
}, {
    "name": "Ig Petrol",
    "ascii": "Ig Petrol"
}, {
    "name": "Ilirska",
    "ascii": "Ilirska"
}, {
    "name": "Ilovica",
    "ascii": "Ilovica"
}, {
    "name": "Imp",
    "ascii": "Imp"
}, {
    "name": "Iška",
    "ascii": "Iska"
}, {
    "name": "Iška Loka",
    "ascii": "Iska Loka"
}, {
    "name": "Iška obračališče",
    "ascii": "Iska obracalisce"
}, {
    "name": "Iška vas",
    "ascii": "Iska vas"
}, {
    "name": "Iška vas obr.",
    "ascii": "Iska vas obr."
}, {
    "name": "Iška vas šola",
    "ascii": "Iska vas sola"
}, {
    "name": "Iški most",
    "ascii": "Iski most"
}, {
    "name": "Ivančna Gorica",
    "ascii": "Ivancna Gorica"
}, {
    "name": "Ivančna Gorica ŠC",
    "ascii": "Ivancna Gorica SC"
}, {
    "name": "Ivane Kobilca",
    "ascii": "Ivane Kobilca"
}, {
    "name": "Ižanska",
    "ascii": "Izanska"
}, {
    "name": "Ižica",
    "ascii": "Izica"
}, {
    "name": "Jadranska",
    "ascii": "Jadranska"
}, {
    "name": "Jamnikarjeva",
    "ascii": "Jamnikarjeva"
}, {
    "name": "Jana Husa",
    "ascii": "Jana Husa"
}, {
    "name": "Japelj",
    "ascii": "Japelj"
}, {
    "name": "Jarše",
    "ascii": "Jarse"
}, {
    "name": "Javor pri Sostrem K.",
    "ascii": "Javor pri Sostrem K."
}, {
    "name": "Javor/K",
    "ascii": "Javor/K"
}, {
    "name": "Jerinov grič",
    "ascii": "Jerinov gric"
}, {
    "name": "JEZERO",
    "ascii": "JEZERO"
}, {
    "name": "Ježa",
    "ascii": "Jeza"
}, {
    "name": "JEŽICA",
    "ascii": "JEZICA"
}, {
    "name": "Ježica",
    "ascii": "Jezica"
}, {
    "name": "JEŽICA P+R",
    "ascii": "JEZICA P+R"
}, {
    "name": "Ježkova",
    "ascii": "Jezkova"
}, {
    "name": "Jurčkova",
    "ascii": "Jurckova"
}, {
    "name": "Kajakaška",
    "ascii": "Kajakaska"
}, {
    "name": "Kajuhova",
    "ascii": "Kajuhova"
}, {
    "name": "Kal/Ambrusu",
    "ascii": "Kal/Ambrusu"
}, {
    "name": "Kal/Ambrusu K",
    "ascii": "Kal/Ambrusu K"
}, {
    "name": "Kalce/Logatcu",
    "ascii": "Kalce/Logatcu"
}, {
    "name": "Kališnikov trg",
    "ascii": "Kalisnikov trg"
}, {
    "name": "KAMNA GORICA",
    "ascii": "KAMNA GORICA"
}, {
    "name": "Kamni Vrh",
    "ascii": "Kamni Vrh"
}, {
    "name": "Kamnik pod Krimom",
    "ascii": "Kamnik pod Krimom"
}, {
    "name": "Kamnik pod Krimom K.",
    "ascii": "Kamnik pod Krimom K."
}, {
    "name": "Kamnogoriška",
    "ascii": "Kamnogoriska"
}, {
    "name": "Kamnolom Rovte",
    "ascii": "Kamnolom Rovte"
}, {
    "name": "Kardeljeva ploščad",
    "ascii": "Kardeljeva ploscad"
}, {
    "name": "Kavčičeva",
    "ascii": "Kavciceva"
}, {
    "name": "Kino Šiška",
    "ascii": "Kino Siska"
}, {
    "name": "Kitni Vrh",
    "ascii": "Kitni Vrh"
}, {
    "name": "Klinični center",
    "ascii": "Klinicni center"
}, {
    "name": "Kneza Koclja",
    "ascii": "Kneza Koclja"
}, {
    "name": "Knjižnica Grosuplje",
    "ascii": "Knjiznica Grosuplje"
}, {
    "name": "Kobiljek/Zagradcu",
    "ascii": "Kobiljek/Zagradcu"
}, {
    "name": "KODELJEVO",
    "ascii": "KODELJEVO"
}, {
    "name": "Kodeljevo",
    "ascii": "Kodeljevo"
}, {
    "name": "Kodrova",
    "ascii": "Kodrova"
}, {
    "name": "KOLODVOR",
    "ascii": "KOLODVOR"
}, {
    "name": "Kolodvor Črnuče",
    "ascii": "Kolodvor Crnuce"
}, {
    "name": "Komanija",
    "ascii": "Komanija"
}, {
    "name": "Komenda Š",
    "ascii": "Komenda S"
}, {
    "name": "Komenda vas",
    "ascii": "Komenda vas"
}, {
    "name": "Kompas",
    "ascii": "Kompas"
}, {
    "name": "Kompolje/Dobrepolju",
    "ascii": "Kompolje/Dobrepolju"
}, {
    "name": "Kongo",
    "ascii": "Kongo"
}, {
    "name": "Kongresni trg",
    "ascii": "Kongresni trg"
}, {
    "name": "Konzorcij",
    "ascii": "Konzorcij"
}, {
    "name": "Koprska",
    "ascii": "Koprska"
}, {
    "name": "Korinj",
    "ascii": "Korinj"
}, {
    "name": "Koseze",
    "ascii": "Koseze"
}, {
    "name": "Koseze Stare",
    "ascii": "Koseze Stare"
}, {
    "name": "Kosmačeva",
    "ascii": "Kosmaceva"
}, {
    "name": "Kot",
    "ascii": "Kot"
}, {
    "name": "Kovinarska",
    "ascii": "Kovinarska"
}, {
    "name": "Kozarje",
    "ascii": "Kozarje"
}, {
    "name": "Kozarje G.D.",
    "ascii": "Kozarje G.D."
}, {
    "name": "Kozler",
    "ascii": "Kozler"
}, {
    "name": "Kožljevec",
    "ascii": "Kozljevec"
}, {
    "name": "Kpl",
    "ascii": "Kpl"
}, {
    "name": "Kranjčeva",
    "ascii": "Kranjceva"
}, {
    "name": "Krimska",
    "ascii": "Krimska"
}, {
    "name": "Kriška vas",
    "ascii": "Kriska vas"
}, {
    "name": "Krivec",
    "ascii": "Krivec"
}, {
    "name": "Križanke",
    "ascii": "Krizanke"
}, {
    "name": "Križišče Sostro",
    "ascii": "Krizisce Sostro"
}, {
    "name": "Krka",
    "ascii": "Krka"
}, {
    "name": "Krošljev grič",
    "ascii": "Krosljev gric"
}, {
    "name": "Krožna pot",
    "ascii": "Krozna pot"
}, {
    "name": "Krpan",
    "ascii": "Krpan"
}, {
    "name": "Kržičeva",
    "ascii": "Krziceva"
}, {
    "name": "KS Pirniče",
    "ascii": "KS Pirnice"
}, {
    "name": "Kulturni center Vrhnika",
    "ascii": "Kulturni center Vrhnika"
}, {
    "name": "Kurešček K",
    "ascii": "Kurescek K"
}, {
    "name": "Kurja vas/Šentjoštu",
    "ascii": "Kurja vas/Sentjostu"
}, {
    "name": "Kužljevec K",
    "ascii": "Kuzljevec K"
}, {
    "name": "Lahova pot",
    "ascii": "Lahova pot"
}, {
    "name": "Lahovče",
    "ascii": "Lahovce"
}, {
    "name": "Lanišče",
    "ascii": "Lanisce"
}, {
    "name": "Lavrica",
    "ascii": "Lavrica"
}, {
    "name": "Lavrica OŠ",
    "ascii": "Lavrica OS"
}, {
    "name": "Lavrica pri Malči",
    "ascii": "Lavrica pri Malci"
}, {
    "name": "Laze/Krko",
    "ascii": "Laze/Krko"
}, {
    "name": "Leskoškova",
    "ascii": "Leskoskova"
}, {
    "name": "Leskovec/Viš.G.",
    "ascii": "Leskovec/Vis.G."
}, {
    "name": "Lesno Brdo",
    "ascii": "Lesno Brdo"
}, {
    "name": "Letališče Brnik",
    "ascii": "Letalisce Brnik"
}, {
    "name": "LETALIŠKA",
    "ascii": "LETALISKA"
}, {
    "name": "Letališka",
    "ascii": "Letaliska"
}, {
    "name": "Ligojna K",
    "ascii": "Ligojna K"
}, {
    "name": "Lipe",
    "ascii": "Lipe"
}, {
    "name": "LITOSTROJ",
    "ascii": "LITOSTROJ"
}, {
    "name": "Litostrojska",
    "ascii": "Litostrojska"
}, {
    "name": "Livada",
    "ascii": "Livada"
}, {
    "name": "Ljubgojna",
    "ascii": "Ljubgojna"
}, {
    "name": "LJUBLJANA AP",
    "ascii": "LJUBLJANA AP"
}, {
    "name": "Ljubljana Dobrunje",
    "ascii": "Ljubljana Dobrunje"
}, {
    "name": "Ljubljana Tivolska",
    "ascii": "Ljubljana Tivolska"
}, {
    "name": "Ljubljana Zadvor",
    "ascii": "Ljubljana Zadvor"
}, {
    "name": "Ljubljanska",
    "ascii": "Ljubljanska"
}, {
    "name": "Ljubljanske brigade",
    "ascii": "Ljubljanske brigade"
}, {
    "name": "Ljubljanski grad",
    "ascii": "Ljubljanski grad"
}, {
    "name": "Lobček",
    "ascii": "Lobcek"
}, {
    "name": "Log pri Brez.Mole",
    "ascii": "Log pri Brez.Mole"
}, {
    "name": "Log pri Brez.vas",
    "ascii": "Log pri Brez.vas"
}, {
    "name": "Log pri Polh G.",
    "ascii": "Log pri Polh G."
}, {
    "name": "Log/Zavratec",
    "ascii": "Log/Zavratec"
}, {
    "name": "Logar",
    "ascii": "Logar"
}, {
    "name": "Logatec",
    "ascii": "Logatec"
}, {
    "name": "Logatec Krpan",
    "ascii": "Logatec Krpan"
}, {
    "name": "Log-Dragomer OŠ",
    "ascii": "Log-Dragomer OS"
}, {
    "name": "Loka pri Mengšu",
    "ascii": "Loka pri Mengsu"
}, {
    "name": "Lučarjev Kal",
    "ascii": "Lucarjev Kal"
}, {
    "name": "Luče Na Šoli",
    "ascii": "Luce Na Soli"
}, {
    "name": "Lukovica pri Brezovici",
    "ascii": "Lukovica pri Brezovici"
}, {
    "name": "Lutkovno gl.",
    "ascii": "Lutkovno gl."
}, {
    "name": "M. Lipljene",
    "ascii": "M. Lipljene"
}, {
    "name": "M. St. vas",
    "ascii": "M. St. vas"
}, {
    "name": "M. vas pri Grosupljem",
    "ascii": "M. vas pri Grosupljem"
}, {
    "name": "M.Hudo",
    "ascii": "M.Hudo"
}, {
    "name": "M.Ilova GORA",
    "ascii": "M.Ilova GORA"
}, {
    "name": "M.Ločnik/Turjaku",
    "ascii": "M.Locnik/Turjaku"
}, {
    "name": "M.Mlačevo",
    "ascii": "M.Mlacevo"
}, {
    "name": "M.Račna",
    "ascii": "M.Racna"
}, {
    "name": "Mačkov graben",
    "ascii": "Mackov graben"
}, {
    "name": "Mala Dobrava",
    "ascii": "Mala Dobrava"
}, {
    "name": "Mala Ligojna",
    "ascii": "Mala Ligojna"
}, {
    "name": "Male Dole K",
    "ascii": "Male Dole K"
}, {
    "name": "Male Kompolje",
    "ascii": "Male Kompolje"
}, {
    "name": "Male Vrhe",
    "ascii": "Male Vrhe"
}, {
    "name": "MALI LIPOGLAV",
    "ascii": "MALI LIPOGLAV"
}, {
    "name": "Mali Vrh",
    "ascii": "Mali Vrh"
}, {
    "name": "Marinča vas",
    "ascii": "Marinca vas"
}, {
    "name": "Marinčev grič",
    "ascii": "Marincev gric"
}, {
    "name": "Maroltova",
    "ascii": "Maroltova"
}, {
    "name": "Martinova",
    "ascii": "Martinova"
}, {
    "name": "Matena",
    "ascii": "Matena"
}, {
    "name": "Medno",
    "ascii": "Medno"
}, {
    "name": "MEDVODE",
    "ascii": "MEDVODE"
}, {
    "name": "Medvode A.P.",
    "ascii": "Medvode A.P."
}, {
    "name": "Medvode na Klancu",
    "ascii": "Medvode na Klancu"
}, {
    "name": "MEDVODE naselje",
    "ascii": "MEDVODE naselje"
}, {
    "name": "Mekinje/Stično",
    "ascii": "Mekinje/Sticno"
}, {
    "name": "Mengeš Lovec",
    "ascii": "Menges Lovec"
}, {
    "name": "Mengeš Pavovec",
    "ascii": "Menges Pavovec"
}, {
    "name": "Mengeš Petrol",
    "ascii": "Menges Petrol"
}, {
    "name": "Mercator",
    "ascii": "Mercator"
}, {
    "name": "MESTNA HIŠA",
    "ascii": "MESTNA HISA"
}, {
    "name": "MESTNI LOG",
    "ascii": "MESTNI LOG"
}, {
    "name": "Metnaj",
    "ascii": "Metnaj"
}, {
    "name": "Mevce",
    "ascii": "Mevce"
}, {
    "name": "Mihov štradon",
    "ascii": "Mihov stradon"
}, {
    "name": "Mirje",
    "ascii": "Mirje"
}, {
    "name": "Mizni Dol",
    "ascii": "Mizni Dol"
}, {
    "name": "Mizni Dol II",
    "ascii": "Mizni Dol II"
}, {
    "name": "Mlake",
    "ascii": "Mlake"
}, {
    "name": "Mleščevo",
    "ascii": "Mlescevo"
}, {
    "name": "Mlin/Rovtah",
    "ascii": "Mlin/Rovtah"
}, {
    "name": "Moste Petrol",
    "ascii": "Moste Petrol"
}, {
    "name": "Mostiček",
    "ascii": "Mosticek"
}, {
    "name": "Mostovna",
    "ascii": "Mostovna"
}, {
    "name": "Mrzle njive",
    "ascii": "Mrzle njive"
}, {
    "name": "Mrzlo polje",
    "ascii": "Mrzlo polje"
}, {
    "name": "Muljava",
    "ascii": "Muljava"
}, {
    "name": "Muljava OŠ",
    "ascii": "Muljava OS"
}, {
    "name": "Murgle",
    "ascii": "Murgle"
}, {
    "name": "Murkova",
    "ascii": "Murkova"
}, {
    "name": "Na Gmajni",
    "ascii": "Na Gmajni"
}, {
    "name": "Na Gmajnici",
    "ascii": "Na Gmajnici"
}, {
    "name": "Na jami",
    "ascii": "Na jami"
}, {
    "name": "Na klancu",
    "ascii": "Na klancu"
}, {
    "name": "Na Pili",
    "ascii": "Na Pili"
}, {
    "name": "Na Žalah",
    "ascii": "Na Zalah"
}, {
    "name": "Nadgorica",
    "ascii": "Nadgorica"
}, {
    "name": "Nasovče",
    "ascii": "Nasovce"
}, {
    "name": "Navje",
    "ascii": "Navje"
}, {
    "name": "Noč",
    "ascii": "Noc"
}, {
    "name": "NOTRANJE GORICE",
    "ascii": "NOTRANJE GORICE"
}, {
    "name": "Nova vas/Viš.G.",
    "ascii": "Nova vas/Vis.G."
}, {
    "name": "Nova vas/Žireh",
    "ascii": "Nova vas/Zireh"
}, {
    "name": "Novaki",
    "ascii": "Novaki"
}, {
    "name": "Nove Jarše",
    "ascii": "Nove Jarse"
}, {
    "name": "NOVE JARŠE",
    "ascii": "NOVE JARSE"
}, {
    "name": "NOVE STOŽICE P+R",
    "ascii": "NOVE STOZICE P+R"
}, {
    "name": "Nove Žale",
    "ascii": "Nove Zale"
}, {
    "name": "Novo naselje",
    "ascii": "Novo naselje"
}, {
    "name": "Novo Polje",
    "ascii": "Novo Polje"
}, {
    "name": "NS RUDNIK",
    "ascii": "NS RUDNIK"
}, {
    "name": "NS Rudnik",
    "ascii": "NS Rudnik"
}, {
    "name": "O.š. Brinje",
    "ascii": "O.s. Brinje"
}, {
    "name": "O.š. Jezero",
    "ascii": "O.s. Jezero"
}, {
    "name": "O.š. Maksa Pečarja",
    "ascii": "O.s. Maksa Pecarja"
}, {
    "name": "O.š. Sostro",
    "ascii": "O.s. Sostro"
}, {
    "name": "Ob daljnovodu",
    "ascii": "Ob daljnovodu"
}, {
    "name": "Ob sotočju",
    "ascii": "Ob sotocju"
}, {
    "name": "Obrtna cona",
    "ascii": "Obrtna cona"
}, {
    "name": "Omersova",
    "ascii": "Omersova"
}, {
    "name": "Opekarska",
    "ascii": "Opekarska"
}, {
    "name": "Opera",
    "ascii": "Opera"
}, {
    "name": "Orel/Žireh",
    "ascii": "Orel/Zireh"
}, {
    "name": "Orle GD",
    "ascii": "Orle GD"
}, {
    "name": "Orle K",
    "ascii": "Orle K"
}, {
    "name": "Orlova",
    "ascii": "Orlova"
}, {
    "name": "Osenjakova",
    "ascii": "Osenjakova"
}, {
    "name": "Oslica",
    "ascii": "Oslica"
}, {
    "name": "OŠ Kopanje",
    "ascii": "OS Kopanje"
}, {
    "name": "OŠ L.A. Tovarniška",
    "ascii": "OS L.A. Tovarniska"
}, {
    "name": "Oval",
    "ascii": "Oval"
}, {
    "name": "Padež/Borovnici",
    "ascii": "Padez/Borovnici"
}, {
    "name": "Pako",
    "ascii": "Pako"
}, {
    "name": "Pance K",
    "ascii": "Pance K"
}, {
    "name": "Parlament",
    "ascii": "Parlament"
}, {
    "name": "Parmova",
    "ascii": "Parmova"
}, {
    "name": "Pece K",
    "ascii": "Pece K"
}, {
    "name": "Peč pri Polici",
    "ascii": "Pec pri Polici"
}, {
    "name": "Perovo",
    "ascii": "Perovo"
}, {
    "name": "Peruzzijeva",
    "ascii": "Peruzzijeva"
}, {
    "name": "Peščenik/Viš.G.",
    "ascii": "Pescenik/Vis.G."
}, {
    "name": "Petačev graben",
    "ascii": "Petacev graben"
}, {
    "name": "Petkovec",
    "ascii": "Petkovec"
}, {
    "name": "Petkovec Zavčan",
    "ascii": "Petkovec Zavcan"
}, {
    "name": "Petrol",
    "ascii": "Petrol"
}, {
    "name": "Pijava Gorica",
    "ascii": "Pijava Gorica"
}, {
    "name": "Plečnikova cerkev",
    "ascii": "Plecnikova cerkev"
}, {
    "name": "Plečnikova hiša",
    "ascii": "Plecnikova hisa"
}, {
    "name": "Plešičeva",
    "ascii": "Plesiceva"
}, {
    "name": "Plešivica pri Žalni",
    "ascii": "Plesivica pri Zalni"
}, {
    "name": "Plešivica/Logu grad",
    "ascii": "Plesivica/Logu grad"
}, {
    "name": "Plešivica/Logu spomenik",
    "ascii": "Plesivica/Logu spomenik"
}, {
    "name": "Pod Golovcem",
    "ascii": "Pod Golovcem"
}, {
    "name": "Pod gozdom",
    "ascii": "Pod gozdom"
}, {
    "name": "Pod Hribom",
    "ascii": "Pod Hribom"
}, {
    "name": "Pod Kamno Gorico",
    "ascii": "Pod Kamno Gorico"
}, {
    "name": "Pod Rožnikom",
    "ascii": "Pod Roznikom"
}, {
    "name": "Pod Urhom",
    "ascii": "Pod Urhom"
}, {
    "name": "Podboršt/Šentvidu",
    "ascii": "Podborst/Sentvidu"
}, {
    "name": "Podgora",
    "ascii": "Podgora"
}, {
    "name": "Podgora pri Verdu",
    "ascii": "Podgora pri Verdu"
}, {
    "name": "Podgora/Dobrepolju K",
    "ascii": "Podgora/Dobrepolju K"
}, {
    "name": "Podgorica",
    "ascii": "Podgorica"
}, {
    "name": "Podgozd",
    "ascii": "Podgozd"
}, {
    "name": "Podklanec/Žireh",
    "ascii": "Podklanec/Zireh"
}, {
    "name": "Podlesec/Žireh",
    "ascii": "Podlesec/Zireh"
}, {
    "name": "Podlipa",
    "ascii": "Podlipa"
}, {
    "name": "Podlipa hrast",
    "ascii": "Podlipa hrast"
}, {
    "name": "Podlipa Kovtrov z.",
    "ascii": "Podlipa Kovtrov z."
}, {
    "name": "Podlipa Podlogar",
    "ascii": "Podlipa Podlogar"
}, {
    "name": "Podlipa pri tran.",
    "ascii": "Podlipa pri tran."
}, {
    "name": "Podlipa Stope",
    "ascii": "Podlipa Stope"
}, {
    "name": "Podlipa Vičič",
    "ascii": "Podlipa Vicic"
}, {
    "name": "Podlipa Zukancov z.",
    "ascii": "Podlipa Zukancov z."
}, {
    "name": "Podlipa Županov z.",
    "ascii": "Podlipa Zupanov z."
}, {
    "name": "Podlipoglav",
    "ascii": "Podlipoglav"
}, {
    "name": "Podmilščakova",
    "ascii": "Podmilscakova"
}, {
    "name": "Podmornica",
    "ascii": "Podmornica"
}, {
    "name": "Podolnica",
    "ascii": "Podolnica"
}, {
    "name": "Podpeč",
    "ascii": "Podpec"
}, {
    "name": "Podpeč 1",
    "ascii": "Podpec 1"
}, {
    "name": "Podpeč/Dobrepolju",
    "ascii": "Podpec/Dobrepolju"
}, {
    "name": "Podpeč/Lj.",
    "ascii": "Podpec/Lj."
}, {
    "name": "Podpeč/Lj. Jereb",
    "ascii": "Podpec/Lj. Jereb"
}, {
    "name": "Podreber/Polh. G.",
    "ascii": "Podreber/Polh. G."
}, {
    "name": "Podsmreka GD",
    "ascii": "Podsmreka GD"
}, {
    "name": "Podsmreka pri Gorjancu",
    "ascii": "Podsmreka pri Gorjancu"
}, {
    "name": "Podsmreka pri Višnji G.",
    "ascii": "Podsmreka pri Visnji G."
}, {
    "name": "Podsmreka Rotar",
    "ascii": "Podsmreka Rotar"
}, {
    "name": "Podtabor/Strugah",
    "ascii": "Podtabor/Strugah"
}, {
    "name": "PODUTIK",
    "ascii": "PODUTIK"
}, {
    "name": "Podvozna pot",
    "ascii": "Podvozna pot"
}, {
    "name": "Pohorskega bataljona",
    "ascii": "Pohorskega bataljona"
}, {
    "name": "Pokojišče/Borovnici",
    "ascii": "Pokojisce/Borovnici"
}, {
    "name": "Pokopališka",
    "ascii": "Pokopaliska"
}, {
    "name": "Polanškova",
    "ascii": "Polanskova"
}, {
    "name": "POLHOV GRADEC",
    "ascii": "POLHOV GRADEC"
}, {
    "name": "POLICA/VIŠ. G.",
    "ascii": "POLICA/VIS. G."
}, {
    "name": "Poliklinika",
    "ascii": "Poliklinika"
}, {
    "name": "Poljanska",
    "ascii": "Poljanska"
}, {
    "name": "Polje",
    "ascii": "Polje"
}, {
    "name": "Polje obračališče",
    "ascii": "Polje obracalisce"
}, {
    "name": "Polje-kolodvor",
    "ascii": "Polje-kolodvor"
}, {
    "name": "Polževo K",
    "ascii": "Polzevo K"
}, {
    "name": "Ponikve/Dobrepolju",
    "ascii": "Ponikve/Dobrepolju"
}, {
    "name": "Ponikve/Dobrepolju Inv.d.",
    "ascii": "Ponikve/Dobrepolju Inv.d."
}, {
    "name": "Ponova vas",
    "ascii": "Ponova vas"
}, {
    "name": "Pošta",
    "ascii": "Posta"
}, {
    "name": "Pošta Dobrunje",
    "ascii": "Posta Dobrunje"
}, {
    "name": "Pot na Fužine",
    "ascii": "Pot na Fuzine"
}, {
    "name": "Pot na Rakovo jelšo",
    "ascii": "Pot na Rakovo jelso"
}, {
    "name": "Pot R. križa",
    "ascii": "Pot R. kriza"
}, {
    "name": "Pot v Hrastovec",
    "ascii": "Pot v Hrastovec"
}, {
    "name": "Pot v Smrečje",
    "ascii": "Pot v Smrecje"
}, {
    "name": "Potiskavec",
    "ascii": "Potiskavec"
}, {
    "name": "Povodje",
    "ascii": "Povodje"
}, {
    "name": "Praproče",
    "ascii": "Praproce"
}, {
    "name": "Praproče pri Temenici",
    "ascii": "Praproce pri Temenici"
}, {
    "name": "Predole K",
    "ascii": "Predole K"
}, {
    "name": "Predstruge",
    "ascii": "Predstruge"
}, {
    "name": "Preglov trg",
    "ascii": "Preglov trg"
}, {
    "name": "Prekmurska",
    "ascii": "Prekmurska"
}, {
    "name": "Preserje pod Krimom",
    "ascii": "Preserje pod Krimom"
}, {
    "name": "Preska",
    "ascii": "Preska"
}, {
    "name": "Preval",
    "ascii": "Preval"
}, {
    "name": "Prevalje pod Krimom",
    "ascii": "Prevalje pod Krimom"
}, {
    "name": "Prežganje Š.",
    "ascii": "Prezganje S."
}, {
    "name": "Prežganje Š. K.",
    "ascii": "Prezganje S. K."
}, {
    "name": "Pri Dogonivar",
    "ascii": "Pri Dogonivar"
}, {
    "name": "Pri Dolencu",
    "ascii": "Pri Dolencu"
}, {
    "name": "Pri Jeršinu",
    "ascii": "Pri Jersinu"
}, {
    "name": "Pri Maranzu",
    "ascii": "Pri Maranzu"
}, {
    "name": "Pri Poku",
    "ascii": "Pri Poku"
}, {
    "name": "Prilesje/Ilovi Gori",
    "ascii": "Prilesje/Ilovi Gori"
}, {
    "name": "Primča vas",
    "ascii": "Primca vas"
}, {
    "name": "Primožev ovinek",
    "ascii": "Primozev ovinek"
}, {
    "name": "Primožičeva",
    "ascii": "Primoziceva"
}, {
    "name": "Privoz",
    "ascii": "Privoz"
}, {
    "name": "Prušnikova",
    "ascii": "Prusnikova"
}, {
    "name": "PRŽAN",
    "ascii": "PRZAN"
}, {
    "name": "Puhova",
    "ascii": "Puhova"
}, {
    "name": "Pungart/Temenici",
    "ascii": "Pungart/Temenici"
}, {
    "name": "Pusti Javor",
    "ascii": "Pusti Javor"
}, {
    "name": "Račeva",
    "ascii": "Raceva"
}, {
    "name": "Radanja vas",
    "ascii": "Radanja vas"
}, {
    "name": "Radna",
    "ascii": "Radna"
}, {
    "name": "Radohova vas/Šentvidu",
    "ascii": "Radohova vas/Sentvidu"
}, {
    "name": "Raj nad mestom",
    "ascii": "Raj nad mestom"
}, {
    "name": "RAKITNA",
    "ascii": "RAKITNA"
}, {
    "name": "Rakovnik",
    "ascii": "Rakovnik"
}, {
    "name": "Rakovnik pri Sori",
    "ascii": "Rakovnik pri Sori"
}, {
    "name": "Rapljevo/Strugah",
    "ascii": "Rapljevo/Strugah"
}, {
    "name": "Rast",
    "ascii": "Rast"
}, {
    "name": "Rašica",
    "ascii": "Rasica"
}, {
    "name": "Rašica V",
    "ascii": "Rasica V"
}, {
    "name": "Razdrto",
    "ascii": "Razdrto"
}, {
    "name": "Razori K.",
    "ascii": "Razori K."
}, {
    "name": "Razori Rogovilc",
    "ascii": "Razori Rogovilc"
}, {
    "name": "Razpotje",
    "ascii": "Razpotje"
}, {
    "name": "Razstavišče",
    "ascii": "Razstavisce"
}, {
    "name": "Rdeči Kal/Šentvidu",
    "ascii": "Rdeci Kal/Sentvidu"
}, {
    "name": "Reaktor",
    "ascii": "Reaktor"
}, {
    "name": "Reber pri Škofljici",
    "ascii": "Reber pri Skofljici"
}, {
    "name": "Remiza",
    "ascii": "Remiza"
}, {
    "name": "Repnje Benk",
    "ascii": "Repnje Benk"
}, {
    "name": "Repnje kapelica",
    "ascii": "Repnje kapelica"
}, {
    "name": "Repnje samostan",
    "ascii": "Repnje samostan"
}, {
    "name": "Rog",
    "ascii": "Rog"
}, {
    "name": "Rogovilc",
    "ascii": "Rogovilc"
}, {
    "name": "Roška",
    "ascii": "Roska"
}, {
    "name": "Rovte",
    "ascii": "Rovte"
}, {
    "name": "Rovte K",
    "ascii": "Rovte K"
}, {
    "name": "Rožičeva",
    "ascii": "Roziceva"
}, {
    "name": "Rožna dolina",
    "ascii": "Rozna dolina"
}, {
    "name": "Rudnik",
    "ascii": "Rudnik"
}, {
    "name": "RUDNIK",
    "ascii": "RUDNIK"
}, {
    "name": "Rusjanov trg",
    "ascii": "Rusjanov trg"
}, {
    "name": "Ruski car",
    "ascii": "Ruski car"
}, {
    "name": "Rutar",
    "ascii": "Rutar"
}, {
    "name": "Sad",
    "ascii": "Sad"
}, {
    "name": "Sadinja vas",
    "ascii": "Sadinja vas"
}, {
    "name": "Samova",
    "ascii": "Samova"
}, {
    "name": "Sap",
    "ascii": "Sap"
}, {
    "name": "Saturnus",
    "ascii": "Saturnus"
}, {
    "name": "Sava",
    "ascii": "Sava"
}, {
    "name": "SAVLJE",
    "ascii": "SAVLJE"
}, {
    "name": "Savske stolpnice",
    "ascii": "Savske stolpnice"
}, {
    "name": "Savsko naselje",
    "ascii": "Savsko naselje"
}, {
    "name": "Sela",
    "ascii": "Sela"
}, {
    "name": "Sela/Viš.G.",
    "ascii": "Sela/Vis.G."
}, {
    "name": "Selo mali Dunaj",
    "ascii": "Selo mali Dunaj"
}, {
    "name": "Selo pri Pancah K",
    "ascii": "Selo pri Pancah K"
}, {
    "name": "Selo pri Radohovi vasi",
    "ascii": "Selo pri Radohovi vasi"
}, {
    "name": "Selo pri Vodicah",
    "ascii": "Selo pri Vodicah"
}, {
    "name": "Selo/Dobu",
    "ascii": "Selo/Dobu"
}, {
    "name": "Senožeti/Viš.G.",
    "ascii": "Senozeti/Vis.G."
}, {
    "name": "Silos",
    "ascii": "Silos"
}, {
    "name": "Sinja Gorica",
    "ascii": "Sinja Gorica"
}, {
    "name": "Sinja Gorica avt.",
    "ascii": "Sinja Gorica avt."
}, {
    "name": "Sinja Gorica Oblak",
    "ascii": "Sinja Gorica Oblak"
}, {
    "name": "Sinja Gorica opekarna",
    "ascii": "Sinja Gorica opekarna"
}, {
    "name": "Skaručna",
    "ascii": "Skarucna"
}, {
    "name": "Skaručna Jurjevc",
    "ascii": "Skarucna Jurjevc"
}, {
    "name": "Slovenija avto",
    "ascii": "Slovenija avto"
}, {
    "name": "Slovenijales",
    "ascii": "Slovenijales"
}, {
    "name": "Smelt",
    "ascii": "Smelt"
}, {
    "name": "Smlednik šola",
    "ascii": "Smlednik sola"
}, {
    "name": "Smolnik nad hribom",
    "ascii": "Smolnik nad hribom"
}, {
    "name": "Smolnik Praprotnik",
    "ascii": "Smolnik Praprotnik"
}, {
    "name": "Smolnik Setnik",
    "ascii": "Smolnik Setnik"
}, {
    "name": "Smolnik Zalog",
    "ascii": "Smolnik Zalog"
}, {
    "name": "Smolnik žaga",
    "ascii": "Smolnik zaga"
}, {
    "name": "Smrečje/Vrh.pri Kapelici",
    "ascii": "Smrecje/Vrh.pri Kapelici"
}, {
    "name": "Smrečje/Vrhniko",
    "ascii": "Smrecje/Vrhniko"
}, {
    "name": "Smrečje/Vrhniko pri Mihu",
    "ascii": "Smrecje/Vrhniko pri Mihu"
}, {
    "name": "Smrečje/Vrhniko Samija",
    "ascii": "Smrecje/Vrhniko Samija"
}, {
    "name": "Smrečje/Vrhniko vrh",
    "ascii": "Smrecje/Vrhniko vrh"
}, {
    "name": "Smrekarjevo",
    "ascii": "Smrekarjevo"
}, {
    "name": "Smrjene",
    "ascii": "Smrjene"
}, {
    "name": "Smrjene 1",
    "ascii": "Smrjene 1"
}, {
    "name": "Smrjene 2",
    "ascii": "Smrjene 2"
}, {
    "name": "Smrjene 3",
    "ascii": "Smrjene 3"
}, {
    "name": "Smrjene 4",
    "ascii": "Smrjene 4"
}, {
    "name": "Sneberje",
    "ascii": "Sneberje"
}, {
    "name": "Snoj",
    "ascii": "Snoj"
}, {
    "name": "Sobrače",
    "ascii": "Sobrace"
}, {
    "name": "Sora",
    "ascii": "Sora"
}, {
    "name": "SORA obračališče",
    "ascii": "SORA obracalisce"
}, {
    "name": "Sora OŠ",
    "ascii": "Sora OS"
}, {
    "name": "SOSTRO",
    "ascii": "SOSTRO"
}, {
    "name": "Sp. Brnik",
    "ascii": "Sp. Brnik"
}, {
    "name": "Sp. Gameljne 1",
    "ascii": "Sp. Gameljne 1"
}, {
    "name": "Sp. Gameljne 2",
    "ascii": "Sp. Gameljne 2"
}, {
    "name": "Sp. Gameljne 3",
    "ascii": "Sp. Gameljne 3"
}, {
    "name": "Sp. Pirniče",
    "ascii": "Sp. Pirnice"
}, {
    "name": "Sp. Slivnica",
    "ascii": "Sp. Slivnica"
}, {
    "name": "Sp.Blato",
    "ascii": "Sp.Blato"
}, {
    "name": "Sp.Brezovo",
    "ascii": "Sp.Brezovo"
}, {
    "name": "Sp.Duplice",
    "ascii": "Sp.Duplice"
}, {
    "name": "Spar",
    "ascii": "Spar"
}, {
    "name": "Spodnji Rudnik",
    "ascii": "Spodnji Rudnik"
}, {
    "name": "Sr. vas pri Polh. G.",
    "ascii": "Sr. vas pri Polh. G."
}, {
    "name": "Središka",
    "ascii": "Srediska"
}, {
    "name": "St.trg/Višnji G.",
    "ascii": "St.trg/Visnji G."
}, {
    "name": "St.Vrhnika K",
    "ascii": "St.Vrhnika K"
}, {
    "name": "Stadion",
    "ascii": "Stadion"
}, {
    "name": "Staje",
    "ascii": "Staje"
}, {
    "name": "Stan in dom",
    "ascii": "Stan in dom"
}, {
    "name": "STANEŽIČE",
    "ascii": "STANEZICE"
}, {
    "name": "Stara cerkev",
    "ascii": "Stara cerkev"
}, {
    "name": "Stara pošta",
    "ascii": "Stara posta"
}, {
    "name": "Stična K",
    "ascii": "Sticna K"
}, {
    "name": "Stična KZ",
    "ascii": "Sticna KZ"
}, {
    "name": "Stožice",
    "ascii": "Stozice"
}, {
    "name": "Strahomer",
    "ascii": "Strahomer"
}, {
    "name": "Stranska vas/Viš.G.",
    "ascii": "Stranska vas/Vis.G."
}, {
    "name": "Strelišče",
    "ascii": "Strelisce"
}, {
    "name": "Streliška",
    "ascii": "Streliska"
}, {
    "name": "Strmca 2",
    "ascii": "Strmca 2"
}, {
    "name": "Strmica/Vrhniki",
    "ascii": "Strmica/Vrhniki"
}, {
    "name": "Struge OŠ",
    "ascii": "Struge OS"
}, {
    "name": "Studenec",
    "ascii": "Studenec"
}, {
    "name": "Studenec/Iv.Gorici",
    "ascii": "Studenec/Iv.Gorici"
}, {
    "name": "SUHI DOL/Planino",
    "ascii": "SUHI DOL/Planino"
}, {
    "name": "Sušica/Muljavi",
    "ascii": "Susica/Muljavi"
}, {
    "name": "Svetčeva",
    "ascii": "Svetceva"
}, {
    "name": "Svetje",
    "ascii": "Svetje"
}, {
    "name": "Šemnikar",
    "ascii": "Semnikar"
}, {
    "name": "Šentjakob",
    "ascii": "Sentjakob"
}, {
    "name": "ŠENTJOŠT",
    "ascii": "SENTJOST"
}, {
    "name": "Šentvid",
    "ascii": "Sentvid"
}, {
    "name": "Šentvid pri Stični",
    "ascii": "Sentvid pri Sticni"
}, {
    "name": "Šentvid pri Stični K",
    "ascii": "Sentvid pri Sticni K"
}, {
    "name": "Šentvid pri Stični OŠ F.V",
    "ascii": "Sentvid pri Sticni OS F.V"
}, {
    "name": "Šentvid pri Stični ŽP",
    "ascii": "Sentvid pri Sticni ZP"
}, {
    "name": "Šinkov Turn",
    "ascii": "Sinkov Turn"
}, {
    "name": "Šinkov Turn hrib",
    "ascii": "Sinkov Turn hrib"
}, {
    "name": "Šišenska",
    "ascii": "Sisenska"
}, {
    "name": "Škocjan",
    "ascii": "Skocjan"
}, {
    "name": "Škoflje/Šentvidu",
    "ascii": "Skoflje/Sentvidu"
}, {
    "name": "ŠKOFLJICA",
    "ascii": "SKOFLJICA"
}, {
    "name": "Škofljica Javornik",
    "ascii": "Skofljica Javornik"
}, {
    "name": "Škofljica Petkovšek",
    "ascii": "Skofljica Petkovsek"
}, {
    "name": "Škofljica Špica",
    "ascii": "Skofljica Spica"
}, {
    "name": "Škofljica žaga",
    "ascii": "Skofljica zaga"
}, {
    "name": "Škrilje",
    "ascii": "Skrilje"
}, {
    "name": "Škrilje 2",
    "ascii": "Skrilje 2"
}, {
    "name": "Škrjanče/Iv.G.",
    "ascii": "Skrjance/Iv.G."
}, {
    "name": "Škrjanka",
    "ascii": "Skrjanka"
}, {
    "name": "Šlandrova",
    "ascii": "Slandrova"
}, {
    "name": "Šmarje",
    "ascii": "Smarje"
}, {
    "name": "Šmartno",
    "ascii": "Smartno"
}, {
    "name": "Šmartno pod Šmarno g.",
    "ascii": "Smartno pod Smarno g."
}, {
    "name": "Šola Jarše",
    "ascii": "Sola Jarse"
}, {
    "name": "Šolska",
    "ascii": "Solska"
}, {
    "name": "Špica",
    "ascii": "Spica"
}, {
    "name": "Špilar",
    "ascii": "Spilar"
}, {
    "name": "Št. Jurij",
    "ascii": "St. Jurij"
}, {
    "name": "Štajerska",
    "ascii": "Stajerska"
}, {
    "name": "Štepanja vas",
    "ascii": "Stepanja vas"
}, {
    "name": "ŠTEPANJSKO NASELJE",
    "ascii": "STEPANJSKO NASELJE"
}, {
    "name": "Študentsko naselje",
    "ascii": "Studentsko naselje"
}, {
    "name": "Šujica",
    "ascii": "Sujica"
}, {
    "name": "Tabor",
    "ascii": "Tabor"
}, {
    "name": "Tacen",
    "ascii": "Tacen"
}, {
    "name": "Tacenski most",
    "ascii": "Tacenski most"
}, {
    "name": "Tavčarjeva",
    "ascii": "Tavcarjeva"
}, {
    "name": "Tbilisijska",
    "ascii": "Tbilisijska"
}, {
    "name": "Tehnounion",
    "ascii": "Tehnounion"
}, {
    "name": "Temenica",
    "ascii": "Temenica"
}, {
    "name": "Temenica OŠ",
    "ascii": "Temenica OS"
}, {
    "name": "Tiki",
    "ascii": "Tiki"
}, {
    "name": "Tisovec/Strugah",
    "ascii": "Tisovec/Strugah"
}, {
    "name": "Tivoli",
    "ascii": "Tivoli"
}, {
    "name": "Tivolska",
    "ascii": "Tivolska"
}, {
    "name": "Tobačna",
    "ascii": "Tobacna"
}, {
    "name": "TOMAČEVO",
    "ascii": "TOMACEVO"
}, {
    "name": "Tomačevska",
    "ascii": "Tomacevska"
}, {
    "name": "Tomišelj",
    "ascii": "Tomiselj"
}, {
    "name": "Tomišelj A",
    "ascii": "Tomiselj A"
}, {
    "name": "Tomišelj K",
    "ascii": "Tomiselj K"
}, {
    "name": "Toplarna",
    "ascii": "Toplarna"
}, {
    "name": "Topniška",
    "ascii": "Topniska"
}, {
    "name": "Topole/Mengšu",
    "ascii": "Topole/Mengsu"
}, {
    "name": "TOVARNA LEK",
    "ascii": "TOVARNA LEK"
}, {
    "name": "Tovorni kolodvor",
    "ascii": "Tovorni kolodvor"
}, {
    "name": "Trata",
    "ascii": "Trata"
}, {
    "name": "Trbeže",
    "ascii": "Trbeze"
}, {
    "name": "Trebeljevo",
    "ascii": "Trebeljevo"
}, {
    "name": "TRNOVO",
    "ascii": "TRNOVO"
}, {
    "name": "Trnovski pristan",
    "ascii": "Trnovski pristan"
}, {
    "name": "Troščine",
    "ascii": "Troscine"
}, {
    "name": "Trzin",
    "ascii": "Trzin"
}, {
    "name": "Trzin ind. cona",
    "ascii": "Trzin ind. cona"
}, {
    "name": "Trzin Mlake",
    "ascii": "Trzin Mlake"
}, {
    "name": "Trzin vas",
    "ascii": "Trzin vas"
}, {
    "name": "Tržič/Strugah",
    "ascii": "Trzic/Strugah"
}, {
    "name": "Tržnica Koseze",
    "ascii": "Trznica Koseze"
}, {
    "name": "Tržnica Moste",
    "ascii": "Trznica Moste"
}, {
    "name": "TUJI GRM",
    "ascii": "TUJI GRM"
}, {
    "name": "Turist",
    "ascii": "Turist"
}, {
    "name": "Turjak",
    "ascii": "Turjak"
}, {
    "name": "Turnovšče",
    "ascii": "Turnovsce"
}, {
    "name": "Ulovka",
    "ascii": "Ulovka"
}, {
    "name": "Utik",
    "ascii": "Utik"
}, {
    "name": "V. Lese",
    "ascii": "V. Lese"
}, {
    "name": "V. Lipljene",
    "ascii": "V. Lipljene"
}, {
    "name": "V. Loka pri Viš. G.",
    "ascii": "V. Loka pri Vis. G."
}, {
    "name": "V. Mlačevo",
    "ascii": "V. Mlacevo"
}, {
    "name": "V. Mlačevo ŽP",
    "ascii": "V. Mlacevo ZP"
}, {
    "name": "V. St. vas",
    "ascii": "V. St. vas"
}, {
    "name": "V.Ligojna",
    "ascii": "V.Ligojna"
}, {
    "name": "V.Račna",
    "ascii": "V.Racna"
}, {
    "name": "Valburga",
    "ascii": "Valburga"
}, {
    "name": "Valična vas",
    "ascii": "Valicna vas"
}, {
    "name": "Vaše",
    "ascii": "Vase"
}, {
    "name": "Vaše Helios",
    "ascii": "Vase Helios"
}, {
    "name": "Večna pot",
    "ascii": "Vecna pot"
}, {
    "name": "Velika Dobrava",
    "ascii": "Velika Dobrava"
}, {
    "name": "Velika Ilova gora",
    "ascii": "Velika Ilova gora"
}, {
    "name": "Velike Kompolje",
    "ascii": "Velike Kompolje"
}, {
    "name": "Velike Pece",
    "ascii": "Velike Pece"
}, {
    "name": "Veliki Lipoglav",
    "ascii": "Veliki Lipoglav"
}, {
    "name": "Veliki štradon",
    "ascii": "Veliki stradon"
}, {
    "name": "Veliko Črnelo",
    "ascii": "Veliko Crnelo"
}, {
    "name": "Verd K",
    "ascii": "Verd K"
}, {
    "name": "Verje",
    "ascii": "Verje"
}, {
    "name": "Vesca/Vodicah",
    "ascii": "Vesca/Vodicah"
}, {
    "name": "VEVČE",
    "ascii": "VEVCE"
}, {
    "name": "Vevče papirnica",
    "ascii": "Vevce papirnica"
}, {
    "name": "Viadukt",
    "ascii": "Viadukt"
}, {
    "name": "Vič",
    "ascii": "Vic"
}, {
    "name": "Videm/Dobrepolju",
    "ascii": "Videm/Dobrepolju"
}, {
    "name": "Vikrče",
    "ascii": "Vikrce"
}, {
    "name": "Vir pri Stični",
    "ascii": "Vir pri Sticni"
}, {
    "name": "Visoko/Kureščkom",
    "ascii": "Visoko/Kuresckom"
}, {
    "name": "Viško polje",
    "ascii": "Visko polje"
}, {
    "name": "Višnja Gora",
    "ascii": "Visnja Gora"
}, {
    "name": "Višnja Gora OŠ",
    "ascii": "Visnja Gora OS"
}, {
    "name": "Višnje/Ambrusu",
    "ascii": "Visnje/Ambrusu"
}, {
    "name": "VIŽMARJE",
    "ascii": "VIZMARJE"
}, {
    "name": "VODICE",
    "ascii": "VODICE"
}, {
    "name": "Vodice K.",
    "ascii": "Vodice K."
}, {
    "name": "Vodice O.Š.",
    "ascii": "Vodice O.S."
}, {
    "name": "Vodičar",
    "ascii": "Vodicar"
}, {
    "name": "Vodovodna",
    "ascii": "Vodovodna"
}, {
    "name": "Vojsko/Vodicah",
    "ascii": "Vojsko/Vodicah"
}, {
    "name": "Volavlje",
    "ascii": "Volavlje"
}, {
    "name": "Vopovlje",
    "ascii": "Vopovlje"
}, {
    "name": "Vrbljene",
    "ascii": "Vrbljene"
}, {
    "name": "Vrh nad Viš.G.",
    "ascii": "Vrh nad Vis.G."
}, {
    "name": "Vrh nad Želimljami",
    "ascii": "Vrh nad Zelimljami"
}, {
    "name": "Vrh nad Želimljami 1",
    "ascii": "Vrh nad Zelimljami 1"
}, {
    "name": "Vrh nad Želimljami 2",
    "ascii": "Vrh nad Zelimljami 2"
}, {
    "name": "Vrh nad Želimljami GD",
    "ascii": "Vrh nad Zelimljami GD"
}, {
    "name": "Vrhnika",
    "ascii": "Vrhnika"
}, {
    "name": "Vrhnika hrib",
    "ascii": "Vrhnika hrib"
}, {
    "name": "Vrhnika Jazon",
    "ascii": "Vrhnika Jazon"
}, {
    "name": "Vrhnika OŠ AM Slomška",
    "ascii": "Vrhnika OS AM Slomska"
}, {
    "name": "Vrhnika OŠ Ivan Cankar Lošca",
    "ascii": "Vrhnika OS Ivan Cankar Losca"
}, {
    "name": "Vrhnika OŠ Ivana Cankarja",
    "ascii": "Vrhnika OS Ivana Cankarja"
}, {
    "name": "Vrhnika samop.",
    "ascii": "Vrhnika samop."
}, {
    "name": "Vrhnika Voljčeva",
    "ascii": "Vrhnika Voljceva"
}, {
    "name": "Vrhnika vrt.",
    "ascii": "Vrhnika vrt."
}, {
    "name": "Vrhovci",
    "ascii": "Vrhovci"
}, {
    "name": "Vrzdenec",
    "ascii": "Vrzdenec"
}, {
    "name": "Yulon",
    "ascii": "Yulon"
}, {
    "name": "Zaboršt/Šentvidu",
    "ascii": "Zaborst/Sentvidu"
}, {
    "name": "Zadobrova",
    "ascii": "Zadobrova"
}, {
    "name": "ZADOBROVA",
    "ascii": "ZADOBROVA"
}, {
    "name": "Zadvor",
    "ascii": "Zadvor"
}, {
    "name": "Zadvor 2",
    "ascii": "Zadvor 2"
}, {
    "name": "Zadvor pri Grosupljem",
    "ascii": "Zadvor pri Grosupljem"
}, {
    "name": "Zagorica/Dobrepolju",
    "ascii": "Zagorica/Dobrepolju"
}, {
    "name": "Zagradec OŠ",
    "ascii": "Zagradec OS"
}, {
    "name": "Zagradec pri Grosupljem",
    "ascii": "Zagradec pri Grosupljem"
}, {
    "name": "Zagradec/Krki",
    "ascii": "Zagradec/Krki"
}, {
    "name": "Zaklanec/Horjulu",
    "ascii": "Zaklanec/Horjulu"
}, {
    "name": "ZALOG",
    "ascii": "ZALOG"
}, {
    "name": "Zaloška",
    "ascii": "Zaloska"
}, {
    "name": "Zaplana",
    "ascii": "Zaplana"
}, {
    "name": "Zaplana cerkev",
    "ascii": "Zaplana cerkev"
}, {
    "name": "Zaplana Jamnik",
    "ascii": "Zaplana Jamnik"
}, {
    "name": "Zaplana Jazbar",
    "ascii": "Zaplana Jazbar"
}, {
    "name": "Zaplana Jerinov grič",
    "ascii": "Zaplana Jerinov gric"
}, {
    "name": "Zaplana ovinek",
    "ascii": "Zaplana ovinek"
}, {
    "name": "Zaplana žaga",
    "ascii": "Zaplana zaga"
}, {
    "name": "Zapoge",
    "ascii": "Zapoge"
}, {
    "name": "Zapoge obračališče",
    "ascii": "Zapoge obracalisce"
}, {
    "name": "ZAPOTOK",
    "ascii": "ZAPOTOK"
}, {
    "name": "Zapotok/Kureščkom K",
    "ascii": "Zapotok/Kuresckom K"
}, {
    "name": "Zapuže",
    "ascii": "Zapuze"
}, {
    "name": "Zasavska",
    "ascii": "Zasavska"
}, {
    "name": "Zavrh",
    "ascii": "Zavrh"
}, {
    "name": "Zavrh/Borovnici",
    "ascii": "Zavrh/Borovnici"
}, {
    "name": "Zbilje Jezero",
    "ascii": "Zbilje Jezero"
}, {
    "name": "Zbilje K",
    "ascii": "Zbilje K"
}, {
    "name": "Zbiljski Gaj",
    "ascii": "Zbiljski Gaj"
}, {
    "name": "ZD Polje",
    "ascii": "ZD Polje"
}, {
    "name": "Zdenska vas",
    "ascii": "Zdenska vas"
}, {
    "name": "Zdravstveni dom Gros.",
    "ascii": "Zdravstveni dom Gros."
}, {
    "name": "ZELENA JAMA",
    "ascii": "ZELENA JAMA"
}, {
    "name": "Zeleni gaj",
    "ascii": "Zeleni gaj"
}, {
    "name": "Zg. Besnica",
    "ascii": "Zg. Besnica"
}, {
    "name": "Zg. Gameljne",
    "ascii": "Zg. Gameljne"
}, {
    "name": "Zg. Pirniče",
    "ascii": "Zg. Pirnice"
}, {
    "name": "Zgornja Draga",
    "ascii": "Zgornja Draga"
}, {
    "name": "Zgornja Šiška",
    "ascii": "Zgornja Siska"
}, {
    "name": "Zgornji log",
    "ascii": "Zgornji log"
}, {
    "name": "Zidarjevec",
    "ascii": "Zidarjevec"
}, {
    "name": "Zidarjevec-2",
    "ascii": "Zidarjevec-2"
}, {
    "name": "Ziherlova",
    "ascii": "Ziherlova"
}, {
    "name": "Zmajski most",
    "ascii": "Zmajski most"
}, {
    "name": "Znojilje K",
    "ascii": "Znojilje K"
}, {
    "name": "Žabja vas",
    "ascii": "Zabja vas"
}, {
    "name": "Žale",
    "ascii": "Zale"
}, {
    "name": "Žalna",
    "ascii": "Zalna"
}, {
    "name": "Žalna ŽP",
    "ascii": "Zalna ZP"
}, {
    "name": "Žažar",
    "ascii": "Zazar"
}, {
    "name": "ŽELEZNA",
    "ascii": "ZELEZNA"
}, {
    "name": "ŽELIMLJE",
    "ascii": "ZELIMLJE"
}, {
    "name": "Želimlje Nučič",
    "ascii": "Zelimlje Nucic"
}, {
    "name": "Želimlje Rogovilec",
    "ascii": "Zelimlje Rogovilec"
}, {
    "name": "Žibrše K",
    "ascii": "Zibrse K"
}, {
    "name": "Žiri",
    "ascii": "Ziri"
}, {
    "name": "Žirovnik",
    "ascii": "Zirovnik"
}, {
    "name": "Žito",
    "ascii": "Zito"
}, {
    "name": "Živalski vrt",
    "ascii": "Zivalski vrt"
}];
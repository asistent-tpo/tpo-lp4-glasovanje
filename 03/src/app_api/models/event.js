module.exports = (sequelize, type) => {
    return sequelize.define('event', {
        id : {
            type : type.INTEGER,
            autoIncrement : true,
            primaryKey  : true
        },
        date : {
            type : type.DATE,
            allowNull : false
        },
        name : {
            type : type.STRING,
            allowNull : false
        },
        organizer : {
            type : type.STRING,
            allowNull : false
        },
        description : {
            type : type.TEXT
        }
    });
};
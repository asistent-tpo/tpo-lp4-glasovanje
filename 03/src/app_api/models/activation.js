module.exports = (sequelize, type) => {
    return sequelize.define('activation', {
        hash : {
            type : type.STRING,
            primaryKey  : true
        },
        userId : {
            type : type.INTEGER,
            references : {
                model : sequelize.models.user,
                key : 'id',
                deferrable: type.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        valid : {
            type : type.DATE,
            allowNull : false
        }
    });
};
module.exports = [
    {
       "lat":"46.0449933",
       "lon":"14.4430546",
       "name":"714032 ~ Razori Rogovilc"
    },
    {
       "lat":"46.0901894",
       "lon":"14.6580404",
       "name":"Kleče pri Dolu"
    },
    {
       "lat":"46.0883546",
       "lon":"14.7126594",
       "name":"Senožeti/Litiji"
    },
    {
       "lat":"46.0951436",
       "lon":"14.6768933",
       "name":"Dolsko"
    },
    {
       "lat":"46.0897772",
       "lon":"14.6407361",
       "name":"Dol pri Ljubljani"
    },
    {
       "lat":"46.0899009",
       "lon":"14.6416350",
       "name":"Dol pri Ljubljani"
    },
    {
       "lat":"46.0902079",
       "lon":"14.6564839",
       "name":"Kleče pri Dolu"
    },
    {
       "lat":"46.0884514",
       "lon":"14.7122464",
       "name":"Senožeti/Litiji"
    },
    {
       "lat":"46.0561703",
       "lon":"14.5021015",
       "name":"700012 ~ Gosposvetska"
    },
    {
       "lat":"46.0551294",
       "lon":"14.5047956",
       "name":"600012 ~ Bavarski dvor (Kozolec)"
    },
    {
       "lat":"46.0567076",
       "lon":"14.5057098",
       "name":"600011 ~ Bavarski dvor (Korabar)"
    },
    {
       "lat":"46.0549015",
       "lon":"14.5037303",
       "name":"700011 ~ Gosposvetska"
    },
    {
       "lat":"46.0602469",
       "lon":"14.4977445",
       "name":"801012 ~ Tivoli / Ljubljana Tivoli"
    },
    {
       "lat":"46.0744429",
       "lon":"14.4853142",
       "name":"803014 ~ Remiza / Ljubljana Šiška"
    },
    {
       "lat":"46.0604639",
       "lon":"14.4971719",
       "name":"801011 ~ Tivoli / Ljubljana Tivoli"
    },
    {
       "lat":"46.0648457",
       "lon":"14.4942799",
       "name":"802012 ~ Stara cerkev"
    },
    {
       "lat":"46.0737464",
       "lon":"14.4851603",
       "name":"803016 ~ Šišenska"
    },
    {
       "lat":"46.0738782",
       "lon":"14.4865248",
       "name":"803012 ~ Slovenija avto"
    },
    {
       "lat":"46.0697188",
       "lon":"14.4894080",
       "name":"802021 ~ Kino Šiška / Ljubljana Šiška"
    },
    {
       "lat":"46.0638902",
       "lon":"14.4943765",
       "name":"802011 ~ Stara cerkev"
    },
    {
       "lat":"46.0691856",
       "lon":"14.4903472",
       "name":"802022 ~ Kino Šiška"
    },
    {
       "lat":"46.0730619",
       "lon":"14.4862890",
       "name":"803011 ~ Slovenija avto"
    },
    {
       "lat":"46.0773277",
       "lon":"14.4823933",
       "name":"803022 ~ Avtomontaža"
    },
    {
       "lat":"46.0768571",
       "lon":"14.4823421",
       "name":"803021 ~ Avtomontaža"
    },
    {
       "lat":"46.0936921",
       "lon":"14.4687332",
       "name":"804032 ~ Podgora"
    },
    {
       "lat":"46.0836691",
       "lon":"14.4779762",
       "name":"803042 ~ Dravlje"
    },
    {
       "lat":"46.0839595",
       "lon":"14.4773110",
       "name":"803041 ~ Dravlje"
    },
    {
       "lat":"46.1002140",
       "lon":"14.4591229",
       "name":"804051 ~ Vižmarje"
    },
    {
       "lat":"46.1010093",
       "lon":"14.4610282",
       "name":"804082 ~ Kosmačeva"
    },
    {
       "lat":"46.0802406",
       "lon":"14.4803908",
       "name":"803032 ~ Kompas"
    },
    {
       "lat":"46.1006938",
       "lon":"14.4604837",
       "name":"804081 ~ Kosmačeva"
    },
    {
       "lat":"46.0898231",
       "lon":"14.4717328",
       "name":"804021 ~ Prušnikova"
    },
    {
       "lat":"46.0925917",
       "lon":"14.4692925",
       "name":"804031 ~ Podgora"
    },
    {
       "lat":"46.0953138",
       "lon":"14.4666112",
       "name":"804041 ~ Šentvid / Ljubljana Šentvid"
    },
    {
       "lat":"46.0968680",
       "lon":"14.4649634",
       "name":"804042 ~ Šentvid / Ljubljana Šentvid"
    },
    {
       "lat":"46.0869114",
       "lon":"14.4747583",
       "name":"804012 ~ Trata / Ljubljana Trata"
    },
    {
       "lat":"46.0872160",
       "lon":"14.4740493",
       "name":"804011 ~ Trata / Ljubljana Trata"
    },
    {
       "lat":"46.0909413",
       "lon":"14.4711761",
       "name":"804022 ~ Prušnikova"
    },
    {
       "lat":"46.0799869",
       "lon":"14.4801182",
       "name":"803031 ~ Kompas"
    },
    {
       "lat":"46.1125182",
       "lon":"14.4618736",
       "name":"804112 ~ Ob daljnovodu"
    },
    {
       "lat":"46.1131410",
       "lon":"14.4616143",
       "name":"804111 ~ Ob daljnovodu"
    },
    {
       "lat":"46.1155407",
       "lon":"14.4627193",
       "name":"804121 ~ Brod"
    },
    {
       "lat":"46.1154012",
       "lon":"14.4639227",
       "name":"804122 ~ Brod"
    },
    {
       "lat":"46.1177589",
       "lon":"14.4615759",
       "name":"804141 ~ Tacenski most / Ljubljana Tacen"
    },
    {
       "lat":"46.1042913",
       "lon":"14.4628028",
       "name":"804091 ~ Na klancu"
    },
    {
       "lat":"46.1038094",
       "lon":"14.4627329",
       "name":"804092 ~ Na klancu"
    },
    {
       "lat":"46.1091814",
       "lon":"14.4618939",
       "name":"804102 ~ Tabor"
    },
    {
       "lat":"46.1182202",
       "lon":"14.4619397",
       "name":"804142 ~ Tacenski most / Ljubljana Tacen"
    },
    {
       "lat":"46.1181162",
       "lon":"14.4612787",
       "name":"804144 ~ Tacenski most"
    },
    {
       "lat":"46.1085761",
       "lon":"14.4616759",
       "name":"804101 ~ Tabor"
    },
    {
       "lat":"46.1092418",
       "lon":"14.4627861",
       "name":"804104 ~ Tabor"
    },
    {
       "lat":"46.1135270",
       "lon":"14.4643815",
       "name":"804131 ~ Martinova"
    },
    {
       "lat":"46.1232713",
       "lon":"14.4768148",
       "name":"104192 ~ Šmartno"
    },
    {
       "lat":"46.1202603",
       "lon":"14.4666103",
       "name":"804151 ~ Tacen"
    },
    {
       "lat":"46.1201631",
       "lon":"14.4669199",
       "name":"804152 ~ Tacen"
    },
    {
       "lat":"46.1238912",
       "lon":"14.4787781",
       "name":"104191 ~ Šmartno"
    },
    {
       "lat":"46.1264511",
       "lon":"14.4879259",
       "name":"104201 ~ Zg. Gameljne"
    },
    {
       "lat":"46.1271072",
       "lon":"14.4928102",
       "name":"104211 ~ Rašica"
    },
    {
       "lat":"46.1270338",
       "lon":"14.4936401",
       "name":"104212 ~ Rašica"
    },
    {
       "lat":"46.1268624",
       "lon":"14.4972056",
       "name":"104221 ~ Gameljne"
    },
    {
       "lat":"46.1259017",
       "lon":"14.4870367",
       "name":"104202 ~ Zg. Gameljne"
    },
    {
       "lat":"46.0485577",
       "lon":"14.5016570",
       "name":"602022 ~ Drama"
    },
    {
       "lat":"46.0534515",
       "lon":"14.5039799",
       "name":"600022 ~ Ajdovščina"
    },
    {
       "lat":"46.0520580",
       "lon":"14.5034369",
       "name":"601011 ~ Pošta"
    },
    {
       "lat":"46.0512159",
       "lon":"14.5029642",
       "name":"601012 ~ Konzorcij"
    },
    {
       "lat":"46.0496353",
       "lon":"14.5025352",
       "name":"602021 ~ Drama"
    },
    {
       "lat":"46.0458162",
       "lon":"14.4900791",
       "name":"602032 ~ Hajdrihova"
    },
    {
       "lat":"46.0466683",
       "lon":"14.4993345",
       "name":"602052 ~ Aškerčeva / Ljubljana Aškerčeva"
    },
    {
       "lat":"46.0470577",
       "lon":"14.4942347",
       "name":"602041 ~ Tobačna"
    },
    {
       "lat":"46.0469814",
       "lon":"14.4934650",
       "name":"602042 ~ Tobačna / 602042 ~ Ljubljana Tobačna"
    },
    {
       "lat":"46.0456811",
       "lon":"14.4903855",
       "name":"602031 ~ Hajdrihova / 602031 ~ Ljubljana Langusova"
    },
    {
       "lat":"46.0466446",
       "lon":"14.4986593",
       "name":"602051 ~ Aškerčeva / Ljubljana Aškerčeva"
    },
    {
       "lat":"46.0366179",
       "lon":"14.4893015",
       "name":"603031 ~ Krimska"
    },
    {
       "lat":"46.0408673",
       "lon":"14.4898941",
       "name":"603021 ~ Gerbičeva"
    },
    {
       "lat":"46.0366251",
       "lon":"14.4889478",
       "name":"603032 ~ Krimska"
    },
    {
       "lat":"46.0406740",
       "lon":"14.4897531",
       "name":"603022 ~ Gerbičeva"
    },
    {
       "lat":"46.0420853",
       "lon":"14.4893510",
       "name":"603012 ~ Jadranska"
    },
    {
       "lat":"46.0435741",
       "lon":"14.4885696",
       "name":"603011 ~ Jadranska"
    },
    {
       "lat":"46.0300263",
       "lon":"14.4725097",
       "name":"604031 ~ Mestni log"
    },
    {
       "lat":"46.0353564",
       "lon":"14.4843099",
       "name":"603041 ~ Koprska"
    },
    {
       "lat":"46.0353069",
       "lon":"14.4833987",
       "name":"603044 ~ Koprska"
    },
    {
       "lat":"46.0654813",
       "lon":"14.5293381",
       "name":"203011 ~ Pokopališka"
    },
    {
       "lat":"46.0662899",
       "lon":"14.5344075",
       "name":"203044 ~ Šola Jarše"
    },
    {
       "lat":"46.0684176",
       "lon":"14.5367522",
       "name":"203091 ~ Jarše"
    },
    {
       "lat":"46.0687287",
       "lon":"14.5385354",
       "name":"203092 ~ Jarše"
    },
    {
       "lat":"46.0691948",
       "lon":"14.5415462",
       "name":"203102 ~ Kodrova / Ljubljana Nove Jarše"
    },
    {
       "lat":"46.0698870",
       "lon":"14.5465874",
       "name":"203152 ~ Žito"
    },
    {
       "lat":"46.0699038",
       "lon":"14.5450478",
       "name":"203151 ~ Žito"
    },
    {
       "lat":"46.0691483",
       "lon":"14.5398094",
       "name":"203101 ~ Kodrova / Ljubljana Nove Jarše"
    },
    {
       "lat":"46.0696474",
       "lon":"14.5518673",
       "name":"203171 ~ Nove Jarše"
    },
    {
       "lat":"46.0654423",
       "lon":"14.5292161",
       "name":"203012 ~ Pokopališka"
    },
    {
       "lat":"46.0666826",
       "lon":"14.5329994",
       "name":"203041 ~ Šola Jarše"
    },
    {
       "lat":"46.0672351",
       "lon":"14.5346888",
       "name":"203042 ~ Šola Jarše"
    },
    {
       "lat":"46.0636026",
       "lon":"14.5292442",
       "name":"202051 ~ Zelena jama"
    },
    {
       "lat":"46.0645647",
       "lon":"14.5290921",
       "name":"202082 ~ Flajšmanova"
    },
    {
       "lat":"46.0640306",
       "lon":"14.5363092",
       "name":"203052 ~ Kavčičeva"
    },
    {
       "lat":"46.0616270",
       "lon":"14.5389458",
       "name":"203061 ~ Tovorni kolodvor"
    },
    {
       "lat":"46.0640567",
       "lon":"14.5358934",
       "name":"203051 ~ Kavčičeva"
    },
    {
       "lat":"46.0634630",
       "lon":"14.5324307",
       "name":"203022 ~ Rožičeva"
    },
    {
       "lat":"46.0634221",
       "lon":"14.5318943",
       "name":"203021 ~ Rožičeva"
    },
    {
       "lat":"46.0555449",
       "lon":"14.5339115",
       "name":"303012 ~ Tržnica Moste"
    },
    {
       "lat":"46.0540210",
       "lon":"14.5399660",
       "name":"303032 ~ Ob sotočju"
    },
    {
       "lat":"46.0564573",
       "lon":"14.5407614",
       "name":"303022 ~ Kajuhova"
    },
    {
       "lat":"46.0560927",
       "lon":"14.5398134",
       "name":"303021 ~ Zaloška"
    },
    {
       "lat":"46.0558779",
       "lon":"14.5424410",
       "name":"303024 ~ Zaloška"
    },
    {
       "lat":"46.0557523",
       "lon":"14.5344137",
       "name":"303011 ~ Tržnica Moste"
    },
    {
       "lat":"46.0542124",
       "lon":"14.5281562",
       "name":"402041 ~ Bolnica"
    },
    {
       "lat":"46.0524511",
       "lon":"14.5206865",
       "name":"402031 ~ Klinični center"
    },
    {
       "lat":"46.0544176",
       "lon":"14.5293393",
       "name":"402042 ~ Bolnica"
    },
    {
       "lat":"46.0523106",
       "lon":"14.5208163",
       "name":"402032 ~ Klinični center"
    },
    {
       "lat":"46.0496972",
       "lon":"14.5169904",
       "name":"502013 ~ Ambrožev trg"
    },
    {
       "lat":"46.0503135",
       "lon":"14.5105092",
       "name":"501032 ~ Lutkovno gledališče"
    },
    {
       "lat":"46.0525873",
       "lon":"14.5167052",
       "name":"402011 ~ Hrvatski trg"
    },
    {
       "lat":"46.0521902",
       "lon":"14.5179209",
       "name":"402012 ~ Hrvatski trg"
    },
    {
       "lat":"46.0503972",
       "lon":"14.5144273",
       "name":"502011 ~ Poljanska"
    },
    {
       "lat":"46.0513076",
       "lon":"14.5107002",
       "name":"501021 ~ Zmajski most / Ljubljana Zmajski most"
    },
    {
       "lat":"46.0535574",
       "lon":"14.5190902",
       "name":"402022 ~ Poliklinika"
    },
    {
       "lat":"46.0522450",
       "lon":"14.5102791",
       "name":"501022 ~ Zmajski most / Ljubljana Zmajski most"
    },
    {
       "lat":"46.0505034",
       "lon":"14.5106627",
       "name":"501031 ~ Lutkovno gledališče"
    },
    {
       "lat":"46.0497822",
       "lon":"14.5165773",
       "name":"502014 ~ Ambrožev trg"
    },
    {
       "lat":"46.0537564",
       "lon":"14.5191648",
       "name":"402021 ~ Poliklinika"
    },
    {
       "lat":"46.0500865",
       "lon":"14.5155777",
       "name":"502012 ~ Poljanska"
    },
    {
       "lat":"46.0457833",
       "lon":"14.5064122",
       "name":"602102 ~ Gornji trg / Ljubljana G.trg"
    },
    {
       "lat":"46.0459375",
       "lon":"14.5060831",
       "name":"602101 ~ Gornji trg / Ljubljana G.trg"
    },
    {
       "lat":"46.0462237",
       "lon":"14.5018931",
       "name":"602061 ~ Križanke"
    },
    {
       "lat":"46.0460543",
       "lon":"14.5020004",
       "name":"602062 ~ Križanke"
    },
    {
       "lat":"46.0528429",
       "lon":"14.4977377",
       "name":"701012 ~ Bleiweisova / 701012 ~ Ljubljana Tivolska"
    },
    {
       "lat":"46.0507981",
       "lon":"14.4983115",
       "name":"602011 ~ Cankarjev dom"
    },
    {
       "lat":"46.0529430",
       "lon":"14.4981233",
       "name":"701011 ~ Bleiweisova / 701011 ~ Ljubljana Tivolska"
    },
    {
       "lat":"46.0546339",
       "lon":"14.5063913",
       "name":"500011 ~ Tavčarjeva"
    },
    {
       "lat":"46.0575627",
       "lon":"14.5110734",
       "name":"300012 ~ Kolodvor"
    },
    {
       "lat":"46.0538944",
       "lon":"14.5064983",
       "name":"600032 ~ Dalmatinova"
    },
    {
       "lat":"46.0576668",
       "lon":"14.5093933",
       "name":"300011 ~ Kolodvor"
    },
    {
       "lat":"46.0530121",
       "lon":"14.5134755",
       "name":"501011 ~ Ilirska"
    },
    {
       "lat":"46.0574526",
       "lon":"14.5184237",
       "name":"301011 ~ Friškovec"
    },
    {
       "lat":"46.0575786",
       "lon":"14.5166620",
       "name":"301012 ~ Friškovec"
    },
    {
       "lat":"46.0529302",
       "lon":"14.5136525",
       "name":"501012 ~ Ilirska"
    },
    {
       "lat":"46.0626930",
       "lon":"14.5246625",
       "name":"202042 ~ Savsko naselje"
    },
    {
       "lat":"46.0622783",
       "lon":"14.5242320",
       "name":"202041 ~ Savsko naselje"
    },
    {
       "lat":"46.0595775",
       "lon":"14.5208802",
       "name":"202012 ~ Viadukt / Ljubljana Viadukt"
    },
    {
       "lat":"46.0594243",
       "lon":"14.5206535",
       "name":"202011 ~ Viadukt / Ljubljana Viadukt"
    },
    {
       "lat":"46.0629889",
       "lon":"14.5259714",
       "name":"202044 ~ Središka / Ljubljana Sav.nas."
    },
    {
       "lat":"46.0626286",
       "lon":"14.5250474",
       "name":"202043 ~ Središka / Ljubljana Sav.nas."
    },
    {
       "lat":"46.0669067",
       "lon":"14.5275601",
       "name":"202062 ~ Žale"
    },
    {
       "lat":"46.0670067",
       "lon":"14.5274627",
       "name":"202061 ~ Žale"
    },
    {
       "lat":"46.0768878",
       "lon":"14.4889822",
       "name":"803052 ~ Litostrojska"
    },
    {
       "lat":"46.0768355",
       "lon":"14.4886382",
       "name":"803051 ~ Litostrojska"
    },
    {
       "lat":"46.0808896",
       "lon":"14.4919959",
       "name":"803212 ~ Kovinarska"
    },
    {
       "lat":"46.0825103",
       "lon":"14.4971876",
       "name":"103201 ~ Litostroj"
    },
    {
       "lat":"46.0804299",
       "lon":"14.4914137",
       "name":"803211 ~ Kovinarska"
    },
    {
       "lat":"46.0633499",
       "lon":"14.5104818",
       "name":"101032 ~ Bežigrad"
    },
    {
       "lat":"46.0637532",
       "lon":"14.5118625",
       "name":"101031 ~ Bežigrad"
    },
    {
       "lat":"46.0612142",
       "lon":"14.5133770",
       "name":"201011 ~ Železna"
    },
    {
       "lat":"46.0597055",
       "lon":"14.5090920",
       "name":"100012 ~ Navje"
    },
    {
       "lat":"46.0611348",
       "lon":"14.5076542",
       "name":"100022 ~ Razstavišče"
    },
    {
       "lat":"46.0607068",
       "lon":"14.5071768",
       "name":"100021 ~ Razstavišče"
    },
    {
       "lat":"46.0228842",
       "lon":"14.5404969",
       "name":"504022 ~ Spodnji Rudnik"
    },
    {
       "lat":"46.0281566",
       "lon":"14.5357586",
       "name":"504011 ~ Gornji Rudnik"
    },
    {
       "lat":"46.0176206",
       "lon":"14.5433478",
       "name":"504032 ~ Rudnik I / Ljubljana Rudnik"
    },
    {
       "lat":"46.0280356",
       "lon":"14.5355830",
       "name":"504012 ~ Gornji Rudnik"
    },
    {
       "lat":"46.0185142",
       "lon":"14.5432618",
       "name":"504033 ~ Rudnik I / Ljubljana Rudnik"
    },
    {
       "lat":"46.0179541",
       "lon":"14.5439167",
       "name":"504031 ~ Rudnik"
    },
    {
       "lat":"46.0237898",
       "lon":"14.5400763",
       "name":"504021 ~ Spodnji Rudnik"
    },
    {
       "lat":"46.0300039",
       "lon":"14.5326165",
       "name":"503041 ~ Peruzzijeva"
    },
    {
       "lat":"46.0303740",
       "lon":"14.5316881",
       "name":"503042 ~ Peruzzijeva"
    },
    {
       "lat":"46.0440544",
       "lon":"14.5109153",
       "name":"602112 ~ Privoz"
    },
    {
       "lat":"46.0320853",
       "lon":"14.5291479",
       "name":"503032 ~ Akademija"
    },
    {
       "lat":"46.0322767",
       "lon":"14.5291452",
       "name":"503031 ~ Akademija"
    },
    {
       "lat":"46.0383386",
       "lon":"14.5180843",
       "name":"503012 ~ Strelišče / Ljubljana str."
    },
    {
       "lat":"46.0385279",
       "lon":"14.5166811",
       "name":"503014 ~ Orlova"
    },
    {
       "lat":"46.0355301",
       "lon":"14.5240089",
       "name":"503022 ~ Rakovnik / Ljubljana Rakovnik"
    },
    {
       "lat":"46.0394315",
       "lon":"14.5169631",
       "name":"503011 ~ Strelišče / Ljubljana str."
    },
    {
       "lat":"46.0440657",
       "lon":"14.5113066",
       "name":"602111 ~ Privoz"
    },
    {
       "lat":"46.0363303",
       "lon":"14.5227636",
       "name":"503021 ~ Rakovnik / Ljubljana Rakovnik"
    },
    {
       "lat":"46.0449853",
       "lon":"14.5183740",
       "name":"502032 ~ Streliška"
    },
    {
       "lat":"46.0484777",
       "lon":"14.5195407",
       "name":"502021 ~ Roška"
    },
    {
       "lat":"46.0445981",
       "lon":"14.5168627",
       "name":"502031 ~ Streliška"
    },
    {
       "lat":"46.0487608",
       "lon":"14.5202995",
       "name":"502022 ~ Roška"
    },
    {
       "lat":"46.0504019",
       "lon":"14.5264730",
       "name":"402051 ~ Glonarjeva"
    },
    {
       "lat":"46.0515435",
       "lon":"14.5391092",
       "name":"403022 ~ Kodeljevo"
    },
    {
       "lat":"46.0489689",
       "lon":"14.5226873",
       "name":"402061 ~ Gornje Poljane"
    },
    {
       "lat":"46.0515343",
       "lon":"14.5394263",
       "name":"403023 ~ Kodeljevo"
    },
    {
       "lat":"46.0479057",
       "lon":"14.5388881",
       "name":"403042 ~ Štepanja vas"
    },
    {
       "lat":"46.0506104",
       "lon":"14.5268458",
       "name":"402052 ~ Glonarjeva"
    },
    {
       "lat":"46.0534604",
       "lon":"14.5481176",
       "name":"303043 ~ Štepanjsko naselje"
    },
    {
       "lat":"46.0479966",
       "lon":"14.5375070",
       "name":"403041 ~ Štepanja vas"
    },
    {
       "lat":"46.0487745",
       "lon":"14.5244137",
       "name":"402062 ~ Gornje Poljane"
    },
    {
       "lat":"46.0475825",
       "lon":"14.5450914",
       "name":"403052 ~ Emona"
    },
    {
       "lat":"46.0478010",
       "lon":"14.5446998",
       "name":"403051 ~ Emona"
    },
    {
       "lat":"46.0521381",
       "lon":"14.5390543",
       "name":"403021 ~ Kodeljevo"
    },
    {
       "lat":"46.0537807",
       "lon":"14.5478629",
       "name":"303041 ~ Štepanjsko naselje"
    },
    {
       "lat":"46.0531996",
       "lon":"14.5330668",
       "name":"403012 ~ Jana Husa"
    },
    {
       "lat":"46.0531959",
       "lon":"14.5327602",
       "name":"403011 ~ Jana Husa"
    },
    {
       "lat":"46.0484112",
       "lon":"14.5297989",
       "name":"403032 ~ Pod Golovcem"
    },
    {
       "lat":"46.0485455",
       "lon":"14.5299376",
       "name":"403031 ~ Pod Golovcem"
    },
    {
       "lat":"46.0484034",
       "lon":"14.5385456",
       "name":"403043 ~ Štepanja vas"
    },
    {
       "lat":"46.0711781",
       "lon":"14.4782932",
       "name":"803102 ~ Zg. Šiška"
    },
    {
       "lat":"46.0746842",
       "lon":"14.4632084",
       "name":"803162 ~ Bratov Babnik"
    },
    {
       "lat":"46.0746023",
       "lon":"14.4627041",
       "name":"803161 ~ Bratov Babnik"
    },
    {
       "lat":"46.0746086",
       "lon":"14.4438370",
       "name":"804221 ~ Podutik"
    },
    {
       "lat":"46.0742707",
       "lon":"14.4485163",
       "name":"804212 ~ Murkova"
    },
    {
       "lat":"46.0736929",
       "lon":"14.4561337",
       "name":"804191 ~ Pod Kamno Gorico"
    },
    {
       "lat":"46.0721689",
       "lon":"14.4673198",
       "name":"803141 ~ Koseze"
    },
    {
       "lat":"46.0698220",
       "lon":"14.4729389",
       "name":"803131 ~ Bratov Učakar"
    },
    {
       "lat":"46.0698378",
       "lon":"14.4732035",
       "name":"803132 ~ Bratov Učakar"
    },
    {
       "lat":"46.0714909",
       "lon":"14.4806955",
       "name":"803082 ~ Kneza Koclja"
    },
    {
       "lat":"46.0712970",
       "lon":"14.4806429",
       "name":"803081 ~ Kneza Koclja"
    },
    {
       "lat":"46.0736520",
       "lon":"14.4499434",
       "name":"804211 ~ Murkova"
    },
    {
       "lat":"46.0739899",
       "lon":"14.4613202",
       "name":"803152 ~ Draveljska gmajna"
    },
    {
       "lat":"46.0738154",
       "lon":"14.4612330",
       "name":"803151 ~ Draveljska gmajna"
    },
    {
       "lat":"46.0741160",
       "lon":"14.4539661",
       "name":"804201 ~ Omersova"
    },
    {
       "lat":"46.0722323",
       "lon":"14.4675707",
       "name":"803142 ~ Koseze"
    },
    {
       "lat":"46.0741611",
       "lon":"14.4530206",
       "name":"804202 ~ Omersova"
    },
    {
       "lat":"46.0700709",
       "lon":"14.4803139",
       "name":"803101 ~ Zg. Šiška"
    },
    {
       "lat":"46.0795547",
       "lon":"14.4652912",
       "name":"803181 ~ Kamna Gorica"
    },
    {
       "lat":"46.0770358",
       "lon":"14.4675840",
       "name":"803172 ~ Spar"
    },
    {
       "lat":"46.0769482",
       "lon":"14.4672301",
       "name":"803171 ~ Spar"
    },
    {
       "lat":"46.0676704",
       "lon":"14.4857293",
       "name":"802042 ~ Bolnica P. Držaja"
    },
    {
       "lat":"46.0666525",
       "lon":"14.4898859",
       "name":"802031 ~ Na jami"
    },
    {
       "lat":"46.0666264",
       "lon":"14.4895971",
       "name":"802032 ~ Na jami"
    },
    {
       "lat":"46.0671753",
       "lon":"14.4863114",
       "name":"802041 ~ Bolnica P. Držaja"
    },
    {
       "lat":"46.0670581",
       "lon":"14.4959941",
       "name":"802051 ~ Drenikova"
    },
    {
       "lat":"46.0669186",
       "lon":"14.4958270",
       "name":"802052 ~ Drenikova"
    },
    {
       "lat":"46.0746309",
       "lon":"14.5318896",
       "name":"203071 ~ Nove Žale"
    },
    {
       "lat":"46.0697107",
       "lon":"14.5271939",
       "name":"203031 ~ Tomačevska"
    },
    {
       "lat":"46.0667447",
       "lon":"14.5047792",
       "name":"102012 ~ Samova"
    },
    {
       "lat":"46.0694505",
       "lon":"14.5275445",
       "name":"203032 ~ Tomačevska"
    },
    {
       "lat":"46.0670973",
       "lon":"14.5146519",
       "name":"102022 ~ Topniška"
    },
    {
       "lat":"46.0694888",
       "lon":"14.5258472",
       "name":"203211 ~ Kranjčeva"
    },
    {
       "lat":"46.0670927",
       "lon":"14.5053117",
       "name":"102011 ~ Samova"
    },
    {
       "lat":"46.0645787",
       "lon":"14.5159744",
       "name":"202021 ~ Prekmurska"
    },
    {
       "lat":"46.0658997",
       "lon":"14.5230411",
       "name":"202032 ~ Savske stolpnice"
    },
    {
       "lat":"46.0647189",
       "lon":"14.5171640",
       "name":"202022 ~ Prekmurska"
    },
    {
       "lat":"46.0794733",
       "lon":"14.5365214",
       "name":"203081 ~ Tomačevo"
    },
    {
       "lat":"46.0657145",
       "lon":"14.5216326",
       "name":"202031 ~ Savske stolpnice"
    },
    {
       "lat":"46.0671764",
       "lon":"14.5148737",
       "name":"102021 ~ Topniška"
    },
    {
       "lat":"46.0745751",
       "lon":"14.5319983",
       "name":"203072 ~ Nove Žale"
    },
    {
       "lat":"46.0653373",
       "lon":"14.5029149",
       "name":"101061 ~ Bratov Židan"
    },
    {
       "lat":"46.0638591",
       "lon":"14.5074866",
       "name":"101012 ~ Hranilniška"
    },
    {
       "lat":"46.0650144",
       "lon":"14.5090558",
       "name":"101052 ~ Astra"
    },
    {
       "lat":"46.0637060",
       "lon":"14.5044049",
       "name":"101021 ~ Parmova"
    },
    {
       "lat":"46.0650098",
       "lon":"14.5137433",
       "name":"101034 ~ Kržičeva"
    },
    {
       "lat":"46.0638065",
       "lon":"14.5073204",
       "name":"101011 ~ Hranilniška"
    },
    {
       "lat":"46.0654311",
       "lon":"14.5030718",
       "name":"101062 ~ Bratov Židan"
    },
    {
       "lat":"46.0649212",
       "lon":"14.5088065",
       "name":"101051 ~ Astra"
    },
    {
       "lat":"46.0638326",
       "lon":"14.5045739",
       "name":"101022 ~ Parmova"
    },
    {
       "lat":"46.1015276",
       "lon":"14.5262018",
       "name":"104101 ~ Kolodvor Črnuče"
    },
    {
       "lat":"46.1054940",
       "lon":"14.5345606",
       "name":"104151 ~ OŠ Maksa Pečarja"
    },
    {
       "lat":"46.0859527",
       "lon":"14.5138408",
       "name":"103101 ~ Stožice"
    },
    {
       "lat":"46.1011789",
       "lon":"14.5249351",
       "name":"104102 ~ Kolodvor Črnuče"
    },
    {
       "lat":"46.1042688",
       "lon":"14.5364255",
       "name":"104161 ~ Črnuče"
    },
    {
       "lat":"46.0776324",
       "lon":"14.5120883",
       "name":"103031 ~ AMZS / Ljubljana AMZS"
    },
    {
       "lat":"46.1005988",
       "lon":"14.5213310",
       "name":"104054 ~ Sava"
    },
    {
       "lat":"46.0902129",
       "lon":"14.5138111",
       "name":"103131 ~ Ruski car / Ljubljana Ruski car"
    },
    {
       "lat":"46.1008867",
       "lon":"14.5217457",
       "name":"104051 ~ Sava"
    },
    {
       "lat":"46.0826150",
       "lon":"14.5131958",
       "name":"103071 ~ Smelt"
    },
    {
       "lat":"46.0694268",
       "lon":"14.5104208",
       "name":"102042 ~ Stadion / Ljubljana stad."
    },
    {
       "lat":"46.1026431",
       "lon":"14.5313902",
       "name":"104112 ~ Rogovilc / Ljubljana Črnuče"
    },
    {
       "lat":"46.0820387",
       "lon":"14.5132935",
       "name":"103072 ~ Smelt"
    },
    {
       "lat":"46.0692893",
       "lon":"14.5101872",
       "name":"102041 ~ Stadion / Ljubljana stad."
    },
    {
       "lat":"46.1029268",
       "lon":"14.5304083",
       "name":"104113 ~ Primožičeva"
    },
    {
       "lat":"46.0974535",
       "lon":"14.5170128",
       "name":"104031 ~ Ježica"
    },
    {
       "lat":"46.0745526",
       "lon":"14.5115781",
       "name":"102082 ~ Mercator"
    },
    {
       "lat":"46.0973697",
       "lon":"14.5168044",
       "name":"104033 ~ Ježica"
    },
    {
       "lat":"46.0907691",
       "lon":"14.5139426",
       "name":"103132 ~ Ruski car / Ljubljana Ruski car"
    },
    {
       "lat":"46.1008053",
       "lon":"14.5217946",
       "name":"104052 ~ Sava"
    },
    {
       "lat":"46.0773529",
       "lon":"14.5122359",
       "name":"103032 ~ AMZS / Ljubljana AMZS"
    },
    {
       "lat":"46.0741525",
       "lon":"14.5112489",
       "name":"102081 ~ Mercator"
    },
    {
       "lat":"46.1050551",
       "lon":"14.5345521",
       "name":"104152 ~ OŠ Maksa Pečarja"
    },
    {
       "lat":"46.0978769",
       "lon":"14.5170506",
       "name":"104032 ~ Ježica"
    },
    {
       "lat":"46.1022965",
       "lon":"14.5297112",
       "name":"104111 ~ Rogovilc / Ljubljana Črnuče"
    },
    {
       "lat":"46.0859967",
       "lon":"14.5140697",
       "name":"103102 ~ Stožice"
    },
    {
       "lat":"46.1083384",
       "lon":"14.5322080",
       "name":"104141 ~ Pot v Hrastovec"
    },
    {
       "lat":"46.1092452",
       "lon":"14.5284590",
       "name":"104132 ~ Pot v Smrečje"
    },
    {
       "lat":"46.1075406",
       "lon":"14.5328195",
       "name":"104142 ~ Pot v Hrastovec"
    },
    {
       "lat":"46.1092533",
       "lon":"14.5276831",
       "name":"104131 ~ Pot v Smrečje"
    },
    {
       "lat":"46.1060009",
       "lon":"14.5257600",
       "name":"104121 ~ Polanškova"
    },
    {
       "lat":"46.1063998",
       "lon":"14.5256477",
       "name":"104122 ~ Polanškova"
    },
    {
       "lat":"46.0434936",
       "lon":"14.4819792",
       "name":"703082 ~ Glince"
    },
    {
       "lat":"46.0444240",
       "lon":"14.4860383",
       "name":"703071 ~ Stan in dom"
    },
    {
       "lat":"46.0367302",
       "lon":"14.4651607",
       "name":"703112 ~ Dolgi most / 703112 ~ Ljubljana Dol.most Gruda"
    },
    {
       "lat":"46.0401525",
       "lon":"14.4731002",
       "name":"703102 ~ Bonifacija"
    },
    {
       "lat":"46.0364066",
       "lon":"14.4636554",
       "name":"704012 ~ Dolgi most obračališče izstop"
    },
    {
       "lat":"46.0447423",
       "lon":"14.4864018",
       "name":"703072 ~ Stan in dom / 703072 ~ Ljubljana Ilirija"
    },
    {
       "lat":"46.0421388",
       "lon":"14.4778122",
       "name":"703091 ~ Vič / 703091 ~ Ljubljana Vič"
    },
    {
       "lat":"46.0372646",
       "lon":"14.4672060",
       "name":"703111 ~ Dolgi most / 703111 ~ Ljubljana Dol.most Gruda"
    },
    {
       "lat":"46.0397383",
       "lon":"14.4726262",
       "name":"703101 ~ Bonifacija"
    },
    {
       "lat":"46.0435624",
       "lon":"14.4826552",
       "name":"703081 ~ Glince"
    },
    {
       "lat":"46.0423057",
       "lon":"14.4778578",
       "name":"703092 ~ Vič / 703092 ~ Ljubljana Vič"
    },
    {
       "lat":"46.0368132",
       "lon":"14.4632541",
       "name":"704011 ~ Dolgi most obračališče"
    },
    {
       "lat":"46.0792873",
       "lon":"14.4713024",
       "name":"803112 ~ Čebelarska"
    },
    {
       "lat":"46.0744306",
       "lon":"14.4758331",
       "name":"803102 ~ Tržnica Koseze"
    },
    {
       "lat":"46.0824653",
       "lon":"14.4690440",
       "name":"803122 ~ Plešičeva"
    },
    {
       "lat":"46.0859324",
       "lon":"14.4648550",
       "name":"804171 ~ Andreja Bitenca"
    },
    {
       "lat":"46.0740466",
       "lon":"14.4758482",
       "name":"803101 ~ Tržnica Koseze"
    },
    {
       "lat":"46.0863351",
       "lon":"14.4649675",
       "name":"804172 ~ Andreja Bitenca"
    },
    {
       "lat":"46.0883395",
       "lon":"14.4589057",
       "name":"804181 ~ Pržan"
    },
    {
       "lat":"46.0816733",
       "lon":"14.4693587",
       "name":"803121 ~ Plešičeva"
    },
    {
       "lat":"46.0790579",
       "lon":"14.4712341",
       "name":"803111 ~ Čebelarska"
    },
    {
       "lat":"46.0887269",
       "lon":"14.4776030",
       "name":"804161 ~ IMP"
    },
    {
       "lat":"46.0893098",
       "lon":"14.4772641",
       "name":"804162 ~ IMP"
    },
    {
       "lat":"46.0798767",
       "lon":"14.4868541",
       "name":"803061 ~ Ljubljanske brigade"
    },
    {
       "lat":"46.0867425",
       "lon":"14.4798902",
       "name":"803072 ~ Tehnounion"
    },
    {
       "lat":"46.0859263",
       "lon":"14.4806182",
       "name":"803071 ~ Tehnounion"
    },
    {
       "lat":"46.1178364",
       "lon":"14.4564969",
       "name":"815111 ~ Kajakaška"
    },
    {
       "lat":"46.1222919",
       "lon":"14.4461422",
       "name":"815011 ~ Vikrče"
    },
    {
       "lat":"46.1222006",
       "lon":"14.4464612",
       "name":"815012 ~ Vikrče"
    },
    {
       "lat":"46.1179126",
       "lon":"14.4561428",
       "name":"815112 ~ Kajakaška"
    },
    {
       "lat":"46.0710863",
       "lon":"14.5800570",
       "name":"204081 ~ Zadobrova"
    },
    {
       "lat":"46.0750394",
       "lon":"14.5639730",
       "name":"204061 ~ Hrastje / Ljubljana Hrastje"
    },
    {
       "lat":"46.0573952",
       "lon":"14.5835675",
       "name":"304081 Polje - kolodvor"
    },
    {
       "lat":"46.0539645",
       "lon":"14.5828550",
       "name":"304071 ~ Polje"
    },
    {
       "lat":"46.0724559",
       "lon":"14.5803981",
       "name":"204083 ~ Zadobrova"
    },
    {
       "lat":"46.0712529",
       "lon":"14.5798733",
       "name":"204082 ~ Zadobrova"
    },
    {
       "lat":"46.0537096",
       "lon":"14.5836404",
       "name":"304072 ~ Polje"
    },
    {
       "lat":"46.0539759",
       "lon":"14.5891304",
       "name":"304091 ~ Cesta na Vevče"
    },
    {
       "lat":"46.0632502",
       "lon":"14.5833763",
       "name":"204102 ~ Novo Polje"
    },
    {
       "lat":"46.0569674",
       "lon":"14.5836952",
       "name":"304082 ~ Polje - kolodvor"
    },
    {
       "lat":"46.0778919",
       "lon":"14.5692561",
       "name":"204071 ~ Sneberje / Ljubljana Sneberje"
    },
    {
       "lat":"46.0636379",
       "lon":"14.5831453",
       "name":"204101 ~ Novo Polje"
    },
    {
       "lat":"46.0538458",
       "lon":"14.5892961",
       "name":"304092 ~ Cesta na Vevče"
    },
    {
       "lat":"46.0747640",
       "lon":"14.5637665",
       "name":"204062 ~ Hrastje / Ljubljana Hrastje"
    },
    {
       "lat":"46.0550519",
       "lon":"14.5836661",
       "name":"304074 ~ Polje"
    },
    {
       "lat":"46.0496127",
       "lon":"14.5931189",
       "name":"304101 ~ Vevče"
    },
    {
       "lat":"46.0779362",
       "lon":"14.5694884",
       "name":"204072 ~ Sneberje / Ljubljana Sneberje"
    },
    {
       "lat":"46.0608274",
       "lon":"14.6137423",
       "name":"204181 ~ Center Zalog"
    },
    {
       "lat":"46.0664135",
       "lon":"14.6128368",
       "name":"204191 ~ Zeleni gaj"
    },
    {
       "lat":"46.0577900",
       "lon":"14.6065892",
       "name":"304132 ~ Silos"
    },
    {
       "lat":"46.0568302",
       "lon":"14.6015145",
       "name":"304111 ~ Petrol"
    },
    {
       "lat":"46.0543049",
       "lon":"14.5767817",
       "name":"304062 ~ Studenec"
    },
    {
       "lat":"46.0672987",
       "lon":"14.6088703",
       "name":"204201 ~ Saturnus"
    },
    {
       "lat":"46.0610265",
       "lon":"14.6137048",
       "name":"204182 ~ Center Zalog"
    },
    {
       "lat":"46.0665237",
       "lon":"14.6128982",
       "name":"204192 ~ Zeleni gaj"
    },
    {
       "lat":"46.0567703",
       "lon":"14.6017101",
       "name":"304112 ~ Petrol"
    },
    {
       "lat":"46.0691186",
       "lon":"14.6047058",
       "name":"204211 ~ Zalog"
    },
    {
       "lat":"46.0578047",
       "lon":"14.6062441",
       "name":"304131 ~ Silos"
    },
    {
       "lat":"46.0545155",
       "lon":"14.5761435",
       "name":"304061 ~ Studenec"
    },
    {
       "lat":"46.0527698",
       "lon":"14.4716572",
       "name":"703122 ~ Živalski vrt - ZOO"
    },
    {
       "lat":"46.0753456",
       "lon":"14.5050086",
       "name":"102072 ~ Vodovodna"
    },
    {
       "lat":"46.0762814",
       "lon":"14.5002906",
       "name":"103012 ~ Toplarna"
    },
    {
       "lat":"46.0747278",
       "lon":"14.5045445",
       "name":"102071 ~ Vodovodna"
    },
    {
       "lat":"46.0757679",
       "lon":"14.5000250",
       "name":"103011 ~ Toplarna"
    },
    {
       "lat":"46.0727886",
       "lon":"14.4960177",
       "name":"802062 ~ Tiki"
    },
    {
       "lat":"46.0742673",
       "lon":"14.4996761",
       "name":"102062 ~ Mostovna"
    },
    {
       "lat":"46.0702525",
       "lon":"14.5037239",
       "name":"102052 ~ Podmilščakova"
    },
    {
       "lat":"46.0697960",
       "lon":"14.5034937",
       "name":"102051 ~ Podmilščakova"
    },
    {
       "lat":"46.0735866",
       "lon":"14.4993567",
       "name":"102061 ~ Mostovna"
    },
    {
       "lat":"46.0729265",
       "lon":"14.4950660",
       "name":"802061 ~ Tiki"
    },
    {
       "lat":"46.0794531",
       "lon":"14.5010523",
       "name":"103051 ~ Tovarna Lek"
    },
    {
       "lat":"46.0898616",
       "lon":"14.5088348",
       "name":"103112 ~ 7. septembra"
    },
    {
       "lat":"46.0901936",
       "lon":"14.5123417",
       "name":"103122 ~ Čerinova"
    },
    {
       "lat":"46.0822929",
       "lon":"14.5073447",
       "name":"103061 ~ Pohorskega bataljona"
    },
    {
       "lat":"46.0778953",
       "lon":"14.5062528",
       "name":"103022 ~ Brinje"
    },
    {
       "lat":"46.0851680",
       "lon":"14.5075885",
       "name":"103091 ~ Gorjančeva"
    },
    {
       "lat":"46.0819989",
       "lon":"14.5074092",
       "name":"103062 ~ Pohorskega bataljona"
    },
    {
       "lat":"46.0767905",
       "lon":"14.5055194",
       "name":"103021 ~ Brinje"
    },
    {
       "lat":"46.0934696",
       "lon":"14.5113100",
       "name":"104022 ~ Kališnikov trg"
    },
    {
       "lat":"46.0936899",
       "lon":"14.5106883",
       "name":"104021 ~ Kališnikov trg"
    },
    {
       "lat":"46.0952333",
       "lon":"14.5034061",
       "name":"104011 ~ Savlje"
    },
    {
       "lat":"46.0902997",
       "lon":"14.5122263",
       "name":"103121 ~ Čerinova"
    },
    {
       "lat":"46.0861030",
       "lon":"14.5077926",
       "name":"103092 ~ Gorjančeva"
    },
    {
       "lat":"46.0899704",
       "lon":"14.5086296",
       "name":"103111 ~ 7. septembra"
    },
    {
       "lat":"46.0718838",
       "lon":"14.5191519",
       "name":"102091 ~ Kardeljeva ploščad"
    },
    {
       "lat":"46.0683915",
       "lon":"14.5164951",
       "name":"102031 ~ Gasilska brigada"
    },
    {
       "lat":"46.0725332",
       "lon":"14.5193070",
       "name":"102092 ~ Kardeljeva ploščad"
    },
    {
       "lat":"46.0818828",
       "lon":"14.5198086",
       "name":"103081 ~ Nove Stožice P+R"
    },
    {
       "lat":"46.0798908",
       "lon":"14.5197635",
       "name":"103161 ~ Maroltova"
    },
    {
       "lat":"46.0765077",
       "lon":"14.5194947",
       "name":"103041 ~ Puhova"
    },
    {
       "lat":"46.0770445",
       "lon":"14.5197355",
       "name":"103042 ~ Puhova"
    },
    {
       "lat":"46.0788149",
       "lon":"14.5198264",
       "name":"103162 ~ Maroltova"
    },
    {
       "lat":"46.0763296",
       "lon":"14.5251544",
       "name":"103182 ~ Štajerska"
    },
    {
       "lat":"46.0756726",
       "lon":"14.5204191",
       "name":"103192 ~ Božičeva"
    },
    {
       "lat":"46.0757500",
       "lon":"14.5204324",
       "name":"103191 ~ Božičeva"
    },
    {
       "lat":"46.0560694",
       "lon":"14.5569267",
       "name":"303062 ~ Archinetova"
    },
    {
       "lat":"46.0545560",
       "lon":"14.5583922",
       "name":"304012 ~ Preglov trg"
    },
    {
       "lat":"46.0547105",
       "lon":"14.5591478",
       "name":"304011 ~ Preglov trg"
    },
    {
       "lat":"46.0542567",
       "lon":"14.5531929",
       "name":"303072 ~ Brodarjev trg"
    },
    {
       "lat":"46.0562445",
       "lon":"14.5552963",
       "name":"303061 ~ Archinetova"
    },
    {
       "lat":"46.0547138",
       "lon":"14.5640470",
       "name":"304031 ~ Rusjanov trg"
    },
    {
       "lat":"46.0562331",
       "lon":"14.5497073",
       "name":"303051 ~ Pot na Fužine"
    },
    {
       "lat":"46.0521257",
       "lon":"14.5659668",
       "name":"304051 ~ Fužine P+R"
    },
    {
       "lat":"46.0548926",
       "lon":"14.5673985",
       "name":"304042 ~ Chengdujska"
    },
    {
       "lat":"46.0545530",
       "lon":"14.5628067",
       "name":"304032 ~ Rusjanov trg"
    },
    {
       "lat":"46.0560789",
       "lon":"14.5607195",
       "name":"304021 ~ Osenjakova"
    },
    {
       "lat":"46.0556764",
       "lon":"14.5622260",
       "name":"304022 ~ Osenjakova"
    },
    {
       "lat":"46.0560420",
       "lon":"14.5512456",
       "name":"303052 ~ Pot na Fužine"
    },
    {
       "lat":"46.0551266",
       "lon":"14.5659277",
       "name":"304041 ~ Chengdujska"
    },
    {
       "lat":"46.0544035",
       "lon":"14.5533085",
       "name":"303071 ~ Brodarjev trg"
    },
    {
       "lat":"46.1162210",
       "lon":"14.4422649",
       "name":"804062 ~ Stanežiče"
    },
    {
       "lat":"46.1042649",
       "lon":"14.4497605",
       "name":"804341 ~ Oval"
    },
    {
       "lat":"46.1105210",
       "lon":"14.4458737",
       "name":"804061 ~ Stanežiče"
    },
    {
       "lat":"46.1088235",
       "lon":"14.4365285",
       "name":"804361 ~ Stanežiče"
    },
    {
       "lat":"46.1042486",
       "lon":"14.4496100",
       "name":"804342 ~ Oval"
    },
    {
       "lat":"46.1060339",
       "lon":"14.4420543",
       "name":"804352 ~ Dvor"
    },
    {
       "lat":"46.1056303",
       "lon":"14.4428992",
       "name":"804351 ~ Dvor"
    },
    {
       "lat":"46.1249088",
       "lon":"14.4367381",
       "name":"804072 ~ Medno"
    },
    {
       "lat":"46.1242538",
       "lon":"14.4370840",
       "name":"804071 ~ Medno"
    },
    {
       "lat":"46.0379334",
       "lon":"14.5054299",
       "name":"603082 ~ Veliki štradon"
    },
    {
       "lat":"46.0410352",
       "lon":"14.5092002",
       "name":"602121 ~ Trnovo"
    },
    {
       "lat":"46.0385255",
       "lon":"14.4998857",
       "name":"603061 ~ Murgle"
    },
    {
       "lat":"46.0377525",
       "lon":"14.5050009",
       "name":"603081 ~ Veliki štradon"
    },
    {
       "lat":"46.0332295",
       "lon":"14.5019404",
       "name":"603072 ~ Pot na Rakovo jelšo"
    },
    {
       "lat":"46.0458121",
       "lon":"14.5007322",
       "name":"602072 ~ Mirje"
    },
    {
       "lat":"46.0403733",
       "lon":"14.4997156",
       "name":"602092 ~ Ziherlova"
    },
    {
       "lat":"46.0443630",
       "lon":"14.5005697",
       "name":"602071 ~ Mirje"
    },
    {
       "lat":"46.0414808",
       "lon":"14.5000167",
       "name":"602093 ~ Ziherlova"
    },
    {
       "lat":"46.0328513",
       "lon":"14.5014220",
       "name":"603071 ~ Pot na Rakovo jelšo"
    },
    {
       "lat":"46.0369676",
       "lon":"14.4428615",
       "name":"714011 ~ Kozarje G.D."
    },
    {
       "lat":"46.0450518",
       "lon":"14.4430599",
       "name":"714022 ~ Gmajna"
    },
    {
       "lat":"46.0449835",
       "lon":"14.4430091",
       "name":"714021 ~ Gmajna"
    },
    {
       "lat":"46.0336271",
       "lon":"14.4520776",
       "name":"704052 ~ Dolgi most Rotar / 704052 ~ Ljubljana Dol.most Rotar"
    },
    {
       "lat":"46.0295750",
       "lon":"14.4398224",
       "name":"704062 ~ Na Gmajnici / 704062 ~ Ljubljana Na Gmajnici"
    },
    {
       "lat":"46.0340163",
       "lon":"14.4536976",
       "name":"704051 ~ Dolgi most Rotar / 704051 ~ Ljubljana Dol.most Rotar"
    },
    {
       "lat":"46.0315338",
       "lon":"14.4458214",
       "name":"704101 ~ Kozarje"
    },
    {
       "lat":"46.0368056",
       "lon":"14.4429715",
       "name":"714012 ~ Kozarje G.D."
    },
    {
       "lat":"46.0320460",
       "lon":"14.4471810",
       "name":"704102 ~ Kozarje"
    },
    {
       "lat":"46.0291876",
       "lon":"14.4390205",
       "name":"704061 ~ Na Gmajnici / 704061 ~ Ljubljana Na Gmajnici"
    },
    {
       "lat":"46.0621818",
       "lon":"14.5706182",
       "name":"Letališka"
    },
    {
       "lat":"46.0621098",
       "lon":"14.5665357",
       "name":"Rog"
    },
    {
       "lat":"46.0620211",
       "lon":"14.5572936",
       "name":"204011 ~ Leskoškova"
    },
    {
       "lat":"46.0650348",
       "lon":"14.5508677",
       "name":"203182 ~ Bratislavska"
    },
    {
       "lat":"46.0619741",
       "lon":"14.5526495",
       "name":"203191 ~ Yulon"
    },
    {
       "lat":"46.0619108",
       "lon":"14.5538142",
       "name":"203192 ~ Yulon"
    },
    {
       "lat":"46.0620803",
       "lon":"14.5634478",
       "name":"GZL"
    },
    {
       "lat":"46.0620508",
       "lon":"14.5676187",
       "name":"Rog"
    },
    {
       "lat":"46.0661485",
       "lon":"14.5514371",
       "name":"203181 ~ Bratislavska"
    },
    {
       "lat":"46.0620103",
       "lon":"14.5644387",
       "name":"GZL"
    },
    {
       "lat":"46.0619610",
       "lon":"14.5582634",
       "name":"204012 ~ Leskoškova"
    },
    {
       "lat":"46.0621289",
       "lon":"14.5713043",
       "name":"Letališka"
    },
    {
       "lat":"46.0621972",
       "lon":"14.5726562",
       "name":"Letališka - obračališče"
    },
    {
       "lat":"46.0977218",
       "lon":"14.5422111",
       "name":"104081 ~ Obrtna cona"
    },
    {
       "lat":"46.0965686",
       "lon":"14.5462135",
       "name":"104091 ~ Cesta na Brod"
    },
    {
       "lat":"46.0979322",
       "lon":"14.5360704",
       "name":"104071 ~ Šlandrova"
    },
    {
       "lat":"46.0978441",
       "lon":"14.5358875",
       "name":"104072 ~ Šlandrova"
    },
    {
       "lat":"46.0973591",
       "lon":"14.5433121",
       "name":"104082 ~ Obrtna cona"
    },
    {
       "lat":"46.0961791",
       "lon":"14.5468004",
       "name":"104092 ~ Cesta na Brod"
    },
    {
       "lat":"46.0937344",
       "lon":"14.5558763",
       "name":"204121 ~ Brnčičeva"
    },
    {
       "lat":"46.0983742",
       "lon":"14.5274389",
       "name":"104062 ~ Elma"
    },
    {
       "lat":"46.0938669",
       "lon":"14.5526553",
       "name":"204111 ~ Slovenijales"
    },
    {
       "lat":"46.0935581",
       "lon":"14.5539029",
       "name":"204112 ~ Slovenijales"
    },
    {
       "lat":"46.0987593",
       "lon":"14.5265870",
       "name":"104061 ~ Elma"
    },
    {
       "lat":"46.1001493",
       "lon":"14.5538003",
       "name":"104182 ~ Ježa"
    },
    {
       "lat":"46.1125940",
       "lon":"14.5462926",
       "name":"Dobrava/Črnučah"
    },
    {
       "lat":"46.0968051",
       "lon":"14.5586202",
       "name":"204132 ~ Nadgorica / Ljubljana Nadgorica"
    },
    {
       "lat":"46.1137448",
       "lon":"14.5472020",
       "name":"Dobrava/Črnučah"
    },
    {
       "lat":"46.1039108",
       "lon":"14.5458938",
       "name":"104171 ~ Zasavska"
    },
    {
       "lat":"46.1034039",
       "lon":"14.5468398",
       "name":"104172 ~ Zasavska"
    },
    {
       "lat":"46.1005380",
       "lon":"14.5533334",
       "name":"104181 ~ Ježa"
    },
    {
       "lat":"46.0972813",
       "lon":"14.5582204",
       "name":"204131 ~ Nadgorica / Ljubljana Nadgorica"
    },
    {
       "lat":"46.0897888",
       "lon":"14.5827172",
       "name":"204154 ~ Šentjakob / Ljubljana Šentjakob"
    },
    {
       "lat":"46.0981138",
       "lon":"14.5870031",
       "name":"204221 ~ Podgorica / Ljubljana Podgorica"
    },
    {
       "lat":"46.0912046",
       "lon":"14.5815851",
       "name":"204152 ~ Šentjakob / Ljubljana Šentjakob"
    },
    {
       "lat":"46.0978621",
       "lon":"14.5869801",
       "name":"204222 ~ Podgorica / Ljubljana Podgorica"
    },
    {
       "lat":"46.0912640",
       "lon":"14.5754430",
       "name":"204142 ~ Belinka / Ljubljana Belinka"
    },
    {
       "lat":"46.0913925",
       "lon":"14.5748877",
       "name":"204141 ~ Belinka / Ljubljana Belinka"
    },
    {
       "lat":"46.0903853",
       "lon":"14.5827373",
       "name":"204153 ~ Šentjakob / Ljubljana Šentjakob"
    },
    {
       "lat":"46.0913603",
       "lon":"14.5826896",
       "name":"204151 ~ Šentjakob / Ljubljana Šentjakob"
    },
    {
       "lat":"46.0655855",
       "lon":"14.5451713",
       "name":"Baby center"
    },
    {
       "lat":"46.0671682",
       "lon":"14.5439146",
       "name":"Aleja mladih"
    },
    {
       "lat":"46.0633782",
       "lon":"14.5482527",
       "name":"203251 ~ BTC-Atlantis / Atlantis"
    },
    {
       "lat":"46.0682959",
       "lon":"14.5419496",
       "name":"203111 ~ BTC-uprava / Stolpnica"
    },
    {
       "lat":"46.0668199",
       "lon":"14.5419053",
       "name":"Kristalna palača"
    },
    {
       "lat":"46.0656359",
       "lon":"14.5484983",
       "name":"203131 ~ BTC-Kolosej / Kolosej"
    },
    {
       "lat":"46.0658876",
       "lon":"14.5473878",
       "name":"203132 ~ BTC-Kolosej"
    },
    {
       "lat":"46.0641475",
       "lon":"14.5435630",
       "name":"Tržnica"
    },
    {
       "lat":"46.0681173",
       "lon":"14.5418522",
       "name":"203112 ~ BTC-uprava"
    },
    {
       "lat":"46.0653693",
       "lon":"14.5440445",
       "name":"203122 ~ BTC-tržnica"
    },
    {
       "lat":"46.0654042",
       "lon":"14.5442051",
       "name":"203121 ~ BTC-tržnica"
    },
    {
       "lat":"46.0452765",
       "lon":"14.5602839",
       "name":"404012 ~ Hrušica"
    },
    {
       "lat":"46.0429590",
       "lon":"14.5804399",
       "name":"404032 ~ Dobrunje"
    },
    {
       "lat":"46.0446025",
       "lon":"14.5702099",
       "name":"404021 ~ Cesta v Bizovik"
    },
    {
       "lat":"46.0445058",
       "lon":"14.5701791",
       "name":"404022 ~ Cesta v Bizovik"
    },
    {
       "lat":"46.0468291",
       "lon":"14.5516388",
       "name":"403062 ~ Rast"
    },
    {
       "lat":"46.0454884",
       "lon":"14.5594671",
       "name":"404011 ~ Hrušica"
    },
    {
       "lat":"46.0432288",
       "lon":"14.5792791",
       "name":"404031 ~ Dobrunje"
    },
    {
       "lat":"46.0469668",
       "lon":"14.5513895",
       "name":"403061 ~ Rast"
    },
    {
       "lat":"46.0402762",
       "lon":"14.5975980",
       "name":"404061 ~ Zadvor"
    },
    {
       "lat":"46.0418960",
       "lon":"14.5865179",
       "name":"404042 ~ Pošta Dobrunje"
    },
    {
       "lat":"46.0412663",
       "lon":"14.5928080",
       "name":"404052 ~ KPL"
    },
    {
       "lat":"46.0474675",
       "lon":"14.5942095",
       "name":"304122 ~ Vevče papirnica"
    },
    {
       "lat":"46.0475046",
       "lon":"14.5943259",
       "name":"304121 ~ Vevče papirnica"
    },
    {
       "lat":"46.0419630",
       "lon":"14.5863247",
       "name":"404041 ~ Pošta Dobrunje"
    },
    {
       "lat":"46.0414619",
       "lon":"14.5920464",
       "name":"404051 ~ KPL"
    },
    {
       "lat":"46.0401365",
       "lon":"14.5977294",
       "name":"404062 ~ Zadvor"
    },
    {
       "lat":"46.0401808",
       "lon":"14.6026008",
       "name":"404072 ~ Cesta v Zavoglje"
    },
    {
       "lat":"46.0402631",
       "lon":"14.6025842",
       "name":"404071 ~ Cesta v Zavoglje"
    },
    {
       "lat":"46.0320201",
       "lon":"14.6044531",
       "name":"404093 ~ Sostro"
    },
    {
       "lat":"46.0371851",
       "lon":"14.6078770",
       "name":"404082 ~ Križišče Sostro"
    },
    {
       "lat":"46.0318377",
       "lon":"14.6043911",
       "name":"404091 ~ Sostro"
    },
    {
       "lat":"46.0372642",
       "lon":"14.6080989",
       "name":"404081 ~ Križišče Sostro"
    },
    {
       "lat":"46.0320683",
       "lon":"14.6043090",
       "name":"404092 ~ Sostro"
    },
    {
       "lat":"46.0382649",
       "lon":"14.5825378",
       "name":"404131 ~ Dobrunje"
    },
    {
       "lat":"46.0359524",
       "lon":"14.5962278",
       "name":"404152 ~ Zadvor 2"
    },
    {
       "lat":"46.0360702",
       "lon":"14.5961484",
       "name":"404151 ~ Zadvor 2"
    },
    {
       "lat":"46.0381830",
       "lon":"14.5825352",
       "name":"404132 ~ Dobrunje"
    },
    {
       "lat":"46.0350827",
       "lon":"14.5995291",
       "name":"404161 ~ OŠ Sostro"
    },
    {
       "lat":"46.0351779",
       "lon":"14.5991155",
       "name":"404162 ~ OŠ Sostro"
    },
    {
       "lat":"46.0382275",
       "lon":"14.5891011",
       "name":"404141 ~ Pod Urhom"
    },
    {
       "lat":"46.0381966",
       "lon":"14.5894449",
       "name":"404142 ~ Pod Urhom"
    },
    {
       "lat":"46.0374237",
       "lon":"14.5709851",
       "name":"404123 ~ Bizovik"
    },
    {
       "lat":"46.0392982",
       "lon":"14.5664286",
       "name":"404111 ~ Hruševska"
    },
    {
       "lat":"46.0391847",
       "lon":"14.5669757",
       "name":"404112 ~ Hruševska"
    },
    {
       "lat":"46.0375153",
       "lon":"14.5711201",
       "name":"404121 ~ Bizovik"
    },
    {
       "lat":"46.0459460",
       "lon":"14.5440766",
       "name":"403071 ~ Bilečanska"
    },
    {
       "lat":"46.0412645",
       "lon":"14.5559323",
       "name":"404101 ~ Krožna pot"
    },
    {
       "lat":"46.0413013",
       "lon":"14.5557416",
       "name":"404102 ~ Krožna pot"
    },
    {
       "lat":"46.0290538",
       "lon":"14.5219728",
       "name":"503052 ~ Mihov štradon"
    },
    {
       "lat":"46.0331808",
       "lon":"14.5133776",
       "name":"603093 ~ Galjevica"
    },
    {
       "lat":"46.0183561",
       "lon":"14.5353705",
       "name":"504061 ~ NS Rudnik"
    },
    {
       "lat":"46.0265750",
       "lon":"14.5275466",
       "name":"504041 ~ Jurčkova"
    },
    {
       "lat":"46.0202616",
       "lon":"14.5348105",
       "name":"504052 ~ NS Rudnik"
    },
    {
       "lat":"46.0259289",
       "lon":"14.5286971",
       "name":"504042 ~ Jurčkova"
    },
    {
       "lat":"46.0295934",
       "lon":"14.5210585",
       "name":"503051 ~ Mihov štradon"
    },
    {
       "lat":"46.0202228",
       "lon":"14.5351463",
       "name":"504051 ~ NS Rudnik"
    },
    {
       "lat":"46.0332065",
       "lon":"14.5125422",
       "name":"603092 ~ Livada"
    },
    {
       "lat":"46.0342623",
       "lon":"14.5109839",
       "name":"603091 ~ Livada"
    },
    {
       "lat":"46.0331734",
       "lon":"14.5130590",
       "name":"603094 ~ Galjevica"
    },
    {
       "lat":"46.0356495",
       "lon":"14.5129500",
       "name":"603122 ~ Ižanska"
    },
    {
       "lat":"46.0360610",
       "lon":"14.5131376",
       "name":"603121 ~ Ižanska"
    },
    {
       "lat":"46.0621117",
       "lon":"14.5449928",
       "name":"203141 ~ BTC-Emporium"
    },
    {
       "lat":"46.0617921",
       "lon":"14.5449219",
       "name":"203142 ~ BTC-Emporium"
    },
    {
       "lat":"46.0148176",
       "lon":"14.5121125",
       "name":"604023 ~ Barje"
    },
    {
       "lat":"46.0199734",
       "lon":"14.5103664",
       "name":"604012 ~ Iški most"
    },
    {
       "lat":"46.0273225",
       "lon":"14.5116807",
       "name":"603101 ~ Ilovica"
    },
    {
       "lat":"46.0200497",
       "lon":"14.5104952",
       "name":"604011 ~ Iški most"
    },
    {
       "lat":"46.0083208",
       "lon":"14.5141895",
       "name":"604051 ~ Pri Maranzu"
    },
    {
       "lat":"46.0149658",
       "lon":"14.5119363",
       "name":"604022 ~ Barje"
    },
    {
       "lat":"46.0277527",
       "lon":"14.5116217",
       "name":"603102 ~ Ilovica"
    },
    {
       "lat":"46.0068320",
       "lon":"14.5145523",
       "name":"604052 ~ Pri Maranzu"
    },
    {
       "lat":"46.0001634",
       "lon":"14.5167900",
       "name":"604041 ~ Havptmance"
    },
    {
       "lat":"46.0005776",
       "lon":"14.5165484",
       "name":"604042 ~ Havptmance"
    },
    {
       "lat":"46.0245244",
       "lon":"14.5111960",
       "name":"603111 ~ Lahova pot"
    },
    {
       "lat":"46.0246622",
       "lon":"14.5110887",
       "name":"603112 ~ Lahova pot"
    },
    {
       "lat":"46.0058683",
       "lon":"14.4906547",
       "name":"614042 ~ Capuder"
    },
    {
       "lat":"46.0118316",
       "lon":"14.5059315",
       "name":"614011 ~ Plečnikova cerkev"
    },
    {
       "lat":"46.0024794",
       "lon":"14.4815763",
       "name":"614052 ~ Zidarjevec"
    },
    {
       "lat":"46.0079046",
       "lon":"14.4963331",
       "name":"614031 ~ Dornig"
    },
    {
       "lat":"46.0024793",
       "lon":"14.4818472",
       "name":"614051 ~ Zidarjevec"
    },
    {
       "lat":"46.0119974",
       "lon":"14.5060896",
       "name":"614012 ~ Plečnikova cerkev"
    },
    {
       "lat":"45.9973519",
       "lon":"14.4701467",
       "name":"614071 ~ Lipe"
    },
    {
       "lat":"45.9950536",
       "lon":"14.4652203",
       "name":"614081 ~ Kozler"
    },
    {
       "lat":"46.0055273",
       "lon":"14.4900083",
       "name":"614041 ~ Capuder"
    },
    {
       "lat":"45.9908627",
       "lon":"14.4566210",
       "name":"614091 ~ Snoj"
    },
    {
       "lat":"45.9909334",
       "lon":"14.4565244",
       "name":"614092 ~ Snoj"
    },
    {
       "lat":"46.0003461",
       "lon":"14.4767783",
       "name":"614061 ~ Rutar"
    },
    {
       "lat":"46.0098349",
       "lon":"14.5007929",
       "name":"614022 ~ Brglezov štradon"
    },
    {
       "lat":"46.0078506",
       "lon":"14.4959120",
       "name":"614032 ~ Dornig"
    },
    {
       "lat":"46.0098713",
       "lon":"14.5011128",
       "name":"614021 ~ Brglezov štradon"
    },
    {
       "lat":"45.9978810",
       "lon":"14.4710293",
       "name":"614072 ~ Lipe"
    },
    {
       "lat":"45.9949996",
       "lon":"14.4648769",
       "name":"614082 ~ Kozler"
    },
    {
       "lat":"46.0001737",
       "lon":"14.4761555",
       "name":"614062 ~ Rutar"
    },
    {
       "lat":"46.0511855",
       "lon":"14.4923401",
       "name":"702011 ~ Pod Rožnikom"
    },
    {
       "lat":"46.0495252",
       "lon":"14.4881860",
       "name":"702021 ~ Študentsko naselje"
    },
    {
       "lat":"46.0493041",
       "lon":"14.4874533",
       "name":"702022 ~ Študentsko naselje"
    },
    {
       "lat":"46.0510813",
       "lon":"14.4918756",
       "name":"702012 ~ Pod Rožnikom"
    },
    {
       "lat":"46.0451912",
       "lon":"14.4739022",
       "name":"703032 ~ Jamnikarjeva"
    },
    {
       "lat":"46.0438978",
       "lon":"14.4552781",
       "name":"704032 ~ Vrhovci"
    },
    {
       "lat":"46.0471378",
       "lon":"14.4618266",
       "name":"703191 ~ Pot R. križa"
    },
    {
       "lat":"46.0449274",
       "lon":"14.4750787",
       "name":"703031 ~ Jamnikarjeva"
    },
    {
       "lat":"46.0473309",
       "lon":"14.4646047",
       "name":"703051 ~ Podmornica"
    },
    {
       "lat":"46.0478522",
       "lon":"14.4819475",
       "name":"703012 ~ Rožna dolina"
    },
    {
       "lat":"46.0413743",
       "lon":"14.4633622",
       "name":"703211 ~ I. Kobilca"
    },
    {
       "lat":"46.0463566",
       "lon":"14.4689235",
       "name":"703042 ~ Viško polje"
    },
    {
       "lat":"46.0459867",
       "lon":"14.4695380",
       "name":"703041 ~ Viško polje"
    },
    {
       "lat":"46.0456414",
       "lon":"14.4808869",
       "name":"703022 ~ Cesta XV"
    },
    {
       "lat":"46.0474759",
       "lon":"14.4536972",
       "name":"704023 ~ Brdo"
    },
    {
       "lat":"46.0473838",
       "lon":"14.4642806",
       "name":"703052 ~ Podmornica"
    },
    {
       "lat":"46.0478309",
       "lon":"14.4820949",
       "name":"703011 ~ Rožna dolina"
    },
    {
       "lat":"46.0475424",
       "lon":"14.4538055",
       "name":"704026 ~ Brdo"
    },
    {
       "lat":"46.0459699",
       "lon":"14.4808215",
       "name":"703021 ~ Cesta XV"
    },
    {
       "lat":"46.0519350",
       "lon":"14.4480773",
       "name":"704041 ~ Bokalce"
    },
    {
       "lat":"46.0441624",
       "lon":"14.4555506",
       "name":"704032 ~ Vrhovci"
    },
    {
       "lat":"46.0365379",
       "lon":"14.6510036",
       "name":"405021 ~ Besnica/Lj."
    },
    {
       "lat":"46.0317387",
       "lon":"14.6682523",
       "name":"405041 ~ Blaževo"
    },
    {
       "lat":"46.0354724",
       "lon":"14.6576751",
       "name":"405031 ~ Besnica/Lj. Š."
    },
    {
       "lat":"46.0316651",
       "lon":"14.6682523",
       "name":"405042 ~ Blaževo"
    },
    {
       "lat":"46.0351990",
       "lon":"14.6582503",
       "name":"405032 ~ Besnica/Lj. Š."
    },
    {
       "lat":"46.0399980",
       "lon":"14.6401178",
       "name":"405012 ~ Javor/K."
    },
    {
       "lat":"46.0400311",
       "lon":"14.6400452",
       "name":"405011 ~Javor/K."
    },
    {
       "lat":"46.0368130",
       "lon":"14.6502953",
       "name":"405022 ~ Besnica/Lj."
    },
    {
       "lat":"46.0332881",
       "lon":"14.6882781",
       "name":"405052 ~ Besnica ambulanta"
    },
    {
       "lat":"46.0259416",
       "lon":"14.7017256",
       "name":"405072 ~ Zg. Besnica"
    },
    {
       "lat":"46.0203607",
       "lon":"14.7100558",
       "name":"405062 ~ Besnica kamnolom"
    },
    {
       "lat":"46.0202381",
       "lon":"14.7105632",
       "name":"405061 ~ Besnica kamnolom"
    },
    {
       "lat":"46.0333616",
       "lon":"14.6881557",
       "name":"405051 ~ Besnica ambulanta"
    },
    {
       "lat":"46.0263074",
       "lon":"14.7017591",
       "name":"405071 ~ Zg. Besnica"
    },
    {
       "lat":"46.0194450",
       "lon":"14.7184355",
       "name":"406012 ~ Šemnikar"
    },
    {
       "lat":"46.0195317",
       "lon":"14.7183864",
       "name":"406011 ~ Šemnikar"
    },
    {
       "lat":"46.0320850",
       "lon":"14.7172828",
       "name":"406061 ~ Volavlje"
    },
    {
       "lat":"46.0321837",
       "lon":"14.7173042",
       "name":"406062 ~ Volavlje"
    },
    {
       "lat":"46.0451131",
       "lon":"14.7095972",
       "name":"406072 ~ Lanišče"
    },
    {
       "lat":"46.0453287",
       "lon":"14.7095155",
       "name":"406071 ~ Lanišče"
    },
    {
       "lat":"46.0178870",
       "lon":"14.6396710",
       "name":"415091 ~ Pri Dolencu"
    },
    {
       "lat":"46.0178137",
       "lon":"14.6396576",
       "name":"415092 ~ Pri Dolencu"
    },
    {
       "lat":"46.0170411",
       "lon":"14.6233018",
       "name":"415021 ~ Podlipoglav"
    },
    {
       "lat":"46.0169803",
       "lon":"14.6232379",
       "name":"415022 ~ Podlipoglav"
    },
    {
       "lat":"46.0124997",
       "lon":"14.6528971",
       "name":"425011 ~ Javor pri Sostrem K."
    },
    {
       "lat":"46.0216366",
       "lon":"14.6123873",
       "name":"415011 ~ Sadinja vas"
    },
    {
       "lat":"46.0215952",
       "lon":"14.6122673",
       "name":"415012 ~ Sadinja vas"
    },
    {
       "lat":"46.0124208",
       "lon":"14.6528490",
       "name":"425012 ~ Javor pri Sostrem K."
    },
    {
       "lat":"46.0051616",
       "lon":"14.6531960",
       "name":"415031 ~ Veliki Lipoglav"
    },
    {
       "lat":"45.9991971",
       "lon":"14.6573208",
       "name":"415042 ~ Selo pri Pancah K."
    },
    {
       "lat":"46.0054489",
       "lon":"14.6531406",
       "name":"415032 ~ Veliki Lipoglav"
    },
    {
       "lat":"45.9992884",
       "lon":"14.6573181",
       "name":"415041 ~ Selo pri Pancah K."
    },
    {
       "lat":"45.9918715",
       "lon":"14.6590771",
       "name":"415081 ~ Primožev ovinek"
    },
    {
       "lat":"45.9892757",
       "lon":"14.6576290",
       "name":"415071 ~ Pri Dogonivar"
    },
    {
       "lat":"45.9919619",
       "lon":"14.6587462",
       "name":"415082 ~ Primožev ovinek"
    },
    {
       "lat":"45.9922858",
       "lon":"14.6476872",
       "name":"415061 ~ Mali Lipoglav"
    },
    {
       "lat":"45.9893658",
       "lon":"14.6575791",
       "name":"415072 ~ Pri Dogonivar"
    },
    {
       "lat":"45.9886014",
       "lon":"14.6642078",
       "name":"415051 ~ Pance K."
    },
    {
       "lat":"45.9886397",
       "lon":"14.6637009",
       "name":"415052 ~ Pance K."
    },
    {
       "lat":"45.9609247",
       "lon":"14.5187211",
       "name":"625022 ~ Ig"
    },
    {
       "lat":"45.9623862",
       "lon":"14.5305082",
       "name":"605082 ~ Ig Petrol"
    },
    {
       "lat":"45.9604375",
       "lon":"14.5181819",
       "name":"625021 ~ Ig"
    },
    {
       "lat":"45.9589229",
       "lon":"14.5280236",
       "name":"605011 ~ Ig AP"
    },
    {
       "lat":"45.9587774",
       "lon":"14.5279738",
       "name":"605012 ~ Ig AP"
    },
    {
       "lat":"45.9640121",
       "lon":"14.5297837",
       "name":"605081 ~ Ig Petrol"
    },
    {
       "lat":"45.9710984",
       "lon":"14.5150582",
       "name":"615011 ~ Iška Loka"
    },
    {
       "lat":"45.9711611",
       "lon":"14.5151644",
       "name":"615012 ~ Iška Loka"
    },
    {
       "lat":"45.9676399",
       "lon":"14.4921537",
       "name":"615042 ~ Brest"
    },
    {
       "lat":"45.9667613",
       "lon":"14.4734214",
       "name":"615051 ~ Tomišelj K"
    },
    {
       "lat":"45.9669397",
       "lon":"14.4736718",
       "name":"Tomišelj K"
    },
    {
       "lat":"45.9718310",
       "lon":"14.4978699",
       "name":"615032 ~ Matena"
    },
    {
       "lat":"45.9739885",
       "lon":"14.5083804",
       "name":"615022 ~ Na Gmajni"
    },
    {
       "lat":"45.9720996",
       "lon":"14.4682538",
       "name":"615071 / 615072 ~ Podkraj"
    },
    {
       "lat":"45.9738998",
       "lon":"14.5082565",
       "name":"615021 ~ Na Gmajni"
    },
    {
       "lat":"45.9370009",
       "lon":"14.5144720",
       "name":"626011 ~ Iška vas obr."
    },
    {
       "lat":"45.9234834",
       "lon":"14.5081500",
       "name":"626041 ~ Iška obračališče"
    },
    {
       "lat":"45.9514499",
       "lon":"14.4902184",
       "name":"625012 ~ Vrbljene"
    },
    {
       "lat":"45.9441993",
       "lon":"14.4888489",
       "name":"626021 ~ Strahomer"
    },
    {
       "lat":"45.9518885",
       "lon":"14.4901983",
       "name":"625012 ~ Vrbljene"
    },
    {
       "lat":"45.9642304",
       "lon":"14.4803403",
       "name":"615062 ~ Tomišelj"
    },
    {
       "lat":"45.9646951",
       "lon":"14.4800886",
       "name":"615061 ~ Tomišelj"
    },
    {
       "lat":"45.9401534",
       "lon":"14.5111880",
       "name":"625052 ~ Iška vas"
    },
    {
       "lat":"45.9442965",
       "lon":"14.4887167",
       "name":"626022 ~ Strahomer"
    },
    {
       "lat":"45.9514095",
       "lon":"14.5120946",
       "name":"625041 ~ Kot"
    },
    {
       "lat":"45.9574028",
       "lon":"14.5136201",
       "name":"625031 ~ Staje"
    },
    {
       "lat":"45.9717751",
       "lon":"14.4980683",
       "name":"615031 ~ Matena"
    },
    {
       "lat":"45.9674155",
       "lon":"14.4919405",
       "name":"615041 ~ Brest"
    },
    {
       "lat":"45.9552429",
       "lon":"14.5329865",
       "name":"605022 ~ Ig mizarstvo"
    },
    {
       "lat":"45.9558179",
       "lon":"14.5318633",
       "name":"605021 ~ Ig mizarstvo"
    },
    {
       "lat":"45.9302136",
       "lon":"14.5384765",
       "name":"605042 ~ Podgozd"
    },
    {
       "lat":"45.9196802",
       "lon":"14.5388914",
       "name":"605051 ~ Škrilje"
    },
    {
       "lat":"45.9467909",
       "lon":"14.5321127",
       "name":"605031 ~ Dobravica K"
    },
    {
       "lat":"45.9464885",
       "lon":"14.5318063",
       "name":"605032 ~ Dobravica K"
    },
    {
       "lat":"45.9305376",
       "lon":"14.5389412",
       "name":"605041 ~ Podgozd"
    },
    {
       "lat":"45.9195494",
       "lon":"14.5385075",
       "name":"605052 ~ Škrilje"
    },
    {
       "lat":"45.9108730",
       "lon":"14.5449683",
       "name":"605062 ~ Golo"
    },
    {
       "lat":"45.9033011",
       "lon":"14.5468746",
       "name":"605071 ~ Hrastje pri Golem"
    },
    {
       "lat":"45.9038565",
       "lon":"14.5467873",
       "name":"605072 ~ Hrastje pri Golem"
    },
    {
       "lat":"45.9109439",
       "lon":"14.5450555",
       "name":"605061 ~ Golo"
    },
    {
       "lat":"46.0240173",
       "lon":"14.4178175",
       "name":"704081 ~ Pri Poku / 704081 ~ Brezovica pri Ljubljani"
    },
    {
       "lat":"46.0244405",
       "lon":"14.4212305",
       "name":"704072 ~ Radna"
    },
    {
       "lat":"46.0239768",
       "lon":"14.4129774",
       "name":"704082 ~ Brezovica pri Ljubljani"
    },
    {
       "lat":"46.0226480",
       "lon":"14.4073065",
       "name":"704091 ~ Lukovica pri Brezovici"
    },
    {
       "lat":"46.0222969",
       "lon":"14.4026630",
       "name":"704092 ~ Lukovica pri Brezovici"
    },
    {
       "lat":"46.0027487",
       "lon":"14.3804182",
       "name":"Plešivica/Logu spomenik"
    },
    {
       "lat":"46.0028624",
       "lon":"14.3803513",
       "name":"Plešivica/Logu spomenik"
    },
    {
       "lat":"45.9999317",
       "lon":"14.3855907",
       "name":"Plešivica/Logu grad"
    },
    {
       "lat":"45.9876423",
       "lon":"14.4013745",
       "name":"Notranje Gorice Š"
    },
    {
       "lat":"46.0022606",
       "lon":"14.4135237",
       "name":"745021 ~ Erbežnik / Vnanje Gorice Japelj"
    },
    {
       "lat":"46.0103069",
       "lon":"14.4127714",
       "name":"744022 ~ Japelj / Vnanje Gorice rampe"
    },
    {
       "lat":"45.9879330",
       "lon":"14.4023989",
       "name":"Notranje Gorice Š"
    },
    {
       "lat":"46.0118383",
       "lon":"14.4130642",
       "name":"744021 ~ Japelj / Vnanje Gorice rampe"
    },
    {
       "lat":"45.9959581",
       "lon":"14.4064303",
       "name":"745032 ~ Mostiček / Notranje Gorice mostiček"
    },
    {
       "lat":"45.9971980",
       "lon":"14.4078264",
       "name":"745031 ~ Mostiček / Notranje Gorice mostiček"
    },
    {
       "lat":"46.0021114",
       "lon":"14.4132731",
       "name":"745022 ~ Erbežnik / Vnanje Gorice Japelj"
    },
    {
       "lat":"45.9878995",
       "lon":"14.4015944",
       "name":"745041 ~ Notranje Gorice"
    },
    {
       "lat":"46.0166925",
       "lon":"14.4144620",
       "name":"744012 ~ Šolska / Brezovica pri Ljubljani Š"
    },
    {
       "lat":"46.0178715",
       "lon":"14.4151535",
       "name":"744011 ~ Šolska / Brezovica pri Ljubljani Š"
    },
    {
       "lat":"45.9708055",
       "lon":"14.4346848",
       "name":"745061 ~ Jezero"
    },
    {
       "lat":"45.9730687",
       "lon":"14.4216622",
       "name":"745052 ~ Podpeč"
    },
    {
       "lat":"45.9734371",
       "lon":"14.4211279",
       "name":"745051 ~ Podpeč / Podpeč/Lj."
    },
    {
       "lat":"45.9565514",
       "lon":"14.4076782",
       "name":"Kamnik pod Krimom"
    },
    {
       "lat":"45.9542543",
       "lon":"14.4159388",
       "name":"Preserje/Krimom"
    },
    {
       "lat":"45.9661726",
       "lon":"14.4031445",
       "name":"Kamnik pod Krimom K"
    },
    {
       "lat":"45.9661670",
       "lon":"14.4028657",
       "name":"Kamnik pod Krimom K"
    },
    {
       "lat":"45.9665117",
       "lon":"14.4112785",
       "name":"Podpeč/Lj.Jereb"
    },
    {
       "lat":"45.9660383",
       "lon":"14.4030186",
       "name":"Kamnik pod Krimom K"
    },
    {
       "lat":"45.9565416",
       "lon":"14.4074252",
       "name":"Kamnik pod Krimom"
    },
    {
       "lat":"45.9666629",
       "lon":"14.4112557",
       "name":"Podpeč/Lj.Jereb"
    },
    {
       "lat":"45.9065328",
       "lon":"14.4310624",
       "name":"Novaki"
    },
    {
       "lat":"45.9531199",
       "lon":"14.4239035",
       "name":"D. Brezovica Baša"
    },
    {
       "lat":"45.9497946",
       "lon":"14.4289242",
       "name":"D. Brezovica Landovec"
    },
    {
       "lat":"45.9497648",
       "lon":"14.4288410",
       "name":"D. Brezovica Landovec"
    },
    {
       "lat":"45.9065309",
       "lon":"14.4309506",
       "name":"Novaki"
    },
    {
       "lat":"45.9409965",
       "lon":"14.4367138",
       "name":"G. Brezovica"
    },
    {
       "lat":"45.9407429",
       "lon":"14.4367915",
       "name":"G. Brezovica"
    },
    {
       "lat":"45.9530394",
       "lon":"14.4238312",
       "name":"D. Brezovica Baša"
    },
    {
       "lat":"45.9612877",
       "lon":"14.3905435",
       "name":"Prevalje pod Krimom"
    },
    {
       "lat":"45.9613679",
       "lon":"14.3904391",
       "name":"Prevalje pod Krimom"
    },
    {
       "lat":"46.0179465",
       "lon":"14.3881623",
       "name":"705011 ~ Dragomer"
    },
    {
       "lat":"46.0175407",
       "lon":"14.3868072",
       "name":"705012 ~ Dragomer"
    },
    {
       "lat":"46.0443939",
       "lon":"14.4288313",
       "name":"715022 ~ Komanija"
    },
    {
       "lat":"46.0395128",
       "lon":"14.4286805",
       "name":"715031 ~ Podsmreka G.D."
    },
    {
       "lat":"46.0437959",
       "lon":"14.4287772",
       "name":"715021 ~ Komanija"
    },
    {
       "lat":"46.0284725",
       "lon":"14.4334470",
       "name":"715041 ~ Podsmreka/Gorjancu"
    },
    {
       "lat":"46.0354039",
       "lon":"14.4305692",
       "name":"725041 ~ Podsmreka Rotar"
    },
    {
       "lat":"46.0288226",
       "lon":"14.4336562",
       "name":"715042 ~ Podsmreka/Gorjancu"
    },
    {
       "lat":"46.0474031",
       "lon":"14.4271163",
       "name":"715012 ~ Draževnik"
    },
    {
       "lat":"46.0392577",
       "lon":"14.4291787",
       "name":"715032 ~ Podsmreka G.D."
    },
    {
       "lat":"46.0462149",
       "lon":"14.4276245",
       "name":"715011 ~ Draževnik"
    },
    {
       "lat":"46.0354593",
       "lon":"14.4308156",
       "name":"715042 ~ Podsmreka Rotar"
    },
    {
       "lat":"46.0509845",
       "lon":"14.4240566",
       "name":"715042 ~ Razori K."
    },
    {
       "lat":"46.0507454",
       "lon":"14.4246773",
       "name":"715041 ~ Razori K."
    },
    {
       "lat":"46.0498063",
       "lon":"14.4247108",
       "name":"715041 ~ Razori K."
    },
    {
       "lat":"46.0489047",
       "lon":"14.4353981",
       "name":"714031 ~ Razori Rogovilc"
    },
    {
       "lat":"46.0487999",
       "lon":"14.4358185",
       "name":"714032 ~ Razori Rogovilc"
    },
    {
       "lat":"46.0540253",
       "lon":"14.4174945",
       "name":"725011 ~ Dobrova"
    },
    {
       "lat":"46.0541267",
       "lon":"14.4173806",
       "name":"725012 ~ Dobrova"
    },
    {
       "lat":"46.0542914",
       "lon":"14.4180404",
       "name":"725012 ~ Dobrova"
    },
    {
       "lat":"46.0545362",
       "lon":"14.4174718",
       "name":"725011 ~ Dobrova"
    },
    {
       "lat":"46.0409979",
       "lon":"14.3812927",
       "name":"715082 ~ Dobrova Maček"
    },
    {
       "lat":"46.0471552",
       "lon":"14.4063929",
       "name":"715062 ~ Dobrova graben"
    },
    {
       "lat":"46.0408999",
       "lon":"14.3813665",
       "name":"715081 ~ Dobrova Maček"
    },
    {
       "lat":"46.0421771",
       "lon":"14.3929430",
       "name":"715072 ~ Dobrova vodomet"
    },
    {
       "lat":"46.0469505",
       "lon":"14.4064009",
       "name":"715061 ~ Dobrova graben"
    },
    {
       "lat":"46.0421184",
       "lon":"14.3930824",
       "name":"715071 ~ Dobrova vodomet"
    },
    {
       "lat":"46.0766349",
       "lon":"14.3841591",
       "name":"725062 ~ Žirovnik"
    },
    {
       "lat":"46.0727191",
       "lon":"14.4003161",
       "name":"725041 ~ Gabrje/D. Brvičar"
    },
    {
       "lat":"46.0730814",
       "lon":"14.3995973",
       "name":"725042 ~ Gabrje/D. Brvičar"
    },
    {
       "lat":"46.0680883",
       "lon":"14.4088328",
       "name":"725031 ~ Gabrje pri Dobrovi"
    },
    {
       "lat":"46.0766253",
       "lon":"14.3843812",
       "name":"725061 ~ Žerovnik"
    },
    {
       "lat":"46.0747408",
       "lon":"14.3930024",
       "name":"725052 ~ Hruševo"
    },
    {
       "lat":"46.0682530",
       "lon":"14.4087912",
       "name":"725032 ~ Gabrje pri Dobrovi"
    },
    {
       "lat":"46.0632563",
       "lon":"14.4123572",
       "name":"725021 ~ Šujica"
    },
    {
       "lat":"46.0746674",
       "lon":"14.3928387",
       "name":"725051 ~ Hruševo"
    },
    {
       "lat":"46.0629622",
       "lon":"14.4126657",
       "name":"725021 ~ Šujica"
    },
    {
       "lat":"46.1379603",
       "lon":"14.4150878",
       "name":"805012 ~ Medvode a.p. / Medvode"
    },
    {
       "lat":"46.1384031",
       "lon":"14.4133478",
       "name":"805011 ~ Medvode a.p. / Medvode"
    },
    {
       "lat":"46.1289113",
       "lon":"14.4235076",
       "name":"Medno g.Tome"
    },
    {
       "lat":"46.1370571",
       "lon":"14.4020428",
       "name":"815181 ~ Vaše / Vaše/Medvodah"
    },
    {
       "lat":"46.1289875",
       "lon":"14.4235560",
       "name":"Medno g.Tome"
    },
    {
       "lat":"46.1347806",
       "lon":"14.4115309",
       "name":"815152 ~ Preska / Preska pri Medvodah"
    },
    {
       "lat":"46.1363307",
       "lon":"14.4050618",
       "name":"815172 ~ Vaše Helios / Vaše/Medvodah"
    },
    {
       "lat":"46.1288452",
       "lon":"14.4445006",
       "name":"815102 ~ Gostilna Kovač"
    },
    {
       "lat":"46.1288577",
       "lon":"14.4444090",
       "name":"815101 ~ Gostilna Kovač"
    },
    {
       "lat":"46.1376015",
       "lon":"14.4333095",
       "name":"815031 ~ Zg. Pirniče"
    },
    {
       "lat":"46.1370717",
       "lon":"14.4340929",
       "name":"815032 ~ Zg. Pirniče"
    },
    {
       "lat":"46.1344720",
       "lon":"14.4381316",
       "name":"815022 ~ Sp. Pirniče"
    },
    {
       "lat":"46.1343292",
       "lon":"14.4390940",
       "name":"815021 ~ Sp. Pirniče"
    },
    {
       "lat":"46.1189322",
       "lon":"14.5505709",
       "name":"Trzin ind.cona"
    },
    {
       "lat":"46.1198591",
       "lon":"14.5515795",
       "name":"Trzin ind.cona"
    },
    {
       "lat":"46.1280026",
       "lon":"14.5583212",
       "name":"Trzin Mlake"
    },
    {
       "lat":"46.1288141",
       "lon":"14.5598079",
       "name":"Trzin Mlake"
    },
    {
       "lat":"46.1317624",
       "lon":"14.5615293",
       "name":"Trzin dom"
    },
    {
       "lat":"46.1332825",
       "lon":"14.5644230",
       "name":"Trzin GD"
    },
    {
       "lat":"46.1292100",
       "lon":"14.5592125",
       "name":"Trzin Mlake"
    },
    {
       "lat":"46.1293383",
       "lon":"14.5590095",
       "name":"Trzin Mlake"
    },
    {
       "lat":"46.1331480",
       "lon":"14.5643425",
       "name":"Trzin GD"
    },
    {
       "lat":"46.1316964",
       "lon":"14.5616379",
       "name":"Trzin dom"
    },
    {
       "lat":"46.1316152",
       "lon":"14.5636413",
       "name":"Trzin"
    },
    {
       "lat":"46.1330336",
       "lon":"14.5665188",
       "name":"Trzin"
    },
    {
       "lat":"46.1382843",
       "lon":"14.5631178",
       "name":"Trzin vas"
    },
    {
       "lat":"46.1391269",
       "lon":"14.5630903",
       "name":"Trzin vas"
    },
    {
       "lat":"46.0910015",
       "lon":"14.5964223",
       "name":"204161 ~ Reaktor / Ljubljana Brinje"
    },
    {
       "lat":"46.0908106",
       "lon":"14.5973602",
       "name":"204162 ~ Reaktor / Ljubljana Brinje"
    },
    {
       "lat":"46.0901874",
       "lon":"14.6130400",
       "name":"204171 ~ Beričevo / Ljubljana Beričevo"
    },
    {
       "lat":"46.0899196",
       "lon":"14.6132680",
       "name":"Ljubljana Beričevo"
    },
    {
       "lat":"46.0896480",
       "lon":"14.6305415",
       "name":"Videm/Ljubljani"
    },
    {
       "lat":"46.0897710",
       "lon":"14.6304500",
       "name":"Videm/Ljubljani"
    },
    {
       "lat":"46.0947242",
       "lon":"14.6756804",
       "name":"Dolsko"
    },
    {
       "lat":"46.0002905",
       "lon":"14.5552554",
       "name":"504081 ~ Lavrica pri Malči / Lavrica pri Malči"
    },
    {
       "lat":"46.0064613",
       "lon":"14.5485041",
       "name":"504072 ~ Pod hribom / Ljubljana pod Hribom"
    },
    {
       "lat":"46.0015993",
       "lon":"14.5542376",
       "name":"504082 ~ Lavrica pri Malči / Lavrica pri Malči"
    },
    {
       "lat":"46.0063309",
       "lon":"14.5490647",
       "name":"504071 ~ Pod hribom / Ljubljana pod Hribom"
    },
    {
       "lat":"45.9895955",
       "lon":"14.5665057",
       "name":"504101 ~ Škofljica Petkovšek / Škofljica Petkovšek"
    },
    {
       "lat":"45.9895404",
       "lon":"14.5663911",
       "name":"504102 ~ Škofljica Petkovšek / Škofljica Petkovšek"
    },
    {
       "lat":"45.9971342",
       "lon":"14.5579337",
       "name":"504091 ~ Lavrica / Lavrica"
    },
    {
       "lat":"45.9969497",
       "lon":"14.5581469",
       "name":"504092 ~ Lavrica / Lavrica"
    },
    {
       "lat":"45.9858545",
       "lon":"14.5717134",
       "name":"504112 ~ Škofljica žaga / Škofljica žaga"
    },
    {
       "lat":"45.9862198",
       "lon":"14.5713043",
       "name":"504111 ~ Škofljica žaga / Škofljica žaga"
    },
    {
       "lat":"45.9843460",
       "lon":"14.5745286",
       "name":"515011 ~ Škofljica špica / Škofljica"
    },
    {
       "lat":"45.9792993",
       "lon":"14.5752090",
       "name":"515013 ~ Škofljica obračališče"
    },
    {
       "lat":"45.9830401",
       "lon":"14.5770564",
       "name":"515012 ~ Škofljica špica"
    },
    {
       "lat":"45.9521693",
       "lon":"14.5727575",
       "name":"Pijava Gorica"
    },
    {
       "lat":"45.9713034",
       "lon":"14.5753155",
       "name":"Glinek/Škofljici"
    },
    {
       "lat":"45.9520504",
       "lon":"14.5730147",
       "name":"Pijava Gorica"
    },
    {
       "lat":"45.9522154",
       "lon":"14.5730332",
       "name":"Pijava Gorica"
    },
    {
       "lat":"45.9712926",
       "lon":"14.5750986",
       "name":"Glinek/Škofljici"
    },
    {
       "lat":"45.9283992",
       "lon":"14.5741899",
       "name":"Želimlje Nučič"
    },
    {
       "lat":"45.9284445",
       "lon":"14.5742733",
       "name":"Želimlje Nučič"
    },
    {
       "lat":"45.9449673",
       "lon":"14.5677652",
       "name":"Želimlje Rogovilec"
    },
    {
       "lat":"45.9449691",
       "lon":"14.5678725",
       "name":"Želimlje Rogovilec"
    },
    {
       "lat":"45.9189873",
       "lon":"14.5741127",
       "name":"Želimlje"
    },
    {
       "lat":"45.9289753",
       "lon":"14.5777433",
       "name":"Smrjene"
    },
    {
       "lat":"45.9042566",
       "lon":"14.5907036",
       "name":"Vrh nad Želimljami"
    },
    {
       "lat":"45.9186210",
       "lon":"14.5840654",
       "name":"Gradišče nad Pijavo G."
    },
    {
       "lat":"45.9196285",
       "lon":"14.5838269",
       "name":"Gradišče nad Pijavo G."
    },
    {
       "lat":"45.9036220",
       "lon":"14.5908431",
       "name":"Vrh nad Želimljami"
    },
    {
       "lat":"45.9858631",
       "lon":"14.5811161",
       "name":"505011 ~ Škofljica Javornik"
    },
    {
       "lat":"45.9855666",
       "lon":"14.5809015",
       "name":"505012 ~ Škofljica Javornik"
    },
    {
       "lat":"45.9854940",
       "lon":"14.5886386",
       "name":"505022 ~ Reber pri Škofljici"
    },
    {
       "lat":"45.9860792",
       "lon":"14.5877830",
       "name":"505021 ~ Reber pri Škofljici"
    },
    {
       "lat":"45.9853884",
       "lon":"14.5941789",
       "name":"505031 ~ Špilar"
    },
    {
       "lat":"45.9857998",
       "lon":"14.5942258",
       "name":"505032 ~ Špilar"
    },
    {
       "lat":"45.9816411",
       "lon":"14.5994376",
       "name":"505042 ~ Mali Vrh"
    },
    {
       "lat":"45.9816956",
       "lon":"14.5995245",
       "name":"505041 ~ Mali Vrh"
    },
    {
       "lat":"45.9776211",
       "lon":"14.6056207",
       "name":"505052 ~ Razdrto"
    },
    {
       "lat":"45.9773041",
       "lon":"14.6066987",
       "name":"505051 ~ Razdrto"
    },
    {
       "lat":"45.9754309",
       "lon":"14.6140896",
       "name":"505062 ~ Šmarje"
    },
    {
       "lat":"45.9757515",
       "lon":"14.6132094",
       "name":"505061 ~ Šmarje"
    },
    {
       "lat":"45.9673757",
       "lon":"14.6318952",
       "name":"505082 ~ Cikava"
    },
    {
       "lat":"45.9672171",
       "lon":"14.6324031",
       "name":"505081 ~ Cikava"
    },
    {
       "lat":"45.9734896",
       "lon":"14.6192477",
       "name":"505072 ~ Sap"
    },
    {
       "lat":"45.9730606",
       "lon":"14.6205915",
       "name":"505071 ~ Sap"
    },
    {
       "lat":"45.9590669",
       "lon":"14.6501553",
       "name":"505111 ~ Pod gozdom"
    },
    {
       "lat":"45.9609962",
       "lon":"14.6452850",
       "name":"505102 ~ Stara pošta"
    },
    {
       "lat":"45.9583937",
       "lon":"14.6510832",
       "name":"505112 ~ Pod gozdom"
    },
    {
       "lat":"45.9617355",
       "lon":"14.6442979",
       "name":"505101 ~ Stara pošta"
    },
    {
       "lat":"45.9639791",
       "lon":"14.6395150",
       "name":"505092 ~ Brvace / Brvace"
    },
    {
       "lat":"45.9639000",
       "lon":"14.6404008",
       "name":"505091 ~ Brvace / Brvace"
    },
    {
       "lat":"45.9590276",
       "lon":"14.6573249",
       "name":"505152 ~ Ljubljanska"
    },
    {
       "lat":"45.9572041",
       "lon":"14.6550127",
       "name":"505132 ~ Adamičev spomenik"
    },
    {
       "lat":"45.9626821",
       "lon":"14.6572056",
       "name":"505162 ~ OŠ Brinje"
    },
    {
       "lat":"45.9669424",
       "lon":"14.6598836",
       "name":"50507x Kongo / 506012 ~ Grosuplje Motel / Grosuplje Motel"
    },
    {
       "lat":"45.9665219",
       "lon":"14.6590450",
       "name":"506011 ~ Grosuplje Motel / Grosuplje Motel"
    },
    {
       "lat":"45.9564424",
       "lon":"14.6611130",
       "name":"505122 ~ Vodičar"
    },
    {
       "lat":"45.9525609",
       "lon":"14.6609378",
       "name":"536022 ~ Krpan"
    },
    {
       "lat":"45.9512872",
       "lon":"14.6612676",
       "name":"556011 ~ Grosuplje SM / Grosuplje SM"
    },
    {
       "lat":"45.9519230",
       "lon":"14.6559489",
       "name":"536012 ~ Mrzle njive"
    },
    {
       "lat":"45.9559889",
       "lon":"14.6529902",
       "name":"505141 ~ Grosuplje"
    },
    {
       "lat":"45.9510806",
       "lon":"14.6613036",
       "name":"556012 ~ Grosuplje SM / Grosuplje SM"
    },
    {
       "lat":"45.9397287",
       "lon":"14.6753315",
       "name":"556101 ~ V. Mlačevo"
    },
    {
       "lat":"45.9399015",
       "lon":"14.6744165",
       "name":"V. Mlačevo"
    },
    {
       "lat":"45.9396451",
       "lon":"14.6753638",
       "name":"556102 ~ V. Mlačevo"
    },
    {
       "lat":"45.9399669",
       "lon":"14.6744504",
       "name":"V. Mlačevo"
    },
    {
       "lat":"45.9350732",
       "lon":"14.6818843",
       "name":"Lobček"
    },
    {
       "lat":"45.9350126",
       "lon":"14.6817848",
       "name":"556092 ~ Lobček / Lobček"
    },
    {
       "lat":"45.9342917",
       "lon":"14.6866424",
       "name":"556082 ~ Zagradec pri Grosupljem / Zagradec pri Grosupljem"
    },
    {
       "lat":"45.9343541",
       "lon":"14.6872171",
       "name":"Zagradec pri Grosupljem"
    },
    {
       "lat":"45.9318323",
       "lon":"14.6957469",
       "name":"556072 ~ Plešivica pri Žalni"
    },
    {
       "lat":"45.9395563",
       "lon":"14.7076492",
       "name":"556041 ~ Žalna ŽP"
    },
    {
       "lat":"45.9400601",
       "lon":"14.6852529",
       "name":"556031 ~ V. Mlačevo ŽP"
    },
    {
       "lat":"45.9390637",
       "lon":"14.6989573",
       "name":"556111 ~ Žalna"
    },
    {
       "lat":"45.9321654",
       "lon":"14.6554630",
       "name":"Avtobusna postaja"
    },
    {
       "lat":"45.9746092",
       "lon":"14.6731681",
       "name":"535022 ~ M. St. vas"
    },
    {
       "lat":"45.9748836",
       "lon":"14.6732997",
       "name":"535021 ~ M. St. vas"
    },
    {
       "lat":"45.9709603",
       "lon":"14.6634924",
       "name":"535012 ~ Perovo"
    },
    {
       "lat":"45.9776518",
       "lon":"14.6771044",
       "name":"535031 ~ V. St. vas"
    },
    {
       "lat":"45.9707883",
       "lon":"14.6624421",
       "name":"535011 ~ Perovo"
    },
    {
       "lat":"45.9776950",
       "lon":"14.6777940",
       "name":"535032 ~ V. St. vas"
    },
    {
       "lat":"45.9809859",
       "lon":"14.6855056",
       "name":"535042 ~ Drobnič"
    },
    {
       "lat":"45.9810493",
       "lon":"14.6854788",
       "name":"535041 ~ Drobnič"
    },
    {
       "lat":"45.9806378",
       "lon":"14.7038853",
       "name":"535092 ~ Polica/Viš. G."
    },
    {
       "lat":"45.9780480",
       "lon":"14.6998790",
       "name":"535082 ~ Peč pri Polici"
    },
    {
       "lat":"45.9806322",
       "lon":"14.7040221",
       "name":"535091 ~ Polica/Viš. G."
    },
    {
       "lat":"45.9781272",
       "lon":"14.6998697",
       "name":"535081 ~ Peč pri Polici"
    },
    {
       "lat":"45.9929939",
       "lon":"14.7099605",
       "name":"535061 ~ Kožljevec"
    },
    {
       "lat":"45.9872077",
       "lon":"14.7075530",
       "name":"535071 ~ Žabja vas"
    },
    {
       "lat":"45.9929106",
       "lon":"14.7099049",
       "name":"535062 ~ Kožljevec"
    },
    {
       "lat":"45.9872482",
       "lon":"14.7074798",
       "name":"535072 ~ Žabja vas"
    },
    {
       "lat":"45.9938660",
       "lon":"14.6977860",
       "name":"535051 ~ Troščine"
    },
    {
       "lat":"45.9937878",
       "lon":"14.6978558",
       "name":"535052 ~ Troščine"
    },
    {
       "lat":"45.9444668",
       "lon":"14.6514566",
       "name":"546011 ~ Zadvor/Grosupljem"
    },
    {
       "lat":"45.9445470",
       "lon":"14.6514056",
       "name":"546012 ~ Zadvor/Grosupljem"
    },
    {
       "lat":"45.9423392",
       "lon":"14.6437679",
       "name":"546022 ~ Benat"
    },
    {
       "lat":"45.9391741",
       "lon":"14.6307809",
       "name":"546031 ~ Ponova vas"
    },
    {
       "lat":"45.9390451",
       "lon":"14.6306462",
       "name":"546032 ~ Ponova vas"
    },
    {
       "lat":"45.9422646",
       "lon":"14.6438510",
       "name":"546021 ~ Benat"
    },
    {
       "lat":"45.9355216",
       "lon":"14.6273491",
       "name":"546042 ~ Pece K"
    },
    {
       "lat":"45.9331318",
       "lon":"14.6248412",
       "name":"546051 ~ M. vas pri Grosupljem"
    },
    {
       "lat":"45.9327819",
       "lon":"14.6244622",
       "name":"546052 ~ M. vas pri Grosupljem"
    },
    {
       "lat":"45.9357136",
       "lon":"14.6276098",
       "name":"546041 ~ Pece K"
    },
    {
       "lat":"45.9267764",
       "lon":"14.6231726",
       "name":"546061 ~ Št. Jurij"
    },
    {
       "lat":"45.9267587",
       "lon":"14.6230520",
       "name":"546062 ~ Št. Jurij"
    },
    {
       "lat":"45.9037445",
       "lon":"14.6374819",
       "name":"546071 ~ V. Lipljene"
    },
    {
       "lat":"45.9037724",
       "lon":"14.6373637",
       "name":"546072 ~ V. Lipljene"
    },
    {
       "lat":"45.9357249",
       "lon":"14.6734364",
       "name":"M. Mlačevo"
    },
    {
       "lat":"45.9356581",
       "lon":"14.6735500",
       "name":"M. Mlačevo"
    },
    {
       "lat":"45.9122965",
       "lon":"14.6797533",
       "name":"Predole K"
    },
    {
       "lat":"45.9042340",
       "lon":"14.6896722",
       "name":"V. Račna"
    },
    {
       "lat":"45.9047076",
       "lon":"14.6886812",
       "name":"V. Račna"
    },
    {
       "lat":"45.9122483",
       "lon":"14.6796552",
       "name":"Predole K"
    },
    {
       "lat":"45.9668776",
       "lon":"14.4736992",
       "name":"615053 ~ Tomišelj K"
    },
    {
       "lat":"45.9558793",
       "lon":"14.6530330",
       "name":"505141 ~ Grosuplje"
    },
    {
       "lat":"45.9730960",
       "lon":"14.4195198",
       "name":"Podpeč/Lj."
    },
    {
       "lat":"46.0492399",
       "lon":"14.5196703",
       "name":"502024 ~ Roška"
    },
    {
       "lat":"46.0316665",
       "lon":"14.5163853",
       "name":"603152 ~ Golouhova"
    },
    {
       "lat":"46.0242012",
       "lon":"14.5322079",
       "name":"504121 ~ Bobrova"
    },
    {
       "lat":"46.0314098",
       "lon":"14.5171186",
       "name":"603151 ~ Golouhova"
    },
    {
       "lat":"45.9584222",
       "lon":"14.6602614",
       "name":"505192 ~ Dom obrtnikov"
    },
    {
       "lat":"45.9623601",
       "lon":"14.6612915",
       "name":"505182 ~ Dom starejših"
    },
    {
       "lat":"45.9402866",
       "lon":"14.5114661",
       "name":"625052 ~ Iška vas"
    },
    {
       "lat":"45.9510653",
       "lon":"14.5118518",
       "name":"625042 ~ Kot"
    },
    {
       "lat":"45.9574742",
       "lon":"14.5136670",
       "name":"625032 ~ Staje"
    },
    {
       "lat":"45.9406132",
       "lon":"14.5114559",
       "name":"625051 ~ Iška vas"
    },
    {
       "lat":"45.9444601",
       "lon":"14.5094912",
       "name":"625061 ~ Iška vas šola"
    },
    {
       "lat":"45.9438951",
       "lon":"14.5094235",
       "name":"625062 ~ Iška vas šola"
    },
    {
       "lat":"46.1244016",
       "lon":"14.4765882",
       "name":"104194 ~ Šmartno pod Šm. goro"
    },
    {
       "lat":"46.1243740",
       "lon":"14.4763613",
       "name":"104193 ~ Šmartno pod Šm. goro"
    },
    {
       "lat":"45.9368681",
       "lon":"14.5147007",
       "name":"626012 ~ Iška vas obr."
    },
    {
       "lat":"45.9309699",
       "lon":"14.5155772",
       "name":"626032 ~ Iška"
    },
    {
       "lat":"45.9159462",
       "lon":"14.5391589",
       "name":"605091 ~ Škrilje 2"
    },
    {
       "lat":"45.9159345",
       "lon":"14.5387361",
       "name":"605091 ~ Škrilje 2"
    },
    {
       "lat":"45.9717966",
       "lon":"14.4289556",
       "name":"745101 ~ OŠ Jezero"
    },
    {
       "lat":"46.0378338",
       "lon":"14.5012907",
       "name":"603064 ~ Murgle"
    },
    {
       "lat":"46.0038160",
       "lon":"14.5156345",
       "name":"604061 ~ Ižica"
    },
    {
       "lat":"46.0243900",
       "lon":"14.5318483",
       "name":"504122 ~ Bobrova"
    },
    {
       "lat":"46.0034321",
       "lon":"14.5156356",
       "name":"604062 ~ Ižica"
    },
    {
       "lat":"46.0721116",
       "lon":"14.5298052",
       "name":"203231 ~ Na Žalah"
    },
    {
       "lat":"46.0717466",
       "lon":"14.5296524",
       "name":"203231 ~ Na Žalah"
    },
    {
       "lat":"46.0337917",
       "lon":"14.4781439",
       "name":"603141 ~ Tbilisijska"
    },
    {
       "lat":"46.0339144",
       "lon":"14.4780919",
       "name":"603142 ~ Tbilisijska"
    },
    {
       "lat":"46.0577312",
       "lon":"14.5090459",
       "name":"300013 ~ AP Ljubljana (peron 2, 3, 4)"
    },
    {
       "lat":"46.0508977",
       "lon":"14.4785997",
       "name":"703182 ~ Večna pot"
    },
    {
       "lat":"46.1065596",
       "lon":"14.5942824",
       "name":"Dragomelj"
    },
    {
       "lat":"46.1063736",
       "lon":"14.5942395",
       "name":"Dragomelj"
    },
    {
       "lat":"45.9290714",
       "lon":"14.5778649",
       "name":"Smrjene"
    },
    {
       "lat":"46.1137961",
       "lon":"14.4644754",
       "name":"804132 ~ Martinova"
    },
    {
       "lat":"45.9567922",
       "lon":"14.6586231",
       "name":"OŠ L.A. Adamičeva"
    },
    {
       "lat":"45.9531700",
       "lon":"14.6593339",
       "name":"OŠ L.A. Tovarniška"
    },
    {
       "lat":"45.9551167",
       "lon":"14.6444227",
       "name":"Brezje"
    },
    {
       "lat":"45.9551028",
       "lon":"14.6443277",
       "name":"Brezje"
    },
    {
       "lat":"45.9556915",
       "lon":"14.6446755",
       "name":"Brezje II."
    },
    {
       "lat":"45.9556470",
       "lon":"14.6447532",
       "name":"Brezje II."
    },
    {
       "lat":"45.9570508",
       "lon":"14.6471888",
       "name":"Sončni dvori"
    },
    {
       "lat":"45.9571076",
       "lon":"14.6471380",
       "name":"Sončni dvori"
    },
    {
       "lat":"45.9711900",
       "lon":"14.5764660",
       "name":"Glinek Gumnišče"
    },
    {
       "lat":"45.9711778",
       "lon":"14.5765695",
       "name":"Glinek Gumnišče"
    },
    {
       "lat":"45.9659741",
       "lon":"14.5755549",
       "name":"Gumnišče Center"
    },
    {
       "lat":"45.9659150",
       "lon":"14.5755336",
       "name":"Gumnišče Center"
    },
    {
       "lat":"45.9635011",
       "lon":"14.5770998",
       "name":"Gumnišče 1"
    },
    {
       "lat":"45.9634303",
       "lon":"14.5770687",
       "name":"Gumnišče 1"
    },
    {
       "lat":"45.9602600",
       "lon":"14.5857832",
       "name":"Gornje Blato Center"
    },
    {
       "lat":"45.9602063",
       "lon":"14.5858055",
       "name":"Gornje Blato Center"
    },
    {
       "lat":"45.9582326",
       "lon":"14.5869877",
       "name":"Gornje Blato 1"
    },
    {
       "lat":"45.9582288",
       "lon":"14.5870730",
       "name":"Gornje Blato 1"
    },
    {
       "lat":"45.9532617",
       "lon":"14.5872028",
       "name":"Gornje Blato 2"
    },
    {
       "lat":"45.9532528",
       "lon":"14.5872864",
       "name":"Gornje Blato 2"
    },
    {
       "lat":"45.9482402",
       "lon":"14.5857096",
       "name":"Drenik"
    },
    {
       "lat":"45.9481642",
       "lon":"14.5856911",
       "name":"Drenik"
    },
    {
       "lat":"45.9524745",
       "lon":"14.5795094",
       "name":"Cesta na Drenik 1"
    },
    {
       "lat":"45.9524229",
       "lon":"14.5794563",
       "name":"Cesta na Drenik 1"
    },
    {
       "lat":"45.9528858",
       "lon":"14.5746549",
       "name":"Cesta na Drenik 2"
    },
    {
       "lat":"45.9528224",
       "lon":"14.5746455",
       "name":"Cesta na Drenik 2"
    },
    {
       "lat":"45.9501500",
       "lon":"14.5752105",
       "name":"Smrjene 4"
    },
    {
       "lat":"45.9502588",
       "lon":"14.5751821",
       "name":"Smrjene 4"
    },
    {
       "lat":"45.9359289",
       "lon":"14.5911212",
       "name":"Smrjene 3"
    },
    {
       "lat":"45.9359699",
       "lon":"14.5910488",
       "name":"Smrjene 3"
    },
    {
       "lat":"45.9334270",
       "lon":"14.5827188",
       "name":"Smrjene 2"
    },
    {
       "lat":"45.9334991",
       "lon":"14.5827290",
       "name":"Smrjene 2"
    },
    {
       "lat":"45.9317132",
       "lon":"14.5776133",
       "name":"Smrjene 1"
    },
    {
       "lat":"45.9316769",
       "lon":"14.5776777",
       "name":"Smrjene 1"
    },
    {
       "lat":"45.9203668",
       "lon":"14.5841657",
       "name":"Gradišče 1"
    },
    {
       "lat":"45.9202327",
       "lon":"14.5842221",
       "name":"Gradišče 1"
    },
    {
       "lat":"45.9196789",
       "lon":"14.5882899",
       "name":"Gradišče 2"
    },
    {
       "lat":"45.9196408",
       "lon":"14.5881275",
       "name":"Gradišče 2"
    },
    {
       "lat":"45.9163048",
       "lon":"14.5892373",
       "name":"Gradišče 3"
    },
    {
       "lat":"45.9162962",
       "lon":"14.5893346",
       "name":"Gradišče 3"
    },
    {
       "lat":"45.9114160",
       "lon":"14.5895235",
       "name":"Vrh nad Želimljami G.D."
    },
    {
       "lat":"45.9113755",
       "lon":"14.5895023",
       "name":"Vrh nad Želimljami G.D."
    },
    {
       "lat":"45.9083423",
       "lon":"14.5910700",
       "name":"Vrh nad Želimljami 1"
    },
    {
       "lat":"45.9083844",
       "lon":"14.5910235",
       "name":"Vrh nad Želimljami 1"
    },
    {
       "lat":"45.9050888",
       "lon":"14.5909979",
       "name":"Vrh nad Želimljami 2"
    },
    {
       "lat":"45.9050728",
       "lon":"14.5909257",
       "name":"Vrh nad Želimljami 2"
    },
    {
       "lat":"45.9386095",
       "lon":"14.6975129",
       "name":"PŠ Žalna"
    },
    {
       "lat":"45.9401181",
       "lon":"14.6853930",
       "name":"55603x ~ V. Mlačevo ŽP"
    },
    {
       "lat":"45.9489901",
       "lon":"14.6554144",
       "name":"Zadvor II."
    },
    {
       "lat":"45.9488730",
       "lon":"14.6555500",
       "name":"Zadvor II."
    },
    {
       "lat":"45.9461037",
       "lon":"14.6537833",
       "name":"Zadvor I."
    },
    {
       "lat":"45.9461452",
       "lon":"14.6537007",
       "name":"Zadvor I."
    },
    {
       "lat":"45.9547130",
       "lon":"14.6737015",
       "name":"Praproče"
    },
    {
       "lat":"45.9546539",
       "lon":"14.6736816",
       "name":"Praproče"
    },
    {
       "lat":"45.9554222",
       "lon":"14.6856328",
       "name":"Gatina"
    },
    {
       "lat":"45.9555085",
       "lon":"14.6856184",
       "name":"Gatina"
    },
    {
       "lat":"45.9679599",
       "lon":"14.6676764",
       "name":"Jerova vas"
    },
    {
       "lat":"45.9614547",
       "lon":"14.6873112",
       "name":"Spodnje Duplice"
    },
    {
       "lat":"45.9614376",
       "lon":"14.6872322",
       "name":"Spodnje Duplice"
    },
    {
       "lat":"45.9555187",
       "lon":"14.6816141",
       "name":"Spodnje Blato"
    },
    {
       "lat":"45.9554646",
       "lon":"14.6816517",
       "name":"Spodnje Blato"
    },
    {
       "lat":"46.0120926",
       "lon":"14.5727195",
       "name":"Orle"
    },
    {
       "lat":"45.9478228",
       "lon":"14.5906856",
       "name":"Drenik vas"
    },
    {
       "lat":"45.9820960",
       "lon":"14.6938701",
       "name":"535041 ~ Gradišče"
    },
    {
       "lat":"45.9820102",
       "lon":"14.6938084",
       "name":"Gradišče"
    },
    {
       "lat":"45.9523118",
       "lon":"14.5722892",
       "name":"Pijava Gorica"
    },
    {
       "lat":"45.9679445",
       "lon":"14.6678185",
       "name":"Jerova vas"
    },
    {
       "lat":"45.9600938",
       "lon":"14.5272076",
       "name":"Ig (športna dvorana)"
    },
    {
       "lat":"45.9262329",
       "lon":"14.5396057",
       "name":"Krajček"
    },
    {
       "lat":"45.9260995",
       "lon":"14.5396312",
       "name":"Krajček"
    },
    {
       "lat":"45.9092247",
       "lon":"14.5468083",
       "name":"Golec"
    },
    {
       "lat":"45.9089793",
       "lon":"14.5467176",
       "name":"Golec"
    },
    {
       "lat":"46.1251341",
       "lon":"14.5944185",
       "name":"Študa"
    },
    {
       "lat":"46.1257142",
       "lon":"14.5947271",
       "name":"Študa"
    },
    {
       "lat":"46.0769583",
       "lon":"14.4896325",
       "name":"803053 ~ Litostrojska"
    },
    {
       "lat":"46.0499530",
       "lon":"14.4973752",
       "name":"602012 ~ Cankarjev dom"
    },
    {
       "lat":"46.0761032",
       "lon":"14.5166548",
       "name":"103212 ~ Baragova"
    },
    {
       "lat":"46.0762679",
       "lon":"14.5158793",
       "name":"103211 ~ Baragova"
    },
    {
       "lat":"46.0823395",
       "lon":"14.4966555",
       "name":"103203 ~ Litostroj"
    },
    {
       "lat":"46.0822380",
       "lon":"14.4968086",
       "name":"103202 ~ Litostroj"
    },
    {
       "lat":"46.0791818",
       "lon":"14.4978806",
       "name":"103232 ~ I.C. Šiška"
    },
    {
       "lat":"46.0792880",
       "lon":"14.4980724",
       "name":"103231 ~ I.C. Šiška"
    },
    {
       "lat":"46.0676331",
       "lon":"14.4759054",
       "name":"803221 ~ Draga"
    },
    {
       "lat":"46.0680463",
       "lon":"14.4768214",
       "name":"803222 ~ Draga"
    },
    {
       "lat":"46.0529646",
       "lon":"14.4871522",
       "name":"702032 ~ Svetčeva"
    },
    {
       "lat":"46.0527099",
       "lon":"14.4877668",
       "name":"702031 ~ Svetčeva"
    },
    {
       "lat":"46.0578097",
       "lon":"14.5077323",
       "name":"300015 ~ Kolodvor (peron 29A)"
    },
    {
       "lat":"46.0469613",
       "lon":"14.4612870",
       "name":"703192 ~ Pot R. križa"
    },
    {
       "lat":"46.0505044",
       "lon":"14.4781976",
       "name":"703181~ Večna pot"
    },
    {
       "lat":"46.0534174",
       "lon":"14.4708636",
       "name":"703121 ~ Živalski vrt - ZOO"
    },
    {
       "lat":"46.0801687",
       "lon":"14.5015164",
       "name":"103052 ~ Tovarna Lek"
    },
    {
       "lat":"46.0448259",
       "lon":"14.4622304",
       "name":"703202 ~ Ježkova"
    },
    {
       "lat":"46.0451287",
       "lon":"14.4622214",
       "name":"703201 ~ Ježkova"
    },
    {
       "lat":"46.0984181",
       "lon":"14.5146814",
       "name":"104321 ~ Ježica P+R"
    },
    {
       "lat":"46.0815753",
       "lon":"14.4852405",
       "name":"803062 ~ Ljubljanske brigade"
    },
    {
       "lat":"45.9764711",
       "lon":"14.4234436",
       "name":"Podpeč 1"
    },
    {
       "lat":"45.9762704",
       "lon":"14.4230224",
       "name":"Podpeč 1"
    },
    {
       "lat":"46.1260558",
       "lon":"14.4998614",
       "name":"104362 ~ GD Gameljne"
    },
    {
       "lat":"46.1259884",
       "lon":"14.4998304",
       "name":"104361 ~ GD Gameljne"
    },
    {
       "lat":"46.1235136",
       "lon":"14.5028599",
       "name":"104352 ~ Sp. Gameljne 3"
    },
    {
       "lat":"46.1231705",
       "lon":"14.5026454",
       "name":"104351 ~ Sp. Gameljne 3"
    },
    {
       "lat":"46.1204506",
       "lon":"14.5039297",
       "name":"104342 ~ Sp. Gameljne 2"
    },
    {
       "lat":"46.1166283",
       "lon":"14.5059969",
       "name":"104331 ~ Sp. Gameljne 1"
    },
    {
       "lat":"46.1165353",
       "lon":"14.5061880",
       "name":"104332 ~ Sp. Gameljne 1"
    },
    {
       "lat":"46.1206153",
       "lon":"14.5037430",
       "name":"104341 ~ Sp. Gameljne 2"
    },
    {
       "lat":"46.0416921",
       "lon":"14.4635430",
       "name":"703211 ~ I. Kobilca"
    },
    {
       "lat":"46.0304140",
       "lon":"14.4717546",
       "name":"604032 ~ Mestni log"
    },
    {
       "lat":"46.0323833",
       "lon":"14.4641581",
       "name":"704152 ~ Cesta v Gorice"
    },
    {
       "lat":"46.0324925",
       "lon":"14.4657454",
       "name":"704152 ~ Cesta v Gorice"
    },
    {
       "lat":"46.0329223",
       "lon":"14.4621589",
       "name":"704141 ~ Zgornji log"
    },
    {
       "lat":"46.0327694",
       "lon":"14.4623268",
       "name":"704142 ~ Zgornji log"
    },
    {
       "lat":"46.1357927",
       "lon":"14.4441121",
       "name":"815141 ~ Zavrh"
    },
    {
       "lat":"46.1357889",
       "lon":"14.4442408",
       "name":"815142 ~ Zavrh"
    },
    {
       "lat":"46.0637772",
       "lon":"14.5477408",
       "name":"203242 ~ BTC-Merkur"
    },
    {
       "lat":"46.0635442",
       "lon":"14.5471354",
       "name":"203241 ~ BTC-Merkur"
    },
    {
       "lat":"46.0539095",
       "lon":"14.5043858",
       "name":"600021 ~ Ajdovščina"
    },
    {
       "lat":"46.0789460",
       "lon":"14.5253472",
       "name":"103173 ~ Center Stožice"
    },
    {
       "lat":"46.0419618",
       "lon":"14.5098383",
       "name":"602121 ~ Trnovo"
    },
    {
       "lat":"46.0374251",
       "lon":"14.5033836",
       "name":"603071 ~ Opekarska"
    },
    {
       "lat":"46.0372831",
       "lon":"14.5035609",
       "name":"603072 ~ Opekarska"
    },
    {
       "lat":"46.0269405",
       "lon":"14.4999030",
       "name":"603181 ~ Barje P+R"
    },
    {
       "lat":"46.0422578",
       "lon":"14.4587050",
       "name":"703222 ~ C. na Vrhovce"
    },
    {
       "lat":"46.0421945",
       "lon":"14.4586523",
       "name":"703221 ~ C. na Vrhovce"
    },
    {
       "lat":"46.1339346",
       "lon":"14.6954800",
       "name":"Selo pri Moravčah"
    },
    {
       "lat":"46.0376637",
       "lon":"14.6079384",
       "name":"404083 ~ Križišče Sostro"
    },
    {
       "lat":"46.0375957",
       "lon":"14.6078744",
       "name":"404084 ~ Križišče Sostro"
    },
    {
       "lat":"46.0706834",
       "lon":"14.5258251",
       "name":"203212 ~ Kranjčeva"
    },
    {
       "lat":"46.0745650",
       "lon":"14.5245370",
       "name":"103181 ~ Štajerska"
    },
    {
       "lat":"46.0680926",
       "lon":"14.5165004",
       "name":"102032 ~ Gasilska brigada"
    },
    {
       "lat":"46.0703962",
       "lon":"14.5490315",
       "name":"203161 ~ Nove Jarše - Šmartinska"
    },
    {
       "lat":"46.0458302",
       "lon":"14.5441134",
       "name":"403072 ~ Bilečanska"
    },
    {
       "lat":"46.0834662",
       "lon":"14.4584224",
       "name":"804382 ~ Zapuže"
    },
    {
       "lat":"46.0832617",
       "lon":"14.4589585",
       "name":"804381 ~ Zapuže"
    },
    {
       "lat":"46.0830169",
       "lon":"14.4536666",
       "name":"804392 ~ Kamnogoriška"
    },
    {
       "lat":"46.0828885",
       "lon":"14.4534937",
       "name":"804391 ~ Kamnogoriška"
    },
    {
       "lat":"46.0817901",
       "lon":"14.4505070",
       "name":"804401 ~ Dolnice"
    },
    {
       "lat":"46.0820603",
       "lon":"14.4501119",
       "name":"804401 ~ Dolnice"
    },
    {
       "lat":"46.0785139",
       "lon":"14.4554024",
       "name":"804411 ~ Krivec"
    },
    {
       "lat":"46.0780326",
       "lon":"14.4554500",
       "name":"804412 ~ Krivec"
    },
    {
       "lat":"46.0742077",
       "lon":"14.4556427",
       "name":"804193 ~ Pod Kamno Gorico"
    },
    {
       "lat":"46.0478894",
       "lon":"14.5132417",
       "name":"Streliška ulica 14"
    },
    {
       "lat":"46.0769149",
       "lon":"14.5754996",
       "name":"204252 ~ Trbeže"
    },
    {
       "lat":"46.0771414",
       "lon":"14.5754607",
       "name":"204251 ~ Trbeže"
    },
    {
       "lat":"46.0611463",
       "lon":"14.5132321",
       "name":"201012 ~ Železna"
    },
    {
       "lat":"46.0514021",
       "lon":"14.5003475",
       "name":"Parlament"
    },
    {
       "lat":"46.0530959",
       "lon":"14.5000025",
       "name":"Opera"
    },
    {
       "lat":"46.0501129",
       "lon":"14.5069768",
       "name":"Mestna hiša"
    },
    {
       "lat":"46.0484716",
       "lon":"14.5095344",
       "name":"Ljubljanski grad"
    },
    {
       "lat":"46.0403789",
       "lon":"14.5119324",
       "name":"Špica"
    },
    {
       "lat":"46.0414734",
       "lon":"14.5088186",
       "name":"Trnovski pristan"
    },
    {
       "lat":"46.0435844",
       "lon":"14.5026842",
       "name":"Plečnikova hiša"
    },
    {
       "lat":"45.9574502",
       "lon":"14.6559588",
       "name":"Knjižnica"
    },
    {
       "lat":"45.9592737",
       "lon":"14.6533371",
       "name":"Zdravstveni dom"
    },
    {
       "lat":"46.1346624",
       "lon":"14.4116375",
       "name":"815151 ~ Preska / Preska pri Medvodah"
    },
    {
       "lat":"46.1397841",
       "lon":"14.3994948",
       "name":"815191 ~ Goričane pod gradom"
    },
    {
       "lat":"46.0668832",
       "lon":"14.5421353",
       "name":"Kristalna palača"
    },
    {
       "lat":"46.0603241",
       "lon":"14.5396870",
       "name":"203062 ~ Tovorni kolodvor"
    },
    {
       "lat":"46.0225925",
       "lon":"14.4060075",
       "name":"704091 ~ Lukovica pri Brezovici"
    },
    {
       "lat":"46.0578605",
       "lon":"14.5088652",
       "name":"Flixbus (peron 29, 30)"
    }
 ];
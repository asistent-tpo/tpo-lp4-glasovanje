module.exports = (sequelize, type) => {
    return sequelize.define('schedule', {
        id : {
            type : type.INTEGER,
            autoIncrement : true,
            primaryKey  : true
        },
        userId : {
            type : type.INTEGER,
            references : {
                model : sequelize.models.user,
                key : 'id',
                deferrable: type.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        startTime : {
            type : type.TIME,
            allowNull : false
        },
        endTime : {
            type : type.TIME,
            allowNull : false
        },
        name : {
            type : type.STRING,
            allowNull : false
        },
        color : {
            type : type.STRING,
            allowNull : false
        },
        dayOfWeek : {
            type : type.INTEGER,
            allowNull : false
        }
    });
};
module.exports = (sequelize, type) => {
    return sequelize.define('user', {
        id : {
            type : type.INTEGER,
            autoIncrement : true,
            primaryKey  : true
        },
        email : {
            type : type.STRING,
            allowNull: false,
            validate : {
                isEmail : true
            },
            unique : true
        },
        passwordHash : {
            type : type.STRING,
            allowNull : false
        },
        salt : {
            type : type.STRING,
            allowNull : false
        },
        permissionLevel : {
            type : type.INTEGER,
            defaultValue : 0,
            allowNull : false
        },
        active : {
            type : type.BOOLEAN,
            defaultValue: false
        }
    });
};
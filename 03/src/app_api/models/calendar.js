module.exports = (sequelize, type) => {
    return sequelize.define('calendar', {
        id : {
            type : type.INTEGER,
            autoIncrement : true,
            primaryKey  : true
        },
        userId : {
            type : type.INTEGER,
            references : {
                model : sequelize.models.user,
                key : 'id',
                deferrable: type.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        startDate : {
            type : type.DATEONLY,
            allowNull : false
        },
        endDate : {
            type : type.DATEONLY,
            allowNull : false
        },
        title : {
            type : type.STRING,
            allowNull : false
        },
        description : {
            type : type.TEXT
        }
    });
};
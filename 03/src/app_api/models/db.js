const Sequelize = require('sequelize');

const UserModel = require('./user');
const CalendarModel = require('./calendar');
const ScheduleModel = require('./schedule');
const ToDoModel = require('./todo');
const EventModel = require('./event');
const SystemNotificationModel = require('./system_notification');
const ActivationModel = require('./activation');


//CREATE DATABASE straightas LC_COLLATE = 'sl_SI.UTF-8' TEMPLATE = template0;
var dbURI = 'postgres://postgres@localhost/straightas';
if(process.env.NODE_ENV === 'production') {
    dbURI = process.env.DATABASE_URL;
}

const sequelize = new Sequelize(dbURI, {
    logging : false,
    define: {
        charset: 'utf8',
        dialectOptions: {
            collate: 'utf8_general_ci'
        },
        timestamps: false
    }
});


const User = UserModel(sequelize, Sequelize);
const Activation = ActivationModel(sequelize, Sequelize);
const Calendar = CalendarModel(sequelize, Sequelize);
const Schedule = ScheduleModel(sequelize, Sequelize);
const ToDo = ToDoModel(sequelize, Sequelize);
const Event = EventModel(sequelize, Sequelize);
const SystemNotification = SystemNotificationModel(sequelize, Sequelize);


sequelize.authenticate().then(() => {
    console.log('Connected to database');

    return sequelize.sync().then(() => {console.log("Initial sync succeeded!")});

}).catch(err => {
    console.error('Error connecting to DB: ', err);
});

var properShutdown = function(message, callback) {
    sequelize.close().then(() => {
        console.log('PSQL connection closed via ' + message);
        callback();
    });
};

// Pri ponovnem zagonu nodemon
process.once('SIGUSR2', function() {
    properShutdown('nodemon restart', function() {
        process.kill(process.pid, 'SIGUSR2');
    });
});

// Pri izhodu iz aplikacije
process.on('SIGINT', function() {
    properShutdown('application exit', function() {
        process.exit(0);
    });
});

// Pri izhodu iz aplikacije na Heroku
process.on('SIGTERM', function() {
    properShutdown('application exit Heroku', function() {
        process.exit(0);
    });
});

module.exports.User = User;
module.exports.Calendar = Calendar;
module.exports.Schedule = Schedule;
module.exports.ToDo = ToDo;
module.exports.Event = Event;
module.exports.SystemNotification = SystemNotification;
module.exports.Activation = Activation;
module.exports.sequelize = sequelize;


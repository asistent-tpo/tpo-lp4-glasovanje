const User = require("../models/db").User;

var levels = {
    ADMINISTRATOR: 100,
    EVENT_EDITOR: 50,
    REGISTERED_USER: 0,
};

let getPermissionLevel = function(id, min, res, next) {

    User.findByPk(id).then(user => {
        if(user.permissionLevel >= min){
            next();
        }else{
            sayInsufficient(res);
            return;
        }
    });

};

let sayInsufficient = function(res){

    res.status(401);
    res.json({
        "error": "Insufficient permissions"
    });

};



module.exports = {
    
    ...levels,
    
    atLeastEditor: function(req, res, next){

        //console.log("CHECKING AUTHORIZATION!");

        getPermissionLevel(req.payload.id, levels.EVENT_EDITOR, res, next);
       
    },

    atLeastAdministrator: function(req, res, next){

        //console.log("CHECKING AUTHORIZATION! ADMIN");

        getPermissionLevel(req.payload.id, levels.ADMINISTRATOR, res, next);

    }
}


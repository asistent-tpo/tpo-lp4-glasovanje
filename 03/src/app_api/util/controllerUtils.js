var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};


var checkColsPresent = function(obj, arr) {

    arr.forEach(prop => {
        if(obj[prop] == null){
            returnJson(res, 400, {error: "No "+prop+" parameter received"}); 
            return true;
        }
    });

    return false;
}

var copyAllowedFields = function(data, allowed){

    var obj = {};

    allowed.forEach(function(key) {
        if(data.hasOwnProperty(key)){
            obj[key] = data[key];
        }
    });

    return obj;
}


module.exports.returnJson = returnJson;
module.exports.checkColsPresent = checkColsPresent;
module.exports.copyAllowedFields = copyAllowedFields;
const restavracije = require('../models/restavracije');
const sortByDistance = require('sort-by-distance');


var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

/*

    How to get the data:

    - Open a Web browser
    - Goto www.studentska-prehrana.si/sl/Restaurants
    - Press F12
    - paste this snippet to console and run


    let mm = []
    let k = function(el){
	    let arr = ["lat", "lon", "naslov", "cena", "doplacilo", "posid", "detailslink", "lokal", "city"];
	
	    var obj = {};
	    arr.forEach(function(key) {
		    obj[key] = el.getAttribute("data-"+key);
	    });
	    mm.push(obj);
    }
    var lines = document.querySelectorAll(".restaurant-row").forEach(k);
    document.write(JSON.stringify(mm));


    - the site will turn all covered in JSON
    - copy the JSON and paste it in ../models/restavracije.js

*/

module.exports.getRestaurantsSortAZ = function(req, res){

    returnJson(res, 200, restavracije);

}

module.exports.getRestaurantsSortNear = function(req, res){

    let origin = {
        lat: req.params.lat,
        lon: req.params.lon
    };

    const opts = {
        yName: 'lat',
        xName: 'lon'
    }
     
    returnJson(res, 200, sortByDistance(origin, restavracije, opts));

}
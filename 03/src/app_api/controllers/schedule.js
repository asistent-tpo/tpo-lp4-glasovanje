const Schedule = require('../models/db').Schedule;

var checkColsPresent = function(obj, arr) {

    arr.forEach(prop => {
        if(obj[prop] == null){
            returnJson(res, 400, {error: "No "+prop+" parameter received"}); 
            
            return true;
        }
    });

    return false;
}

var copyAllowedFields = function(data, allowed){

    var obj = {};

    allowed.forEach(function(key) {
        if(data.hasOwnProperty(key)){
            obj[key] = data[key];
        }
    });

    return obj;
}

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

let allowedFields = ["startTime", "endTime", "name", "color", "dayOfWeek"];
let mandatoryFields = ["startTime", "endTime", "name", "dayOfWeek"];

module.exports.createUserScheduleEntry = function(req, res){

    /*
    JSON request:
    {
        "startTime": DateTime
        "endTime": DateTime
        "name": string
        "color": string
    }
    */

    if(req.body == null){ returnJson(res, 400, {error: "Invalid or no JSON received"}); return; }
    if(checkColsPresent(req.body, mandatoryFields)){ return; }

    let userId = req.payload.id;


    Schedule.create({
        userId: userId,
        ...copyAllowedFields(req.body, allowedFields)
    }).then(newEntry => {
        returnJson(res, 200, newEntry);
    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot create the schedule entry.", database: error.message});             
    });    

};


module.exports.getUserScheduleEntry = function(req, res){

    let userId = req.payload.id;

    Schedule.findAll({
        where:
        {
            userId: userId,
        }
    }).then(entries => {
        returnJson(res, 200, entries);

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot get user schedule entries.", database: error.message});        
    }); 

};


module.exports.deleteUserScheduleEntry = function(req, res){

    let id = req.params.id;
    let userId = req.payload.id;

    Schedule.destroy({
        where:
        {
            id: id,
            userId: userId 
        }
    }).then(status => {
        
        returnJson(res, 200, {
            deleted: status > 0,
        });

    }).catch(function(error) {
        
        returnJson(res, 500, {error: "Error deleting schedule entry.", database: error.message});        

    }); 

};


module.exports.editUserScheduleEntry = function(req, res){

    // TODO Body null checks
    if(req.params == null){ returnJson(res, 400, {error: "No params received"}); return; }
    if(checkColsPresent(req.params, ["id"])){ return; }

    let id = parseInt(req.params.id);
    let userId = req.payload.id;

    Schedule.update(copyAllowedFields(req.body, allowedFields), 
    {
        where: {
            id: id,
            userId: userId
        }

    }).then(numChanged => {

        if(numChanged == 0){
            util.returnJson(res, 402, {error: "You do not have permission to update this entry!"});
            return;
        }

        Schedule.findByPk(id).then(item => {
            returnJson(res, 200, {changed: numChanged > 0, ...item.dataValues});
        })

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot update schedule entry.", database: error.message});             
    });
};
const TODO = require('../models/db').ToDo;

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};


module.exports.createUserTodo = function(req, res){


    /*
    JSON request:
    
    {
        "text": "To je tekst od TODO-ja"
    }
    */
    
    // TODO Body null checks
    if(req.body == null){ returnJson(res, 400, {error: "Invalid or no JSON received"}); return; }
    if(req.body.text == null){ returnJson(res, 400, {error: "No text parameter received"}); return; }

    // TODO add login check
    let userId = req.payload.id;

        
    TODO.create({
        userId: userId,
        name: req.body.text
    }).then(newTodo => {
        returnJson(res, 200, newTodo);
    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot create the todo item.", database: error.message});             
    });    

};


module.exports.getUserTodos = function(req, res){

    let userId = req.payload.id;

        
    TODO.findAll({
        where:
        {
            userId: userId,
        }
    }).then(todos => {
        returnJson(res, 200, todos);

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot get the todo item.", database: error.message});        
    }); 

};


module.exports.deleteUserTodo = function(req, res){

    let id = req.params.id;
    let userId = req.payload.id;

    TODO.destroy({
        where:
        {
            id: id,
            userId: userId 
        }
    }).then(status => {
        
        returnJson(res, 200, {
            deleted: status > 0,
        });

    }).catch(function(error) {
        
        returnJson(res, 500, {error: "Error deleting todo item.", database: error.message});        

    }); 

};


module.exports.editUserTodo = function(req, res){
    // TODO Body null checks
    if(req.params == null){ returnJson(res, 400, {error: "No params received"}); return; }
    if(req.params.id == null){ returnJson(res, 400, {error: "No id parameter received"}); return; }


    let id = parseInt(req.params.id);
    
    // Todo add user logic
    let userId = req.payload.id;
    

    TODO.update({
        name: req.body.text,
      }, {
        where: {
          id: id,
          userId: userId
        }
      }).then(numChanged => {

        if(numChanged == 0){
            util.returnJson(res, 402, {error: "You do not have permission to update this entry!"});
            return;
        }

        TODO.findByPk(id).then(item => {
            returnJson(res, 200, {changed: numChanged > 0, ...item.dataValues});
        })

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot update the todo item.", database: error.message});             
    });
};
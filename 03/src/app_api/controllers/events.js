const Event = require('../models/db').Event;

var checkColsPresent = function(obj, arr) {

    arr.forEach(prop => {
        if(obj[prop] == null){
            returnJson(res, 400, {error: "No "+prop+" parameter received"}); 
            
            return true;
        }
    });

    return false;
}

var copyAllowedFields = function(data, allowed){

    var obj = {};

    allowed.forEach(function(key) {
        if(data.hasOwnProperty(key)){
            obj[key] = data[key];
        }
    });

    return obj;
}

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

let allowedFields = ["date", "name", "organizer", "description"];
let mandatoryFields = ["date", "name"];

module.exports.createEvent = function(req, res){

    /*
    JSON request:
    {
        "date": DATE
        "name": STRING
        "organizer": STRING
        "description": STRING
    }
    */

    if(req.body == null){returnJson(res, 400, {error: "Invalid or no JSON received"}); return; }
    if(checkColsPresent(req.body, mandatoryFields)){ return; }

    // @ TODO Check USER roles

    Event.create({
        ...copyAllowedFields(req.body, allowedFields)
    }).then(newEntry => {
        returnJson(res, 200, newEntry);
    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot create the event.", database: error.message});             
    });    

};


module.exports.getAllEvents = function(req, res){

    Event.findAll({ where:{} })
    .then(entries => {
        returnJson(res, 200, entries);
    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot get all events.", database: error.message});        
    }); 

};

module.exports.getEvent = function(req, res){

    let id = req.params.id;

    Event.findByPk(id)
    .then(entries => {
        returnJson(res, 200, entries);
    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot get this event.", database: error.message});        
    }); 

};


module.exports.deleteEvent = function(req, res){

    if(req.params == null){ returnJson(res, 400, {error: "No params received"}); return; }
    if(checkColsPresent(req.params, ["id"])){ return; }

    let id = req.params.id;

    Event.destroy({
        where: { id: id }
    }).then(status => {
        
        returnJson(res, 200, {
            deleted: status > 0,
        });

    }).catch(function(error) {
        
        returnJson(res, 500, {error: "Error deleting the event.", database: error.message});        

    }); 

};


module.exports.editEvent = function(req, res){

    // TODO Body null checks
    if(req.params == null){ returnJson(res, 400, {error: "No params received"}); return; }
    if(checkColsPresent(req.params, ["id"])){ return; }

    let id = parseInt(req.params.id);

    Event.update(copyAllowedFields(req.body, allowedFields), 
    { 
        where: { 
            id: id 
        }
    }).then(numChanged => {

        if(numChanged == 0){
            util.returnJson(res, 402, {error: "You do not have permission to update this entry!"});
            return;
        }

        Event.findByPk(id).then(item => {
            returnJson(res, 200, {changed: true, ...item.dataValues});
        })

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot update the event.", database: error.message});             
    });
};
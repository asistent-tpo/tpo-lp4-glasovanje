const SystemNotification = require('../models/db').SystemNotification;

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};


module.exports.createNotification = function(req, res){
    /*
    JSON request:

    {
        "notification": "System notification"
    }
    */

    if(!req.body){ returnJson(res, 400, {error: "Invalid or no JSON received"}); return; }
    if(!req.body.notification){ returnJson(res, 400, {error: "No notification parameter received"}); return; }



    SystemNotification.create({
        notification : req.body.notification,
        active : true
    }).then(notification => {
        returnJson(res, 201, notification);
    }).catch((error) => {
        returnJson(res, 500, {error: "Cannot create notification.", database: error.message});
    });

};


module.exports.getNotifications = function(req, res){
    let all = false;
    if(req.query && req.query.all) {
        all = (req.query.all === 'true');
    }

    let options = { where : {active : true}};
    if(all) options = {};

    SystemNotification.findAll(options).then(notifications => {
        returnJson(res, 200, notifications);

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot get notifications.", database: error.message});
    });

};


module.exports.deleteNotification = function(req, res){
    if(!req.params || !req.params.id) {
        returnJson(res, 400, {error: "No id received"}); return;
    }

    SystemNotification.destroy({
        where:
            {
                id: req.params.id
            }
    }).then(status => {
        returnJson(res, 200, {
            deleted: status > 0,
        });

    }).catch(function(error) {

        returnJson(res, 500, {error: "Error deleting todo item.", database: error.message});

    });

};


module.exports.updateNotification = function(req, res){
    if(!req.params || !req.params.id) {
        returnJson(res, 400, {error: "No id received"}); return;
    }

    if(!req.body || !req.body.notification || req.body.active == null) {
        returnJson(res, 400, {error: "Invalid/no JSON received or missing fields"}); return;
    }



    let id = parseInt(req.params.id);


    SystemNotification.update({
        notification: req.body.notification,
        active: req.body.active
    }, {
        where: {
            id: id
        }
    }).then(numChanged => {

        if(numChanged == 0){
            returnJson(res, 402, {error: "No entries updated"});
            return;
        }

        SystemNotification.findByPk(id).then(item => {
            returnJson(res, 200, item);
        })

    }).catch(function(error) {
        returnJson(res, 500, {error: "Cannot update notification.", database: error.message});
    });
};
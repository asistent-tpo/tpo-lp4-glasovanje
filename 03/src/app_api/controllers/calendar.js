const Calendar = require('../models/db').Calendar;
const util = require("../util/controllerUtils");
const moment = require("moment");

let allowedFields = ["startDate", "endDate", "title", "description"];
let mandatoryFields = ["startDate", "endDate", "title"];


let verifyBodyContent = function(req, res){

    let start = moment(req.body.startDate);
    let end   = moment(req.body.startDate);
    let today = moment();

    if(
        // Če je začetni datum več kot leto v preteklosti
        start.isSameOrBefore(today.subtract(1, 'year'))
        // Če je začetni datum več kot 5 let v prihodnosti
        || start.isSameOrAfter(today.add(5, 'years'))
        // Če je končni datum pred začetnim datumom
        || end.isBefore(start)
        // Če je končni datum več kot 5 let v prihodnosti
        || end.isSameOrAfter(today.add(5, 'year'))
    )
    {
        return false;
    }

}

module.exports.createUserCalendarEntry = function(req, res){

    /*
    JSON request:
    
    {
        "startDate": DateTime
        "endDate": DateTime
        "title": string
        "description": string
    }
    */

    if(req.body == null){ util.returnJson(res, 400, {error: "Invalid or no JSON received"}); return; }
    if(util.checkColsPresent(req.body, mandatoryFields)) { return; }

    let userId = req.payload.id;

    if(!verifyBodyContent){
        util.returnJson(res, 400, {error: "Start date and end date are invalid or out of acceptable ranges."}); return;
    }

    Calendar.create({
        userId: userId,
        ...util.copyAllowedFields(req.body, allowedFields)
    }).then(newEntry => {
        util.returnJson(res, 200, newEntry);
    }).catch(function(error) {
        util.returnJson(res, 500, {error: "Cannot create the entry.", database: error.message});             
    });    

};


module.exports.getUserCalendarEntry = function(req, res){

    let userId = req.payload.id;

    Calendar.findAll({
        where:
        {
            userId: userId,
        }
    }).then(entries => {
        util.returnJson(res, 200, entries);

    }).catch(function(error) {
        util.returnJson(res, 500, {error: "Cannot get the entries.", database: error.message});        
    }); 

};


module.exports.deleteUserCalendarEntry = function(req, res){

    let id = req.params.id;
    let userId = req.payload.id;

    Calendar.destroy({
        where:
        {
            id: id,
            userId: userId 
        }
    }).then(status => {
        
        util.returnJson(res, 200, {
            deleted: status > 0,
        });

    }).catch(function(error) {
        
        util.returnJson(res, 500, {error: "Error deleting calendar entry.", database: error.message});        

    }); 

};

module.exports.editUserCalendarEntry = function(req, res){

    if(req.params == null){ returnJson(res, 400, {error: "No params received"}); return; }
    if(req.params.id == null){ returnJson(res, 400, {error: "No id parameter received"}); return; }

    let id = req.params.id;
    let userId = req.payload.id;

    if(!verifyBodyContent){
        util.returnJson(res, 400, {error: "Start date and end date are invalid or out of acceptable ranges."}); return;
    }

    Calendar.update({
        ...util.copyAllowedFields(req.body, allowedFields)    
    }, {
        where: {
          id: id,
          userId: userId
        }
      }).then(numChanged => {

        if(numChanged == 0){
            util.returnJson(res, 402, {error: "You do not have permission to update this entry!"});
            return;
        }

        Calendar.findByPk(id).then(item => {
            util.returnJson(res, 200, {changed: numChanged > 0, ...item.dataValues});
        })

    }).catch(function(error) {
        util.returnJson(res, 500, {error: "Cannot update this calendar entry.", database: error.message});             
    });
};
const passport = require('passport');
const User = require('../models/db').User;
const Activation = require('../models/db').Activation;
const sequelize = require('../models/db').sequelize;
const nodemailer = require('nodemailer');
const crypto = require('crypto');

let returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.register = function (req, res) {
    let data = req.body;
    if(!data.email || !data.password) {
        returnJson(res, 400, {msg : "Missing field email or password"});
        return;
    }
    if(data.password.length < 8) {
        returnJson(res, 400, {msg : "Password too short"});
        return;
    }

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.GMAIL_USER,
            pass: process.env.GMAIL_PASSWORD
        }
    });

    let mailOptions = {
        from: process.env.GMAIL_USER,
        to: data.email,
        subject: 'Activation link for StraightAs'
    };

    let user = new User();
    user.email = data.email;
    user.setPassword(data.password);

    let activation = new Activation();

    let validDate = new Date();
    validDate.setDate(validDate.getDate() + 2);
    activation.valid = validDate;

    activation.hash = crypto.randomBytes(32).toString('hex');
    mailOptions.text = process.env.DOMAIN + "/activation/" + activation.hash;

    sequelize.transaction(t => {
       return user.save({transaction : t}).then(cUser => {
           activation.userId = cUser.id;
           return activation.save({transaction : t}).then(a => {

               transporter.sendMail(mailOptions, function (err, info) {
                   if(err) {
                       throw new Error();
                       returnJson(res, 500, {msg : "Could not send email"});
                   }
               });
           });
       }).catch(() => {
           throw new Error();
       });
    }).then(() => {
       returnJson(res, 200, {msg : "User added"});
    }).catch(err => {
        returnJson(res, 400, {msg : "User exists"});
    });
};

module.exports.login = function (req, res) {
    let data = req.body;
    if(!data.email || !data.password) {
        returnJson(res, 400, {msg : "Missing field email or password"});
        return;
    }

    passport.authenticate('local', function (err, user, data) {
        if(err) {
            returnJson(res, 404, err);
        }

        if(user) {
            returnJson(res, 200, {
                authToken: user.generateToken()
            });
        } else {
            returnJson(res, 401, data);
        }
    })(req, res);
};

module.exports.activate = function (req, res) {
    if(!req.params || !req.params.hash) {
        returnJson(res, 400, {msg : "Missing hash"});
        return;
    }

    sequelize.transaction(t => {
        return Activation.findOne({ where: {hash: req.params.hash} }, {transaction : t}).then(act => {
            if(act.valid >= (new Date())) {
                return User.findOne({where : {id : act.userId}}, {transaction : t}).then(u => {
                   u.active = true;
                   return u.save({transaction : t}).then(user => {
                      return act.destroy({transaction : t}).then(() => {
                          return user;
                      });
                   });
                });
            } else {
                throw new Error();
            }
        });
    }).then(user => {
        returnJson(res, 200, {authToken: user.generateToken()});
    }).catch(err => {
       returnJson(res, 400, {msg : "Invalid activation"});
    });
};

module.exports.changePassword = function (req, res) {
    if(!req.payload || !req.payload.id) {
        returnJson(res, 401, {msg : "Invalid payload"});
        return;
    }
    if(!req.body || !req.body.password) {
        returnJson(res, 400, {msg : "Missing password field"});
        return;
    }


    sequelize.transaction(t => {
       return User.findByPk(req.payload.id, {transaction : t}).then(user => {
          user.setPassword(req.body.password);
          return user.save({transaction : t});
       });
    }).then(() => {
        returnJson(res, 200, {msg : "Password changed"});
    }).catch(err => {
        returnJson(res, 500, {msg : "Could not set password"});
    });
};
const request = require("superagent")
const util = require("../util/controllerUtils");
const unidecode = require("unidecode");

module.exports.getArrivalsForStation = function(req, res){

    if(!req.params.station){
        
        util.returnJson(res, 400, {error: "Station parameter is missing!"});
        return;

    }

    let postaja = unidecode(req.params.station);

    request
    .get('https://www.trola.si/'+postaja)
    .set('Accept', 'application/json')
    .then(data => {

        let stations = data.body.stations;
        util.returnJson(res, 200, fillStationCoords(stations));
        
    }).catch(err => {

        util.returnJson(res, 500, {error: "Could not get the arrivals", api: err.message});

    });

}

function fillStationCoords(stations){

    stations.forEach(s => {
        let va = busCoordsFilter(s.number);
        if(va == null) return;
        if(va.length == 0) return;

        let kk = va[0];
        if(kk){
            s.lat = kk.lat;
            s.lon = kk.lon;
        }
    });

    return stations;

}

module.exports.getAllStations = function(req, res){

    const allStations = require("../models/allBusStationsObj");
    util.returnJson(res, 200, allStations);

}

module.exports.getCoordsFile = function(req, res){

    /*
        let jj = [];
        document.querySelectorAll("node").forEach(function(node){

            var obj = {
                lat: node.getAttribute("lat"),
                lon: node.getAttribute("lon")
            };
            
            var nameTag = node.querySelector("tag[k=name]");
            if(!nameTag) return;
            
            obj.name = nameTag.getAttribute("v");

            jj.push(obj);
            
        });
        document.write(JSON.stringify(jj));
    */

    const coords = require("../models/busStationsCoords");
    util.returnJson(res, 200, coords);

}


function busCoordsFilter(f){
    
    const coords = require("../models/busStationsCoords");
    let filter = unidecode(f);
    let results = [];

    coords.forEach(c => {
        let name = unidecode(c.name);
        if(name.match(new RegExp(filter, "i"))){
            results.push(c);
        }
    });

    return results;
}


module.exports.getCoordsFilter = function(req, res){

    let filter = unidecode(req.params.name);
    util.returnJson(res, 200, busCoordsFilter(filter));

}

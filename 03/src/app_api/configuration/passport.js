const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/db').User;
const crypto = require('crypto');
const jwt = require('jsonwebtoken');


User.prototype.checkPassword = function(password) {
    let hash = crypto.pbkdf2Sync(password.toString(), this.salt, 1000, 64, 'sha512').toString('hex');
    return this.passwordHash == hash;
};

User.prototype.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.passwordHash = crypto.pbkdf2Sync(password.toString(), this.salt, 1000, 64, 'sha512').toString('hex');
}

User.prototype.generateToken = function() {
    let expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 7);

    return jwt.sign({
        id: this.id,
        email: this.email,
        permissionLevel: this.permissionLevel,
        expireDate: parseInt(expireDate.getTime() / 1000, 10)
    }, process.env.JWT_PASSWORD);
}

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(username, password, finish) {
        User.findOne({ where: {email: username} }).then( user => {
            if (!user) {
                return finish(null, false, {msg: "Invalid email or password"});
            }
            if (!user.checkPassword(password)) {
                return finish(null, false, {msg: "Invalid email or password"});
            }
            if (!user.active) {
                return finish(null, false, {msg : "Email not activated"});
            }
            return finish(null, user);
        }).catch(error => {
            return finish(error);
        });
    }
));
// git push https://heroku:26c9e959-542e-44e9-996f-a0bd548c4bdf@git.heroku.com/tpo-lp4-straight-as.git master --force
// git push https://heroku:26c9e959-542e-44e9-996f-a0bd548c4bdf@git.heroku.com/tpo-lp4-straight-as.git production:master

const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const nodemailer = require("nodemailer");
const fs = require('fs');

var port = process.env.PORT || 3000;
var app = express();

var MongoClient = require('mongodb').MongoClient;
var databaseName = "heroku_mx8kl03h";
var url = "mongodb://admin:M43FieldCap@ds155606.mlab.com:55606/heroku_mx8kl03h";

/* LOCAL DATABASE
var databaseName = "mydb";
var url = "mongodb://localhost:27017/";
*/

var transporter = nodemailer.createTransport({
	service: 'gmail',
	port: 587,
    secure: false,
    requireTLS: true,
	auth: {
		user: 'jnpelicon@gmail.com',
		pass: 'pelicon1'
	}
});

var mailOptions = {
  from: 'jnpelicon@gmail.com',
  to: 'default',
  subject: 'default',
  text: 'default'
};

app.use(bodyParser.json());
app.use(cookieParser());

app.use(bodyParser.urlencoded({ 
	extended: true 
}));

app.use(session({
	key: 'user_sid', 
	secret: 'safesecret',
	resave: false,
	saveUninitialized: false,
	cookie: {
		maxAge: 6000000 // 1 hour
	}
}));

app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});

var editorCheck = (req, res, next) => {
    if (req.session.usertype == 2 && req.cookies.user_sid) {
        next();
	} else {
        res.redirect('/home');
    }    
};

var adminCheck = (req, res, next) => {
    if (req.session.usertype == 1 && req.cookies.user_sid) {
        next();
	} else {
        res.redirect('/home');
    }    
};

var sessionCheck = (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect('/home');
	} else {
        next();
    }    
};

var userCheck = (req, res, next) => {
    if (req.session.usertype == 0 && req.cookies.user_sid) {
        next();
    }else{
        res.redirect('/');
	}		
};

///////////////////////////////////////////////////
// LOGIN, REGISTRATION, CHANGE PASSWORD, SESSION //
///////////////////////////////////////////////////

app.get('/', sessionCheck, function(req, res) {
	res.redirect('/login');
});

app.route('/login')
	.get(sessionCheck, (req, res) => {
		res.sendFile(__dirname + '/html/prijava.html');
	})
	.post((req, res) => {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			if (err) throw err;
			var dbo = db.db(databaseName);
			var query = {email: req.body.email, password: req.body.password};

			dbo.collection("users").find(query).toArray(function(err, result) {
				if (err) throw err;
				if(result.length > 0){
					if( result[0].active ){
						if( result[0].type == 0 ){
							req.session.user = query;
							req.session.usertype = 0;
							console.log("LOGIN: Success login");
							console.log("USER: ", req.session.user);						
							res.send({type:0});										
						}else if( result[0].type == 1 ){
							req.session.user = query;
							req.session.usertype = 1;
							console.log("LOGIN: Admin login");	
							res.send({type:1});	
						}else if( result[0].type == 2 ){
							req.session.user = query;
							req.session.usertype = 2;	
							console.log("LOGIN: Editor login");						
							res.send({type:2});	
						}					
					}else{
						console.log("LOGIN: Account not verified");	
						res.send({type:3});							
					}
				}else{
					console.log("LOGIN: Failed login");	
					res.send({type:4});				
				}
				db.close();
			});
		});
	});

app.route('/registration')
	.get(sessionCheck, (req, res) => {
		res.sendFile(__dirname + '/html/registracija.html');
	})
	.post((req, res) => {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			if (err) throw err;
			var dbo = db.db(databaseName);
			var date = new Date();
			code1 = Math.floor(Math.random() * 1000);
			code2 = Math.floor(Math.random() * 1000);
			var query_insert = { email: req.body.email, password: req.body.password, type: 0, active: false, verifyCode: code1+""+code2, date: date};
			var query_exist = { email: req.body.email };
			if( validateEmail(req.body.email)){
				if( req.body.password.length > 7){
					dbo.collection("users").find(query_exist).toArray(function(err, result) {
						if (err) throw err;
						if(result.length == 0){
							dbo.collection("users").insertOne(query_insert, function(err, result) {
								if (err) throw err;
								mailOptions.to = req.body.email;
								mailOptions.subject = "StraightAS - Verifikacija";
								mailOptions.text = "Vaša verifikacijska koda: "+code1+""+code2+".\nV kolikor vas stran ni preusmerila pojdite na: https://tpo-lp4-straight-as.herokuapp.com/verify";
								transporter.sendMail(mailOptions, function(error, info){
								  if (error) {
									console.log(error);
								  } else {
									console.log('REGISTRATION: Verification email sent');
								  }
								});
								console.log("REGISTRATION: User registered");
								res.send({type:0}); 
								db.close();
							});
						}else{
							console.log("REGISTRATION: User already exists");
							res.send({type:1}); 
						}
						db.close();
					});	
				}else{
					console.log("REGISTRATION: Password to short");
					res.send({type:2});  
				}				
			}else{
				console.log("REGISTRATION: Wrong email used");
				res.send({type:3}); 
			}
		});				
	});
	
app.route('/verify')
	.get(sessionCheck, (req, res) => {
		res.sendFile(__dirname + '/html/verifikacija.html');
	})
	.post((req, res) => {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			if (err) throw err;
			var dbo = db.db(databaseName);
			var query_exist = { email: req.body.email };
			if( validateEmail(req.body.email)){
				dbo.collection("users").find(query_exist).toArray(function(err, result) {
					if (err) throw err;
					if(result.length > 0){
						if( result[0].verifyCode == req.body.code ){
							var query = { email: req.body.email, password: result[0].password };
							var query_new = { $set: {active: true} };
							dbo.collection("users").updateOne(query, query_new, function(err, res) {
								if (err) throw err;
								console.log("VERIFICATION: User data updated (active -> true)");
								db.close();
							});
							req.session.user = query;
							req.session.usertype = 0;
							console.log("VERIFICATION: User successfuly verified: "+result[0].verifyCode+" == "+req.body.code);
							console.log("LOGIN: Success login");
							console.log("USER: ", req.session.user);
							res.send({type:0}); 							
						}else{
							console.log("VERIFICATION: Verify code is wrong");
							res.send({type:1}); 							
						}
					}else{
						console.log("VERIFICATION: User with that email does not exist");
						res.send({type:2}); 
					}
					db.close();
				});					
			}else{
				console.log("VERIFICATION: Wrong email used");
				res.send({type:3}); 
			}
		});				
	});
	
	
app.route('/changepass')
	.get(sessionCheck, (req, res) => {
		res.sendFile(__dirname + '/html/sprememba.html');
	})
	.post((req, res) => {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			if (err) throw err;
			var dbo = db.db(databaseName);
			code1 = Math.floor(Math.random() * 1000);
			code2 = Math.floor(Math.random() * 1000);
			var query_exist = { email: req.body.email };
			dbo.collection("users").find(query_exist).toArray(function(err, result) {
				if( req.body.password.length > 7){
					if (err) throw err;
					if(result.length > 0){
						mailOptions.to = req.body.email;
						mailOptions.subject = "StraightAS - Sprememba gesla";
						mailOptions.text = "Vaša potrditvena koda za spremembo gesla: "+code1+""+code2+".\nV kolikor vas stran ni preusmerila pojdite na: https://tpo-lp4-straight-as.herokuapp.com/verify";
						transporter.sendMail(mailOptions, function(error, info){
				            if (error) {
								console.log(error);
							} else {
								console.log('PASSWORD CHANGE: Email change password code sent');
						    }
						});
						var query_new = { $set: {password: req.body.password, verifyCode: code1+""+code2, active: false} };
						dbo.collection("users").updateOne(query_exist, query_new, function(err, res) {
							if (err) throw err;
							console.log("PASSWORD CHANGE: User data updated (active -> false, password -> new, verifyCode -> new)");
							db.close();
						});						
						res.send({type:0});
					}else{
						res.send({type:1}); 			
					};
				}else{
					console.log("PASSWORD CHANGE: New password too short");	
					res.send({type:2});	
				}
				db.close()
			})
		});
	});

app.route('/admin')
	.get(adminCheck, (req, res) => {
		res.sendFile(__dirname + '/html/admin.html');	
	})
	.post((req, res) => {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			if (err) throw err;
			var dbo = db.db(databaseName);
			var query_insert = { info:req.body.info, date:req.body.date, uid:req.body.uid };
			dbo.collection("info").insertOne(query_insert, function(err, result) {
				res.send();	
				db.close()
			})
		});
	});
	
app.route('/editor')
	.get(editorCheck, (req, res) => {
		res.sendFile(__dirname + '/html/editor.html');	
	})
	.post((req, res) => {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			if (err) throw err;
			var dbo = db.db(databaseName);
			var query_insert = { name:req.body.name, date:req.body.date, location:req.body.location, time:req.body.time,
									endtime:req.body.endtime, description:req.body.description, price:req.body.price, uid:req.body.uid };
			dbo.collection("dogodki").insertOne(query_insert, function(err, result) {
				res.send();	
				db.close()
			})
		});
	});
	
app.get('/getevents', function (req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);	
		dbo.collection("dogodki").find({}).toArray(function(err, result) {
			if (err) throw err;
			console.log("EVENTS: events fetched");
			res.send(result);
			db.close();			
		});
	});
});

app.post('/delevents', editorCheck, function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) { 
		var dbo = db.db(databaseName);
		var query = { uid: req.body.uid};		
		dbo.collection("dogodki").deleteOne(query, function(err, result){	
			if (err) throw err;
			console.log("EVENTS: event deleted with id: "+req.body.uid);
			res.send();
			db.close();
		});
	});
});

app.get('/getinfo', function (req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);	
		dbo.collection("info").find({}).toArray(function(err, result) {
			if (err) throw err;
			console.log("EVENTS: events fetched");
			res.send(result);
			db.close();			
		});
	});
});

app.post('/delinfo', adminCheck, function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) { 
		var dbo = db.db(databaseName);
		var query = { uid: req.body.uid};		
		dbo.collection("info").deleteOne(query, function(err, result){	
			if (err) throw err;
			console.log("EVENTS: event deleted with id: "+req.body.uid);
			res.send();
			db.close();
		});
	});
});
	
app.get('/hrana_protected', function (req, res) {
	if (req.session.usertype) {
        if (req.session.usertype == 3) {
			res.sendFile(__dirname + '/html/hrana_protected.html');
		}else{
			res.redirect('/hrana');
		}
	}else{
		req.session.usertype = 3;
		res.sendFile(__dirname + '/html/hrana_protected.html');
	}
});

app.get('/avtobusi_protected', function (req, res) {
	if (req.session.usertype) {
        if (req.session.usertype == 3) {
			res.sendFile(__dirname + '/html/avtobusi_protected.html');
		}else{
			res.redirect('/avtobusi');
		}
	}else{
		req.session.usertype = 3;
		res.sendFile(__dirname + '/html/avtobusi_protected.html');
	}
});

app.get('/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
		console.log("LOGOUT: User data cleared");
        res.clearCookie('user_sid');
    }
	res.redirect('/login');
});

app.get('/gettodos', function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);	
		if( req.session.user ){ 
			var query = {email: req.session.user.email};
			dbo.collection("todos").find(query).toArray(function(err, result) {
				if (err) throw err;
				console.log("TODO: todos fetched");
				res.send(result);
				db.close();			
			});
		}else{
			res.send({type:0});
		}
	});
});

app.post('/addtodo', function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);		
		if( req.session.user ){ 
			var input = {email: req.session.user.email, todo: req.body.todo, uid: req.body.uid};
			dbo.collection("todos").insertOne(input, function(err, result){
				if (err) throw err;
				console.log("TODO: todo added");
				res.send();
				db.close();
			});
		}else{
			res.send({type:0});
		}
	});
});

app.post('/deltodo', function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) { 
		var dbo = db.db(databaseName);
		if( req.session.user ){ 
			var query = { email: req.session.user.email, uid: req.body.uid};		
			dbo.collection("todos").deleteOne(query, function(err, result){	
				if (err) throw err;
				console.log("TODO: todo deleted with id: "+req.body.uid+" for user: "+req.session.user.email);
				res.send();
				db.close();
			});
		}else{
			res.send({type:0});
		}
	});
});

app.post('/addsubject', function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);
		if( req.session.user ){ 
			var input = {
				email: req.session.user.email,
				hour: req.body.hour,
				day: req.body.day,
				subject: req.body.subject
			};
			
			dbo.collection("urnik").insertOne(input, function(err, res2){
				if (err) throw err;
				console.log("TIMETABLE: Subject added for user: "+req.session.user.email);
				res.send();
				db.close;
			});
		}else{
			res.send({type:0});
		}
	});

});

app.post('/delsubject', function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);
		if( req.session.user ){ 
			var query = {
				email: req.session.user.email,
				hour: req.body.hour,
				day: req.body.day,
			}
		
		dbo.collection("urnik").deleteOne(query, function(err, res2) {
			if (err) throw err;
			console.log("TIMETABLE: Subject deleted for user: "+req.session.user.email);
			res.send();
			db.close();
		});
		}else{
			res.send({type:0});
		}		
	});
});

app.get('/geturnik', function(req, res){
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		var dbo = db.db(databaseName);
		if( req.session.user ){ 
			var query = {email: req.session.user.email};

			dbo.collection("urnik").find(query).toArray(function(err, result) {
				if (err) throw err;
				res.send(result);
				db.close();			
			});
		}else{
			res.send({type:0});
		}	
	});
});

app.get('/home', userCheck, function (req, res) {
	res.sendFile(__dirname + '/html/home.html');
});

app.get('/dogodki', userCheck, function (zahteva, odgovor) {
    odgovor.sendFile(__dirname + '/html/dogodki.html');
});

app.get('/hrana', userCheck, function (zahteva, odgovor) {
    odgovor.sendFile(__dirname + '/html/hrana.html');
});

app.get('/avtobusi', userCheck, function (zahteva, odgovor) {
    odgovor.sendFile(__dirname + '/html/avtobusi.html');
});

app.get('/js/prijava.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/prijava.js');
});

app.get('/js/registracija.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/registracija.js');
});

app.get('/js/verifikacija.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/verifikacija.js');
});

app.get('/js/sprememba.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/sprememba.js');
});

app.get('/js/main.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/main.js');
});

app.get('/js/hrana.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/hrana.js');
});

app.get('/js/dogodki.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/dogodki.js');
});

app.get('/js/avtobusi.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/dogodki.js');
});

app.get('/js/hrana_protected.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/hrana_protected.js');
});

app.get('/js/avtobusi_protected.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/avtobusi_protected.js');
});

app.get('/js/admin.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/admin.js');
});

app.get('/js/editor.js', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/js/editor.js');
});

app.get('/css/stili.css', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/css/stili.css');
});

app.get('/css/indexStili.css', function (zahteva, odgovor) {
	odgovor.sendFile(__dirname + '/html/css/indexStili.css');
});

app.use(function (req, res, next) {
  res.status(404).send("404");
});

app.listen(port, function () {
  console.log("SERVER: Running on port " + port);
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

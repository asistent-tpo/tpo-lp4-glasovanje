function toHome(){
    window.location = "./home";
}

function verifikacija(){
	var email = document.getElementById("email_ver").value;
	var code = document.getElementById("koda_ver").value;
	
	var uporabnik = { email: email, code: code };

	console.log(uporabnik);
	$.post(
		"./verify",
		uporabnik,
		function(data, status){
			if(data.type == 0){ 
				alert("Verifikacija uspešna! Preusmeritev na domačo stran."); 
				toHome();
			}
			if(data.type == 1){ 
				alert("Napačna verifikacijska koda!"); 
			}
			if(data.type == 2){ 
				alert("Uporabnik s tem emailom ni registriran!"); 
			}				
			if(data.type == 3){ 
				alert("Napačen email!");
			}
		});
	
}
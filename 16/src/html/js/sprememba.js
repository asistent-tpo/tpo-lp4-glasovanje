function toPrijava(){
    window.location = "./login";
}

function toChangePassCode(){
    window.location = "./verify";
}

function sprememba(){
	var email = document.getElementById("email_ch_pass").value;
	var pass = document.getElementById("geslo_ch_pass").value;
	var pass_ = document.getElementById("geslo_repeat_ch_pass").value;
	
	if( pass != pass_ ){
		alert("Gesli se ne ujemata");
	}else{	
		var uporabnik = {email: email, password: pass};
		
		$.post(
			"./changepass",
			uporabnik,
			function(data, status){
				if(data.type == 0){
					alert("Geslo uspešno spremenjeno! Na vaš mail naslov bo poslana verifikacijska koda. Preusmerjeni boste na stran za verifikacijo");	
					toChangePassCode();
				}
				if(data.type == 1){
					alert("Uporabnik s tem email naslovom ne obstaja!");					
				}
				if(data.type == 2){ 
					alert("Geslo mora biti dolgo vsaj 8 znakov!"); 
				}
			});
	}
}
function toHome(){
	window.location = "./home";
}

function toHrana(){
	window.location = "./hrana";
}

function toDogodki(){
	window.location = "./dogodki";
}

function toAvtobusi(){
	window.location = "./avtobusi";
}

function toLogout(){
	window.location = "./logout";
}

window.onload = function() {
	getEvents();
	getInfo();
};

function getEvents(){
	$.get(
		"./getevents",
		function(data, status){
			data.forEach(function(entry) {
				var base = document.createElement("div");
				base.className = "base";
				var todo = document.createElement("div");
				todo.className = "todo";
				var list = document.createTextNode("Ime dogodka: "+entry.name+"\nDatum: "+entry.date+"\nLokacija: "+entry.location+"\nZačetek: "+entry.time+"\nKonec: "+entry.endtime+"\nOpis:"+entry.description+"\nCena: (eur)"+entry.price);
				
				var oktodo = document.createElement("div");
				oktodo.className = "okTODO";			
				
				todo.appendChild(list);
				todo.appendChild(oktodo);
				base.appendChild(todo);
				
				document.getElementById("levastran").appendChild(base);
			});
		});
}

function getInfo(){
	$.get(
		"./getinfo",
		function(data, status){
			data.forEach(function(entry) {
				var base = document.createElement("div");
				base.className = "base";
				var todo = document.createElement("div");
				todo.className = "todo";
				var list = document.createTextNode("Datum: "+entry.date+"\nOpis: "+entry.info);
				
				var oktodo = document.createElement("div");
				oktodo.className = "okTODO";
				
				todo.appendChild(list);
				todo.appendChild(oktodo);
				base.appendChild(todo);
				
				document.getElementById("desnastran").appendChild(base);
			});
		});
}
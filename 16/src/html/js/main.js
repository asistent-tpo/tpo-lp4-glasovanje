function toHome(){
	window.location = "./home";
}

function toHrana(){
	window.location = "./hrana";
}

function toDogodki(){
	window.location = "./dogodki";
}

function toAvtobusi(){
	window.location = "./avtobusi";
}

function toLogout(){
	window.location = "./logout";
}

window.onload = function() {
	appendTodos();
	appendUrnik();
};

function appendUrnik(){
	var urnik = new Array(24);
	for (var i = 0; i < urnik.length; i++) {
	  urnik[i] = new Array(5);
	}	
	
	$.get(
		"./geturnik",
		function(data, status){
			data.forEach(function(entry) {
				urnik[entry.hour][entry.day] = entry.subject;
			});
			for (var i = 0; i < 24; i++) {
				var row = document.createElement("tr");
				var ura = document.createElement("th");
				var textUra = document.createTextNode(i + ":00");
				ura.appendChild(textUra);
				row.appendChild(ura);
				
				for (var j = 0; j < 5; j++) {
					
					var ele = document.createElement("td");
					var text;
					if (typeof urnik[i][j] !== 'undefined'){
						text = document.createTextNode(urnik[i][j]);
					}else{
						text = document.createTextNode("");
					}			
					ele.appendChild(text);
					row.appendChild(ele);
					ele.className = "urnikEle";
				}
				document.getElementById("urnik_table").appendChild(row);
			}
		});
}

function addSubject(){
	var dur = document.getElementById("duration").value;
	var startTime = document.getElementById("subjectHour").value;
	for(var i = 0; i < dur; i++){
		var input = {
			hour: parseInt(document.getElementById("subjectHour").value) + i,
			day: document.getElementById("subjectDay").value,
			subject: document.getElementById("subject").value
		};
		
		$.post(
			"./addsubject",
			input,
			function(data, status){
				location.reload();
			}
		);
	}
	
	
}

function delSubject(){
	var input = {
		hour: document.getElementById("subjectHourDel").value,
		day: document.getElementById("subjectDayDel").value
	};
	
	$.post(
		"./delsubject",
		input,
		function(data, status){
			location.reload();
		}
	);
}

function appendTodos(){
	$.get(
		"./gettodos",
		function(data, status){
			data.forEach(function(entry) {
				var base = document.createElement("div");
				base.className = "base";
				var todo = document.createElement("div");
				todo.className = "todo";
				var text = document.createTextNode(entry.todo);
				
				var oktodo = document.createElement("div");
				oktodo.className = "okTODO";
				var button = document.createElement("button");
				button.className = "fas fa-check";
				button.id = entry.uid;
				
				var button_id = button.id;
				
				button.onclick = function(){
					$.post(
						"./deltodo",
						{uid: button_id},
						function(data, status){
							location.reload();
						});	
				};
				
				oktodo.appendChild(button);
				todo.appendChild(text);
				todo.appendChild(oktodo);
				base.appendChild(todo);
				
				document.getElementById("desnastran").appendChild(base);
			});
		});
}

function addtodo(){
	var uid_1 = Math.floor(Math.random() * 1000);
	var uid_2 = Math.floor(Math.random() * 1000);
	var uid = uid_1+""+uid_2;

	var text = {todo: document.getElementById("todotext").value, uid:uid};
	$.post(
		"./addtodo",
		text,
		function(data, status){
			location.reload();
		}
	);	
}

function spremeniPostajo(){
	document.getElementById("busframe").src = "https://www.trola.si/" + document.getElementById("postaja").value
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

function openAddSubject() {
  document.getElementById("addSubject").style.display = "block";
}

function closeAddSubject() {
  document.getElementById("addSubject").style.display = "none";
}

function openDelSubject() {
  document.getElementById("delSubject").style.display = "block";
}

function closeDelSubject() {
  document.getElementById("delSubject").style.display = "none";
}


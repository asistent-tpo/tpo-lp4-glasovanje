function toEditor(){
    window.location = "./editor";
}

function toLogout(){
    window.location = "./logout";
}

window.onload = function() {
	getEvents();
};

function addEvent(){
	var name = document.getElementById("name").value;
	var date = document.getElementById("date").value;
	var location = document.getElementById("location").value;
	var start = document.getElementById("start").value;
	var end = document.getElementById("end").value;
	var description = document.getElementById("description").value;
	var price = document.getElementById("price").value;	

	var uid_1 = Math.floor(Math.random() * 1000);
	var uid_2 = Math.floor(Math.random() * 1000);
	var uid = uid_1+""+uid_2;
	
	var event_new = { name:name, date:date, location:location, time:start, endtime:end, description:description, price:price, uid:uid };

	$.post(
		"./editor",
		event_new,
		function(data, status){
			console.log("OK");
			closeForm();
		});
}

function getEvents(){
	$.get(
		"./getevents",
		function(data, status){
			data.forEach(function(entry) {
				var base = document.createElement("div");
				base.className = "base";
				var todo = document.createElement("div");
				todo.className = "todo";
				var list = document.createTextNode("Ime dogodka: "+entry.name+"\nDatum: "+entry.date+"\nLokacija: "+entry.location+"\nZačetek: "+entry.time+"\nKonec: "+entry.endtime+"\nOpis:"+entry.description+"\nCena: (eur)"+entry.price);
				
				var oktodo = document.createElement("div");
				oktodo.className = "okTODO";
				var button = document.createElement("button");
				button.className = "fas fa-check";
				button.id = entry.uid;
				
				var button_id = button.id;
				
				button.onclick = function(){
					$.post(
						"./delevents",
						{uid: button_id},
						function(data, status){
							location.reload();
						});	
				};
				
				oktodo.appendChild(button);
				
				todo.appendChild(list);
				todo.appendChild(oktodo);
				base.appendChild(todo);
				
				document.getElementById("levastran").appendChild(base);
			});
		});
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
function toPrijava(){
    window.location = "./login";
}

function toVerify(){
    window.location = "./verify";
}

function registracija(){
	var pass = document.getElementById("geslo_reg").value;
	var pass_ = document.getElementById("geslo_r_reg").value;
	var email = document.getElementById("email_reg").value;
	
	var uporabnik = { email: email, password: pass };

	if( pass != pass_ ){
		alert("Gesli se ne ujemata");
	}else{
		console.log(uporabnik);
		$.post(
			"./registration",
			uporabnik,
			function(data, status){
				if(data.type == 0){ 
					alert("Poslan vam je bil email s verifikacijsko kodo. Preusmerjeni boste na stran za verifikacijo!"); 
					toVerify();
				}
				if(data.type == 1){ 
					alert("Uporabnik s tem email naslovom že obstaja!"); 
				}
				if(data.type == 2){ 
					alert("Geslo mora biti dolgo vsaj 8 znakov!"); 
				}				
				if(data.type == 3){ 
					alert("Napačen email!");
				}
			});
	}
}
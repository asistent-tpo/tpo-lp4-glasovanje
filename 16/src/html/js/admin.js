function toAdmin(){
    window.location = "./admin";
}

function toLogout(){
    window.location = "./logout";
}

window.onload = function() {
	getInfo();
};

function addInfo(){
	var date = document.getElementById("date").value;
	var info = document.getElementById("info").value;

	var uid_1 = Math.floor(Math.random() * 1000);
	var uid_2 = Math.floor(Math.random() * 1000);
	var uid = uid_1+""+uid_2;
	
	var event_new = { date:date, info:info, uid:uid };

	$.post(
		"./admin",
		event_new,
		function(data, status){
			console.log("OK");
			closeForm();
		});
}

function getInfo(){
	$.get(
		"./getinfo",
		function(data, status){
			data.forEach(function(entry) {
				var base = document.createElement("div");
				base.className = "base";
				var todo = document.createElement("div");
				todo.className = "todo";
				var list = document.createTextNode("Datum: "+entry.date+"\nOpis: "+entry.info);
				
				var oktodo = document.createElement("div");
				oktodo.className = "okTODO";
				var button = document.createElement("button");
				button.className = "fas fa-check";
				button.id = entry.uid;
				
				var button_id = button.id;
				
				button.onclick = function(){
					$.post(
						"./delinfo",
						{uid: button_id},
						function(data, status){
							location.reload();
						});	
				};
				
				oktodo.appendChild(button);
				
				todo.appendChild(list);
				todo.appendChild(oktodo);
				base.appendChild(todo);
				
				document.getElementById("levastran").appendChild(base);
			});
		});
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
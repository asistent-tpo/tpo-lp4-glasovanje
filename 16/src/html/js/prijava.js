function toRegistracija(){
    window.location = "./registration";
}

function toSprememba(){
    window.location = "./changepass";
}

function toAdmin(){
    window.location = "./admin";
}

function toEditor(){
    window.location = "./editor";
}

function toHome(){
    window.location = "./home";
}

function toHomeNonreg(){
    window.location = "./hrana_protected";
}

function prijava(){
	var email = document.getElementById("email_prijava").value;
	var pass = document.getElementById("geslo_prijava").value;

	var uporabnik = {email: email, password: pass};
	$.post(
		"./login",
		uporabnik,
		function(data, status){
			if(data.type == 0){ 
				toHome();
			}
			if(data.type == 1){ 
				toAdmin();
			}
			if(data.type == 2){ 
				toEditor();
			}
			if(data.type == 3){ 
				alert("Ra�un ni potrjen! Poglejte va�o spletno po�to ter potrdite ra�un na /verify");
			}
			if(data.type == 4){ 
				alert("Prijava neuspe�na - email naslov ali geslo je napa�no!");
			}
		});
}
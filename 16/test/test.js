var request = require('supertest');
var streznik = require('../src/streznik.js')
var assert = require('assert');
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = chai.expect;
chai.use(chaiHttp);
chai.should();

var host = 'http://127.0.0.1:3000';

describe('Test prijava', function() {
	
	it('GET Prijava', function(done) {
	  chai.request(host)
	  .get('/login')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
    it("POST Prijava (existing user)", (done) => {
	    chai.request(host)
		    .post('/login')
			.send(existingUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(0);
			  res.should.have.status(200);
			  done();
		    });
    });
	
    it("POST Prijava (nonexsisting user)", (done) => {
	    chai.request(host)
		    .post('/login')
			.send(nonexistingUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(4);
			  res.should.have.status(200);
			  done();
		    });
    });
	
    it("POST Prijava Admin", (done) => {
	    chai.request(host)
		    .post('/login')
			.send(adminUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(1);
			  res.should.have.status(200);
			  done();
		    });
    });
	
    it("POST Prijava Editor", (done) => {
	    chai.request(host)
		    .post('/login')
			.send(editorUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(2);
			  res.should.have.status(200);
			  done();
		    });
    });
});

describe('Test registracija', function() {
	
	it('GET Registracija', function(done) {
	  chai.request(host)
	  .get('/registration')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
    it("POST Registracija (existing user)", (done) => {
	    chai.request(host)
		    .post('/registration')
			.send(existingUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(1);
			  res.should.have.status(200);
			  done();
		    });
    });
	
    it("POST Registracija (short password)", (done) => {
	    chai.request(host)
		    .post('/registration')
			.send(wrongFormUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(2);
			  res.should.have.status(200);
			  done();
		    });
    });
	
    it("POST Registracija (nonexisting user)", (done) => {
	    chai.request(host)
		    .post('/registration')
			.send(nonexistingUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(3);
			  res.should.have.status(200);
			  done();
		    });
    });
	

});

describe('Test spremembaGesla', function() {
	
	it('GET Sprememba', function(done) {
	  chai.request(host)
	  .get('/changepass')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
	it("POST Sprememba (non-existing)", (done) => {
	    chai.request(host)
		    .post('/changepass')
			.send(changePassUserFailMail)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(1);
			  res.should.have.status(200);
			  done();
		    });
    });
	
    it("POST Sprememba (success)", (done) => {
	    chai.request(host)
		    .post('/changepass')
			.send(changePassUser)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(0);
			  res.should.have.status(200);
			  done();
		    });
    });
	
	it("POST Sprememba (pass fail)", (done) => {
	    chai.request(host)
		    .post('/changepass')
			.send(changePassUserFailPass)
		    .end((err, res) => {
			  if(err) done(err);
              res.body.type.should.equal(2);
			  res.should.have.status(200);
			  done();
		    });
    });
	
});

describe('Test Index', function() {
	it('GET Home', function(done) {
	  chai.request(host)
	  .get('/home')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	it('GET Hrana', function(done) {
	  chai.request(host)
	  .get('/hrana')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	it('GET Avtobusi', function(done) {
	  chai.request(host)
	  .get('/avtobusi')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	it('GET Dogodki', function(done) {
	  chai.request(host)
	  .get('/dogodki')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
});

describe('Test Index - Nonregistered', function() {
	it('GET Hrana', function(done) {
	  chai.request(host)
	  .get('/hrana_protected')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
	it('GET Avtobusi', function(done) {
	  chai.request(host)
	  .get('/avtobusi_protected')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
});

describe('Test Admin/Editor', function() {
	it('GET Admin', function(done) {
	  chai.request(host)
	  .get('/admin')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
	it('GET Editor', function(done) {
	  chai.request(host)
	  .get('/editor')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
});

describe('Functionalities', function() {
	it('GET TODO', function(done) {
		  chai.request(host)
		  .get('/gettodos')
		  .end(function(err, res) {
			expect(res).to.have.status(200);
			done();
		  });
	});
	
	it('GET Event', function(done) {
	  chai.request(host)
	  .get('/getevents')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
	it('GET Info', function(done) {
	  chai.request(host)
	  .get('/getinfo')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
	
	it('GET Urnik', function(done) {
	  chai.request(host)
	  .get('/geturnik')
	  .end(function(err, res) {
		expect(res).to.have.status(200);
		done();
	  });
	});
});

let existingUser = {
    "email": "test@test.test",
    "password": "11111111"
};

let nonexistingUser = {
    "email": "",
    "password": ""
};

let wrongFormUser = {
    "email": "test@test.test",
    "password": ""
};

let changePassUser = {
    "email": "test@change.test",
    "password": "11111111"
};

let changePassUserFailPass = {
    "email": "test@test.test",
    "password": "22"
};

let changePassUserFailMail = {
    "email": "",
    "password": "11111111"
};

let adminUser = {
    "email": "admin@admin.com",
    "password": "administrator"
};

let editorUser = {
    "email": "admin@dogodek.com",
    "password": "admineditor"
};
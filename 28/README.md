# 28. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

HEROKU : https://todoappfri.herokuapp.com

BITBUCKET: https://bitbucket.org/saulas/tpo-lp4/commits/b55da5c8994f79a2af019d0a467b5cff49465eb3

TESTNI UPORABNIK:

Uporabniško ime: student@gmail.com

Geslo: student

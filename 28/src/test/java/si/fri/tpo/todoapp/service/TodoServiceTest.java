/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.isA;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.model.TODO;
import si.fri.tpo.todoapp.repository.TodoRepository;

/**
 *
 * @author StuZender
 */

@RunWith(SpringRunner.class)
public class TodoServiceTest {
    
    @MockBean TodoRepository todoRepository;
    
    @Test
    public void testFindAllTodo() {
        
        TODO todo1 = new TODO("Hello", null); // user je null
        TODO todo2 = new TODO("HEllo2", null); // user je null
        
        Mockito.when(todoRepository.findAll()).thenReturn(Arrays.asList(todo1, todo2));
        
        TodoService todoService = new TodoService(todoRepository);
        List<TODO> allTodo = todoService.findAll();
        assertThat(allTodo.size()).isEqualTo(2);
    }
    
    @Test
    public void testAddTodo() {
        
        TODO todo = new TODO("hello", null);
        
        Mockito.when(todoRepository.save(todo)).thenReturn(todo);
        TodoService todoService = new TodoService(todoRepository);
        
        TODO savedTodoForAssert = todoService.addTodo(todo);
        assertNotNull(savedTodoForAssert);
    }
    
    @Test
    public void testDeleteTodo() {
        
        doNothing().when(todoRepository).deleteById(isA(Long.class));
        TodoService todoService = new TodoService(todoRepository);
        
        todoService.deleteTodo(Long.MIN_VALUE);
        
        Assert.assertTrue(true);
    }
}

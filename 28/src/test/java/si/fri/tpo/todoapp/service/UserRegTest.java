package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.repository.CalendarEventRepository;
import si.fri.tpo.todoapp.repository.RoleRepository;
import si.fri.tpo.todoapp.repository.TimetableCourseRepository;
import si.fri.tpo.todoapp.repository.UserRegRepository;

@RunWith(SpringRunner.class)
public class UserRegTest {

    @MockBean
    private UserRegRepository userRegRepository;
    @MockBean
    private TimetableCourseRepository timetableCourseRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @MockBean
    private CalendarEventRepository calendarEventRepository;

    @Test
    public void findByEmail() {
        UserReg userReg = new UserReg("fri4@fri.si", "pass", "");
        userReg.setId(1L);

        Mockito.when(userRegRepository.findByEmail(userReg.getEmail()))
                .thenReturn(userReg);

        UserRegService userRegService = new UserRegService(userRegRepository, timetableCourseRepository, calendarEventRepository, roleRepository, bCryptPasswordEncoder);

        UserReg userReg2 = userRegService.findByEmail(userReg.getEmail());

        assertThat(userReg2.getId()).isEqualTo(userReg.getId());
    }

    @Test
    public void findAllUsers() {
        UserReg userReg = new UserReg("fri4@fri.si", "pass", "pass");
        UserReg userReg2 = new UserReg("fri4@fri.si", "pass", "pass");

        userReg.setId(1L);

        Mockito.when(userRegRepository.findAll()).thenReturn(Arrays.asList(userReg, userReg2));

        UserRegService userRegService = new UserRegService(userRegRepository, timetableCourseRepository, calendarEventRepository, roleRepository, bCryptPasswordEncoder);

        List<UserReg> users = userRegService.getAllUsers();

        assertThat(users.size()).isEqualTo(2);
    }
}

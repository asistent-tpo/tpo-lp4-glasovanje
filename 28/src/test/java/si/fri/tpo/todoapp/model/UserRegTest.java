/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Mitja
 */
public class UserRegTest {
       
    @Test

    public void testUserGetters() throws Exception {
       UserReg user = new UserReg("neki.neki@gmail.com", "uselessGeslo123", "uselessGeslo123");
       assertEquals(user.getEmail(), "neki.neki@gmail.com");
       assertEquals(user.getPassword(), "uselessGeslo123");
       assertEquals(user.getPasswordConfirm(), "uselessGeslo123");
    }
    
    @Test
    public void testUserSetters() throws Exception {;
       UserReg user = new UserReg("neki.neki@gmail.com", "uselessGeslo123", "uselessGeslo123");
       user.setEmail("nov@gmail.com");
       user.setPassword("newPass123");
       user.setPasswordConfirm("newPass123");
       
       assertEquals(user.getEmail(), "nov@gmail.com");
       assertEquals(user.getPassword(), "newPass123");
       assertEquals(user.getPasswordConfirm(), "newPass123");
    }
}

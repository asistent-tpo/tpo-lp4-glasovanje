/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.dto;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import si.fri.tpo.todoapp.dto.Bus;

/**
 *
 * @author urbic
 */
public class BusTest {
    @Test
    public void testNew() throws Exception {
        Bus bus = new Bus("Direction", "Number");

        assertEquals(bus.getDirection(),"Direction");
        assertEquals(bus.getNumber(),"Number");
    }
    
    @Test
    public void testSet() throws Exception {
        Bus bus = new Bus();

        bus.setDirection("Direction");
        bus.setNumber("Number");
        
        assertEquals(bus.getDirection(),"Direction");
        assertEquals(bus.getNumber(),"Number");
    }
}

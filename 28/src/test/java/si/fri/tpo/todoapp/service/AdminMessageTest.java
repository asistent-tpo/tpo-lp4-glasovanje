/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.model.AdminMessage;
import si.fri.tpo.todoapp.repository.AdminMessageRepository;

/**
 *
 * @author StuZender
 */

@RunWith(SpringRunner.class)
public class AdminMessageTest {
    
    @MockBean
    private AdminMessageRepository adminMessageRepository;
    
    @MockBean
    private AdminMessageService adminMessageService;
    
    @Test
    public void getAllMessages() {
        String message = "hello";
    	AdminMessage adminMessage = new AdminMessage(message);
    	
        Mockito.when(adminMessageRepository.findAll())
        	.thenReturn(Arrays.asList(adminMessage));
                
        List<AdminMessage> messages = new AdminMessageService(adminMessageRepository).getAllMessages();

        assertThat(messages.size()).isEqualTo(1);
        //assertThat(messages.get(0).getAlias()).isEqualTo(name);
    }
    
    @Test
    public void saveMessage() {
        
        String message = "hello";
        AdminMessage adminMessage = new AdminMessage(message);
        
        Mockito.when(adminMessageRepository.findAll())
                    .thenReturn(Arrays.asList(adminMessage));
        
        AdminMessage result = new AdminMessageService(adminMessageRepository).addMessage(adminMessage);
        
        assertNull(result);
    }
}

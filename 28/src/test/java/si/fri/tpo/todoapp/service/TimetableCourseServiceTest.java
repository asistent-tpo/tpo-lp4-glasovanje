/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.Optional;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.isA;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.repository.CalendarEventRepository;
import si.fri.tpo.todoapp.repository.RoleRepository;
import si.fri.tpo.todoapp.repository.TimetableCourseRepository;
import si.fri.tpo.todoapp.repository.UserRegRepository;

/**
 *
 * @author Mitja
 */
@RunWith(SpringRunner.class)
public class TimetableCourseServiceTest {
    @MockBean
    private TimetableCourseRepository timetableCourseRepository;
    private UserRegRepository userRegRepository;
    private CalendarEventRepository calendarEventRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Test
    public void testSaveTimetableCourse() {
        Timetable tempTt = new Timetable(null);
        TimetableCourse ttc1 = new TimetableCourse("TPO", 1, "#FFFFFF", "Monday", "13:00", tempTt);
        
        Mockito.when(timetableCourseRepository.save(ttc1)).thenReturn(ttc1);
        TimetableCourseService timeCourseServ = new TimetableCourseService(timetableCourseRepository);

        TimetableCourse savedCourse = timeCourseServ.saveCourse(ttc1);
        assertNotNull(savedCourse);
    }
    
    @Test
    public void testDeleteTimetableCourse() {        
        doNothing().when(timetableCourseRepository).deleteById(isA(Long.class));
        TimetableCourseService timeCourseServ = new TimetableCourseService(timetableCourseRepository);
        timeCourseServ.deleteCourseById(Long.MIN_VALUE);
        Assert.assertTrue(true);
    }
    
    @Test
    public void testFindTimetableCourseById() {
        Timetable tempTt = new Timetable(null);
        TimetableCourse ttc1 = new TimetableCourse("TPO", 1, "#FFFFFF", "Monday", "13:00", tempTt);

        Mockito.when(timetableCourseRepository.findById(ttc1.getId())).thenReturn(Optional.of(ttc1));
        
        TimetableCourse result = new TimetableCourseService(timetableCourseRepository).findById(ttc1.getId());
        
        assertThat(result.getId()).isEqualTo(ttc1.getId());
    }
}

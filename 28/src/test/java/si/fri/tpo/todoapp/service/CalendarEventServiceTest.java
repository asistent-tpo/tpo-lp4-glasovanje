/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.isA;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.model.CalendarEvent;
import si.fri.tpo.todoapp.repository.CalendarEventRepository;

/**
 *
 * @author Urban
 */

@RunWith(SpringRunner.class)
public class CalendarEventServiceTest {
    
    @MockBean CalendarEventRepository calendarEventRepository;
    
   
    
    @Test
    public void testAddEvent() {
        
        CalendarEvent event1 = new CalendarEvent("name", "10:00", "02:00", "desc", "25-04-2019", null); 
        
        Mockito.when(calendarEventRepository.save(event1)).thenReturn(event1);
        CalendarEventService calendarEventService = new CalendarEventService(calendarEventRepository);
        assertNotNull(calendarEventService.addEvent(event1));
    }
    
    @Test
    public void testDeleteEvent() {
        
        doNothing().when(calendarEventRepository).deleteById(isA(Long.class));
        CalendarEventService calendarEventService = new CalendarEventService(calendarEventRepository);
        
        calendarEventService.deleteEventById(Long.MIN_VALUE);
        
        Assert.assertTrue(true);
    }
    
    @Test
    public void testFindCalendarEventById() {
        CalendarEvent event1 = new CalendarEvent("name", "10:00", "02:00", "desc", "25-04-2019", null); 

        Mockito.when(calendarEventRepository.findById(event1.getId())).thenReturn(Optional.of(event1));
        
        CalendarEvent res = new CalendarEventService(calendarEventRepository).findById(event1.getId());
        
        assertThat(res.getId()).isEqualTo(event1.getId());
    }
    
}

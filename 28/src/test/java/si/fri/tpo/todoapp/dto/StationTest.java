/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.dto;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import si.fri.tpo.todoapp.dto.Station;

/**
 *
 * @author urbic
 */
public class StationTest {
    @Test
    public void testNew() throws Exception {
        Station station = new Station("Number", "Name");

        assertEquals(station.getName(),"Name");
        assertEquals(station.getNumber(),"Number");
    }
    
    @Test
    public void testSet() throws Exception {
        Station station = new Station();

        station.setName("Name");
        station.setNumber("Number");
        
        assertEquals(station.getName(),"Name");
        assertEquals(station.getNumber(),"Number");
    }
}

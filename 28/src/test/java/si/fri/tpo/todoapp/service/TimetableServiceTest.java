/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.repository.TimetableRepository;

/**
 *
 * @author Mitja
 */
@RunWith(SpringRunner.class)
public class TimetableServiceTest {
    
    @MockBean
    private TimetableRepository timetableRepository;
    
    @Test
    public void testFindAllTimetables() {
        Timetable tt1 = new Timetable(null);
        Timetable tt2 = new Timetable(null);
        Mockito.when(timetableRepository.findAll()).thenReturn(Arrays.asList(tt1,tt2));
        
        TimetableService timetableService = new TimetableService(timetableRepository);
        
        List<Timetable> timetables = timetableService.getAllTimetables();
        
        assertThat(timetables.size()).isEqualTo(2);
    }
    
    @Test
    public void testSaveTimetableWorks() {
        Timetable tt1 = new Timetable(null);
        Timetable tt2 = new Timetable(null);
        
        Mockito.when(timetableRepository.save(tt1)).thenReturn(tt1);
        TimetableService timetableService = new TimetableService(timetableRepository);
        Mockito.when(timetableRepository.findAll()).thenReturn(Arrays.asList(tt1));
        
        Timetable savedTimetable = timetableService.saveTimetable(tt1);

        assertNotNull(savedTimetable);
    }
    
    @Test
    public void testFindTimetableById() {
        Timetable tt1 = new Timetable(null);

        Mockito.when(timetableRepository.findById(tt1.getId())).thenReturn(Optional.of(tt1));
        
        Timetable result = new TimetableService(timetableRepository).findById(tt1.getId());
        
        assertThat(result.getId()).isEqualTo(tt1.getId());
    }
}

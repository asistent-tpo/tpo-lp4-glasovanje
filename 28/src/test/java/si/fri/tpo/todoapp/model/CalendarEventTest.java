/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;


import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Urban
 */
public class CalendarEventTest {    
    
    @Test
    public void testNew() throws Exception {
        CalendarEvent event = new CalendarEvent("Name", "time", "duration", "Desc", "date", null);

        assertEquals(event.getName(),"Name");
        assertEquals(event.getDescription(),"Desc");
        assertEquals(event.getDate(),"date");
        assertEquals(event.getTime(),"time");
        assertEquals(event.getDuration(),"duration");
    }
    @Test
    public void testSet() throws Exception {
        CalendarEvent event = new CalendarEvent();

        event.setName("Name");
        event.setDescription("Desc");
        event.setDate("date");
        event.setTime("time");
        event.setDuration("duration");
        
        assertEquals(event.getName(),"Name");
        assertEquals(event.getDescription(),"Desc");
        assertEquals(event.getDate(),"date");
        assertEquals(event.getTime(),"time");
        assertEquals(event.getDuration(),"duration");
    }
}

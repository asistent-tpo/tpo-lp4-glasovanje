/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

/**
 *
 * @author HP
 */
import java.text.ParseException;
import si.fri.tpo.todoapp.model.Event;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import si.fri.tpo.todoapp.repository.EventRepository;

/**
 *
 * @author tomaz
 */
@RunWith(SpringRunner.class)
public class EventServiceTest {

    @MockBean
    private EventRepository eventRepository;

    @Test
    public void testFindAllEvent() {

        Event event1 = new Event("Test name", "Test date", "organizer", "desc");
        Event event2 = new Event("Test name2", "Test date2", "organizer2", "desc2");
        Event event3 = new Event("Test name3", "Test date3", "organizer3", "desc3");
        Mockito.when(eventRepository.findAll()).thenReturn(Arrays.asList(event1, event2, event3));

        EventService eventService = new EventService(eventRepository);

        List<Event> allEvents = eventService.getAllEvents();

        assertThat(allEvents.size()).isEqualTo(3);
    }
    
    @Test
    public void testAddEventOk() throws ParseException {

        Event event = new Event("Test name", "Test date", "organizer", "desc");

        Mockito.when(eventRepository.save(event)).thenReturn(event);
        EventService eventService = new EventService(eventRepository);
        Mockito.when(eventRepository.findAll()).thenReturn(Arrays.asList(event));

        Event savedEventForAsset = eventService.addEvent(event);

        assertNotNull(savedEventForAsset);
    }

}

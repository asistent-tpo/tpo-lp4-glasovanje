/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author StuZender
 */
@RunWith(SpringRunner.class)
public class TodoTest {
    
    @Test
    public void testNewTodo() throws Exception {
       TODO todo = new TODO("description", null);
       assertEquals(todo.getDescription(), "description");
    }
    
    @Test
    public void testSetTodo() throws Exception {
       TODO todo = new TODO("description", null);
       todo.setDescription("newDescription");
       assertEquals(todo.getDescription(), "newDescription");
    }
}

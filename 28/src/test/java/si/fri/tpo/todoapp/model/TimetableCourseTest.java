/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import java.util.Date;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;

/**
 *
 * @author Mitja
 */

public class TimetableCourseTest {

    @Test
    public void testCourseGetters() throws Exception {
       Timetable tempTt = new Timetable(null);
       TimetableCourse course = new TimetableCourse("TPO", 1, "#FFFFFF", "Monday", "13:00", tempTt);
       assertEquals(course.getName(), "TPO");
       assertEquals(course.getDuration(), 1);
       assertEquals(course.getColour(), "#FFFFFF");
       assertEquals(course.getDay(),"Monday");
       assertEquals(course.getHour(),"13:00");
    }

    @Test
    public void testCourseSetters() throws Exception {
       Timetable tempTt = new Timetable(null);
       TimetableCourse course = new TimetableCourse("TPO", 1, "#FFFFFF", "Monday", "13:00", tempTt);
       
       course.setName("DS");
       course.setDuration(1);
       course.setColour("#000000");
       course.setDay("Wednesday");
       course.setHour("14:00");
       
       assertEquals(course.getName(), "DS");
       assertEquals(course.getDuration(), 1);
       assertEquals(course.getColour(), "#000000");
       assertEquals(course.getDay(),"Wednesday");
       assertEquals(course.getHour(),"14:00");
    }
}

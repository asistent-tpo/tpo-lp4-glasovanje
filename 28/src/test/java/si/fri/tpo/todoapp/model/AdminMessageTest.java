/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author StuZender
 */
@RunWith(SpringRunner.class)
public class AdminMessageTest {
    
    @Test
    public void testNewAdminMessage() throws Exception {
       AdminMessage adminMessage = new AdminMessage("message");
       assertEquals(adminMessage.getMessage(), "message");
    }
    
    @Test
    public void testSetAdminMessage() throws Exception {
       AdminMessage adminMessage = new AdminMessage("message");
       adminMessage.setMessage("newMessage");
       assertEquals(adminMessage.getMessage(), "newMessage");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author StuZender
 */
@RunWith(SpringRunner.class)
public class EventTest {
    
    @Test
    public void testNewEvent() throws Exception {
       Event event = new Event("name", "01-01-2018", "organizer", "description");
       assertEquals(event.getName(), "name");
       assertEquals(event.getDate(), "01-01-2018");
       assertEquals(event.getOrganizer(), "organizer");
       assertEquals(event.getDescription(), "description");
    }
    
    @Test
    public void testSetEvent() throws Exception {
       Event event = new Event("name", "01-01-2018", "organizer", "description");
       event.setName("newName");
       event.setDate("02-02-2018");
       event.setOrganizer("newOrganizer");
       event.setDescription("newDescription");
       assertEquals(event.getName(), "newName");
       assertEquals(event.getDate(), "02-02-2018");
       assertEquals(event.getOrganizer(), "newOrganizer");
       assertEquals(event.getDescription(), "newDescription");
    }
}

package si.fri.tpo.client.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CustomAuthenticationFailureHandler.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {

        LOG.info("1");

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        LOG.info("2");
        Map<String, Object> data = new HashMap<>();
        System.out.println("3");
        data.put("timestamp", Calendar.getInstance().getTime());
        data.put("exception", exception.getMessage());

        LOG.info("4");
        response.getOutputStream().println(objectMapper.writeValueAsString(data));
    }
}

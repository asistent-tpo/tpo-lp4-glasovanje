/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author urban
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Station {
    private String number;
    private String name;
    private Bus[] buses;

    @JsonProperty("number")
    public String getNumber() { return number; }
    @JsonProperty("number")
    public void setNumber(String value) { this.number = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("buses")
    public Bus[] getBuses() { return buses; }
    @JsonProperty("buses")
    public void setBuses(Bus[] value) { this.buses = value; }
    
    public String toString(){
        String res = "";         
        for(Bus bus : buses){
            res+= "Bus: " + bus.getNumber() + " Direction: " + bus.getDirection() + " Arrivals: ";                
            for(Long arrival : bus.getArrivals()){
                res += arrival + " ";
            }
        }
        return res;
    }

    public Station(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public Station() {
    }
    
}

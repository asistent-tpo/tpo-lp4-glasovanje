/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.dto;

/**
 *
 * @author urbic
 */
public class TrolaSubmit {
    private String station;
    private Trola data;
    
    public Trola getData(){
        return data;
    }
    
    public void setData(Trola data) {
        this.data = data;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String content) {
        this.station = content;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.model.AdminMessage;
import si.fri.tpo.todoapp.repository.AdminMessageRepository;

/**
 *
 * @author StuZender
 */

@Service
public class AdminMessageService {
    
    private AdminMessageRepository adminMessageRepository;
    
    @Autowired
    public AdminMessageService(AdminMessageRepository adminMessageRepository) {
        this.adminMessageRepository = adminMessageRepository;
    }
    
    public List<AdminMessage> getAllMessages() {
        return adminMessageRepository.findAll();
    }

    public AdminMessage addMessage(AdminMessage message) {
        return this.adminMessageRepository.save(message);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.model.Calendar;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.repository.CalendarRepository;
import si.fri.tpo.todoapp.repository.TimetableRepository;

/**
 *
 * @author Urban
 */
@Service
public class CalendarService {
    private final CalendarRepository calendarRepository;
    
    @Autowired
    public CalendarService(CalendarRepository calendarRepository) {
        this.calendarRepository = calendarRepository;
    }
    
    public Calendar findById(Long id) {
        return this.calendarRepository.findById(id).orElse(null);
    }
    
    public Calendar findByUserId(Long id) {
        return this.calendarRepository.findById(id).orElse(null);
    }   

    @Transactional
    public Calendar saveCalendar(Calendar calendar) {
        return this.calendarRepository.save(calendar);
    }
    
     public List<Calendar> getAllCalendars() {
        return this.calendarRepository.findAll();
    }
}

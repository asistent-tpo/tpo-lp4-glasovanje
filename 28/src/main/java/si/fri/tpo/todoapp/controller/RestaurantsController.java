/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author StuZender
 */

@Controller
public class RestaurantsController {
    
    @RequestMapping("/restaurants")
    public String showHome() {
        return "restaurants";
    }    
    
    @RequestMapping("/restaurantsP")
    public String showResPublic() {
        return "restaurantsP";
    }  
}

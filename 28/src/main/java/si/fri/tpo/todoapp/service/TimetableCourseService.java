/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.controller.HelloController;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.repository.TimetableCourseRepository;

/**
 *
 * @author Mitja
 */
@Service
public class TimetableCourseService {
    private final TimetableCourseRepository timetableCourseRepository;
    private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);
    
    @Autowired
    public TimetableCourseService(TimetableCourseRepository timetableCourseRepository) {
        this.timetableCourseRepository = timetableCourseRepository;
    }
    
    public TimetableCourse findById(Long id) {
        return this.timetableCourseRepository.findById(id).orElse(null);
    }
    
    public TimetableCourse saveCourse(TimetableCourse course) {
        return this.timetableCourseRepository.save(course);
    }

    @Transactional
    public void deleteCourseById(Long id) {
        LOG.info("Delete");
        timetableCourseRepository.deleteById(id);
    }
    
    public void updateCourse(Long courseId, String courseName, int courseDuration, String courseColour, String courseDay, String courseHour) {
        TimetableCourse updatedCourse = timetableCourseRepository.getOne(courseId);
        updatedCourse.setName(courseName);
        updatedCourse.setDuration(courseDuration);
        updatedCourse.setColour(courseColour);
        updatedCourse.setDay(courseDay);
        updatedCourse.setHour(courseHour);
        //LOG.info(courseName + " " + courseColour + " " + courseDay + " " + courseHour);
        timetableCourseRepository.save(updatedCourse);
    }
}

package si.fri.tpo.todoapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import si.fri.tpo.todoapp.service.UserRegService;

@Controller
public class LoginController {

    @Autowired
    private UserRegService userRegService;

    private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        LOG.info("/login");
        LOG.info("----- registered users -------");
        LOG.info("users= {}", userRegService.getAllUsers());
        LOG.info("----- registered users -------");

        return "login";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import si.fri.tpo.todoapp.model.Calendar;
import si.fri.tpo.todoapp.model.CalendarEvent;
import si.fri.tpo.todoapp.model.TODO;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.repository.RoleRepository;
import si.fri.tpo.todoapp.security.service.interfaces.AuthenticationFacade;
import si.fri.tpo.todoapp.service.CalendarEventService;
import si.fri.tpo.todoapp.service.CalendarService;
import si.fri.tpo.todoapp.service.TimetableCourseService;
import si.fri.tpo.todoapp.service.TimetableService;
import si.fri.tpo.todoapp.service.TodoService;
import si.fri.tpo.todoapp.service.UserRegService;

/**
 *
 * @author StuZender
 */

@Controller
public class HomeController {
    
    private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);
    
    private TodoService todoService;
    private UserRegService userRegService;
    private AuthenticationFacade authenticationFacade;
    private TimetableService timetableService;
    private TimetableCourseService timetableCourseService;
    private final RoleRepository roleRepository;
    private CalendarService calendarService;
    private CalendarEventService calendarEventService;
    
    @Autowired
    public HomeController(TodoService todoService,
                          UserRegService userRegService, 
                          TimetableService timetableService, 
                          TimetableCourseService timetableCourseService,
                          AuthenticationFacade authenticationFacade,
                          RoleRepository roleRepository,
                          CalendarService calendarService,
                          CalendarEventService calendarEventService) {
        this.todoService = todoService;
        this.userRegService = userRegService;
        this.timetableService = timetableService;
        this.timetableCourseService = timetableCourseService;
        this.authenticationFacade = authenticationFacade;
        this.roleRepository = roleRepository;
        this.calendarService = calendarService;
        this.calendarEventService = calendarEventService;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello(Model model) {
        Authentication authentication = authenticationFacade.getAuthentication();
        LOG.info("user= {}", authentication.getName());
        //model.addAttribute("users", userService.getAllUsers());
        String userEmail = authentication.getName();
        UserReg user = userRegService.findByEmail(userEmail);
        
        if(user.getRoles().contains(roleRepository.findOneByName("STUDENT"))) {
            return "redirect:/home";
        }
        else if(user.getRoles().contains(roleRepository.findOneByName("EVENT_MANAGER"))) {
            return "redirect:/plannerEvents";
        }
        else {
            return "redirect:/adminPanel";
        }
    }    
    
    
    @RequestMapping("/home")
    public String showHome(Model model, @ModelAttribute("isError") String checkError) {
        Authentication authentication = this.authenticationFacade.getAuthentication();
        String userEmail = authentication.getName();
        UserReg currentUser = this.userRegService.findByEmail(userEmail);
        
        Timetable tempTimetable = userRegService.getUserTimetable(currentUser.getId());
        Calendar calendar = userRegService.getCalendar(currentUser.getId());
        model.addAttribute("isError", checkError+"");
        model.addAttribute("courses", tempTimetable.getCourses());
        model.addAttribute("calendarEvents", calendar.getEvents());
        model.addAttribute("todolist", this.todoService.findAllByUserEmail(userEmail));
        return "home";
    }
    
    @RequestMapping(method=RequestMethod.POST, path = "/home/timetable")
    public String saveCourse(RedirectAttributes checkError, @RequestParam String courseName, @RequestParam int courseDuration, @RequestParam String courseColour, @RequestParam String  courseDay, @RequestParam String  courseHour, Model model) {
        Authentication authentication = this.authenticationFacade.getAuthentication();
        String userEmail = authentication.getName();
        UserReg currentUser = this.userRegService.findByEmail(userEmail);
        
        String [] arrayH = courseHour.split(":");
        int hour = Integer.parseInt(arrayH[0]);
        
        if(hour + courseDuration > 16){ // more bit 1 več ker 15:00 + 1 is still viable
            checkError.addFlashAttribute("isError", "true");
            return "redirect:/home";
        }else{
            Timetable tempTimetable = userRegService.getUserTimetable(currentUser.getId());
            List <TimetableCourse> tempList = tempTimetable.getCourses();
            for(TimetableCourse element : tempList){
                String [] timeArray = element.getHour().split(":");
                int timeHour = Integer.parseInt(timeArray[0]); //ura predmeta
                
                for(int i = 0; i < courseDuration; i++){
                    int comparableHour = hour + i;
                    if(comparableHour == timeHour && element.getDay().equals(courseDay)){
                         //LOG.info("Time error: " + comparableHour + "  existing course: " + timeHour);
                         checkError.addFlashAttribute("isError", "true");
                         return "redirect:/home";
                    }
                }
            }
        }
        
        //If all checks out, loop and make courses
        for(int j = 0; j < courseDuration; j++){
            String adjustableHour = (hour+j) + ":00";
            this.userRegService.saveTimetableCourse(currentUser.getId(), courseName, 1, courseColour, courseDay, adjustableHour);
            
            //LOG.info(id + " " + courseName + " " + courseColour + " " + courseDay + " " + adjustableHour);
        }
        return "redirect:/home";
    }
    
    @RequestMapping(method=RequestMethod.GET, path="/home/deleteCourse", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteCourse(@RequestParam("courseId") Long courseId) {
        this.timetableCourseService.deleteCourseById(courseId);
        return "redirect:/home";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/home/editCourse")
    public String editCourse(@RequestParam Long courseId, RedirectAttributes checkError,
                                        @RequestParam String courseName,  
                                        @RequestParam String courseColour, 
                                        @RequestParam String  courseDay, 
                                        @RequestParam String  courseHour) {
        Authentication authentication = this.authenticationFacade.getAuthentication();
        String userEmail = authentication.getName();
        UserReg currentUser = this.userRegService.findByEmail(userEmail);
        
        String [] arrayH = courseHour.split(":");
        int hour = Integer.parseInt(arrayH[0]);
        int courseDuration = 1;
        
        if(hour + courseDuration > 16){ // more bit 1 več ker 15:00 + 1 is still viable
            checkError.addFlashAttribute("isError", "true");
            return "redirect:/home";
        }else{
            Timetable tempTimetable = userRegService.getUserTimetable(currentUser.getId());//TODO: HARDCODED! Ko bos prestavljal vse delaj z string email!
            List <TimetableCourse> tempList = tempTimetable.getCourses();
            for(TimetableCourse element : tempList){
                String [] timeArray = element.getHour().split(":");
                int timeHour = Integer.parseInt(timeArray[0]); //ura predmeta
                
                for(int i = 0; i < courseDuration; i++){
                    int comparableHour = hour + i;
                    //pogledamo da ne primerjamo ure samo z sabo
                    if(courseId != element.getId()){
                        //LOG.info("Nov čas: " + comparableHour + " Nov dan: " + courseDay + " " +  "||||         Obstoječ čas: " + timeHour + " Obstoječ dan " + element.getDay());
                        if(comparableHour == timeHour && element.getDay().equals(courseDay)){
                             checkError.addFlashAttribute("isError", "true");
                             return "redirect:/home";
                        }
                    }
                }
            }
        }
        
        
        //LOG.info(courseId + " " + courseName + " " + courseColour + " " + courseDay + " " + courseHour);
        this.timetableCourseService.updateCourse(courseId, courseName, courseDuration, courseColour, courseDay, courseHour);
        return "redirect:/home";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/home/todo")
    public String addTodo(@RequestParam("description") String description) {
        Authentication authentication = this.authenticationFacade.getAuthentication();
        String userEmail = authentication.getName();
        UserReg currentUser = this.userRegService.findByEmail(userEmail);
        
        if(description == null || description.length() > 250 || description.length() == 0) {
            return "redirect:/home";
        }
        
        TODO newTodo = new TODO(description, currentUser);
        this.todoService.addTodo(newTodo);
             
        return "redirect:/home";
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/home/deletetodo")
    public String deleteTodo(@RequestParam("todoId") Long todoId) {
        this.todoService.deleteTodo(todoId);
        return "redirect:/home";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/home/edittodo")
    public String editTodo(@RequestParam("todoId") Long todoId,
                           @RequestParam("description") String description) {
        
        if(description == null || description.length() > 250 || description.length() == 0) {
            return "redirect:/home";
        }
        
        this.todoService.updateTodo(todoId, description);
        return "redirect:/home";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/home/calendar")
    public String addCalEvent(@RequestParam("description") String description, @RequestParam("name") String name, @RequestParam("time") String time, @RequestParam("duration") String duration, @RequestParam("date") String date) {
        Authentication authentication = this.authenticationFacade.getAuthentication();
        String userEmail = authentication.getName();
        UserReg currentUser = this.userRegService.findByEmail(userEmail);
        this.userRegService.saveCalendarEvent(currentUser.getId(), name, duration, time, description, date);
       
        LOG.info("CalEvents:" + currentUser.getCalendar()); 
        return "redirect:/home";
    }
    @RequestMapping(method=RequestMethod.GET, value="/home/eventDelete")
    public String delCalEvent(@RequestParam("eventId") Long eventId) {
        this.calendarEventService.deleteEventById(eventId);
        return "redirect:/home";
    }
    @RequestMapping(method=RequestMethod.POST, value="/home/eventUpdate")
    public String updateCalEvent(@RequestParam("eventId") Long eventId, @RequestParam("description") String description, @RequestParam("name") String name, @RequestParam("time") String time, @RequestParam("duration") String duration, @RequestParam("date") String date) {
        Authentication authentication = this.authenticationFacade.getAuthentication();
        String userEmail = authentication.getName();
        UserReg currentUser = this.userRegService.findByEmail(userEmail);
        this.calendarEventService.updateEvent(eventId, name, duration, time, description, date);
        return "redirect:/home";
    }
}

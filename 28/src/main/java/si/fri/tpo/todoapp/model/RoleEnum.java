package si.fri.tpo.todoapp.model;

public enum RoleEnum {
    ADMIN, STUDENT, EVENT_MANAGER;
}

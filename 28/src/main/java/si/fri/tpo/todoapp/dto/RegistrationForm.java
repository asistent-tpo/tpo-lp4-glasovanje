package si.fri.tpo.todoapp.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class RegistrationForm {

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private String passwordConfirmed;

    public RegistrationForm() {
    }

    public RegistrationForm(String email, String password, String passwordConfirmed) {
        this.email = email;
        this.password = password;
        this.passwordConfirmed = passwordConfirmed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmed() {
        return passwordConfirmed;
    }

    public void setPasswordConfirmed(String passwordConfirmed) {
        this.passwordConfirmed = passwordConfirmed;
    }

    @Override
    public String toString() {
        return "RegistrationForm{" + "email=" + email + ", password=" + password + ", passwordConfirmed=" + passwordConfirmed + '}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.model.TODO;
import si.fri.tpo.todoapp.repository.TodoRepository;

/**
 *
 * @author StuZender
 */

@Service
public class TodoService {
    
    private final TodoRepository todoRepository;
    
    @Autowired
    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }
    
    public List<TODO> findAllByUserEmail(String userEmail) {
        return todoRepository.findByUserEmail(userEmail);
    }
    
    public void deleteTodo(Long todoId) {
        todoRepository.deleteById(todoId);
    }

    public List<TODO> findAll() {
        return todoRepository.findAll();
    }
    
    public TODO addTodo(TODO todo) {
        return this.todoRepository.save(todo);
    }
    
    public void updateTodo(Long todoId, String description) {
        TODO updatedTodo = todoRepository.getOne(todoId);
        updatedTodo.setDescription(description);
        todoRepository.save(updatedTodo);
    }
}

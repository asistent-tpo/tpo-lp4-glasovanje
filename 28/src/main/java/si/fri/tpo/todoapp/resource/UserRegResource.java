package si.fri.tpo.todoapp.resource;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.service.UserRegService;

@RestController
@RequestMapping("/api/user")
public class UserRegResource {

    private final UserRegService userService;

    @Autowired
    public UserRegResource(UserRegService userService) {
        this.userService = userService;
    }

    /*@GetMapping(path = "/{id}/timetable")
    public Timetable get(@PathVariable(name = "id") String id) {
        return this.userService.getUserTimetable(Long.parseLong(id));
    }*/

    /*
    @PostMapping(path = "/{id}/timetable")
    public TimetableCourse post(@PathVariable(name = "id") String id, @RequestParam String courseName, @RequestParam int courseDuration, @RequestParam String courseColour, @RequestParam String  courseDay, @RequestParam String  courseHour) {
        return this.userService.saveTimetableCourse(Long.parseLong(id), courseName, courseDuration, courseColour, courseDay, courseHour);
    }*/

    @GetMapping
    public List<UserReg> getAllAliases() {
        int randomNum = ThreadLocalRandom.current().nextInt(1, 1000);

//        Role adminRole = new Role(RoleEnum.ROLE_ADMIN.toString());
//        Role eventManager = new Role(RoleEnum.ROLE_EVENT_MANAGER.toString());
//        Role studentRole = new Role(RoleEnum.ROLE_STUDENT.toString());
//
//        Set<Role> roles = new HashSet<>();
//        roles.add(studentRole);
//
//        userService.saveUser(new UserReg("Brkonja" + randomNum, "Ivi" + randomNum, "password", roles));
        return userService.getAllUsers();
    }
}

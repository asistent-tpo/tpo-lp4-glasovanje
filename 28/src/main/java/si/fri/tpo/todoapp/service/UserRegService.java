package si.fri.tpo.todoapp.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.controller.HelloController;
import si.fri.tpo.todoapp.dto.RegistrationForm;
import si.fri.tpo.todoapp.model.Calendar;
import si.fri.tpo.todoapp.model.CalendarEvent;
import si.fri.tpo.todoapp.model.Role;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.repository.CalendarEventRepository;
import si.fri.tpo.todoapp.repository.RoleRepository;
import si.fri.tpo.todoapp.repository.TimetableCourseRepository;
import si.fri.tpo.todoapp.repository.UserRegRepository;

@Service
public class UserRegService {

    private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);

    private final UserRegRepository userRepository;
    private final TimetableCourseRepository timetableCourseRepository;
    private final CalendarEventRepository calendarEventRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserRegService(UserRegRepository userRepository, TimetableCourseRepository timetableCourseRepository, CalendarEventRepository calendarEventRepository,
            RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.timetableCourseRepository = timetableCourseRepository;
        this.calendarEventRepository = calendarEventRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserReg findById(Long id) {
        return this.userRepository.findById(id).orElse(null);
    }

    public UserReg findByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    public Timetable getUserTimetable(Long id) {
        return this.userRepository.findById(id).get().getTimetable();
    }

    public List<UserReg> getAllUsers() {
        return this.userRepository.findAll();
    }

    public Calendar getCalendar(Long id) {
        return this.userRepository.findById(id).get().getCalendar();
    }

    @Transactional
    public UserReg saveUser(UserReg user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        // hardcoded ----------------------------------------
        Set<Role> roles = new HashSet<>();
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        // hardcoded ----------------------------------------
        return this.userRepository.save(user);
    }

    @Transactional
    public UserReg updateUser(UserReg user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    // se uporablja samo za registracijo
    @Transactional
    public UserReg saveUser(RegistrationForm form) {

        if (form != null && form.getEmail() != null && form.getPassword() != null && form.getPasswordConfirmed() != null) {
            String passwordEncoded = bCryptPasswordEncoder.encode(form.getPassword());
            UserReg user = new UserReg(form.getEmail(), passwordEncoded, passwordEncoded);
            UserReg userInDb = userRepository.findByEmail(user.getEmail());

            if (userInDb == null) {
                Set<Role> roles = new HashSet<>();
                roles.add(roleRepository.findOneByName("STUDENT"));
                user.setRoles(roles);
                return this.userRepository.save(user);
            }
        }
        return null;
    }

    @Transactional
    public TimetableCourse saveTimetableCourse(Long id, String courseName, int courseDuration, String courseColour, String courseDay, String courseHour) {
        Timetable tempTimetable = this.userRepository.findById(id).get().getTimetable();
        TimetableCourse tc = new TimetableCourse(courseName, courseDuration, courseColour, courseDay, courseHour, tempTimetable);
        tempTimetable.addCourse(tc);
        return this.timetableCourseRepository.save(tc);
    }

    @Transactional
    public CalendarEvent saveCalendarEvent(Long id, String name, String duration, String time, String description, String date) {
        Calendar calendar = this.userRepository.findById(id).get().getCalendar();
        CalendarEvent calendarEvent = new CalendarEvent(name, time, duration, description, date, calendar);
        calendar.addEvent(calendarEvent);
        return this.calendarEventRepository.save(calendarEvent);
    }
}

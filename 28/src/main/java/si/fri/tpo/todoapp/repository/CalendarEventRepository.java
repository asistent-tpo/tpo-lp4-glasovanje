/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import si.fri.tpo.todoapp.model.Calendar;
import si.fri.tpo.todoapp.model.CalendarEvent;
import si.fri.tpo.todoapp.model.UserReg;

/**
 *
 * @author Urban
 */
public interface CalendarEventRepository extends JpaRepository<CalendarEvent, Long>{
    List<CalendarEvent> findByNameContainingIgnoreCase(String name);
    List<CalendarEvent> findByCalendar(Calendar calendar);
}

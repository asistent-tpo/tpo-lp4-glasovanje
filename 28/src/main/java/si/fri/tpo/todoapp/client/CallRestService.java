/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.client;


import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import si.fri.tpo.todoapp.dto.Bus;
import si.fri.tpo.todoapp.dto.Station;

/**
 *
 * @author urbic
 */
@Component
public class CallRestService{
    
    private static final Logger log = LoggerFactory.getLogger(CallRestService.class);
    public static String callRestService(String station){
        
        log.info("callRest");
        final String uri = "https://www.trola.si/" + station;
     
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        log.info("" + result);
        return result.getBody();
    }   
    
       
}

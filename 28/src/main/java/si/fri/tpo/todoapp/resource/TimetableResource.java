/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.resource;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.service.TimetableService;

/**
 *
 * @author Mitja
 */
@RestController
@RequestMapping("/api/timetable")
public class TimetableResource {
    
    private final TimetableService timetableService;

    @Autowired
    public TimetableResource(TimetableService timetableService) {
        this.timetableService = timetableService;
    }
    
    @GetMapping(path = "{id}")
    public Timetable get(@PathVariable(name = "id") String id) {
        return this.timetableService.findById(Long.parseLong(id));        
    }
    
    @GetMapping
    public List<Timetable> getAllAliases() {
        int randomNum = ThreadLocalRandom.current().nextInt(1, 1000);
        //timetableService.saveTimetable(new Timetable());
        return timetableService.getAllTimetables();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import si.fri.tpo.todoapp.model.Event;

/**
 *
 * @author StuZender
 */
public interface EventRepository extends JpaRepository<Event, Long> {
    
}

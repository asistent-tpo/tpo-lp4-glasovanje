/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.model.UserReg;

/**
 *
 * @author Mitja
 */
public interface TimetableCourseRepository extends JpaRepository<TimetableCourse, Long>{
    List<TimetableCourse> findByNameContainingIgnoreCase(String name);
}

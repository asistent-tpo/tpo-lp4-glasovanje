package si.fri.tpo.todoapp.dto;

import javax.validation.constraints.NotEmpty;

public class PasswordForm {

    @NotEmpty
    private String password;
    
    @NotEmpty
    private String passwordConfirmed;

    public String getPasswordConfirmed() {
        return passwordConfirmed;
    }

    public void setPasswordConfirmed(String passwordConfirmed) {
        this.passwordConfirmed = passwordConfirmed;
    }

    public PasswordForm() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PasswordForm{" + "password=" + password + '}';
    }

}

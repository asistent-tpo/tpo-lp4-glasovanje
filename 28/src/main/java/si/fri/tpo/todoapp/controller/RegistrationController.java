package si.fri.tpo.todoapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import si.fri.tpo.todoapp.dto.RegistrationForm;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.security.constraint.RegistrationFormValidator;
import si.fri.tpo.todoapp.service.UserRegService;

@Controller
public class RegistrationController {

    private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    private UserRegService userRegService;

    @Autowired
    private RegistrationFormValidator formValidator;

    @GetMapping("/registration")
    public String displayRegistration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(@ModelAttribute("registrationForm") RegistrationForm form, BindingResult bindingResult, Model model) {
//        formValidator.validate(LOG, bindingResult);
//        if (bindingResult.hasErrors()) {
//            return "registration";
//        }
        
        if(!form.getPassword().contentEquals(form.getPasswordConfirmed())) {
            String errorMessage = "Passwords do not match!";
            model.addAttribute("errorMessage", errorMessage);
        }
        else {
            UserReg userExists = userRegService.findByEmail(form.getEmail());
            if(userExists == null) {
                userRegService.saveUser(form);

                // ce je uspela registracija
                userExists = userRegService.findByEmail(form.getEmail());

                if (userExists == null) {
                    String errorMessage = "Invalid form, please try again!";
                    model.addAttribute("errorMessage", errorMessage);
                }
                else {
                    System.out.println(userExists.getEmail());
                    String successMessage = "You have registered successfully!";
                    model.addAttribute("successMessage", successMessage);
                }
            }
            else {
                System.out.println("nau slo");
                String errorMessage = "This email is already taken!";
                model.addAttribute("errorMessage", errorMessage);
            }            
        }

        
        return "registration";
    }
}

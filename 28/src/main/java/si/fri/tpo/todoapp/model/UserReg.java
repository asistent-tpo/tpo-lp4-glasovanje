package si.fri.tpo.todoapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
public class UserReg implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Email must be set")
    @Email
    @Column(unique = true)                       // Pomembno !!
    private String email;

    @NotNull(message = "Password must be set")
    private String password;

    @NotNull(message = "PasswordConfirm must be set")
    private String passwordConfirmed;

    private String name;
    private String surname;

    @ManyToMany
    @JsonIgnore
    private Set<Role> roles;

    @JsonBackReference
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "timetable_id", referencedColumnName = "id")
    private Timetable timetable;

    @JsonBackReference
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "calendar_id", referencedColumnName = "id")
    private Calendar calendar;

    @OneToMany(mappedBy = "user")
    private List<TODO> todoList;

    public UserReg() {
        this.timetable = new Timetable(this);
        this.calendar = new Calendar(this);
    }

    public UserReg(String email, String password, String passwordConfirmed) {
        this.email = email;
        this.password = password;
        this.passwordConfirmed = passwordConfirmed;
        this.timetable = new Timetable(this);
        this.calendar = new Calendar(this);
    }

    public UserReg(String email, String password, String passwordConfirmed, Set<Role> roles) {
        this.email = email;
        this.password = password;
        this.passwordConfirmed = passwordConfirmed;
        this.roles = roles;
        this.timetable = new Timetable(this);
        this.calendar = new Calendar(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public List<TODO> getTodoList() {
        return todoList;
    }

    public void setTodoList(List<TODO> todoList) {
        this.todoList = todoList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirmed;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirmed = passwordConfirm;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserReg{" + "id=" + id + ", email=" + email + ", name=" + name + ", surname=" + surname + ", password=" + password + ", passwordConfirmed=" + passwordConfirmed + '}';
    }

}

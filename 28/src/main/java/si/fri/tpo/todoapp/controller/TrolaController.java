package si.fri.tpo.todoapp.controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import si.fri.tpo.todoapp.client.CallRestService;
import si.fri.tpo.todoapp.dto.TrolaSubmit;
import si.fri.tpo.todoapp.dto.Trola;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.security.service.interfaces.AuthenticationFacade;
import si.fri.tpo.todoapp.service.TrolaService;
import si.fri.tpo.todoapp.service.UserRegService;

@Controller
public class TrolaController {

    private static final Logger LOG = LoggerFactory.getLogger(TrolaController.class);

    @Autowired
    private final TrolaService trolaService;
    private AuthenticationFacade authenticationFacade;
    private UserRegService userRegService;

    public TrolaController(TrolaService trolaService, AuthenticationFacade authenticationFacade, UserRegService userRegService) {
        this.trolaService = trolaService;
        this.authenticationFacade = authenticationFacade;
        this.userRegService = userRegService;
    }

    @GetMapping("/trole")
    public String trolaForm(Model model) {
        model.addAttribute("trolaSubmit", new TrolaSubmit());
        return "trolaSubmit";
    }

    @PostMapping("/trole")
    public String trolaSubmit(@ModelAttribute TrolaSubmit trolaSubmit) throws IOException {
        LOG.info(trolaSubmit.getStation());
        String data = CallRestService.callRestService(trolaSubmit.getStation());
        Trola trola = trolaService.fromJsonString(data);
        LOG.info("" + trola.isError());
        trolaSubmit.setData(trola);
        return "trolaShow";
    }
    
    @GetMapping("/troleP")
    public String trolaFormP(Model model) {
        model.addAttribute("trolaSubmit", new TrolaSubmit());
        return "trolaSubmitP";
    }

    @PostMapping("/troleP")
    public String trolaSubmitP(@ModelAttribute TrolaSubmit trolaSubmit) throws IOException {
        LOG.info(trolaSubmit.getStation());
        String data = CallRestService.callRestService(trolaSubmit.getStation());
        Trola trola = trolaService.fromJsonString(data);
        LOG.info("" + trola.isError());
        trolaSubmit.setData(trola);
        return "trolaShowP";
    }
}

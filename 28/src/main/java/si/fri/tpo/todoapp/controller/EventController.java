/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import si.fri.tpo.todoapp.model.Event;
import si.fri.tpo.todoapp.service.EventService;

/**
 *
 * @author StuZender
 */

@Controller
public class EventController {
    
    @Autowired
    private EventService eventService;
    
    @RequestMapping("/userEvents")
    public String showUserEventsPage(Model model) {
        model.addAttribute("events", eventService.getAllEvents());
        return "userEvents.html";
    }
    
    @RequestMapping("/plannerEvents")
    public String showPlannerEventsPage(Model model) {
        model.addAttribute("events", eventService.getAllEvents());
        return "plannerEvents";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/plannerEvents")
    public String addEvent(@RequestParam("name") String name,
                           @RequestParam("date") String date,
                           @RequestParam("organizer") String organizer,
                           @RequestParam("description") String description,
                            Model model) {
        
        if(name == null || name.length() > 255 || name.length() == 0 ||
           date == null || date.length() > 255 || date.length() == 0 ||
           organizer == null || organizer.length() > 255 || organizer.length() == 0 ||
           description == null || description.length() > 255 || description.length() == 0) {
        
            return "redirect:/plannerEvents";
        }
        
        Event event = new Event(name, date, organizer, description);
        eventService.addEvent(event);
        model.addAttribute("events", eventService.getAllEvents());
        return "plannerEvents";
    }
}

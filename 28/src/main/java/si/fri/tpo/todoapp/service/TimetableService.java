/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.repository.TimetableRepository;

/**
 *
 * @author Mitja
 */
@Service
public class TimetableService {
    private final TimetableRepository timetableRepository;
    
    @Autowired
    public TimetableService(TimetableRepository timetableRepository) {
        this.timetableRepository = timetableRepository;
    }
    
    public Timetable findById(Long id) {
        return this.timetableRepository.findById(id).orElse(null);
    }
    
    public Timetable findByUserId(Long id) {
        return this.timetableRepository.findById(id).orElse(null);
    }
    
    public List<Timetable> getAllTimetables() {
        return this.timetableRepository.findAll();
    }

    @Transactional
    public Timetable saveTimetable(Timetable timetable) {
        return this.timetableRepository.save(timetable);
    }
}

package si.fri.tpo.todoapp;

import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import si.fri.tpo.todoapp.model.Role;
import si.fri.tpo.todoapp.model.RoleEnum;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.repository.RoleRepository;
import si.fri.tpo.todoapp.repository.UserRegRepository;

@Component
public class DbDataInitializer implements CommandLineRunner {

    @Autowired
    private UserRegRepository userRegRepositorty;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void run(String... args) throws Exception {

        // roles {STUDENT, ADMIN, EVENT_MANAGER} added
        Role roleStudent = new Role(RoleEnum.STUDENT.toString());
        roleRepository.save(roleStudent);
        Role roleAdmin = new Role(RoleEnum.ADMIN.toString());
        roleRepository.save(roleAdmin);
        Role roleEventManager = new Role(RoleEnum.EVENT_MANAGER.toString());
        roleRepository.save(roleEventManager);

        //------ users ---------------------------------------------------------------------------------
        // admin
        String passwordAdminEncoded = bCryptPasswordEncoder.encode("admin");
        UserReg userAdmin = new UserReg("admin@gmail.com", passwordAdminEncoded, passwordAdminEncoded);
        Set<Role> adminRolesSet = new HashSet<>();
        adminRolesSet.add(roleRepository.findOneByName(RoleEnum.ADMIN.toString()));
        userAdmin.setRoles(adminRolesSet);
        userRegRepositorty.save(userAdmin);
        // end admin
        // student
        String passwordStudentEncoded = bCryptPasswordEncoder.encode("student");
        UserReg userStudent = new UserReg("student@gmail.com", passwordStudentEncoded, passwordStudentEncoded);
        Set<Role> studentRolesSet = new HashSet<>();
        studentRolesSet.add(roleRepository.findOneByName(RoleEnum.STUDENT.toString()));
        userStudent.setRoles(studentRolesSet);
        userRegRepositorty.save(userStudent);
        // end student
        // event_manager
        String passwordEventManagerEncoded = bCryptPasswordEncoder.encode("eventmanager");
        UserReg userEventManager = new UserReg("eventmanager@gmail.com", passwordEventManagerEncoded, passwordEventManagerEncoded);
        Set<Role> eventManagerRolesSet = new HashSet<>();
        eventManagerRolesSet.add(roleRepository.findOneByName(RoleEnum.EVENT_MANAGER.toString()));
        userEventManager.setRoles(eventManagerRolesSet);
        userRegRepositorty.save(userEventManager);
        // end event_manager

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Mitja
 */
@Entity
public class Calendar {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @JsonManagedReference
    @OneToMany(mappedBy = "calendar")
    private List<CalendarEvent> events;
    
    @JsonManagedReference
    @OneToOne(mappedBy = "calendar")
    private UserReg user;
    
    public Calendar(UserReg user){
        this.user = user;
    }

    public Calendar() {
    }

    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the events
     */
    public List<CalendarEvent> getEvents() {
        return events;
    }

    /**
     * @param events the events to set
     */
    public void setEvents(List<CalendarEvent> events) {
        this.events = events;
    }

    /**
     * @return the user
     */
    public UserReg getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserReg user) {
        this.user = user;
    }
    
    public void addEvent(CalendarEvent event) {
        this.events.add(event);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import si.fri.tpo.todoapp.model.AdminMessage;
import si.fri.tpo.todoapp.service.AdminMessageService;

/**
 *
 * @author StuZender
 */

@Controller
public class AdminController {
    
    @Autowired
    private AdminMessageService adminMessageService;
    
    @RequestMapping("/adminPanel") // glavna in edina stran od admina
    public String showAdminPanel(Model model) {
        model.addAttribute("messages", adminMessageService.getAllMessages());
        return "adminPanel.html";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/adminPanel")
    public String addAdminMessage(@RequestParam("message") String message,
                            Model model) {
        
        
        if(message == null || message.length() > 512 || message.length() == 0) {
            return "redirect:/adminPanel";
        }
        
        AdminMessage newAdminMessage = new AdminMessage(message);
        adminMessageService.addMessage(newAdminMessage);
        
        return "redirect:/adminPanel";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import si.fri.tpo.todoapp.model.Timetable;
import si.fri.tpo.todoapp.model.TimetableCourse;
import si.fri.tpo.todoapp.service.TimetableCourseService;
import si.fri.tpo.todoapp.service.TimetableService;
import si.fri.tpo.todoapp.service.UserRegService;

/**
 *
 * @author Mitja
 */
@Controller
public class TimetableController {
    @Autowired
    private TimetableService timetableService;
    private TimetableCourseService timetableCourseService;
    private UserRegService userService;
        private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);
        
    @Autowired
    public TimetableController(UserRegService userService, TimetableCourseService timetableCourseService) {
        this.userService = userService;
        this.timetableCourseService = timetableCourseService;
    }
    
    // ?? Okej i guess it works
    @RequestMapping(path = "/{id}/timetable", produces = MediaType.APPLICATION_JSON_VALUE)
    public String showUserTimetable(@PathVariable(name = "id") String id, Model model, @ModelAttribute("isError") String checkError) {
        Timetable tempTimetable = userService.getUserTimetable(Long.parseLong(id));
        //model.addAttribute("timetable", userService.getUserTimetable(Long.parseLong(id)));
        model.addAttribute("isError", checkError+"");
        model.addAttribute("courses", tempTimetable.getCourses());
        return "timetable.html";
    }
    
    @RequestMapping(method=RequestMethod.POST, path = "/{id}/timetable")
    public String saveCourse(@PathVariable(name = "id") String id, RedirectAttributes checkError, @RequestParam String courseName, @RequestParam int courseDuration, @RequestParam String courseColour, @RequestParam String  courseDay, @RequestParam String  courseHour, Model model) {
        String [] arrayH = courseHour.split(":");
        int hour = Integer.parseInt(arrayH[0]);
        
        if(hour + courseDuration > 16){ // more bit 1 več ker 15:00 + 1 is still viable
            checkError.addFlashAttribute("isError", "true");
            return "redirect:/1/timetable";
        }else{
            Timetable tempTimetable = userService.getUserTimetable(Long.parseLong(id));
            List <TimetableCourse> tempList = tempTimetable.getCourses();
            for(TimetableCourse element : tempList){
                String [] timeArray = element.getHour().split(":");
                int timeHour = Integer.parseInt(timeArray[0]); //ura predmeta
                
                for(int i = 0; i < courseDuration; i++){
                    int comparableHour = hour + i;
                    if(comparableHour == timeHour && element.getDay().equals(courseDay)){
                        LOG.info("Time error: " + comparableHour + "  existing course: " + timeHour);
                         checkError.addFlashAttribute("isError", "true");
                         return "redirect:/1/timetable";
                    }
                }
            }
        }
        
        //If all checks out, loop and make courses
        for(int j = 0; j < courseDuration; j++){
            String adjustableHour = (hour+j) + ":00";
            this.userService.saveTimetableCourse(Long.parseLong(id), courseName, 1, courseColour, courseDay, adjustableHour);
            
            LOG.info(id + " " + courseName + " " + courseColour + " " + courseDay + " " + adjustableHour);
        }
        return "redirect:/1/timetable";
    }
    
    @RequestMapping(method=RequestMethod.GET, path="/timetable/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteCourse(@RequestParam("courseId") Long courseId) {
        this.timetableCourseService.deleteCourseById(courseId);
        return "redirect:/1/timetable";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/timetable/edit")
    public String editCourse(@RequestParam Long courseId, RedirectAttributes checkError,
                                        @RequestParam String courseName,  
                                        @RequestParam String courseColour, 
                                        @RequestParam String  courseDay, 
                                        @RequestParam String  courseHour) {
        String [] arrayH = courseHour.split(":");
        int hour = Integer.parseInt(arrayH[0]);
        int courseDuration = 1;
        
        if(hour + courseDuration > 16){ // more bit 1 več ker 15:00 + 1 is still viable
            checkError.addFlashAttribute("isError", "true");
            return "redirect:/1/timetable";
        }else{
            Long enka = new Long(1);
            Timetable tempTimetable = userService.getUserTimetable(enka);//TODO: HARDCODED! Ko bos prestavljal vse delaj z string email!
            List <TimetableCourse> tempList = tempTimetable.getCourses();
            for(TimetableCourse element : tempList){
                String [] timeArray = element.getHour().split(":");
                int timeHour = Integer.parseInt(timeArray[0]); //ura predmeta
                
                for(int i = 0; i < courseDuration; i++){
                    int comparableHour = hour + i;
                    //pogledamo da ne primerjamo ure samo z sabo
                    if(courseId != element.getId()){
                        //LOG.info("Nov čas: " + comparableHour + " Nov dan: " + courseDay + " " +  "||||         Obstoječ čas: " + timeHour + " Obstoječ dan " + element.getDay());
                        if(comparableHour == timeHour && element.getDay().equals(courseDay)){
                             checkError.addFlashAttribute("isError", "true");
                             return "redirect:/1/timetable";
                        }
                    }
                }
            }
        }
        
        
        LOG.info(courseId + " " + courseName + " " + courseColour + " " + courseDay + " " + courseHour);
        this.timetableCourseService.updateCourse(courseId, courseName, courseDuration, courseColour, courseDay, courseHour);
        return "redirect:/1/timetable";
    }
}

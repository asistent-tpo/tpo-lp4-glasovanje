/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import si.fri.tpo.todoapp.model.Timetable;

/**
 *
 * @author Mitja
 */
@Repository
public interface TimetableRepository extends JpaRepository<Timetable, Long>{
    List<Timetable> findByUserId(Long user);
}

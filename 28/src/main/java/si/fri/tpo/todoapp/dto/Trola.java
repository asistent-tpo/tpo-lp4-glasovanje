/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.dto;

/**
 *
 * @author urbic
 */

import java.util.*;
import com.fasterxml.jackson.annotation.*;
import si.fri.tpo.todoapp.dto.Station;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Trola {
    private String error;
    private Station[] stations;

    @JsonProperty("stations")
    public Station[] getStations() { return stations; }
    @JsonProperty("stations")
    public void setStations(Station[] value) { this.stations = value; }
    
    @JsonProperty("error")
    public String getError() { return error; }
    @JsonProperty("error")
    public void setError(String error) { this.error = error; }
    
    
    public String toString(){
        String res = "Station: ";
        for(Station station : stations){
            res += station.getName() ;            
            for(Bus bus : station.getBuses()){
                res+= "Bus: " + bus.getNumber() + " Direction: " + bus.getDirection() + " Arrivals: ";                
                for(Long arrival : bus.getArrivals()){
                    res += arrival + " ";
                }
            }
        }
        return res;
    }
    
    public String getStation(){
        String res = "Station: ";
        for(Station station : stations){
            res += station.getName() ;            
        }
        return res;
    }
    
    public boolean isError(){
        if(error == null){
            return false;
        }
        return true;
    }
}

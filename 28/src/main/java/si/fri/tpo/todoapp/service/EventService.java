/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.model.Event;
import si.fri.tpo.todoapp.repository.EventRepository;

/**
 *
 * @author StuZender
 */

@Service
public class EventService {
    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }
    
    public List<Event> getAllEvents() {
        return this.eventRepository.findAll();
    }

    public Event addEvent(Event event) {
        return this.eventRepository.save(event);
    }
}

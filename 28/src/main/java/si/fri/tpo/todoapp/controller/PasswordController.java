package si.fri.tpo.todoapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import si.fri.tpo.todoapp.dto.PasswordForm;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.security.service.interfaces.AuthenticationFacade;
import si.fri.tpo.todoapp.service.UserRegService;

@Controller
public class PasswordController {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordController.class);

    @Autowired
    private UserRegService userRegService;

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @GetMapping("/password")
    public String displayPasswordChangeForm() {
        return "changePassword";
    }

    @PostMapping("/password")
    public String changePassword(RedirectAttributes checkError, @ModelAttribute("passwordForm") PasswordForm form, BindingResult bindingResult, Model model) {

        if(!form.getPassword().contentEquals(form.getPasswordConfirmed())) {
            String errorMessage = "Passwords do not match!";
            model.addAttribute("errorMessage", errorMessage);
            return "changePassword";
        }
        else if(form.getPassword().length() < 8) {
            String errorMessage = "Password is too short";
            model.addAttribute("errorMessage", errorMessage);
            return "changePassword";
        }
        else {
           try {
               String userEmail = authenticationFacade.getAuthentication().getName();
               UserReg user = userRegService.findByEmail(userEmail);
               user.setPassword(form.getPassword());
               userRegService.updateUser(user);

           } catch (Exception ex) {
               checkError.addFlashAttribute("isError", "true");
               return "redirect:password";
           }
           return "redirect:logout";   // logout => deleteCookie => login   
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import si.fri.tpo.todoapp.service.AdminMessageService;

/**
 *
 * @author StuZender
 */

@Controller
public class AdminMessagesController {
    
    private AdminMessageService adminMessageService;
    
    @Autowired
    public AdminMessagesController(AdminMessageService adminMessageService) {
        this.adminMessageService = adminMessageService;
    }
    
    @RequestMapping("/adminmessages") // pregled adminovih sporočil
    public String showAdminMessages(Model model) {
        model.addAttribute("messages", adminMessageService.getAllMessages());
        return "adminmessages.html";
    }
    
    @RequestMapping("/messagesmanager") // pregled adminovih sporočil
    public String showManagerMessages(Model model) {
        model.addAttribute("messages", adminMessageService.getAllMessages());
        return "adminMessagesManager.html";
    }
}

package si.fri.tpo.todoapp.security.service;

import si.fri.tpo.todoapp.security.service.interfaces.AuthenticationFacade;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFacadeImpl implements AuthenticationFacade {

    public AuthenticationFacadeImpl() {
        super();
    }

    @Override
    public final Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}

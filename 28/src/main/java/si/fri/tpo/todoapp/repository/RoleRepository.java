package si.fri.tpo.todoapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import si.fri.tpo.todoapp.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

    public Role findOneByName(String roleName);
}

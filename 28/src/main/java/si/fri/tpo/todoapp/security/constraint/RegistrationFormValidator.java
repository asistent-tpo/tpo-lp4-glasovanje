package si.fri.tpo.todoapp.security.constraint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import si.fri.tpo.todoapp.dto.RegistrationForm;
import si.fri.tpo.todoapp.model.UserReg;
import si.fri.tpo.todoapp.service.UserRegService;

@Component
public class RegistrationFormValidator implements Validator {

    @Autowired
    private UserRegService userRegService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserReg.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegistrationForm form = (RegistrationForm) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (form.getEmail().length() < 4 || form.getEmail().length() > 64) {
            errors.rejectValue("email", "Size.userForm.username");
        }
        if (userRegService.findByEmail(form.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (form.getPassword().length() < 3 || form.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        if (!form.getPasswordConfirmed().equals(form.getPassword())) {
            errors.rejectValue("passwordConfirmed", "Diff.userForm.passwordConfirm");
        }
    }
}

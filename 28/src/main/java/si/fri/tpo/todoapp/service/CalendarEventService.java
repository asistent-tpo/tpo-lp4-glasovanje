/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.fri.tpo.todoapp.controller.HelloController;
import si.fri.tpo.todoapp.model.Calendar;
import si.fri.tpo.todoapp.model.CalendarEvent;
import si.fri.tpo.todoapp.repository.CalendarEventRepository;

/**
 *
 * @author Urban
 */
@Service
public class CalendarEventService {
    private final CalendarEventRepository calendarEventRepository;
    private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);
    
    @Autowired
    public CalendarEventService(CalendarEventRepository calendarEventRepository) {
        this.calendarEventRepository = calendarEventRepository;
    }
    
    public CalendarEvent findById(Long id) {
        return this.calendarEventRepository.findById(id).orElse(null);
    }

    @Transactional
    public void deleteEventById(Long id) {
        LOG.info("Delete");
        calendarEventRepository.deleteById(id);
    }
    
    @Transactional
    public void updateEvent(Long eventId, String name, String duration, String time, String description, String date) {
        CalendarEvent calendarEvent = calendarEventRepository.getOne(eventId);
        calendarEvent.setName(name);
        calendarEvent.setDuration(duration);
        calendarEvent.setTime(time);
        calendarEvent.setDescription(description);
        calendarEvent.setDate(date);
        calendarEventRepository.save(calendarEvent);
    }
    
    @Transactional
    public CalendarEvent addEvent(CalendarEvent event) {
        return this.calendarEventRepository.save(event);
    }
    
    public List<CalendarEvent> findAllByCalendar(Calendar calendar) {
        return this.calendarEventRepository.findByCalendar(calendar);
    }
}

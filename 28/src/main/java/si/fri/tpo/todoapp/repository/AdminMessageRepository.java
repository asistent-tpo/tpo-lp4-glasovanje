/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import si.fri.tpo.todoapp.model.AdminMessage;

/**
 *
 * @author StuZender
 */
public interface AdminMessageRepository extends JpaRepository<AdminMessage, Long> {
    
    List<AdminMessage> findByMessage(String message);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Mitja
 */
@Entity
@Table(name="Timetable")
public class Timetable implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @JsonManagedReference
    @OneToMany(mappedBy = "timetable")
    private List<TimetableCourse> courses;
    
    @JsonManagedReference
    @OneToOne(mappedBy = "timetable")
    private UserReg user;
    
    public Timetable(){
    }
    
    public Timetable(UserReg user){
        this.user = user;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

     /**
     * @return the courses
     */
    public List<TimetableCourse> getCourses() {
        return courses;
    }

    /**
     * @param courses the courses to set
     */
    public void setCourses(List<TimetableCourse> courses) {
        this.courses = courses;
    }
    
    public void addCourse(TimetableCourse course) {
        this.courses.add(course);
    }
    
    /**
     * @return the user
     */
    public UserReg getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserReg user) {
        this.user = user;
    }
}

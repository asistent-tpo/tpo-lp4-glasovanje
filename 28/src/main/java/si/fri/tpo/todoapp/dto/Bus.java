/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package si.fri.tpo.todoapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mitja
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bus {
    private String direction;
    private String number;
    private long[] arrivals;

    @JsonProperty("direction")
    public String getDirection() { return direction; }
    @JsonProperty("direction")
    public void setDirection(String value) { this.direction = value; }

    @JsonProperty("number")
    public String getNumber() { return number; }
    @JsonProperty("number")
    public void setNumber(String value) { this.number = value; }

    @JsonProperty("arrivals")
    public long[] getArrivals() { return arrivals; }
    @JsonProperty("arrivals")
    public void setArrivals(long[] value) { this.arrivals = value; }
    
    public String toString(){
        String res = "";
        res+= "Bus: " + number + " Direction: " + direction;                
        
        return res;
    }
    
    public String arrivalsToString(){
        String res = "Arrivals: ";
        for(Long arrival : arrivals){
            res += arrival + " ";
        }
        return res;
    }   

    public Bus(String direction, String number) {
        this.direction = direction;
        this.number = number;
    }

    public Bus() {
    }
    
}

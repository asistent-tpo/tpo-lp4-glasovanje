package si.fri.tpo.todoapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import si.fri.tpo.todoapp.model.UserReg;

public interface UserRegRepository extends JpaRepository<UserReg, Long> {

    UserReg findByEmail(String email);    //
}

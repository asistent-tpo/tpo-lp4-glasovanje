# 11. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Navadni uporabnik: račun: testuser | geslo:12345678

Uporabnik z vlogami admin in urejevalec dogodkov - uporabniški račun: testEventsAdmin | geslo: adminadmin

Povezava do commita: https://bitbucket.org/NameBrez/tpo-lp4/commits/04082e7116a3c2ffdefb0b2b6ef30f8e7c0a62d7?at=production

Povezava do spletne aplikacije: https://straight-ass.herokuapp.com

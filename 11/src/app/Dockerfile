### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:8-alpine as builder

ENV appDir /app
ENV HOST 0.0.0.0
RUN apk add --no-cache git

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
COPY package.json package-lock.json ./
RUN npm ci --no-save --no-cache --no-progress && mkdir ${appDir} && cp -R ./node_modules .${appDir}

WORKDIR ${appDir}

COPY ./assets/ ./assets
COPY ./config/ ./config
COPY ./components/ ./components
COPY ./pages/ ./pages
COPY ./enums/ ./enums
COPY ./layouts/ ./layouts
COPY ./middleware/ ./middleware
COPY ./mixins/ ./mixins
COPY ./plugins/ ./plugins
COPY ./static/ ./static
COPY ./store/ ./store
COPY ./package.json ./package.json
COPY ./nuxt.config.js ./nuxt.config.js

# Build the app

RUN $(npm bin)/nuxt build

EXPOSE 3000


CMD [ "npm", "start" ]

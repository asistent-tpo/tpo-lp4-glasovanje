import { isEmail } from "../src/lib/validators";
import { expect } from 'chai';
import 'mocha';

describe('isEmail function', () => {
  it('should return true', () => {
    const result = isEmail("test@domain.com")
    expect(result).to.equal(true);
  });

  it('should return false', () => {
    const result = isEmail("notAValidEmailString")
    expect(result).to.equal(false);
  });
});

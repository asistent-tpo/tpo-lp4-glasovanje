# 24. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

https://bitbucket.org/alenjursic1997/tpo-lp4/commits/dc32bae6d0703f99d8c6abd586fd304409029d17

https://straigh-as.herokuapp.com/

Testni uporabniki:

- študent:   user1@tpo.si

- profesor: user2@tpo.si

- admin:     user3@tpo.si

Geslo: Geslo123

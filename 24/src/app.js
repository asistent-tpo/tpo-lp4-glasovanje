// set up ======================================================================
// get all the tools we need
//require('dotenv').load();

var createError = require('http-errors');
var express = require('express');
var expressSession = require('express-session');
var app = express();
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var uglifyJs = require('uglify-js');
var fs = require('fs');

var zdruzeno = uglifyJs.minify({
  'app.js': fs.readFileSync('app_client/app.js', 'utf-8'),
  'prijava.krmilnik.js': fs.readFileSync('app_client/krmilniki/prijava/prijava.krmilnik.js', 'utf-8'),
  'main.krmilnik.js': fs.readFileSync('app_client/krmilniki/main/main.krmilnik.js', 'utf-8'),
  'dogodki.krmilnik.js': fs.readFileSync('app_client/krmilniki/dogodki/dogodki.krmilnik.js', 'utf-8'),
  'dogodek.krmilnik.js': fs.readFileSync('app_client/krmilniki/dogodki/dogodek.krmilnik.js', 'utf-8'),
  'registracija.krmilnik.js': fs.readFileSync('app_client/krmilniki/registracija/registracija.krmilnik.js', 'utf-8'),
  'dogodki.storitev.js': fs.readFileSync('app_client/skupno/storitve/dogodki.storitev.js', 'utf-8'),
  'obvestila.storitev.js': fs.readFileSync('app_client/skupno/storitve/obvestila.storitev.js', 'utf-8'),
  'todos.storitev.js': fs.readFileSync('app_client/skupno/storitve/todos.storitev.js', 'utf-8'),
  'avtentikacija.storitev.js': fs.readFileSync('app_client/skupno/storitve/avtentikacija.storitev.js', 'utf-8'),
  'navigacija.direktiva.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.direktiva.js', 'utf-8'),
  'navigacija.krmilnik.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.krmilnik.js', 'utf-8'),
  'uporabnik.krmilnik.js': fs.readFileSync('app_client/krmilniki/uporabnik/uporabnik.krmilnik.js', 'utf-8'),
  'obvestila.krmilnik.js': fs.readFileSync('app_client/krmilniki/obvestila/obvestila.krmilnik.js', 'utf-8'),
  'formatirajDatum.filter.js': fs.readFileSync('app_client/skupno/filtri/formatirajDatum.filter.js', 'utf-8'),
});

fs.writeFile('public/angular/straightas.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "straightas.min.js".');
});


// configuration ===============================================================
require('./app_api/models/db');
require('./app_api/config/passport');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(expressSession({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));


// router files ===============================================================
var apiRouter = require('./app_api/routes/index');

app.use('/api', apiRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

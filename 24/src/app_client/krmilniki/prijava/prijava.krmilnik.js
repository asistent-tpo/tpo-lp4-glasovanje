(function() {
  function prijavaCtrl($location, avtentikacija) {
    var vm = this;

    vm.prijavniPodatki = {
      username: "",
      password: ""
    };

    vm.prvotnaStran = '/';

    vm.posiljanjePodatkov = function() {
      vm.izvediPrijavo();
    };

    vm.izvediPrijavo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .prijava(vm.prijavniPodatki)
        .then(
          function(success) {
            //$location.search('stran', null);
            $location.path(vm.prvotnaStran);
          },
          function(napaka) {
            vm.napakaNaObrazcu = napaka.data.sporocilo;
          }
        );
    };
  }
  prijavaCtrl.$inject = ['$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('straightas')
    .controller('prijavaCtrl', prijavaCtrl);
})();
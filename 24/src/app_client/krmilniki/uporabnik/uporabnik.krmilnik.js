(function() {
  function uporabnikCtrl($scope, $location, avtentikacija) {
    var vm = this;

    vm.spremembaGeslaPodatki = {
        id: "",
        newpass: "",
        password: "",
        confpass: ""
    };
    
    var resetForms = function() {
        vm.spremembaGeslaPodatki  = {
            id: "",
            newpass: "",
            password: "",
            confpass: ""
        };
    };

    vm.spremeniGeslo = function() {
        vm.izvediSprememboGesla();
    };
  
    vm.izvediSprememboGesla = function() {
      vm.spremembaGeslaPodatki.id = avtentikacija.myId();
      avtentikacija
        .spremeniGeslo(vm.spremembaGeslaPodatki)
        .then(
          function(success) {
            //$location.search('stran', null);
            angular.element('#spremeni-geslo-modal').modal('hide');
            resetForms();
            console.log("PRAVILNO");
            //$location.path(vm.prvotnaStran);
          },
          function(napaka) {
            vm.napakaNaObrazcu = napaka.data.sporocilo;
            console.log("NAPACNO");
          }
        );
    };
  }
  uporabnikCtrl.$inject = ['$scope', '$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('straightas')
    .controller('uporabnikCtrl', uporabnikCtrl);
})();
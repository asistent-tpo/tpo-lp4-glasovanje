(function() {
  function registracijaCtrl($location, avtentikacija) {
    var vm = this;

    vm.prijavniPodatki = {
      email: "",
      password: "",
      confpass: ""
    };
    
    vm.prvotnaStran = '/prijava';

    vm.posiljanjePodatkov = function() {
        vm.izvediRegistracijo();
    };
  
    vm.izvediRegistracijo = function() {
      avtentikacija
        .registracija(vm.prijavniPodatki)
        .then(
          function(success) {
            //$location.search('stran', null);
            $location.path(vm.prvotnaStran);
          },
          function(napaka) {
            vm.napakaNaObrazcu = napaka.data.sporocilo;
          }
        );
    };
  }
  registracijaCtrl.$inject = ['$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('straightas')
    .controller('registracijaCtrl', registracijaCtrl);
})();
(function() {

    function mainCtrl($scope, dogodki, avtentikacija, $location, todos) {
        var vm = this;

        vm.vloga = avtentikacija.vloga();

        var resetForms = function() {
            vm.newDogodek  = {
                naziv: "",
                datum: "",
                organizator: "",
                opis: ""
            };
            vm.newTodo  = {
                user: "",
                opis: ""
            };

            vm.editTodo = {
                user: "",
                opis: "",
            }
        };

        resetForms();

        var editTodoId;


        vm.ustvari = function() {
            console.log("ustvari");
            dogodki.create(vm.dogodek).then(function success(odgovor) {
                console.log("ODGOVOR NA CREATE DOGODEK", odgovor);
                angular.element('#dodaj-dogodek-modal').modal('hide');
                vm.events.push({
                    id: odgovor.data._id,
                    title: vm.dogodek.naziv,
                    start: new Date(vm.dogodek.datum),
                    end: vm.dogodek.datumDo ? new Date(vm.dogodek.datumDo) : undefined
                });
                resetForms();
            }, function error(odgovor) {
                console.log(odgovor.e);
            });
        };

        vm.createTODO = function() {
            //console.log(avtentikacija.myId());
            vm.newTodo.user = avtentikacija.myId();

            todos.create(vm.newTodo).then(function success(odgovor) {
                vm.todos.push({
                    id: odgovor.data._id,
                    opis: odgovor.data.opis
                });
                console.log(vm.todos);
                angular.element('#dodaj-TODO-modal').modal('hide');
                resetForms();
            }, function error(odgovor) {
                console.log(odgovor.e);
            });
        };

        vm.editTODO = function() {
            console.log("edit todo", vm.editTodo);

            todos.update(editTodoId, vm.editTodo).then(function success(odgovor) {
                removeTodoFromList(editTodoId);
                console.log("ODGOVOR", odgovor.data);
                vm.todos.push({
                    id: odgovor.data._id,
                    opis: odgovor.data.opis
                });
                console.log(vm.todos);
                angular.element('#uredi-TODO-modal').modal('hide');
                resetForms();
            }, function error(odgovor) {
                console.log(odgovor.e);
            });
        }


        vm.deleteTodo = function(id) {
            todos.remove(id).then(function success(odgovor) {
                removeTodoFromList(id);
            }, function error(odgovor) {
                console.log(odgovor.e);
            });
        }

        vm.showEdit = function(id) {
            console.log("click todo");
            editTodoId = id;
            vm.editTodo.user = avtentikacija.myId();
            todos.getTodo(id).then(
                function success(odgovor) {
                    console.log('success', odgovor);
                    vm.editTodo.opis = odgovor.data.opis;
                    angular.element('#uredi-TODO-modal').modal('show');
                }, function error(odgovor) {
                    console.log('error', odgovor);
                }
            );
        };

        var refreshCalendar = function (start, end) {
            dogodki.getEvents(start, end).then(function (odgovor) {
                console.log('getEventsFromMonth success', odgovor);
                odgovor.data.forEach(function (d) {
                    // console.log('title:', d.naziv);
                    vm.events.push({
                        id: d._id,
                        title: d.naziv,
                        start: new Date(d.datum),
                        end: d.datumDo ? new Date(d.datumDo) : undefined
                    });
                });
            }, function (odgovor) {
                console.log('getEventsFromMonth error', odgovor);
            });
        };

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        /* event source that contains custom events on the scope */
        vm.events = [];

        vm.eventSources = [vm.events];

        $scope.alertOnEventClick = function(date, jsEvent, view){
            console.log(date);
            $location.path('/dogodki/' + date.id);
        };

        /* config calendar object */
        $scope.uiConfig = {
            calendar:{
                height: 450,
                editable: true,
                header:{
                    left: 'title',
                    center: '',
                    right: 'today prev,next'
                },
                eventClick: $scope.alertOnEventClick,
                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                eventRender: $scope.eventRender,
                viewRender: function(view, element) {
                    console.log(view.start._d, new Date(view.end._d).toString());
                    refreshCalendar(view.start._d, view.end._d);
                }
            }
        };

        // seznam todojev od trenutnega uporabnika
        vm.todos = [];

        todos.getTodosForUser({user: avtentikacija.myId()})
        .then(function (odgovor) {
            console.log('getTodosForUser success', odgovor);
            odgovor.data.forEach(function (d) {
                // console.log('title:', d.naziv);
                vm.todos.push({
                    id: d._id,
                    opis: d.opis,
                });
            });
        }, function (odgovor) {
            console.log('getEventsFromMonth error', odgovor);
        });

        var removeTodoFromList = function(id) {
            //console.log(id);
            for (var i = 0; i < vm.todos.length; i++) {
                //console.log(vm.todos[i].id === id);
                if (vm.todos[i].id === id) {
                    console.log(vm.todos[i].opis);
                    vm.todos.splice(i, 1);
                }
            }
        }
    }
  
    mainCtrl.$inject = ['$scope', 'dogodki', 'avtentikacija', '$location', 'todos'];
  
    /* global angular */
    angular
        .module('straightas')
        .controller('mainCtrl', mainCtrl);
    
})();
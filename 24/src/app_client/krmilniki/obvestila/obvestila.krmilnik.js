(function() {

    function obvestilaCtrl($scope, avtentikacija, $location, obvestila) {
        var vm = this;

        vm.vloga = avtentikacija.vloga();

        vm.obvestila = [];
        vm.action = 'ustvari';

        obvestila.getObvestila(vm.vloga == 3 ? undefined : true).then(
            function success(odgovor) {
                console.log('success', odgovor);
                vm.obvestila = odgovor.data;
            }, function error(odgovor) {
                console.log('error', odgovor);
            }
        );

        vm.prikazi = function (id) {
            vm.action = 'prikazi';
            if (vm.vloga != 3) {
                return;
            }
            obvestila.getObvestilo(id).then(
                function success(odgovor) {
                    console.log('success', odgovor);
                    vm.obvestilo = odgovor.data;
                    angular.element('#dodaj-obvestilo-modal').modal('show');
                }, function error(odgovor) {
                    console.log('error', odgovor);
                }
            );
        };

        vm.dodaj = function() {
            vm.action = 'ustvari';
            vm.obvestilo = {};
        };

        vm.ustvari = function(valid) {
            if (valid) {
                console.log(vm.action);
                if (vm.action == 'ustvari') {
                    vm.obvestilo.datum = new Date();
                    obvestila.createObvestilo(vm.obvestilo).then(
                        function success(odgovor) {
                            console.log('success', odgovor);
                            vm.obvestila.unshift(vm.obvestilo);
                            angular.element('#dodaj-obvestilo-modal').modal('hide');
                        }, function error(odgovor) {
                            console.log('error', odgovor);
                        }
                    );
                } else if (vm.action == 'prikazi') {
                    obvestila.updateObvestilo(vm.obvestilo._id, vm.obvestilo).then(
                        function success(odgovor) {
                            console.log('success', odgovor);
                            for (var i = 0; i < vm.obvestila.length; i++) {
                                if (vm.obvestila[i]._id == vm.obvestilo._id) {
                                    vm.obvestila[i] = vm.obvestilo;
                                }
                            }
                            angular.element('#dodaj-obvestilo-modal').modal('hide');
                        }, function error(odgovor) {
                            console.log('error', odgovor);
                        }
                    );
                }
            }
        };

        vm.opozoriloIzbris = function() {
            console.log('opozoriloIzbris');
            angular.element('#dodaj-obvestilo-modal').modal('hide');
            angular.element('#izbrisi-obvestilo-modal').modal('show');
        };

        vm.izbrisi = function (id) {
            obvestila.deleteObvestilo(id).then(
                function success(odgovor) {
                    console.log('success', odgovor);
                    for (var i = 0; i < vm.obvestila.length; i++) {
                        if (vm.obvestila[i]._id == vm.obvestilo._id) {
                            vm.obvestila.splice(i, 1);
                        }
                    }
                    angular.element('#izbrisi-obvestilo-modal').modal('hide');
                }, function error(odgovor) {
                    console.log('error', odgovor);
                }
            );
        };

    }

    obvestilaCtrl.$inject = ['$scope', 'avtentikacija', '$location', 'obvestila'];

    /* global angular */
    angular
        .module('straightas')
        .controller('obvestilaCtrl', obvestilaCtrl);

})();
(function() {

    function dogodkiCtrl($scope, dogodki, avtentikacija, $location, todos) {
        var vm = this;

        vm.vloga = avtentikacija.vloga();

        vm.dogodki = [];

        dogodki.getEventsFromMonth(undefined).then(function (odgovor) {
            console.log('getEventsFromMonth success', odgovor);
            vm.dogodki = odgovor.data;
            console.log('getEventsFromMonth success', vm.events);
        }, function (odgovor) {
            console.log('getEventsFromMonth error', odgovor);
        });

        vm.prikazi = function (id) {
            $location.path('/dogodki/' + id);
        };

        vm.ustvari = function(valid) {
            if (valid) {
                console.log("ustvari");
                dogodki.create(vm.dogodek).then(function success(odgovor) {
                    console.log("ODGOVOR NA CREATE DOGODEK", odgovor);
                    angular.element('#dodaj-dogodek-modal').modal('hide');
                    vm.dogodki.push({
                        _id: odgovor.data._id,
                        naziv: vm.dogodek.naziv,
                        opis: vm.dogodek.opis,
                        organizator: vm.dogodek.organizator,
                        datum: vm.dogodek.datum,
                        datumDo: vm.dogodek.datumDo,
                    });
                    vm.dogodek = {};
                }, function error(odgovor) {
                    console.log(odgovor.e);
                });
            }
        };

    }
  
    dogodkiCtrl.$inject = ['$scope', 'dogodki', 'avtentikacija', '$location', 'todos'];
  
    /* global angular */
    angular
        .module('straightas')
        .controller('dogodkiCtrl', dogodkiCtrl);
    
})();
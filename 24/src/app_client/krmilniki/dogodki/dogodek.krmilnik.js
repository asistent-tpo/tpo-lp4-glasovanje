(function() {

    function dogodekCtrl($scope, dogodki, avtentikacija, $location, $routeParams) {
        var vm = this;

        vm.vloga = avtentikacija.vloga();

        var id = $routeParams.id;

        dogodki.getOne(id).then(
            function success(odgovor) {
                console.log('success', odgovor);
                vm.dogodek = odgovor.data;
            }, function error(odgovor) {
                console.log('error', odgovor);
            }
        );

        vm.uredi = function () {
            console.log('Uredi', id);
            $location.path('/dogodki/' + id + '/uredi');
        }

        vm.izbrisi = function () {
            dogodki.deleteDogodek(id).then(
                function success(odgovor) {
                    console.log('success', odgovor);
                    $location.path('/dogodki');
                }, function error(odgovor) {
                    console.log('error', odgovor);
                }
            );
        }

    }

    dogodekCtrl.$inject = ['$scope', 'dogodki', 'avtentikacija', '$location', '$routeParams'];

    function urediDogodekCtrl($scope, dogodki, avtentikacija, $location, $routeParams, $timeout) {
        var vm = this;

        var id = $routeParams.id;

        dogodki.getOne(id).then(
            function success(odgovor) {
                console.log('success', odgovor);
                vm.dogodek = odgovor.data;
                vm.dogodek.datum = new Date(vm.dogodek.datum);
                if (vm.dogodek.datumDo) {
                    vm.dogodek.datumDo = new Date(vm.dogodek.datumDo);
                }
            }, function error(odgovor) {
                console.log('error', odgovor);
            }
        );

        vm.potrdi = function (valid) {
            console.log(valid);
            if (valid) {
                console.log('Potrdi urejanje', id);
                // show editing notification
                vm.alert = {urejanje: true};
                dogodki.update(id, vm.dogodek).then(
                    function success(odgovor) {
                        console.log('success', odgovor);
                        // show updated notification
                        $timeout(function () {
                            vm.alert = {urejeno: true};
                            $timeout(function () {
                                $location.path('/dogodki/' + id);
                            }, 2000);
                        }, 500);
                    }, function error(odgovor) {
                        console.log('error', odgovor);
                        // show erorr notification
                        vm.alert = {napaka: true};
                    }
                );
            } else {
                console.log('Neveljavna polja.');
            }
        };

        vm.preklici = function () {
            console.log('Prekliči urejanje');
            $location.path('/dogodki/' + id);
        };

    }

    urediDogodekCtrl.$inject = ['$scope', 'dogodki', 'avtentikacija', '$location', '$routeParams', '$timeout'];

    /* global angular */
    angular
        .module('straightas')
        .controller('dogodekCtrl', dogodekCtrl)
        .controller('urediDogodekCtrl', urediDogodekCtrl);

})();
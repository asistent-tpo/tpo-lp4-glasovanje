(function() {

  function nastavitev($routeProvider, $locationProvider) {
    $routeProvider
      .when('/prijava', {
        templateUrl: 'skupno/pogledi/prijava.pogled.html',
        controller: 'prijavaCtrl',
        controllerAs: 'vm'
      })
      .when('/registracija', {
        templateUrl: 'skupno/pogledi/registracija.pogled.html',
        controller: 'registracijaCtrl',
        controllerAs: 'vm'
      })
      .when('/domov', {
        templateUrl: 'skupno/pogledi/main.pogled.html',
        controller: 'mainCtrl',
        controllerAs: 'vm'
      })
      .when('/', {
        templateUrl: 'skupno/pogledi/main.pogled.html',
        controller: 'mainCtrl',
        controllerAs: 'vm'
      })
      .when('/dogodki', {
        templateUrl: 'skupno/pogledi/dogodki.pogled.html',
        controller: 'dogodkiCtrl',
        controllerAs: 'vm'
      })
      .when('/dogodki/:id', {
        templateUrl: 'skupno/pogledi/dogodek.pogled.html',
        controller: 'dogodekCtrl',
        controllerAs: 'vm'
      })
      .when('/dogodki/:id/uredi', {
        templateUrl: 'skupno/pogledi/dogodek-uredi.pogled.html',
        controller: 'urediDogodekCtrl',
        controllerAs: 'vm'
      })
      .when('/hrana', {
        templateUrl: 'skupno/pogledi/hrana.pogled.html'
      })
      .when('/obvestila', {
        templateUrl: 'skupno/pogledi/obvestila.pogled.html',
        controller: 'obvestilaCtrl',
        controllerAs: 'vm'
      })
      .when('/404',{
        templateUrl: 'skupno/pogledi/404.pogled.html'
      })
      .when('/nastavitve', {
        templateUrl: 'skupno/pogledi/nastavitve.pogled.html',
        controller: 'uporabnikCtrl',
        controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/prijava'});
    $locationProvider.html5Mode(true);
  }
  
  /* global angular */
  angular
    .module('straightas', ['ngRoute', 'ui.calendar'])
    .config(['$routeProvider', '$locationProvider', nastavitev])
    .run(['$rootScope', '$location', 'avtentikacija', 
      function($rootScope, $location, avtentikacija) {
        // register listener to watch route changes
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {
          // console.log('next', next);
          
          if (!next.$$route) {
            console.log('sel je na stran, ki ne obstaja.');
            $location.path( "/404" );
          }
          
          var path = next.$$route.originalPath;
        
          
          //TODO potrebno samo nastaviti za vsakega, kar mu je dovoljeno!!!
          var dovoljenePotiZaGosta = ['/prijava', '/registracija', '/404', '/hrana'];
          var dovoljenePotiZaUporabnika = ['/', '/prijava', '/registracija', '/domov', '/dogodki', '/dogodki/:id', '/dogodki/:id/uredi', '/hrana', '/404', '/nastavitve', '/obvestila'];
          var dovoljenePotiZaUpravljalca = ['/prijava', '/registracija', '/404', '/dogodki', '/dogodki/:id', '/dogodki/:id/uredi', '/nastavitve', '/obvestila'];
          var dovoljenePotiZaAdministratorja = ['/prijava', '/registracija', '/404', '/nastavitve', '/obvestila'];

          var vloga = avtentikacija.vloga();
          var dovoljenePoti = [];
          console.log("vloga:", vloga);
          
          if (vloga == 0){
            dovoljenePoti = dovoljenePotiZaGosta;
          } else if (vloga == 1) {
            dovoljenePoti = dovoljenePotiZaUporabnika;
          } else if (vloga == 2) {
            dovoljenePoti = dovoljenePotiZaUpravljalca;
          } else if (vloga == 3) {
            dovoljenePoti = dovoljenePotiZaAdministratorja;
          }

          
          if (dovoljenePoti.includes(path)) {
            if (vloga >= 1 && (path == "/prijava" || path == "/registracija")) {
              avtentikacija.odjava();
              $location.path(path);
            }
            return;
          } else if (path != '/') {
            $location.path('/404');
            return;
          }
          
          if (vloga == 1) {
            $location.path("/domov");
          } else if (vloga == 0) {
            $location.path("/hrana");
          } else if (vloga == 2) {
            $location.path("/dogodki");
          } else if (vloga == 3) {
            $location.path("/obvestila");
          }

          
        });
    }]);
  
})();
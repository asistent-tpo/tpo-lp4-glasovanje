(function() {
  
  var navigacija = function() {
    console.log('navigacija');
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/navigacija/navigacija.predloga.html',
      controller: 'navigacijaCtrl',
      controllerAs: 'nvm'
    };
  };
  
  /* global angular */
  angular
    .module('straightas')
    .directive('navigacija', navigacija);
    
})();
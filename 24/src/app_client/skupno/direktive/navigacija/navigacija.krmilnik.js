(function() {
  console.log('navigacijaCtrl');
  function navigacijaCtrl($location, avtentikacija, $route) {
    var nvm = this;

      nvm.jePrijavljen = avtentikacija.jePrijavljen();
      nvm.vloga = avtentikacija.vloga();

  	nvm.odjava = function () {
        $location.path('/prijava');
    }

    nvm.switchToHome = function() {
        $location.path('/domov');
    }
    
    nvm.hrana = function() {
        $location.path('/hrana');
    }

    nvm.switchToEvents = function() {
        $location.path('/dogodki');
    }
    
    nvm.nastavitve = function() {
        $location.path('/nastavitve');
    }

    nvm.obvestila = function () {
        $location.path('/obvestila');
    }
    
  }

  navigacijaCtrl.$inject = ['$location', 'avtentikacija', '$route'];

  /* global angular */
  angular
    .module('straightas')
    .controller('navigacijaCtrl', navigacijaCtrl);
})();
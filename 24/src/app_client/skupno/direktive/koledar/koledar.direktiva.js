(function() {
  
  var koledar = function() {
    console.log('koledar');
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/koledar/koledar.predloga.html',
      controller: 'koledarCtrl',
      controllerAs: 'kvm'
    };
  };
  
  /* global angular */
  angular
    .module('straightas')
    .directive('koledar', koledar);
    
})();
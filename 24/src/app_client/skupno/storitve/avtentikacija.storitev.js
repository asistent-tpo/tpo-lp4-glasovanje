(function() {
  function avtentikacija($window, $http) {
    var b64Utf8 = function (niz) {
      return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
    };
    
    var shraniZeton = function(zeton) {
      $window.localStorage['zeton'] = zeton;
    };
    
    var vrniZeton = function() {
     return $window.localStorage['zeton'];
    };
    
    var registracija = function(uporabnik) {
      return $http.post('/api/registracija', uporabnik);
    };

    var prijava = function(uporabnik) {
      return $http.post('/api/prijava', uporabnik).then(
        function success(odgovor) {
          shraniZeton(odgovor.data.jwt);
        });
    };

    var odjava = function() {
      $window.localStorage.removeItem('zeton');
    };
    
    var jePrijavljen = function() {
      var zeton = vrniZeton();
      if (zeton) {
          var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
          return koristnaVsebina.notAfter > Date.now() / 1000;
      } else {
        return false;
      }
    };
    
    var vloga = function() {
      if (jePrijavljen()) {
        var zeton = vrniZeton();
        var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
        if (koristnaVsebina.vloga){
          return koristnaVsebina.vloga;
        } else {
          return undefined;
        }
      } else {
        return 0;
      }
    };
    
    var spremeniGeslo = function(podatki) {
      if (jePrijavljen()) {
        return $http.put('/api/spremenigeslo', podatki);
      }
    };

    var myId = function() {
      if (jePrijavljen()) {
        var zeton = vrniZeton();
        var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
        return koristnaVsebina._id;
      }
    };
    
    var trenutniUporabnik = function() {
      if (jePrijavljen()) {
        var zeton = vrniZeton();
        var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
        return {
          elektronskiNaslov: koristnaVsebina.email,
        };
      }
    };
    
    return {
      shraniZeton: shraniZeton,
      vrniZeton: vrniZeton,
      registracija: registracija,
      prijava: prijava,
      odjava: odjava,
      jePrijavljen: jePrijavljen,
      myId:myId,
      spremeniGeslo: spremeniGeslo,
      trenutniUporabnik: trenutniUporabnik,
      vloga:vloga
    };
  }
  avtentikacija.$inject = ['$window', '$http'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('avtentikacija', avtentikacija);
})();
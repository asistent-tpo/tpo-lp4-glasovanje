(function() {
  
  var todos = function($http, avtentikacija) {
    
    
    var create = function(data) {
      return $http.post("api/todos", data, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };

    var update = function(id, data) {
      return $http.put("api/todos/" + id, data, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };

    var remove = function(id) {
      return $http.delete("api/todos/" + id, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };

    var getTodosForUser = function (data) {
      console.log('getTodosForUser', data);
      return $http.get("api/todos", {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null,
        params: data
      });
    };

    var getTodo = function (id) {
      return $http.get("api/todos/" + id, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };
      
    return {
      create: create,
      update: update,
      remove: remove,
      getTodosForUser: getTodosForUser,
      getTodo: getTodo,
    };
  };
  

  todos.$inject = ['$http', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('todos', todos);

})();
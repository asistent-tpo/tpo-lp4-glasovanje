(function() {

    var obvestila = function($http, avtentikacija) {

        var createObvestilo = function(obvestilo) {
            console.log('createObvestilo');
            return $http.post("api/obvestila", obvestilo, {
                headers: avtentikacija.jePrijavljen() ? {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                } : null
            });
        };

        var getObvestila = function (aktivno) {
            console.log('getObvestila');
            return $http.get("api/obvestila", {
                headers: avtentikacija.jePrijavljen() ? {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                } : null,
                params: {
                    aktivno: aktivno
                }
            });
        };

        var getObvestilo = function (id) {
            console.log('getObvestilo', id);
            return $http.get("api/obvestila/" + id, {
                headers: avtentikacija.jePrijavljen() ? {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                } : null
            });
        };

        var updateObvestilo = function(id, obvestilo) {
            console.log('updateObvestilo', id, obvestilo);
            return $http.put("api/obvestila/" + id, obvestilo, {
                headers: avtentikacija.jePrijavljen() ? {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                } : null
            });
        };

        var deleteObvestilo = function (id) {
            return $http.delete("api/obvestila/" + id, {
                headers: avtentikacija.jePrijavljen() ? {
                    Authorization: 'Bearer ' + avtentikacija.vrniZeton()
                } : null
            });
        };

        return {
            createObvestilo: createObvestilo,
            getObvestila: getObvestila,
            getObvestilo: getObvestilo,
            updateObvestilo: updateObvestilo,
            deleteObvestilo: deleteObvestilo,
        };
    };


    obvestila.$inject = ['$http', 'avtentikacija'];

    /* global angular */
    angular
        .module('straightas')
        .service('obvestila', obvestila);

})();
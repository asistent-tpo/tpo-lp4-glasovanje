(function() {
  
  var dogodki = function($http, avtentikacija) {
    
    var create = function(dogodek) {
      return $http.post("api/events", dogodek, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };

    var getEventsFromMonth = function (month) {
      console.log('getEventsFromMonth', month);
      return $http.get("api/events", {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null,
        params: {
          month: month
        }
      });
    };

    var getEvents = function (start, end) {
      console.log('getEventsFromMonth');
      return $http.get("api/events", {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null,
        params: {
          start: start,
          end: end,
        }
      });
    };

    var getOne = function (id) {
      console.log('getOne', id);
      return $http.get("api/events/" + id, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };

    var update = function(id, dogodek) {
      console.log(id, dogodek);
      return $http.put("api/events/" + id, dogodek, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };

    var deleteDogodek = function (id) {
      return $http.delete("api/events/" + id, {
        headers: avtentikacija.jePrijavljen() ? {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        } : null
      });
    };
      
    return {
      create: create,
      getEventsFromMonth: getEventsFromMonth,
      getEvents: getEvents,
      getOne: getOne,
      update: update,
      deleteDogodek: deleteDogodek,
    };
  };
  

  dogodki.$inject = ['$http', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('dogodki', dogodki);

})();
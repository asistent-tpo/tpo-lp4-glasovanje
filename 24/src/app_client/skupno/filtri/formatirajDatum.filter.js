(function() {

    var formatirajDatum = function() {
        return function(datum) {

            var formatDate = function(date) {
                var monthNames = [
                    "Januar", "Februar", "Marec",
                    "April", "Maj", "Junij", "Julij",
                    "August", "September", "Oktober",
                    "November", "December"
                ];

                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();

                return day + ' ' + monthNames[monthIndex] + ' ' + year;
            };

            if (typeof datum != undefined) {
                var date = new Date(datum);
                return formatDate(date);
                // return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
            } else {
                return "Unknown";
            }
        };
    };

    var formatirajUro = function () {
        return function(datum) {
            if (typeof datum != undefined) {
                var date = new Date(datum);
                var ure = date.getHours();
                var minute = date.getMinutes();
                if (minute.toString().length == 1) {
                    minute = '0' + minute.toString()
                }
                return ure + ':' + minute;
            } else {
                return "Unknown";
            }
        };
    };

    var formatTimestamp = function() {
        return function(datum) {
            if (typeof datum != undefined) {
                var date = new Date(datum);
                var year = date.getFullYear().toString();
                var month = (date.getMonth() + 1).toString();
                var day = date.getDate().toString();
                var hours = date.getHours().toString();
                var minutes = date.getMinutes().toString();
                var seconds = date.getSeconds().toString();

                month = month.length == 2 ? month : '0' + month;
                day = day.length == 2 ? day : '0' + day;
                hours = hours.length == 2 ? hours : '0' + hours;
                minutes = minutes.length == 2 ? minutes : '0' + minutes;
                seconds = seconds.length == 2 ? seconds : '0' + seconds;

                return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
            } else {
                return "Unknown";
            }
        };
    };

    /* global angular */
    angular
        .module('straightas')
        .filter('formatTimestamp', formatTimestamp)
        .filter('formatirajUro', formatirajUro)
        .filter('formatirajDatum', formatirajDatum);
})();
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');
var passport = require('passport');

passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new LocalStrategy(function (email, password, done) {
    console.log("email:", email);
    Uporabnik.findOne({
        email: email
    }, function (err, doc) {
        console.log("v authenticate");
        if (err) {
            console.log("error v authenticate");
            done(err);
        } else {
            if (doc) {
                var valid = doc.preveriGeslo(password);
                if (valid) {
                    // do not add password hash to session
                    console.log("valid v authenticate");
                    done(null, doc);
                    return;
                } else {
                    console.log("invalid v authenticate");
                    done(null, false);
                }
            } else {
                console.log("not found v authenticate");
                done(null, false);
            }
        }
    });
}));
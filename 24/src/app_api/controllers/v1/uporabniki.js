var mongoose = require('mongoose');
var passport = require('passport');
var Uporabnik = mongoose.model('Uporabnik');
var Validate = require('./shared/validations');
var Utils = require('./shared/utils');

var TAG = "API /users [app_api/controllers/users.js]";

var JSONcallback = function(res, status, msg) {
  res.status(status);
  res.json(msg);
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

//Registracija uporabnika
module.exports.registracija = function(zahteva, odgovor) {
    console.log(TAG, '@createUser');
    
    Validate.validateCreateUserRequest(odgovor, zahteva, function(res, status, data){
        if(status >= 200 && status < 300){
            createUserDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, {
                status: status,
                msg: "Nek error"
            });
        }
    });
};


// Prijava uporabnika
module.exports.prijava = function (zahteva, odgovor) {
    console.log(TAG, '@login');
    zahteva.body.username = zahteva.body.email;
    Validate.validateLoginRequest(odgovor, {
        email: zahteva.body.email,
        password: zahteva.body.password
    }, function (res, status, data) {
        if (status >= 400) {
            Utils.JSONcallback(res, status, data);
        } else {
            console.log("je kul", status, data);
            passport.authenticate('local', function(error, uporabnik, data) {
                console.log("uporabnik", uporabnik);
                if (error) {
                    Utils.JSONcallback(res, 404, error);
                } else if (uporabnik) {
                    Utils.JSONcallback(res, 200, {
                        "jwt": uporabnik.generirajJwt()
                    });
                } else {
                    Utils.JSONcallback(res, 401, {
                        message: "Unauthorized",
                        description: "Invalid credentials"
                    });
                }
            })(zahteva, res);
        }
    });
};

module.exports.spremeniGeslo = function (req, res) {
    console.log(TAG, '@updateUser');
    Validate.validateUpdateUserRequest(res, req, function (res, status, data) {
        if (status >= 200 && status < 300) {
            data._id = data.id;
            updateUserDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

// Odjava uporabnika
module.exports.odjava = function (req, res) {
    console.log(TAG, '@logout');
    Utils.JSONcallback(res, 200, null);
};



//#region Functions with database 
var createUserDb = function (res, data, callback) {
    console.log(TAG, '@createUserDb');
 
    var uporabnik = new Uporabnik();
    uporabnik.email = data.email;
    uporabnik.nastaviGeslo(data.password);
    uporabnik.vloga = data.vloga ? data.vloga : undefined;
    
    save(res, uporabnik, function(res, status, user) {
        if (status >= 400) {
            callback(res, status, user);
        } else {
            callback(res, 200, {
                _id: user._id
            });
        }
    });
};
module.exports.createUserDb = createUserDb;


var updateUserDb = function (res, data, callback) {
    console.log(TAG, '@updateUserDb');
    findById(res, data._id, function(res, status, user) {
        if (status >= 400) {
            callback(res, status, user);
        } else {
            //TODO potrebno preveriti če je data.password res geslo od uporabnika
            if (data.newpass) {
                user.nastaviGeslo(data.newpass);
            }
            console.log(TAG, 'SVASTA');
            save(res, user, function (res, status, user) {
                if (status >= 400) {
                    callback(res, status, user);
                } else {
                    callback(res, 200, user);
                }
            });
        }
    });
}

//#endregion



//#region SAVE, FIND_BY_ID, REMOVE_BY_ID
var save = function (res, user, callback) {
    user.save(function (err, user) {
        if (err) {
            callback(res, 409, {
                message: "Error saving user.",
                description: "User with this email already exists."
            });
        } else {
            callback(res, 200, user);
        }
    });
};
module.exports.save = save;

var findById = function (res, id, callback) {
    Uporabnik.findOne({
        _id: id
    }, function (err, user) {
        if (err) {
            callback(res, 500, {
                message: "Error finding user by id.",
                description: err
            });
        } else if (!user) {
            callback(res, 404, {
                message: "User not found",
                description: "User with id not found."
            });
        } else {
            callback(res, 200, user);
        }
    });
};
module.exports.findById = findById;

var removeById = function (res, id, callback) {
    Uporabnik.remove({
        _id: id
    }, function (err) {
        if (err) {
            callback(res, 500, {
                message: "Error removing user by id.",
                description: err
            });
        } else {
            callback(res, 200, null);
        }
    });
};
module.exports.removeById = removeById;
//#endregion
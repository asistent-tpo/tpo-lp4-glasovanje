var mongoose = require('mongoose');
var Dogodek = mongoose.model('Dogodek');
var Utils = require('./shared/utils');
var Validate = require('./shared/validations')

var TAG = "API /event [app_api/controllers/dogodki.js]";

var JSONcallback = function(res, status, msg) {
  res.status(status);
  res.json(msg);
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.getEvent = function(zahteva, odgovor) {
    console.log(TAG, '@getEvent');
    
    Validate.validateGetEventRequest(odgovor, zahteva, function(res, status, data){
        if(status >= 200 && status < 300){
            getEventDb(odgovor, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

module.exports.getEvents = function(zahteva, odgovor) {
    console.log(TAG, '@getEvents');

    Validate.validateGetEventsRequest(odgovor, zahteva, function(res, status, data){
        if(status >= 200 && status < 300){
            var query = data.month ? {
                datum: {
                    $gte: Utils.getStartOfMonth(data.month),
                    $lte: Utils.getEndOfMonth(data.month)
                }
            } : {
                datum: {
                    $gte: Utils.getStartOfDay(new Date())
                }
            };
            if (data.start && data.end) {
                query = {
                    datum: {
                        $gte: new Date(data.start),
                        $lte: new Date(data.end)
                    }
                }
            }
            getEventsDb(odgovor, query, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

//dodajanje dogodka
module.exports.createEvent = function(zahteva, odgovor) {
    console.log(TAG, '@createEvent');
    
    Validate.validateCreateEventRequest(odgovor, zahteva.body, function(res, status, data){
        if(status >= 200 && status < 300){
            createEventDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data.body);
        }
    });
};

// urejanje dogodka
module.exports.updateEvent = function (zahteva, odgovor) {
    console.log(TAG, '@updateEvent');
    zahteva.body.id = zahteva.params.id;
    Validate.validateUpdateEventRequest(odgovor, zahteva.body, function (res, status, data) {
        if (status >= 400) {
            Utils.JSONcallback(res, status, data);
        } else {
            updateEventDb(res, data, function (res, status, event) {
                if (status >= 400) {
                    Utils.JSONcallback(res, status, event);
                } else {
                    Utils.JSONcallback(res, status, event);
                }
            });
        }
    });
};

module.exports.deleteEvent = function (req, res) {
    console.log(TAG, '@deleteEvent', req.params.id);
    Validate.validateDeleteEventRequest(res, {
        id: req.params.id
    }, function (res, status, data) {
        if (status >= 400) {
            Utils.JSONcallback(res, status, data);
        } else {
            deleteEventDb(res, data, function (res, status, err) {
                if (status >= 400) {
                    Utils.JSONcallback(res, status, err);
                } else {
                    Utils.JSONcallback(res, 200, {
                        message: "Operation successful.",
                        description: "Event successfully deleted."
                    });
                }
            });
        }
    });
};


//#region Functions with database 
var createEventDb = function (res, data, callback) {
    console.log(TAG, '@createEventDb');
    
    var dogodek = new Dogodek();
    dogodek.naziv = data.naziv;
    dogodek.datum = new Date(data.datum);
    dogodek.datumDo = dogodek.datumDo == null ? undefined : new Date(data.datumDo);
    dogodek.organizator = data.organizator;
    dogodek.opis = data.opis;
    
    save(res, dogodek, function(res, status, event) {
        if (status >= 400) {
            callback(res, status, event);
        } else {
            callback(res, 200, {
                _id: event._id
            });
        }
    });
};

var getEventDb = function (res, id, callback) {
    console.log(TAG, '@getEventDb');
    
    findById(res, id, function(res, status, event) {
        if (status >= 400) {
            callback(res, status, event);
        } else {
            callback(res, 200, event);
        }
    });
};

var getEventsDb = function (res, data, callback) {
    console.log(TAG, '@getEventsDb');
    console.log(data);

    getAll(res, data, function(res, status, event) {
        if (status >= 400) {
            callback(res, status, event);
        } else {
            callback(res, 200, event);
        }
    });
};

var updateEventDb = function (res, data, callback) {
    console.log(TAG, '@updateEventDb');
    // first get an event
    findById(res, data.id, function(res, status, event) {
        if (status >= 400) {
            callback(res, status, event);
        } else {
            event.naziv = data.naziv;
            event.datum = data.datum;
            event.datumDo = data.datumDo;
            event.organizator = data.organizator;
            event.opis = data.opis;

            save(res, event, function(res, status, event) {
                if (status >= 400) {
                    callback(res, status, event);
                } else {
                    callback(res, 200, {
                        _id: event._id
                    });
                }
            });
        }
    });
};

var deleteEventDb = function (res, data, callback) {
    console.log(TAG, '@deleteEventDb');
    findById(res, data.id, function (res, status, event) {
        if (status >= 400) {
            callback(res, status, event);
        } else {
            removeById(res, data.id, callback);
        }
    });
};

//#endregion



//#region SAVE, FIND_BY_ID, REMOVE_BY_ID
var save = function (res, event, callback) {
    event.save(function (err, event) {
        if (err) {
            callback(res, 409, {
                message: "Error saving event.",
                description: err
            });
        } else {
            callback(res, 200, event);
        }
    });
};
module.exports.save = save;

var findById = function (res, id, callback) {
    Dogodek.findOne({
        _id: id
    }, function (err, event) {
        if (err) {
            callback(res, 500, {
                message: "Error finding event by id.",
                description: err
            });
        } else if (!event) {
            callback(res, 404, {
                message: "Event not found",
                description: "Event with id not found."
            });
        } else {
            console.log(event);
            callback(res, 200, event);
        }
    });
};
module.exports.findById = findById;

var getAll = function (res, data, callback) {
    Dogodek.find(data, null, { sort: { datum: 1 } }, function(err, events) {
        if (err) {
            callback(res, 500, {
                message: "Error finding events.",
                description: err
            });
        } else {
            callback(res, 200, events);
        }
    }).sort();
};
module.exports.getAll = getAll;

var removeById = function (res, id, callback) {
    Dogodek.remove({
        _id: id
    }, function (err) {
        if (err) {
            callback(res, 500, {
                message: "Error removing event by id.",
                description: err
            });
        } else {
            callback(res, 200, null);
        }
    });
};
module.exports.removeById = removeById;
//#endregion
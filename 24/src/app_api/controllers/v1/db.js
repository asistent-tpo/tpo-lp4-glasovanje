var mongoose = require('mongoose');
var Validate = require('./shared/validations');
var Utils = require('./shared/utils');
var Uporabniki = require('./uporabniki');

var TAG = "API /users [app_api/controllers/db.js]";

var JSONcallback = function(res, status, msg) {
    res.status(status);
    res.json(msg);
};

module.exports.generate = function (req, res) {
    console.log(TAG, '@generate');
    mongoose.connection.db.dropDatabase(function (err) {
        if (err) {
            console.log(TAG, err);
            Utils.JSONcallback(res, 500, {
                msg: 'Internal Server Error',
                description: 'Error droping database'
            });
        } else {
            console.log(TAG, 'Drop sucessful');
            generateDb(res, dummyUsers, Utils.JSONcallback);
        }
    });
};

var generateDb = function (res, data, callback) {
    Utils.foldl(function (user, list, res, callback) {
        Uporabniki.createUserDb(res, user, callback);
    }, undefined, data, res, callback);
};

var dummyUsers = [
    {
        email: 'user1@tpo.si',
        password: 'Geslo123',
        vloga: 1
    }, {
        email: 'user2@tpo.si',
        password: 'Geslo123',
        vloga: 2
    }, {
        email: 'user3@tpo.si',
        password: 'Geslo123',
        vloga: 3
    }
];
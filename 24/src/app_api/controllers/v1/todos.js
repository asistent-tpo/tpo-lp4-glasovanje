var mongoose = require('mongoose');
var Todo = mongoose.model('Todo');
var Utils = require('./shared/utils');
var Validate = require('./shared/validations')

var TAG = "API /Todo [app_api/controllers/dogodki.js]";

var JSONcallback = function(res, status, msg) {
  res.status(status);
  res.json(msg);
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.getTodo = function(zahteva, odgovor) {
    console.log(TAG, '@getTodo');
    
    Validate.validateGetTodoRequest(odgovor, zahteva, function(res, status, data){
        if(status >= 200 && status < 300){
            getTodoDb(odgovor, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

module.exports.getTodos = function(zahteva, odgovor) {
    console.log(TAG, '@getTodos');

    Validate.validateGetTodosRequest(odgovor, zahteva, function(res, status, data){
        if(status >= 200 && status < 300){
            getTodosDb(odgovor, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

//dodajanje dogodka
module.exports.createTodo = function(zahteva, odgovor) {
    console.log(TAG, '@createTodo');
    
    Validate.validateCreateTodoRequest(odgovor, zahteva.body, function(res, status, data){
        if(status >= 200 && status < 300){
            createTodoDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

// urejanje dogodka
module.exports.updateTodo = function (zahteva, odgovor) {
    console.log(TAG, '@updateTodo');
    zahteva.body.id = zahteva.params.id;
    Validate.validateUpdateTodoRequest(odgovor, zahteva, function (res, status, data) {
        if (status >= 200 && status < 300) {
            updateTodoDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

module.exports.deleteTodo = function (req, res) {
    console.log(TAG, '@deleteTodo', req.params.id);
    Validate.validateDeleteTodoRequest(res, {
        id: req.params.id
    }, function (res, status, data) {
        if (status >= 400) {
            Utils.JSONcallback(res, status, data);
        } else {
            deleteTodoDb(res, data, function (res, status, err) {
                if (status >= 400) {
                    Utils.JSONcallback(res, status, err);
                } else {
                    Utils.JSONcallback(res, 200, {
                        message: "Operation successful.",
                        description: "Todo successfully deleted."
                    });
                }
            });
        }
    });
};


//#region Functions with database 
var createTodoDb = function (res, data, callback) {
    console.log(TAG, '@createTodoDb');
    
    var todo = new Todo();
    todo.user = data.user;
    todo.opis = data.opis;
    
    save(res, todo, function(res, status, todo) {
        if (status >= 400) {
            callback(res, status, todo);
        } else {
            callback(res, 200, todo);
        }
    });
};

var getTodoDb = function (res, id, callback) {
    console.log(TAG, '@getTodoDb');
    
    findById(res, id, function(res, status, Todo) {
        if (status >= 400) {
            callback(res, status, Todo);
        } else {
            callback(res, 200, Todo);
        }
    });
};

var getTodosDb = function (res, data, callback) {
    console.log(TAG, '@getTodosDb');

    getAll(res, data, function(res, status, event) {
        if (status >= 400) {
            callback(res, status, event);
        } else {
            callback(res, 200, event);
        }
    });
};

var updateTodoDb = function (res, data, callback) {
    console.log(TAG, '@updateTodoDb');
    // first get an Todo
    findById(res, data.id, function(res, status, todo) {
        if (status >= 400) {
            callback(res, status, todo);
        } else {
            todo.naziv = data.naziv;
            todo.datum = data.datum;
            todo.organizator = data.organizator;
            todo.opis = data.opis;

            save(res, todo, function(res, status, todo) {
                if (status >= 400) {
                    callback(res, status, todo);
                } else {
                    callback(res, 200, todo);
                }
            });
        }
    });
};

var deleteTodoDb = function (res, data, callback) {
    console.log(TAG, '@deleteTodoDb');
    findById(res, data.id, function (res, status, todo) {
        if (status >= 400) {
            callback(res, status, todo);
        } else {
            removeById(res, data.id, callback);
        }
    });
};

//#endregion



//#region SAVE, FIND_BY_ID, REMOVE_BY_ID
var save = function (res, todo, callback) {
    todo.save(function (err, Todo) {
        if (err) {
            callback(res, 409, {
                message: "Error saving Todo.",
                description: err
            });
        } else {
            callback(res, 200, Todo);
        }
    });
};
module.exports.save = save;

var findById = function (res, id, callback) {
    Todo.findOne({
        _id: id
    }, function (err, todo) {
        if (err) {
            callback(res, 500, {
                message: "Error finding Todo by id.",
                description: err
            });
        } else if (!todo) {
            callback(res, 404, {
                message: "Todo not found",
                description: "Todo with id not found."
            });
        } else {
            console.log(todo);
            callback(res, 200, todo);
        }
    });
};
module.exports.findById = findById;

var getAll = function (res, data, callback) {
    Todo.find(data, null, {}, function(err, todos) {
        if (err) {
            callback(res, 500, {
                message: "Error finding todos.",
                description: err
            });
        } else {
            callback(res, 200, todos);
        }
    }).sort();
};
module.exports.getAll = getAll;

var removeById = function (res, id, callback) {
    Todo.remove({
        _id: id
    }, function (err) {
        if (err) {
            callback(res, 500, {
                message: "Error removing Todo by id.",
                description: err
            });
        } else {
            callback(res, 200, null);
        }
    });
};
module.exports.removeById = removeById;
//#endregion
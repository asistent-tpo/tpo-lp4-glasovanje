var mongoose = require('mongoose');
var Obvestilo = mongoose.model('Obvestilo');
var Uporabnik = mongoose.model('Uporabnik');
var Utils = require('./shared/utils');
var Validate = require('./shared/validations')

var TAG = "API /event [app_api/controllers/obvestila.js]";

var JSONcallback = function(res, status, msg) {
  res.status(status);
  res.json(msg);
};

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};


//dodajanje obvestila
module.exports.createObvestilo = function(zahteva, odgovor) {
    console.log(TAG, '@createObvestilo');
    
    Validate.validateCreateObvestiloRequest(odgovor, zahteva.body, function(res, status, data){
        if(status >= 200 && status < 300){
            createObvestiloDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data.body);
        }
    });
};

module.exports.getObvestilo = function(req, res) {
    console.log(TAG, '@getObvestilo');
    
    Validate.validateGetObvestiloRequest(res, req.params, function(res, status, data){
        if(status >= 200 && status < 300){
            getObvestiloDb(res, req.params.id, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

module.exports.getObvestila = function(req, res) {
    console.log(TAG, '@getObvestila');

    Validate.validateGetObvestilaRequest(res, req.query, function (res, status, data) {
        if(status >= 200 && status < 300){
            console.log("data:", data);
            var query = data.aktivno ? {
                aktivno: data.aktivno
            } : undefined;
            getObvestilaDb(res, query, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data);
        }
    });
};

module.exports.updateObvestilo = function(req, res) {
    console.log(TAG, '@updateObvestilo');

    req.body._id = req.params.id;

    Validate.validateUpdateObvestiloRequest(res, req.body, function(res, status, data){
        if(status >= 200 && status < 300){
            updateObvestiloDb(res, data, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data.body);
        }
    });
};

module.exports.deleteObvestilo = function(req, res) {
    console.log(TAG, '@deleteObvestilo');

    Validate.validateDeleteObvestiloRequest(res, req.params, function(res, status, data){
        if(status >= 200 && status < 300){
            console.log("before deleteObvestiloDb");
            deleteObvestiloDb(res, data.id, Utils.JSONcallback);
        } else {
            Utils.JSONcallback(res, status, data.body);
        }
    });
};

//#region Functions with database 

var getObvestiloDb = function (res, id, callback) {
    console.log(TAG, '@getObvestiloDb');
    
    findByIdObvestilo(res, id, function(res, status, obv) {
        if (status >= 400) {
            callback(res, status, obv);
        } else {
            callback(res, 200, obv);
        }
    });
};

var getObvestilaDb = function (res, data, callback) {
    console.log(TAG, '@getObvestilaDb', data);
    
    getAll(res, data, function(res, status, obv) {
        if (status >= 400) {
            callback(res, status, obv);
        } else {
            callback(res, 200, obv);
        }
    });
};

var createObvestiloDb = function (res, data, callback) {
    console.log(TAG, '@createObvestiloDb');
    
    var obvestilo = new Obvestilo();
    obvestilo.message = data.message;
    obvestilo.aktivno = data.aktivno;
    obvestilo.datum = new Date(data.datum);

    save(res, obvestilo, function(res, status, obvestilo) {
        if (status >= 400) {
            callback(res, status, obvestilo);
        } else {
            callback(res, 200, {
                _id: obvestilo._id
            });
        }
    });
};

var updateObvestiloDb = function (res, data, callback) {
    console.log(TAG, '@updateObvestiloDb');
    findByIdObvestilo(res, data._id, function (res, status, obvestilo) {
        if (status >= 400) {
            callback(res, status, obvestilo);
        } else {
            obvestilo.message = data.message ? data.message : obvestilo.message;
            obvestilo.aktivno = data.aktivno != undefined ? data.aktivno : obvestilo.aktivno;
            obvestilo.datum = data.datum ? new Date(data.datum) : new Date();

            save(res, obvestilo, function(res, status, obvestilo) {
                if (status >= 400) {
                    callback(res, status, obvestilo);
                } else {
                    callback(res, 200, {
                        _id: obvestilo._id
                    });
                }
            });
        }
    });
};

var deleteObvestiloDb = function (res, id, callback) {
    console.log(TAG, '@deleteObvestiloDb', id);
    findByIdObvestilo(res, id, function (res, status, obvestilo) {
        if (status >= 400) {
            callback(res, status, obvestilo);
        } else {
            removeById(res, obvestilo._id, callback);
        }
    });
};

//#region SAVE, FIND_BY_ID, REMOVE_BY_ID
var save = function (res, obvestilo, callback) {
    obvestilo.save(function (err, obv) {
        if (err) {
            callback(res, 409, {
                message: "Error saving obvestilo.",
                description: err
            });
        } else {
            callback(res, 200, obv);
        }
    });
};
module.exports.save = save;

var findByIdObvestilo = function (res, id, callback) {
    Obvestilo.findOne({
        _id: id
    }, function (err, event) {
        if (err) {
            callback(res, 500, {
                message: "Error finding obvestilo by id.",
                description: err
            });
        } else if (!event) {
            callback(res, 404, {
                message: "Obvestilo not found",
                description: "Obvestilo with id not found."
            });
        } else {
            console.log(event);
            callback(res, 200, event);
        }
    });
};
module.exports.findByIdObvestilo = findByIdObvestilo;

var getAll = function (res, data, callback) {
    Obvestilo.find(data, null, { sort: { datum: -1 } }, function(err, obvestila) {
        if (err) {
            callback(res, 500, {
                message: "Error finding obvestila.",
                description: err
            });
        } else {
            callback(res, 200, obvestila);
        }
    }).sort();
};
module.exports.getAll = getAll;

var removeById = function (res, id, callback) {
    Obvestilo.remove({
        _id: id
    }, function (err) {
        if (err) {
            callback(res, 500, {
                message: "Error removing obvestilo by id.",
                description: err
            });
        } else {
            console.log("Obvestilo removed!");
            callback(res, 200, null);
        }
    });
};
module.exports.removeById = removeById;


//#endregion
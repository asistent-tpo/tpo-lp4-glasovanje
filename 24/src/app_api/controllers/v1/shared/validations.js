var TAG = "API [app_api/controllers/validations.js]";

module.exports.validateCreateUserRequest = function (res, data, callback) {
    console.log(TAG, '@validateCreateUserRequest');
    // ali je sploh body
    if (!data.body) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing request body."
        });
    } 
    //ali so vsi potrebni podatki
    else if (!data.body.email || !data.body.password || !data.body.confpass) { 
        console.log("EMAIL: ", data.body.email ? "not null" : "null");
        console.log("PASSWORD: ", data.body.password ? "not null" : "null");
        console.log("CONFPASS: ", data.body.confpass ? "not null" : "null");
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing required data."
        });
    } 
    //ali je email ustrezen
    else if (!isEmailValid(data.body.email)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid field email."
        });
    } 
    //ali se gesli ujemata
    else if (data.body.password !== data.body.confpass) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Passwords do not match."
        });
    } 
    // ali je geslo ustrezno
    else if (!isPasswordValid(data.body.password)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid field password."
        });
    } 
    //uspesno
    else {
        callback(res, 200, data.body);
    }
};

module.exports.validateLoginRequest = function (res, data, callback) {
    console.log(TAG, '@validateLoginRequest');
    console.log("data:", data);
    if (!data.email) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing body param email"
        });
    } else if (!data.password) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing body param password"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateUpdateUserRequest = function (res, data, callback) {
    console.log(TAG, '@validateUpdateUserRequest');
    // check for required data
    if (!data.body) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing request body."
        });
    } else if (!data.body.id || !isIdValid(data.body.id)) {
        callback(res, 404, {
            message: "User not found",
            description: "Path param user id is invalid."
    });
    } else if (data.body.newpass !== data.body.confpass) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Passwords do not match."
        });
    } else if (data.body.newpass && !isPasswordValid(data.body.newpass)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid field password."
        });
    } /*else if (data.body.email && !isEmailValid(data.body.email)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid field email."
        });*/
    else {
        callback(res, 200, data.body);
    }
};

module.exports.validateCreateEventRequest = function (res, data, callback) {
    console.log(TAG, '@validateCreateEventRequest', data);

    if (!data.naziv) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing or invalid body param 'naziv'"
        });
    } else if (!data.datum || !isDatetimeValid(data.datum)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing or invalid body param 'datum'"
        });
    } else if (data.datumDo && !isDatetimeValid(data.datumDo)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid body parameter 'datumDo'"
        });
    } else if (data.organizator && !isTextValid(data.organizator)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid body parameter 'organizator'"
        });
    } else if (data.opis && !isTextValid(data.opis)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid body parameter 'opis'"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateGetEventRequest = function (res, data, callback) {
    console.log(TAG, '@validateGetEventRequest');

    if (!data.params.id) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing param id"
        });
    } else {
        callback(res, 200, data.params.id);
    }
};

module.exports.validateGetEventsRequest = function (res, data, callback) {
    console.log(TAG, '@validateGetEventsRequest');

    if (data.query.month && !isDatetimeValid(data.query.month)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid field month."
        });
    } else if ((data.query.start || data.query.end) && (!isDatetimeValid(data.query.start) || !isDatetimeValid(data.query.end))) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Invalid field 'start' or 'end'."
        });
    } else {
        callback(res, 200, data.query);
    }
};

module.exports.validateUpdateEventRequest = function (res, data, callback) {
    console.log(TAG, '@validateUpdateEventRequest', data);
    if (!data.id || !isIdValid(data.id)) {
        callback(res, 404, {
            message: "Not found",
            description: "Event not found because of wrong id format"
        });
    } else if (data.datum && !isDatetimeValid(data.datum)) {
        callback(res, 405, {
            message: "Invalid parameter",
            description: "Invalid body parameter 'datum'"
        });
    } else if (data.datumDo != null && !isDatetimeValid(data.datumDo)) {
        callback(res, 405, {
            message: "Invalid parameter",
            description: "Invalid body parameter 'datumDo'"
        });
    } else if (data.naziv && !isTextValid(data.naziv)) {
        callback(res, 405, {
            message: "Invalid parameter",
            description: "Invalid body parameter 'naziv'"
        });
    } else if (data.organizator && !isTextValid(data.organizator)) {
        callback(res, 405, {
            message: "Invalid parameter",
            description: "Invalid body parameter 'organizator'"
        });
    } else if (data.opis && !isTextDescription(data.opis)) {
        callback(res, 405, {
            message: "Invalid parameter",
            description: "Invalid body parameter 'opis'"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateDeleteEventRequest = function (res, data, callback) {
    console.log(TAG, '@validateDeleteEventRequest');
    if (data.id && !isIdValid(data.id)) {
        callback(res, 400, "Invalid path parameter id.")
    } else {
        callback(res, 200, data);
    }
};


// Validations for TODOs
module.exports.validateCreateTodoRequest = function (res, data, callback) {
    console.log(TAG, '@validateCreateTodoRequest');

    if (!data.user) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing or invalid body param user"
        });
    } else if (!data.opis) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing or invalid body param opis"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateGetTodoRequest = function (res, data, callback) {
    console.log(TAG, '@validateGetTodoRequest');

    if (!data.params.id) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing param id"
        });
    } else {
        callback(res, 200, data.params.id);
    }
};

module.exports.validateGetTodosRequest = function (res, data, callback) {
    console.log(TAG, '@validateGetTodosRequest');

    if (!data.query.user) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing or invalid body param user."
        });
    } else {
        callback(res, 200, data.query);
    }
};

module.exports.validateUpdateTodoRequest = function (res, data, callback) {
    console.log(TAG, '@validateUpdateTodoRequest');
    if (!data.params.id || !isIdValid(data.params.id)) {
        callback(res, 404, {
            message: "Not found",
            description: "Event not found because of wrong id format"
        });
    } else {
        module.exports.validateCreateTodoRequest(res, data.body, callback);
    }
};

module.exports.validateDeleteTodoRequest = function (res, data, callback) {
    console.log(TAG, '@validateDeleteTodoRequest');
    if (data.id && !isIdValid(data.id)) {
        callback(res, 400, "Invalid path parameter id.")
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateCreateObvestiloRequest = function (res, data, callback) {
    console.log(TAG, '@validateCreateObvestiloRequest');

    if (!data){
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing data"
        });
    } else if (!data.datum || !isDatetimeValid(data.datum)) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing or invalid field 'datum'."
        });
    } else if (!data.message) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing message"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateGetObvestiloRequest = function (res, data, callback) {
    console.log(TAG, '@validateGetObvestiloRequest');

    if (!data.id) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing param id"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateGetObvestilaRequest = function (res, data, callback) {
    console.log(TAG, '@validateGetObvestilaRequest');

    // if (data.params.active && !(data.params.active == true || data.params.active == false)) {
    //     callback(res, 405, {
    //         message: "Invalid input",
    //         description: "Missing param id"
    //     });
    // } else {
    // }
    callback(res, 200, data);
};

module.exports.validateUpdateObvestiloRequest = function (res, data, callback) {
    console.log(TAG, '@validateUpdateObvestiloRequest');

    if (!data){
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing data"
        });
    } else if (!data._id || !isIdValid(data._id)) {
        callback(res, 405, {
            message: "Invalid path param",
            description: "Invalid path param 'id'"
        });
    } else if (!data.message) {
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing message"
        });
    } else {
        callback(res, 200, data);
    }
};

module.exports.validateDeleteObvestiloRequest = function (res, data, callback) {
    console.log(TAG, '@validateDeleteObvestiloRequest', data);

    if (!data){
        callback(res, 405, {
            message: "Invalid input",
            description: "Missing data"
        });
    } else if (!data.id || !isIdValid(data.id)) {
        callback(res, 405, {
            message: "Invalid path param",
            description: "Invalid path param 'id'"
        });
    } else {
        callback(res, 200, data);
    }
};

//#region Validate fields functions
var isPasswordValid = function (password) {
    return /.{6,255}$/.test(password);
};

var isEmailValid = function (email) {
    return !email || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
};

var isDatetimeValid = function (datetime) {
    // console.log(TAG, "@isDatetimeValid");
    var d = new Date(datetime);
    return isNaN(d) ? false : true;
};

var isIdValid = function (id) {
    return /^[a-f\d]{24}$/i.test(id.toString());
};

var isTextValid = function (text) {
    // console.log('isTextValid');
    return /^[a-zA-Z0-9#ćđčšžĆĐČŠŽ\.,\-\(\) ]{1,255}$/.test(text);
};

var isTextDescription = function (text) {
    return true;
};
//#endregion
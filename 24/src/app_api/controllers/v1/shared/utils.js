var TAG = "API [app_api/controllers/utils.js]";
var verbose = 1;

module.exports.JSONcallback = function(res, status, data) {
    console.log(TAG, '@JSONcallback');
    if (data && (status >= 400 || [201].includes(status) || verbose >= 2)) {
        console.log(TAG, data);
        if (status >= 500) {
            data = "Internal Server Error";
        }
    }
    res.status(status);
    res.json(data);
};

// fun: el -> acc -> res -> callback:(res -> status -> newAcc)
var foldl = function (fun, acc, list, res, callback) {
    (function next(i) {
        if (!list || i >= list.length) {
            callback(res, 200, acc);
        } else {
            fun(list[i], acc, res, function (res, status, newAcc) {
                if (status >= 400) {
                    callback(res, status, newAcc);
                } else {
                    acc = newAcc;
                    next(i + 1);
                }
            });
        }
    })(0);
};
module.exports.foldl = foldl;

module.exports.getStartOfMonth = function (date) {
    date = new Date(date);
    var firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
    return firstDate;
};

module.exports.getEndOfMonth = function (date) {
    date = new Date(date);
    var lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    return lastDate;
};

module.exports.getStartOfDay = function (date) {
    date = new Date(date);
    var firstDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    return firstDate;
};

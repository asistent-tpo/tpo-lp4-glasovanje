var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var uporabnikiSchema = new mongoose.Schema({
    email: {type: String, unique: true, required: true},
    salt: String,
    hashed_geslo: String,
    vloga: { type: Number, "default": 1, min: 1, max: 3 }
});

uporabnikiSchema.methods.nastaviGeslo = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hashed_geslo = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

uporabnikiSchema.methods.preveriGeslo = function(password) {
    var hashValue = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    console.log("geslo", hashValue == this.hashed_geslo);
    return this.hashed_geslo == hashValue;
};

uporabnikiSchema.methods.generirajJwt = function() {
    var notAfter = new Date();
    notAfter.setDate(notAfter.getDate() + 7); // not valid after
    
    return jwt.sign({
        _id: this._id,
        email: this.email,
        vloga: this.vloga,
        notAfter: parseInt(notAfter.getTime() / 1000, 10)
    }, "geslo123");//process.env.JWT_GESLO);
};

mongoose.model('Uporabnik', uporabnikiSchema, 'Uporabniki');

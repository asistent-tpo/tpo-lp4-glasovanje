var mongoose = require('mongoose');

var obvestilaSchema = new mongoose.Schema({
    message: {type: String, unique: false, required: true},
    datum: {type: Date, required: true},
    aktivno: {type: Boolean, "default": false}
});

mongoose.model('Obvestilo', obvestilaSchema, 'Obvestila');
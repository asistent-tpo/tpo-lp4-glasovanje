var mongoose = require('mongoose');

var dogodkiSchema = new mongoose.Schema({
    naziv: {type: String, unique: false, required: true},
    datum: {type: Date, required: true},
    datumDo: {type: Date, required: false},
    organizator: String,
    opis: { type: String }
});

mongoose.model('Dogodek', dogodkiSchema, 'Dogodki');

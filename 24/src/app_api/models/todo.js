var mongoose = require('mongoose');

var todoSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.ObjectId, required: true},
    opis: { type: String }
});

mongoose.model('Todo', todoSchema, 'Todo');
var express = require('express');
var router = express.Router();
var controller_events = require('../controllers/v1/dogodki');
var controller_todos = require('../controllers/v1/todos');
var controller_db = require('../controllers/v1/db');
var kontrolerUporabniki = require('../controllers/v1/uporabniki');
var kontrolerObvestila = require('../controllers/v1/obvestila');

var jwt = require('express-jwt');
var auth = jwt({
  secret: "geslo123",
  userProperty: 'payload'
});

var authPassThrough = jwt({
  credentialsRequired: false,
  secret: "geslo123",
  userProperty: 'payload',
  function (err, req, res, next) {
    if (err.code === 'invalid_token') {
      console.log('INVALID TOKEN');
      return next();
    }
    return next(err);
  }
});

var TAG = "API [app_api/routes/index.js]";

console.log(TAG);

router.get('/db', controller_db.generate);

router.post('/prijava', kontrolerUporabniki.prijava);
router.post('/registracija', kontrolerUporabniki.registracija);
router.put('/spremenigeslo', kontrolerUporabniki.spremeniGeslo);

router.get("/events/:id", authPassThrough, controller_events.getEvent);
router.get('/events', controller_events.getEvents);
router.post('/events', authPassThrough, controller_events.createEvent);
router.put('/events/:id', authPassThrough, controller_events.updateEvent);
router.delete('/events/:id', authPassThrough, controller_events.deleteEvent);

router.get("/todos/:id", authPassThrough, controller_todos.getTodo);
router.get('/todos', controller_todos.getTodos);
router.post('/todos', authPassThrough, controller_todos.createTodo);
router.put('/todos/:id', authPassThrough, controller_todos.updateTodo);
router.delete('/todos/:id', authPassThrough, controller_todos.deleteTodo);

router.post("/obvestila", authPassThrough, kontrolerObvestila.createObvestilo);
router.put("/obvestila/:id", authPassThrough, kontrolerObvestila.updateObvestilo);
router.delete("/obvestila/:id", authPassThrough, kontrolerObvestila.deleteObvestilo);
router.get("/obvestila/:id", authPassThrough, kontrolerObvestila.getObvestilo);
router.get("/obvestila", authPassThrough, kontrolerObvestila.getObvestila);

module.exports = router;
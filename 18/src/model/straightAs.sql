DROP DATABASE straightAs;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `straightAs` DEFAULT CHARACTER SET utf8 ;
USE `straightAs` ;

-- -----------------------------------------------------
-- Table `straightAs`.`obvestilo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `straightAs`.`obvestilo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sporocilo` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `straightAs`.`vloga`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `straightAs`.`vloga` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `ime_UNIQUE` (`ime` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `straightAs`.`uporabnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `straightAs`.`uporabnik` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `geslo` VARCHAR(45) NOT NULL,
  `obvestilo_id` INT NOT NULL,
  `vloga_id` INT NOT NULL,
  PRIMARY KEY (`id`, `vloga_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_uporabnik_vloga1_idx` (`vloga_id` ASC),
  CONSTRAINT `fk_uporabnik_vloga1`
    FOREIGN KEY (`vloga_id`)
    REFERENCES `straightAs`.`vloga` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `straightAs`.`vnos_todo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `straightAs`.`vnos_todo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `opis` VARCHAR(45) NOT NULL,
  `uporabnik_id` INT NOT NULL,
  PRIMARY KEY (`id`, `uporabnik_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_vnos_todo_uporabnik1_idx` (`uporabnik_id` ASC),
  CONSTRAINT `fk_vnos_todo_uporabnik1`
    FOREIGN KEY (`uporabnik_id`)
    REFERENCES `straightAs`.`uporabnik` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `straightAs`.`vnos_urnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `straightAs`.`vnos_urnik` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `opis` VARCHAR(45) NOT NULL,
  `trajanje` INT NOT NULL,
  `barva` VARCHAR(20) NOT NULL,
  `zacetek` INT NOT NULL,
  `uporabnik_id` INT NOT NULL,
  PRIMARY KEY (`id`, `uporabnik_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_vnos_urnik_uporabnik1_idx` (`uporabnik_id` ASC),
  CONSTRAINT `fk_vnos_urnik_uporabnik1`
    FOREIGN KEY (`uporabnik_id`)
    REFERENCES `straightAs`.`uporabnik` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `straightAs`.`vnos_koledar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `straightAs`.`vnos_koledar` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `opis` VARCHAR(45) NOT NULL,
  `datum` DATETIME NOT NULL,
  `uporabnik_id` INT NOT NULL,
  PRIMARY KEY (`id`, `uporabnik_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_vnos_koledar_uporabnik1_idx` (`uporabnik_id` ASC),
  CONSTRAINT `fk_vnos_koledar_uporabnik1`
    FOREIGN KEY (`uporabnik_id`)
    REFERENCES `straightAs`.`uporabnik` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Vnosi v podatkovno bazo
-- -----------------------------------------------------

INSERT INTO vloga (id, ime)
VALUES (1, 'registrirani');

INSERT INTO vloga (id, ime)
VALUES (2, 'upravljalec');

INSERT INTO vloga (id, ime)
VALUES (3, 'administrator');

INSERT INTO uporabnik (email, geslo, vloga_id) VALUES ('ligenj.przen@gmail.com', '1234', 1);
INSERT INTO uporabnik (email, geslo, vloga_id) VALUES ('admin@gmail.com', '1234', 3);
INSERT INTO uporabnik (email, geslo, vloga_id) VALUES ('upravljalec@gmail.com', '1234', 2);
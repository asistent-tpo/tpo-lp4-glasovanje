var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "straightAs"
});

con.connect(function(err) {
    if (err) console.log("Cannot connect to DB");
    console.log("Connencted to DB");
});

module.exports = {

// vrni vse uporabnike
get_all: function(callback) {
    con.query("SELECT * FROM uporabnik", function (err, res) {
        if (err) throw err;
        callback(res);
    });
},

// vrni uporabnika glede na email
get_uporabnik: function(email, callback) {
    con.query("SELECT * FROM uporabnik WHERE email = ?", email, function (err, res) {
        if (err) throw err;
        callback(res);
    });
},

// sprememba obstoječega uporabnika - userdata: [email, geslo, obvestilo_id, vloga_id, id]
edit_uporabnik: function(userdata, callback) {
    var sql = 'UPDATE uporabnik SET email = ?, geslo = ?, obvestilo_id = ?, vloga_id = ? WHERE id = ?';
    con.query(sql, userdata, (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// vnos novega uporabnika
insert_uporabnik: function(userdata, callback) {
    var sql = 'INSERT INTO uporabnik (email, geslo, vloga_id) VALUES ?';
    con.query(sql, [[userdata]], (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// izbris uporabnika
delete_uporabnik: function(email, callback) {
    var sql = 'DELETE FROM uporabnik WHERE email = ?';
    con.query(sql, email, (err, res) => {
        if(err) throw err;
        callback(res);
    });
}

};
var u = require('./uporabnik');
var vk = require('./vnos_koledar');
var vu = require('./vnos_urnik');
var vt = require('./vnos_todo');
var ob = require('./obvestilo')

// TEST uporabnik
{
function test_get_all_uporabnik() {
    u.get_all(function(dbResp) {
        console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_get_uporabnik(email) {
    u.get_uporabnik(email, function(dbResp) {
        console.log(dbResp);
        // Naredi nekaj s podatki.
        if(dbResp != []){
            return true;
        }
        
        return false;
    });
}

function test_insert_uporabnik(userdata) {
    u.insert_uporabnik(userdata, function(dbResp) {
        //console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_edit_uporabnik(userdata) {
    u.edit_uporabnik(userdata, function(dbResp) {
        //console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_delete_uporabnik(email) {
    console.log("Deleting " + email);
    u.delete_uporabnik(email, function(dbResp) {
        //console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

test_get_all_uporabnik(); // izpis vseh uporabnikov
//test_insert_uporabnik(['burek.majster@gmail.com', 'bedanc', 1]); // vnos novega uporabnika [email, geslo, vloga_id]
//test_edit_uporabnik(['burek.majster@gmail.com', 'spremenjenogeslo', 1, 3]); // sprememba gesla uporabnika [email, geslo, vloga_id, uporabnik_id]
//test_get_uporabnik('burek.majster@gmail.com'); // izpis uporabnika gelede na email
//test_delete_uporabnik('burek.majster@gmail.com'); // izbris uporabnika glede na email
//test_get_all_uporabnik(); // izpis vseh uporabnikov
}

// TEST vnos_todo
{
function test_uporabnikovi_vnosi_todo(uporabnik_id) {
    vt.uporabnikovi_vnosi_todo(uporabnik_id, function(dbResp) {
        console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_vnesi_todo(data) {
    vt.vnesi_todo(data, function(dbResp) {
        //console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_uredi_todo(data) {
    vt.uredi_todo(data, function(dbResp) {
        // console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_izbrisi_todo(id) {
    vt.izbrisi_todo(id, function(dbResp) {
        // console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

//test_vnesi_todo(['To je novo opravilo stevilka 1.', 1]); // nov vnos za uporabnika [opis, uporabnik_id]
//test_uredi_todo(['Spremenjeno opravilo', 1, 2]); // sprememba imena vnosa z id=2 in uporabnikom z id=1 [opis, uporabnik_id, vnos_id]
//test_uporabnikovi_vnosi_todo(1); // izpis vseh vnosov od uporabnika z id=1
//test_izbrisi_todo(2); // izbris vnosa z id=2
//test_uporabnikovi_vnosi_todo(1); // izpis vseh vnosov od uporabnika z id=1
}

// TEST vnos_urnik
{
function test_uporabnikovi_vnosi_urnik(uporabnik_id) {
    vu.uporabnikovi_vnosi_urnik(uporabnik_id, function(dbResp) {
        console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_vnesi_urnik(data) {
    vu.vnesi_urnik(data, function(dbResp) {
        //console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_uredi_urnik(data) {
    vu.uredi_urnik(data, function(dbResp) {
        // console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_izbrisi_urnik(id) {
    vu.izbrisi_urnik(id, function(dbResp) {
        // console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

//test_vnesi_urnik(['Prvi vnos v urnik', 32, 'modra', 1]); // nov vnos za uporabnika [opis, trajanje, barva, uporabnik_id]
//test_uredi_urnik(['Spremeba v urniku', 14, 'rdeca', 1, 1]); // sprememba imena vnosa z id=1 in uporabnikom z id=1 [opis, trajanje, barva, uporabnik_id, vnos_id]
//test_uporabnikovi_vnosi_urnik(1); // izpis vseh vnosov od uporabnika z id=1
//test_izbrisi_urnik(1); // izbris vnosa z id=1
//test_uporabnikovi_vnosi_urnik(1); // izpis vseh vnosov od uporabnika z id=1
}

// TEST vnos_koledar
{
function test_uporabnikovi_vnosi_koledar(uporabnik_id) {
    vk.uporabnikovi_vnosi_koledar(uporabnik_id, function(dbResp) {
        console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_vnesi_koledar(data) {
    vk.vnesi_koledar(data, function(dbResp) {
        //console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_uredi_koledar(data) {
    vk.uredi_koledar(data, function(dbResp) {
        // console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

function test_izbrisi_koledar(id) {
    vk.izbrisi_koledar(id, function(dbResp) {
        // console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}
//test_vnesi_koledar(['Prvi vnos v koledar', '2019-5-15', 1]); // nov vnos za uporabnika [opis, datum, uporabnik_id]
//test_uredi_koledar(['Spremeba v koledarju', '2019-5-16', 1, 1]); // sprememba imena vnosa z id=1 in uporabnikom z id=1 [opis, datum, uporabnik_id, vnos_id]
//test_uporabnikovi_vnosi_koledar(1); // izpis vseh vnosov od uporabnika z id=1
//test_izbrisi_koledar(1); // izbris vnosa z id=1
//test_uporabnikovi_vnosi_koledar(1); // izpis vseh vnosov od uporabnika z id=1
}

function test_vrni_obvsetila() {
    ob.vrni_vsa_obvestila(function(dbResp) {
        console.log(dbResp);
        // Naredi nekaj s podatki.
    });
}

ob.dodaj_obvestilo(["prvo obvestilo"], function(){return});
ob.dodaj_obvestilo(["drugo obvestilo"], function(){return});
test_vrni_obvsetila();
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "straightAs"
});

con.connect(function(err) {
    if (err) console.log("Cannot connect to DB");
    console.log("Connencted to DB");
});

module.exports = {

// vrni vse vnose od uporabnika
// sprejme uporabnikov id
uporabnikovi_vnosi_todo: function(uporabnik_id, callback) {
    con.query("SELECT * FROM vnos_todo WHERE uporabnik_id = ?", uporabnik_id, function (err, res) {
        if (err) throw err;
        callback(res);
    });
},

// sprememba obstoječega vnosa
// sprejme seznam [opis, uporabnik_id, id]
uredi_todo: function(data, callback) {
    var sql = 'UPDATE vnos_todo SET opis = ?, uporabnik_id = ? WHERE id = ?';
    con.query(sql, data, (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// kreiranje novega vnosa
// sprejme seznam [opis, uporabnik_id]
vnesi_todo: function(data, callback) {
    var sql = 'INSERT INTO vnos_todo (opis, uporabnik_id) VALUES ?';
    con.query(sql, [[data]], (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// izbris vnosa
// sprejme id od vnosa, ki ga zelimo izbrisati
izbrisi_todo: function(id, callback) {
    var sql = 'DELETE FROM vnos_todo WHERE id = ?';
    con.query(sql, id, (err, res) => {
        if(err) throw err;
        callback(res);
    });
}

};
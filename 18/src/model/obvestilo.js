var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "straightAs"
});

con.connect(function(err) {
    if (err) console.log("Cannot connect to DB");
    console.log("Connencted to DB");
});

module.exports = {
    
// dodajanje obvestila
dodaj_obvestilo: function(sporocilo, callback) {
    var sql = 'INSERT INTO obvestilo (sporocilo) VALUES ?';
    con.query(sql, [[sporocilo]], (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// vrni vsa obvestila, mlajša od podanega id
vrni_obvestila: function(id, callback) {
    var sql = "SELECT * FROM obvestilo WHERE id > ?"
    con.query(sql, id, function (err, res) {
        if (err) throw err;
        callback(res);
    });
},

// vrni vsa obvestila
vrni_vsa_obvestila: function(callback) {
    con.query("SELECT * FROM obvestilo", function (err, res) {
        if (err) throw err;
        callback(res);
    });
}


    
};
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "straightAs"
});

con.connect(function(err) {
    if (err) console.log("Cannot connect to DB");
    console.log("Connencted to DB");
});

module.exports = {

// vrni vse vnose od uporabnika
// sprejme uporabnikov id
uporabnikovi_vnosi_urnik: function(uporabnik_id, callback) {
    con.query("SELECT * FROM vnos_urnik WHERE uporabnik_id = ?", uporabnik_id, function (err, res) {
        if (err) throw err;
        callback(res);
    });
},

// sprememba obstoječega vnosa
// sprejme seznam [opis, trajanje, barva, uporabnik_id, id]
uredi_urnik: function(data, callback) {
    var sql = 'UPDATE vnos_urnik SET opis = ?, trajanje = ?, barva = ?, uporabnik_id = ? WHERE id = ?';
    con.query(sql, data, (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// vnos novega vnosa v todo list
// sprejme seznam [opis, trajanje, barva, uporabnik_id]
vnesi_urnik: function(data, callback) {
    var sql = 'INSERT INTO vnos_urnik (opis, trajanje, barva, zacetek, uporabnik_id) VALUES ?';
    con.query(sql, [[data]], (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// izbris vnosa
// sprejme zacetek od vnosa, ki ga zelimo izbrisati
izbrisi_urnik: function(id, callback) {
    var sql = 'DELETE FROM vnos_urnik WHERE zacetek = ?';
    con.query(sql, id, (err, res) => {
        if(err) throw err;
        callback(res);
    });
}

};
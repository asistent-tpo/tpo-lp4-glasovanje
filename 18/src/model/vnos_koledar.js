var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "straightAs"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connencted to DB");
});

module.exports = {

// vrni vse vnose od uporabnika
// sprejme uporabnikov id
uporabnikovi_vnosi_koledar: function(uporabnik_id, callback) {
    con.query("SELECT * FROM vnos_koledar WHERE uporabnik_id = ?", uporabnik_id, function (err, res) {
        if (err) throw err;
        callback(res);
    });
},

// sprememba obstoječega vnosa
// sprejme seznam [opis, datum, uporabnik_id, id]
uredi_koledar: function(data, callback) {
    var sql = 'UPDATE vnos_koledar SET opis = ?, datum = ?, uporabnik_id = ? WHERE id = ?';
    con.query(sql, data, (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// kreiranje novega vnosa
// sprejme seznam [opis, datum, uporabnik_id]
vnesi_koledar: function(data, callback) {
    var sql = 'INSERT INTO vnos_koledar (opis, datum, uporabnik_id) VALUES ?';
    con.query(sql, [[data]], (err, res) => {
        if(err) throw err;
        callback(res);
    });
},

// izbris vnosa
// sprejme id od vnosa, ki ga zelimo izbrisati
izbrisi_koledar: function(id, callback) {
    var sql = 'DELETE FROM vnos_koledar WHERE id = ?';
    con.query(sql, id, (err, res) => {
        if(err) throw err;
        callback(res);
    });
}

};
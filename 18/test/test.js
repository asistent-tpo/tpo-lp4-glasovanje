var zahteva = require('supertest')
var streznik = require('../streznik.js')


describe('Začetna stran', function() {
  it ('naslov strani', function(done) {
    zahteva(streznik).get('/').expect(/<h1>Login<\/h1>/i, done);
  });

  it ('povezava na stran za registracijo', function(done) {
    zahteva(streznik).get('/').expect(/<p class="sporocilo">Not registered\? <a href="\/registracija">Register<\/a><\/p>/i, done);
  });
  it ('vnos za prijavo', function(done) {
    zahteva(streznik).get('/').expect(/<input id="emailInput" type="email" name='email' placeholder="Email" required>/i, done);
  });
});

describe('Stran za registracijo', function() {
  it ('naslov strani', function(done) {
    zahteva(streznik).get('/registracija').expect(/<h1>Registration<\/h1>/i, done);
  });

  it ('povezava na stran za prijavo', function(done) {
    zahteva(streznik).get('/registracija').expect(/<p class="sporocilo">Already registered\? <a href="\/">Login<\/a><\/a><\/p>/i, done);
  });
  it ('vnos za registracijo', function(done) {
    zahteva(streznik).get('/registracija').expect(/<input name="pgeslo" type="password" placeholder="Repeat password">/i, done);
  });
});

describe('preostale datoteke', function() {
  it ('skripta za odjemalca', function(done) {
    zahteva(streznik).get('/client.js').expect(/function postSpremembaGesla\(staroGeslo, novoGeslo\){/i, done);
  });

  it ('stran za urejanje profila', function(done) {
    zahteva(streznik).get('/urejanjeProfila').expect(/<input id="newPasswd1" type="password" name='novoGeslo' placeholder="New Password"  required>/i, done);
  });
  it ('stran za restavracije', function(done) {
    zahteva(streznik).get('/Food.html').expect(/<option value="0">Izberi restavracijo:<\/option>/i, done);
  });
  it ('stran za dogodke', function(done) {
    zahteva(streznik).get('/Events.html').expect(/<title>Events<\/title>/i, done);
  });
});

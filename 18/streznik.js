var express = require('express');
var streznik = express();
var port = process.env.PORT || 3000;
var dbUporabnik = require('./src/model/uporabnik.js');
var dbObvestilo = require('./src/model/obvestilo.js');
var dbTodo = require("./src/model/vnos_todo.js");
var dbUrnik = require("./src/model/vnos_urnik.js");
var fs = require("fs");


streznik.use(express.urlencoded({extended: true}));
streznik.use(express.json());

/**
 * Ko uporabnik obišče začetno stran,
 * izpiši začetni pozdrav
 */
 
streznik.get('/login', function(zahteva, odgovor) {
  let vars = pridobiSpremenljivkeIzURL(zahteva.originalUrl);
  preveriLogin(vars.email, vars.geslo, function(odg){
    if(odg != 0){
      //uspesen login
      console.log(odg);
      if(odg.vloga_id == 1){
        //registriran uporabnik
        return odgovor.send('/homepage');
         
      }
      else if(odg.vloga_id == 2){
        return odgovor.send('/eventmanager');
      }
      else if(odg.vloga_id == 3){
        return odgovor.send('/admin');
      }
      
    }
  })
})
streznik.get('/', function (zahteva, odgovor) {
  
  var path = require('path');
  var filePath = path.join(__dirname, 'index.html');
  odgovor.sendFile(
    filePath
  ); 
  
  /*let vars = pridobiSpremenljivkeIzURL(zahteva.originalUrl);
  if(typeof vars['email'] !== 'undefined' && typeof vars['geslo'] !== 'undefined'){
    console.log(vars['email'] + " ni undefined, klicem bazo. ")
    dbUporabnik.get_uporabnik(vars['email'], function(dbOdg){
      if(dbOdg[0] == null){
        console.log("Vnosa s takim emailom ni v bazi. ")
      }
      else{
        if(vars['geslo'] == dbOdg[0].geslo){
          console.log("Login uspešen, geslo je pravilno. ");
          console.log("Vloga: " + dbOdg[0].vloga_id);
          if(dbOdg[0].vloga_id == 1){
            //user
            console.log("posiljam homepage");
            var path = require('path');
            var filePath = path.join(__dirname, 'homePage.html');
            odgovor.sendFile(
              filePath
            );
          }
          else if(dbOdg[0].vloga_id == 3){
            //administrator
            var path = require('path');
            var filePath = path.join(__dirname, 'admin.html');
            odgovor.sendFile(
              filePath
            );
          }
        }
        else{
          console.log("Napačno geslo. ");
          var path = require('path');
          var filePath = path.join(__dirname, 'index.html');
          odgovor.sendFile(
            filePath
          );  
        }
      }
    });  
  }
  else{
    var path = require('path');
    var filePath = path.join(__dirname, 'index.html');
    odgovor.sendFile(
      filePath
    );  
  }
  */
  
  
});

streznik.get('/homepage', function(zahteva, odgovor) {
  var path = require('path');
  var filePath = path.join(__dirname, 'homePage.html');
  odgovor.sendFile(
    filePath
  ); 
});

streznik.get('/homePage.html', function(zahteva, odgovor) {
  var path = require('path');
  var filePath = path.join(__dirname, 'homePage.html');
  odgovor.sendFile(
    filePath
  ); 
});

streznik.post('/checklogin', function(zahteva, odgovor) {
    preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
        if(uporabnik == 0){
          odgovor.send("0");
        }
        else{
          odgovor.send("1");
        }
    })
})

streznik.get('/registracija', function (zahteva, odgovor) {

  let vars = pridobiSpremenljivkeIzURL(zahteva.originalUrl);
  console.log(vars);
  if(typeof vars['email'] !== 'undefined' && typeof vars['geslo'] !== 'undefined' && typeof vars['pgeslo'] !== 'undefined'){
    console.log("Vsi podatki preneseni. ");
    if(vars['geslo'] != vars['pgeslo']){
      console.log("Geslo in ponovljeno geslo se ne ujemata. ");
    }
    else{
      dbUporabnik.get_uporabnik(vars['email'], function(dbOdg){
        if(dbOdg[0] != null){
          console.log('Uporabnik s takim emailom ze obstaja. ');
        }
        else{
          console.log("Vnasam uporabnika v bazo. ");
          dbUporabnik.insert_uporabnik([vars['email'], vars['geslo'], 1], function(dbOdg){
            
          });    
        }
      });
      
    }
  }
  
  var path = require('path');
  var filePath = path.join(__dirname, 'registracija.html');
  odgovor.sendFile(
    filePath
  );
});

streznik.get('/admin', function (zahteva, odgovor) {
  
  var path = require('path');
  var filePath = path.join(__dirname, 'admin.html');
  odgovor.sendFile(
    filePath
  );
});




streznik.get('/client.js', function(zahteva, odgovor) {
  console.log("posiljam client skripto. ");
  var path = require('path');
  var filePath = path.join(__dirname, 'client.js');
  odgovor.sendFile(
    filePath
  );
});

streznik.get('/urejanjeProfila', function(zahteva, odgovor){
  var path = require('path');
  var filePath = path.join(__dirname, 'urejanjeProfila.html');
  odgovor.sendFile(
    filePath
  );
});

streznik.get('/Food.html', function(zahteva, odgovor){
  var path = require('path');
  var filePath = path.join(__dirname, 'Food.html');
  odgovor.sendFile(
    filePath
  );
});

streznik.get('/Bus.html', function(zahteva, odgovor){
  var path = require('path');
  var filePath = path.join(__dirname, 'Bus.html');
  odgovor.sendFile(
    filePath
  );
});

streznik.get('/Events.html', function(zahteva, odgovor){
  var path = require('path');
  var filePath = path.join(__dirname, 'Events.html');
  odgovor.sendFile(
    filePath
  );
});


streznik.post('/', function(zahteva, odgovor) {
  console.log("dobil post request");
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(vloga){
    console.log(zahteva.body);
    var path = require('path');
    var filePath = path.join(__dirname, 'homePage.html');
    odgovor.sendFile(
      filePath
    );
  });
});

streznik.post('/urejanjeProfila', function(zahteva, odgovor){
  console.log("dobil post request za spremembo gesla. ");
  
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
     if(uporabnik == 0){
       return odgovor.send("0");
     } 
     
     dbUporabnik.edit_uporabnik([zahteva.body.email, zahteva.body.novoGeslo, uporabnik.obvestilo_id, uporabnik.vloga_id, uporabnik.id], function(dbOdg2){
       return odgovor.send('/');
     });
  });
  
});


streznik.post('/gettodo', function(zahteva, odgovor) {
  
    console.log('Dobil get todo post request za: ' + zahteva.body.email);
    preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
      if(uporabnik != 0 && uporabnik.vloga_id == 1){
        //registriran uporabnik
        dbTodo.uporabnikovi_vnosi_todo(uporabnik.id, function(vnosi){
          odgovor.send(vnosi);
        });
      }
      else{
        return;
      }
  });
});

streznik.post('/addtodo', function(zahteva, odgovor){
  console.log('Dobil add todo post request za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
      if(uporabnik != 0 && uporabnik.vloga_id == 1){
        //registriran uporabnik
        dbTodo.vnesi_todo([zahteva.body.vsebina, uporabnik.id], function(){
          odgovor.send();
        });
      }
      else{
        return;
      }
  });
});

streznik.post('/deletetodo', function(zahteva, odgovor) {
  console.log('Dobil delete todo post request za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
      if(uporabnik != 0 && uporabnik.vloga_id == 1){
        //registriran uporabnik
        dbTodo.izbrisi_todo([zahteva.body.id], function(){
          odgovor.send();
        });
      }
      else{
        return;
      }
  });
});

streznik.post('/updatetodo', function(zahteva, odgovor) {
  console.log('Dobil delete todo post request za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
      if(uporabnik != 0 && uporabnik.vloga_id == 1){
        //registriran uporabnik
        dbTodo.uredi_todo([zahteva.body.vsebina, uporabnik.id, zahteva.body.id], function(){
          odgovor.send();
        });
      }
      else{
        return;
      }
  });
});

streznik.post('/getadminnotes', function(zahteva, odgovor) {
  console.log('Dobil get admin notes post request za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
      if(uporabnik != 0 && uporabnik.vloga_id == 1){
        //registriran uporabnik
        dbObvestilo.vrni_obvestila(uporabnik.obvestilo_id, function(dbOdg){
          if(dbOdg.length > 0){
            dbUporabnik.edit_uporabnik([uporabnik.email, uporabnik.geslo, dbOdg[dbOdg.length - 1].id, uporabnik.vloga_id, uporabnik.id], function(dbOdg2){
              odgovor.send(dbOdg);
            }) 
          }
          else{
            odgovor.send();
          }
        });
      }
      else{
        return;
      }
  });
});

streznik.post('/postadminnote', function(zahteva, odgovor){
  console.log('Dobil post admin note zahtevo za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
      if(uporabnik != 0 && uporabnik.vloga_id == 3){
        //admin
        dbObvestilo.dodaj_obvestilo([zahteva.body.sporocilo], function(dbOdg){
          odgovor.send("ok");
        });
      }
      else{
        odgovor.send("not ok");
      }
  });
});

streznik.post('/addura', function(zahteva, odgovor) {
  console.log('Dobil add ura post request za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
    if(uporabnik == 0){
      return odgovor.send("0");
    }
    dbUrnik.vnesi_urnik([zahteva.body.opis, 1, zahteva.body.barva, zahteva.body.zacetek, uporabnik.id], function(dbOdg){
      return odgovor.send("1");
    });
  });
});

streznik.post('/deleteura', function(zahteva, odgovor) {
   console.log('Dobil delete ura post request za: ' + zahteva.body.email);
   preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
    if(uporabnik == 0){
      return odgovor.send("0");
    }
    dbUrnik.izbrisi_urnik(zahteva.body.zacetek, function(dbOdg){
      return odgovor.send("1");
    })
  });
});

streznik.post('/geturnik', function(zahteva, odgovor) {
  console.log('Dobil get urnik post request za: ' + zahteva.body.email);
  preveriLogin(zahteva.body.email, zahteva.body.geslo, function(uporabnik) {
    if(uporabnik == 0){
      return odgovor.send("0");
    }
    dbUrnik.uporabnikovi_vnosi_urnik(uporabnik.id, function(dbOdg){
      if(dbOdg.length > 0)
        return odgovor.send(dbOdg);
      else
        return odgovor.send("0");
    });
  });
});

/**
 * Poženi strežnik
 */
streznik.listen(port, function () {
  console.log('Strežnik je pognan na portu ' + port + '!');
});


module.exports = streznik;


//funkcija iz URLja ekstrahira vse podane spremenljivke v dictionary obliki
function pridobiSpremenljivkeIzURL(URL){
  URL = (URL.split('?'))[1];
  URL = decodeURIComponent(URL);
  
  let args = URL.split('&');
  var vars = {};
  for(var i = 0; i < args.length; i++){
    let s = args[i].split('=');
    vars[s[0]] = s[1];
  }
  return vars;
}


//najde uporabnika z email v bazi, preveri ce je pravilno geslo,
//vrne uporabnika ali 0, ce login ni uspesen
function preveriLogin(email, geslo, callback){
  if(typeof email !== 'undefined' && typeof geslo !== 'undefined'){
    //console.log(email + " ni undefined, klicem bazo. ")
    dbUporabnik.get_uporabnik(email, function(dbOdg){
      if(dbOdg[0] == null){
        //console.log("Vnosa s takim emailom ni v bazi. ")
        callback(0);
      }
      else{
        if(geslo == dbOdg[0].geslo){
          //console.log("Login uspešen, geslo je pravilno. ");
          //console.log("Vloga: " + dbOdg[0].vloga_id);
          callback(dbOdg[0]);
        }
        else{
          //console.log("Napačno geslo. ");
          callback(0);
        }
      }
    });  
  }
  else{
    callback(0);
  }
}




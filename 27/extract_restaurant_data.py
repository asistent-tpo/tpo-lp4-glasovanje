# import os
import urllib.request
import pandas as pd
from time import sleep

# os.environ['DJANGO_SETTINGS_MODULE'] = 'straightas.settings'
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "straightas.settings")

from bs4 import BeautifulSoup

# from home.models import Restavracija

soup = BeautifulSoup(open('imenik_lokalov.html'), 'html.parser')
res = soup.findAll("div", {"class": "restaurant-row"})

data_dict = {
    'ime': [],
    'naslov': [],
    'odpiralni_cas': [],
    'doplacilo': [],
    'lat': [],
    'long': []

}

for i, r in enumerate(res):
    print('Processing restaurant {}/{}'.format(i, len(res)))
    full_info_url = 'https://www.studentska-prehrana.si' + r['data-detailslink']
    request = urllib.request.Request(full_info_url)
    try:
        response = urllib.request.urlopen(request)
    except:
        print("something wrong")
    html_string = (response.read().decode('utf8'))
    sleep(1)  # Dont spam the site
    soup2 = BeautifulSoup(html_string, 'html.parser')
    res2 = soup2.findAll('div', {'class': 'col-md-12 text-bold'})

    found = False
    for thing in res2:
        if ':' in thing.text:
            odpiralni_cas = ""
            for text2 in thing.text.strip().split('\n'):
                if ':' in text2:
                    dan = text2.strip().split(':')[0].strip()
                    timeindex = text2.index(':')
                    cas = text2[timeindex + 1:].strip()
                    odpiralni_cas += "{}: {}\n".format(dan, cas)
                    found = True
            if found:
                break

    if found:
        odpiralni_cas = odpiralni_cas[:-1]
    else:
        odpiralni_cas = ""
    cena = r['data-cena']
    mesto = r['data-city']
    doplacilo = r['data-doplacilo']
    latitude = float(r['data-lat'])
    longitude = float(r['data-lon'])
    ime = r['data-lokal']
    naslov = r['data-naslov']

    data_dict['ime'].append(ime)
    data_dict['naslov'].append('{}, {}'.format(naslov, mesto))
    data_dict['odpiralni_cas'].append(odpiralni_cas)
    data_dict['doplacilo'].append(doplacilo)
    data_dict['lat'].append(latitude)
    data_dict['long'].append(longitude)
    continue
    restavracija_django_obj = Restavracija(
        ime=ime,
        naslov="{}, {}".format(naslov, mesto),
        odpiralni_cas="scrape by visiting individual pages with timeout",
        doplacilo=doplacilo
    )
    restavracija_django_obj.save()
    print(ime, cena, naslov, mesto, doplacilo)

df = pd.DataFrame.from_dict(data_dict)
df.to_csv('restavracije.csv', index=False)

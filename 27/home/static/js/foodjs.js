var map;
var geocoder;
var address = "Ljubljana";

function codeAddress(geocoder, map) {
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
              }
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }


  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: {lat: 46.0569, lng: 14.5058}
    });
    geocoder = new google.maps.Geocoder();
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        // infoWindow.setPosition(pos);
        // infoWindow.setContent('Location found.');
        // infoWindow.open(map);
        map.setCenter(pos);
        showUserLocationMarker(map, pos);
      }, function() {});
    }
  }

function showUserLocationMarker(map_obj, user_loc) {
  var marker = new google.maps.Marker({
          map: map_obj,
          position: user_loc,
          icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
          }
  });
}

function displayRestaurantInfo(restaurant_name_str, addr, opens, cost) {
    document.getElementById("restaurantname").innerHTML = "Restaurant: <strong>" + restaurant_name_str + "</strong>";
    document.getElementById("restaurantaddr").innerHTML = "Address: <strong>" + addr + "</strong>";
    document.getElementById("restaurantopens").innerHTML = "Opening time: <strong>" + opens + "</strong>";
    document.getElementById("restaurantcost").innerHTML = "Cost: <strong>" + cost + "</strong>";
    address = addr;
    codeAddress(geocoder, map);
}
from math import sqrt

from django.shortcuts import render, HttpResponse, redirect, get_object_or_404, render_to_response
from django.template import RequestContext
from django.urls import reverse_lazy, reverse
from django.contrib.auth.models import Group
from . import models
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from . import forms
from .forms import SignupForm, NewLoginForm, KoledarForm, UrnikForm, TODOForm
from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required, user_passes_test

from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
import json


def prikazi_obvestila(request):
    vsa_obvestila = models.AdministratorskaObvestila.objects.all()
    vsa_obvestila_index = []
    for i in vsa_obvestila:
        vsa_obvestila_index.append(False)

    prikazana_sporocila = []
    prikazana_sporocila_indices = []
    index_obvestil = 0
    for o in vsa_obvestila:
        sporocilo = "Warning: " + o.sporocilo
        piskotki = request.COOKIES

        for cookie_name in piskotki.keys():
            if (sporocilo == piskotki[cookie_name]):
                vsa_obvestila_index[index_obvestil] = True
                # print("Found cookie in list")

        if (vsa_obvestila_index[index_obvestil] == False):
            messages.warning(request, sporocilo)
            prikazana_sporocila.append(sporocilo)
            prikazana_sporocila_indices.append(index_obvestil)

        index_obvestil += 1

    return prikazana_sporocila, prikazana_sporocila_indices


def obvestila_set_cookies(response, prikazana_sporocila, prikazana_sporocila_indices):
    index_sp = 0
    for sporocilo in prikazana_sporocila:
        response.set_cookie(str(prikazana_sporocila_indices[index_sp]), sporocilo)
        # print("cookie name: ", str(prikazana_sporocila_indices[index_sp]),   "  cookies set: ", sporocilo )
        index_sp += 1


def is_registered(user):
    return user.groups.filter(name='registriran').exists()


def is_upravljalec(user):
    return user.groups.filter(name='upravljalec').exists()


def is_admin(user):
    return user.groups.filter(name='admin').exists()


def is_not_logged_in(user):
    return user.is_anonymous


def index(request):
    return render(request=request, template_name='index.html')
    

@login_required(login_url='/register')
@user_passes_test(is_registered)
def dogodki(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    response = render(request=request,
                      template_name='EventsPogled.html',
                      context={'dogodki': models.Dogodek.objects.all})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
def delete_on_post(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    template = "HomePogled.html"

    if request.method == "POST":
        print(request.POST.get('podatki'))

    response = redirect('/home')
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def home_pogled(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    response = render(request=request,
                      template_name="HomePogled.html",
                      context={'koledarski_vnosi': models.Koledar.objects.filter(author=request.user),
                               'urnik_vnosi': models.Urnik.objects.filter(author=request.user),
                               'todo_vnosi': models.TODO.objects.filter(author=request.user)})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


def food_pogled(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    all_objs = models.Restavracija.objects.all()
    restaurants_sorted = sorted(all_objs, key=lambda h: h.ime)
    # distances = [sqrt((r.lat - 46.051120) ** 2 + (r.long - 14.467210) ** 2) for r in all_objs]
    #     # sort_idx = [i[0] for i in sorted(enumerate(distances), key=lambda x: x[1])]
    #     # restaurants_sorted = [all_objs[i] for i in sort_idx]
    response = render(request=request,
                      template_name='FoodPogled.html',
                      context={'restavracije': restaurants_sorted})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
@user_passes_test(is_upravljalec)
def dodajanjedogodkov(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    if request.method == "POST":
        form = forms.DodajanjeDogodkaForm(request.POST)
        if form.is_valid():
            # Naredimo nov dogodek
            ime = form.cleaned_data.get('ime')
            datum = form.cleaned_data.get('datum')
            organizator = form.cleaned_data.get('organizator')
            opis = form.cleaned_data.get('opis')
            nov_dogodek = models.Dogodek(ime=ime, datum=datum, organizator=organizator, opis=opis)
            nov_dogodek.save()  # in ga shranimo v bazo

    form = forms.DodajanjeDogodkaForm()
    response = render(request=request,
                      template_name="UpravljalcevPogled.html",
                      context={"form": form,
                               'dogodki': models.Dogodek.objects.all})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
@user_passes_test(is_admin)
def dodajanjeobvestil(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    if request.method == "POST":
        form = forms.DodajanjeObvestilaForm(request.POST)
        if form.is_valid():
            # Naredimo nov dogodek
            sporocilo = form.cleaned_data.get('sporocilo')
            novo_obvestilo = models.AdministratorskaObvestila(sporocilo=sporocilo)
            novo_obvestilo.save()  # in ga shranimo v bazo
        return redirect('/dodajanjeobvestil')

    form = forms.DodajanjeObvestilaForm()
    response = render(request=request,
                      template_name="AdminPogled.html",
                      context={"form": form})

    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@user_passes_test(is_not_logged_in)
def register(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    if request.method == "POST":
        # form = NewUserForm(request.POST)
        form = SignupForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            # Oznacimo ga kot registriran uporabnik, admin in upravljalec sta rocno dodana v http://127.0.0.1:8000/admin
            registered_group = Group.objects.get(name='registriran')
            new_user.groups.add(registered_group)
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your StraightAs account.'
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('username')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            messages.info(request, "Please confirm your email address to complete the registration")
        else:
            response = render(request=request,
                              template_name="register.html",
                              context={"form": form})
            obvestila_set_cookies(response, obvestila, obvestila_indices)
            return response

    form = SignupForm()
    response = render(request=request,
                      template_name="register.html",
                      context={"form": form})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@user_passes_test(is_not_logged_in)
def login_request(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    if request.method == "POST":
        form = NewLoginForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}")
                response = redirect("StraightAs:index")
                obvestila_set_cookies(response, obvestila, obvestila_indices)
                return response
            else:
                messages.error(request, "Invalid username or password")
        else:
            messages.error(request, "Invalid username or password")

    form = NewLoginForm()
    response = render(request,
                      template_name="login.html",
                      context={"form": form})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


def logout_request(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    logout(request)
    messages.info(request, "Logged out successfully!")
    response = redirect("StraightAs:index")
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
def change_password(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect("StraightAs:index")
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)

    response = render(request, 'ChangePassword.html', {'form': form})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@user_passes_test(is_not_logged_in)
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.success(request, f"Thank you for your email confirmation")
        return redirect('StraightAs:index')
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')


@login_required(login_url='/register')
@user_passes_test(is_registered)
def calendar_create_view(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    form = KoledarForm(request.POST or None)
    if form.is_valid():
        # Naredimo nov dogodek
        ime = form.cleaned_data.get('ime')
        datum = form.cleaned_data.get('datum')
        opis = form.cleaned_data.get('opis')
        nov_dogodek = models.Koledar(author=request.user, ime=ime, datum=datum, opis=opis)
        nov_dogodek.save()  # in ga shranimo v bazo
        response = redirect('/home')
        obvestila_set_cookies(response, obvestila, obvestila_indices)
        return response
    context = {
        'form': form
    }
    response = render(request, "KoledarForm.html", context)
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def calendar_update_view(request, id):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    dogodek = get_object_or_404(models.Koledar, id=id)
    form = KoledarForm(request.POST or None, instance=dogodek)
    if form.is_valid():
        dogodek.ime = form.cleaned_data.get('ime')
        dogodek.datum = form.cleaned_data.get('datum')
        dogodek.opis = form.cleaned_data.get('opis')
        dogodek.save()
        return redirect('/home')

    action_url = reverse('StraightAs:calendarEdit', args=[id])
    context = {
        'form': form,
        'actionUrl': action_url
    }
    response = render(request, "KoledarFormEdit.html", context)
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def calendar_specific_day_view(request, dan):  # day je oblike 2019-05-23 npr.
    user_events = models.Koledar.objects.filter(author=request.user)
    day_events = []
    for event in user_events:
        if dan in str(event.datum):
            day_events.append(event)
    context = {
        'vnosi': day_events
    }
    return render(request, "KoledarSpecific.html", context)


@login_required(login_url='/register')
@user_passes_test(is_registered)
def calendar_delete_view(request, id):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    obj = get_object_or_404(models.Koledar, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('/home')

    action_url = reverse('StraightAs:calendarDelete', args=[id])
    context = {
        'actionUrl': action_url
    }
    response = render(request, "DeleteForm.html", context)
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def timetable_create_view(request, dan, ura):
    if request.method == 'POST':
        form = UrnikForm(request.POST or None)
        if form.is_valid():
            ime = form.cleaned_data.get('ime')
            barva = form.cleaned_data.get('barva')
            trajanje = form.cleaned_data.get('trajanje')
            vnos = models.Urnik(author=request.user, ime=ime, dan=dan, ura=ura, barva=barva, trajanje=trajanje)
            vnos.save()
            return redirect('/home')
        else:
            action_url = reverse('StraightAs:timetableCreate', args=[dan, ura])
            return redirect(action_url)

    data = {'trajanje': 1, 'ime': 'Name', 'ura': ura}
    form = UrnikForm(initial=data)

    action_url = reverse('StraightAs:timetableCreate', args=[dan, ura])
    context = {
        'form': form,
        'action_url': action_url,
        'ura': ura,
        'dan': dan,
        'vnosi': models.Urnik.objects.filter(author=request.user)
    }
    return render(request, "TimetableForm.html", context)


@login_required(login_url='/register')
@user_passes_test(is_registered)
def timetable_update_view(request, id):
    vnos = get_object_or_404(models.Urnik, id=id)
    form = UrnikForm(request.POST or None, instance=vnos)

    if request.method == 'POST':
        if form.is_valid():
            vnos.ime = form.cleaned_data.get('ime')
            vnos.barva = form.cleaned_data.get('barva')
            vnos.trajanje = form.cleaned_data.get('trajanje')
            vnos.save()
            return redirect('/home')
        else:
            action_url = reverse('StraightAs:timetableEdit', args=[id])
            return redirect(action_url)

    action_url = reverse('StraightAs:timetableEdit', args=[id])
    context = {
        'form': form,
        'action_url': action_url,
        'ura': vnos.ura,
        'dan': vnos.dan,
        'vnosi': models.Urnik.objects.filter(author=request.user)
    }
    response = render(request, "TimetableFormEdit.html", context)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def timetable_delete_view(request, id):
    obj = get_object_or_404(models.Urnik, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('/home')

    action_url = reverse('StraightAs:timetableDelete', args=[id])
    context = {
        'actionUrl': action_url
    }
    response = render(request, "DeleteForm.html", context)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def todo_create_view(request):
    form = TODOForm(request.POST or None)
    if form.is_valid():
        opis = form.cleaned_data.get('opis')
        todo = models.TODO(author=request.user, opis=opis)
        todo.save()
        response = redirect('/home')
        return response

    context = {
        'form': form
    }
    response = render(request, "TodoForm.html", context)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def todo_update_view(request, id):
    todo = get_object_or_404(models.TODO, id=id)
    form = TODOForm(request.POST or None, instance=todo)
    if form.is_valid():
        todo.opis = form.cleaned_data.get('opis')
        todo.save()
        return redirect('/home')

    action_url = reverse('StraightAs:todoEdit', args=[id])
    delete_url = reverse('StraightAs:todoDelete', args=[id])
    context = {
        'form': form,
        'actionUrl': action_url,
        'deleteUrl': delete_url
    }
    response = render(request, "TodoFormEdit.html", context)
    return response


@login_required(login_url='/register')
@user_passes_test(is_registered)
def todo_delete_view(request, id):
    obj = get_object_or_404(models.TODO, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('/home')

    action_url = reverse('StraightAs:todoDelete', args=[id])
    context = {
        'actionUrl': action_url
    }
    response = render(request, "DeleteForm.html", context)
    return response


def bus_search_view(request):
    obvestila, obvestila_indices = prikazi_obvestila(request)
    stations = models.Avtobus.objects.all()
    # autocomplete_data_dict = {}
    auto_data = []
    for station in stations:
        # autocomplete_data_dict[station.naziv] = None
        # autocomplete_data_dict[station.id_postaje] = None
        if str(station.naziv) not in auto_data:
            auto_data.append(str(station.naziv))
        if str(station.id_postaje) not in auto_data:
            auto_data.append(str(station.id_postaje))

    # autocomplete_data = json.dumps(autocomplete_data_dict, ensure_ascii=False).encode('utf8')
    # autocomplete_data = json.dumps(autocomplete_data_dict)
    # print(auto_data)

    response = render(request=request,
                      template_name="BusPogled.html",
                      context={'postaje': stations, 'autocomplete_data': auto_data})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


def pretty_request(request):
    headers = ''
    for header, value in request.META.items():
        if not header.startswith('HTTP'):
            continue
        header = '-'.join([h.capitalize() for h in header[5:].lower().split('_')])
        headers += '{}: {}\n'.format(header, value)

    return (
        '{method} HTTP/1.1\n'
        'Content-Length: {content_length}\n'
        'Content-Type: {content_type}\n'
        '{headers}\n\n'
        '{body}'
    ).format(
        method=request.method,
        content_length=request.META['CONTENT_LENGTH'],
        content_type=request.META['CONTENT_TYPE'],
        headers=headers,
        body=request.body,
    )


def bus_search_query_view(request, query):
    obvestila, obvestila_indices = prikazi_obvestila(request)

    postaje = models.Avtobus.get_bus_arrival_data(query)
    error = postaje is None
    response = render(request=request,
                      template_name="BusResults.html",
                      context={'postaje': postaje, "error": error})
    obvestila_set_cookies(response, obvestila, obvestila_indices)
    return response


def restaurant_dropdown_reqeust(request, location):
    locs = location.split("$")
    lat = float(locs[0])
    lon = float(locs[1])

    all_objs = models.Restavracija.objects.all()

    distances = [sqrt((r.lat - lat) ** 2 + (r.long - lon) ** 2) for r in all_objs]
    sort_idx = [i[0] for i in sorted(enumerate(distances), key=lambda x: x[1])]
    restaurants_sorted = [all_objs[i] for i in sort_idx]

    response = render(request=request,
                      template_name="RestaurantDropdown.html",
                      context={'restavracije': restaurants_sorted})
    return response

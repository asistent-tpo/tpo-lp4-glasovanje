from django.contrib import admin
from . import models


# Register your models here.

class DogodekAdmin(admin.ModelAdmin):
    fields = ['ime', 'organizator', 'datum', 'opis']

class UrnikAdmin(admin.ModelAdmin):
    fields = ['author', 'ime', 'barva', 'dan', 'ura']


class AdministratorskaObvestilaAdmin(admin.ModelAdmin):
    fields = ['datum', 'sporocilo']


admin.site.register(models.Dogodek, DogodekAdmin)
admin.site.register(models.AdministratorskaObvestila, AdministratorskaObvestilaAdmin)
admin.site.register(models.Urnik, UrnikAdmin)
admin.site.register(models.Koledar)
admin.site.register(models.TODO)
admin.site.register(models.Restavracija)
admin.site.register(models.Avtobus)


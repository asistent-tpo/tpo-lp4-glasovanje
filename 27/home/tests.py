from django.test import SimpleTestCase, TransactionTestCase, TestCase
from django.db.utils import IntegrityError
from django.urls import reverse_lazy, reverse
from django.contrib.auth.models import User, Group
from django.contrib import auth
from django.utils import timezone

from . import models


class PagesTests(TestCase):
    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_page_text(self):
        response = self.client.get('/')
        self.assertContains(response, 'Welcome to StraightAS!!!')

    def test_restaurant_html(self):
        response = self.client.get('/restavracije')
        self.assertContains(response, 'restaurant_dropdown')
        self.assertContains(response, 'restaurantname')
        self.assertContains(response, 'restaurantaddr')
        self.assertContains(response, 'restaurantopens')
        self.assertContains(response, 'restaurantcost')

# helper function
def create_groups(Group):
    Group.objects.get_or_create(name='registriran')
    Group.objects.get_or_create(name='admin')
    Group.objects.get_or_create(name='upravljalec')


class DatabaseTests(TransactionTestCase):

    def test_registration_exception1(self):
        # Uporabnik se je poskusil registrirati z e-poštnim naslovom, ki je že v uporabi.
        # Aplikacija vrne obvestilo o zavrnjeni registraciji.

        # Preverimo, ali to vrne IntegrityError
        with self.assertRaises(IntegrityError):
            user = User.objects.create_user('user1@gmail.com',
                                            password='securepassword123')
            user.save()
            user = User.objects.create_user('user1@gmail.com',
                                            password='securepassword123')
            user.save()

    def test_registration_too_short_password(self):
        create_groups(Group)
        credentials = {'username': 'user1@gmail.com', 'password1': '123', 'password2': '123'}
        self.client.post('/register', credentials, follow=True)

        self.assertEquals(len(User.objects.filter(username='user1@gmail.com')), 0)

    def test_registration_password_mismatch(self):
        credentials = {'username': 'user1@gmail.com', 'password1': '46465465444646', 'password2': 'asdfadsfasdfadsf'}
        self.client.post('/register', credentials, follow=True)

        self.assertEquals(len(User.objects.filter(username='user1@gmail.com')), 0)

    def test_registration_is_active(self):
        Group.objects.get_or_create(name='registriran')

        credentials = {'username': 'user1@gmail.com', 'password1': '46465465444646', 'password2': '46465465444646'}
        self.client.post('/register', credentials, follow=True)

        self.assertEquals(User.objects.filter(username='user1@gmail.com')[0].is_active, False)

    def test_change_password_success(self):
        user = User.objects.create_user('user1@gmail.com',
                                        password='securepassword123')
        user.save()
        # Assume the password was correctly entered twice
        user.set_password('new_pass')
        user.save()
        self.assertEquals(user.check_password('new_pass'), True)

    def test_change_password_too_short_password(self):
        create_groups(Group)

        user = User.objects.create_user(username='user1@gmail.com',
                                        password='securepassword123')

        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'old_password': 'securepassword123', 'new_password1': '123', 'new_password2': '123'}
        self.client.post('/changepassword', data, follow=True)
        self.assertEquals(user.check_password('securepassword123'), True)

    def test_change_password_password_mismatch(self):
        user = User.objects.create_user(username='user1@gmail.com',
                                        password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'old_password': 'securepassword123', 'new_password1': 'asdfaadfsdfas',
                'new_password2': 'uzuzuzuzuzuzuzuz'}
        self.client.post('changepassword', data, follow=True)
        self.assertEquals(user.check_password('securepassword123'), True)

    def test_login(self):
        user = User.objects.create_user(username='user1@gmail.com',
                                        password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        response = self.client.post('/login', credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)

    def test_refuse_login_bad_password(self):
        user = User.objects.create_user('user1@gmail.com', password='securepassword123')
        user.save()
        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword1234'}
        response = self.client.post('/login', credentials, follow=True)
        self.assertFalse(response.context['user'].is_active)

    def test_refuse_login_bad_email(self):
        user = User.objects.create_user('user1@gmail.com', password='securepassword123')
        user.save()
        credentials = {'username': 'asdf@gmail.com', 'password': 'securepassword123'}
        response = self.client.post('/login', credentials, follow=True)
        self.assertFalse(response.context['user'].is_active)

    # uspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': '2019-06-25 19:30:00', 'organizator': 'Studentski svet FRI',
                'opis': 'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 1)

    # neuspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': '', 'datum': '2019-06-25 19:30:00', 'organizator': 'Studentski svet FRI',
                'opis': 'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='')), 0)

    # neuspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': 'random_String', 'organizator': 'Studentski svet FRI',
                'opis': 'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 0)

    # neuspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': '2019-06-25 19:30:00', 'organizator': '',
                'opis': 'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 0)

    # neuspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_fail_4(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': '2019-06-25 19:30:00', 'organizator': 'Studentski svet FRI',
                'opis': ''}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 0)

    # neuspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_fail_5(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='admin')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': '2019-06-25 19:30:00', 'organizator': 'Studentski svet FRI',
                'opis': 'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 0)

    # neuspesno dodajanje dogodkov, s strani upravljalca
    def test_upravljalec_add_event_fail_6(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': '2019-06-25 19:30:00', 'organizator': 'Studentski svet FRI',
                'opis': 'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 0)


    def test_logout(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        response = self.client.post('/login', credentials, follow=True)

        response = self.client.post('/logout', follow=True)
        self.assertFalse(response.context['user'].is_active)


    def test_upravljalec_add_event_fail_no_name(self):
        from django.contrib.auth.models import User
        from django.contrib.auth.models import Group
        from . import models

        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()
        
        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Event1', 'datum': 'a', 'organizator': 'Studentski svet FRI', 'opis':'Zabava diplomantov'}
        self.client.post('/dodajanjedogodkov', data, follow=True)

        self.assertEquals(len(models.Dogodek.objects.filter(ime='Event1')), 0)


    # uspesno dodajanje sistemskih obvestil, s strani admina
    def test_admin_dodajanje_obvestil_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='admin')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'sporocilo': 'Testno sporocilo'}
        self.client.post('/dodajanjeobvestil', data, follow=True)

        self.assertEquals(len(models.AdministratorskaObvestila.objects.filter(sporocilo='Testno sporocilo')), 1)

    # neuspesno dodajanje sistemskih obvestil
    def test_admin_dodajanje_obvestil_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'sporocilo': 'Testno sporocilo'}
        self.client.post('/dodajanjeobvestil', data, follow=True)

        self.assertEquals(len(models.AdministratorskaObvestila.objects.filter(sporocilo='Testno sporocilo')), 0)

    # neuspesno dodajanje sistemskih obvestil
    def test_admin_dodajanje_obvestil_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'sporocilo': 'Testno sporocilo'}
        self.client.post('/dodajanjeobvestil', data, follow=True)

        self.assertEquals(len(models.AdministratorskaObvestila.objects.filter(sporocilo='Testno sporocilo')), 0)

    # uspesno dodajanje sistemskih obvestil, s strani admina
    def test_admin_dodajanje_obvestil_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='admin')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'sporocilo': ''}
        self.client.post('/dodajanjeobvestil', data, follow=True)

        self.assertEquals(len(models.AdministratorskaObvestila.objects.filter(sporocilo='')), 0)

    # uspesno dodajanje na koledar, s strani registriranega
    def test_registriran_add_koledar_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Koledar1')), 1)

    def test_registriran_add_koledar_failure(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-065-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Koledar1')), 0)

    def test_registriran_add_koledar_failure2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00'}
        self.client.post('/calendarCreate', data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Koledar1')), 0)

    def test_add_calendar_database(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()
        cal_entry = models.Koledar(author=user, ime='k1', datum=timezone.now(), opis='ayy lmao')
        cal_entry.save()
        self.assertEquals(len(models.Koledar.objects.filter(ime='k1')), 1)

    def test_add_calendar_database_fail(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()
        cal_entry = models.Koledar(author=user, ime='k1', datum=timezone.now(), opis='ayy lmao')
        cal_entry.save()
        self.assertEquals(len(models.Koledar.objects.filter(ime='k2')), 0)

    # uspesno brisanje iz koledarja, s strani registriranega
    def test_registriran_delete_koledar_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        id = models.Koledar.objects.filter(ime='Koledar1')[0].id
        data = {}
        action_url = reverse('StraightAs:calendarDelete', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Koledar1')), 0)

    def test_registriran_delete_koledar_fail(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        data = {}
        action_url = reverse('StraightAs:calendarDelete', args=[12949421])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Koledar1')), 1)

    def test_registriran_delete_koledar_fail2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        data = {}
        action_url = reverse('StraightAs:calendarDelete', args=[12949421])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Koledar1')), 0)

    # uspesno urejanje koledarja, s strani registriranega
    def test_registriran_edit_koledar_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        id = models.Koledar.objects.filter(ime='Koledar1')[0].id
        data = {'ime': 'Spreminjanje1', 'datum': '2019-06-25 19:30:00', 'opis': 'Test spreminjanja'}
        action_url = reverse('StraightAs:calendarEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Koledar.objects.filter(ime='Spreminjanje1')), 1)

    # uspesno urejanje koledarja, s strani registriranega
    def test_registriran_edit_koledar_succ2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        id = models.Koledar.objects.filter(ime='Koledar1')[0].id
        data = {'ime': 'Spreminjanje1', 'datum': '2019-06-25 19:30:00', 'opis': 'Test spreminjanja'}
        action_url = reverse('StraightAs:calendarEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(models.Koledar.objects.filter(ime='Spreminjanje1')[0].opis, 'Test spreminjanja')

    def test_registriran_edit_koledar_fail(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Koledar1', 'datum': '2019-06-25 19:30:00', 'opis': 'Testni vnos v koledar'}
        self.client.post('/calendarCreate', data, follow=True)

        id = models.Koledar.objects.filter(ime='Koledar1')[0].id
        data = {'ime': 'Spreminjanje1', 'datum': '2019-06-25 19:30:00', 'opis': 'Test spreminjanja'}
        action_url = reverse('StraightAs:calendarEdit', args=[321])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(models.Koledar.objects.filter(ime='Koledar1')[0].opis, 'Testni vnos v koledar')


    # uspesno dodajanje na urnik, s strani registriranega
    def test_registriran_add_urnik_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99', 'trajanje': 1, 'ura': 8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 1)

    # neuspesno dodajanje na urnik, s strani registriranega
    def test_registriran_add_urnik_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje':'', 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 0)

    # neuspesno dodajanje na urnik, s strani registriranega
    def test_registriran_add_urnik_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje': 1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 0)

    # neuspesno dodajanje na urnik, s strani registriranega
    def test_registriran_add_urnik_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '','trajanje': 1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 0)

    # uspesno brisanje iz urnika, s strani registriranega
    def test_registriran_delete_urnik_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99', 'trajanje': 1, 'ura': 8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        id = models.Urnik.objects.filter(ime='Urnik1')[0].id
        data = {}
        action_url = reverse('StraightAs:timetableDelete', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 0)


    # neuspesno brisanje iz urnika, s strani registriranega
    def test_registriran_delete_urnik_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje':1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        urnik_objects = models.Koledar.objects.filter(ime='Urnik2')
        self.assertEquals(len(urnik_objects), 0)

    # neuspesno brisanje iz urnika, s strani registriranega
    def test_registriran_delete_urnik_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje':1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        id = models.Urnik.objects.filter(ime='Urnik1')[0].id + 1
        data = {}
        action_url = reverse('StraightAs:timetableDelete', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 1)

    # uspesno brisanje iz urnika, s strani registriranega
    def test_registriran_edit_urnik_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99', 'trajanje': 1, 'ura': 8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        id = models.Urnik.objects.filter(ime='Urnik1')[0].id
        data = {'ime': 'UrnikSpremenjen', 'barva': '40AD99', 'trajanje': 1, 'ura': 8}
        action_url = reverse('StraightAs:timetableEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='UrnikSpremenjen')), 1)

    # neuspesno brisanje iz urnika, s strani registriranega
    def test_registriran_edit_urnik_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje':1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        id = models.Urnik.objects.filter(ime='Urnik1')[0].id
        data = {'ime': 'UrnikSpremenjen', 'barva': '40AD99','trajanje':"a", 'ura':8}
        action_url = reverse('StraightAs:timetableEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 1)
        self.assertEquals(len(models.Urnik.objects.filter(ime='UrnikSpremenjen')), 0)

    # neuspesno brisanje iz urnika, s strani registriranega
    def test_registriran_edit_urnik_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje':1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        id = models.Urnik.objects.filter(ime='Urnik1')[0].id
        data = {'ime': 'UrnikSpremenjen', 'barva': '','trajanje': 1, 'ura': 2}
        action_url = reverse('StraightAs:timetableEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 1)
        self.assertEquals(len(models.Urnik.objects.filter(ime='UrnikSpremenjen')), 0)

    # neuspesno brisanje iz urnika, s strani registriranega
    def test_registriran_edit_urnik_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'ime': 'Urnik1', 'barva': '40AD99','trajanje':1, 'ura':8}
        action_url = reverse('StraightAs:timetableCreate', args=[0, 8])
        self.client.post(action_url, data, follow=True)

        data = {'ime': 'Urnik2', 'barva': '40AD99','trajanje':2, 'ura':1}
        action_url = reverse('StraightAs:timetableCreate', args=[1, 1])
        self.client.post(action_url, data, follow=True)

        id = models.Urnik.objects.filter(ime='Urnik2')[0].id
        data = {'ime': 'UrnikSpremenjen', 'barva': '40AD99','trajanje': 1, 'ura': 2}
        action_url = reverse('StraightAs:timetableEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.Urnik.objects.filter(ime='Urnik1')), 1)
        self.assertEquals(len(models.Urnik.objects.filter(ime='UrnikSpremenjen')), 1)

    # uspesno dodajanje na todo, s strani registriranega
    def test_registriran_add_todo_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis': 'Testni vnos v todo'}
        self.client.post('/todoCreate', data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Testni vnos v todo')), 1)

    # neuspesno dodajanje na todo, s strani registriranega
    def test_registriran_add_todo_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'' }
        self.client.post('/todoCreate', data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Testni vnos v todo')), 0)

    # neuspesno dodajanje na todo, s strani registriranega
    def test_registriran_add_todo_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos' }
        self.client.post('/todoCreate', data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Testni vnos v todo')), 0)

    # neuspesno dodajanje na todo, s strani registriranega
    def test_registriran_add_todo_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='admin')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos' }
        self.client.post('/todoCreate', data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Testni vnos v todo')), 0)

    # uspesno brisanje iz todo, s strani registriranega
    def test_registriran_delete_todo_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis': 'Testni vnos v todo'}
        self.client.post('/todoCreate', data, follow=True)

        id = models.TODO.objects.filter(opis='Testni vnos v todo')[0].id
        data = {}
        action_url = reverse('StraightAs:todoDelete', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Testni vnos v todo')), 0)

    # uspesno brisanje iz todo, s strani registriranega
    def test_registriran_delete_todo_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos v todo' }
        self.client.post('/todoCreate', data, follow=True)

        todo_objects = models.TODO.objects.filter(opis='Testnaaaa vnos v todo')
        self.assertEquals(len(todo_objects), 0)

    # uspesno brisanje iz todo, s strani registriranega
    def test_registriran_delete_todo_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'' }
        self.client.post('/todoCreate', data, follow=True)

        todo_objects = models.TODO.objects.filter(opis='')
        self.assertEquals(len(todo_objects), 0)

    # uspesno brisanje iz todo, s strani registriranega
    def test_registriran_delete_todo_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='upravljalec')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos' }
        self.client.post('/todoCreate', data, follow=True)

        todo_objects = models.TODO.objects.filter(opis='Testni vnos')
        self.assertEquals(len(todo_objects), 0)

    # uspesno urejanje todo, s strani registriranega
    def test_registriran_edit_todo_success(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis': 'Testni vnos v todo'}
        self.client.post('/todoCreate', data, follow=True)

        id = models.TODO.objects.filter(opis='Testni vnos v todo')[0].id
        data = {'opis': 'Spremenjen vnos v todo'}
        action_url = reverse('StraightAs:todoEdit', args=[id])
        self.client.post(action_url, data, follow=True)
        self.assertEquals(len(models.TODO.objects.filter(opis='Spremenjen vnos v todo')), 1)
        self.assertEquals(len(models.TODO.objects.filter(opis='Spremenjen vnos v todo')), 1)


    # neuspesno urejanje todo, s strani registriranega
    def test_registriran_edit_todo_fail_1(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos v todo' }
        self.client.post('/todoCreate', data, follow=True)

        todo_object = models.TODO.objects.filter(opis='')
        self.assertEquals(len(todo_object), 0)

    # uspesno urejanje todo, s strani registriranega
    def test_registriran_edit_todo_fail_2(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos v todo' }
        self.client.post('/todoCreate', data, follow=True)

        id = models.TODO.objects.filter(opis='Testni vnos v todo')[0].id + 1
        data = {'opis':'Spremenjen vnos v todo'}
        action_url = reverse('StraightAs:todoEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Spremenjen vnos v todo')), 0)

    # uspesno urejanje todo, s strani registriranega
    def test_registriran_edit_todo_fail_3(self):
        user = User.objects.create_user(username='user1@gmail.com', password='securepassword123')
        create_groups(Group)
        group = Group.objects.get(name='registriran')
        user.groups.add(group)
        user.save()

        credentials = {'username': 'user1@gmail.com', 'password': 'securepassword123'}
        self.client.post('/login', credentials, follow=True)

        data = {'opis':'Testni vnos v todo' }
        self.client.post('/todoCreate', data, follow=True)

        id = models.TODO.objects.filter(opis='Testni vnos v todo')[0].id
        data = {'opis':''}
        action_url = reverse('StraightAs:todoEdit', args=[id])
        self.client.post(action_url, data, follow=True)

        self.assertEquals(len(models.TODO.objects.filter(opis='Spremenjen vnos v todo')), 0)


    def test_restaurant_model_test3(self):
        r = models.Restavracija(ime="Nekej", naslov="TutNekej", odpiralni_cas="8:30", doplacilo="230", lat=14.0, long=13.0)
        r.save()
        r = models.Restavracija(ime="aaaaa", naslov="TutNekej", odpiralni_cas="8:30", doplacilo="230", lat=14.0, long=13.0)
        r.save()
        self.assertEquals(len(models.Restavracija.objects.all()), 2)
    
    def test_restaurant_model_test23(self):
        r = models.Restavracija(ime="Nekej", naslov="TutNekej", odpiralni_cas="8:30", doplacilo="230", lat=14.0, long=13.0)
        r.save()
        self.assertEquals(len(models.Restavracija.objects.filter(ime='Nekej')), 1)

    def test_bus_model_test12(self):
        b = models.Avtobus(naziv="Nekej", id_postaje=27)
        b.save()
        b = models.Avtobus(naziv="aaa", id_postaje=13)
        b.save()
        self.assertEquals(len(models.Avtobus.objects.all()), 2)

    def test_bus_model_test2(self):
        b = models.Avtobus(naziv="Nekej", id_postaje=27)
        b.save()
        self.assertEquals(len(models.Avtobus.objects.filter(naziv='Nekej')), 1)
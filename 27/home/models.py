import urllib
from bs4 import BeautifulSoup
# import json

from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator, MaxLengthValidator
from django.db import models
from django.utils import timezone
from colorfield.fields import ColorField
from django.contrib.auth.models import User


# Create your models here.


class Dogodek(models.Model):
    ime = models.CharField(max_length=200)
    datum = models.DateTimeField('datum dogodka', default=timezone.now)
    organizator = models.CharField(max_length=200)
    opis = models.TextField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return '[{}] {} by {}'.format(self.datum, self.ime, self.organizator)


class Urnik(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    ime = models.CharField(max_length=200)
    barva = ColorField(default='40AD99')
    # dan: 0 = ponedeljek, 6 = nedelja
    dan = models.IntegerField(default=0, validators=[MaxValueValidator(6), MinValueValidator(0)])
    # 0 = 0:00, 23=23:00
    ura = models.IntegerField(default=8, validators=[MaxValueValidator(23), MinValueValidator(0)])
    trajanje = models.IntegerField(default=1)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return 'uporabnik: {} ima na dan {} ob {} aktivnost {}'.format(self.author, self.dan, self.ura, self.ime)


class Koledar(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    ime = models.CharField(max_length=200)
    datum = models.DateTimeField('datum vnosa', default=timezone.now)
    opis = models.TextField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return 'uporabnik: {} ima na {} dogodek {}'.format(self.author, self.datum, self.ime)


class TODO(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    opis = models.TextField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return 'uporabnik: {}'.format(self.author)


class AdministratorskaObvestila(models.Model):
    sporocilo = models.CharField(max_length=500)
    datum = models.DateTimeField('cas objave', default=timezone.now)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return '[{}] {}'.format(self.datum, self.sporocilo)


class Restavracija(models.Model):
    ime = models.CharField(max_length=200)
    naslov = models.CharField(max_length=200)
    odpiralni_cas = models.TextField(max_length=1000, blank=True, validators=[MaxLengthValidator(1000)])
    doplacilo = models.CharField(max_length=10)
    lat = models.FloatField()
    long = models.FloatField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return 'Restavracija: {}, {}, odprta: {}, doplacilo: {}'.format(self.ime, self.naslov, self.odpiralni_cas,
                                                                        self.doplacilo)


class Avtobus(models.Model):
    naziv = models.CharField(max_length=100)
    id_postaje = models.IntegerField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return 'Avtobusna postaja: id:{}, naziv: {}'.format(self.id_postaje, self.naziv)

    @staticmethod
    def get_bus_arrival_data(query):
        full_info_url = 'https://www.trola.si/' + str(query) + '/'
        print(full_info_url)
        request = urllib.request.Request(full_info_url)
        try:
            response = urllib.request.urlopen(request)
        except:
            print("Couldnt get response")
            return None

        html_string = (response.read().decode('utf8'))
        error1 = 'ostaje s tem imenom'
        error2 = 'odatki trenutno niso dosegljiv'
        if error1 in html_string or error2 in html_string:
            print("Error message")
            return None

        soup = BeautifulSoup(html_string, 'html.parser')
        titles = soup.findAll('h1')
        tables = soup.findAll('tbody')

        stations = []

        query_split = "".join(str(query).split("%20"))
        query_len = len(query_split)
        isnum = query_split.isdigit()

        for i in range(1, len(titles)):
            title = titles[i].get_text()
            title_split = "".join(str(title).split(" ")[1:])
            if not isnum and len(title_split) != query_len:
                continue
            # print(title)
            data = {'ime': title, 'busi': []}
            # print(str(tables[i-1]))
            soup_child = BeautifulSoup(str(tables[i - 1]), 'html.parser')
            tds = soup_child.findAll('td')

            for j in range(0, len(tds), 3):
                bus = {}
                num = tds[j].get_text().strip()
                arrivals = tds[j + 2].get_text()
                end = tds[j + 1].get_text()
                bus['num'] = num
                bus['arrivals'] = arrivals
                bus['end'] = end

                data['busi'].append(bus)

            stations.append(data)

        # stations_json = json.dumps(stations)
        # print(stations_json)
        return stations




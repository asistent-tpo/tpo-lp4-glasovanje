from django import forms
from django.utils import timezone
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import Koledar, Urnik, TODO, Dogodek
from colorfield.fields import ColorField


class DodajanjeDogodkaForm(forms.ModelForm):
    class Meta:
        model = Dogodek
        fields = ["ime", "datum", "organizator", "opis"]


class DodajanjeObvestilaForm(forms.Form):
    sporocilo = forms.CharField(widget=forms.Textarea)


class SignupForm(UserCreationForm):
    username = forms.EmailField(required=True, label='Email')

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2')


class NewLoginForm(AuthenticationForm):
    username = forms.EmailField(required=True, label='Email')

    class Meta:
        model = User
        fields = ("username", "password")

    def save(self, commit=True):
        user = super(NewLoginForm, self).save(commit=False)
        if commit:
            user.save()
        return user


class KoledarForm(forms.ModelForm):
    class Meta:
        model = Koledar
        fields = ["ime", "datum", "opis"]


class UrnikForm(forms.ModelForm):
    class Meta:
        model = Urnik
        fields = ["ime", "trajanje", "barva"]


class TODOForm(forms.ModelForm):
    class Meta:
        model = TODO
        fields = ["opis"]

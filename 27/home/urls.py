from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'StraightAs'

urlpatterns = [
    path('', views.index, name='index'),
    path('register', views.register, name='register'),
    path('logout', views.logout_request, name='logout'),
    path('login', views.login_request, name='login'),
    path('changepassword', views.change_password, name='change_password'),
    path('dogodki', views.dogodki, name='dogodki'),
    path('dodajanjedogodkov', views.dodajanjedogodkov, name='dodajanjedogodkov'),
    path('dodajanjeobvestil', views.dodajanjeobvestil, name='dodajanjeobvestil'),
    path('home', views.home_pogled, name='home_pogled'),
    path('restavracije', views.food_pogled, name='food_pogled'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
    path('delete_me', views.delete_on_post, name='delete_on_post'),
    path('calendarCreate', views.calendar_create_view, name='calendarCreate'),
    path('<int:id>/calendarEdit', views.calendar_update_view, name='calendarEdit'),
    path('<int:id>/calendarDelete', views.calendar_delete_view, name='calendarDelete'),
    path('timetableCreate/<int:dan>/<int:ura>', views.timetable_create_view, name='timetableCreate'),
    path('calendarSpecific/<str:dan>', views.calendar_specific_day_view, name='calendarSpecific'),
    path('<int:id>/timetableEdit', views.timetable_update_view, name='timetableEdit'),
    path('<int:id>/timetableDelete', views.timetable_delete_view, name='timetableDelete'),
    path('todoCreate', views.todo_create_view, name='todoCreate'),
    path('<int:id>/todoEdit', views.todo_update_view, name='todoEdit'),
    path('<int:id>/todoDelete', views.todo_delete_view, name='todoDelete'),
    path('bus', views.bus_search_view, name='bus'),
    path('busQuery/<str:query>', views.bus_search_query_view, name='busQuery'),
    path('restaurants/loc/<str:location>', views.restaurant_dropdown_reqeust, name='restLocation')
]

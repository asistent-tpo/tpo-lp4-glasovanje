# 27. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Spletna povezava do različice produkcijske postavitve aplikacije: straightass.herokuapp.com



Spletna povezava do uveljavitve: https://bitbucket.org/davidnabergoj/tpo-lp4/commits/cc61764df0dd2f79c7fa6cd01fa966a14f0f0058



Uporabniško ime: student@tpo.com

Geslo: geslo123

import urllib
import json
from bs4 import BeautifulSoup


def get_bus_arrival_data(query):
    full_info_url = 'https://www.trola.si/' + str(query) + '/'
    request = urllib.request.Request(full_info_url)
    try:
        response = urllib.request.urlopen(request)
    except:
        print("Couldnt get response")
        return None

    html_string = (response.read().decode('utf8'))
    error1 = 'ostaje s tem imenom'
    error2 = 'odatki trenutno niso dosegljiv'
    if error1 in html_string or error2 in html_string:
        print("Error message")
        return None

    soup = BeautifulSoup(html_string, 'html.parser')
    titles = soup.findAll('h1')
    tables = soup.findAll('tbody')

    stations = []

    for i in range(1, len(titles)):
        title = titles[i].get_text()
        # print(title)
        data = {'ime': title, 'busi': []}
        # print(str(tables[i-1]))
        soup_child = BeautifulSoup(str(tables[i-1]), 'html.parser')
        tds = soup_child.findAll('td')

        for j in range(0, len(tds), 3):
            bus = {}
            num = tds[j].get_text().strip()
            arrivals = tds[j+2].get_text()
            end = tds[j+1].get_text()
            bus['num'] = num
            bus['arrivals'] = arrivals
            bus['end'] = end

            data['busi'].append(bus)

        stations.append(data)


    stations_json = json.dumps(stations)
    # print(stations_json)
    return stations_json


get_bus_arrival_data('fuzine')

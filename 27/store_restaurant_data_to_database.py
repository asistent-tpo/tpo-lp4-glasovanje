import pandas as pd
from django.shortcuts import get_object_or_404

from home.models import Restavracija

df = pd.read_csv('restavracije.csv')
for ime, naslov, odpiralni_cas, doplacilo, lat, long in df.values:
    print(ime, odpiralni_cas, doplacilo)
    print(naslov)
    # restavracija_django_obj = get_object_or_404(Restavracija, ime=ime, naslov=naslov)
    # restavracija_django_obj.odpiralni_cas = odpiralni_cas
    restavracija_django_obj = Restavracija(
        ime=ime,
        naslov=naslov,
        odpiralni_cas=odpiralni_cas,
        doplacilo=doplacilo,
        lat=lat,
        long=long
    )
    restavracija_django_obj.save()
    print('-' * 40)

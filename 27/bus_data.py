import pandas as pd

from home.models import Avtobus


def preprocess_data():
    df = pd.read_csv('postaje.csv')
    data_dict = {
        'ime' : [],
        'id': []
    }

    for ime, id in df.values:
        data_dict['ime'].append(ime.lower())
        data_dict['id'].append(id)
        print(ime.lower(), id)

    df = pd.DataFrame.from_dict(data_dict)
    df.to_csv('processed_postaje.csv', index=False)


def save_to_database():
    df = pd.read_csv('processed_postaje.csv')
    for ime, id in df.values:
        postaja = Avtobus(naziv=ime, id_postaje=id)
        print(ime, id)
        postaja.save()
        print('-' * 40)


# preprocess_data()
save_to_database()
/* global angular */
var straightasApp = angular.module('straightas', []);

var userListCtrl = function($scope) {
    $scope.data = {
        users:
        [
            {
              "name": "Testni Uporabnik 1",
              "email": "testni1@email.com"  
            },
            {
              "name": "Testni Uporabnik 2",
              "email": "testni2@email.com"  
            }
        ]
    }
}

var straightasData = function() {
    return [{
        "name": "Testni Uporabnik",
        "email": "testni@email.com"
    }]
};

var userList = function($scope, straightasData) {
    $scope.data = {
        user: straightasData
    };
};


straightasApp
    .controller('userListCtrl', userListCtrl)
    .service('straightasData', straightasData);
var izracunajDan = function(dd, mm, yyyy) {
    var prejLeto = false;
    if (mm == 1 || mm == 2) {
        prejLeto = true;
    }
    var mesec = (mm + 9) % 12 + 1;
    //console.log(mesec);
    var D = yyyy % 100;
    if (prejLeto)
        D -= 1;
    var C = Math.floor(yyyy / 100);

    var prviNavzdolZaokrozenDel = Math.floor((13 * mesec - 1)/5);
    var drugiNavzdolZaokrozenDel = Math.floor(D / 4);
    var tretjiNavzdolZaokrozenDel = Math.floor(C / 4);

    var dan = dd + prviNavzdolZaokrozenDel + D + drugiNavzdolZaokrozenDel + tretjiNavzdolZaokrozenDel - 2 * C;
    dan = dan % 7;
    if (dan < 0) {
        dan += 7;
    }

    return dan;
}

var deleteMarkedTask = function(stevec) {
    //console.log("delete " + stevec)
}

var lastHovered = null

var vrniLastHovered = function() {
    return lastHovered;
}

document.onmouseover = function(e) {
    var targ;
    if (!e) var e = window.event;
    if (e.target) targ = e.target;
    else if (e.srcElement) targ = e.srcElement;
    if (targ.nodeType == 3) // defeat Safari bug
        targ = targ.parentNode;
    //console.log(targ.id);
    lastHovered = targ.id
}

var stDnevovVMesecu = function(mesec, leto) {
    var stDnevov = 0;
    if (mesec == 4 || mesec == 6 || mesec == 9 || mesec == 11)  
        stDnevov = 30;  
    else if (mesec == 2)  { 
        var jePrestopno = (leto % 4 == 0 && leto % 100 != 0) || (leto % 400 == 0);  
        if (jePrestopno)  
            stDnevov = 29;  
        else  
            stDnevov = 28;  
    }  
    else  
        stDnevov = 31;
    return stDnevov;
}

var nastaviDanId = function(dan) {
    var danZacKoledar = "pon";
    switch(dan) {
            case 0:
                danZacKoledar = "ned";
                break;
            case 1:
                danZacKoledar = "pon";
                break;
            case 2:
                danZacKoledar = "tor";
                break;
            case 3:
                danZacKoledar = "sre";
                break;
            case 4:
                danZacKoledar = "cet";
                break;
            case 5:
                danZacKoledar = "pet";
                break;
            case 6:                
                danZacKoledar = "sob";
                break;
        }

    return danZacKoledar;
}

var nastaviMesec = function(mesec) {
    if (document.getElementById("koledar_mesec") != null) {
        switch(mesec) {
            case 1:
                document.getElementById("koledar_mesec").innerHTML = "Januar";
                break;
            case 2:
                document.getElementById("koledar_mesec").innerHTML = "Februar";
                break;
            case 3:
                document.getElementById("koledar_mesec").innerHTML = "Marec";
                break;
            case 4:
                document.getElementById("koledar_mesec").innerHTML = "April";
                break;
            case 5:
                document.getElementById("koledar_mesec").innerHTML = "Maj";
                break;
            case 6:
                document.getElementById("koledar_mesec").innerHTML = "Junij";
                break;
            case 7:
                document.getElementById("koledar_mesec").innerHTML = "Julij";
                break;
            case 8:
                document.getElementById("koledar_mesec").innerHTML = "Avgust";
                break;
            case 9:
                document.getElementById("koledar_mesec").innerHTML = "September";
                break;
            case 10:
                document.getElementById("koledar_mesec").innerHTML = "Oktober";
                break;
            case 11:
                document.getElementById("koledar_mesec").innerHTML = "November";
                break;
            case 12:
                document.getElementById("koledar_mesec").innerHTML = "December";
                break;        
        }
    }
}

var nastaviDatum = function(dd, mm, yyyy) {
    if (document.getElementById("koledar_datum") != null) {
        switch(mm) {
            case 1:
                document.getElementById("koledar_datum").innerHTML = dd + ". Januar " + yyyy;
                break;
            case 2:
                document.getElementById("koledar_datum").innerHTML = dd + ". Februar " + yyyy;
                break;
            case 3:
                document.getElementById("koledar_datum").innerHTML = dd + ". Marec " + yyyy;
                break;
            case 4:
                document.getElementById("koledar_datum").innerHTML = dd + ". April " + yyyy;
                break;
            case 5:
                document.getElementById("koledar_datum").innerHTML = dd + ". Maj " + yyyy;
                break;
            case 6:
                document.getElementById("koledar_datum").innerHTML = dd + ". Junij " + yyyy;
                break;
            case 7:
                document.getElementById("koledar_datum").innerHTML = dd + ". Julij " + yyyy;
                break;
            case 8:
                document.getElementById("koledar_datum").innerHTML = dd + ". Avgust " + yyyy;
                break;
            case 9:
                document.getElementById("koledar_datum").innerHTML = dd + ". September " + yyyy;
                break;
            case 10:
                document.getElementById("koledar_datum").innerHTML = dd + ". Oktober " + yyyy;
                break;
            case 11:
                document.getElementById("koledar_datum").innerHTML = dd + ". November " + yyyy;
                break;
            case 12:
                document.getElementById("koledar_datum").innerHTML = dd + ". December " + yyyy;
                break;        
        }
    }
}

var nastaviDan = function(id) {
    //console.log(id.substring(1));
    var dan = id.substring(1);
    if (document.getElementById("koledar_dan") != null) {
        if (dan == "pon") {
            document.getElementById("koledar_dan").innerHTML = "ponedeljek";
        }
        else if (dan == "tor") {
            document.getElementById("koledar_dan").innerHTML = "torek";
        }
        else if (dan == "sre") {
            document.getElementById("koledar_dan").innerHTML = "sreda";
        }
        else if (dan == "cet") {
            document.getElementById("koledar_dan").innerHTML = "četrtek";
        }
        else if (dan == "pet") {
            document.getElementById("koledar_dan").innerHTML = "petek";
        }
        else if (dan == "sob") {
            document.getElementById("koledar_dan").innerHTML = "sobota";
        }
        else if (dan == "ned") {
            document.getElementById("koledar_dan").innerHTML = "nedelja";
        }
    }
}

var curDatum = "112018";
var enkratSpremeni = true;
var dd = 1;
var mm = 1;
var yyyy = 2018;
var idPrej = "";
var classPrej = "";
var preslikovalnaZaDneve = new Map();
var dogodkiVDomu = new Map();

var vrniCurMesec = function() {
    return mm;
}

var vrniCurLeto = function() {
    return yyyy;
}

var pobrisiDogodkeKol = function() {
    dogodkiVDomu = new Map();
}

var izdelajKoledar = function() {
    //console.log("izdelaj koledar");
    var today = new Date();
    dd = today.getDate();
    mm = today.getMonth()+1;
    yyyy = today.getFullYear();
    curDatum = "" + dd + mm + yyyy;
    enkratSpremeni = true;
    if (dogodkiVDomu == null)
        dogodkiVDomu = new Map();
    //dodajDogodekVKoledar("18-10-2018", "neko srečanje");
    //dodajDogodekVKoledar("31-10-2018", "neko srečanje");
    //dodajDogodekVKoledar("18-10-2018", "Proba dogodka");
    //dodajDogodekVKoledar("30-10-2018", "Še zadnji dan pred prazniki :)");
    //("1-11-2018", "Dan spomina na mrtve");
    //("31-12-2018", "Proba");
    pobrisiPrikazDogodkov();
    if (dogodkiVDomu.get(dd + "-" + mm + "-" + yyyy) != null) {
        prikaziDogodek(dogodkiVDomu.get(dd + "-" + mm + "-" + yyyy));
    }

    spremeniKoledar();
}


var spremeniKoledar = function() {
    //console.log("spremeni koledar");

    //mm = 10;
    //yyyy = 2019;

    //console.log("funkcija se izvede");
    //pobrisiPrikazDogodkov();

    nastaviMesec(mm);

    if (enkratSpremeni) {

        nastaviDatum(dd, mm, yyyy);
        //enkratSpremeni = false;
    }

    var dan = izracunajDan(dd, mm, yyyy);

    if (enkratSpremeni) {
        if (document.getElementById("koledar_dan") != null) {
            switch(dan) {
                case 0:
                    document.getElementById("koledar_dan").innerHTML = "nedelja";
                    danKoledar = "ned";
                    break;
                case 1:
                    document.getElementById("koledar_dan").innerHTML = "ponedeljek";
                    danKoledar = "pon";
                    break;
                case 2:
                    document.getElementById("koledar_dan").innerHTML = "torek";
                    danKoledar = "tor";
                    break;
                case 3:
                    document.getElementById("koledar_dan").innerHTML = "sreda";
                    danKoledar = "sre";
                    break;
                case 4:
                    document.getElementById("koledar_dan").innerHTML = "četrtek";
                    danKoledar = "cet";
                    break;
                case 5:
                    document.getElementById("koledar_dan").innerHTML = "petek";
                    danKoledar = "pet";
                    break;
                case 6:
                    document.getElementById("koledar_dan").innerHTML = "sobota";
                    danKoledar = "sob";
                    break;
            }
        }
        enkratSpremeni = false;
    }

    //NAPOLNI KOLEDAR DNEVE

    var stRowKoledar = 1;

    var danStKoledar = izracunajDan(1, mm, yyyy);
    var povecaj = false;
    if (danStKoledar == 0)
        povecaj = true;


    //PREJSNJI DNEVI
    var prevMesec = mm - 1;
    var prevLeto = yyyy;
    if (prevMesec == 0) {
        prevMesec = 12;
        prevLeto--;
    }

    //console.log(stDnevovVMesecu(prevMesec, prevLeto));
    var danPrevMonth = stDnevovVMesecu(prevMesec, prevLeto);

    var danStKoledarZaPrej = (danStKoledar + 7) % 8;
    for (var i = danStKoledarZaPrej; i > 0; --i) {
        var danKoledar = nastaviDanId(i);
        //console.log(danKoledar);

        var id = "" + stRowKoledar + danKoledar;
        //console.log(id);
        if (document.getElementById(id + "") != null) {
            document.getElementById(id + "").innerHTML = danPrevMonth;
            preslikovalnaZaDneve.set(id + "", danPrevMonth);
            preslikovalnaZaDneve.set(id + "mesec", prevMesec);
            document.getElementById(id + "").className = "prev-month koledar_dnevi";
            if (dogodkiVDomu.get(danPrevMonth + "-" + prevMesec + "-" + prevLeto) != null) {
                document.getElementById(id + "").className = "event prev-month koledar_dnevi";
            }
        }

        danPrevMonth--;
    }


    //ZDEJSNJI DNEVI
    for (var i = 1; i <= stDnevovVMesecu(mm, yyyy); ++i) {
        var danKoledar = nastaviDanId(danStKoledar);

        var id = "" + stRowKoledar + danKoledar;
        //console.log(id);
        if (document.getElementById(id + "") != null) {
            document.getElementById(id + "").innerHTML = i;
            preslikovalnaZaDneve.set(id + "", i);
            preslikovalnaZaDneve.set(id + "mesec", mm);
            document.getElementById(id + "").className = "koledar_dnevi";
            if (i == dd && ("" + dd + mm + yyyy) == curDatum) {
                document.getElementById(id + "").className = "current-day event koledar_dnevi";
            } else if (dogodkiVDomu.get(i + "-" + mm + "-" + yyyy) != null) {
                document.getElementById(id + "").className = "event koledar_dnevi";
            }
        }

        if (povecaj) {
            stRowKoledar++;
            povecaj = false;
        }
        danStKoledar++;
        if (danStKoledar > 6) {
            danStKoledar = 0;
            povecaj = true;
        }
    }

    var naslMesec = mm + 1;
    var naslLeto = yyyy;
    if (naslMesec > 12) {
        naslMesec = 1;
        naslLeto += 1;
    }
    //NASLEDNJI DNEVI
    var i = 1;
    while (stRowKoledar <= 6 || danStKoledar < 7) {
        var danKoledar = nastaviDanId(danStKoledar);

        var id = "" + stRowKoledar + danKoledar;
        //console.log(id);
        if (document.getElementById(id + "") != null) {
            document.getElementById(id + "").innerHTML = i;
            preslikovalnaZaDneve.set(id + "", i);
            preslikovalnaZaDneve.set(id + "mesec", naslMesec);
            document.getElementById(id + "").className = "next-month koledar_dnevi";
            if (dogodkiVDomu.get(i + "-" + naslMesec + "-" + naslLeto) != null) {
                document.getElementById(id + "").className = "event prev-month koledar_dnevi";
            }
        }

        if (povecaj) {
            stRowKoledar++;
            povecaj = false;
        }
        danStKoledar++;
        if (stRowKoledar > 6) {
            break;
        }
        if (danStKoledar > 6) {
            danStKoledar = 0;
            povecaj = true;
        }
        i++;
    }

    //console.log(parseInt(document.getElementById("2sre").innerHTML) + 1);

}

var spremeniCurDan = function(id) {
    //console.log("spremeni cur dan");
    pobrisiPrikazDogodkov();
    var dan = preslikovalnaZaDneve.get(id + "");
    var mesec = preslikovalnaZaDneve.get(id + "mesec");
    nastaviDan(id);

    var leto = yyyy;
    if (mm == 1 && mesec == 12) {
        nastaviDatum(dan, mesec, yyyy - 1);
        leto -= 1;
    }
    else if (mm == 12 && mesec == 1) {
        nastaviDatum(dan, mesec, yyyy + 1);
        leto += 1;
    }
    else {
        nastaviDatum(dan, mesec, yyyy);
    }

    if (dogodkiVDomu.get(dan + "-" + mesec + "-" + leto) != null) {
        prikaziDogodek(dogodkiVDomu.get(dan + "-" + mesec + "-" + leto));
    }

    if (document.getElementById(idPrej) != null) {
        document.getElementById(idPrej).className = classPrej;
    }

    var curClass = document.getElementById(id + "").className;
    idPrej = id + "";
    classPrej = curClass;
    document.getElementById(id + "").className = "clicked_koledar " + curClass;
}

var spremeniMesecNaprej = function() {
    mm += 1;
    if (mm > 12) {
        mm = 1;
        yyyy += 1;
    }
    spremeniKoledar();
}

var spremeniMesecNazaj = function() {
    mm -= 1;
    if (mm < 1) {
        mm = 12;
        yyyy -= 1;
    }
    spremeniKoledar();
}

//datum oblike "dd-mm-yyyy"
var dodajDogodekVKoledar = function(datum, dogodek) {
    //console.log("dodajanje dogodka, z datumom " + datum + " in naslovom " + dogodek);
    //console.log("dodajanje dogodka, z datumom");
    //console.log("dodaj")
    var doZdejDogodki = dogodkiVDomu.get(datum, dogodek);
    //console.log(doZdejDogodki)
    var dodaj = '<p class="koledar_dogodki uk-text-left uk-margin-large-left">- ' + dogodek + '</p>';
    //var dodaj = "";
    if (doZdejDogodki != null && !doZdejDogodki.includes(dodaj))
        dodaj = "" + doZdejDogodki + dodaj;
    //console.log(dodaj)
    dogodkiVDomu.set(datum, dodaj);
}

var pretvoriVMojDatum = function(datum) {
    if (datum != null) {
        var res = datum.split("T");
        var res2 = res[0].split("-");
        for (var i = 0; i < 3; ++i) {
            var dt = parseInt(res2[i]);
            res2[i] = "" + dt + "";
        }
        return res2[2] + "-" + res2[1] + "-" + res2[0];
    }
}

var prikaziDogodek = function(dogodek) {
    if (document.getElementById("dogodki") != null) {
        var dogodkiDoZdej = document.getElementById("dogodki").innerHTML;
        document.getElementById("dogodki").innerHTML = "" + dogodkiDoZdej + dogodek;
    }
}

var pobrisiPrikazDogodkov = function() {
    if (document.getElementById("dogodki") != null) {
        document.getElementById("dogodki").innerHTML = "";
    }
}

var napolniDogodkeIzKoledarja = function() {
    
}

//window.onload = izdelajKoledar;
window.onresize = windowSizeFixes;
var counter = 0;


function resizeCallback() {
    //console.log("Interval function!");
    if (window.location.pathname != '/') {
        clearInterval(myVar);
    }
    else if (document.getElementById('myCalendar') != null) {
        windowSizeFixes();
        counter++;
        clearInterval(myVar);
    }
}

var myVar = setInterval(resizeCallback, 50);

function windowSizeFixes() {
    //do a load of stuff
    //console.log("window size fixes!");
    /*while (document.getElementById('myCalendar') == null) {
        sleep(500);
    }*/
    var widthCalendar = document.getElementById('myCalendar').offsetWidth;
    //console.log("width of calendar: " + widthCalendar)
    
    if (document.getElementById('myCalendar') != null) {
        //delim s 45?!
        //console.log(widthCalendar)
        var velikost = widthCalendar / 45.0;
        if (widthCalendar < 460) {
            velikost = widthCalendar / 48.0;        
        }
        if (widthCalendar < 300) {
            velikost = widthCalendar / 52.0;        
        }
        var elements = document.getElementsByClassName('koledar_dnevi');

        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (widthCalendar > 539)
                element.style.width = widthCalendar/7.43 + "px";
            else if (widthCalendar > 500)
                element.style.width = widthCalendar/7.5 + "px";
            else if (widthCalendar > 450)
                element.style.width = widthCalendar/7.6 + "px";
            else if (widthCalendar > 350)
                element.style.width = widthCalendar/7.7 + "px";
            else if (widthCalendar > 300)
                element.style.width = widthCalendar/7.8 + "px";
            else if (widthCalendar > 250)
                element.style.width = widthCalendar/8.0 + "px";
            else 
                element.style.width = widthCalendar/8.5 + "px";
            element.style.fontSize = velikost + "px"
        }
        
    }
    
    var widthUrnik = document.getElementById('myTimeTable').offsetWidth;
    //console.log("width of calendar: " + widthCalendar)
    
    if (document.getElementById('myTimeTable') != null) {
        //delim s 45?!
        var velikost = widthUrnik / 38.5;
        if (widthUrnik < 460) {
            velikost = widthUrnik / 41.1;
        }
        var elements = document.getElementsByClassName('urnik_elementi');

        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (widthUrnik > 538)
                element.style.width = widthUrnik/6.37 + "px";
            else if (widthCalendar > 500)
                element.style.width = widthUrnik/6.40 + "px";
            else if (widthCalendar > 450)
                element.style.width = widthUrnik/6.47 + "px";
            else if (widthCalendar > 350)
                element.style.width = widthUrnik/6.57 + "px";
            else if (widthCalendar > 300)
                element.style.width = widthUrnik/6.67 + "px";
            else if (widthCalendar > 250)
                element.style.width = widthUrnik/6.87 + "px";
            else 
                element.style.width = widthUrnik/7.37 + "px";
            element.style.fontSize = velikost + "px"
        }
    }
}
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

// config of local strategy
// username is email
passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(userName, password, done) {
        User.findOne(
            {
                email: userName
            },
            function(error, user) {
                if (error) {
                    return done(error);
                }
                if (!user) {
                    console.log("wrong username");
                    return done(null, false, {
                        message: 'Wrong username or password!'
                    });
                }
                if (!user.checkPassword(password)) {
                    return done(null, false, {
                        message: 'Wrong username or password!'
                    })
                }
                return done(null, user);
            })
    }));
var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var calendarEntrySchema = new mongoose.Schema({
  
  title: {type: String, required: true},
  description: {type: String, required: true},
  dateCreated: {type: Date, "default": Date.now},
  dateToDo: {type: Date, "default": Date.now},
	dateCompleted: {type: Date, "default": Date.now},
  duration: {type: Number}
  
});

var timetableEntrySchema = new mongoose.Schema({
  
  title: {type: String, required: true},
  description: {type: String, required: true},
  dateCreated: {type: Date, "default": Date.now},
	dateCompleted: {type: Date, "default": Date.now},
  duration: {type: Number, "default": 0},
  color: {type: String},
  dan: {type: Number, required: false}
  
});

var todoListSchema = new mongoose.Schema({
  
  title: {type: String, required: true},
  dateCreated: {type: Date, "default": Date.now},
	dateCompleted: {type: Date, "default": Date.now},
	completed: {type: Boolean, "default": false}
  
})


var userSchema = new mongoose.Schema({
  
  name: {type: String},
  email: {type: String, required: true, unique: true},
  
  hashValue: String,
	randomValue: String,
  
  verifiedEmail: {type: Boolean, required: true, "default": false},
  verificationUuid: {type: String},
  
  userCreatedDate: {type: Date, "default": Date.now, required: true},
  
  administrator: {type: Boolean, "default": false, required: true},
  animator: {type: Boolean, "default": false, required: true},
  normalUser: {type: Boolean, "default": true, required: true},
  
  userTypes: {type: [String]},
  
  calendarEntries: [calendarEntrySchema],
  timetableEntries: [timetableEntrySchema],
  todoList: [todoListSchema]
  
});

userSchema.methods.setPassword = function(password) {
  this.randomValue = crypto.randomBytes(16).toString('hex');
  this.hashValue = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.checkPassword = function(password) {
  var hashValue = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
  return this.hashValue == hashValue;
};

userSchema.methods.generateJwt = function() {
  var expiresDate = new Date();
  expiresDate.setDate(expiresDate.getDate() + 7);
  
 
  console.log("GENERATING JWT");
  console.log(this);
   //TODO ZAKAJ TO NE DELA
  console.log(this.verifiedEmail);
  
  return jwt.sign({
    _id: this._id,
    email: this.email,
    administrator: this.administrator,
    animator: this.animator,
    normalUser: this.normalUser,
    verifiedEmail: this.verifiedEmail,
    expiresDate: parseInt(expiresDate.getTime() / 1000, 10)
  }, process.env.JWT_PASSWORD);
};

//mongoose je ime povezave,
//User je ime modela,
//userSchema je shema, ki jo uporabimo in
//Users je (opcijsko) ime MongoDB zbirke.
mongoose.model('User', userSchema, 'Users');

var mongoose = require('mongoose');

var eventSchema = new mongoose.Schema({
  author: {type: String, required: true},
  name: {type: String, required: true},
  date: {type: Date, required: true},
  organizer: {type: String, required: true},
  descriptionOfEvent: {type: String, required: true}
});

mongoose.model('Event', eventSchema, 'Events');
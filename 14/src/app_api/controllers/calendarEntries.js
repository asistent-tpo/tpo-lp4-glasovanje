var mongoose = require('mongoose');
var User = mongoose.model('User');

var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};

module.exports.calendarEntryGetAll = function(request, response) {
  if (request.params && request.params.idUser) {
    User
        .findById(request.params.idUser)
        .select('userName calendarEntries')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.calendarEntries && user.calendarEntries.length > 0) {

              returnJsonResponse(response, 200, {
                user: {
                  userName: user.userName,
                  id: request.params.idUser,
                },
                calendarEntries: user.calendarEntries
              });
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any calendarEntry"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idCalendarEntry must be provided"
      });
  }
};

module.exports.calendarEntryCreate = function(request, response) {
  var idUser = request.params.idUser;
  if (idUser) {
    User
      .findById(idUser)
      .select('calendarEntries')
      .exec(
        function(error, user) {
          if (error) {
            returnJsonResponse(response, 400, error);
          } else {
            //console.log("poklici add calendarEntry")
            addCalendarEntry(request, response, user);
            //returnJsonResponse(response, 404, { "message": "še ni implementirano" });
          }
        });
  } else {
    returnJsonResponse(response, 400, {
      "message": "Cannot find user, idUser must be provided"
    })
  }
};

module.exports.calendarEntryGetOne = function(request, response) {
  if (request.params && request.params.idUser && request.params.idCalendarEntry) {
    // are ids alphanumerical
    if (!(/^\w+$/.test(request.params.idUser)) || !(/^\w+$/.test(request.params.idcalendarEntry))) {
      returnJsonResponse(response, 400, {
        "sporočilo": "Napačna zahteva!"
      });
      return;
    }
    
    User
        .findById(request.params.idUser)
        .select('userName calendarEntries')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.calendarEntries && user.calendarEntries.length > 0) {
            var calendarEntry = user.calendarEntries.id(request.params.idCalendarEntry);
            if (!calendarEntry) {
              returnJsonResponse(response, 404, {
                "message":
                  "Can not find calendarEntry with provided id."
              });
              return;
            } else {
              returnJsonResponse(response, 200, {
                user: {
                  userName: user.userName,
                  id: request.params.idUser
                },
                calendarEntry: calendarEntry
              });
            }
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any calendarEntry"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idcalendarEntry must be provided"
      });
  }
};

module.exports.calendarEntryUpdate = function(request, response) {
  if (request.params && request.params.idUser && request.params.idCalendarEntry) {
    User
      .findById(request.params.idUser)
      .select('calendarEntries')
      .exec(function(error, user) {
        if (!user) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find user with provided id."
        });
        return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        if (user.calendarEntries && user.calendarEntries.length > 0) {
          var calendarEntry = user.calendarEntries.id(request.params.idCalendarEntry);
        
          //console.log(calendarEntry.title);
          if (request.body.calendarEntry == null){
            //console.log(calendarEntry.title);
            if (request.body.dateCreated != null)
              calendarEntry.dateCreated = request.body.dateCreated;
        	
          	if (request.body.dateToDo != null)
            	calendarEntry.dateToDo = request.body.dateToDo;
          	
          	if (request.body.dateCompleted != null)
          	  calendarEntry.dateCompleted = request.body.dateCompleted;
          	
          	if (request.body.title != null)
          	  calendarEntry.title = request.body.title;
          	  
          	if (request.body.description != null)
          	  calendarEntry.description = request.body.description;
          	
          	if (request.body.duration != null)
          	  calendarEntry.duration = request.body.duration;
          } else {
          
            if (request.body.calendarEntry.dateCreated != null)
              calendarEntry.dateCreated = request.body.calendarEntry.dateCreated;
        	
          	if (request.body.calendarEntry.dateToDo != null)
            	calendarEntry.dateToDo = request.body.calendarEntry.dateToDo;
          	
          	if (request.body.calendarEntry.dateCompleted != null)
          	  calendarEntry.dateCompleted = request.body.calendarEntry.dateCompleted;
          	
          	if (request.body.calendarEntry.title != null)
          	  calendarEntry.title = request.body.calendarEntry.title;
          	  
          	if (request.body.calendarEntry.description != null)
          	  calendarEntry.description = request.body.calendarEntry.description;
          	
          	if (request.body.calendarEntry.duration != null)
          	  calendarEntry.duration = request.body.calendarEntry.duration;
          }
            
            //returnJsonResponse(response, 404, { "message": "še ni implementirano" });
          user.save(function(error, calendarEntry) {
                if (error) {
                  console.log(error)
                  returnJsonResponse(response, 400, error);
                } else {
                  returnJsonResponse(response, 200, calendarEntry);
                }
          });
        }
      });
  } else {
    returnJsonResponse(response, 400, {
      "message":
      "Cant find user, idUser must be provided"
    });
  }
};

module.exports.calendarEntryDelete = function(request, response) {
  if (request.params && request.params.idUser && request.params.idCalendarEntry) {
    User
        .findById(request.params.idUser)
        .select('calendarEntries')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.calendarEntries && user.calendarEntries.length > 0) {
            var calendarEntry = user.calendarEntries.id(request.params.idCalendarEntry);
            //console.log(calendarEntry);
            if (!calendarEntry) {
              returnJsonResponse(response, 404, {
                "message":
                  "Can not find calendarEntry with provided id."
              });
              return;
            } else {
              user.calendarEntries.id(request.params.idCalendarEntry).remove();
              user.save(function(error) {
              if (error) {
                returnJsonResponse(response, 500, error);
              } else {
                returnJsonResponse(response, 204, null);
              }
            });
            }
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any calendarEntry"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idCalendarEntry must be provided"
      });
  }
};

//TODO imena polj iz request bodya je treba prov dolocit 
var addCalendarEntry = function(request, response, user) {
  if (!user) {
    returnJsonResponse(response, 404, {
      "message": "Can not find user."
    });
  } else {
    if (request.body.calendarEntries != null) {
      request.body.calendarEntries.forEach(function(calendarEntry) {
        user.calendarEntries.push({
            title: calendarEntry.title,
        	description: calendarEntry.description,
        	dateCreated: calendarEntry.dateCreated,
        	dateToDo: calendarEntry.dateToDo,
        	dateCompleted: calendarEntry.dateCompleted,
        	duration: calendarEntry.duration
 
        });
       
      });
    } else {
      //console.log(request.body);
      if (request.body.duration == null) {
        request.body.duration = 0
      }
      if (request.body.calendarEntry == null) {
        user.calendarEntries.push({
          title: request.body.title,
        	description: request.body.description,
        	dateCreated: request.body.dateCreated,
        	dateToDo: request.body.dateToDo,
        	dateCompleted: request.body.dateCompleted,
        	duration: request.body.duration
        });
      } else {
        user.calendarEntries.push({
          title: request.body.calendarEntry.title,
        	description: request.body.calendarEntry.description,
        	dateCreated: request.body.calendarEntry.dateCreated,
        	dateToDo: request.body.calendarEntry.dateToDo,
        	dateCompleted: request.body.calendarEntry.dateCompleted,
        	duration: request.body.calendarEntry.duration
        });
      }
    }
    user.save(function(error, user) {
      var addedCalendarEntry;
      if (error) {
        returnJsonResponse(response, 400, error);
      } else {
        addedCalendarEntry = user.calendarEntries[user.calendarEntries.length - 1];
        returnJsonResponse(response, 201, addedCalendarEntry);
      }
    })
  }
}

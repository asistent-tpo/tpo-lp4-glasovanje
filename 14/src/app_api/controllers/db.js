var mongoose = require('mongoose');
var Event = mongoose.model('Event');
var User = mongoose.model('User');
var crypto = require('crypto');

var pridobiNakljucnoVrednost = function(){
  return crypto.randomBytes(16).toString('hex');
};
    
var pridobiHashGesla = function(nakljucnaVrednost, geslo){
  return crypto.pbkdf2Sync(geslo, nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};

module.exports.eventsInsert = function (request, response) {
  Event
    .create ( {
      author: request.body.author,
      name: request.body.name,
      date: request.body.date,
      organizer: request.body.organizer,
      descriptionOfEvent: request.body.descriptionOfEvent,
    }
    , function(error, event) {
        if (error) {
          returnJsonResponse(response, 400, error);
        } else {
          returnJsonResponse(response, 201, event);
        }
      });
};

module.exports.eventsDelet = function (request, response) {
  Event
    .remove (
      {}, function(error, event) {
          if (error) {
            returnJsonResponse(response, 404, error);
          } else {
            returnJsonResponse(response, 204, null);
          }
      });
};


module.exports.usersInsert = function (request, response) {
  var nakljucnaVrednost = pridobiNakljucnoVrednost();
  var hashVrednost = pridobiHashGesla(nakljucnaVrednost, request.body.geslo)
  User
    .create ({
          name: request.body.name,
          email: request.body.email,
          
        	hashValue: hashVrednost,
        	randomValue: nakljucnaVrednost,
        	
        	verifiedEmail: request.body.verifiedEmail,
        	verificationUuid: request.body.verificationUuid,
        	userCreatedDate: request.body.userCreatedDate,
        	administrator: request.body. administrator,
        	animator: request.body.animator,
        	normalUser: request.body.normalUser,
          calendarEntries: request.body.calendarEntries,
          timetableEntries: request.body.timetableEntries,
          todoList: request.body.todoList,
      }
      
      , function(error, user) {
        if (error) {
          console.log(error);
          returnJsonResponse(response, 400, error);
        } else {
          returnJsonResponse(response, 201, user);
        }
      });
};

module.exports.usersDelet = function (request, response) {
  User
    .remove (
      {}, function(error, user) {
          if (error) {
            console.log(error);
            returnJsonResponse(response, 404, error);
          } else {
            returnJsonResponse(response, 204, null);
          }
      });
};
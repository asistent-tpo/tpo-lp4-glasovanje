var mongoose = require('mongoose');
var User = mongoose.model('User');
    
var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};

module.exports.userVerify = function(request, response) {
  if (request.params && request.params.idUser) {
    User
      .findById(request.params.idUser)
      .exec(function(error, user) {
        if (!user) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find user with provided id."
        });
        return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        console.log(user.verificationUuid + " SERVER")
        console.log(request.body.verificationUuid)
        
        if (request.body.verificationUuid != null) {
          if (user.verificationUuid === request.body.verificationUuid) {
            user.verifiedEmail = true;
          } else {
              returnJsonResponse(response, 400, {
                "message": "Niz za verifikacijo je neustrezen!"
              })
              return;
            }
        }
        
        user.save(function(error, user) {
              if (error) {
                returnJsonResponse(response, 400, error);
              } else {
                returnJsonResponse(response, 200, user);
              }
        });
      });
  } else {
    returnJsonResponse(response, 400, {
      "message":
      "Cant find user, idUser must be provided"
    });
  }
}

module.exports.userGetAll = function(request, response) {
  User
    .find()
    .exec(function(error, users) {
      if (!users) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find any users."
        });
        return;
      } else if (error) {
        returnJsonResponse(response, 500, error);
        return;
      }
        returnJsonResponse(response, 200, users);
      });
};

module.exports.userCreate = function(request, response) {
  User.create({
    
  	email: request.body.email,
    calendarEntries: request.body.calendarEntries,
    timetableEntries: request.body.timetableEntries,
    todoList: request.body.todoList
  	
  }, function(error, user) {
      if (error) {
        returnJsonResponse(response, 400, error);
      } else {
        returnJsonResponse(response, 201, user);
      }
  });
};

module.exports.userGetOne = function(request, response) {
  if (request.params && request.params.idUser) {
      
      // is id alphanumerical and only parameter? 
      if (!(/^\w+$/.test(request.params.idUser)) || Object.keys(request.query).length > 0) {
        returnJsonResponse(response, 400, {
          "sporočilo": "Napačna zahteva!"
        });
        return;
      }
      
      User
        .findById(request.params.idUser)
        .exec(function(error, user) {
          if (!user) {
          returnJsonResponse(response, 404, {
            "message": 
              "Can not find user with provided id " + request.params.idUser + "."
          });
          return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
          returnJsonResponse(response, 200, user);
        });
  } else {
    returnJsonResponse(response, 400, {"Message": "Id of user is missing!"});
  }
};

module.exports.userUpdate = function(request, response) {
  if (request.params && request.params.idUser) {
    User
      .findById(request.params.idUser)
      .exec(function(error, user) {
        if (!user) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find user with provided id."
        });
        return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        if (request.body.name != null)
          user.name = request.body.name;
      	
      	if (request.body.email != null)
        	user.email = request.body.email;
      	
      	if (request.body.verifiedEmail != null)
      	  user.verifiedEmail = request.body.verifiedEmail;
      	
      	if (request.body.userCreatedDate != null)
      	  user.userCreatedDate = request.body.userCreatedDate;
      	
      	if (request.body.administrator != null)
      	  user.administrator = request.body.administrator;

      	if (request.body.animatorage != null)
      	  user.animator = request.body.animator;
      	
      	if (request.body.normalUser != null)
      	  user.normalUser = request.body.normalUser;
      	
      	if (request.body.userTypes != null)
      	  user.userTypes = request.body.userTypes;
      	
      	if (request.body.calendarEntries != null)
      	  user.calendarEntries = request.body.calendarEntries;
      	
      	if (request.body.timetableEntries != null)
      	  user.timetableEntries = request.body.timetableEntries;
      	
      	if (request.body.todoList != null)
      	  user.todoList = request.body.todoList;
        /*
        if (request.body.verificationUuid != null)
          user.verificationUuid == request.body.verificationUuid;
        */
        user.save(function(error, user) {
              if (error) {
                returnJsonResponse(response, 400, error);
              } else {
                returnJsonResponse(response, 200, user);
              }
        });
      });
  } else {
    returnJsonResponse(response, 400, {
      "message":
      "Cant find user, idUser must be provided"
    });
  }
};

module.exports.userDelete = function(request, response) {
  if (request.params && request.params.idUser) {
    User.remove(
      {
        "_id": request.params.idUser
      }, function(error, user) {
        if (error) {
          returnJsonResponse(response, 400, error);
        } else {
          returnJsonResponse(response, 204, user);
        }
    });
  } else {
    returnJsonResponse(response, 400, {"Message": "Id of user is missing!"});
  }  
};

module.exports.changePassword = function(request, response) {
  if (request.params && request.params.idUser) {
    User
      .findById(request.params.idUser)
      .exec(function(error, user) {
        if (!user) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find user with provided id."
        });
        return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        //console.log("setting password");
        //console.log(request.body);
        //console.log(typeof request.body.passwordOld)
        //console.log(typeof new String(request.body.passwordOld))
        if (user.checkPassword(request.body.passwordOld)) {
          user.setPassword(request.body.passwordNew.toString());  
        } else {
              returnJsonResponse(response, 400, {
                "message": "Staro geslo se ne ujema z obstoječim!"
              })
              return;
        }
        
        user.save(function(error, user) {
              if (error) {
                returnJsonResponse(response, 400, error);
              } else {
                returnJsonResponse(response, 200, user);
              }
        });
      });
  } else {
    returnJsonResponse(response, 400, {
      "message":
      "Cant find user, idUser must be provided"
    });
  }
}


//**** Pridobi user glede na email
module.exports.userGetIdFromUserName = function(request, response) {
  if(request.params && request.params.emailUser) {
    User.findOne({
      email: request.params.emailUser
    }, function(error, user) {
        if (!user) {
          // sprozilo bo kasnejso napako, ker User z _id = 'UporabnikNeObstaja' ne obstaja
          returnJsonResponse(response, 404, 'UporabnikNeObstaja');
        }
        else if (error) {
          // sprozilo bo kasnejso napako, ker User z _id = 'UporabnikNeObstaja' ne obstaja
          returnJsonResponse(response, 500, 'UporabnikNeObstaja');
        } else {
          returnJsonResponse(response, 200, user);
        }
    });
  } else {
    returnJsonResponse(response, 400, {"Message": "email of user is missing!"});
  }
};

//******
var mongoose = require('mongoose');
var User = mongoose.model('User');

var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};

module.exports.todoListGetAll = function(request, response) {
  if (request.params && request.params.idUser) {
    User
        .findById(request.params.idUser)
        .select('userName todoList')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.todoList && user.todoList.length > 0) {

              returnJsonResponse(response, 200, {
                user: {
                  userName: user.userName,
                  id: request.params.idUser,
                },
                todoList: user.todoList
              });
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any todoList"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, idUser must be provided"
      });
  }
};

module.exports.todoListCreate = function(request, response) {
  console.log("v create metodi")
  console.log(request.body.date)
  var idUser = request.params.idUser;
  if (idUser) {
    User
      .findById(idUser)
      .select('todoList')
      .exec(
        function(error, user) {
          if (error) {
            returnJsonResponse(response, 400, error);
          } else {
            //console.log("poklici add todoList")
            addTodoList(request, response, user);
            //returnJsonResponse(response, 404, { "message": "še ni implementirano" });
          }
        });
  } else {
    returnJsonResponse(response, 400, {
      "message": "Cannot find user, idUser must be provided"
    })
  }
};

module.exports.todoListGetOne = function(request, response) {
  if (request.params && request.params.idUser && request.params.idTodoList) {
    // are ids alphanumerical
    if (!(/^\w+$/.test(request.params.idUser)) || !(/^\w+$/.test(request.params.idTodoList))) {
      returnJsonResponse(response, 400, {
        "sporočilo": "Napačna zahteva!"
      });
      return;
    }
    
    User
        .findById(request.params.idUser)
        .select('userName todoList')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.todoList && user.todoList.length > 0) {
            var todoListTask = user.todoList.id(request.params.idTodoList);
            if (!todoListTask) {
              returnJsonResponse(response, 404, {
                "message":
                  "Can not find todoListTask with provided id."
              });
              return;
            } else {
              returnJsonResponse(response, 200, {
                user: {
                  userName: user.userName,
                  id: request.params.idUser
                },
                todoListTask: todoListTask
              });
            }
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any todoListTask"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idTodoList must be provided"
      });
  }
};

module.exports.todoListUpdate = function(request, response) {
  if (request.params && request.params.idUser && request.params.idTodoList) {
    User
      .findById(request.params.idUser)
      .select('todoList')
      .exec(function(error, user) {
        if (!user) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find user with provided id."
        });
        return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        if (user.todoList && user.todoList.length > 0) {
          var todoListTask = user.todoList.id(request.params.idTodoList);
        
          //console.log(todoListTask.title);
          if (request.body.todoListTask.dateCreated != null)
            todoListTask.dateCreated = request.body.todoListTask.dateCreated;
      	
        	if (request.body.todoListTask.color != null)
          	todoListTask.color = request.body.todoListTask.color;
        	
        	if (request.body.todoListTask.dateCompleted != null)
        	  todoListTask.dateCompleted = request.body.todoListTask.dateCompleted;
        	
        	if (request.body.todoListTask.title != null)
        	  todoListTask.title = request.body.todoListTask.title;
        	  
        	if (request.body.todoListTask.description != null)
        	  todoListTask.description = request.body.todoListTask.description;
        	
        	if (request.body.todoListTask.duration != null)
        	  todoListTask.duration = request.body.todoListTask.duration;
            
            //returnJsonResponse(response, 404, { "message": "še ni implementirano" });
          user.save(function(error, todoListTask) {
                if (error) {
                  console.log(error)
                  returnJsonResponse(response, 400, error);
                } else {
                  returnJsonResponse(response, 200, todoListTask);
                }
          });
        }
      });
  } else {
    returnJsonResponse(response, 400, {
      "message":
      "Cant find user, idUser must be provided"
    });
  }
};

module.exports.todoListDelete = function(request, response) {
  console.log("to do list delete")
  if (request.params && request.params.idUser && request.params.idTodoList) {
    User
        .findById(request.params.idUser)
        .select('todoList')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.todoList && user.todoList.length > 0) {
            var todoListTask = user.todoList.id(request.params.idTodoList);
            //console.log(todoListTask);
            if (!todoListTask) {
              returnJsonResponse(response, 404, {
                "message":
                  "Can not find todoListTask with provided id."
              });
              return;
            } else {
              user.todoList.id(request.params.idTodoList).remove();
              user.save(function(error) {
              if (error) {
                returnJsonResponse(response, 500, error);
              } else {
                returnJsonResponse(response, 204, null);
              }
            });
            }
          } else {
            console.log("Can not find any todoListTask")
            returnJsonResponse(response, 200, {
              "message": "Can not find any todoListTask"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idTodoList must be provided"
      });
  }
};


//TODO imena polj iz request bodya je treba prov dolocit 
var addTodoList = function(request, response, user) {
  if (!user) {
    returnJsonResponse(response, 404, {
      "message": "Can not find user."
    });
  } else {
    if (request.body.todoList != null) {
        console.log(request.body.todoList);
        console.log("v if")
      request.body.todoList.forEach(function(todoTask) {
        user.todoList.push({
            title: todoTask.title,
        	dateCreated: todoTask.dateCreated,
        	dateCompleted: todoTask.dateCompleted,
        	completed: todoTask.completed
        });
       
      });
    } else {
      console.log("v else")
      console.log(request.body);
      user.todoList.push({
        title: request.body.title,
      	dateCreated: request.body.date,
      	dateCompleted: request.body.dateCompleted,
      	completed: request.body.completed
      });
    }
    user.save(function(error, user) {
      var addedTodoList;
      if (error) {
        returnJsonResponse(response, 400, error + "Be sure, that the name of task is todoListTask if sending only one, not todoList!!");
      } else {
        //console.log(user.todoList)
        addedTodoList = user.todoList[user.todoList.length - 1];
        returnJsonResponse(response, 201, addedTodoList);
      }
    })
  }
}
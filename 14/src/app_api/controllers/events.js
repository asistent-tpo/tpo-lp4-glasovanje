var mongoose = require('mongoose');
var Event = mongoose.model('Event');
var User = mongoose.model('User');

var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};


module.exports.eventGetAllList = function(request, response) {
	Event
  	.find()
  	.exec(function(error, events) {
  	  if (!events) {
  	    returnJsonResponse(response, 404, {
          "message": 
            "Can not find any events."
        });
        return;
  		} else if (error) {
  			returnJsonResponse(response, 500, error);
  			return;
  		}
  		returnJsonResponse(response, 200, events);
  });
};

module.exports.eventCreate = function(request, response) {
  returnUserEmail(request, response, function(request, response, userEmail) {
    Event.create({
      author: userEmail,
      name: request.body.name,
      date: request.body.date,
      organizer: request.body.organizer,
      descriptionOfEvent: request.body.descriptionOfEvent,
    }, function(error, event) {
      if (error) {
        returnJsonResponse(response, 400, error);
      } else {
        returnJsonResponse(response, 201, event);
      }
    });
  });
};

module.exports.eventGet = function(request, response) {
  if (request.params && request.params.idEvent) {
    
    // is id alphanumerical and only parameter? 
    if (!(/^\w+$/.test(request.params.idEvent)) || Object.keys(request.query).length > 0) {
      returnJsonResponse(response, 400, {
        "message": "Wrong request!"
      });
      return;
    }
    
    Event
      .findById(request.params.idEvent)
      .exec(function(error, event) {
        if (!event) {
          returnJsonResponse(response, 404, {
            "message": 
              "Can not find event with provided id."
          });
          return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        returnJsonResponse(response, 200, event);
      });
  } else {
    returnJsonResponse(response, 400, { 
      "message": "Id of event is missing!"
    });
  }
};

module.exports.eventUpdate = function(request, response) {
  if (request.params && request.params.idEvent) {
    Event
    .findById(request.params.idEvent)
    .exec(
      function(error, event) {
        if (!event) {
          returnJsonResponse(response, 404, {
            "message": 
              "Can not find event with provided id."
          });
          return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        event.author = event.author;
        event.name = request.body.name;
        event.date = request.body.date;
        event.organizer = request.body.organizer;
        event.descriptionOfEvent = request.body.descriptionOfEvent;
        event.save(function(error, event) {
          if (error) {
            returnJsonResponse(response, 400, error);
          } else {
            returnJsonResponse(response, 200, event);
          }
        });
      }
    );
  } else {
    returnJsonResponse(response, 400, {
      "message": "Id of event is missing!"
    });
    return;
  }
};

module.exports.eventDelete = function(request, response) {
  if (request.params && request.params.idEvent) {
      var idEvent = request.params.idEvent;
    Event
      .findByIdAndRemove(idEvent)
      .exec(
        function(error, event) {
          if (error) {
            returnJsonResponse(response, 404, error);
            return;
          }
          returnJsonResponse(response, 204, null);
        }
      );
  } else {
    returnJsonResponse(response, 400, {
      "message": "Id of event is missing!"
    });
  }
};


var returnUserEmail = function(request, response, callback) {
  if (request.payload && request.payload.email) {
    User
      .findOne({
        email: request.payload.email
      })
      .exec(function(error, user) {
        if (!user) {
          returnJsonResponse(response, 404, {
            "message": "I do not find user"
          });
          return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        callback(request, response, user.email);
      });
  } else {
    returnJsonResponse(response, 400, {
      "message": "There is no data of user"
    });
    return;
  }
};
var mongoose = require('mongoose');
var User = mongoose.model('User');

var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};

module.exports.timetableEntryGetAll = function(request, response) {
  if (request.params && request.params.idUser) {
    User
        .findById(request.params.idUser)
        .select('userName timetableEntries')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.timetableEntries && user.timetableEntries.length > 0) {

              returnJsonResponse(response, 200, {
                user: {
                  userName: user.userName,
                  id: request.params.idUser,
                },
                timetableEntries: user.timetableEntries
              });
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any timetableEntry"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, idUser must be provided"
      });
  }
  
};

module.exports.timetableEntryCreate = function(request, response) {
  var idUser = request.params.idUser;
  if (idUser) {
    User
      .findById(idUser)
      .select('timetableEntries')
      .exec(
        function(error, user) {
          if (error) {
            returnJsonResponse(response, 400, error);
          } else {
            //console.log("poklici add timetableEntry")
            addTimetableEntry(request, response, user);
            //returnJsonResponse(response, 404, { "message": "še ni implementirano" });
          }
        });
  } else {
    returnJsonResponse(response, 400, {
      "message": "Cannot find user, idUser must be provided"
    })
  }
};

module.exports.timetableEntryGetOne = function(request, response) {
  if (request.params && request.params.idUser && request.params.idTimetableEntry) {
    // are ids alphanumerical
    if (!(/^\w+$/.test(request.params.idUser)) || !(/^\w+$/.test(request.params.idTimetableEntry))) {
      returnJsonResponse(response, 400, {
        "sporočilo": "Napačna zahteva!"
      });
      return;
    }
    
    User
        .findById(request.params.idUser)
        .select('userName timetableEntries')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.timetableEntries && user.timetableEntries.length > 0) {
            var timetableEntry = user.timetableEntries.id(request.params.idTimetableEntry);
            if (!timetableEntry) {
              returnJsonResponse(response, 404, {
                "message":
                  "Can not find timetableEntry with provided id."
              });
              return;
            } else {
              returnJsonResponse(response, 200, {
                user: {
                  userName: user.userName,
                  id: request.params.idUser
                },
                timetableEntry: timetableEntry
              });
            }
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any timetableEntry"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idTimetableEntry must be provided"
      });
  }
};

module.exports.timetableEntryUpdate = function(request, response) {
  if (request.params && request.params.idUser && request.params.idTimetableEntry) {
    User
      .findById(request.params.idUser)
      .select('timetableEntries')
      .exec(function(error, user) {
        if (!user) {
        returnJsonResponse(response, 404, {
          "message": 
            "Can not find user with provided id."
        });
        return;
        } else if (error) {
          returnJsonResponse(response, 500, error);
          return;
        }
        if (user.timetableEntries && user.timetableEntries.length > 0) {
          var timetableEntry = user.timetableEntries.id(request.params.idTimetableEntry);
        
          //console.log(timetableEntry.title);
          if (request.body.timetableEntry.dateCreated != null)
            timetableEntry.dateCreated = request.body.timetableEntry.dateCreated;
      	
        	if (request.body.timetableEntry.color != null)
          	timetableEntry.color = request.body.timetableEntry.color;
        	
        	if (request.body.timetableEntry.dateCompleted != null)
        	  timetableEntry.dateCompleted = request.body.timetableEntry.dateCompleted;
        	
        	if (request.body.timetableEntry.title != null)
        	  timetableEntry.title = request.body.timetableEntry.title;
        	  
        	if (request.body.timetableEntry.description != null)
        	  timetableEntry.description = request.body.timetableEntry.description;
        	
        	if (request.body.timetableEntry.duration != null)
        	  timetableEntry.duration = request.body.timetableEntry.duration;
            
            //returnJsonResponse(response, 404, { "message": "še ni implementirano" });
          user.save(function(error, timetableEntry) {
                if (error) {
                  console.log(error)
                  returnJsonResponse(response, 400, error);
                } else {
                  returnJsonResponse(response, 200, timetableEntry);
                }
          });
        }
      });
  } else {
    returnJsonResponse(response, 400, {
      "message":
      "Cant find user, idUser must be provided"
    });
  }
};

module.exports.timetableEntryDelete = function(request, response) {
  if (request.params && request.params.idUser && request.params.idTimetableEntry) {
    User
        .findById(request.params.idUser)
        .select('timetableEntries')
        .exec(function(error, user) {
          if (!user) {
            returnJsonResponse(response, 404, {
              "message": 
                "Can not find user with provided id."
            });
            return;
          } else if (error) {
            returnJsonResponse(response, 500, error);
            return;
          }
          if (user.timetableEntries && user.timetableEntries.length > 0) {
            var timetableEntry = user.timetableEntries.id(request.params.idTimetableEntry);
            //console.log(timetableEntry);
            if (!timetableEntry) {
              returnJsonResponse(response, 404, {
                "message":
                  "Can not find timetableEntry with provided id."
              });
              return;
            } else {
              user.timetableEntries.id(request.params.idTimetableEntry).remove();
              user.save(function(error) {
              if (error) {
                returnJsonResponse(response, 500, error);
              } else {
                returnJsonResponse(response, 204, null);
              }
            });
            }
          } else {
            returnJsonResponse(response, 200, {
              "message": "Can not find any timetableEntry"
            });
          }
        });
  } else {
      returnJsonResponse(response, 400, {
        "message": "Can not find record in database, both idUser and idTimetableEntry must be provided"
      });
  }
};

//TODO imena polj iz request bodya je treba prov dolocit 
var addTimetableEntry = function(request, response, user) {
  if (!user) {
    returnJsonResponse(response, 404, {
      "message": "Can not find user."
    });
  } else {
    if (request.body.timetableEntries != null) {
      request.body.timetableEntries.forEach(function(timetableEntry) {
        user.timetableEntries.push({
            title: timetableEntry.title,
        	description: timetableEntry.description,
        	dateCreated: timetableEntry.dateCreated,
        	dateCompleted: timetableEntry.dateCompleted,
        	duration: timetableEntry.duration,
            color: timetableEntry.color
        });
       
      });
    } else {
      //console.log(request.body);
      if (request.body.timetableEntry == null) {
        user.timetableEntries.push({
          title: request.body.title,
        	description: request.body.description,
        	dateCreated: request.body.dateCreated,
        	dateCompleted: request.body.dateCompleted,
        	duration: request.body.duration,
        	color: request.body.color
        });
      }else {
        user.timetableEntries.push({
          title: request.body.timetableEntry.title,
        	description: request.body.timetableEntry.description,
        	dateCreated: request.body.timetableEntry.dateCreated,
        	dateCompleted: request.body.timetableEntry.dateCompleted,
        	duration: request.body.timetableEntry.duration,
        	color: request.body.timetableEntry.color
        });
      }
    }
    user.save(function(error, user) {
      var addedtimetableEntry;
      if (error) {
        returnJsonResponse(response, 400, error);
      } else {
        addedtimetableEntry = user.timetableEntries[user.timetableEntries.length - 1];
        returnJsonResponse(response, 201, addedtimetableEntry);
      }
    })
  }
}
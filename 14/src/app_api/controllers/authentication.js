var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');
const uuidv4 = require('uuid/v4');
const nodemailer = require('nodemailer');

var returnJsonResponse = function(response, status, content) {
  response.status(status);
  response.json(content);
};

module.exports.registration = function(request, response) {
    if (!request.body.email || !request.body.password) {
        console.log(request.body);
        returnJsonResponse(response, 400, {
            "message": "Prosim izpolnite vsa polja"
        });
    } else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(request.body.email))) {
        
        returnJsonResponse(response, 400, {
          "sporočilo": "Elektronski naslov je neustrezen!"
        });
        return;
    }
    
    var user = new User();
    
    user.verificationUuid = uuidv4();
    user.administrator = false;
	user.email = request.body.email;
	user.setPassword(request.body.password);
	user.verifiedEmail = false;
	user.userCreatedDate = new Date();
	user.administrator = false;
	user.animator = false;
	user.normalUser = true;
	user.calendarEntries = [
        {
            "dateCreated": "2019-05-08T00:00:00.000Z",
            "dateToDo": "2019-05-28T00:00:00.000Z",
            "dateCompleted": "2019-05-08T00:00:00.000Z",
            "_id": "5cd3056ef4c271b34f0f00a0",
            "title": "DummyCalendarEntry",
            "description": "DummyCalendarEntry",
            "duration": 0
        }
    ],
    user.timetableEntries = [
        {
            "dateCreated": "2019-05-08T00:00:00.000Z",
            "dateCompleted": "2019-05-08T00:00:00.000Z",
            "_id": "5cd3056ef4c271b34f0f00a2",
            "title": "DummyTimeTableEntry",
            "description": "DummyTimeTableEntry",
            "duration": 0,
            "color": "DummyColor"
        }
    ],
    user.todoList = [
        {
            "completed": true,
            "dateCreated": "2019-05-08T00:00:00.000Z",
            "dateCompleted": "2019-05-08T00:00:00.000Z",
            "_id": "5cd3056ef4c271b34f0f00a4",
            "title": "DummyTodoList"
        }
    ]
	
	var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: 'straightas.tpo@gmail.com',
            clientId: process.env.GMAIL_CLIENTID,
            clientSecret: process.env.GMAIL_CLIENTSECRET,
            refreshToken: process.env.GMAIL_REFRESHTOKEN
        }
    });
    
    var mailOptions = {
        from: 'StraightAs <straightas.top@gmail.com',
        to: request.body.email,
        subject: 'StraightAs Email Verification',
        text: 'Prosimo vnesite sledeč niz znakov v obrazec za verifikacijo v vašem StraightAs profilu:\n\n' + user.verificationUuid
    }
    
	
	user.save(function(error) {
	    if (error) {
	        returnJsonResponse(response, 500, error);
	    } else {
	        transporter.sendMail(mailOptions, function(err, res) {
	            if (err) {
	                console.log('Error when sending email: ' + err);
	            } else {
	                console.log('Email sent');
	            }
	        })
	        returnJsonResponse(response, 200, {
	            "token": user.generateJwt()
	        })
	    }
	})
};

module.exports.login = function(request, response) {
    if (!request.body.email || !request.body.password) {
        returnJsonResponse(response, 400, {
            "message": "Mail and Username must be included"
        });
    } else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(request.body.email))) {
        
        returnJsonResponse(response, 400, {
          "sporočilo": "Elektronski naslov je neustrezen!"
        });
        return;
    }
    passport.authenticate('local', function(error, user, data) {
        //console.log(user);
        if (error) {
            returnJsonResponse(response, 404, error);
            return;
        }
        if (user) {
            returnJsonResponse(response, 200, {
                "token": user.generateJwt()
            });
        } else {
            returnJsonResponse(response, 401, data);
        }
    })(request, response);
};



var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');

var authentication = jwt({
  secret: process.env.JWT_PASSWORD,
  userProperty: 'payload'
});


var ctrlUsers = require('../controllers/users');
var ctrlCalendarEntries = require('../controllers/calendarEntries');
var ctrlTimetableEntries = require('../controllers/timetableEntries');
var ctrlTodoLists = require('../controllers/todoLists');
var ctrlEvents = require('../controllers/events');
var ctrlDB      = require('../controllers/db');
var ctrlAuthentication = require('../controllers/authentication');
var ctrlSystemMessage = require('../controllers/systemMessages');

/* Authentication */
router.post('/registration', ctrlAuthentication.registration);
router.post('/login', ctrlAuthentication.login);

/* users */
router.put('/users/:idUser/changepassword',
  authentication, ctrlUsers.changePassword);
router.put('/users/:idUser/verify',
  authentication, ctrlUsers.userVerify);

router.get('/users', 
   authentication, ctrlUsers.userGetAll);
  
router.post('/users', 
  authentication, ctrlUsers.userCreate);
  
router.get('/users/:idUser', 
  authentication, ctrlUsers.userGetOne);
  
router.put('/users/:idUser', 
  authentication, ctrlUsers.userUpdate);
  
router.delete('/users/:idUser', 
  authentication, ctrlUsers.userDelete);

/* calendarEntries */
router.get('/users/:idUser/calendarEntries', 
  authentication, ctrlCalendarEntries.calendarEntryGetAll);
  
router.post('/users/:idUser/calendarEntries', 
  authentication, ctrlCalendarEntries.calendarEntryCreate);
  
router.get('/users/:idUser/calendarEntries/:idCalendarEntry', 
  authentication, ctrlCalendarEntries.calendarEntryGetOne);
  
router.put('/users/:idUser/calendarEntries/:idCalendarEntry', 
  authentication, ctrlCalendarEntries.calendarEntryUpdate);
  
router.delete('/users/:idUser/calendarEntries/:idCalendarEntry', 
  authentication, ctrlCalendarEntries.calendarEntryDelete);

/* timetableEntries */
router.get('/users/:idUser/timetableEntries', 
  authentication, ctrlTimetableEntries.timetableEntryGetAll);
  
router.post('/users/:idUser/timetableEntries', 
  authentication, ctrlTimetableEntries.timetableEntryCreate);
  
router.get('/users/:idUser/timetableEntries/:idTimetableEntry', 
  authentication, ctrlTimetableEntries.timetableEntryGetOne);
  
router.put('/users/:idUser/timetableEntries/:idTimetableEntry', 
  authentication, ctrlTimetableEntries.timetableEntryUpdate);
  
router.delete('/users/:idUser/timetableEntries/:idTimetableEntry', 
  authentication, ctrlTimetableEntries.timetableEntryDelete);

/* todoList */
router.get('/users/:idUser/todoList', 
  authentication, ctrlTodoLists.todoListGetAll);
  
router.post('/users/:idUser/todoList', 
  authentication, ctrlTodoLists.todoListCreate);
  
router.get('/users/:idUser/todoList/:idTodoList', 
  authentication, ctrlTodoLists.todoListGetOne);
  
router.put('/users/:idUser/todoList/:idTodoList', 
  authentication, ctrlTodoLists.todoListUpdate);
  
router.delete('/users/:idUser/todoList/:idTodoList', 
   authentication, ctrlTodoLists.todoListDelete);
   
/* events */
router.get('/events', 
  authentication, ctrlEvents.eventGetAllList);
  
router.post('/events', 
  authentication, ctrlEvents.eventCreate);
  
router.get('/events/:idEvent', 
  authentication, ctrlEvents.eventGet);
  
router.put('/events/:idEvent', 
  authentication, ctrlEvents.eventUpdate);
  
router.delete('/events/:idEvent', 
   authentication, ctrlEvents.eventDelete);
   
/* System messages */
router.post('/systemMessage', 
  authentication, ctrlSystemMessage.systemMessage);
  
// To je za testiranje

router.get('/userEmail/:emailUser',
  ctrlUsers.userGetIdFromUserName);
   
// TO JE ZA DB STRAN -------------------------------------

router.post('/DBevents',
  ctrlDB.eventsInsert);
  
router.delete('/DBevents',
  ctrlDB.eventsDelet);
  
router.post('/DBusers',
  ctrlDB.usersInsert);
  
router.delete('/DBusers',
  ctrlDB.usersDelet);

module.exports = router;
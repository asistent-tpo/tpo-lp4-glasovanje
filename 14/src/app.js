require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var passport = require('passport');
require('./app_api/models/db');
require('./app_api/configuration/passport');

var uglifyJs = require('uglify-js');
var fs = require('fs');

var zdruzeno = uglifyJs.minify({
  
  'app.js': fs.readFileSync('src/app_client/app.js', 'utf-8'),
  'home.controller.js': fs.readFileSync('src/app_client/home/home.controller.js', 'utf-8'),
  'info.controller.js': fs.readFileSync('src/app_client/info/info.controller.js', 'utf-8'),
  'registration.controller.js': fs.readFileSync('src/app_client/authentication/registration/registration.controller.js', 'utf-8'),
  'login.controller.js': fs.readFileSync('src/app_client/authentication/login/login.controller.js', 'utf-8'),
  'verify.controller.js': fs.readFileSync('src/app_client/verification/verify.controller.js', 'utf-8'),
  'changePassword.controller.js': fs.readFileSync('src/app_client/authentication/changePassword/changePassword.controller.js', 'utf-8'),
  'restaurants.controller.js': fs.readFileSync('src/app_client/restaurants/restaurants.controller.js', 'utf-8'),
  'trola.controller.js': fs.readFileSync('src/app_client/trola/trola.controller.js', 'utf-8'),
  'events.controller.js': fs.readFileSync('src/app_client/events/events.controller.js', 'utf-8'),
  'eventModal.controller.js': fs.readFileSync('src/app_client/eventModal/eventModal.controller.js', 'utf-8'),
  'taskModalnoOkno.controller.js': fs.readFileSync('src/app_client/taskModalnoOkno/taskModalnoOkno.controller.js', 'utf-8'),
  'todoModalnoOkno.controller.js': fs.readFileSync('src/app_client/todoModalnoOkno/todoModalnoOkno.controller.js', 'utf-8'),
  'systemMessage.controller.js': fs.readFileSync('src/app_client/systemMessage/systemMessage.controller.js', 'utf-8'),
  'systemMessageModal.controller.js': fs.readFileSync('src/app_client/systemMessageModal/systemMessageModal.controller.js', 'utf-8'),
  'navigation.controller.js': fs.readFileSync('src/app_client/mutual/directives/navigation/navigation.controller.js', 'utf-8'),
  'db.controller.js': fs.readFileSync('src/app_client/db/db.controller.js', 'utf-8'),
  'db.service.js': fs.readFileSync('src/app_client/mutual/services/db.service.js', 'utf-8'),
  'authentication.service.js': fs.readFileSync('src/app_client/mutual/services/authentication.service.js', 'utf-8'),
  'showUserData.service.js': fs.readFileSync('src/app_client/mutual/services/showUserData.service.js', 'utf-8'),
  'verification.service.js': fs.readFileSync('src/app_client/mutual/services/verification.service.js', 'utf-8'),
  'events.service.js': fs.readFileSync('src/app_client/mutual/services/events.service.js', 'utf-8'),
  'systemMessage.service.js': fs.readFileSync('src/app_client/mutual/services/systemMessage.service.js', 'utf-8'),
  'navigation.directive.js': fs.readFileSync('src/app_client/mutual/directives/navigation/navigation.directive.js', 'utf-8'),
  'footer.directive.js': fs.readFileSync('src/app_client/mutual/directives/footer/footer.directive.js', 'utf-8'),
  'header.directive.js': fs.readFileSync('src/app_client/mutual/directives/header/header.directive.js', 'utf-8')
});

fs.writeFile('src/public/angular/straightas.min.js', zdruzeno.code, function(error) {
  if (error)
    console.log(error);
  else
    console.log('Skripta je zgenerirana in shranjena v "src/public/angular/straightas.min.js".');
});

var indexApiv1 = require('./app_api/routes/index');
//var indexRouter = require('./app_server/routes/index');
var usersRouter = require('./app_server/routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

app.use('/api/v1', indexApiv1);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

//app.use('/', indexRouter);
app.use('/users', usersRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "sporočilo": err.name + ": " + err.message
    });
  }
});

module.exports = app;

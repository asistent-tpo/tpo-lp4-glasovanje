var apiParams = {
  server: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParams.server = 'https://straightas-tpo.herokuapp.com';
}

/* Vrni stran z Angular SPA */
module.exports.angularApp = function(req, res) {
   //console.log("loading mainView");
//    res.render('insideLayout', {
 //     baseUrl: apiParams.req
  //  });
  res.render('layout', { title: 'Login page' });
};
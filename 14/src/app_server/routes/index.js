var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');
var ctrlOther = require('../controllers/other');

/* GET home page. */
//router.get('/', ctrlMain.index);

// ANGULAR APP
router.get('/', ctrlOther.angularApp);

/**
 * Ko uporabnik obišče začetno stran,
 * izpiši začetni pozdrav

router.get('/', function (zahteva, odgovor) {
  odgovor.send(
    '<h1>Lepo pozdravljen naključen uporabnik!</h1>' +
    '<p>A veš, da ti verjetno ne veš, da jaz vem, da ti ' +
    'uporabljaš naslednji spletni brskalnik?</p>' + '<pre>' +
    zahteva.get('User-Agent') + '</pre>'
  );
});
 */
module.exports = router;

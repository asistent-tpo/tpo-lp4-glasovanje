(function() {
  function navigationCtrl($location, authentication, $route) {
    var navvm = this;
    
    //vm.trenutnaLokacija = $location.path();
    
    navvm.isLoggedIn = authentication.isLoggedIn();
    
    navvm.normalUser = false;
    if(authentication.isLoggedIn() && authentication.currentUser().normalUser) {
      navvm.normalUser = true;
    }
    //vm.currentUser = authentication.currentUser();

    navvm.logout = function() {
      //console.log("odjavljlam v kontrolerju navigacija");
      authentication.logout()
      $location.path('/login');
      $route.reload();
    };
  }
  navigationCtrl.$inject = ['$location', 'authentication', '$route'];

  /* global angular */
  angular
    .module('straightas')
    .controller('navigationCtrl', navigationCtrl);
})();
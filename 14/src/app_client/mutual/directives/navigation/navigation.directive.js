/* TODO kaj je s temi templateUrl potmi?? */
(function() {
  var navigation = function() {

    return {
      restrict: 'EA',
      templateUrl: '/mutual/directives/navigation/navigation.template.html',
      controller: 'navigationCtrl',
      controllerAs: 'navvm'
    };
  };
  
  /* global angular */
  angular
    .module('straightas')
    .directive('navigation', navigation);
})();
(function() {
  var noga = function() {
    return {
      restrict: 'EA',
      templateUrl: '/mutual/directives/footer/footer.template.html'
    };
  };
  
  /* global angular */
  angular
    .module('straightas')
    .directive('noga', noga);
})();
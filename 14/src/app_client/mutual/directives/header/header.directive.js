(function() {
  var glava = function() {
    return {
      restrict: 'EA',
      scope: {
        vsebina: '=vsebina'
      },
      templateUrl: '/mutual/directives/header/header.template.html'
    };
  };

  /* global angular */
  angular
    .module('straightas')
    .directive('glava', glava);
})();
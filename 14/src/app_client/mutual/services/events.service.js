(function() {  
  var eventsData = function($http, authentication) {
    var eventsList = function() {
      return $http.get('/api/v1/events', {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
             });
    };
    
    var addEvent = function(data) {
      //console.log(podatki);
      return $http.post('/api/v1/events', data, {
                headers: {
                  'Authorization': 'Bearer ' +  authentication.returnToken()
                }
             });
    };
    
    var deleteEvent = function(idDelete) {
      return $http.delete('/api/v1/events/' + idDelete, {
                headers: {
                  'Authorization': 'Bearer ' +  authentication.returnToken()
                }
             });
    };
    
    
    return {
      eventsList: eventsList,
      addEvent: addEvent,
      deleteEvent: deleteEvent
    };
  };
  
  
  eventsData.$inject = ['$http', 'authentication'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('eventsData', eventsData);
})();
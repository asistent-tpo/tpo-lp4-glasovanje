(function() {
    var idUser = null
    var datum = null
    
    var showUserData = function($http, authentication) {
        
        var nastaviDatum = function(datum2) {
            //console.log("nastavil datum na " + datum2)
            datum = datum2
        }
            
        var userData = function(id) {
            idUser = id
            return $http.get('/api/v1/users/' + id, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
             });
        };
        
        var addTask = function(data) {
            //console.log("dobil v controller:")
            //console.log(datum)
            data.date = datum
            return $http.post('/api/v1/users/' + idUser + '/todoList', data, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            });
        }
        
        var deleteTask = function(id) {
            //console.log(id)
            return $http.delete('/api/v1/users/' + idUser + '/todoList/' + id, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            })
        }
        
        var addToDo = function(data) {
            //console.log("dobil v controller:")
            //console.log(data)
            return $http.post('/api/v1/users/' + idUser + '/calendarEntries', data, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            });
        }
        
        var posodobiToDo = function(idToDo, data) {
            return $http.put('/api/v1/users/' + idUser + '/calendarEntries/' + idToDo, data, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            });
        }
        
        var deleteToDo = function(idToDo) {
            return $http.delete('/api/v1/users/' + idUser + '/calendarEntries/' + idToDo, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            });
        }
        
        var deleteTimeTableEntry = function(id) {
            return $http.delete('/api/v1/users/' + idUser + '/timetableEntries/' + id, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            });
        }
        
        var addToTimetable = function(data) {
            return $http.post('/api/v1/users/' + idUser + '/timetableEntries', data, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
            });
        }
        
        var allUsers = function(id) {
            return $http.get('/api/v1/users', {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
             });
        };
        return {
            userData: userData,
            addTask: addTask,
            deleteTask: deleteTask,
            addToDo: addToDo,
            posodobiToDo: posodobiToDo,
            addToTimetable: addToTimetable,
            deleteToDo: deleteToDo,
            deleteTimeTableEntry: deleteTimeTableEntry,
            nastaviDatum: nastaviDatum,
            allUsers: allUsers,
        };
    }

    showUserData.$inject = ['$http', 'authentication'];
    
    /* global angular */
    angular
        .module('straightas')
        .service('showUserData', showUserData);
})();
(function() {
  function verification($window, $http, authentication) {
    
    var verify = function(user) {
        //console.log("verification");
        
        //console.log("USER VERICIFATION SERVICE");
        //console.log(user);
        //console.log(authentication.currentUser().id);
        
        return $http.put('/api/v1/users/' + authentication.currentUser().id + '/verify', user, {
                headers: {
                  'Authorization': 'Bearer ' + authentication.returnToken()
                }
             }).then(
                function success(response) {
                    //console.log("verificirano");
                    return true;
                });
    };
    
    return {
      verify: verify
    };
  }
  verification.$inject = ['$window', '$http', 'authentication'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('verification', verification);
})();
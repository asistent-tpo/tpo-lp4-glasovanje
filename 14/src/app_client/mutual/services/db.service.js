(function() {
  var dbStoritev = function($http) {

    var vstaviEvent = function(podatki) {
      return $http.post('/api/v1/DBevents', podatki);
    };
    
    var izbrisiEvent = function() {
      return $http.delete('/api/v1/DBevents');
    };
    
    
    var vstaviUser = function(podatki) {
      return $http.post('/api/v1/DBusers', podatki);
    };
    
    var izbrisiUser = function() {
      return $http.delete('/api/v1/DBusers');
    };
    
    
    return {
      vstaviEvent: vstaviEvent,
      izbrisiEvent: izbrisiEvent,
      vstaviUser: vstaviUser,
      izbrisiUser: izbrisiUser
    };
  };
  dbStoritev.$inject = ['$http'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('dbStoritev', dbStoritev);
})();
(function() {  
  var systemMessageData = function($http, authentication) {
    
    var sendMessage = function(data) {
      //console.log(podatki);
      return $http.post('/api/v1/systemMessage', data, {
                headers: {
                  'Authorization': 'Bearer ' +  authentication.returnToken()
                }
             });
    };
    
    return {
      sendMessage: sendMessage
    };
  };
  
  
  systemMessageData.$inject = ['$http', 'authentication'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('systemMessageData', systemMessageData);
})();
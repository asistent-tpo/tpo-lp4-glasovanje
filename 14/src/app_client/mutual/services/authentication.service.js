(function() {
  function authentication($window, $http) {
    
    var saveToken = function(token) {
      //console.log("ZETON = ");
      //console.log(token);
      $window.localStorage['straightastpo-token'] = token;
    };
    
    var returnToken = function() {
      //console.log("sem v vrni token token = " + );
     return $window.localStorage['straightastpo-token'];
    };
    
    var registration = function(user) {
      return $http.post('/api/v1/registration', user).then(
        function success(response) {
          saveToken(response.data.token);
        });
    };

    var login = function(user) {
      //console.log("sem v login v app_client/skupno/storitve/avtentikacija.storitev.js");
      //console.log(user);
      
      return $http.post('/api/v1/login', user).then(
        function success(response) {
          //console.log("response.DATA V FUNKCIJI login = ");
          //console.log(response.data);
          saveToken(response.data.token);
        });
    };
    
    var changePass = function(user){
      //console.log("authentication pass reset");
        
        //console.log("USER VERICIFATION SERVICE");
        //console.log(user);
        //console.log(authentication.currentUser().id);
        
        return $http.put('/api/v1/users/' + currentUser().id + '/changepassword', user, {
                headers: {
                  'Authorization': 'Bearer ' + returnToken()
                }
             }).then(
                function success(response) {
                    //console.log("verificirano");
                    return true;
                });
    };
    
    var logout = function() {
      $window.localStorage.removeItem('straightastpo-token');
    };
    
    var isLoggedIn = function() {
      var token = returnToken();
      if (token) {
        var usefulContent = JSON.parse($window.atob(token.split('.')[1]));
        return usefulContent.expiresDate > Date.now() / 1000;
      } else {
        return false;
      }
    };
    
    var currentUser = function() {
        //console.log("sem v currentUser");
        if (isLoggedIn()) {
          var token = returnToken();
          var usefulContent = JSON.parse($window.atob(token.split('.')[1]));
          return {
            email: usefulContent.email,
            id: usefulContent._id,
            administrator: usefulContent.administrator,
            animator: usefulContent.animator,
            normalUser: usefulContent.normalUser,
            verified: usefulContent.verifiedEmail
          };
        } else {
          console.log("nisem prijavljen");
        }
     };
    
    return {
      saveToken: saveToken,
      returnToken: returnToken,
      registration: registration,
      login: login,
      logout: logout,
      isLoggedIn: isLoggedIn,
      currentUser: currentUser,
      changePass: changePass
    };
  }
  authentication.$inject = ['$window', '$http'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('authentication', authentication);
})();
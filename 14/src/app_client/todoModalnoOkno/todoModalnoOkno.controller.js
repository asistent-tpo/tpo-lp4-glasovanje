(function() {
  function todoModalnoOkno($uibModalInstance, authentication, showUserData) {
    var vm = this;

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.name) {
        vm.napakaNaObrazcu = "Please fill all fields!";
        return false;
      } else {
        vm.adToDo(vm.podatkiObrazca);
      }
    };
    
    vm.adToDo = function(podatkiObrazca) {
      showUserData.addToDo({
        title: podatkiObrazca.name,
        date: podatkiObrazca.date,
        description: podatkiObrazca.descriptionOfEvent
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Error at saving new tip, please try again!";
        }
      );
    };
    
  }
  todoModalnoOkno.$inject = ['$uibModalInstance', 'authentication', 'showUserData'];

  /* global angular */
  angular
    .module('straightas')
    .controller('todoModalnoOkno', todoModalnoOkno);
})();
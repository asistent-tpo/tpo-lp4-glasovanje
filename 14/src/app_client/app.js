(function() {

    function setting($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'home/home.view.html',
                controller: 'homeCtrl',
                controllerAs: 'vm'
            })
            .when('/info', {
                templateUrl: 'mutual/views/genericText.view.html',
                controller: 'infoCtrl',
                controllerAs: 'vm'
            })
            .when('/register', {
                templateUrl: '/authentication/registration/registration.view.html',
                controller: 'registrationCtrl',
                controllerAs: 'vm'
            })
            .when('/login', {
                templateUrl: '/authentication/login/login.view.html',
                controller: 'loginCtrl',
                controllerAs: 'vm'
            })
            .when('/verify', {
                templateUrl: '/verification/verify.view.html',
                controller: 'verifyCtrl',
                controllerAs: 'vm'
            })
            .when('/changePassword', {
                templateUrl: '/authentication/changePassword/changePassword.view.html',
                controller: 'changePasswordCtrl',
                controllerAs: 'vm'
            })
            .when('/trola', {
                templateUrl: 'trola/trola.view.html',
                controller: 'trolaCtrl',
                controllerAs: 'vm'
            })
            .when('/events', {
                templateUrl: 'mutual/views/events.view.html',
                controller: 'eventsCtrl',
                controllerAs: 'vm'
            })
            .when('/restaurants', {
                templateUrl: 'restaurants/restaurants.view.html',
                controller: 'restaurantsCtrl',
                controllerAs: 'vm'
            })
            .when('/systemMessages', {
                templateUrl: 'mutual/views/systemMessage.view.html',
                controller: 'systemMessageCtrl',
                controllerAs: 'vm'
            })
            .when('/db', {
            templateUrl: 'mutual/views/db.view.html',
            controller: 'dbCtrl',
            controllerAs: 'vm'
            })
            .otherwise({redirectTo: '/'});
        
        $locationProvider.html5Mode(true);

    }
    
    /* global angular */
    angular
        .module('straightas', ['ngRoute', 'ngSanitize', 'ui.bootstrap'])
        .config(['$routeProvider', '$locationProvider', setting]);

    
})();
(function() {
  function dbCtrl(dbStoritev, $location) {
    var vm = this;
    
    // PODATKOV NE SPREMINJATI BREZ DOVOLJENJA ALJAŽa KER SO POSEBNOSTI IN POTEM NE BO DELOVALA STRAN
    vm.email1 = "student@email.si";
    vm.geslo1 = "student";
    
    vm.email2 = "animator@email.si";
    vm.geslo2 = "animator";
    
    vm.email3 = "admin@email.si";
    vm.geslo3 = "admin";
    
    
    vm.dodajEvent = function(podatki) {
      dbStoritev.vstaviEvent(podatki).then(
        function success(odgovor) {
          console.myLog('STRAN DB: Event uspesno dodan');
        },
        function error(odgovor) {
          console.myLog('STRAN DB: Event NEuspesno dodan');
        }
      );
    };
    
    vm.izbrisiEvent = function() {
      dbStoritev.izbrisiEvent().then(
        function success(odgovor) {
          console.myLog('STRAN DB: Eventi uspesno izbrisani');
        },
        function error(odgovor) {
          console.myLog('STRAN DB: Eventi NEuspesno izbrisani');
        }
      );
    };
    
    vm.dodajUser = function(podatki) {
      dbStoritev.vstaviUser(podatki).then(
        function success(odgovor) {
          console.myLog('STRAN DB: User uspesno dodan');
        },
        function error(odgovor) {
          console.myLog('STRAN DB: User NEuspesno dodan');
        }
      );
    };
    
    vm.izbrisiUser = function() {
      dbStoritev.izbrisiUser().then(
        function success(odgovor) {
          console.myLog('STRAN DB: Userji uspesno izbrisani');
        },
        function error(odgovor) {
          console.myLog('STRAN DB: Userji NEuspesno izbrisani');
        }
      );
    };
    
(function (logger) {
    console.myLog = function () {
        var output = "", arg, i;

        for (i = 0; i < arguments.length; i++) {
            arg = arguments[i];
            output += "<span class=\"log-" + (typeof arg) + "\">";

            if (
                typeof arg === "object" &&
                typeof JSON === "object" &&
                typeof JSON.stringify === "function"
            ) {
                output += JSON.stringify(arg);   
            } else {
                output += arg;   
            }

            output += "</span>&nbsp;";
        }

        logger.innerHTML += output + "<br>";
    };
    })(document.getElementById("logger"));
    
    vm.izprazniLogger = function() {
      console.log('izprazni logger');
      document.getElementById("logger").innerHTML = "";
    };
    
    vm.pojdiNaHomePage = function() {
      $location.path("/");
    };
    
    vm.event1 = {
        author: vm.email2,
        name: 'ŠKISOVA tržnica',
        date: "2019-05-09T12:10:40.748Z",
        organizer: 'Zveza ŠKIS',
        descriptionOfEvent: 'Škisova tržnica vsako leto gosti uveljavljena imena slovenske in tuje glasbene scene, priložnost pa dobijo tudi še neuveljavljene perspektivne glasbene skupine.',
      };
      
    vm.event2 = {
        author: vm.email2,
        name: 'Konferenca Evroštudent VI',
        date: "2018-12-14T12:10:40.748Z",
        organizer: 'Pedagoški inštitut',
        descriptionOfEvent: 'V petek, 14. decembra 2018, na Ministrstvu za izobraževanje, znanost in šport  v sodelovanju s Pedagoškim inštitutom organizirajo konferenco ob zaključku VI. cikla mednarodne raziskave EVROŠTUDENT, na katerem bo predstavljena tudi tiskana publikacija slovenskih izsledkov raziskave.',
      };
      
    vm.event3 = {
        author: vm.email2,
        name: 'Krvodajalska akcija – Častim pol litra!',
        date: "2019-04-06T12:10:40.748Z",
        organizer: 'ŠOS',
        descriptionOfEvent: 'Vsi študenti, ki lahko darujejo kri, so vabljeni, da se vnovič številčno odzovejo povabilu in častijo rundo, ki rešuje življenja. Študenti, ki jim predlagani termini ne ustrezajo, pa so vabljeni, da se kadarkoli zglasijo na območnih enotah Zavoda RS za transfuzijsko medicino, saj so potrebe po krvi skozi vso leto velike.',
      };
      
      
    vm.user1 =
        {
          name: 'Student Janez',
	        email: vm.email1,
        	geslo: vm.geslo1,
        	verifiedEmail: true,
        	verificationUuid: '123456789',
        	userCreatedDate: '2019-04-04T12:10:40.748Z',
        	administrator: false,
        	animator: false,
        	normalUser: true,
        	// to je za todo seznam
        	calendarEntries: [{
                dateCreated: "2019-05-26T22:24:30.480Z",
                dateToDo: "2019-05-26T22:24:30.480Z",
                dateCompleted: "2019-05-26T22:24:30.480Z",
                title: "Naredi TPO",
                description: "Obvezno naredi tpo",
                duration: 0
            },
            {
                dateCreated: "2019-05-26T22:25:17.624Z",
                dateToDo: "2019-05-26T22:25:17.624Z",
                dateCompleted: "2019-05-26T22:25:17.624Z",
                title: "Pojdi v trgovino",
                description: "Kupi kruh, mleko in pivo",
                duration: 0
            },
            {
                dateCreated: "2019-05-26T22:27:46.016Z",
                dateToDo: "2019-05-26T22:27:46.016Z",
                dateCompleted: "2019-05-26T22:27:46.016Z",
                title: "Pokliči zdravnika",
                description: "Prosi ga za napotnico k specialistu",
                duration: 0
            }],
          timetableEntries: [{
                duration: 3,
                dateCreated: "2019-05-20T11:00:00.000Z",
                dateCompleted: "2019-05-26T22:28:54.970Z",
                title: "TPO",
                description: "#ccffff"
            },
            {
                "duration": 3,
                "dateCreated": "2019-05-21T12:00:00.000Z",
                "dateCompleted": "2019-05-27T00:43:50.057Z",
                "_id": "5ceb32c6167d40123e397916",
                "title": "OIM",
                "description": "#ffccff"
            },
            {
                "duration": 1,
                "dateCreated": "2019-05-21T14:00:00.000Z",
                "dateCompleted": "2019-05-27T00:43:50.060Z",
                "_id": "5ceb32c6167d40123e397917",
                "title": "OIM",
                "description": "#ffccff"
            },
            {
                "duration": 2,
                "dateCreated": "2019-05-21T13:00:00.000Z",
                "dateCompleted": "2019-05-27T00:43:50.091Z",
                "_id": "5ceb32c6167d40123e397918",
                "title": "OIM",
                "description": "#ffccff"
            },
            {
                duration: 1,
                dateCreated: "2019-05-23T08:00:00.000Z",
                dateCompleted: "2019-05-26T23:02:22.132Z",
                title: "RIS",
                description: "#ffff99"
            }],
          // to je za koledar
          todoList: [{
                completed: false,
                dateCreated: "2019-05-26T22:31:08.997Z",
                dateCompleted: "2019-05-26T22:31:08.997Z",
                title: "Pojdi v knjižnico"
            },
            {
                completed: false,
                dateCreated: "2019-05-29T22:31:08.997Z",
                dateCompleted: "2019-05-29T22:31:08.997Z",
                title: "Pokliči teto"
            },
            {
                completed: false,
                dateCreated: "2019-06-02T22:31:08.997Z",
                dateCompleted: "2019-06-02T22:31:08.997Z",
                title: "Pospravi sobo"
            },{
                completed: false,
                dateCreated: "2019-06-12T22:31:08.997Z",
                dateCompleted: "2019-06-12T22:31:08.997Z",
                title: "Pojdi v kino"
            },
            {
                completed: false,
                dateCreated: "2019-05-26T22:31:08.997Z",
                dateCompleted: "2019-05-26T22:31:08.997Z",
                title: "Rojstni dan Rok"
            }],
      };
      
    vm.user2 = {
          name: 'Animator Mitja',
	        email: vm.email2,
        	geslo: vm.geslo2,
        	verifiedEmail: true,
        	verificationUuid: '123456789',
        	userCreatedDate: '2019-04-04T12:10:40.748Z',
        	administrator: false,
        	animator: true,
        	normalUser: false,
      };
      
      vm.user3 = {
          name: 'Admin Slavko',
	        email: vm.email3,
        	geslo: vm.geslo3,
        	verifiedEmail: true,
        	verificationUuid: '123456789',
        	userCreatedDate: '2019-04-04T12:10:40.748Z',
        	administrator: true,
        	animator: false,
        	normalUser: false,
      };

  }
  dbCtrl.$inject = ['dbStoritev', '$location'];
  
  /* global angular */
  angular
    .module('straightas')
    .controller('dbCtrl', dbCtrl);
})();
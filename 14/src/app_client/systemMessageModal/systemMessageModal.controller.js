(function() {
  function systemMessageModal(systemMessageData, showUserData, $uibModalInstance, authentication) {
    var vm = this;

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.message) {
        vm.napakaNaObrazcu = "Please write the message!";
        return false;
      } else {
        vm.getAllEmails(vm.sendMessage, vm.podatkiObrazca.message);
      }
    };
    
    vm.sendMessage = function(emails, message) {
      systemMessageData.sendMessage({
        emails: emails,
        text: message
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Error at sending message, please try again!";
        }
      );
    };
    
    vm.getAllEmails = function(callback, message) {
      showUserData.allUsers({
      }).then(
        function success(odgovor) {
          var users = odgovor.data;
          var rez = ""
          var i;
          for(i = 0; i < users.length - 1; i++) {
            rez += users[i].email + ", ";
          }
          rez += users[i].email;
          callback(rez, message);
        },
        function error(odgovor) {
          return "";
        }
      );
    };
    
  }
  systemMessageModal.$inject = ['systemMessageData', 'showUserData', '$uibModalInstance', 'authentication'];

  /* global angular */
  angular
    .module('straightas')
    .controller('systemMessageModal', systemMessageModal);
})();
(function() {
  function taskModalnoOkno($uibModalInstance, authentication, showUserData) {
    var vm = this;

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.name) {
        vm.napakaNaObrazcu = "Please fill all fields!";
        return false;
      } else {
        vm.addTask(vm.podatkiObrazca);
      }
    };
    
    vm.addTask = function(podatkiObrazca) {
      showUserData.addTask({
        title: podatkiObrazca.name
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Error at saving new tip, please try again!";
        }
      );
    };
    
  }
  taskModalnoOkno.$inject = ['$uibModalInstance', 'authentication', 'showUserData'];

  /* global angular */
  angular
    .module('straightas')
    .controller('taskModalnoOkno', taskModalnoOkno);
})();
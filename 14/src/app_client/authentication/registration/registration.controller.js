(function() {
  function registrationCtrl($location, authentication) {
    var vm = this;
    
    vm.pageHeader = {
        title: "Registriraj se že danes!",
    };

    vm.loginData = {
      email: "",
      password: "",
      password2: ""
    };
    vm.prvotnaStran = '/';
    vm.errorOnForm = "";
    
    vm.sendingData = function() {
        //console.log("posiljanje podatkov funkcija");
        vm.errorOnForm = "";
        if (!vm.loginData.email || !vm.loginData.password || !vm.loginData.password2) {
          vm.errorOnForm = "Zahtevani so vsi podatki, prosim poskusite znova!";
          return false;
        } else if (vm.loginData.password != vm.loginData.password2) {
            vm.errorOnForm = "Vnešena gesla morata biti enaka!";
        } 
        else {
          vm.registerUser();
        }
    };

    vm.registerUser = function() {
        vm.errorOnForm = "";
        authentication
        .registration(vm.loginData)
        .then(
            function(success) {
              $location.path(vm.prvotnaStran);
            },
            function(error) {
              if (error.data.name === "MongoError" && error.data.code === 11000)
                vm.errorOnForm = "Račun z uporabljenim e-mailom že obstaja!";
              else {
                vm.errorOnForm = "Unhandled error!";
              }
            }
        );
    };
      
  }
  registrationCtrl.$inject = ['$location', 'authentication'];

  /* global angular */
  angular
    .module('straightas')
    .controller('registrationCtrl', registrationCtrl);
})();
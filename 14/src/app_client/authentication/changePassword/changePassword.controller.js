(function() {
  function changePasswordCtrl($location, authentication) {
    var vm = this;
    
    vm.pageHeader = {
      title: "Spremembra gesla"
    };
    
    vm.loginData = {
      passwordOld: "",
      passwordNew2: "",
      passwordNew: ""
    };

    vm.homePage = '/';
    vm.errorOnForm = "";
    
    vm.sendingData = function() {
      vm.errorOnForm = "";
      if (!vm.loginData.passwordOld || !vm.loginData.passwordNew || !vm.loginData.passwordNew2) {
        vm.errorOnForm = "Zahtevani so vsi podatki, prosim poskusite znova!";
        console.log(vm.loginData)
        return false;
      } 
      else if (vm.loginData.passwordNew != vm.loginData.passwordNew2){
          vm.errorOnForm = "Vnešeni gesli nista enaki";
          return false;
      }
      else {
        vm.doChange();
      }
    };

    vm.doChange = function() {
      vm.errorOnForm = "";
      authentication
        .changePass(vm.loginData)
        .then(
          function(success) {
            $location.search('stran', null);
            $location.path(vm.homePage);
          },
          function(napaka) {
            vm.errorOnForm = napaka.data.message;
          }
        );
    };
  }
  changePasswordCtrl.$inject = ['$location', 'authentication'];

  /* global angular */
  angular
    .module('straightas')
    .controller('changePasswordCtrl', changePasswordCtrl);
})();
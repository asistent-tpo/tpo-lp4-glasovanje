(function() {
  function loginCtrl($location, authentication) {
    var vm = this;
    //console.log("IN LOGIN CTRL");
    vm.pageHeader = {
      title: "Prijava v StraightAs"
    };
    
    vm.loginData = {
      email: "",
      password: ""
    };

    vm.homePage = '/';
    vm.errorOnForm = "";

    
    vm.sendingData = function() {
      //console.log("sending data");
      vm.errorOnForm = "";
      if (!vm.loginData.email || !vm.loginData.password) {
        vm.errorOnForm = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } else {
        vm.doLogin();
      }
    };

    vm.doLogin = function() {
      //console.log("doLogin");
      vm.errorOnForm = "";
      authentication
        .login(vm.loginData)
        .then(
          function(success) {
            $location.search('stran', null);
            $location.path(vm.homePage);
          },
          function(napaka) {
            vm.errorOnForm = napaka.data.message;
          }
        );
    };
  }
  loginCtrl.$inject = ['$location', 'authentication'];

  /* global angular */
  angular
    .module('straightas')
    .controller('loginCtrl', loginCtrl);
})();
(function() {
  function eventModal($uibModalInstance, authentication, eventsData) {
    var vm = this;

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.name || !vm.podatkiObrazca.date || !vm.podatkiObrazca.organizer || !vm.podatkiObrazca.descriptionOfEvent) {
        vm.napakaNaObrazcu = "Please fill all fields!";
        return false;
      } else {
        vm.addEvent(vm.podatkiObrazca);
      }
    };
    
    vm.addEvent = function(podatkiObrazca) {
      eventsData.addEvent({
        author: authentication.currentUser().id,
        name: podatkiObrazca.name,
        date: podatkiObrazca.date,
        organizer: podatkiObrazca.organizer,
        descriptionOfEvent: podatkiObrazca.descriptionOfEvent
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Error at saving new tip, please try again!";
        }
      );
    };
    
  }
  eventModal.$inject = ['$uibModalInstance', 'authentication', 'eventsData'];

  /* global angular */
  angular
    .module('straightas')
    .controller('eventModal', eventModal);
})();
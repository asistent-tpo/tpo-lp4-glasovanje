(function() {
  function verifyCtrl($location, authentication, verification) {
    
    if (!authentication.isLoggedIn()) {
        $location.path("/login");
    } else {
    
        var vm = this;
        vm.pageHeader = {
          title: "Verificiranje v aplikaciji StraightAs"
        };
        
        vm.verificationData = {
          verificationUuid: "",
        };
    
        vm.homePage = '/';
        vm.errorOnForm = "";
        vm.successOnForm = "";
        vm.infoOnForm = "";
        vm.verified = false;
        console.log("VERIFICIRANJE")
        console.log(authentication.currentUser())
        if (authentication.currentUser().verified) {
            vm.infoOnForm = "Verifikacijo ste ze opravili!"
            vm.verified = true;
        }
        
        vm.sendingData = function() {
          vm.errorOnForm = "";
          if (!vm.verificationData.verificationUuid) {
            vm.errorOnForm = "Zahtevani so vsi podatki, prosim poskusite znova!";
            return false;
          } else {
            //console.log(vm.verificationData)
            vm.doVerification();
          }
        };
    
        vm.doVerification = function() {
          vm.errorOnForm = "";
          verification
            .verify(vm.verificationData)
            .then(
              function(success) {
                vm.successOnForm = "Uspešno ste se verificirali. Veselo uporabljanje aplikacije StraightAs še naprej"
              },
              function(error) {
                //console.log("Error na obrazcu");
                //console.log(error);
                vm.errorOnForm = error.data.message;
              }
            );
        };
    }
  }
  verifyCtrl.$inject = ['$location', 'authentication', 'verification'];

  /* global angular */
  angular
    .module('straightas')
    .controller('verifyCtrl', verifyCtrl);
})();
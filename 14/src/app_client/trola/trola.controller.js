(function() {
  function trolaCtrl($location, authentication) {
    var vm = this;
    
    if (!authentication.isLoggedIn()) {
      $location.path("/login");
    } else if(authentication.currentUser().administrator) {
      $location.path("/systemMessages");
    } else if(authentication.currentUser().animator) {
      $location.path("/events");
    }
    
    vm.pageHeader = {
      title: "Poizvedbe za avtobuse"
    };
    
  }
  trolaCtrl.$inject = ['$location', 'authentication']
  
  /* global angular */
  angular
    .module('straightas')
    .controller('trolaCtrl', trolaCtrl);
})();
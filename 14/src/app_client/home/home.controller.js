(function() { 
    
    function homeCtrl($uibModal, $scope, showUserData, authentication, $location) {
        if (!authentication.isLoggedIn()) {
            $location.path("/login");
        } else if(authentication.currentUser().administrator) {
            $location.path("/systemMessages");
        } else if(authentication.currentUser().animator) {
            $location.path("/events");
        } else {
        
            var vm = this;
            
            vm.idUser = authentication.currentUser().id;
            vm.title = 'To prihaja iz home controllerja, nad tem bi mogl pisat Pozdravljeni pa email prijavljenga/registriranga uporabnika';
            vm.data = "";
            
            if (!authentication.currentUser().verified) {
                vm.warningOnForm = "Pozor, niste še opravili verifikacije. Na voljo imate 2 tedna od dneva, ko ste si ustvarili račun. Nato vam bo dostop do aplikacije odvzet dokler ne boste verificirani."
                vm.verified = true;
            }
            showUserData.userData(vm.idUser).then(
                function success(response) {
                    vm.data = { user: response.data};
                    izvediFunkcijeZaCalendarTab(vm, $uibModal, showUserData)
                },
                function error(odgovor) {
                  //console.log(odgovor.e);
                }
            );
        }
    }
    
    var stevilkaMesecaVMesec = function(stevilkaDneva) {
        switch(stevilkaDneva) {
            case 1: return 'Januar';
            case 2: return 'Februar';
            case 3: return 'Marec';
            case 4: return 'April';
            case 5: return 'Maj';
            case 6: return 'Junij';
            case 7: return 'Juilj';
            case 8: return 'Avgust';
            case 9: return 'September';
            case 10: return 'Oktober';
            case 11: return 'November';
            case 12: return 'December';
            default: return '';
            
        }
    }
    
    var pogledToDos = function(vm) {
        //console.log(vm.data.user.calendarEntries)
        
        var allToDos = ''
        var stevecToDos = 0
        for (var i = 0; i < vm.data.user.calendarEntries.length; ++i) {
            if (vm.data.user.calendarEntries[i] != null && vm.data.user.calendarEntries[i].duration == 0) {
                var datum = vm.data.user.calendarEntries[i].dateToDo.split('T')[0]
                var todo = '<div class="col-6"><div class="text-center todos"><div class="card"><div class="card-body todo2" style="background: rgb(253, 255, 191);"><h5 class="card-title">'
                todo += vm.data.user.calendarEntries[i].title
                todo += '</h5><h6 class="card-subtitle mb-2 text-muted">'
                todo += datum
                todo += '</h6><p class="card-text">'
                todo += vm.data.user.calendarEntries[i].description
                todo += '</p><a href="#" id=gumbCompleteT' + stevecToDos + ' class="btn btn-success btn-sm">Complete</a></div></div></div></div>'
                
                allToDos += todo
                stevecToDos += 1
            }
        }
        
        document.getElementById('allToDos').innerHTML = allToDos
    }
    
    var napolniUrnik = function(vm) {
        for (var i = 0; i < vm.data.user.timetableEntries.length; ++i) {
            //console.log(vm.data.user.timetableEntries[i].dateCreated)
            if (vm.data.user.timetableEntries[i] != null) {
                var datumCel = vm.data.user.timetableEntries[i].dateCreated.split('T')
                var datumZaDan = datumCel[0].split('-')
                var datumZaUro = parseInt(datumCel[1].split(':')[0])
                
                //if (datumZaUro == 0)
                //    datumZaUro = 7
                var dan = 1
                
                if (datumZaDan[2] == '20') {
                    var dan = 1
                }
                if (datumZaDan[2] == '21') {
                    var dan = 2
                }
                if (datumZaDan[2] == '22') {
                    var dan = 3
                }
                if (datumZaDan[2] == '23') {
                    var dan = 4
                }
                if (datumZaDan[2] == '24') {
                    var dan = 5
                }
            
                
                //var dan = izracunajDan(datumZaDan[2], datumZaDan[1], datumZaDan[0])
                var color = vm.data.user.timetableEntries[i].description
                
                if (datumZaUro >= 7 && datumZaUro <= 21) {
                    var datumZaUroId = datumZaUro - 6
                    var idDnevaZaUrnik = datumZaUroId + pretvoriIdDanVDan(dan) + 'Urnik';
                    
                    var text = vm.data.user.timetableEntries[i].title
                    var textEntryUrnik = text.substring(0, 3)
                    if (text.length > 3) {
                        textEntryUrnik += '...'
                    }
                    //console.log(idDnevaZaUrnik)
                    document.getElementById(idDnevaZaUrnik).textContent = textEntryUrnik
                    if (color != null || color == 'empty')
                        document.getElementById(idDnevaZaUrnik).style.backgroundColor = color
                }
            }
        }
    }
    
    var pretvoriIdUreVUro = function(id) {
        switch (parseInt(id)) {
            case 1: return '7:00';
            case 2: return '8:00';
            case 3: return '9:00';
            case 4: return '10:00';
            case 5: return '11:00';
            case 6: return '12:00';
            case 7: return '13:00';
            case 8: return '14:00';
            case 9: return '15:00';
            case 10: return '16:00';
            case 11: return '17:00';
            case 12: return '18:00';
            case 13: return '19:00';
            case 14: return '20:00';
            case 15: return '21:00';
            default: return "";
        }
    }
    
    var pretvoriIdDanVDan = function(idDan) {
        switch (idDan) {
            case 1: return 'mon';
            case 2: return 'tor';
            case 3: return 'sre';
            case 4: return 'cet';
            case 5: return 'pet';
            default: return '';
        }
    }
    
    var spremeniImeDneva = function(dan) {
        switch(dan) {
            case 'mon': return 'Mon';
            case 'tor': return 'Tue';
            case 'sre': return 'Wed';
            case 'cet': return 'Thu';
            case 'pet': return 'Fri';
            default: '';
        }
    }
    
    var izvediFunkcijeZaCalendarTab = function(vm, $uibModal, showUserData) {
        izdelajKoledar();
        
        var brisanje = true
        var idPrejCollapsed = null;
        var prejMesec = null;
        var prejLeto = null;
        var prejDan = null
        var idDanZnan = null;
        
        angular.forEach(vm.data.user.todoList, function (value, key) {
            //console.log(value);
            //dodajIdRewarda(value._id)
            //dodajCenoRewarda(value.price)
            if (value.dateCreated != null) {
                var dateOriginal = value.dateCreated
                var title = value.title
                var date = pretvoriVMojDatum(dateOriginal)
                //console.log(title)
                //console.log(value)
                dodajDogodekVKoledar(date, title);
            }
        });
        izdelajKoledar();
        
        
        
        pogledToDos(vm);
        napolniUrnik(vm);
        
        vm.data.completeToDo = function() {
            //console.log("complete To do " + vrniLastHovered())
            var gumb = vrniLastHovered()
            if (gumb == null) {
                return
            }
            var gumb = gumb.split('T')
            if (gumb[0] == 'gumbComplete') {
                var stevec = gumb[1]
                //console.log(stevec)
                var stevecToDos = 0
                for (var i = 0; i < vm.data.user.calendarEntries.length; ++i) {
                    if (vm.data.user.calendarEntries[i] != null && vm.data.user.calendarEntries[i].duration == 0) {
                        if (stevecToDos == stevec) {
                            var id = vm.data.user.calendarEntries[i]._id
                            /*showUserData.posodobiToDo(vm.data.user.calendarEntries[i]._id, { duration: -1 }).then(
                                function success(odgovor) {
                                  //vm.modalnoOkno.zapri(odgovor.data);
                                  //console.log("success")
                                  //console.log(odgovor)
                                  for (var j = 0; j < vm.data.user.calendarEntries.length; ++j) {
                                      if (vm.data.user.calendarEntries[j]._id == id) {
                                          vm.data.user.calendarEntries[j].duration = -1
                                      }
                                  }
                                  pogledToDos(vm);
                                },
                                function error(odgovor) {
                                    //console.log("error")
                                    cosole.log(odgovor)
                                  //vm.napakaNaObrazcu = "Error at deleting task, please try again!";
                                }
                            );*/
                            showUserData.deleteToDo(vm.data.user.calendarEntries[i]._id).then(
                                function success(odgovor) {
                                  //vm.modalnoOkno.zapri(odgovor.data);
                                  //console.log("success")
                                  //console.log(odgovor)
                                  for (var j = 0; j < vm.data.user.calendarEntries.length; ++j) {
                                      if (vm.data.user.calendarEntries[j]._id == id) {
                                          vm.data.user.calendarEntries.splice(j, 1)
                                      }
                                  }
                                  pogledToDos(vm);
                                },
                                function error(odgovor) {
                                    //console.log("error")
                                    cosole.log(odgovor)
                                  //vm.napakaNaObrazcu = "Error at deleting task, please try again!";
                                }
                            );
                            break;
                        }
                        stevecToDos += 1
                    }
                }
            }
        }
    
        
        vm.data.dodajTodo = function() {
            var primerekModalnegaOkna = $uibModal.open({
            templateUrl: '/todoModalnoOkno/todoModalnoOkno.view.html',
            controller: 'todoModalnoOkno',
            controllerAs: 'vm',
            resolve: {
                detailsEvent: function() {
                    return {
                        title: "Add new todo"
                    };
                }
            },
            windowClass: 'show',
            backdropClass: 'show'
            });
            
            primerekModalnegaOkna.result.then(function(podatki) {
                //if (typeof podatki != 'undefined')
                //    vm.data.events.push(podatki);
                //console.log(podatki)
                vm.data.user.calendarEntries.push(podatki)
                pogledToDos(vm)
            }, function(napaka) {
                // Ulovi dogodek in ne naredi ničesar
            });
        }
        
        vm.data.deleteTask = function() {
            //console.log("delete tasks")
            if (brisanje) {
                var elements = document.getElementsByClassName('items');
                document.getElementById("delete_task").textContent = "cancel";
                
                for (var i = 0; i < elements.length; i++) {
                    elements[i].classList.add("items_deletion");
                }
                brisanje = false;
            } else {
                var elements = document.getElementsByClassName('items');
                document.getElementById("delete_task").textContent = "delete";
                
                for (var i = 0; i < elements.length; i++) {
                    elements[i].classList.remove("items_deletion");
                }
                brisanje = true;
            }
        }
        
        vm.data.deleteMarkedTask = function() {
            if (!brisanje) {
                //console.log("delete " + vrniLastHovered())
                var stevec2 = vrniLastHovered()
                var stevec = 0
                angular.forEach(vm.data.user.todoList, function (value, key) {
                    //console.log(value);
                    if (value.dateCreated != null) {
                        var datum = value.dateCreated.split('T')[0].split('-')
                        //console.log(datum)
                        var dan = document.getElementById(idDanZnan).textContent
                        if (parseInt(dan) < 10) {
                            dan = '0' + dan
                        }
                        if (datum[2] == dan && datum[1] == prejMesec && datum[0] == prejLeto) {
                            if (stevec2 == stevec) {
                                //console.log(value)
                                var id = value._id
                                showUserData.deleteTask(value._id).then(
                                    function success(odgovor) {
                                      //vm.modalnoOkno.zapri(odgovor.data);
                                      //console.log("success")
                                      //console.log(odgovor)
                                      var index = 0
                                      for (var index = 0; index < vm.data.user.todoList.length ; index += 1){
                                          if (vm.data.user.todoList[index]._id == id) {
                                              vm.data.user.todoList.splice(index, 1)
                                              break;
                                          }
                                      }
                                      idPrejCollapsed = null;
                                      vm.data.obdelovanjeDatuma(idDanZnan)
                                    },
                                    function error(odgovor) {
                                        //console.log("error")
                                        //console.log(odgovor)
                                      //vm.napakaNaObrazcu = "Error at deleting task, please try again!";
                                    }
                                );
                                stevec += 1
                            } else {
                                stevec += 1
                            }
                        }
                    }
                });
            }
        }
        
        idUraZnana = null
        var danZnan = null
        var idUraZnana = null
        var idUraNovaZnana = null
        
        var vrniDatum = function() {
            //console.log("dan znan je " + danZnan)
            switch(danZnan) {
                case 'Mon': return '2019-05-20';
                case 'Tue': return '2019-05-21';
                case 'Wed': return '2019-05-22';
                case 'Thu': return '2019-05-23';
                case 'Fri': return '2019-05-24';
                default: return '';
            }
        }
        
        vm.data.addToTimeTable = function() {
            var stUr = document.getElementById('urnikDanInputStUr').value
            if (stUr == '' || stUr == 0) {
                stUr = 1
            }
            for (var i = 0; i < stUr; ++i) {
                vm.data.addToTimeTable2(stUr, i)
            }
        }
        
        vm.data.addToTimeTable2 = function(stUrNaZacetku, stUr) {
            var datum = vrniDatum()
            var ura = parseInt(idUraZnana) + 6 + stUr
            if (ura < 10) {
                ura = '0' + ura
            }
            var celaUra = ura + ':00:00.000Z'
            var datumCel = datum + "T" + celaUra
            
            for (var i = 0; i < vm.data.user.timetableEntries.length; ++i) {
                if (vm.data.user.timetableEntries[i].dateCreated == datumCel) {
                    //console.log('je ze noter ' + vm.data.user.timetableEntries[i].title)
                    var id = vm.data.user.timetableEntries[i]._id
                    showUserData.deleteTimeTableEntry(id).then(
                        function success(odgovor) {
                            //console.log(odgovor)
                            //vm.modalnoOkno.zapri(odgovor.data);
                            for (var j = 0; j < vm.data.user.timetableEntries.length; ++j) {
                                if (vm.data.user.timetableEntries[j]._id == id) {
                                    vm.data.user.timetableEntries.splice(j, 1)
                                }
                            }
                        },
                        function error(odgovor) {
                            //console.log(odgovor)
                            //vm.napakaNaObrazcu = "Error at saving new tip, please try again!";
                        }
                    );
                }
            }
            //console.log(datumCel)
            var color = document.getElementById('urnikDanInputColor').value
            var titleNov = document.getElementById('urnikDanInput').value
            if (titleNov[0] == ' ') {
                titleNov = titleNov.substr(1, titleNov.length)
            }
            if (titleNov == '') {
                titleNov = ' '
                color = 'empty'
            }
            showUserData.addToTimetable({
                title: titleNov,
                dateCreated: datumCel,
                description: color,
                duration: stUrNaZacetku - stUr
            }).then(
                function success(odgovor) {
                    //console.log(odgovor)
                    //vm.modalnoOkno.zapri(odgovor.data);
                    vm.data.user.timetableEntries.push(odgovor.data)
                    napolniUrnik(vm)
                    $('#collapseDanUrnik').collapse("hide")
                    idUraNovaZnana = null
                },
                function error(odgovor) {
                    //console.log(odgovor)
                    //vm.napakaNaObrazcu = "Error at saving new tip, please try again!";
                }
            );
            /*} else {
                napolniUrnik(vm)
                $('#collapseDanUrnik').collapse("hide")
                idUraNovaZnana = null
            }*/
        }
        
        
        vm.data.obdelavaUrnika = function(idUra) {
            var idUraCelotna = idUra
            idUra = idUra.split('Urnik')[0]
            //console.log("obdelava urnika id " + idUra)
            var dan = idUra.substring(idUra.length - 3, idUra.length );
            dan = spremeniImeDneva(dan)
            danZnan = dan
            var idUraZaUro = idUra.substring(0, idUra.length - 3);
            idUraZnana = idUraZaUro
            var ura = pretvoriIdUreVUro(idUraZaUro);
            if (idUraNovaZnana != idUraCelotna) {
                document.getElementById('danOpisInput').textContent = dan + " " + ura
                document.getElementById('urnikDanInput').value = ''
                document.getElementById('urnikDanInputStUr').value = ''
                
                for (var i = 0; i < vm.data.user.timetableEntries.length; ++i) {
                    var datumCel = vm.data.user.timetableEntries[i].dateCreated.split('T')
                    var datumZaDan = datumCel[0].split('-')
                    var datumZaUro = parseInt(datumCel[1].split(':')[0])
                    
                    //if (datumZaUro == 0)
                    //    datumZaUro = 7
                    var dan = 1
                    
                    if (datumZaDan[2] == '20') {
                        var dan = 1
                    }
                    if (datumZaDan[2] == '21') {
                        var dan = 2
                    }
                    if (datumZaDan[2] == '22') {
                        var dan = 3
                    }
                    if (datumZaDan[2] == '23') {
                        var dan = 4
                    }
                    if (datumZaDan[2] == '24') {
                        var dan = 5
                    }
                
                    
                    //var dan = izracunajDan(datumZaDan[2], datumZaDan[1], datumZaDan[0])
                    var color = vm.data.user.timetableEntries[i].description
                    if (datumZaUro >= 7 && datumZaUro <= 21) {
                        var datumZaUroId = datumZaUro - 6
                        var idDnevaZaUrnik = datumZaUroId + pretvoriIdDanVDan(dan) + 'Urnik';
                        if (idDnevaZaUrnik == idUraCelotna) {
                            var text = vm.data.user.timetableEntries[i].title
                            document.getElementById('urnikDanInput').value = text
                            document.getElementById('urnikDanInputStUr').value = vm.data.user.timetableEntries[i].duration
                            if (color != null || color != 'empty')
                                document.getElementById('urnikDanInputColor').value = color
                        }
                    }
                }
                
                
                $('#collapseDanUrnik').collapse("show")
                idUraNovaZnana = idUraCelotna
            } else {
                $('#collapseDanUrnik').collapse("hide")
                idUraNovaZnana = null
            }
        }
        
        vm.data.closeCurDanUrnik = function() {
            $('#collapseDanUrnik').collapse("hide")
            idUraNovaZnana = null
        }
        
        
        vm.data.obdelovanjeDatuma = function(idDan) {
            //console.log(document.getElementById(idDan).textContent + vrniCurMesec())
            var mesec = vrniCurMesec()
            var leto = vrniCurLeto()
            idDanZnan = idDan
            
            if (document.getElementById(idDan).classList.contains('prev-month')) {
                mesec -= 1
                if (mesec == 0) {
                    mesec = 12
                    leto -= 1
                }
            }
            else if (document.getElementById(idDan).classList.contains('next-month')) {
                mesec += 1
                if (mesec == 13) {
                    mesec = 1
                    leto += 1
                }
            }
            prejDan = document.getElementById(idDan).textContent
            document.getElementById('curDan').textContent = document.getElementById(idDan).textContent + ". " + stevilkaMesecaVMesec(mesec) + " " + leto;
            
            if (idPrejCollapsed != idDan || prejMesec != mesec || prejLeto != leto) {
                //prikazi potrebne podatke
                //console.log(vm.data.user.todoList)
                var innerHtml = "";
                var stevec = 0
                angular.forEach(vm.data.user.todoList, function (value, key) {
                    //console.log(value);
                    if (value.dateCreated != null) {
                        //console.log(value.dateCreated)
                        var datum = value.dateCreated.split('T')[0].split('-')
                        //console.log(datum)
                        var dan = document.getElementById(idDan).textContent
                        if (parseInt(dan) < 10) {
                            dan = '0' + dan
                        }
                        if (datum[2] == dan && datum[1] == mesec && datum[0] == leto) {
                            innerHtml += '<p class="items" id=' + stevec + '>' + value.title + '</p>';
                            stevec += 1
                        }
                    } else {
                        //console.log("date created null " + value.title)
                    }
                });
                
                document.getElementById('tasksVsebina').innerHTML = innerHtml;
                
                
                
                $('#collapseDan').collapse("show")
                idPrejCollapsed = idDan
            } else {
                $('#collapseDan').collapse("hide")
                idPrejCollapsed = null
                //vm.data.deleteTask()
            }
            prejMesec = mesec;
            prejLeto = leto;
            
            if (!brisanje) {
                //var elements = document.getElementsByClassName('items');
                document.getElementById("delete_task").textContent = "delete";
                brisanje = true;
                
                /*for (var i = 0; i < elements.length; i++) {
                    elements[i].classList.add("items_deletion");
                }*/
            }
        }
        
        vm.data.zapriCurDanKoledar = function() {
            $('#collapseDan').collapse("hide")
            idPrejCollapsed = null
            //vm.data.deleteTask()
        }
        
        vm.data.addTask = function() {
            //vm.showModalForAddTask = function() {
            //showUserData.nastaviDatum("2019-05-29")
            var leto = prejLeto
            var mesec = parseInt(prejMesec)
            if (mesec < 10) {
                mesec = '0' + mesec
            } 
            var dan = parseInt(prejDan)
            if (dan < 10) {
                dan = '0' + dan
            }
            var datum = leto + "-" + mesec + "-" + dan
            //console.log(datum)
            showUserData.nastaviDatum(datum)
            var primerekModalnegaOkna = $uibModal.open({
            templateUrl: '/taskModalnoOkno/taskModalnoOkno.view.html',
            controller: 'taskModalnoOkno',
            controllerAs: 'vm',
            resolve: {
                detailsEvent: function() {
                    return {
                        title: "Add new task"
                    };
                }
            },
            windowClass: 'show',
            backdropClass: 'show'
            });
            
            primerekModalnegaOkna.result.then(function(podatki) {
                //if (typeof podatki != 'undefined')
                //    vm.data.events.push(podatki);
                //console.log(podatki)
                vm.data.user.todoList.push(podatki)
                idPrejCollapsed = null;
                vm.data.obdelovanjeDatuma(idDanZnan)
                izdelajKoledar();
            }, function(napaka) {
                // Ulovi dogodek in ne naredi ničesar
                //console.log(napaka)
            });
            //};
        }
        
        vm.data.reload = function() {
            window.location.reload();
        }
    }
    
    homeCtrl.$inject = ['$uibModal', '$scope', 'showUserData', 'authentication', '$location']
    
    /* global angular */
    angular
        .module('straightas')
        .controller('homeCtrl', homeCtrl);
      
})();
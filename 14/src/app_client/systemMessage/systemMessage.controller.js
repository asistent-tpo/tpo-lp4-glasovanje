(function() {
  function systemMessageCtrl($uibModal, authentication, $location) {
  var vm = this;
  vm.message = "";
  vm.naslovUporabnika = 'Admin'

  if (!authentication.isLoggedIn()) {
    $location.path("/login");
  } else if(authentication.currentUser().animator) {
    $location.path("/events");
  }else if(authentication.currentUser().normalUser) {
    $location.path("/");
  } else {
    vm.administrator = true;
    // dodajanje sistemskega sporocila
    vm.showModalForSystemMessage = function() {
    var primerekModalnegaOkna = $uibModal.open({
      templateUrl: '/systemMessageModal/systemMessageModal.view.html',
      controller: 'systemMessageModal',
      controllerAs: 'vm',
      resolve: {
        detailsEvent: function() {
          return {
            title: "System message"
          };
        }
      },
      windowClass: 'show',
      backdropClass: 'show'
    });
    
    primerekModalnegaOkna.result.then(function(podatki) {
      if (typeof podatki != 'undefined')
        vm.message = "System message send";
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
        vm.message = "System message send ERROR";
    });
  };
  }
};
  
  systemMessageCtrl.$inject = ['$uibModal', 'authentication', '$location'];
  
  /* global angular */
  angular
    .module('straightas')
    .controller('systemMessageCtrl', systemMessageCtrl)
  
})();
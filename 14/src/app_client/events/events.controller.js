(function() {
  function eventsCtrl($uibModal, eventsData, authentication, $location) {
  var vm = this;
  vm.naslovUporabnika = '';
  vm.message = "Searching events.";
  vm.pageHead = {
    title: 'Events'
  };

  if(!authentication.isLoggedIn()) {
    $location.path("/login");
  } else if(authentication.currentUser().administrator) {
    $location.path("/systemMessages");
  } else {
    eventsData.eventsList().then(
      function success(response) {
        vm.message = response.data.length > 0 ? "" : "I do not find any events.";
        vm.data = {
          events: response.data
        };
      }, function error(response) {
        vm.message = "Error!";
        console.log(response.e);
     }
    );
    if(authentication.currentUser().normalUser) {
      vm.normalUser = true;
      vm.animator = false;
    }
    // dodajanje eventa
    else if(authentication.currentUser().animator) {
      vm.naslovUporabnika = 'Event manager'
      vm.animator = true;
      vm.normalUser = false;
      vm.showModalForAddEvent = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/eventModal/eventModal.view.html',
        controller: 'eventModal',
        controllerAs: 'vm',
        resolve: {
          detailsEvent: function() {
            return {
              title: "Add new event"
            };
          }
        },
        windowClass: 'show',
        backdropClass: 'show'
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.data.events.push(podatki);
        }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
    }
  }
};
  
  eventsCtrl.$inject = ['$uibModal', 'eventsData', 'authentication', '$location'];
  
  /* global angular */
  angular
    .module('straightas')
    .controller('eventsCtrl', eventsCtrl)
  
})();
# 14. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

BITBUCKET POVEZAVA:

https://bitbucket.org/an2006/tpo-lp4/commits/626549f

POVEZAVA NA PRODUKCIJO:

GLAVNA:

https://straightas-fri.herokuapp.com/

REZERVNA:

https://straightas-fri-tpo-private.herokuapp.com/

UPORABNIKI:

Student: email: student@email.si geslo: student

Upravljalec dogodkov: email: animator@email.si geslo: animator

Administrator: email: admin@email.si geslo: admin

DATOTEKA .env KI JO JE POTREBNO DODATI v primeru lokalne uporabe:

JWT_PASSWORD=toleNašeGeslo

GMAIL_CLIENTID=702869339258-v5p6sjiaqatv8nolv769pu86250l3nke.apps.googleusercontent.com

GMAIL_CLIENTSECRET=DaLANzP9U_c35jM4oDEjOMIM

GMAIL_REFRESHTOKEN=1/ABK95S0vKntVu-hZQxaN_H3Qn767ITBOF93H2IQgYdM

MLAB_URI=mongodb://user:userTpoStraightAs12345@ds151586.mlab.com:51586/straightas

var zahteva = require('supertest')
var app = require('../src/app.js')
var assert = require('assert');

describe('Calculator Tests', function() {
	describe('Absolute value tests', function() {
		it('returns 1 for -1', function(done) {
			assert.equal(Math.abs(-1), 1);
			done();
		});
		it('returns 2 for -2', function(done) {
			assert.equal(Math.abs(-2), 2);
			done();
		});
		it('returns 3 for -3', function(done) {
			assert.equal(Math.abs(-3), 3);
			done();
		});
		it('returns 4 for -4', function(done) {
			assert.equal(Math.abs(-4), 4);
			done();
		});
		it('returns 5 for -5', function(done) {
			assert.equal(Math.abs(-5), 5);
			done();
		});
		it('returns 6 for 6', function(done) {
			assert.equal(Math.abs(6), 6);
			done();
		});
	});
});
/*
describe('Začetna stran', function() {
  it ('Prikaži naslov "Prijava"', function(done) {
    zahteva(app).get('/login').expect(/<legend>Prijava<\/legend>/i, done);
  });
});
*/


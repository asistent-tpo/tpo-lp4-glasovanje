var request = require('supertest');
var chai = require('chai');
var app = require('../src/app.js');
var expect = chai.expect;
var should = chai.should;


  describe('/api/v1/event ANIMATOR', function() {
    
    it('should not get events because not log in', function(done) { 
      request(app).get('/api/v1/events')
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(401); 
          done(); 
        }); 
    });
    
    it('could not add 1 new event because not log in', function(done) { 
      request(app).post('/api/v1/events')
        .send({
          author: 'animator@email.si',
          name: 'Novoletna zabava',
          date: '2019-12-31T12:10:40.748Z',
          organizer: 'ŠUS',
          descriptionOfEvent: 'Praznovali bomo novo leto.'
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(401); 
          expect(res.body).to.be.an('Object');;
          done(); 
        }); 
    });
    
    var animator = {};
    before(loginUser(animator, 'animator@email.si', 'animator'));
    
    it('should get all 3 events', function(done) { 
      request(app).get('/api/v1/events')
        .set('Authorization', 'bearer ' + animator.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body).to.be.an('array');
          expect(res.body).to.have.lengthOf(3);
          done(); 
        }); 
    });
    
    var idNewEvent;
    
    it('should add 1 new event', function(done) { 
      request(app).post('/api/v1/events')
        .set('Authorization', 'bearer ' + animator.token)
        .send({
          author: 'animator@email.si',
          name: 'Novoletna zabava',
          date: '2019-12-31T12:10:40.748Z',
          organizer: 'ŠUS',
          descriptionOfEvent: 'Praznovali bomo novo leto.'
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(201); 
          expect(res.body).to.be.an('Object');
          expect(res.body).to.have.property('author', 'animator@email.si');
          expect(res.body).to.have.property('name', 'Novoletna zabava');
          expect(res.body).to.have.property('date', '2019-12-31T12:10:40.748Z');
          expect(res.body).to.have.property('organizer', 'ŠUS');
          expect(res.body).to.have.property('descriptionOfEvent', 'Praznovali bomo novo leto.');
          idNewEvent = res.body._id;
          done(); 
        }); 
    });
    
    it('should get all 4 events (3 old and 1 new)', function(done) { 
      request(app).get('/api/v1/events')
        .set('Authorization', 'bearer ' + animator.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body).to.be.an('array');
          expect(res.body).to.have.lengthOf(4);
          done(); 
        }); 
    });
    
    it('should delete new event)', function(done) { 
      request(app).delete('/api/v1/events/' + idNewEvent)
        .set('Authorization', 'bearer ' + animator.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(204); 
          done(); 
        }); 
    });
  });
    
  describe('/api/v1/event STUDENT', function() {  
    var student = {};
    before(loginUser(student, 'student@email.si', 'student'));
    
    it('should get all 3 events as Student', function(done) { 
      request(app).get('/api/v1/events')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body).to.be.an('array');
          expect(res.body).to.have.lengthOf(3);
          done(); 
        }); 
    });

});



function loginUser(auth, email, password) {
  return function(done) {
  request(app)
    .post('/api/v1/login')
    .send({
      email: email,
      password: password
    })
    .expect(200)
    .end(onResponse);
    function onResponse(err, res) {
      auth.token = res.body.token;
      return done();
    }
  };
}
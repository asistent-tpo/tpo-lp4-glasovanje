var request = require('supertest');
var chai = require('chai');
var app = require('../src/app.js');
var expect = chai.expect;
var should = chai.should;
var newUser = {};
var user = {};

  describe('New user (Student)', function() {
    
    it('should register new user', function(done) { 
      request(app).post('/api/v1/registration')
        .send({
          email: 'newUser@email.si',
          password: 'geslo'
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body).to.be.an('Object');
          expect(res.body).to.have.property('token');
          done(); 
        }); 
    });
    
    it('should login new user', function(done) { 
      request(app).post('/api/v1/login')
        .send({
          email: 'newUser@email.si',
          password: 'geslo'
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200); 
          expect(res.body).to.be.an('Object');
          expect(res.body).to.have.property('token');
          done(); 
        }); 
    });
  });
  
  describe('Delete new user (Student)', function() {
    
    var admin = {};
    before(loginUser(admin, 'admin@email.si', 'admin'));
    
    before(currentUser(newUser, 'newUser@email.si'));
    
    it('should get new user', function(done) { 
      request(app).get('/api/v1/users/' + newUser.id)
        .set('Authorization', 'bearer ' + admin.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.have.property('email', 'newUser@email.si');
          done(); 
        }); 
    });
    
    it('should delete new user', function(done) { 
      request(app).delete('/api/v1/users/' + newUser.id)
        .set('Authorization', 'bearer ' + admin.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(204); 
          done(); 
        }); 
    });
    
    it('should not get new user (it vas deleted)', function(done) { 
      request(app).get('/api/v1/users/' + newUser.id)
        .set('Authorization', 'bearer ' + admin.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(404);
          done(); 
        }); 
    });

});

describe('Operations on Calendar (Student)', function() {
  
    var student = {};
    before(loginUser(student, 'student@email.si', 'student'));
    before(currentUser(user, 'student@email.si'));
    
    it('should get all calendar Entries', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/todoList')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.todoList).to.be.an('array');
          expect(res.body.todoList).to.have.lengthOf(5);
          done(); 
        }); 
    });
    
    var idNewCalendarEntry;
    
    it('should add 1 new calendar Entry', function(done) { 
      request(app).post('/api/v1/users/' + user.id + '/todoList')
        .set('Authorization', 'bearer ' + student.token)
        .send({
          completed: false,
          dateCreated: "2019-06-06T22:31:08.997Z",
          dateCompleted: "2019-06-06T22:31:08.997Z",
          title: "Kolokvij PUI"
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(201); 
          expect(res.body).to.be.an('Object');
          expect(res.body).to.have.property('completed', false);
          expect(res.body).to.have.property('title', "Kolokvij PUI");
          idNewCalendarEntry = res.body._id;
          done(); 
        }); 
    });
    
    it('should get new calendar Entry', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/todoList/' + idNewCalendarEntry)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('Object');
          expect(res.body.todoListTask).to.have.property('completed', false);
          expect(res.body.todoListTask).to.have.property('title', "Kolokvij PUI");
          done(); 
        }); 
    });
    
    it('should get all calendar Entries (5 old and 1 new)', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/todoList')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.todoList).to.be.an('array');
          expect(res.body.todoList).to.have.lengthOf(6);
          done(); 
        }); 
    });
    
    
    it('should delete new calendar Entry)', function(done) { 
      request(app).delete('/api/v1/users/' + user.id + '/todoList/' + idNewCalendarEntry)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(204); 
          done(); 
        }); 
    });
    
    it('should not get new calendar Entry (it was deleted)', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/todoList/' + idNewCalendarEntry)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(404);
          done(); 
        }); 
    });
    
    it('should get all calendar Entries without deleted', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/todoList')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.todoList).to.be.an('array');
          expect(res.body.todoList).to.have.lengthOf(5);
          done(); 
        }); 
    });
    
});

describe('Operations on Timetable (Student)', function() {
  
    var student = {};
    before(loginUser(student, 'student@email.si', 'student'));
    before(currentUser(user, 'student@email.si'));
    
    it('should get all timetable Entries', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/timetableEntries')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.timetableEntries).to.be.an('array');
          expect(res.body.timetableEntries).to.have.lengthOf(5);
          done(); 
        }); 
    });
    
    var idNewTimetableEntry;
    
    it('should add 1 new timetable Entry', function(done) { 
      request(app).post('/api/v1/users/' + user.id + '/timetableEntries')
        .set('Authorization', 'bearer ' + student.token)
        .send({
          duration: 2,
          dateCreated: "2019-05-22T09:00:00.000Z",
          dateCompleted: "2019-05-26T23:02:41.134Z",
          title: "DS",
          description: "#ffcc99"
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(201); 
          expect(res.body).to.be.an('Object');
          expect(res.body).to.have.property('title', "DS");
          expect(res.body).to.have.property('description', "#ffcc99");
          expect(res.body).to.have.property('duration', 2);
          idNewTimetableEntry = res.body._id;
          done(); 
        }); 
    });
    
    it('should get new timetable Entry', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/timetableEntries/' + idNewTimetableEntry)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('Object');
          expect(res.body.timetableEntry).to.have.property('title', "DS");
          expect(res.body.timetableEntry).to.have.property('description', "#ffcc99");
          expect(res.body.timetableEntry).to.have.property('duration', 2);
          done(); 
        }); 
    });
    
    it('should get all timetable Entries (5 old and 1 new)', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/timetableEntries')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.timetableEntries).to.be.an('array');
          expect(res.body.timetableEntries).to.have.lengthOf(6);
          done(); 
        }); 
    });
    
    
    it('should delete new timetable Entry)', function(done) { 
      request(app).delete('/api/v1/users/' + user.id + '/timetableEntries/' + idNewTimetableEntry)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(204); 
          done(); 
        }); 
    });
    
    it('should not get new timetable Entry (it was deleted)', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/timetableEntries/' + idNewTimetableEntry)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(404);
          done(); 
        }); 
    });
    
    it('should get all timetable Entries without deleted', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/timetableEntries')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.timetableEntries).to.be.an('array');
          expect(res.body.timetableEntries).to.have.lengthOf(5);
          done(); 
        });  
    });
    
});


describe('Operations on TODO list (Student)', function() {
  
    var student = {};
    before(loginUser(student, 'student@email.si', 'student'));
    before(currentUser(user, 'student@email.si'));
    
    it('should get all todo lists', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/calendarEntries')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.calendarEntries).to.be.an('array');
          expect(res.body.calendarEntries).to.have.lengthOf(3);
          done(); 
        }); 
    });
    
    var idNewTodoList;
    
    it('should add 1 new todo list', function(done) { 
      request(app).post('/api/v1/users/' + user.id + '/calendarEntries')
        .set('Authorization', 'bearer ' + student.token)
        .send({
          dateCreated: "2019-05-29T22:28:35.599Z",
          dateToDo: "2019-05-29T22:28:35.599Z",
          dateCompleted: "2019-05-29T22:28:35.599Z",
          title: "Obišči babico",
          description: "Nesi ji knjige",
          duration: 0
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(201); 
          expect(res.body).to.be.an('Object');
          expect(res.body).to.have.property('title', "Obišči babico");
          expect(res.body).to.have.property('description', "Nesi ji knjige");
          idNewTodoList = res.body._id;
          done(); 
        }); 
    });
    
    it('should get new todo list', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/calendarEntries/' + idNewTodoList)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('Object');
          expect(res.body.calendarEntry).to.have.property('title', "Obišči babico");
          expect(res.body.calendarEntry).to.have.property('description', "Nesi ji knjige");
          done(); 
        }); 
    });
    
    it('should get all todo lists (3 old and 1 new)', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/calendarEntries')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.calendarEntries).to.be.an('array');
          expect(res.body.calendarEntries).to.have.lengthOf(4);
          done(); 
        }); 
    });
    
    
    it('should delete new todo list)', function(done) { 
      request(app).delete('/api/v1/users/' + user.id + '/calendarEntries/' + idNewTodoList)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(204); 
          done(); 
        }); 
    });
    
    it('should not get new todo list (it was deleted)', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/calendarEntries/' + idNewTodoList)
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(404);
          done(); 
        }); 
    });
    
    it('should get all todo lists without deleted', function(done) { 
      request(app).get('/api/v1/users/' + user.id + '/calendarEntries')
        .set('Authorization', 'bearer ' + student.token)
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(200);
          expect(res.body.calendarEntries).to.be.an('array');
          expect(res.body.calendarEntries).to.have.lengthOf(3);
          done(); 
        });  
    });
    
});



function loginUser(auth, email, password) {
  return function(done) {
  request(app)
    .post('/api/v1/login')
    .send({
      email: email,
      password: password
    })
    .expect(200)
    .end(onResponse);
    function onResponse(err, res) {
      auth.token = res.body.token;
      return done();
    }
  };
}

function currentUser(user, email) {
  return function(done) {
  request(app)
    .get('/api/v1/userEmail/' + email)
    .expect(200)
    .end(onResponse);
    function onResponse(err, res) {
      user.id = res.body._id;
      return done();
    }
  };
}
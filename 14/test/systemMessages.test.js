var request = require('supertest');
var chai = require('chai');
var app = require('../src/app.js');
var expect = chai.expect;
var should = chai.should;


  describe('/api/v1/systemMessage ADMIN', function() {
    
    var admin = {};
    before(loginUser(admin, 'admin@email.si', 'admin'));
    
    it('should send 1 new system message to student, animator, admin', function(done) { 
      request(app).post('/api/v1/systemMessage')
        .set('Authorization', 'bearer ' + admin.token)
        .send({
          emails: 'student@email.si, animator@email.si, admin@email.si',
          text: 'Testno sistemsko obvestilo'
        })
        .end(function(err, res) { 
          expect(res.statusCode).to.equal(201);
          done(); 
        }); 
    });

});



function loginUser(auth, email, password) {
  return function(done) {
  request(app)
    .post('/api/v1/login')
    .send({
      email: email,
      password: password
    })
    .expect(200)
    .end(onResponse);
    function onResponse(err, res) {
      auth.token = res.body.token;
      return done();
    }
  };
}
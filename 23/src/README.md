# README #

## Aplikacija NoClue ##

Glavna funkcionalnost spletne strani NoClue je iskanje dogodkov. Dogodke bojo ustvarjali uporabniki. Na spletno stran se je potrebno registrirati.
Uporabnik bo imel možnost ustvariti, urejati in brisati dogodke. Dogodke bo mogoče iskati in filtrirati po različnih lastnostih, tako da bo lahko 
uporabnik enostavno našel to kar ga zanima. Aplikacija uporablja Google Maps API, ki je implementiran tako, da prikazuje lokacijo vsakega eventa.
Poleg tega, je se je mozno tudi na dogodek priajviti(pokazati zanimanje) preko gumba "interested" in seveda tudi odjaviti. Viden je tudi rating dogokda, implementiran z sistemom zvezdic.

### Zaslonske maske ###
#### Prijava ####
Maska je dostopna na root naslovu torej "https://sp-noclue.herokuapp.com/".  

Na prijavni strani ima uporabnik možnost vnosa emaila in gesla. Ob pritisku na gumb za prijavo, uporabnik vstopi v aplikacijo, ali pa ga aplikacija opozori, da je vnesel napačne podake.
Uporabnik ima tudi možnost registracije.

* Dovoljene vrednosti za posamezna polja:
	* Email address: Dovoljene vrednosti v obliki {besedilo}@{besedilo}.{nekaj}
	* Password: dovoljene so vse vrednosti


#### Eventi ####
Maska je dostopna na root naslovu torej "https://sp-noclue.herokuapp.com/events".  

Na tej povezavi uporabnik lahko išče, izbira in sortira evente.

#### Registracija ####
Maska je dostopna naslovu "https://sp-noclue.herokuapp.com/register".  

* Uporabnik mora vnesti svoje osebne podatke kot so:
	* ime
	* priimek
	* email
	* geslo
	* datum rojstva
	* etc.
	
Če so podatki ustrezni, se ustvari nov uporabnik.

#### Profil ####
Maska je dostopna na naslovu "https://sp-noclue.herokuapp.com/profile".

Na profilu uporabnik lahko poljubno kreira nove evente hkrati pa lahko tudi ureja in ali briše evente.

#### Dodajanje eventov ####
Maska je dostopna na naslovu "https://sp-noclue.herokuapp.com/events/create".  

Uporabnik na tem naslovu ustvari nov event. 

#### Informacije o eventu ####
Maska je dostopna na naslovu https://sp-noclue.herokuapp.com/events/{id}.  

Primer: "https://sp-noclue.herokuapp.com/events/5c0dc9f2616a9f0015f39f70"

#### Urejanje eventa ####
Maska je dostopna na naslovu https://sp-noclue.herokuapp.com/events/{id}/edit.

Primer: "https://sp-noclue.herokuapp.com/events/5c0dc9f2616a9f0015f39f70/edit"

Pri urejanju eventa se pridobijo in pokažejo podatki eventa katere lahko poljubno urejamo.

#### Seznam podprtih naprav ####
* Iphone 6
* Ipad
* Desktop

#### Seznam dovoljenih vnosov za vsa uporabniška vnosna polja ####
* sign up:
	* Name: string
	* Surname: string
	* email: email
	* password: string
	* day: int
	* year: int
	* phone number: numeric value
-------------------------------
* sign in:
	* email addres: email
	* password: string
-------------------------------
* search:
	* string, key words
-------------------------------
* events/create:
	* name of event: string
	* loaction: string (name of a place)
	* date: date
	* description: string

### Čas nalaganja strani ###
Aplikacija NoClue je bila testirana glede hitrosti nalaganja vseh njenih spletnih mest. Meritve časa nalaganj, smo izvajali s pomočjo Dootcom-tools.
Hitrost nalaganja smo preverili na dveh različnih brskalnikih in sicer, na Google Chrome ter na Mozila Firefox. Čase nalaganja smo lahko preverili na 6ih različnih lokacijah.
Lokacije so naslednje: London, Amsterdam, Madrid, Pariz, Frankfurt in Varšava. Nalaganja strani, ki so potekala na brskalniku Google Chrome, smo še dodatno analizirali
s pomočjo uporabo pogleda Inspect ter pomočkov, ki nam jih nudi.

#### Google Chrome - časi nalaganja: ####
* Prijava:
	* AVG 1ST VISIT: 2.3 sec
	* AVG 2ND VISIT: 1.9 sec
* Registracija:
	* AVG 1ST VISIT: 3.4 sec
	* AVG 2ND VISIT: 2.7 sec
* Profil:
	* AVG 1ST VISIT: 2.5 sec
	* AVG 2ND VISIT: 2.2 sec
* Eventi:
	* AVG 1ST VISIT: 2.4 sec
	* AVG 2ND VISIT: 2.3 sec
* Dodajanje eventov:
	* AVG 1ST VISIT: 814 ms
	* AVG 2ND VISIT: 506 ms
* Informacije o eventu:
	* AVG 1ST VISIT: 3.5 sec
	* AVG 2ND VISIT: 3.1 sec
* Urejanje eventa:
	* AVG 1ST VISIT: 609 ms
	* AVG 2ND VISIT: 704 ms
#### Firefox - časi nalaganja: ####
* Prijava:
	* AVG 1ST VISIT: 2.0 sec
	* AVG 2ND VISIT: 1.5 sec
* Registracija:
	* AVG 1ST VISIT: 3.4 sec
	* AVG 2ND VISIT: 1.6 sec
* Profil:
	* AVG 1ST VISIT: 2.5 sec
	* AVG 2ND VISIT: 1.8 sec
* Eventi:
	* AVG 1ST VISIT: 2.9 sec
	* AVG 2ND VISIT: 1.8 sec
* Dodajanje eventov:
	* AVG 1ST VISIT: 587 ms
	* AVG 2ND VISIT: 669 ms
* Informacije o eventu:
	* AVG 1ST VISIT: 3.3 sec
	* AVG 2ND VISIT: 2.7 sec
* Urejanje eventa:
	* AVG 1ST VISIT: 682 ms
	* AVG 2ND VISIT: 907 ms
#### Del spletnega mesta z najpošasnejšim nalaganjem ####
Najpočasnejša komponenta na Google Chrome je bila stran o Informacijah o eventu - 3.5 sec
Najpošasnejša komponenta na Mozila Firefox je bila Registracija - 3.4 sec

##### ugotovitve #####
Vemo da hitrost nalaganja zelo vpliva na uporabniško izkušnjo in ravno zaradi tega razloga, jo je potrebno vedno opazovati ter optimizirati, če je le možno. 
Pri nalaganju aplikacije NoClue, smo ob podrobnejšem opazovanju prišli do ugotovitev, da nam pri nalaganju vzame največ časa nalaganje slikovnega materiala, saj so slike visoke rezolucije.
Na mestih, kjer slik ni bilo prisotnih, se opazi takojšnje izboljšanje. Npr. nalaganje strani "Dodajanje eventov", ki ne vsebuje slik, je mnogo boljši kot čas nalaganja "Registracije", ki vsebuje v
ozadju sliko z zelo veliko resolucijo. Poleg slik, pa sta v razponu od 100 do 400 ms potrebovali tudi datoteki jquery.min.js in bootstrap.min.js, kljub temu da sta minimizirani. 
Potrebno je omeniti, da je čas nalaganja strani odvisen tudi od strežnika na katerem spletna stran gostuje.

### Apache JMeter ###
Apache JMeter test nam pomaga preveriti, kako dobro naša aplokacija prenaša veliko zahtev.

Testa kdaj aplikacija "počepne" oziroma zaradi prevelikaga števila zahtev se neha odzivati ni bilo mogoče izvesti, saj se je zaradi prevelikega števila podatkov prej sesu(crashal ) JMeter kot pa aplikacija.

Zato smo testirali Aplikacijo na pričakovanih realnih primerih. Za našo aplikacijo ki pomaga iskati dogodke, smo predividevali, da bi povprečni user na 10 sekund (kar je ekstremno prehitro, ampak vseeeno)
zahteval nove podatke iz baze, pri tem pa bi si vzel 10 sekund da prelista evente in potem se odloči zamenjati sortiranje eventov ali pa lokacijo.

Za tesiranje smo dali 10000 uporabnikov ki bo imelo aktivne povezave(threde) na server in bodo izvajali zahteve.

To v povprečju pomeni 1000 uporabnikov na sekundo, ampak zaradi toliko večjega števila aktivnih uporabnikov server dela počasneje saj ima odprtih še 10 krat več thredov.

Če izvedemo test na 1000 uporabnikov na sekundo so rezultati veliko boljši saj strežnik ni obremenjen s toliko thredi.

* Kratka obrazlaga posameznih vrednosti:
	* latest sample - to je vzorec v milisekundah. To je čas odgovora za zadnji zahtevan URL in izvedbo skripte.
	* Throughput - je število zahtev pa enoto(v našem primeru minuta), ki poslane našemu strežniku med testiranjem.
	* average - povprečen čas odgovora vseh vzorcev(poslanih zahtev).
	* deviation- odstopanje od povprečja.

* Za test 10000 aktivnih uporabnikov na 10 sekund (to ponovimo 5 krat za večje število vzorcev)so rezultati sledeči:
	* število vzorcev : 50 000.
	* Throughput: 124 890 721 / minuto.
	* latest sample: 15 ms.
	* average: 2053 ms.
	* deviation: 690 ms.
	
Glede na prodobljene rezultate moramo upoštevati, da so rezultati boljši, saj testoramo na lokalnem računalniku,
kar pomeni da nimamo prenosa po internetu in zato tudi ne upoštevamo latence interneta, kar pohitri rezultate.

* Test je bil izveden na napravi lenovo g510 laptop:
	* Operating System: Windows 10
	* Intel® Core™ i7-4702MQ
		* Processor Base Frequency: 2.20 GHz
		* Max Turbo Frequency: 3.20 GHz
		* Number of Cores: 4
		* Number of Threads: 8
	* Ram: 8Gb
	* SSD 850 EVO


### Selenium test ###

Aplikacija je bila testirana z ogrodjem Protractor, ki je zgrajeno na Selenium WebDriverJS in je namenjeno testiranju AngularJS aplikacij.

Ogrodje namestimo z ukazom npm install -g protractor.  
Selenium Server pridobimo z webdriver-manager update ter poženemo z ukazom webdriver-manager start.

Za testiranje sta bili uporabljeni datoteki conf.js in spec.js.  
V conf.js navedemo naslov Selenium strežnika, brskalnik in ime datoteke s testi. Testiranje se požene z ukazom protractor spec.js

### Spremljanje uporabe aplikacije ###

Aplikacija za sledenje uporablja Google Data Analytics. 


#### Heroku ####
"https://sp-noclue.herokuapp.com"

#### Navodila za namestitev ####
https://bitbucket.org/fri_sp/sp_projekt/commits/d05ba73

cd d05ba73/

npm install

npm install -g nodemon

#### Razlike med brskalniki ####
Zaenkrat sta podprta samo Chrome in Opera

var mongoose = require('mongoose');
var Urnik = mongoose.model('Urnik');
var Todo = mongoose.model('Todo');
var Koledar = mongoose.model('Koledar');


var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};


module.exports.koledar = function(zahteva, odgovor) {
  var userId = zahteva.payload._id;
  console.log("pride: " + userId );

   Koledar
    .find({ 'user_id': userId })
    .exec(function(napaka, urnik) {
      if(napaka) {
        console.log(JSON.stringify(napaka, null, 4));
        vrniJsonOdgovor(odgovor, 500, napaka);
      } else {
         vrniJsonOdgovor(odgovor, 200, urnik);
         console.log("dela api");
      }
    });
};

module.exports.createDogodekKoledar = function(zahteva, odgovor) {
  console.log(JSON.stringify(zahteva.body, null, 4));
   var userId = zahteva.payload._id;
  console.log("pride: " + userId );
  
   Koledar.create({
      _id: mongoose.Types.ObjectId(),
      //userId: zahteva.body.userId,
      date: zahteva.body.date,
      //time: zahteva.body.time,
      //duration: zahteva.body.duration,
      title: zahteva.body.title,
      description: zahteva.body.description,
      user_id: userId,

    }, function(napaka, koledar) {
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        vrniJsonOdgovor(odgovor, 201, koledar);
      }
    });
};

module.exports.deleteKoledar = function(zahteva, odgovor) {
  var date = zahteva.body.date;
  var title = zahteva.body.title;
  if (!date || !title) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  }
  Koledar
  .findOneAndRemove({'date': date, 'title': title}, 
    function(napaka) {
      if (napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
      }
      else {
        vrniJsonOdgovor(odgovor, 204, "Event in calendar deleted");
      }
  });
};




module.exports.urnik = function(zahteva, odgovor) {
   var userId = zahteva.payload._id;
  console.log("pride: " + userId );
  
   Urnik
    .find({ 'user_id': userId })
    .exec(function(napaka, urnik) {
      if(napaka) {
        console.log(JSON.stringify(napaka, null, 4));
        vrniJsonOdgovor(odgovor, 500, napaka);
      } else {
         vrniJsonOdgovor(odgovor, 200, urnik);
      }
    });
};

module.exports.createPredmet = function(zahteva, odgovor) {
   Urnik.create({
      _id: mongoose.Types.ObjectId(),
      title: zahteva.body.title,
      day: zahteva.body.day,
      hourStart: zahteva.body.hourStart,
      minStart: zahteva.body.minStart,
      hourEnd: zahteva.body.hourEnd,
      minEnd: zahteva.body.minEnd,
      color: zahteva.body.color,
      user_id: zahteva.body.userId,
    }, function(napaka, urnik) {
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        vrniJsonOdgovor(odgovor, 201, urnik);
      }
    });
};

module.exports.deletePredmet = function(zahteva, odgovor) {
  var idPredmeta = zahteva.params.idPredmeta;
  if (idPredmeta) {
    Urnik
    .findByIdAndRemove(idPredmeta)
    .exec(
      function(napaka, event) {
        if (napaka) {
          return  vrniJsonOdgovor(odgovor, 404, napaka);
        }
         return vrniJsonOdgovor(odgovor, 204, null);
      }
    );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem predmeta, idPredmeta je obvezen parameter."
    });
  }
};


module.exports.updatePredmet = function(zahteva,odgovor){
  console.log("izpis parametrov")
  console.log(zahteva.body,4,null)
  if (!zahteva.params.idPredmeta) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem predmeta, idPredmeta je obvezen parameter"
    });
    return;
  }
   console.log("iscem todo")
  Urnik
  .findById(zahteva.params.idPredmeta)
   .exec(
     function(napaka, predmet) {
       console.log("nasel pravi predmet")
       if (!predmet) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem predmeta."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        
        if(zahteva.body.title !== null) {
          predmet.title = zahteva.body.title;
           console.log("nov description  = " + predmet.description);
        }
        if(zahteva.body.day !== null) {
          predmet.day = zahteva.body.day;
        }
        if(zahteva.body.hourStart !== null) {
          predmet.hourStart = zahteva.body.hourStart;
        } 
        if(zahteva.body.minStart !== null) {
          predmet.minStart = zahteva.body.minStart;
        } 
        if(zahteva.body.hourEnd !== null) {
          predmet.hourEnd = zahteva.body.hourEnd;
        } 
        if(zahteva.body.minEnd !== null) {
          predmet.minEnd = zahteva.body.minEnd;
        }
        if(zahteva.body.hourEnd !== null) {
          predmet.color = zahteva.body.color;
        }
       
       predmet.save(function(napaka, predmet) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, predmet);
          }
        });
       
       
     }
     );
  
  
};





//------------------------------
module.exports.todo = function(zahteva, odgovor) {
  var userId = zahteva.payload._id;
  console.log("pride: " + userId );
  Todo
  .find({ 'user_id': userId })
  .exec(function(napaka, todo) {
    if(napaka) {
      vrniJsonOdgovor(odgovor, 500, napaka);
    } else {
       vrniJsonOdgovor(odgovor, 200, todo);
    }
  });
};

module.exports.createTodo = function(zahteva, odgovor) {
  console.log("podatki so prispeli");
  console.log("desc = " + zahteva.body.description);
  console.log("userid = " + zahteva.body.userId);
   Todo.create({
      _id: mongoose.Types.ObjectId(),
      description: zahteva.body.description,
      user_id: zahteva.body.userId,
    }, function(napaka, todo) {
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        console.log(JSON.stringify(todo, null, 4));
        vrniJsonOdgovor(odgovor, 201, todo);
      }
    });
};

module.exports.deleteTodo = function(zahteva, odgovor) {
  var idTodo = zahteva.params.idTodo;
  if (idTodo) {
    Todo
    .findByIdAndRemove(idTodo)
    .exec(
      function(napaka, event) {
        if (napaka) {
          return  vrniJsonOdgovor(odgovor, 404, napaka);
        }
         return vrniJsonOdgovor(odgovor, 204, null);
      }
    );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem predmeta, idTodo je obvezen parameter."
    });
  }
};

module.exports.updateTodo = function(zahteva,odgovor){
  console.log("izpis parametrov")
  console.log(zahteva.params,4,null)
  if (!zahteva.params.idTodo) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem todo-ja, idTodo je obvezen parameter"
    });
    return;
  }
   console.log("iscem todo")
  Todo
  .findById(zahteva.params.idTodo)
   .exec(
     function(napaka, todo) {
       console.log("nasel pravi todo")
       if (!todo) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem eventa."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        
        if(zahteva.body.description !== null) {
          todo.description = zahteva.body.description;
           console.log("nov description  = " + todo.description);
        }  
       
       todo.save(function(napaka, todo) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, todo);
          }
        });
       
       
     }
     );
  
  
};


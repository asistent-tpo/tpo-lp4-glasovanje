
var mongoose = require('mongoose');
var Event = mongoose.model('Event');
var User = mongoose.model('User');
var searchable = require('mongoose-searchable');


var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

/*
module.exports.eventsById = function(zahteva, odgovor) {
    Event
    .findById(zahteva.params.idEvent)
    .exec(function(napaka, event) {
      vrniJsonOdgovor(odgovor, 200, event);
    });
};
*/
//Latest
module.exports.eventsList = function(zahteva, odgovor) {
  // collection.find().sort({datefield: -1}, function(err, cursor){...});
  //.limit(1)
  //.skip(1)
  //console.log("SORT=" + zahteva.query.sort);
  
    Event
    .find()
    //.limit(1)
    //.skip(offset)
    .sort({"date": 1})
    .populate('img', 'event_image')
    .exec(function(napaka, event) {
      if(napaka) {
        console.log(JSON.stringify(napaka, null, 4));
        vrniJsonOdgovor(odgovor, 500, napaka);
        
      } else {
         vrniJsonOdgovor(odgovor, 200, event);
      }
    });

  
  

};

module.exports.myEvents = function(zahteva, odgovor) {
  var email = zahteva.payload.email;
  console.log(email);
  //console.log("pride: " +userId );
  Event
    .find({ 'createdBy': email })
    .exec(function(napaka, event) {
      if(napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
      } else {
         vrniJsonOdgovor(odgovor, 200, event);
      }
  });
        
};




module.exports.eventsCreate = function(zahteva, odgovor) {
  //console.log(JSON.stringify(zahteva.payload, null, 4));
  
    Event.create({
      createdBy: zahteva.payload.email,
      organizer: zahteva.body.organizer,
      _id: mongoose.Types.ObjectId(),
      title: zahteva.body.title,
      date: zahteva.body.date,
      event_image: zahteva.body.event_image,
      description: zahteva.body.description,
    }, function(napaka, event) {
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        User
        .findById(zahteva.payload._id)
        .select()
        .exec(
          function(napaka, user) {
            if (!user) {
              //ne najde uporabnika
              return deleteEventById(event._id, odgovor, 404, {
                sporocilo: "Napaka pri kreiranju eventa. Ne najdem uporabnika"
              });
            } else if (napaka) {
              return deleteEventById(event._id, odgovor, 500, napaka);
            }
            user.eventsCreated.push(event._id);
            user.save(function(napaka, user) {
              if (napaka) {
                return deleteEventById(event._id, odgovor, 400, napaka);
              } else {
                vrniJsonOdgovor(odgovor, 201, event);
              }
            });
          }
        );
        
      }
    });
 // });
};

module.exports.deleteEvents = function(zahteva, odgovor) {
  Event.
  deleteMany({}, function(napaka, event) {
    vrniJsonOdgovor(odgovor, 204, event);
  });
};


module.exports.eventsById = function(zahteva, odgovor) {
    Event
    .findById(zahteva.params.idEvent)
    .exec(function(napaka, event) {
      vrniJsonOdgovor(odgovor, 200, event);
    });
};

module.exports.eventInterested = function(zahteva, odgovor) {
  var idEventa = zahteva.params.idEvent;
  var isInterested = zahteva.query.isInterested;
  //console.log(JSON.stringify(zahteva.payload, null, 4));
  
  
  if (isInterested === "true") {
    Event
    .findById(idEventa)
    .select()
    .exec(
      function(napaka, event) {
        if (!event) {
          //ne najde eventa
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": 
              "Ne najdem eventa."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        var interestedUser = {
          _id: zahteva.payload._id,
          email: zahteva.payload.email
        };
        
        event.interested.push(interestedUser);
        event.save(function(napaka, user) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, event);
          }
        });
      }
    );
    
  } else if (isInterested === "false"){
    var interestedUser = {
      _id: zahteva.payload._id,
      email: zahteva.payload.email
    };
    
    Event
    .update( 
      {'_id': idEventa}, { $pull: {'interested': interestedUser } },
      function(napaka, data){
        if(napaka) {
          console.log(napaka);
          vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
          vrniJsonOdgovor(odgovor, 200, data);
        }
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporocilo": "Neznan ukaz"
  });
  }
};



module.exports.eventsUpdate = function(zahteva, odgovor) {
 if (!zahteva.params.idEvent) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem eventa, idEvent je obvezen parameter"
    });
    return;
  }
  Event
    .findById(zahteva.params.idEvent)
    .select('-interested')                                      //kaj je to
    .exec(
      function(napaka, event) {
        if (!event) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem eventa."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
         //console.log(JSON.stringify(zahteva.body, null, 4));
         
        if(zahteva.body.title !== null) 
          event.title = zahteva.body.title;
        if(zahteva.body.location !== null) 
          event.location = zahteva.body.location;
        if(zahteva.body.date !== null) 
          event.date = zahteva.body.date;
        if(zahteva.body.entry !== null) 
          event.entry = zahteva.body.entry;
        if(zahteva.body.event_image !== null) 
          event.event_image = zahteva.body.event_image;
        if(zahteva.body.description !== null) 
          event.description = zahteva.body.description;
        //za testiranje
        if(zahteva.body.interested !== null) 
          event.interested = zahteva.body.interested;
        if(zahteva.body.is_interested !== null) 
          event.is_interested = zahteva.body.is_interested;
        
        event.save(function(napaka, event) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, event);
          }
        });
      }
    );
};


module.exports.eventsDelete = function(zahteva, odgovor) {
  var idEventa = zahteva.params.idEvent;
  if (idEventa) {
    deleteEventById(idEventa, odgovor, 204, null);
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem eventa, idEventa je obvezen parameter."
    });
  }
};

var deleteEventById = function(idEvent, odgovor, napakaSt, sporocilo) {
  Event
    .findByIdAndRemove(idEvent)
    .exec(
      function(napaka, event) {
        if (napaka) {
          return  vrniJsonOdgovor(odgovor, 404, napaka);
        }
         return vrniJsonOdgovor(odgovor, napakaSt, sporocilo);
      }
    );
};

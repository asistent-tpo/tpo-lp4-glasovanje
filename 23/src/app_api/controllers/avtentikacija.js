var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.spremembaGesla = function(zahteva, odgovor){
  if (!zahteva.body.email || !zahteva.body.password) {
    console.log("NI VSEH PODATKOV");
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  }

  passport.authenticate('local', function(napaka, uporabnik, podatki) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
      return;
    }
    if (uporabnik) {
      
        User.findOne(
        {
          email: zahteva.body.email
        },
        function(napaka, uporabnik) {
        if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        } 
        if (uporabnik) {
          uporabnik.nastaviGeslo(zahteva.body.newPassword);
  
          uporabnik.save(function(napaka, user) {
            if (napaka) {
              console.log(JSON.stringify(napaka, null, 4));
              vrniJsonOdgovor(odgovor, 400, napaka);
            } else {
              vrniJsonOdgovor(odgovor, 200, {
               "zeton": user.generirajJwt()
             });
            }
          });
        } else {
            vrniJsonOdgovor(odgovor, 400, {
              "sporočilo": "Uporabnik NE obstaja" 
            });
          }
        });    
        
    } else {
      console.log(JSON.stringify(podatki, null, 4));
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
};


module.exports.registracija = function(zahteva, odgovor) {
  console.log("avtentikacija registracija");
  console.log(JSON.stringify(zahteva.body, null, 4));
  if (!zahteva.body.email || !zahteva.body.password || !zahteva.body.userType) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  }
  
  User.findOne(
      {
        email: zahteva.body.email
      },
      function(napaka, uporabnik) {
        if (napaka) {
          console.log(JSON.stringify(napaka, null, 4));
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        } 
        if (uporabnik) {
          vrniJsonOdgovor(odgovor, 400, {
            "sporočilo": "Uporabnik že obstaja" 
          });
        }
        else {
          var user = new User(); 
          user.email = zahteva.body.email;
          user.tipUporabnika = zahteva.body.userType;
          user.nastaviGeslo(zahteva.body.password);
          //console.log(JSON.stringify(user, null, 4));
          
          user.save(function(napaka) {
           if (napaka) {
              console.log(JSON.stringify(napaka, null, 4));
             vrniJsonOdgovor(odgovor, 500, napaka);
           } else {
             vrniJsonOdgovor(odgovor, 200, {
               "zeton": user.generirajJwt()
             });
           }
          });
        }
      });
};


module.exports.prijava = function(zahteva, odgovor) {
  console.log("vprijavi");
  console.log(zahteva.body.email);
  console.log(zahteva.body.password);
  console.log(zahteva.body.tipUporabnika);
  if (!zahteva.body.email || !zahteva.body.password || !zahteva.body.tipUporabnika) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  }
  console.log("pred avtentikacijo");
  passport.authenticate('local', function(napaka, uporabnik, podatki) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
      return;
    }
    if (uporabnik) {
      
      User.findOne(
        {
          email: zahteva.body.email
        },
        function(napaka, uporabnik) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
            return;
          } 
          
          console.log("preverjam tip uporabnika");
          if (!(uporabnik.tipUporabnika == zahteva.body.tipUporabnika)) {
            console.log("napacen tip uporabnika");
              vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Napačen tip uporabnika" });
              return;
  
          } else {
            console.log("tip uporabnika OK");
            vrniJsonOdgovor(odgovor, 200, {
              "zeton": uporabnik.generirajJwt()
            });
          }
        });    
         
    } else {
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
};


module.exports.deleteUser = function(zahteva, odgovor) {
  if (!zahteva.body.email) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  }
  
  User
    .findOneAndRemove({'email' : zahteva.body.email}, 
      function(napaka, obj) {
        if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
        }
        else {
          vrniJsonOdgovor(odgovor, 204, "User deleted");
        }
    });
};
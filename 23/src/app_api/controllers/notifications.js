
var mongoose = require('mongoose');
var Notification = mongoose.model('Notification');


var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};


//Latest
module.exports.notificationList = function(zahteva, odgovor) {
    Notification
    .find()
    //.limit(1)
    //.skip(offset)
    .sort({"date": -1})
    .exec(function(napaka, event) {
      if(napaka) {
        console.log(JSON.stringify(napaka, null, 4));
        vrniJsonOdgovor(odgovor, 500, napaka);
        
      } else {
         vrniJsonOdgovor(odgovor, 200, event);
      }
    });

  
  

};
/*
module.exports.myEvents = function(zahteva, odgovor) {
  var email = zahteva.payload.email;
  console.log(email);
  //console.log("pride: " +userId );
  Event
    .find({ 'createdBy': email })
    .exec(function(napaka, event) {
      if(napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
      } else {
         vrniJsonOdgovor(odgovor, 200, event);
      }
  });
        
};
*/




module.exports.notificationCreate = function(zahteva, odgovor) {
  
  console.log(JSON.stringify(zahteva.payload, null, 4));
  
    Notification.create({
      _id: mongoose.Types.ObjectId(),
      title: zahteva.body.title,
      date: zahteva.body.date,
      description: zahteva.body.description,
    }, function(napaka, notification) {
      if (napaka) {
        console.log(napaka);
        vrniJsonOdgovor(odgovor, 400, napaka);
      } else {
        vrniJsonOdgovor(odgovor, 201, notification);
      }
    });
 // });
};

module.exports.deleteNotifications = function(zahteva, odgovor) {
  Notification.
  deleteMany({}, function(napaka, event) {
    vrniJsonOdgovor(odgovor, 204, event);
  });
};


module.exports.notificationsById = function(zahteva, odgovor) {
    Notification
    .findById(zahteva.params.idEvent)
    .exec(function(napaka, event) {
      vrniJsonOdgovor(odgovor, 200, event);
    });
};


module.exports.notificationsDelete = function(zahteva, odgovor) {
  var idEventa = zahteva.params.idEvent;
  if (idEventa) {
    deleteNotificationById(idEventa, odgovor, 204, null);
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem eventa, idEventa je obvezen parameter."
    });
  }
};

var deleteNotificationById = function(idEvent, odgovor, napakaSt, sporocilo) {
  Notification
    .findByIdAndRemove(idEvent)
    .exec(
      function(napaka, event) {
        if (napaka) {
          return  vrniJsonOdgovor(odgovor, 404, napaka);
        }
         return vrniJsonOdgovor(odgovor, napakaSt, sporocilo);
      }
    );
};

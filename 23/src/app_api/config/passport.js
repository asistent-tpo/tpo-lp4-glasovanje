var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LokalnaStrategija({
    usernameField: 'email',
    passwordField: 'password'
  }, 
  function(uporabniskoIme, geslo, koncano) {
    console.log("yuhu");
    console.log(uporabniskoIme);
    console.log(geslo);
    //console.log(tipUporabnika)
    
    User.findOne(
      {
        email: uporabniskoIme
      },
      function(napaka, uporabnik) {
        console.log("uporabnik najden");
        if (napaka)
          return koncano(napaka);
          
          console.log("preverjam username");
        if (!uporabnik) {
          return koncano(null, false, {
            sporocilo: 'Napačno uporabniško ime'
          });
        }
        console.log("username je kul");
        console.log("preverjam geslo")
        if (!uporabnik.preveriGeslo(geslo)) {
          return koncano(null, false, {
            sporocilo: 'Napačno geslo'
          });
        }
        console.log("geslo je kul");
        /*
        console.log("preverjam tip uporabnika");
        if(!uporabnik.preveriTipUporabnika(tipUporabnika)){
          return koncano(null, false, {
            sporocilo: 'Napačen tip uporabnika'
          });
        }
        */
        console.log("vracanje avtentikacije")
        return koncano(null, uporabnik);
      }
    );
  }
));
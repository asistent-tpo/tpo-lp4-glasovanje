var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notificationSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    date: {type: Date, required: true},
    title: {type: String, required: true},
    description: {type: String },
});


mongoose.model('Notification', notificationSchema, 'Notifications');

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var koledarSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    //userId: {type: String, required: true},
    date: {type: String, required: true},
    time: {type: String},
    duration: {type: Number},
    title: {type: String, required: true},
    description: {type: String },
    user_id: Schema.Types.ObjectId,
});


mongoose.model('Koledar', koledarSchema);

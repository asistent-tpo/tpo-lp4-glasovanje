var mongoose = require('mongoose');
const uniqueArrayPlugin = require('mongoose-unique-array');
var Schema = mongoose.Schema;

var organizerSchema = new mongoose.Schema({
    //_id: Schema.Types.ObjectId,
    name:{type: String, required: true},
    img: {type: String, "default": "/images/nat-9.jpg"},
});

var interestedSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    email:{type: String},
});
//interestedSchema.plugin(uniqueArrayPlugin);

var eventsSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    organizer: {type: String, required: true},
    createdBy: {type: String, required: true},
    title: {type: String, required: true},
    date: {type: Date, required: true},
    //event_image: {type: mongoose.Schema.Types.ObjectId, ref: "Image"},
    event_image: {type: String, "default": "/images/tusev.jpeg"},
    description: {type: String },
});

eventsSchema.plugin(uniqueArrayPlugin);

//eventsSchema.index( { title: "loc" } );
eventsSchema.index({'$**': 'text'});
//eventsSchema.index({'title': 'loc'});

mongoose.model('Event', eventsSchema, 'Events');

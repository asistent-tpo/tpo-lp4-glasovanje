var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var todoSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    description: {type: String },
    user_id: Schema.Types.ObjectId
});

mongoose.model('Todo', todoSchema, 'Todos');

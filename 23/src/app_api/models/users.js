var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var jwt = require('jsonwebtoken');


var usersSchema = new mongoose.Schema({
    //_id: Schema.Types.ObjectId,
    tipUporabnika: {type: String, required: true},
    
    img: {type: String, "default": "/images/nat-9.jpg"},
    email: {type: String, unique: true, required: true},
    eventsCreated: [{type: mongoose.Schema.Types.ObjectId, ref: "Event"}],
    zgoscenaVrednost: String,
    nakljucnaVrednost: String
});

usersSchema.methods.nastaviGeslo = function(geslo) {
  this.nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
  this.zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

usersSchema.methods.preveriGeslo = function(geslo) {
  var zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
  return this.zgoscenaVrednost == zgoscenaVrednost;
};
usersSchema.methods.preveriTipUporabnika = function(tipUporabnika) {
  return this.tipUporabnika == tipUporabnika;
};
usersSchema.methods.generirajJwt = function() {
  var datumPoteka = new Date();
  datumPoteka.setDate(datumPoteka.getDate() + 7);
  
  return jwt.sign({
    _id: this._id,
    email: this.email,
    img: this.img,
    tipUporabnika: this.tipUporabnika,
    datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
  }, process.env.JWT_GESLO);
};


mongoose.model('User', usersSchema, 'Users');
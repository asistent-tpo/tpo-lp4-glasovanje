var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var urnikSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    title: {type: String },
    day: {type: Number},
    hourStart: {type: Number},
    minStart: {type: Number},
    hourEnd: {type: Number},
    minEnd: {type: Number},
    color: {type: String },
    user_id: Schema.Types.ObjectId,
});

mongoose.model('Urnik', urnikSchema);

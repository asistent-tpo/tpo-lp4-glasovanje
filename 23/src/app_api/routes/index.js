var express = require('express');
var router = express.Router();
var ctrlEvents = require('../controllers/events');
var ctrlAvtentikacija = require('../controllers/avtentikacija');
var ctrlNotifications = require('../controllers/notifications');
var ctrlHomepage = require('../controllers/homepage');

var jwt = require('express-jwt');
var avtentikacija = jwt({
  secret: process.env.JWT_GESLO,
  userProperty: 'payload'
});

/* Events */
router.get('/events', avtentikacija,
  ctrlEvents.eventsList);
router.get('/events/myEvents', avtentikacija,
  ctrlEvents.myEvents);
router.post('/events', avtentikacija,
  ctrlEvents.eventsCreate);
router.delete('/events', avtentikacija,
  ctrlEvents.deleteEvents);
router.get('/events/:idEvent', avtentikacija,
  ctrlEvents.eventsById);
router.put('/events/:idEvent', avtentikacija,
  ctrlEvents.eventsUpdate);
router.delete('/events/:idEvent', avtentikacija,
  ctrlEvents.eventsDelete);
router.get('/events/:idEvent/interested', avtentikacija,
  ctrlEvents.eventInterested);
  
/* Avtentikacija */
router.post('/register', ctrlAvtentikacija.registracija);
router.post('/spremembaGesla',ctrlAvtentikacija.spremembaGesla);
router.post('/login', ctrlAvtentikacija.prijava); 
router.post('/userDelete', ctrlAvtentikacija.deleteUser);

/* Notifications */
router.get('/notifications', ctrlNotifications.notificationList);
router.post('/notifications', ctrlNotifications.notificationCreate);

/* Koledar */
router.get('/koledar', avtentikacija, ctrlHomepage.koledar);
router.post('/koledar', avtentikacija, ctrlHomepage.createDogodekKoledar);
router.post('/deleteKoledar', avtentikacija, ctrlHomepage.deleteKoledar);

/* Urnik */
router.get('/urnik', avtentikacija, ctrlHomepage.urnik);
router.post('/urnik', avtentikacija, ctrlHomepage.createPredmet);
router.put('/urnik/:idPredmeta', avtentikacija, ctrlHomepage.updatePredmet);
router.delete('/urnik/:idPredmeta', avtentikacija, ctrlHomepage.deletePredmet);

/* Todo */
router.get('/todo', avtentikacija, ctrlHomepage.todo);
router.post('/todo', avtentikacija, ctrlHomepage.createTodo);
router.put('/todo/:idTodo', avtentikacija, ctrlHomepage.updateTodo);
router.delete('/todo/:idTodo', avtentikacija, ctrlHomepage.deleteTodo);


module.exports = router;
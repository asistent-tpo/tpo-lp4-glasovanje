var express = require('express');
var streznik = express();
var port = process.env.PORT || 3000;


/**
 * Ko uporabnik obišče začetno stran,
 * izpiši začetni pozdrav
 */
streznik.get('/', function (zahteva, odgovor) {
  odgovor.send(
    '<h1>Lepo pozdravljen naključen uporabnik!</h1>' +
    '<p>A veš, da ti verjetno ne veš, da jaz vem, da ti ' +
    'uporabljaš naslednji spletni brskalnik?</p>' + '<pre>' +
    zahteva.get('User-Agent') + '</pre>'
  );
});


/**
 * Poženi strežnik
 */
streznik.listen(port, function () {
  console.log('Strežnik je pognan na portu ' + port + '!');
});


module.exports = streznik;
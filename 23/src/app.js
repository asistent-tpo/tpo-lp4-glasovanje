require('dotenv').load();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var uglifyJs = require('uglify-js');
var fs = require('fs');

var zdruzeno = uglifyJs.minify({

  'app.js': fs.readFileSync('src/app_client/app.js', 'utf-8'),
  'eventsData.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/eventsData.storitev.js', 'utf-8'),
  'avtentikacija.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/avtentikacija.storitev.js', 'utf-8'),
  'trola.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/trola.storitev.js', 'utf-8'),
  'homepage.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/homepage.storitev.js', 'utf-8'),
  'notificationData.storitev.js': fs.readFileSync('src/app_client/skupno/storitve/notificationData.storitev.js', 'utf-8'),
  'nav_man.direktiva.js': fs.readFileSync('src/app_client/skupno/direktive/navigations/nav_man/nav_man.direktiva.js', 'utf-8'),
  'nav_admin.direktiva.js': fs.readFileSync('src/app_client/skupno/direktive/navigations/nav_admin/nav_admin.direktiva.js', 'utf-8'),
  'nav_user.direktiva.js': fs.readFileSync('src/app_client/skupno/direktive/navigations/nav_user/nav_user.direktiva.js', 'utf-8'),
  'nav_neregistriran.direktiva.js': fs.readFileSync('src/app_client/skupno/direktive/navigations/nav_neregistriran/nav_neregistriran.direktiva.js', 'utf-8'),
  'formatirajDatum.filter.js': fs.readFileSync('src/app_client/skupno/filtri/formatirajDatum.filter.js', 'utf-8'),
  'prijava.krmilnik.js': fs.readFileSync('src/app_client/avtentikacija/prijava/prijava.krmilnik.js', 'utf-8'),
  'registracija.krmilnik.js': fs.readFileSync('src/app_client/avtentikacija/registracija/registracija.krmilnik.js', 'utf-8'),
  'spremembaGesla.krmilnik.js': fs.readFileSync('src/app_client/avtentikacija/spremembaGesla/spremembaGesla.krmilnik.js', 'utf-8'),
  'student_eventi.krmilnik.js': fs.readFileSync('src/app_client/student_eventi/student_eventi.krmilnik.js', 'utf-8'),
  'dodaj_event.krmilnik.js': fs.readFileSync('src/app_client/okno_dodaj_event/dodaj_event.krmilnik.js', 'utf-8'),
  'upravljalec.krmilnik.js': fs.readFileSync('src/app_client/upravljalec/upravljalec.krmilnik.js', 'utf-8'),
  'administrator.krmilnik.js': fs.readFileSync('src/app_client/administrator/administrator.krmilnik.js', 'utf-8'),
  'dodaj_obvestilo.krmilnik.js': fs.readFileSync('src/app_client/okno_dodaj_obvestilo/dodaj_obvestilo.krmilnik.js', 'utf-8'),
  'dodaj_urnik.krmilnik.js': fs.readFileSync('src/app_client/okno_dodaj_urnik/dodaj_urnik.krmilnik.js', 'utf-8'),
  'home.krmilnik.js': fs.readFileSync('src/app_client/home/home.krmilnik.js', 'utf-8'),
  'okno_todo.krmilnik.js': fs.readFileSync('src/app_client/okno_todo/okno_todo.krmilnik.js', 'utf-8'),
  'profile.krmilnik.js': fs.readFileSync('src/app_client/profil/profile.krmilnik.js', 'utf-8'),
  'trola.krmilnik.js': fs.readFileSync('src/app_client/trola/trola.krmilnik.js', 'utf-8'),
  'food.krmilnik.js': fs.readFileSync('src/app_client/food/food.krmilnik.js', 'utf-8'),
  'koledar.krmilnik.js': fs.readFileSync('src/app_client/koledar/koledar.krmilnik.js', 'utf-8'),
  'adm_prijava.krmilnik.js': fs.readFileSync('src/app_client/avtentikacija/adm_prijava/adm_prijava.krmilnik.js', 'utf-8'),
  'potrdi_izbris.krmilnik.js': fs.readFileSync('src/app_client/okno_potrdi_izbris/potrdi_izbirs.krmilnik.js', 'utf-8'),
  'edit_todo.krmilnik.js': fs.readFileSync('src/app_client/edit_todo/edit_todo.krmilnik.js', 'utf-8'),
  'logout.krmilnik.js': fs.readFileSync('src/app_client/logout/logout.krmilnik.js', 'utf-8'),
  'edit_predmet.krmilnik.js': fs.readFileSync('src/app_client/edit_predmet/edit_predmet.krmilnik.js', 'utf-8')
  
});

fs.writeFile('src/public/angular/noclue.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "noclue.min.js".');
});

var passport = require('passport');
require('./app_api/models/db');
require('./app_api/config/passport');

var indexApi = require('./app_api/routes/index');

var app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());


// Odprava varnostnih pomanjkljivosti
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

//app.use('/', indexRouter);
app.use('/api', indexApi);
//app.use('/users', usersRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  console.log("error je: " + err.name);
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "sporočilo": err.name + ": " + err.message
    });
  }
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

(function() {
  function homeCtrl($uibModal, $window, homepage,$rootScope, avtentikacija, $location,$scope) {
    //$scope.$on('$viewContentLoaded', function(){
    //document.addEventListener('DOMContentLoaded', function(event) {
        var vm = this;
    vm.todos1 = [];
    vm.todos2 = [];
    vm.todos3 = [];
    vm.monday = [];
    vm.tuesday = [];
    vm.wednesday = [];
    vm.thursday = [];
    vm.friday = [];
    vm.IdZaizbris = null;
    vm.kajBrises = "";
    vm.todoObject = null;
    vm.predmetObject = null;
    
    
    if(!avtentikacija.preveriTipUporabnika("student")) {
      $location.path("/");
    }
    
    homepage.todo().then(
      function succes(response){
       //console.log(JSON.stringify(response.data, null, 4));
        var resultEvents = response.data;
        
        for(var i = 0 ; i< resultEvents.length; i++){
          if(i%3 == 0){
            vm.todos1.push(resultEvents[i]);  
          }else if(i%3 == 1){
            vm.todos2.push(resultEvents[i]);  
          }else if(i%3 == 2){
            vm.todos3.push(resultEvents[i]);  
          }
        }
      },
      function error(response){
        //console.log(JSON.stringify(response, null, 4));
        if (response.statusText === 'Unauthorized') {
          $window.location.href = '/';
        } else {
          vm.msg = "Error while fetching todo.";
        }
    });
    
    vm.dodajTodo = function() {
     $uibModal.open({
        templateUrl: 'okno_todo/okno_todo.html',
        controller: 'oknoTodoCtrl',
        controllerAs: 'vm'
      });
    };
    
    vm.editTodo = function(todoObject) {
      vm.todoObject = todoObject;
     $uibModal.open({
        templateUrl: 'edit_todo/edit_todo.html',
        controller: 'editTodoCtrl',
        controllerAs: 'vm'
      });
    };
    $rootScope.getTodoObject = function(){
      return vm.todoObject;
    }
    
    $rootScope.potrdiIzbris = function(idTodo,KajBrises) {
      vm.IdZaizbris = idTodo;
      vm.kajBrises = KajBrises;
      $uibModal.open({
        templateUrl: 'okno_potrdi_izbris/potrdi_izbris.html',
        controller: 'potrdiIzbrisCtrl',
        controllerAs: 'vm'
      });
    };
    
    $rootScope.kajJeZaZabrisat = function(){
      return vm.kajBrises;
    };
    
    $rootScope.zbrisiTodo = function() {
      homepage.deleteTodo(vm.IdZaizbris).then(
        function succes(response){
          vm.IdZaizbris = null;
           window.location.reload();
           window.alert("Todo successfully deleted.");
        },
        function error(response){
          window.alert("Error while deleting Todo.");
          console.log(response.e);
      });
    };
    
      homepage.urnik().then(
        function succes(response){
         console.log(JSON.stringify(response.data, null, 4));
          var resultData = response.data;
          
          for(var i = 0 ; i< resultData.length; i++){
            var res = resultData[i];
            res.start = res.hourStart + ":" + res.minStart; 
            res.end = res.hourEnd + ":" + res.minEnd; 
            switch(res.color) {
              case "0": res.barva="event-1"; break;
              case "1": res.barva="event-2"; break;
              case "2": res.barva="event-3"; break;
              case "3": res.barva="event-4"; break;
            }
            
            if(res.day == 4){
              vm.monday.push(res);  
            }else if(res.day == 5){
              vm.tuesday.push(res);  
            }else if(res.day == 6){
              vm.wednesday.push(res);  
            } else if(res.day == 7){
              vm.thursday.push(res);  
            } else if(res.day == 8){
              vm.friday.push(res);  
            }
            
            console.log(JSON.stringify(res, null, 4));
          }
          
        
      },
      function error(response){
        //console.log(JSON.stringify(response, null, 4));
        if (response.statusText === 'Unauthorized') {
          $window.location.href = '/';
        } else {
          vm.msg = "Error while fetching urnik.";
        }
      });
    //-----------
   
    
    
    vm.dodajPredmet = function() {
     $uibModal.open({
        templateUrl: 'okno_dodaj_urnik/dodaj_urnik.html',
        controller: 'oknoDodajUrnikCtrl',
        controllerAs: 'vm'
      });
    };
    
     vm.editPredmet = function(predmetObject) {
       vm.predmetObject = predmetObject;
     $uibModal.open({
        templateUrl: 'edit_predmet/edit_predmet.html',
        controller: 'oknoEditUrnikCtrl',
        controllerAs: 'vm'
      });
    };
    
    $rootScope.getPredmet = function(){
      return vm.predmetObject;
    }
    
    $rootScope.zbrisiPredmet = function() {
      homepage.deletePredmet(vm.IdZaizbris).then(
          function succes(response){
             window.location.reload();
             window.alert("Predmet successfully deleted.");
          },
          function error(response){
            window.alert("Error while deleting Predmet.");
            console.log(response.e);
        });
    };


    var script1 = document.createElement('script');
    script1.type = 'text/javascript';
    script1.src = "/schedule_assets/js/util.js";
    document.body.appendChild(script1);
    var script2 = document.createElement('script');
    script2.type = 'text/javascript';
    script2.src = "/schedule_assets/js/main.js";
    document.body.appendChild(script2); 
    var script3 = document.createElement('script');
    script3.type = 'text/javascript';
    script3.src = "/koledar/js/all.js";
    document.body.appendChild(script3);
      
    //}) //----------------------
   // });
  
  }
  
  homeCtrl.$inject = ['$uibModal', '$window', 'homepage','$rootScope', 'avtentikacija', '$location','$scope'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('homeCtrl', homeCtrl);
})();
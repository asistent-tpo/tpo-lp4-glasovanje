(function() {
  function oknoEditUrnikCtrl($uibModalInstance, $window, homepage,$scope) {
    var vm = this;
   
    vm.predmet = {
      _id:"",
      title: "",
      day: "4",
      hourStart: "",
      minStart: "",
      hourEnd: "",
      minEnd: "",
      color: "0",
    };
    var predmet = $scope.getPredmet();
    
    vm.predmet._id = predmet._id;
    vm.predmet.title = predmet.title;
    vm.predmet.day = predmet.day+"";
    vm.predmet.hourStart = predmet.hourStart;
    vm.predmet.minStart = predmet.minStart;
    vm.predmet.hourEnd = predmet.hourEnd;
    vm.predmet.minEnd = predmet.minEnd;
    vm.predmet.color = predmet.color;
    console.log("id predmeta je = "+vm.predmet._id);
    console.log("title predmeta je = "+vm.predmet._id);
    
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.updatePredmet = function(){
      homepage.updatePredmet(vm.predmet).then(
        function succes(response){
           window.location.reload();
           window.alert("predmet successfully updated.");
        },
        function error(response){
          window.alert("Error while Updating predmet.");
          console.log(response.e);
      });
      
      $uibModalInstance.close();
    }
    
  }
    
  
  oknoEditUrnikCtrl.$inject = ['$uibModalInstance', '$window','homepage','$scope'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('oknoEditUrnikCtrl', oknoEditUrnikCtrl);
})();


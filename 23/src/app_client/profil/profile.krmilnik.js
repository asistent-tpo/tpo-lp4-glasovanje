(function() {
  function profileCtrl( $window, notificationData, $location, avtentikacija) {
    var vm = this;
    
    if(!avtentikacija.preveriTipUporabnika("student") && !avtentikacija.preveriTipUporabnika("organizer")) {
      $location.path("/");
    }
    
    vm.userType = avtentikacija.trenutniUporabnik().userType;
    vm.nav = false;
    if(vm.userType === "student"){
      vm.nav = true;
    }
    
    vm.msg = "Searching events...";
    
    
    notificationData.notifications().then(
        function succes(response){
         console.log(JSON.stringify(response.data, null, 4));
         var resultEvents = response.data;
         
         vm.notifications  = resultEvents;
         vm.msg = "";
      },
      function error(response){
        //console.log(JSON.stringify(response, null, 4));
        if (response.statusText === 'Unauthorized') {
          $window.location.href = '/';
        } else {
          vm.msg = "Error while fetching notifications.";
        }
      });
    
    
    vm.changePassword = function(){
      $location.path("/spremembaGesla");
    };
  }
   
  
  
  
  profileCtrl.$inject = ['$window','notificationData', '$location', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('profileCtrl', profileCtrl);
})();
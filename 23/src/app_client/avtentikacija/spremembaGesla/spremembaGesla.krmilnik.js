(function() {
  function spremeniGeslo($location, avtentikacija) {
    var vm = this;
  
    vm.geslo = {
      OldPassword: "",
      newPassword: "",
      newPassword2: "",
    };

    
    vm.novoGeslo = function() {
      vm.napakaNaObrazcu = "";
      if ( !vm.geslo.OldPassword || !vm.geslo.newPassword || !vm.geslo.newPassword2) {
        window.alert("Zahtevani so vsi podatki, prosim poskusite znova!");
        return;
      } 
        console.log("PREVERI GESLA");
        if(vm.geslo.newPassword != vm.geslo.newPassword2){
           window.alert("GESLI SE NE UJEMTA!");
          console.log("napaka pri podatkih-gesla se ne ujemata");
          return false;
        }
 
       var user = {
          email: "",
          password: "",
          newPassword: ""
        };
        
        user.email = avtentikacija.trenutniUporabnik().email;
        console.log("trenuten uporabnik = " + avtentikacija.trenutniUporabnik().userType);
        user.password = vm.geslo.OldPassword;
        user.newPassword = vm.geslo.newPassword;
 
        avtentikacija
        .spremembaGesla(user)
        .then(
          function(success) {
            if(avtentikacija.trenutniUporabnik().userType == "student"){
              $location.path("/profile");   
            }else if(avtentikacija.trenutniUporabnik().userType == "organizer"){
              $location.path("/upravljalec");   
            }else if(avtentikacija.trenutniUporabnik().userType == "administrator"){
              $location.path("/administrator");   
            }
           else {
             console.log(avtentikacija.trenutniUporabnik().userType);
             console.log("tip uporabnika ni nobena od izbranih pocij zato je redirectan na login");
             $location.path("/")
           }
          },
          function(napaka) {
            //console.log(JSON.stringify(napaka, null, 4));
            window.alert(napaka.data.sporocilo);
          }
      );
      
    };
  }
  
  spremeniGeslo.$inject = ['$location', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('spremeniGeslo', spremeniGeslo);
})();
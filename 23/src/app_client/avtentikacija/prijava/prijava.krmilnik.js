(function() {
  function prijavaCtrl($location, avtentikacija) {
    var vm = this;
    vm.loginImg = "/images/favicon.png";

    vm.glavaStrani = {
      naslov: "Login v NoClue"
    };

    vm.prijavniPodatki = {
      email: "",
      passowrd: "",
      tipUporabnika:"student"
    };

    vm.posiljanjePodatkov = function() {
      console.log("pride1");
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.email || !vm.prijavniPodatki.password || !vm.prijavniPodatki.tipUporabnika) {
        console.log("problem pri podatkih");
        console.log(vm.prijavniPodatki.email);
        console.log(vm.prijavniPodatki.password);
        console.log(vm.prijavniPodatki.tipUporabnika);
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } else {
        console.log("vstopam v prijavo");
        vm.izvediPrijavo();
      }
    };

    vm.izvediPrijavo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .prijava(vm.prijavniPodatki)
        .then(
          function(success) {
            if(vm.prijavniPodatki.tipUporabnika == "student"){
              console.log("student");
              $location.path("/home");
              
            }else if(vm.prijavniPodatki.tipUporabnika == "organizer"){
              console.log("upravljalec");
              $location.path("/upravljalec");
            }
            
            //$location.path("/events");
          },
          function(napaka) {
            window.alert(napaka.data.sporocilo);
            vm.napakaNaObrazcu = napaka.data.sporocilo;
          }
        );
    };
  }
  prijavaCtrl.$inject = ['$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('noclue')
    .controller('prijavaCtrl', prijavaCtrl);
})();
(function() {
  function registracijaCtrl($location, avtentikacija) {
    var vm = this;
  
    vm.registeracijskiPodatki = {
      email: "",
      password: "",
      password2: "",
      userType: "student"
    };
    

    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if ( !vm.registeracijskiPodatki.email || !vm.registeracijskiPodatki.password ||
             !vm.registeracijskiPodatki.password2 ) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        console.log("napaka pri podatkih");
        return false;
      } 
      else {
        if(vm.registeracijskiPodatki.password2 != vm.registeracijskiPodatki.password){
           vm.napakaNaObrazcu = "GESLI SE NE UJEMTA!";
          console.log("napaka pri podatkih");
          return false;
        }

        vm.izvediRegistracijo();
      }
    };

    vm.izvediRegistracijo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .registracija(vm.registeracijskiPodatki)
        .then(
          function(success) {
            if(vm.registeracijskiPodatki.userType == "student") {
              console.log("student");
              $location.path("/home");
            } else {
              $location.path("/upravljalec");
            }
          },
          function(napaka) {
            console.log("napaka");
          }
      );
    };
  }
  
  registracijaCtrl.$inject = ['$location', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('registracijaCtrl', registracijaCtrl);
})();
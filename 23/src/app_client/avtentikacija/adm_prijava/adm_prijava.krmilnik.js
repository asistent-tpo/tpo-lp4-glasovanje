(function() {
  function admPrijavaCtrl($location, avtentikacija) {
    var vm = this;
    vm.loginImg = "/images/favicon.png";

    vm.glavaStrani = {
      naslov: "Login v NoClue"
    };

    vm.prijavniPodatki = {
      email: "",
      passowrd: "",
      tipUporabnika:"admin" // administrator
    };

    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.email || !vm.prijavniPodatki.password ) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } else {
        console.log("vstopam v prijavo");
        vm.izvediPrijavo();
      }
    };

    vm.izvediPrijavo = function() {
      vm.napakaNaObrazcu = "";
      console.log("prijava ho!");
      avtentikacija
        .prijava(vm.prijavniPodatki)
        .then(
          function(success) {
              $location.path("/administrator");
          },
          function(napaka) {
            window.alert(napaka.data.sporocilo);
            vm.napakaNaObrazcu = napaka.data.sporocilo;
          }
        );
    };
  }
  admPrijavaCtrl.$inject = ['$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('noclue')
    .controller('admPrijavaCtrl', admPrijavaCtrl);
})();
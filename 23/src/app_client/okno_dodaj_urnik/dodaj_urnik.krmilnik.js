(function() {
  
  function oknoDodajUrnikCtrl($uibModalInstance, $window, homepage,avtentikacija) {
 
      var vm = this;
   
    vm.predmet = {
      title: "",
      day: "4",
      hourStart: "",
      minStart: "",
      hourEnd: "",
      minEnd: "",
      color: "0",
      userId: ""
    };
    
    vm.dodajPredmet = function() {
      console.log("dodaj urnikkkkkkkkkkkkkkkkk");
      console.log(JSON.stringify(vm.predmet, null, 4));
      if (!vm.predmet.title || vm.predmet.hourStart == null || vm.predmet.minStart == null || vm.predmet.hourEnd == null ||
          vm.predmet.minEnd == null || vm.predmet.color == null) {
        window.alert("Zahtevani so vsi podatki, prosim poskusite znova!");
        return false;
      } else {
        vm.predmet.userId = avtentikacija.trenutniUporabnik()._id;
        homepage.createPredmet(vm.predmet).then(
          function succes(response){
             window.location.reload();
             window.alert("Predmet successfully created.");
          },
          function error(response){
            window.alert("Error while creating Predmet.");
            console.log(response.e);
        });
      }
    };
    
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
      
  
    
    
  }
    
  
  
  oknoDodajUrnikCtrl.$inject = ['$uibModalInstance', '$window','homepage','avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('oknoDodajUrnikCtrl', oknoDodajUrnikCtrl);
})();


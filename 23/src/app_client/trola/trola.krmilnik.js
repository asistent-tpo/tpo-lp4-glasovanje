(function() {
  function trolaCtrl( $window, trolaStoreitev,$location,$rootScope, avtentikacija) {
    var vm = this;
    
    vm.showNavStudent = false;
    var trenutenUporabnik = avtentikacija.trenutniUporabnik();
    if(trenutenUporabnik) {
      var tipUporabnika = trenutenUporabnik.userType;
      if(tipUporabnika == "student") {
        vm.showNavStudent=true;
      }
    }
    
    vm.msg = "";
    vm.msgToCtr = "";
    vm.msgFromCtr = "";
    var stationCenter = null;
    var stationFromCenter = null;
    vm.sortBusses = function(sort,value){
        vm.msg = "Searching busses...";
        //klic api za vse postaje
        trolaStoreitev.pridobiVsePostaje().then(
            function succes(response){
                stationCenter = null;
                stationFromCenter = null;
                
                var resultStations = response.data.data;
                //console.log(response.data,null,4);
                for (var i = 0 ; i< resultStations.length; i++){
                    console.log((resultStations[i].name.toUpperCase()) +" =?= "+ (value.toUpperCase())  )
                    if((resultStations[i].name.toUpperCase()) == (value.toUpperCase())){
                        console.log(((resultStations[i].ref_id).toString().substr(-1)) );
                        
                        if(((resultStations[i].ref_id).toString().substr(-1))  ==  "1" || ((resultStations[i].ref_id).toString().substr(-1))  ==  "3"){
                            stationCenter = resultStations[i];
                            console.log( "postaja to center =" + stationCenter.int_id);
                        }else {
                            stationFromCenter = resultStations[i];
                            console.log( "postaja from center =" + stationFromCenter.int_id);
                        }
                    }
                }
                console.log("klicSpecificnePostaje");
                vm.msgToCtr = "";
                vm.msgFromCtr = "";
                pridobiUrnikZaPostajo(sort,stationCenter,stationFromCenter);
                
            },
            function error(response){
                vm.msg = "Error while fetching busArivals.";
            }
            );
            
            
        
    }
    
    var pridobiUrnikZaPostajo = function(smer,postajaCenter,postajaFromCenter){
        //if(smer == "center"){
            if(postajaCenter != null){
                klicSpecificnePostaje(postajaCenter.int_id,1);
                
            }else {
                 vm.msgToCtr = "ta postaja verjetno ne obstaja"
            }
            
        //}else {
            if(postajaFromCenter != null){
                 klicSpecificnePostaje(postajaFromCenter.int_id,2);
            }else {
                 vm.msgFromCtr = "ta postaja verjetno ne obstaja"
            }
            
        //}
        
    }
    var klicSpecificnePostaje = function(idPostaje,smer){
        trolaStoreitev.pridobiUrnikZaPostajo(idPostaje).then(
        function succes(response){
         console.log(JSON.stringify(response.data, null, 4));
         var resultEvents = response.data;
         
         if(smer == 1){
             vm.busArivalsToCenter = resultEvents.data;
         }else if(smer == 2){
             vm.busArivalsFromCenter = resultEvents.data;
         }
         //vm.busArivals  = resultEvents.data;
         //console.log(JSON.stringify(vm.busArivals, null, 4));
         //console.log(vm.busArivals.data[0].route_number + " , " + vm.busArivals.data[0].route_name + " , " + vm.busArivals.data[0].eta);
         //console.log(resultEvents[0].route_number + ", " + resultEvents[0].route_name + " , " + resultEvents[0].eta);
         vm.msg = "";
         
         
         
      },
      function error(response){
        //console.log(JSON.stringify(response, null, 4));
        if (response.statusText === 'Unauthorized') {
          $window.location.href = '/';
        } else {
          vm.msg = "Error while fetching notifications.";
        }
      });
    }
    
    
  }
    
  
  
  trolaCtrl.$inject = ['$window', 'trolaStoreitev','$location','$rootScope', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('trolaCtrl', trolaCtrl);
})();
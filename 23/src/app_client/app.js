(function() {
  function nastavitev($routeProvider, $locationProvider, $sceDelegateProvider) {
    $routeProvider
      .when('/', {
          templateUrl: 'avtentikacija/prijava/prijava.html',
          controller: 'prijavaCtrl',
          controllerAs: 'vm'
      }).when('/administrator', {
          templateUrl: 'administrator/administrator.html',
          controller: 'administratorCtrl',
          controllerAs: 'vm'
      })
      .when('/spremembaGesla', {
          templateUrl: 'avtentikacija/spremembaGesla/spremembaGesla.pogled.html',
          controller: 'spremeniGeslo',
          controllerAs: 'vm'
      })
      .when('/home', {
          templateUrl: 'home/home.html',
          controller: 'homeCtrl',
          controllerAs: 'vm'
      })
      .when('/studentEventi', {
          templateUrl: 'student_eventi/student_eventi.html',
          controller: 'studentEventCtrl',
          controllerAs: 'vm'
      })
      .when('/upravljalec', {
          templateUrl: 'upravljalec/upravljalec.html',
          controller: 'upravljalecCtrl',
          controllerAs: 'vm'
      })
      .when('/register', {
          templateUrl: 'avtentikacija/registracija/registracija.pogled.html',
          controller: 'registracijaCtrl',
          controllerAs: 'vm'
      })
      .when('/notifications', {
          templateUrl: 'okno_dodaj_obvestilo/dodaj_obvestilo.pogled.html',
          controller: 'dodajObvestiloCtrl',
          controllerAs: 'vm'
      })
      .when('/profile', {
          templateUrl: 'profil/profil.html',
          controller: 'profileCtrl',
          controllerAs: 'vm'
      })
      .when('/food', {
          templateUrl: 'food/food.html',
          controller: 'foodCtrl',
          controllerAs: 'vm'
      })
      .when('/trola', {
          templateUrl: 'trola/trola.html',
          controller: 'trolaCtrl',
          controllerAs: 'vm'
      })
      .when('/koledar', {
          templateUrl: 'koledar/koledar.html',
          controller: 'koledarCtrl',
          controllerAs: 'vm'
      })
      .when('/logout', {
          templateUrl: 'logout/logout.html',
          controller: 'logoutCtrl',
          controllerAs: 'vm'
      })
      .when('/adminPrijava', {
          templateUrl: 'avtentikacija/adm_prijava/adm_prijava.html',
          controller: 'admPrijavaCtrl',
          controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/'});
      $locationProvider.html5Mode(true);
      
      $sceDelegateProvider.resourceUrlWhitelist([
      'self', 'https://maps.google.com/maps?q=**','https://194.33.12.24/**','https://cors.io/?**'
    ]);
  }
  
  /* global angular */
  angular
    .module('noclue', ['ngRoute' , 'ui.bootstrap','ngSanitize', 'ngGeolocation']) 
    .config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', nastavitev]);
})();
(function() {
  function logoutCtrl($location, avtentikacija) {
    
    avtentikacija.odjava();
  }
  
  logoutCtrl.$inject = ['$location', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('logoutCtrl', logoutCtrl);
})();
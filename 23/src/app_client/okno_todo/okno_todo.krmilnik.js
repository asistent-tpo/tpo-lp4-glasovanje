(function() {
  function oknoTodoCtrl($uibModalInstance, $window, homepage,avtentikacija) {
      //var mongoose = require('mongoose');
      //var Schema = mongoose.Schema;
    
    var vm = this;
   
    vm.todo = {
      description: "",
      userId: "" //Schema.Types.ObjectId
    };
    
    vm.createTodo = function() {
      if (!vm.todo.description) {
        window.alert("Zahtevani so vsi podatki, prosim poskusite znova!");
        return false;
      } else {
        vm.todo.userId = avtentikacija.trenutniUporabnik()._id;
        console.log("user id je  = "+ vm.todo.userId);
        homepage.createTodo(vm.todo).then(
          function succes(response){
           
             window.location.reload();
             window.alert("Todo successfully created.");
          },
          function error(response){
            window.alert("Error while creating Todo.");
            console.log(response.e);
        });
      }
    };
    
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
  }
    
  
  
  oknoTodoCtrl.$inject = ['$uibModalInstance', '$window','homepage','avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('oknoTodoCtrl', oknoTodoCtrl);
})();


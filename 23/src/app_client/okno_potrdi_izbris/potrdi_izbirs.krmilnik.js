(function() {
  function potrdiIzbrisCtrl($uibModalInstance, $window, notificationData,$scope) {
    var vm = this;
    
    
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.izbrisi = function(){
      var kajZbrisat = $scope.kajJeZaZabrisat();
      if(kajZbrisat == "kolendar"){
        alert("akcija za brisanje kolendarja");
      }else if(kajZbrisat == "urnik"){
        $scope.zbrisiPredmet();
      }else if(kajZbrisat == "todo"){
        $scope.zbrisiTodo();
      }
      $uibModalInstance.close();
    }
    /*
    vm.zbrisiTodo = function(){
      $scope.zbrisiTodo();
      $uibModalInstance.close();
    }
    */
    
  }
    
  
  
  potrdiIzbrisCtrl.$inject = ['$uibModalInstance', '$window','notificationData','$scope'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('potrdiIzbrisCtrl', potrdiIzbrisCtrl);
})();
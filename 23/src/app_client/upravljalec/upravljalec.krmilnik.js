(function() {
  function upravljalecCtrl($routeParams, $window, $uibModal, eventsData, $location, avtentikacija) {
    var vm = this;
    
    if(!avtentikacija.preveriTipUporabnika("organizer")) {
      $location.path("/");
    }
    
    vm.idEvent = $routeParams.idEvent;
    vm.event = {
      _id: vm.idEvent,
      title: "",
      date: "",
      entry: "",
      description: "",
      event_image: "",
      location: "",
      day: "",
      month: "",
      year: "",
    };
    
    
    vm.eventPrikaziPojavnoOkno = function() {
     $uibModal.open({
         templateUrl: 'okno_dodaj_event/dodaj_event.html',
        controller: 'dodajEventCtrl',
        controllerAs: 'vm'
     
      });
    };
    
    vm.event;
    
    eventsData.usersEvents().then(
      function succes(response){
        //console.log("odgovor: " + response.data);
        //vm.msg = response.data.length > 0 ? "" : "You haven\'t created any events yet. Create new events now.";
        vm.events = response.data;
        console.log(JSON.stringify(vm.events, null, 4));
        
        /*
        vm.hidePagination = false;
          if(events.length<11){
            vm.showEvents = events;
            vm.hidePagination = true;
          }else{
            vm.showEvents = events.slice(0,10);
          }
          */
      },
      function error(response){
        if (response.statusText === 'Unauthorized') {
          $location.path('/');
        }
        vm.msg = "Error while fetching events.";
        console.log(response.e);
      });
    
    
  }
  
  upravljalecCtrl.$inject = ['$routeParams', '$window', '$uibModal','eventsData', '$location', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('upravljalecCtrl', upravljalecCtrl);
})();
(function() {
  function dodajObvestiloCtrl($uibModalInstance, $window, notificationData, $location) {
    var vm = this;
    vm.msg = "Searching events...";
    
    vm.notification = {
      title: "",
      date: "",
      description: ""
    };
    
    vm.createNotification = function($scope) {
      vm.napakaNaObrazcu = "";
      if (!vm.notification.title || !vm.notification.description ) {
        window.alert("Zahtevani so vsi podatki, prosim poskusite znova!");
        console.log("napaka pri podatkih");
        return false;
      } else {
        
        var localDate = new Date();
        var userTimezoneOffset = localDate.getTimezoneOffset() * 60000;
        vm.notification.date = new Date(localDate.getTime() - userTimezoneOffset);

        notificationData.createNotification(vm.notification).then(
          function succes(response){
             window.location.reload();
             window.alert("Notification successfully created.");
          },
          function error(response){
            window.alert("Error while creating event.");
            console.log(response.e);
        });
      }
    };
    
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
  }
    
  
  
  dodajObvestiloCtrl.$inject = ['$uibModalInstance', '$window','notificationData', '$location'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('dodajObvestiloCtrl', dodajObvestiloCtrl);
})();
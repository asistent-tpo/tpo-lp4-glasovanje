(function() {
  function editTodoCtrl($uibModalInstance, $window, homepage,avtentikacija,$scope) {
      //var mongoose = require('mongoose');
      //var Schema = mongoose.Schema;
    
    var vm = this;
    
   
    vm.todo = {
      description: "",
      _id: "" //Schema.Types.ObjectId
    };
    var tempTodo = $scope.getTodoObject();
    vm.todo.description = tempTodo.description;//$scope.getTodoObject().description;
    vm.todo._id = tempTodo._id;
    console.log("id todoja = "+ vm.todo._id);
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.updateTodo = function(){
      homepage.updateTodo(vm.todo).then(
        function succes(response){
           window.location.reload();
           window.alert("Todo successfully updated.");
        },
        function error(response){
          window.alert("Error while Updating Todo.");
          console.log(response.e);
      });
      
      $uibModalInstance.close();
    }
    
    vm.testFun = function(){
      alert("testtttttt");
    }
    
  }
    
  
  
  editTodoCtrl.$inject = ['$uibModalInstance', '$window','homepage','avtentikacija','$scope'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('editTodoCtrl', editTodoCtrl);
})();


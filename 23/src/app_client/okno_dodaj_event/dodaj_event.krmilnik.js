(function() {
  function dodajEventCtrl($uibModalInstance, $window, eventsData, $location) {
    var vm = this;
    vm.msg = "Searching events...";
    
    vm.event = {
      title: "",
      location: "",
      date: "",
      day: "",
      month: "0",
      year: "",
      description: ""
    };
    
    vm.createEvent = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.event.title || !vm.event.organizer || !vm.event.day || !vm.event.month || !vm.event.year) {
        window.alert("Zahtevani so vsi podatki, prosim poskusite znova!");
        console.log("napaka pri podatkih");
        return false;
      } else {
        
        var localDate = new Date(vm.event.year, vm.event.month, vm.event.day);
        var userTimezoneOffset = localDate.getTimezoneOffset() * 60000;
        vm.event.date = new Date(localDate.getTime() - userTimezoneOffset);

        eventsData.createEvent(vm.event).then(
          function succes(response){
           
             window.location.reload();
             window.alert("Event successfully created.");
          },
          function error(response){
            window.alert("Error while creating event.");
            console.log(response.e);
        });
      }
    };
    
    vm.modalnoOknoPreklic = {
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
  }
    
  
  
  dodajEventCtrl.$inject = ['$uibModalInstance', '$window','eventsData', '$location'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('dodajEventCtrl', dodajEventCtrl);
})();
(function() {
  function foodCtrl( $window,  $geolocation, avtentikacija) {
    var vm = this;
    
    vm.showNavStudent = false;
    var trenutenUporabnik = avtentikacija.trenutniUporabnik();
    if(trenutenUporabnik) {
      var tipUporabnika = trenutenUporabnik.userType;
      if(tipUporabnika == "student") {
        vm.showNavStudent=true;
      }
    }
    
    
    vm.position = {
      lat: "46.0538819",
      lon: "14.5030808"
    };
    
    
    $geolocation.getCurrentPosition().then(function(position) {
      console.log(position, 'current position');
      vm.position.lat = position.coords.latitude;
      vm.position.lon = position.coords.longitude;
    });

  }
  
    
  
  
  foodCtrl.$inject = ['$window', '$geolocation', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('noclue')
    .controller('foodCtrl', foodCtrl);
})();
 (function() {
  var formatirajDatum = function() {
    return function(datum) {
      var date = new Date(datum);
      var imenaMesecev = ["januar", "februar", "marec", "april", "maj", "junij", "julij", "avgust", "september", "oktober", "november", "december"];
      var d = date.getDate();
      var m = imenaMesecev[date.getMonth()];
      var l = date.getFullYear();
      var odgovor = d + ". " + m + ", " + l;
      return odgovor;
    };
  };


  /* global angular */
  angular
    .module('noclue')
   .filter('formatirajDatum', formatirajDatum);
 })();
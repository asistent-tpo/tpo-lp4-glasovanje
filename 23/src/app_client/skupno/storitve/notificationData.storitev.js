(function() {
  var notificationData = function($http, avtentikacija) {

    var notifications = function(sort, value) {
        return $http.get('/api/notifications');  
    };
    
    var createNotification = function(notification) {
      return $http.post('/api/notifications', notification);
    };
    
    
    
    return {
      'notifications': notifications,
      'createNotification': createNotification
    };
  };
  
  notificationData.$inject = ['$http', 'avtentikacija'];

  /* global angular */
  angular
    .module('noclue')
    .service('notificationData', notificationData);
})();
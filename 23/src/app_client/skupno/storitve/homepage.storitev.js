(function() {
  var homepage = function($http, avtentikacija) {

    
    var urnik = function() {
        return $http.get('/api/urnik',{headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }});  
    };

    var createPredmet = function(predmet) {
        return $http.post('/api/urnik', predmet, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    
    var deletePredmet = function(idPredmet) {
        return $http.delete('/api/urnik/'+idPredmet, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var updatePredmet = function(predmet) {
      return $http.put('/api/urnik/'+predmet._id, predmet, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };

    var todo = function() {
        return $http.get('/api/todo', {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };


    var createTodo = function(todo) {
        return $http.post('/api/todo', todo, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });  
    };
    
    var deleteTodo = function(idTodo) {
        return $http.delete('/api/todo/'+idTodo, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    
    var updateTodo = function(todo) {
      return $http.put('/api/todo/'+todo._id, todo, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    
  
    return {
        'urnik': urnik,
        'createPredmet': createPredmet,
        'deletePredmet': deletePredmet,
        'todo': todo,
        'createTodo': createTodo,
        'deleteTodo': deleteTodo,
        'updateTodo': updateTodo,
        'updatePredmet': updatePredmet
    };
  };
  
  homepage.$inject = ['$http', 'avtentikacija'];

  /* global angular */
  angular
    .module('noclue')
    .service('homepage', homepage);
})();
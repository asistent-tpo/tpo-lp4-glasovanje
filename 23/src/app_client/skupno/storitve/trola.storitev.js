(function() {
  var trolaStoreitev = function($http) {

    var pridobiUrnikZaPostajo = function(idPostaje) {
        //https://cors.io/?
        //return $http.get('https://194.33.12.24/timetables/liveBusArrival?station_int_id='+idPostaje);  
        return $http.get('https://cors.io/?http://194.33.12.24/timetables/liveBusArrival?station_int_id='+idPostaje);  
    };
    
    var pridobiVsePostaje = function() {
        //https://cors.io/?
        return $http.get('https://cors.io/?http://data.lpp.si/stations/getAllStations');  
    };
    
    
    /*
    var podrobnostiEventaZaId = function(idEvent) {
      return $http.get('/api/events/' + idEvent, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var izbrisiEventZId = function(idEvent) {
      return $http.delete('/api/events/' + idEvent, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var izbrisiVseEvente = function() {
      return $http.delete('/api/events/', {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var usersEvents = function() {
      return $http.get('/api/events/myEvents', {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var createEvent = function(event) {
      return $http.post('/api/events', event, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var editEvent = function(event) {
      return $http.put('/api/events/'+event._id, event, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var eventInterested = function(eventId, isInterested) {
      return $http.get('/api/events/'+eventId+"/interested?isInterested="+isInterested, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    */
    
    return {
      'pridobiUrnikZaPostajo': pridobiUrnikZaPostajo,
      'pridobiVsePostaje': pridobiVsePostaje
      //'podrobnostiEventaZaId': podrobnostiEventaZaId,
      //'izbrisiEventZId': izbrisiEventZId,
      //'usersEvents': usersEvents,
      //'editEvent': editEvent,
      //'eventInterested': eventInterested,
      //'izbrisiVseEvente': izbrisiVseEvente
    };
  };
  
  trolaStoreitev.$inject = ['$http'];

  /* global angular */
  angular
    .module('noclue')
    .service('trolaStoreitev', trolaStoreitev);
})();
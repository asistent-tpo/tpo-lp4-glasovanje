(function() {
    function avtentikacija($window, $http, $location) {
     var b64Utf8 = function (niz) {
      return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
    };
  
    var shraniZeton = function(zeton) {
      $window.localStorage['noclue-zeton'] = zeton;
    };
    
    var vrniZeton = function() {
     return $window.localStorage['noclue-zeton'];
    };
    
    var registracija = function(uporabnik) {
      return $http.post('/api/register', uporabnik).then(
        function success(odgovor) {
          shraniZeton(odgovor.data.zeton);
        });
    };
    
    var spremembaGesla = function(uporabnik) {
      return $http.post('/api/spremembaGesla', uporabnik).then(
        function success(odgovor) {
          shraniZeton(odgovor.data.zeton);
        });
    };
    
    
    var prijava = function(uporabnik) {
      return $http.post('/api/login', uporabnik).then(
        function success(odgovor) {
          shraniZeton(odgovor.data.zeton);
        });
    };

    var odjava = function() {
      $window.localStorage.removeItem('noclue-zeton');
      $location.path("/");
    };
    
    var jePrijavljen = function() {
    var zeton = vrniZeton();
    if (zeton) {
      var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
      return koristnaVsebina.datumPoteka > Date.now() / 1000;
    } else {
      return false;
    }
  };
  
  var trenutniUporabnik = function() {
    if (jePrijavljen()) {
      var zeton = vrniZeton();
      var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
     // console.log(JSON.stringify(koristnaVsebina, null, 4));
      return {
        _id: koristnaVsebina._id,
        email: koristnaVsebina.email,
        //name: koristnaVsebina.name,
        //surname: koristnaVsebina.surname,
        img: koristnaVsebina.img,
        userType: koristnaVsebina.tipUporabnika
      };
    }
  };
  
   var preveriTipUporabnika = function(tip) {
    if (jePrijavljen()) {
      if(trenutniUporabnik().userType == tip) {
        return true;
      }
    } 
    return false;
  };
    
    return {
      'shraniZeton': shraniZeton,
      'vrniZeton': vrniZeton,
      'registracija': registracija,
      'spremembaGesla':spremembaGesla,
      'prijava': prijava,
      'odjava': odjava,
      'jePrijavljen': jePrijavljen,
      'trenutniUporabnik': trenutniUporabnik,
      'preveriTipUporabnika': preveriTipUporabnika
    };
  }
  avtentikacija.$inject = ['$window', '$http', '$location'];
  
  /* global angular */
  angular
    .module('noclue')
    .service('avtentikacija', avtentikacija);
})();
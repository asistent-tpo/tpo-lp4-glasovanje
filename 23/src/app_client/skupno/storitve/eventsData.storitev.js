(function() {
  var eventsData = function($http, avtentikacija) {

    var events = function(sort, value) {
        return $http.get('/api/events', {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });  
    };
    var podrobnostiEventaZaId = function(idEvent) {
      return $http.get('/api/events/' + idEvent, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var izbrisiEventZId = function(idEvent) {
      return $http.delete('/api/events/' + idEvent, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var izbrisiVseEvente = function() {
      return $http.delete('/api/events/', {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var usersEvents = function() {
      return $http.get('/api/events/myEvents', {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var createEvent = function(event) {
      return $http.post('/api/events', event, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var editEvent = function(event) {
      return $http.put('/api/events/'+event._id, event, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var eventInterested = function(eventId, isInterested) {
      return $http.get('/api/events/'+eventId+"/interested?isInterested="+isInterested, {
         headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    
    return {
      'events': events,
      'podrobnostiEventaZaId': podrobnostiEventaZaId,
      'izbrisiEventZId': izbrisiEventZId,
      'usersEvents': usersEvents,
      'createEvent': createEvent,
      'editEvent': editEvent,
      'eventInterested': eventInterested,
      'izbrisiVseEvente': izbrisiVseEvente
    };
  };
  
  eventsData.$inject = ['$http', 'avtentikacija'];

  /* global angular */
  angular
    .module('noclue')
    .service('eventsData', eventsData);
})();
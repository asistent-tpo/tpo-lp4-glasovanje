(function() {
  var navNeregistriran = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/navigations/nav_neregistriran/nav_neregistriran.html'
    };
  };
  
  /* global angular */
  angular
    .module('noclue')
    .directive('navNeregistriran', navNeregistriran);
})();
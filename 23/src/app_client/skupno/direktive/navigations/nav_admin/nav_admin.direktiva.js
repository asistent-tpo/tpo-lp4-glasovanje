(function() {
  var navAdmin = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/navigations/nav_admin/nav_admin.html'
    };
  };
  
  /* global angular */
  angular
    .module('noclue')
    .directive('navAdmin', navAdmin);
})();
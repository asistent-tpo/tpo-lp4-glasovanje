(function() {
  var navUser = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/navigations/nav_user/nav_user.html'
    };
  };
  
  /* global angular */
  angular
    .module('noclue')
    .directive('navUser', navUser);
})();
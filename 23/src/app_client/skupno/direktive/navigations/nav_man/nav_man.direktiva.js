(function() {
  var navMan = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/navigations/nav_man/nav_man.html'
    };
  };
  
  /* global angular */
  angular
    .module('noclue')
    .directive('navMan', navMan);
})();
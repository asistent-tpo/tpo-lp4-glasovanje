(function() {
  function administratorCtrl($window, $uibModal, notificationData, $location, avtentikacija) {
    var vm = this;
    
    if(!avtentikacija.preveriTipUporabnika("admin")) {
      $location.path("/adminPrijava");
    }
    
    notificationData.notifications().then(
        function succes(response){
         console.log(JSON.stringify(response.data, null, 4));
         vm.notifications = response.data;
      },
      function error(response){
        //console.log(JSON.stringify(response, null, 4));
        if (response.statusText === 'Unauthorized') {
          $window.location.href = '/';
        } else {
          vm.msg = "Error while fetching notifications.";
        }
    });
    
    
    vm.eventPrikaziPojavnoOkno = function() {
     $uibModal.open({
         templateUrl: 'okno_dodaj_obvestilo/dodaj_obvestilo.html',
        controller: 'dodajObvestiloCtrl',
        controllerAs: 'vm'
     
      });
    };
    
    vm.changePassword = function(){
      $location.path("/spremembaGesla");
    };
    
 

  }
  
  administratorCtrl.$inject = ['$window', '$uibModal', 'notificationData', '$location', 'avtentikacija'];

  /* global angular */
  angular
    .module('noclue')
    .controller('administratorCtrl', administratorCtrl);
})();
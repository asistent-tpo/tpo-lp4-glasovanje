(function() {
  function studentEventCtrl(eventsData, $window, avtentikacija, $location) {
 var vm = this;
    vm.events1 = [];
    vm.events2 = [];
    vm.events3 = [];
    
   if(!avtentikacija.preveriTipUporabnika("student")) {
      $location.path("/");
    }
    
   eventsData.events().then(
        function succes(response){
         console.log(JSON.stringify(response.data, null, 4));
          var resultEvents = response.data;
          
          
          for(var i = 0 ; i< resultEvents.length; i++){
            if(i%3 == 0){
              vm.events1.push(resultEvents[i]);  
            }else if(i%3 == 1){
              vm.events2.push(resultEvents[i]);  
            }else if(i%3 == 2){
              vm.events3.push(resultEvents[i]);  
            }
          }
          
    
          vm.msg = "";
      },
      function error(response){
        //console.log(JSON.stringify(response, null, 4));
        if (response.statusText === 'Unauthorized') {
          $window.location.href = '/';
        } else {
          vm.msg = "Error while fetching events.";
        }
      });
   
  
  }
  
  studentEventCtrl.$inject = ['eventsData', '$window', 'avtentikacija', '$location'];
  /* global angular */
  angular
    .module('noclue')
    .controller('studentEventCtrl', studentEventCtrl);
})();
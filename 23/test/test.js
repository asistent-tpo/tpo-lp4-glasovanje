var zahteva = require('supertest')
var streznik = require('../src/app.js')
var zeton = "";
var atob = require('atob');
var id = "";

var b64Utf8 = function (niz) {
      return decodeURIComponent(Array.prototype.map.call(atob(niz), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
};

describe('Testi', function() {
  
  //STUDENT
  
  it ('register', function(done) {
    zahteva(streznik)
    .post('/api/register')
    .set('Content-Type', 'application/json')
    .send({email: "a@b", password: "aaa", userType: "student"})
    .expect(200)
    .end(done);
  });
  
  
  it ('login', function(done) {
    zahteva(streznik)
    .post('/api/login')
    .set('Content-Type', 'application/json')
    .send({email: "a@b", tipUporabnika: "student", password: "aaa"})
    .expect(200)
    .then(response => {
      zeton="Bearer "+response.body.zeton;
      //console.log(zeton);
      var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
      id=koristnaVsebina._id;
      //console.log("iddd: " + id);
      done();
    });
  });
  
  
  it ('ustvariEvent', function(done) {
    //console.log("iddd222222: " + id);
    zahteva(streznik)
    .post('/api/koledar')
    .set('Content-Type', 'application/json')
    .set('Authorization', zeton)
    .send({"date":"21/4/2019","title":"sad"})
    .expect(201)
    .end(done);
  });
  
  
  it ('izbrisiEvent', function(done) {
    //console.log("iddd222222: " + id);
    zahteva(streznik)
    .post('/api/deleteKoledar')
    .set('Content-Type', 'application/json')
    .set('Authorization', zeton)
    .send({"date":"21/4/2019","title":"sad"})
    .expect(204)
    .end(done);
  });
  
  
  it ('ustvariUrnik', function(done) {
    //console.log("iddd222222: " + id);
    zahteva(streznik)
    .post('/api/urnik')
    .set('Content-Type', 'application/json')
    .set('Authorization', zeton)
    .send({"title":"asdsadsad","day":"4","hourStart":9,"minStart":0,"hourEnd":12,"minEnd":0,"color":"0","userId":id})
    .expect(201)
    .end(done);
  });
  
  
  it ('editUrnik', function(done) {
    var idUrnik = "";
    var delIdUrnik = "";
    zahteva(streznik)
    .get('/api/urnik')
    .set('Authorization', zeton)
    .then(response => {
      //console.log("RES BODYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY: " + response.body[0]._id);
      idUrnik = response.body[0]._id;
      console.log("ID URNIKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKkkkkkkkkkkkkkkkkkkkkkkkkKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK: " + idUrnik);
      delIdUrnik="/api/urnik/"+idUrnik;
      //console.log("DEL ID: " + delId);
      zahteva(streznik)
      .put(delIdUrnik)
      .set('Content-Type', 'application/json')
      .set('Authorization', zeton)
      .send({"title":"testUrnik","day":"5","hourStart":12,"minStart":0,"hourEnd":15,"minEnd":22,"color":"0","_id":idUrnik})
      .expect(200)
      .end(done);
    });
  });
  
  
  it ('izbrisiUrnik', function(done) {
    var idUrnik = "";
    var delIdUrnik = "";
    zahteva(streznik)
    .get('/api/urnik')
    .set('Authorization', zeton)
    .then(response => {
      //console.log("RES BODYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY: " + response.body[0]._id);
      idUrnik = response.body[0]._id;
      //console.log("ID TODO: " + idTodo);
      delIdUrnik="/api/urnik/"+idUrnik;
      //console.log("DEL ID: " + delId);
      zahteva(streznik)
      .delete(delIdUrnik)
      .set('Authorization', zeton)
      .expect(204)
      .end(done);
    });
  });
  
  
  it ('ustvariTodo', function(done) {
    //console.log("iddd222222: " + id);
    zahteva(streznik)
    .post('/api/todo')
    .set('Content-Type', 'application/json')
    .set('Authorization', zeton)
    .send({"description":"test","userId":id})
    .expect(201)
    .end(done);
  });
  
  it ('editTodo', function(done) {
    var idTodo = "";
    var delId = "";
    zahteva(streznik)
    .get('/api/todo')
    .set('Authorization', zeton)
    .then(response => {
      //console.log("RES BODYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY: " + response.body[0]._id);
      idTodo = response.body[0]._id;
      //console.log("ID TODO: " + idTodo);
      delId="/api/todo/"+idTodo;
      //console.log("DEL ID: " + delId);
      zahteva(streznik)
      .put(delId)
      .set('Content-Type', 'application/json')
      .set('Authorization', zeton)
      .send({"description":"testEdit","_id":idTodo})
      .expect(200)
      .end(done);
    });
  });
  
  it ('izbrisiTodo', function(done) {
    var idTodo = "";
    var delId = "";
    zahteva(streznik)
    .get('/api/todo')
    .set('Authorization', zeton)
    .then(response => {
      //console.log("RES BODYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY: " + response.body[0]._id);
      idTodo = response.body[0]._id;
      //console.log("ID TODO: " + idTodo);
      delId="/api/todo/"+idTodo;
      //console.log("DEL ID: " + delId);
      zahteva(streznik)
      .delete(delId)
      .set('Authorization', zeton)
      .expect(204)
      .end(done);
    });
  });
  
  
  it ('spremembaGesla', function(done) {
    zahteva(streznik)
    .post('/api/spremembaGesla')
    .set('Content-Type', 'application/json')
    .send({email: "a@b", password: "aaa", newPassword: "n"})
    .expect(200)
    .end(done);
  });
  
  
  it ('postaje', function(done) {
    zahteva(streznik)
    .get('/trola/')
    .expect(200)
    .end(done);
  });
  
  
  it ('food', function(done) {
    zahteva(streznik)
    .get('/food/')
    .expect(200)
    .end(done);
  });
  
  
  it ('studentEventi', function(done) {
    zahteva(streznik)
    .get('/studentEventi/')
    .expect(200)
    .end(done);
  });
  
  
  it ('userDelete', function(done) {
    zahteva(streznik)
    .post('/api/userDelete')
    .set('Content-Type', 'application/json')
    .send({email: "a@b"})
    .expect(204)
    .end(done);
  });
  
  
  
  
  /////////////////ORGANIZATOR//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  
  it ('registerO', function(done) {
    zahteva(streznik)
    .post('/api/register')
    .set('Content-Type', 'application/json')
    .send({"email":"org1@org1","password":"org1","password2":"org1","userType":"organizer"})
    .expect(200)
    .end(done);
  });
  
  
  it ('loginO', function(done) {
    zahteva(streznik)
    .post('/api/login')
    .set('Content-Type', 'application/json')
    .send({email: "org1@org1", tipUporabnika: "organizer", password: "org1"})
    .expect(200)
    .then(response => {
      zeton="Bearer "+response.body.zeton;
      //console.log(zeton);
      var koristnaVsebina = JSON.parse(b64Utf8(zeton.split('.')[1]));
      id=koristnaVsebina._id;
      //console.log("iddd: " + id);
      done();
    });
  });
  
  
  it ('dodajEvent', function(done) {
    //console.log("v eventi: " + zeton);
    zahteva(streznik)
    .post('/api/events')
    .set('Content-Type', 'application/json')
    .set('Authorization', zeton)
    .send({"title":"test1","location":"","date":"1901-01-01T00:00:00.000Z","day":1,"month":"0","year":1,"description":"opis1","organizer":"org1"})
    .expect(201)
    .end(done);
  });
  
  it ('spremembaGeslaO', function(done) {
    zahteva(streznik)
    .post('/api/spremembaGesla')
    .set('Content-Type', 'application/json')
    .send({email: "org1@org1", password: "org1", newPassword: "org2"})
    .expect(200)
    .end(done);
  });
  
  
  it ('userDeleteO', function(done) {
    zahteva(streznik)
    .post('/api/userDelete')
    .set('Content-Type', 'application/json')
    .send({email: "org1@org1"})
    .expect(204)
    .end(done);
  });
  


  
  
});
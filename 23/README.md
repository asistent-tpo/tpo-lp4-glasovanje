# 23. skupina

S pomočjo naslednje oddaje na spletni učilnici lahko z uporabo uporabniškega imena in gesla dostopate do produkcijske različice aplikacije.

## Oddaja na spletni učilnici

Bitbucket: https://bitbucket.org/BuckyBuckk/tpo-lp4/commits/574bbef1eaa41d58c7bd48a83d358f4b0a03fa24

Produkcija: https://tpo-skupina23.herokuapp.com/



Student:

Uporabniško ime: student@student 

Geslo: student



Organizer:

Uporabniško ime: a@a

Geslo: a



Administrator:

Uporabniško ime: admin@admin

Geslo: admin
